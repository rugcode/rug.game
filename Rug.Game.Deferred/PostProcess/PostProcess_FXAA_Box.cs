﻿using OpenTK.Graphics;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Simple;
using Rug.Game.Shaders.PostProcess;

namespace Rug.Game.Core.PostProcess
{
	public class PostProcess_FXAA_Box : TextureBox
	{
		protected new PostProcess_FXAA_Effect Effect;

		#region Properties

		#endregion

		public PostProcess_FXAA_Box()
		{
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["PostProcess_FXAA_Effect"] as PostProcess_FXAA_Effect;
			}

			this.Depth = 0.5f;
			this.Color = new Color4(1f, 1f, 1f, 1f); 
		}

		public override void Render(View3D view)
		{
			CheckAndWriteRectangle();

			Effect.Render(Texture, Vertices); 
		}
	}
}
