﻿
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Deferred.Pipeline;
namespace Rug.Game.Core.PostProcess
{
	public class PostProcess_FXAA : PipelineManager, IResource, IResizablePipeline
	{
		private FrameBuffer m_FrameBuffer;
		private PostProcess_FXAA_Box m_Process;

		private DeferredQuality m_Quality;

		private bool m_IsLoaded = false;

		#region Properties

		public int Width { get; set; }
		public int Height { get; set; }

		public FrameBufferTexture2D ColorTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0Ext]; } }

		//public FrameBufferTexture2D DepthTexture { get { return m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt]; } }

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public new ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		

		#endregion

		public PostProcess_FXAA(int width, int height, DeferredQuality quality)
		{
			Name = "Post Process: FXAA";

			Width = width;
			Height = height;
			m_Quality = quality; 
		}

		public void Initiate()
		{
			PixelInternalFormat internalFormat;
			PixelType pixelType;

			switch (m_Quality)
			{
				case DeferredQuality.High:
					internalFormat = PixelInternalFormat.Rgba16f;
					pixelType = PixelType.HalfFloat;
					break;
				case DeferredQuality.Medium:
					internalFormat = PixelInternalFormat.Rgb10A2;
					pixelType = PixelType.HalfFloat;
					break;
				case DeferredQuality.Low:
					internalFormat = PixelInternalFormat.Rgba8;
					pixelType = PixelType.UnsignedByte;
					break;
				default:
					internalFormat = PixelInternalFormat.Rgba16f;
					pixelType = PixelType.HalfFloat;
					break;
			}		

			m_FrameBuffer = new FrameBuffer("Meshes Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(Width, Height),
						InternalFormat = internalFormat,
						PixelFormat = PixelFormat.Rgba,
						PixelType = pixelType,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}
					/* ,					
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(Width, Height),
						InternalFormat = (PixelInternalFormat)All.DepthComponent24,
						PixelFormat = PixelFormat.DepthComponent,
						PixelType = PixelType.UnsignedInt,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}*/ 
					));

			m_Process = new PostProcess_FXAA_Box();
			m_Process.Texture = ColorTexture;

			m_Process.FlipVertical = true; 
		}

		public override void Render(View3D view)
		{
			m_FrameBuffer.Bind();

			GLState.EnableDepthMask = false;
			GLState.EnableDepthTest = false; 
			GLState.ClearColor(Color4.Black);
			GLState.ClearDepth(1.0f);
			GLState.EnableBlend = false;
			GLState.EnableCullFace = false;
			GLState.EnableAlphaTest = false;

			GLState.Apply(Width, Height);
			
			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			//GL.Clear(ClearBufferMask.DepthBufferBit);			

			base.Render(view);

			m_FrameBuffer.Unbind();

			m_Process.Render(view); 
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width;
			Height = height;

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(width, height, samples);
				}
			}
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				System.Drawing.Size size = new System.Drawing.Size(Width, Height);

				m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = size;
				//m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt].ResourceInfo.Size = size;

				m_FrameBuffer.LoadResources();

				m_Process.LoadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.LoadResources();
					}
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_FrameBuffer.UnloadResources();

				m_Process.UnloadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.UnloadResources();
					}
				}

				m_IsLoaded = false; 
			}
		}

		#endregion
	}
}
