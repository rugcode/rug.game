﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.MeshData;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Deferred.Data;
using Rug.Game.Shaders.Deferred;
using Rug.Game.Shaders.Terrain;
using System;

namespace Rug.Game.Deferred.Pipeline
{
	public class DirectLightingSettings
	{
		public Vector3 LightDir;
		public Matrix4[] LightMatrix = new Matrix4[3];
		public float[] DepthStarts = new float[3];
		public float[] DepthEnds = new float[3];
	}

	// 
	// Deferred_Shadow_Mesh
	public class DeferredShadowPass : PipelineBase, IResource
	{
		protected Deferred_Shadow_Mesh Effect;
		protected DynamicGridRender GridEffect; 

		//protected Deferred_DiffuseTextured_Skinned Effect_Skinned; 

		private bool m_IsLoaded; 

		#region Properties

		public MeshCollection Meshes { get; set; }

		public DirectLightingSettings LightingSettings { get; set; }

		public DynamicHexGrid DynamicHexGrid { get; set; } 

		//public BonedMeshCollection BonedMeshes { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		
		private int m_Index;
		private float m_Size;
		private float m_Offset;

		#endregion

		public DeferredShadowPass(DirectLightingSettings settings, int index, float size, float offset)
		{
			Name = "Deferred Shadow Mesh";

			LightingSettings = settings;
			m_Index = index;
			m_Size = size;
			m_Offset = offset; 

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_Shadow_Mesh"] as Deferred_Shadow_Mesh;
			}

			GridEffect = SharedEffects.Effects["DynamicGridRender"] as DynamicGridRender;

			IsEnabled = true; 
		}

        public override void Update(View3D view)
		{
		
		}

		public override void Render(View3D view)
		{
			GLState.EnableDepthMask = true; 
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = false; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = true; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = true; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true; // Enable(EnableCap.DepthTest);
			//GLState.FrontFace(FrontFaceDirection.Cw); // Back face!
			GLState.FrontFace(FrontFaceDirection.Ccw); // front face
			GLState.Apply(view);

			Vector3 dir = view.LightDirection; // *-1f;
			dir.Normalize();
			
			/* 
			Vector3 right = new Vector3(dir.Z, 0, -dir.X);
			right.Normalize();
			Vector3 up = Vector3.Cross(dir, right);
			*/

			//Vector3 min = new Vector3(-m_Size, -m_Size, -100); // -view.Camera.Center;
			//Vector3 max = new Vector3(m_Size, m_Size, 800); // -view.Camera.Center; 
			Vector3 min = new Vector3(-m_Size, -m_Size, -1024); // -view.Camera.Center;
			Vector3 max = new Vector3(m_Size, m_Size, 1024); // -view.Camera.Center; 

			//Vector3 min = new Vector3(-512, -512, -2000); // -view.Camera.Center;
			//Vector3 max = new Vector3(512, 512, 2000); // -view.Camera.Center; 

			//Matrix4 inverseView = view.View;

			//inverseView.Invert(); 

			if (m_Index == 0)
			{
				LightingSettings.DepthStarts[m_Index] = 0;
			}
			else
			{
				LightingSettings.DepthStarts[m_Index] = (LightingSettings.DepthEnds[m_Index - 1] * 0.25f) * 3.0f;
			}

			LightingSettings.DepthEnds[m_Index] = m_Offset + (m_Size * 0.75f); 

			Vector3 center = view.Camera.Center + (view.Camera.Forward * m_Offset);

			// Compute the MVP matrix from the light's point of view
			Matrix4 depthProjectionMatrix = Matrix4.CreateOrthographicOffCenter(min.X, max.X, min.Y, max.Y, min.Z, max.Z); // (-500, 500, -300, 300, -3000, 3000);
			//Matrix4 depthViewMatrix = Matrix4.LookAt(view.LightDirection * -1f, view.Camera.Center, Vector3.UnitY);
			Matrix4 depthViewMatrix = Matrix4.LookAt(dir - center, -center, Vector3.UnitY);// up);

			Matrix4 depthMVP = depthViewMatrix * depthProjectionMatrix;

			LightingSettings.LightDir = view.LightDirection;
			LightingSettings.LightMatrix[m_Index] = depthMVP; 

			//Matrix4 depthViewMatrix = Matrix4.LookAt((view.LightDirection * -1f) + view.Camera.Center, view.Camera.Center, Vector3.UnitY);
			// or, for spot light :
			//glm::vec3 lightPos(5, 20, 20);
			//glm::mat4 depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 2.0f, 50.0f);
			//glm::mat4 depthViewMatrix = glm::lookAt(lightPos, lightPos-lightInvDir, glm::vec3(0,1,0));

			/* 
			if (Meshes != null)
			{
				Effect.Begin(view.World, view.Projection);

				foreach (Mesh mesh in Meshes)
				{
					if (mesh.IsVisible == true)
					{
						Matrix4 depthModelMatrix = depthMVP * mesh.ObjectMatrix; 
						//Matrix4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
						RenderMesh(mesh, depthModelMatrix);
					}
				}

				Effect.End();
			}
			*/ 
			Matrix4 depthModelMatrix = depthMVP;

			GridEffect.Render(view, ref depthModelMatrix, DynamicHexGrid.Buffer, DynamicHexGrid.Count, DynamicHexGrid.Texture); 
		}

		protected virtual void RenderMesh(Mesh mesh, Matrix4 depthMVP) 
		{
			mesh.Bind();

			Effect.Render(ref depthMVP, mesh.IndexCount, mesh.IndexType);

			mesh.Unbind();
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_IsLoaded = false;
			}			
		}
	}
}
