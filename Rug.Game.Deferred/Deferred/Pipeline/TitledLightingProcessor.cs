﻿using Rug.Game.Core.Effect;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Data;
using Rug.Game.Shaders.Deferred;

namespace Rug.Game.Deferred.Pipeline
{
	public class TitledLightingProcessor : IResource, IPipeline, IResizablePipeline
	{
		public const int MaxLights = 16;
		public const int BlockSize = 16;
		private float m_Time = 0; 

		private Deferred_TiledLighting[] Effect; 

		// Lighting tiles, Calculate Min / Max 
		// Calculate Lights per Tile
		
		// Per-pixel, iterate all lights in tile for pixel
		// apply pecular and diffuse for each light

		public Texture2D NormalMap { get; set; }

		//public Texture2D DepthMap;
		//private ComputeImage2D DepthMapCL;

		public Texture2D DepthMap { get; set; }

		public Texture2D MaterialMap { get; set; }

		private bool m_IsLoaded;
		
		public Texture2D LightingMap { get; set; }

		public PointLightCollection Lights { get; set; } 

		#region Public Members

		public int Width { get; set; }

		public int Height { get; set; } 

		public bool IsEnabled { get; set; }

		public string Name { get; set; }

		public PipelineMode PipelineMode { get { return Core.Pipeline.PipelineMode.Render; } }

		public ResourceType ResourceType { get { return ResourceType.Custom; } }

		public ResourceMode ResourceMode { get { return ResourceMode.Static; } }

		public IResourceInfo ResourceInfo { get { return null; } }

		public uint ResourceHandle { get { return 0; } }

		public bool IsLoaded { get { return m_IsLoaded; } }

		
		private int m_Quality;

		public Deferred_TiledLighting_Mode Mode { get; set; }

		public bool Animate { get; set; } 

		#endregion

		public TitledLightingProcessor(int width, int height, int quality)
		{
			Mode = Deferred_TiledLighting_Mode.Lighting;
			m_Quality = quality; 

			if (Effect == null)
			{
				Effect = new Deferred_TiledLighting[3];

				for (int i = 0; i < 3; i++) 
				{
					Effect[i] = SharedEffects.Effects["Deferred_TiledLighting_" + ((Deferred_TiledLighting_Mode)i).ToString()] as Deferred_TiledLighting;
				}
			}

			Width = width / m_Quality;
			Height = height / m_Quality;

			IsEnabled = true;
			Animate = false; 
		}

		public void Initiate()
		{
			LightingMap = new Texture2D("Lighting Map", ResourceMode.Static, new Texture2DInfo()
			{
				Border = 0,
				InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba8,
				MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear,
				MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear,
				PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba,
				PixelType = OpenTK.Graphics.OpenGL.PixelType.UnsignedInt8888,
				Size = new System.Drawing.Size(Width, Height),
				WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});			
		}		

		#region IPipeline Members

        public void Update(View3D view)
		{

		}

		public void Render(View3D view)
		{					
			int cellsX, cellsY;

			cellsX = Width / BlockSize;
			cellsY = Height / BlockSize;

			if (cellsX * BlockSize < Width)
			{
				cellsX += 1;
			}

			if (cellsY * BlockSize < Height)
			{
				cellsY += 1;
			}

			Effect[(int)Mode].Render(view, Width, Height, (uint)cellsX, (uint)cellsY, LightingMap, NormalMap, MaterialMap, DepthMap, Lights.Buffer, Lights.Count); 
		}

		public void Begin()
		{
	
		}

		public void End()
		{
		
		}

		#endregion

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width / m_Quality;
			Height = height / m_Quality;

			//DepthMap.ResourceInfo.Size = new System.Drawing.Size(ExternalDepth.ResourceInfo.Size.Width, ExternalDepth.ResourceInfo.Size.Height); 
			LightingMap.ResourceInfo.Size = new System.Drawing.Size(Width, Height); 
		}

		public void LoadResources()
		{
			LightingMap.LoadResources();
			LightingMap.CreateBlank();
		}

		public void UnloadResources()
		{
			LightingMap.UnloadResources();
		}
	}
}
