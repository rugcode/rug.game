﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Shaders.Deferred;
using System;
using System.Collections.Generic;

namespace Rug.Game.Deferred.Pipeline
{
	public interface ILineBuffer
	{
		int VertexCount { get; } 
		VertexBuffer Vertices { get; } 
	}

	public class DeferredLinePass : PipelineBase, IResource
	{
		protected Deferred_Lines Effect; 
		private bool m_IsLoaded; 

		#region Properties

		public List<ILineBuffer> Buffers = new List<ILineBuffer>(); 

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		

		#endregion

		public DeferredLinePass()
		{
			Name = "Lines Pass"; 

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_Lines"] as Deferred_Lines;
			}
		}

        public override void Update(View3D view)
		{
		
		}

		public override void Render(View3D view)
		{
			GLState.EnableDepthMask = true; 
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = false; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = true; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = true; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true; // Enable(EnableCap.DepthTest);
			GLState.FrontFace(FrontFaceDirection.Ccw); 
			GLState.Apply(view);

			Effect.Begin(ref view.World, ref view.Projection); // , ref view.NormalWorld);

			Matrix4 mat = Matrix4.Identity; 

			foreach (ILineBuffer buffer in Buffers)
			{
				Effect.Render(ref mat, buffer.Vertices, buffer.VertexCount); 
			}

			Effect.End(); 		
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_IsLoaded = false;
			}			
		}
	}
}

