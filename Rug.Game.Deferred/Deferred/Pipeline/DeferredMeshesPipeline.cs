﻿using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;

using Rug.Game.Core.Resources;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredMeshesPipeline : PipelineManager, IResource, IResizablePipeline
	{
		private FrameBuffer m_FrameBuffer;
		private DeferredQuality m_Quality;
		private bool m_IsLoaded = false;

		#region Properties

		public int Width { get; set; }
		public int Height { get; set; }

		public FrameBufferTexture2D ColorTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0Ext]; } }

		public FrameBufferTexture2D NormalTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment1Ext]; } }

		public FrameBufferTexture2D MaterialTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment2Ext]; } }

		public FrameBufferTexture2D DepthTexture { get { return m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt]; } }

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public new ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public int MSAA { get; set; }
		public Viewport Viewport { get; set; }

		

		#endregion

		public DeferredMeshesPipeline(int width, int height, DeferredQuality quality, int MSAA)
		{
			Name = "Deferred Meshes";

			Width = width;
			Height = height;
			m_Quality = quality;

			this.MSAA = MSAA;
			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);
		}

		public void Initiate()
		{
			PixelInternalFormat internalFormat;
			PixelInternalFormat internalFormat_Materials;
			PixelType pixelType;
			PixelType pixelType_Materials;

			switch (m_Quality)
			{
				case DeferredQuality.Medium:
					internalFormat = PixelInternalFormat.Rgb10A2;
					pixelType = PixelType.UnsignedInt1010102;

					internalFormat_Materials = PixelInternalFormat.Rgba8;
					pixelType_Materials = PixelType.UnsignedInt8888;
					break;
				case DeferredQuality.Low:
					internalFormat = PixelInternalFormat.Rgb10A2;
					pixelType = PixelType.UnsignedInt1010102;

					internalFormat_Materials = PixelInternalFormat.Rgba8;
					pixelType_Materials = PixelType.UnsignedInt8888;
					break;
				default:
					internalFormat = PixelInternalFormat.Rgba16f;					
					pixelType = PixelType.HalfFloat;

					internalFormat_Materials = PixelInternalFormat.Rgba16f;
					pixelType_Materials = PixelType.HalfFloat;
					break;
			}	

			m_FrameBuffer = new FrameBuffer("Meshes Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = internalFormat,
						PixelFormat = PixelFormat.Rgba,
						PixelType = pixelType,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Normal", FramebufferAttachment.ColorAttachment1Ext)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = PixelInternalFormat.Rgb10A2, // PixelInternalFormat.Rgb10A2
						PixelFormat = PixelFormat.Rgba,
						PixelType = PixelType.UnsignedInt1010102,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Material", FramebufferAttachment.ColorAttachment2Ext)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = internalFormat_Materials,
						PixelFormat = PixelFormat.Rgba,
						PixelType = pixelType_Materials,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = (PixelInternalFormat)All.DepthComponent24,
						//InternalFormat = (PixelInternalFormat)All.DepthComponent32f,
						PixelFormat = PixelFormat.DepthComponent,
						PixelType = PixelType.UnsignedInt,
						//PixelType = PixelType.Float,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}));

			// ArtworkEnvironment.ResolutionDependentResources.Add(m_FrameBuffer);
		}

		public override void Render(View3D view)
		{
			//GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBOHandle);
			//GL.DrawBuffers(2, buffers);


			Viewport backup = GLState.Viewport;
			GLState.Viewport = Viewport;
			//GLState.Apply(view); 
			
			m_FrameBuffer.Bind();

			//GLState.ClearColor(new Color4(0f, 0f, 0f, 1f)); 

			GLState.EnableDepthMask = true;
			GLState.EnableDepthTest = true; 
			//GLState.ClearColor(Color4.DarkOrange);
			GLState.ClearColor(Color4.Black);
			GLState.ClearDepth(1.0f);

			GLState.EnableBlend = false;
			GLState.EnableDepthTest = true;
			GLState.EnableCullFace = true;
			GLState.EnableAlphaTest = true; // (EnableCap.AlphaTest);
			GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true;

			GLState.Apply(Width * MSAA, Height * MSAA);

			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit | ClearBufferMask.AccumBufferBit);
			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);			
			//GL.ClearBuffer(ClearBuffer.Color, 2, new float[] { 0f, 8f, 0f, 0f});

			GL.Clear(ClearBufferMask.DepthBufferBit);
			//GL.ClearBuffer(ClearBuffer.Color, 2, new float[] { 0f, 8f, 0f, 0f });

			base.Render(view);

			m_FrameBuffer.Unbind();

			GLState.EnableAlphaTest = false;

			GLState.Viewport = backup;

			//GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width;
			Height = height;

			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(width * MSAA, height * MSAA, MultiSamples.X1);
				}
			}
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

				System.Drawing.Size size = new System.Drawing.Size(Width * MSAA, Height * MSAA);

				m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = size;
				m_FrameBuffer[FramebufferAttachment.ColorAttachment1].ResourceInfo.Size = size;
				m_FrameBuffer[FramebufferAttachment.ColorAttachment2].ResourceInfo.Size = size;
				m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt].ResourceInfo.Size = size;

				m_FrameBuffer.LoadResources();

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_FrameBuffer.UnloadResources();

				m_IsLoaded = false; 
			}
		}

		#endregion
	}
}
