﻿using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredLightingPipeline : PipelineManager, IResizablePipeline, IResource
	{
		private FrameBuffer m_FrameBuffer;
		private bool m_IsLoaded = false;

		#region Properties
		
		public int Width { get; set; }
		public int Height { get; set; }

		public Texture2D ColorTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0Ext]; } }

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public int MSAA { get; set; }
		public Viewport Viewport { get; set; }

		

		#endregion

		public DeferredLightingPipeline(int width, int height, int MSAA)
		{
			Name = "Deferred Lighting";

			Width = width;
			Height = height;


			this.MSAA = MSAA;
			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);
		}

		public void Initiate()
		{
			m_FrameBuffer = new FrameBuffer("Lighting Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Light", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = PixelInternalFormat.Rgba16f,
						PixelFormat = PixelFormat.Rgba,
						PixelType = PixelType.Float,
						MagFilter = TextureMagFilter.Nearest,
						MinFilter = TextureMinFilter.Nearest,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},			
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = (PixelInternalFormat)All.DepthComponent24,
						PixelFormat = PixelFormat.DepthComponent,
						PixelType = PixelType.UnsignedInt,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}));

			// ArtworkEnvironment.ResolutionDependentResources.Add(m_FrameBuffer);
			/* 
			width =a;
			height =h;
        
			program = new ProgramLoader("DeferredShaderPointLight");
			program.loadProgram();
    
			GL.BindAttribLocation(program.Program, (int)ATTRIB.ATTRIB_VERTEX, "position");
			GL.BindAttribLocation(program.Program, (int)ATTRIB.ATTRIB_NORMAL, "normal");
			GL.BindAttribLocation(program.Program, (int)ATTRIB.ATTRIB_COLOR, "color");

			program.linkProgram();
			GL.UseProgram(program.Program);
    
			uNormalTexture= GL.GetUniformLocation(program.Program, "normalTexture");
			uDepthTexture= GL.GetUniformLocation(program.Program, "depthTexture");
			GL.Uniform1(uNormalTexture, 0);
			GL.Uniform1(uDepthTexture, 1);
    
			uCenter = GL.GetUniformLocation(program.Program, "center");
			uLightSize= GL.GetUniformLocation(program.Program, "lightSize");
			uObjectMatrix = GL.GetUniformLocation(program.Program, "objectMatrix");
   
			uNormalMatrix = GL.GetUniformLocation(program.Program,"worldMatrix");
			uProj = GL.GetUniformLocation(program.Program,"perspectiveMatrix");
			uPerspectiveInvMatrix = GL.GetUniformLocation(program.Program,"perspectiveInvMatrix");
			uNormalWorldMatrix= GL.GetUniformLocation(program.Program,"normalWorldMatrix");
   
			GL.UseProgram(0);
			*/ 
		}

		public override void Render(View3D view)
		{

			Viewport backup = GLState.Viewport;
			GLState.Viewport = Viewport;

			m_FrameBuffer.Bind();
			GLState.ClearColor(new Color4(0f, 0f, 0f, 1f));
			//GLState.ClearColor(Color4.DarkGreen);
			GLState.ClearDepth(1.0f);
			GLState.EnableDepthMask = true;
			GLState.Apply(Width * MSAA, Height * MSAA);
			//GLState.Viewport = new Objects.Rendering.Viewport(0, 0, width, height);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GLState.EnableDepthMask = false;
			GLState.Apply(Width * MSAA, Height * MSAA);
			/* 
			GL.BindFramebuffer(FramebufferTarget.FramebufferExt, fbo);
			GL.ClearColor(0, 0, 0, 1);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.Viewport(0, 0, width  ,height);
			GL.UseProgram(program.Program);  
    
			GL.DepthMask(false);
      
			GL.ActiveTexture(TextureUnit.Texture1);
			GL.BindTexture(TextureTarget.Texture2D, depthTexture);
  
			GL.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, normalTexture);  
        
			GL.UniformMatrix4(uNormalMatrix, false, ref cam.worldMatrix);
			GL.UniformMatrix4(uProj, false, ref cam.perspectiveMatrix);
			GL.UniformMatrix4(uPerspectiveInvMatrix, false, ref cam.perspectiveInvMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref cam.normalWorldMatrix);

			GL.EnableVertexAttribArray((int)ATTRIB.ATTRIB_VERTEX);
			GL.EnableVertexAttribArray((int)ATTRIB.ATTRIB_NORMAL);
			GL.EnableVertexAttribArray((int)ATTRIB.ATTRIB_COLOR);
			*/

			base.Render(view);

			/* 
			GL.DisableVertexAttribArray((int)ATTRIB.ATTRIB_VERTEX);
			GL.DisableVertexAttribArray((int)ATTRIB.ATTRIB_NORMAL);
			GL.DisableVertexAttribArray((int)ATTRIB.ATTRIB_COLOR);
			GL.BindBuffer(BufferTarget.ElementArrayBuffer,  0);
			GL.BindBuffer(BufferTarget.ArrayBuffer,0);    
    
			GL.UseProgram(0);
			GL.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);   
			GL.DepthMask(true);
			 */

			GLState.EnableDepthMask = true;
			GLState.ClearColor(new Color4(0f, 0f, 0f, 0f));

			m_FrameBuffer.Unbind();


			GLState.Viewport = backup;
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width;
			Height = height;

			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(width * MSAA, height * MSAA, MultiSamples.X1);
				}
			}
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

				System.Drawing.Size size = new System.Drawing.Size(Width * MSAA, Height * MSAA);

				m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = size;
				m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt].ResourceInfo.Size = size;

				m_FrameBuffer.LoadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.LoadResources();
					}
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.UnloadResources();
					}
				}

				m_FrameBuffer.UnloadResources();

				m_IsLoaded = false; 
			}
		}
	}
}
