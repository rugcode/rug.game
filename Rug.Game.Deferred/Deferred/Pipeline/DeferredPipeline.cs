﻿using Rug.Game.Core.MeshData;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.PostProcess;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Data;
using Rug.Game.Deferred.Data.Terrain;
using System;
using System.Collections.Generic;

namespace Rug.Game.Deferred.Pipeline
{
	public enum DeferredQuality { High, Medium, Low } 

	public class DeferredPipeline : PipelineManager, IResource, IResizablePipeline
	{
		private bool m_IsLoaded = false;
		private DeferredQuality m_Quality;
		private bool m_ApplyDynamicLighting = true;

		#region Properties

		public DeferredMeshesPipeline MeshesPipeline;
		//public DeferredLightingPipeline Lighting;
		public PostProcess_FXAA FXAA;
		private DeferredFinalPass m_Final = new DeferredFinalPass();
		private DiffuseTexturedPass m_MeshColor = new DiffuseTexturedPass();

		public TitledLightingProcessor Lighting;
		public DeferredShadowPipeline[] ShadowPipeline;
		public DeferredShadowFinalPipeline ShadowFinalPipeline;
		private DeferredShadowFinalPass m_ShadowFinal = new DeferredShadowFinalPass();

		public Viewport Viewport { get; set; } 

		public int Width { get; set; }
		public int Height { get; set; }
		public int Oversize { get; set; }
		public int MSAA { get; set; }

		public ITerrainPatchDataProvider TerrainData { get; set; } 

		public DeferredQuality Quality { get { return m_Quality; } } 

		public List<ILineBuffer> LineBuffers { get; set; } 

		public MeshCollection Meshes { get; set; }
		
		public BonedMeshCollection BonedMeshes { get; set; } 

		public MeshCollection Decals { get; set; }

		public MeshBatchCollection MeshBatches { get; set; }

		public PointLightCollection PointLights { get; set; }

		public DynamicHexGrid DynamicHexGrid { get; set; }

		//public LightingMeshCollection LightingMeshes { get; set; }

		public Texture2D LambertMap { get { return m_Final.LambertMap; } set { m_Final.LambertMap = value; } }

		public Texture2D ColorCastTexture
		{
			get { return m_Final.ColorCastTexture; }
			set { m_Final.ColorCastTexture = value; }
		}

		public float ColorCastAmount
		{
			get { return m_Final.ColorCastAmount; }
			set { m_Final.ColorCastAmount = value; }
		}

		public float IndirectAmbiant
		{
			get { return m_Final.IndirectAmbiant; }
			set { m_Final.IndirectAmbiant = value; }
		}

		public TextureDDS ReflectionsMap { get { return m_Final.ReflectionsMap; } set { m_Final.ReflectionsMap = value; } }

		public TextureDDS FogMap { get { return m_Final.FogMap; } set { m_Final.FogMap = value; } }

		public Texture2D NoiseTexture { get { return m_ShadowFinal.NoiseTexture; } set { m_ShadowFinal.NoiseTexture = value; } } 

		public bool ShowMaterials { get { return m_Final.ShowMaterials; } set { m_Final.ShowMaterials = value; } }
		public bool ShowNormals { get { return m_Final.ShowNormals; } set { m_Final.ShowNormals = value; } }
		public bool ShowBump 
		{
			get 
			{ 
				return m_MeshColor.ShowBump; 
			} 
			set 
			{ 
				m_MeshColor.ShowBump = value;
				m_Final.ShowBump = value;
			} 
		}

		public bool ApplyTextured { get { return m_Final.ApplyTextured; } set { m_Final.ApplyTextured = value; } }
		public bool ApplyAmbient { get { return m_Final.ApplyAmbient; } set { m_Final.ApplyAmbient = value; } }
		public bool ApplySpecular { get { return m_Final.ApplySpecular; } set { m_Final.ApplySpecular = value; } }
		public bool ApplyGloss { get { return m_Final.ApplyGloss; } set { m_Final.ApplyGloss = value; } }
		public bool ApplyColorCast { get { return m_Final.ApplyColorCast; } set { m_Final.ApplyColorCast = value; } }

		public bool ApplyDynamicLights
		{
			get { return m_ApplyDynamicLighting; }
			set
			{
				m_ApplyDynamicLighting = value;

				Lighting.IsEnabled = m_ApplyDynamicLighting;
				m_Final.ApplyDynamicLights = m_ApplyDynamicLighting; 

				if (m_ApplyDynamicLighting == true)
				{
					m_Final.LightingTexture = Lighting.LightingMap;
				}
				else
				{
					m_Final.LightingTexture = null;
				}
			}
		}

		public bool ApplyShadows
		{
			get
			{
				return m_Final.ApplyShadows;
			}
			set
			{
				m_Final.ApplyShadows = value;

				ShadowPipeline[0].IsEnabled = value;
				ShadowPipeline[1].IsEnabled = value;
				ShadowPipeline[2].IsEnabled = value;
				m_ShadowFinal.IsEnabled = value;
			}
		}
	
		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

						


		#endregion

		public DeferredPipeline(int width, int height, int msaa, DeferredQuality quality)
		{
			Name = "Deferred Render";

			MSAA = msaa;
			Oversize = 1; 

			Width = width;
			Height = height;

			Viewport = new Viewport(0, 0, Width, Height);

			m_Quality = quality;
		}

		public DeferredPipeline(int width, int height, int msaa, int oversize, DeferredQuality quality)
		{
			Name = "Deferred Render";

			MSAA = msaa;
			Oversize = oversize;

			Width = width * Oversize;
			Height = height * Oversize;

			Viewport = new Viewport(0, 0, Width, Height);

			m_Quality = quality;
		}

		public void CreateDefault()
		{
			MeshesPipeline = new DeferredMeshesPipeline(Width, Height, Quality, MSAA);
			MeshesPipeline.Initiate(); 
			this.Add(MeshesPipeline);

			if (TerrainData != null)
			{
				DeferredTerrainPass terrain = new DeferredTerrainPass(TerrainData);

				Environment.Resources.Add(terrain.Terrain); // HACK !

				MeshesPipeline.Add(terrain);
			}

			//ColorPass m_CoordPass = new ColorPass();			
			m_MeshColor.Meshes = Meshes;
			m_MeshColor.BonedMeshes = BonedMeshes;
			MeshesPipeline.Add(m_MeshColor);

			DiffuseTexturedPass_Batched meshColor_Batched = new DiffuseTexturedPass_Batched();
			meshColor_Batched.Meshes = MeshBatches;
			MeshesPipeline.Add(meshColor_Batched);

			DeferredLinePass linePass = new DeferredLinePass();
			linePass.Buffers = LineBuffers;
			MeshesPipeline.Add(linePass);

			ShadowPipeline = new DeferredShadowPipeline[3]; 
			
			DirectLightingSettings m_LightSettings = new DirectLightingSettings();

			ShadowPipeline[0] = CreateShadowPipeline(Width, Height, Quality, MSAA, m_LightSettings, 0, 2048, 1024.0f, 0f); // 128.0f);
			ShadowPipeline[1] = CreateShadowPipeline(Width, Height, Quality, MSAA, m_LightSettings, 1, 4, 1024.0f, 1024.0f - 512f); // 256.0f); 
			ShadowPipeline[2] = CreateShadowPipeline(Width, Height, Quality, MSAA, m_LightSettings, 2, 4, 4096.0f, 4096.0f - 512f); 
			/* 
			ShadowPipeline[0] = CreateShadowPipeline(Width, Height, Quality, MSAA, m_LightSettings, 0, 2048, 512.0f, 0f); // 128.0f);
			ShadowPipeline[1] = CreateShadowPipeline(Width, Height, Quality, MSAA, m_LightSettings, 1, 1024, 1024.0f, 1024.0f - 512f); // 256.0f); 
			ShadowPipeline[2] = CreateShadowPipeline(Width, Height, Quality, MSAA, m_LightSettings, 2, 512, 4096.0f, 4096.0f - 512f); 
			*/ 
			ShadowFinalPipeline = new DeferredShadowFinalPipeline(Width, Height, Quality, MSAA);
			ShadowFinalPipeline.Initiate(); 
			this.Add(ShadowFinalPipeline);			

			m_ShadowFinal.DepthTexture = MeshesPipeline.DepthTexture;
			m_ShadowFinal.NormalTexture = MeshesPipeline.NormalTexture;
			m_ShadowFinal.ShadowTexture1 = ShadowPipeline[0].DepthTexture;
			m_ShadowFinal.ShadowTexture2 = ShadowPipeline[1].DepthTexture;
			m_ShadowFinal.ShadowTexture3 = ShadowPipeline[2].DepthTexture;

			m_ShadowFinal.LightingSettings = m_LightSettings; 
			ShadowFinalPipeline.Add(m_ShadowFinal);


			//Lighting = new DeferredLightingPipeline(Width, Height, MSAA);
			//Lighting.Initiate(); 
			//this.Add(Lighting);

			//LightingPass lights = new LightingPass();
			//lights.LightingMeshes = LightingMeshes;
			//lights.DepthTexture = MeshesPipeline.DepthTexture;
			//lights.NormalTexture = MeshesPipeline.NormalTexture;
			//Lighting.Add(lights);
			Lighting = new TitledLightingProcessor(Width, Height, 1);
			Lighting.Lights = PointLights; 

			Lighting.NormalMap = MeshesPipeline.NormalTexture;
			Lighting.MaterialMap = MeshesPipeline.MaterialTexture;
			//Lighting.ExternalDepth = MeshesPipeline.DepthTexture2; 
			Lighting.DepthMap = MeshesPipeline.DepthTexture;

			Lighting.Initiate();

			this.Add(Lighting);

			DeferredDecalPipeline decalPipeline = new DeferredDecalPipeline(Width, Height, Quality, MSAA);

			decalPipeline.DepthTexture = MeshesPipeline.DepthTexture;
			decalPipeline.ColorTexture = MeshesPipeline.ColorTexture;			

			decalPipeline.Initiate();

			DecalColorPass decalPass = new DecalColorPass();

			decalPass.Meshes = Decals;

			decalPipeline.Add(decalPass); 

			this.Add(decalPipeline);			

			m_Final.ColorTexture = MeshesPipeline.ColorTexture;
			m_Final.NormalTexture = MeshesPipeline.NormalTexture;
			m_Final.MaterialTexture = MeshesPipeline.MaterialTexture;
			m_Final.DepthTexture = MeshesPipeline.DepthTexture;
			m_Final.ShadowTexture = ShadowFinalPipeline.ShadowTexture;
			//m_Final.ShadowTexture = pipeline.ShadowTexture; 

			//m_Final.LightingTexture = 
			//m_Final.LambertMap = LambertMap;
			//m_Final.ReflectionsMap = ReflectionsMap;
			//m_Final.FogMap = FogMap;  
			m_Final.LightingTexture = Lighting.LightingMap;

			FXAA = new PostProcess_FXAA(Width, Height, Quality);

			FXAA.Initiate(); 

			FXAA.Add(m_Final); 

			this.Add(FXAA);			
		}

		private DeferredShadowPipeline CreateShadowPipeline(int Width, int Height, DeferredQuality Quality, int MSAA, DirectLightingSettings settings, int index, int textureSize, float size, float offset)
		{
			DeferredShadowPipeline pipeline = new DeferredShadowPipeline(Width, Height, Quality, MSAA, textureSize);
			pipeline.Initiate();
			this.Add(pipeline);

			DeferredShadowPass shadow = new DeferredShadowPass(settings, index, size, offset);
			shadow.Meshes = Meshes;
			shadow.DynamicHexGrid = DynamicHexGrid; 

			pipeline.Add(shadow);

			return pipeline; 
		}

		public override void Render(View3D view)
		{
			Viewport backup = GLState.Viewport;
			GLState.Viewport = Viewport;
			GLState.Apply(Width, Height); 

			base.Render(view);

			GLState.Viewport = backup;
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width * Oversize;
			Height = height * Oversize;

			Viewport = new Viewport(0, 0, Width, Height);

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(Width, Height, samples);
				}
			}
		}

		public void DownloadContents(IntPtr imagePtr, bool useLighting)
		{
			if (useLighting == false)
			{
				MeshesPipeline.ColorTexture.DownloadImage(imagePtr);
			}
			else
			{
				FXAA.ColorTexture.DownloadImage(imagePtr); 
			}
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				//LightingOpenCLContext.Initiate(); 

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.LoadResources(); 
					}
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.UnloadResources();
					}
				}


				//LightingOpenCLContext.Finalize(); 

				m_IsLoaded = false; 
			}
		}

		#endregion		
	}
}
