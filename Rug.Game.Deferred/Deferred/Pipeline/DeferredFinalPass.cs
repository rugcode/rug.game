﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredFinalPass : PipelineBase, IResource
	{
		private bool m_IsLoaded;
		//private DeferredFinalBox m_Box;
		private DeferredFinalBox_Mapped m_Box;

		#region Properties

		public Texture2D LightingTexture 
		{
			get { return m_Box.LightingTexture; }
			set { m_Box.LightingTexture = value; } 
		}

		public Texture2D ColorTexture
		{
			get { return m_Box.Texture; }
			set { m_Box.Texture = value; }
		}

		public Texture2D DepthTexture
		{
			get { return m_Box.DepthTexture; }
			set { m_Box.DepthTexture = value; }
		}

		public Texture2D NormalTexture
		{
			get { return m_Box.NormalTexture; }
			set { m_Box.NormalTexture = value; }
		}

		public Texture2D MaterialTexture
		{
			get { return m_Box.MaterialTexture; }
			set { m_Box.MaterialTexture = value; }
		}

		public Texture2D ShadowTexture
		{
			get { return m_Box.ShadowTexture; }
			set { m_Box.ShadowTexture = value; }
		}

		public Texture2D LambertMap
		{
			get { return m_Box.LambertMap; }
			set { m_Box.LambertMap = value; }
		}

		public Texture2D ColorCastTexture
		{
			get { return m_Box.ColorCastTexture; }
			set { m_Box.ColorCastTexture = value; }
		}

		public float ColorCastAmount
		{
			get { return m_Box.ColorCastAmount; }
			set { m_Box.ColorCastAmount = value; }
		}

		public float IndirectAmbiant
		{
			get { return m_Box.IndirectAmbiant; }
			set { m_Box.IndirectAmbiant = value; }
		}

		public TextureDDS ReflectionsMap
		{
			get { return m_Box.ReflectionsMap; }
			set { m_Box.ReflectionsMap = value; } 
		}

		public TextureDDS FogMap
		{
			get { return m_Box.FogMap; }
			set { m_Box.FogMap = value; } 
		}

		public bool ShowMaterials
		{
			get { return m_Box.ShowMaterials; }
			set { m_Box.ShowMaterials = value; }
		}

		public bool ShowNormals
		{
			get { return m_Box.ShowNormals; }
			set { m_Box.ShowNormals = value; }
		}

		public bool ShowBump
		{
			get { return m_Box.ShowBump; }
			set { m_Box.ShowBump = value; }
		}

		public bool ApplyTextured
		{
			get { return m_Box.ApplyTextured; }
			set { m_Box.ApplyTextured = value; }
		}

		public bool ApplyAmbient
		{
			get { return m_Box.ApplyAmbient; }
			set { m_Box.ApplyAmbient = value; }
		}

		public bool ApplySpecular
		{
			get { return m_Box.ApplySpecular; }
			set { m_Box.ApplySpecular = value; }
		}

		public bool ApplyGloss
		{
			get { return m_Box.ApplyGloss; }
			set { m_Box.ApplyGloss = value; }
		}

		public bool ApplyColorCast
		{
			get { return m_Box.ApplyColorCast; }
			set { m_Box.ApplyColorCast = value; }
		}

		public bool ApplyDynamicLights
		{
			get { return m_Box.ApplyDynamicLights; }
			set { m_Box.ApplyDynamicLights = value; }
		}

		public bool ApplyShadows
		{
			get { return m_Box.ApplyShadows; }
			set { m_Box.ApplyShadows = value; }
		}

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}
		
		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}
		
		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

				

		#endregion

		public DeferredFinalPass()
		{
			Name = "Final Pass"; 

			//m_Box = new DeferredFinalBox();
			m_Box = new DeferredFinalBox_Mapped(); 
		}

        public override void Update(View3D view)
		{
			
		}

		public override void Render(View3D view)
		{
			//GLState.EnableBlend = true; // (EnableCap.Blend);
			//GLState.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
			//GLState.CullFace(CullFaceMode.Front);
			//GLState.Enable(EnableCap.CullFace);

			GLState.EnableBlend = false; 
			GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GLState.CullFace(CullFaceMode.Back);
			GLState.EnableCullFace = false;
			GLState.Apply(view); 
			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			

			m_Box.Render(view); 
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false) 
			{
				m_Box.LoadResources();

				m_IsLoaded = true;
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_Box.UnloadResources();

				m_IsLoaded = false; 
			}
		}
	}
}
