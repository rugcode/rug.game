﻿using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredShadowFinalPipeline : PipelineManager, IResource, IResizablePipeline
	{
		private FrameBuffer m_FrameBuffer;
		private DeferredQuality m_Quality;
		private bool m_IsLoaded = false;

		#region Properties

		public int Width { get; set; }
		public int Height { get; set; }

		public FrameBufferTexture2D ShadowTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0Ext]; } }

		//public FrameBufferTexture2D DepthTexture { get { return m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt]; } }

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public new ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public int MSAA { get; set; }
		public Viewport Viewport { get; set; }

		
		private int m_Detail;
		private OpenTK.Vector3 m_LightDir;
		private OpenTK.Matrix4 m_ViewMatrix;

		#endregion

		public DeferredShadowFinalPipeline(int width, int height, DeferredQuality quality, int MSAA)
		{
			Name = "Deferred Shadow Final";

			m_Quality = quality;
			m_Detail = 2;
			Width = width / m_Detail;
			Height = height / m_Detail;
			
			this.MSAA = MSAA;
			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);
		}

		public void Initiate()
		{
			PixelInternalFormat internalFormat;
			PixelType pixelType;
			PixelFormat pixelFormat; 

			/* 
			internalFormat = PixelInternalFormat.R32f;
			pixelType = PixelType.Float;
			pixelFormat = PixelFormat.Red; 
			*/

			internalFormat = PixelInternalFormat.Rgba8;
			pixelType = PixelType.Byte;
			pixelFormat = PixelFormat.Rgba; 


			//internalFormat = PixelInternalFormat.R8;
			//pixelType = PixelType.UnsignedByte;

			/* 
			switch (m_Quality)
			{
				case DeferredQuality.Low:
					internalFormat = PixelInternalFormat.R8;
					pixelType = PixelType.UnsignedByte;
					break;
				case DeferredQuality.Medium:
					internalFormat = PixelInternalFormat.R16f;
					pixelType = PixelType.HalfFloat;
					break;
				default:
					internalFormat = PixelInternalFormat.R32f;					
					pixelType = PixelType.Float;
					break;
			}
			*/ 

			m_FrameBuffer = new FrameBuffer("Shadow Final Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Shadow", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = internalFormat,
						PixelFormat = pixelFormat,
						PixelType = pixelType,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}
				/* 
				,new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
				{
					Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
					InternalFormat = (PixelInternalFormat)All.DepthComponent24,
					PixelFormat = PixelFormat.DepthComponent,
					PixelType = PixelType.UnsignedInt,
					MagFilter = TextureMagFilter.Nearest,
					MinFilter = TextureMinFilter.Nearest,
					WrapS = TextureWrapMode.ClampToBorder,
					WrapT = TextureWrapMode.ClampToBorder,
				}*/
					));		
		}

		public override void Render(View3D view)
		{
			if (m_LightDir == view.LightDirection &&
				m_ViewMatrix == view.View)
			{
				//return;
			}

			m_LightDir = view.LightDirection;
			m_ViewMatrix = view.View; 


			Viewport backup = GLState.Viewport;
			GLState.Viewport = Viewport;
			//GLState.Apply(view); 
			
			m_FrameBuffer.Bind();

			//GLState.ClearColor(new Color4(0f, 0f, 0f, 1f)); 

			GLState.EnableDepthMask = false;
			GLState.EnableDepthTest = false; 
			//GLState.ClearColor(Color4.DarkOrange);
			GLState.ClearColor(Color4.Black);
			GLState.ClearDepth(1.0f);

			GLState.EnableBlend = false;
			GLState.EnableDepthTest = false;
			GLState.EnableCullFace = true;
			GLState.EnableAlphaTest = false; 
			GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = false;
			
			GLState.Apply(Width * MSAA, Height * MSAA);

			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit | ClearBufferMask.AccumBufferBit);
			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			//GL.Clear(ClearBufferMask.DepthBufferBit);
			//GL.ClearBuffer(ClearBuffer.Color, 0, new float[] { 0f });

			base.Render(view);

			m_FrameBuffer.Unbind();

			GLState.EnableAlphaTest = false;

			GLState.Viewport = backup;			
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width / m_Detail;
			Height = height / m_Detail;

			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(Width * MSAA, Height * MSAA, MultiSamples.X1);
				}
			}
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

				System.Drawing.Size size = new System.Drawing.Size(Width * MSAA, Height * MSAA);

				m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = size;
				//m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt].ResourceInfo.Size = size;

				m_FrameBuffer.LoadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.LoadResources();
					}
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_FrameBuffer.UnloadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.UnloadResources();
					}
				}

				m_IsLoaded = false; 
			}
		}

		#endregion
	}
}
