﻿
namespace Rug.Game.Deferred.Pipeline
{
	/* 
	public class DeferredParticlePass: PipelineBase, IResource
	{
		protected Deferred_Terrain Effect;

		private bool m_Disposed; 

		#region Properties

		public LodQuadTree Terrain { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_Disposed; }
		}

		


		#endregion

		public DeferredParticlePass()
		{
			Name = "Deferred Particle Pass"; 

			if (Effect == null)
			{
				Effect = SharedEffects.Effect["Deferred_Terrain"] as Deferred_Terrain;
			}

			if (EffectSimple == null)
			{
				EffectSimple = SharedEffects.Effect["Deferred_TerrainSimple"] as Deferred_TerrainSimple;
			}

			Terrain = new LodQuadTree(new TestTerrainPatchDataProvider(7), 256); // 128); 
		}

		public override void Update()
		{
		
		}

		public override void Render(View3D view)
		{
			GLState.EnableDepthMask = true; 
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = false; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = true; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = true; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true; // Enable(EnableCap.DepthTest);
			GLState.FrontFace(FrontFaceDirection.Cw); 
			GLState.Apply(view);			

			Terrain.Render(view, Effect, EffectSimple);
		}		

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			Terrain.LoadResources();

			if (m_Disposed == false)
			{


				m_Disposed = true;				
			}
		}

		public void UnloadResources()
		{
			Terrain.UnloadResources();

			if (m_Disposed == true)
			{


				m_Disposed = false;
			}			
		}
	}
	 * */ 
}
