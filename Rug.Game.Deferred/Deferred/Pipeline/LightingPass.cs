﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Data;
using Rug.Game.Shaders.Deferred;
using System;

namespace Rug.Game.Deferred.Pipeline
{
	public class LightingPass : PipelineBase
	{
		protected Deferred_LightingMesh Effect; 

		public LightingMeshCollection LightingMeshes { get; set; }

		public Texture2D DepthTexture { get; set; }

		public Texture2D NormalTexture { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public LightingPass()
		{
			Name = "Lighting Mesh Pass";

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_LightingMesh"] as Deferred_LightingMesh;
			}
		}

        public override void Update(View3D view)
		{
			
		}


		public override void Render(View3D view)
		{
			/* 
			GLState.EnableDepthMask = true;
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = false; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = true; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = true; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true; // Enable(EnableCap.DepthTest);
			*/

			GLState.EnableBlend = true;
			GLState.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
			GLState.CullFace(CullFaceMode.Front);
			GLState.FrontFace(FrontFaceDirection.Cw); 
			GLState.EnableCullFace = true; 
			GLState.Apply(view);

			Effect.Begin(ref view.World, ref view.Projection, ref view.ProjectionInverse, ref view.NormalWorld, NormalTexture, DepthTexture);

			foreach (LightingMesh mesh in LightingMeshes)
			{
				RenderMesh(mesh);
			}

			Effect.End();
		}

		protected virtual void RenderMesh(LightingMesh mesh)
		{
			mesh.Bind();

			Effect.Render(ref mesh.ObjectMatrix, mesh.Center, mesh.LightSize, mesh.IndexCount, mesh.IndexType);

			//GL.UniformMatrix4(uObjectMatrix, false, ref mesh.objectMatrix);
			//GL.UniformMatrix4(uNormalMatrix, false, ref mesh.normalMatrix);
			//GL.DrawElements(BeginMode.Triangles, mesh.indexCount, mesh.DrawElementsType, 0);

			mesh.Unbind();

			/* 
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.vertexBuffer);

			GL.VertexAttribPointer((int)ATTRIB.ATTRIB_VERTEX, 3, VertexAttribPointerType.Float, false, mesh.MeshData.VertexStride, 0);
			GL.VertexAttribPointer((int)ATTRIB.ATTRIB_NORMAL, 3, VertexAttribPointerType.Float, false, mesh.MeshData.VertexStride, Marshal.SizeOf(typeof(Vector3)));
			GL.VertexAttribPointer((int)ATTRIB.ATTRIB_COLOR, 3, VertexAttribPointerType.Float, false, mesh.MeshData.VertexStride, Marshal.SizeOf(typeof(Vector3)) * 2);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.indexBuffer);

			GL.UniformMatrix4(uObjectMatrix, false, ref mesh.objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref mesh.normalMatrix);

			GL.DrawElements(BeginMode.Triangles, mesh.indexCount, mesh.DrawElementsType, 0);
			*/
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}
	}
}
