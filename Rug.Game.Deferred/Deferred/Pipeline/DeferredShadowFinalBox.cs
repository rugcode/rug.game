﻿using OpenTK.Graphics;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using Rug.Game.Shaders.Deferred;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredShadowFinalBox : TextureBox
	{
		protected new Deferred_Shadow_Final Effect;

		#region Properties
		
		public Texture2D NormalTexture { get; set; }
		public Texture2D DepthTexture { get; set; }

		public Texture2D ShadowTexture1 { get; set; }
		public Texture2D ShadowTexture2 { get; set; }
		public Texture2D ShadowTexture3 { get; set; }

		public Texture2D NoiseTexture { get; set; }

		public DirectLightingSettings LightingSettings { get; set; }

		#endregion

		public DeferredShadowFinalBox()
		{

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_Shadow_Final"] as Deferred_Shadow_Final;
			}

			FlipVertical = true;
			this.Depth = 0.5f;
			this.Color = new Color4(1f, 1f, 1f, 1f); 
		}

		public override void Render(View3D view)
		{
			//GLState.EnableDepthTest = true;
			//GLState.Apply(view); 

			CheckAndWriteRectangle();

			Effect.Render(view, NormalTexture, DepthTexture, ShadowTexture1, ShadowTexture2, ShadowTexture3, LightingSettings.LightMatrix,
						LightingSettings.DepthStarts,
						LightingSettings.DepthEnds, 
						NoiseTexture, Vertices); 
		}
	}
}
