﻿using OpenTK.Graphics;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using Rug.Game.Shaders.Deferred;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredFinalBox : TextureBox
	{
		protected new Deferred_Final Effect;

		#region Properties
		
		public Texture2D NormalTexture { get; set; }
		public Texture2D DepthTexture { get; set; }		
		public Texture2D LightingTexture { get; set; }
		public Texture2D LambertMap { get; set; }

		#endregion

		public DeferredFinalBox()
		{
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_Final"] as Deferred_Final;
			}

			this.Depth = 0.5f;
			this.Color = new Color4(1f, 1f, 1f, 1f); 
		}

		public override void Render(View3D view)
		{
			CheckAndWriteRectangle();

			Effect.Render(view, Texture, NormalTexture, DepthTexture, LightingTexture, LambertMap, Vertices); 
		}
	}
}
