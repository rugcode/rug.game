﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.MeshData;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Shaders.Deferred;
using System;

namespace Rug.Game.Deferred.Pipeline
{
	public class DecalColorPass : PipelineBase, IResource
	{
		protected Deferred_Decal_Color Effect; 
		private bool m_IsLoaded; 

		#region Properties

		public MeshCollection Meshes { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		

		#endregion

		public DecalColorPass()
		{
			Name = "Decal Color Pass"; 

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_Decal_Color"] as Deferred_Decal_Color;
			}
		}

        public override void Update(View3D view)
		{
		
		}

		public override void Render(View3D view)
		{
			GLState.EnableDepthMask = false; 
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = true; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = true; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = true; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true; // Enable(EnableCap.DepthTest);
			GLState.FrontFace(FrontFaceDirection.Cw); 
			GLState.Apply(view);

			/* 
			GLState.EnableDepthMask = true;
			GLState.EnableDepthTest = false;
			GLState.EnableBlend = true; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = false; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = false; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = false; // Enable(EnableCap.DepthTest);
			GLState.FrontFace(FrontFaceDirection.Ccw);
			GLState.Apply(view);
			*/ 

			Effect.Begin(ref view.World, ref view.Projection, ref view.NormalWorld);

			foreach (Mesh mesh in Meshes)
			{
				if (mesh.IsVisible == true)
				{
					RenderMesh(mesh);
				}
			}

			Effect.End(); 		
		}

		protected virtual void RenderMesh(Mesh mesh) 
		{
			mesh.Bind();

			Effect.Render(ref mesh.ObjectMatrix, ref mesh.NormalMatrix, mesh.IndexCount, mesh.IndexType, mesh.DiffuseTexture);

			//GL.UniformMatrix4(uObjectMatrix, false, ref mesh.objectMatrix);
			//GL.UniformMatrix4(uNormalMatrix, false, ref mesh.normalMatrix);
			//GL.DrawElements(BeginMode.Triangles, mesh.indexCount, mesh.DrawElementsType, 0);

			mesh.Unbind();

			/* 
			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.vertexBuffer);

			GL.VertexAttribPointer((int)ATTRIB.ATTRIB_VERTEX, 3, VertexAttribPointerType.Float, false, mesh.MeshData.VertexStride, 0);
			GL.VertexAttribPointer((int)ATTRIB.ATTRIB_NORMAL, 3, VertexAttribPointerType.Float, false, mesh.MeshData.VertexStride, Marshal.SizeOf(typeof(Vector3)));
			GL.VertexAttribPointer((int)ATTRIB.ATTRIB_COLOR, 3, VertexAttribPointerType.Float, false, mesh.MeshData.VertexStride, Marshal.SizeOf(typeof(Vector3)) * 2);

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.indexBuffer);

			GL.UniformMatrix4(uObjectMatrix, false, ref mesh.objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref mesh.normalMatrix);

			GL.DrawElements(BeginMode.Triangles, mesh.indexCount, mesh.DrawElementsType, 0);
			*/ 
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_IsLoaded = false;
			}			
		}
	}
}

