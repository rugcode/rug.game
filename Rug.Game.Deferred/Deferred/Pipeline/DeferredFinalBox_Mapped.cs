﻿using OpenTK.Graphics;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using Rug.Game.Shaders.Deferred;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredFinalBox_Mapped : TextureBox
	{
		protected new Deferred_Final_Mapped[] Effect;

		#region Properties
		
		public Texture2D NormalTexture { get; set; }
		public Texture2D MaterialTexture { get; set; }
		public Texture2D ShadowTexture { get; set; }
		public Texture2D DepthTexture { get; set; }		
		public Texture2D LightingTexture { get; set; }
		public Texture2D LambertMap { get; set; }
		public Texture2D ColorCastTexture { get; set; }

		public float ColorCastAmount { get; set; }
		public float IndirectAmbiant { get; set; } 

		public TextureDDS ReflectionsMap { get; set; }
		public TextureDDS FogMap { get; set; }

		public bool ShowMaterials { get; set; }
		public bool ShowNormals { get; set; }
		public bool ShowBump { get; set; }

		public bool ApplyTextured { get; set; }
		public bool ApplyAmbient { get; set; }
		public bool ApplySpecular { get; set; }
		public bool ApplyGloss { get; set; }
		public bool ApplyColorCast { get; set; }
		public bool ApplyDynamicLights { get; set; }
		public bool ApplyShadows { get; set; } 

		#endregion

		public DeferredFinalBox_Mapped()
		{
			ShowMaterials = false;
			ShowNormals = false;
			ShowBump = false; 

			ApplyTextured = true; 
			ApplyAmbient = true;
			ApplySpecular = true;
			ApplyGloss = true;
			ApplyColorCast = true;
			ApplyDynamicLights = true; 

			if (Effect == null)
			{
				Effect = new Deferred_Final_Mapped[(int)FinalType.Normal + 1];

				for (int i = 0; i < Effect.Length; i++)
				{
					Effect[i] = SharedEffects.Effects["Deferred_Final_Mapped_" + i] as Deferred_Final_Mapped;
				}
			}

			FlipVertical = true;
			this.Depth = 0.5f;
			this.Color = new Color4(1f, 1f, 1f, 1f); 
		}

		public override void Render(View3D view)
		{
			CheckAndWriteRectangle();
			
			FinalType type;

			if (ShowMaterials == true || ShowBump == true) 
			{
				type = FinalType.Material;
			}
			else if (ShowNormals == true) 
			{
				type = FinalType.Normal;
			}
			else 
			{
				type = (ApplyTextured ? FinalType.Textured : FinalType.None) |
							 (ApplyAmbient ? FinalType.Ambient : FinalType.None) |
							 (ApplySpecular ? FinalType.Specular : FinalType.None) |
							 (ApplyGloss ? FinalType.Gloss : FinalType.None) |
							 (ApplyDynamicLights ? FinalType.DynamicLights : FinalType.None) |
							 (ApplyColorCast ? FinalType.ColorCast : FinalType.None) |
							 (ApplyShadows ? FinalType.Shadows : FinalType.None); 
			}

			Effect[(int)type].Render(view, Texture, NormalTexture, MaterialTexture, DepthTexture, LightingTexture, ShadowTexture,
									LambertMap, ColorCastTexture, ColorCastAmount, IndirectAmbiant, ReflectionsMap, FogMap, 
									Vertices); 
		}
	}
}
