﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredShadowFinalPass : PipelineBase, IResource
	{
		private bool m_IsLoaded;
		private DeferredShadowFinalBox m_Box;

		#region Properties

		public Texture2D DepthTexture
		{
			get { return m_Box.DepthTexture; }
			set { m_Box.DepthTexture = value; }
		}

		public Texture2D NormalTexture
		{
			get { return m_Box.NormalTexture; }
			set { m_Box.NormalTexture = value; }
		}

		public Texture2D ShadowTexture1 
		{
			get { return m_Box.ShadowTexture1; }
			set { m_Box.ShadowTexture1 = value; } 
		}

		public Texture2D ShadowTexture2
		{
			get { return m_Box.ShadowTexture2; }
			set { m_Box.ShadowTexture2 = value; }
		}

		public Texture2D ShadowTexture3
		{
			get { return m_Box.ShadowTexture3; }
			set { m_Box.ShadowTexture3 = value; }
		}

		public Texture2D NoiseTexture
		{
			get { return m_Box.NoiseTexture; }
			set { m_Box.NoiseTexture = value; } 
		}

		public DirectLightingSettings LightingSettings
		{
			get { return m_Box.LightingSettings; }
			set { m_Box.LightingSettings = value; }
		} 

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}
		
		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}
		
		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

				

		#endregion

		public DeferredShadowFinalPass()
		{
			Name = "Shadow Final Pass"; 

			//m_Box = new DeferredFinalBox();
			m_Box = new DeferredShadowFinalBox(); 
		}

        public override void Update(View3D view)
		{
			
		}

		public override void Render(View3D view)
		{
			//GLState.EnableBlend = true; // (EnableCap.Blend);
			//GLState.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
			//GLState.CullFace(CullFaceMode.Front);
			//GLState.Enable(EnableCap.CullFace);

			GLState.EnableBlend = false; 
			GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GLState.CullFace(CullFaceMode.Back);
			GLState.EnableCullFace = false;
			GLState.Apply(view); 
			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.Clear(ClearBufferMask.DepthBufferBit);

			m_Box.Render(view); 
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false) 
			{
				m_Box.LoadResources();

				m_IsLoaded = true;
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_Box.UnloadResources();

				m_IsLoaded = false; 
			}
		}
	}
}
