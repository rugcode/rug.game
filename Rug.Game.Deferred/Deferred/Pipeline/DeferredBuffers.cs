﻿
namespace Rug.Game.Deferred.Pipeline
{
	/* 
	public class DeferredBuffers :
	{
		int m_Width;
		int m_Height;

		//public uint ColorTexture;
		//public uint DepthTexture;
		//public uint NormalTexture;
		//uint FBOHandle;

		private FrameBuffer m_FrameBuffer;

		/* 
		DrawBuffersEnum[] buffers = {
				DrawBuffersEnum.ColorAttachment0,
				DrawBuffersEnum.ColorAttachment1
			};
		* / 


		public DeferredBuffers(int width, int height)
		{
			m_Width = width;
			m_Height = height;

			m_FrameBuffer = new FrameBuffer("Frame Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(m_Width, m_Height),
						InternalFormat = PixelInternalFormat.Rgba16f,
						PixelFormal = PixelFormat.Rgba,
						PixelType = PixelType.Float,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Normal", FramebufferAttachment.ColorAttachment1Ext)
					{
						Size = new System.Drawing.Size(m_Width, m_Height),
						InternalFormat = PixelInternalFormat.Rgba16f,
						PixelFormal = PixelFormat.Rgba,
						PixelType = PixelType.Float,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(m_Width, m_Height),
						InternalFormat = (PixelInternalFormat)All.DepthComponent24,
						PixelFormal = PixelFormat.DepthComponent,
						PixelType = PixelType.UnsignedInt,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}));
		}

		/* 
			m_FrameBuffer = new FrameBuffer("Frame Buffer", ResourceMode.Static, 
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(WindowWidth * AppContext.Count, WindowHeight), 
						InternalFormat = PixelInternalFormat.Rgba16f, 
						PixelFormal = PixelFormat.Rgba, 
						PixelType = PixelType.Float, 
						MagFilter = TextureMagFilter.Linear, 
						MinFilter = TextureMinFilter.Linear, 
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,	
					},
					new FrameBufferTexture2DInfo("Normal", FramebufferAttachment.ColorAttachment1Ext)
					{
						Size = new System.Drawing.Size(WindowWidth * AppContext.Count, WindowHeight), 
						InternalFormat = PixelInternalFormat.Rgba16f, 
						PixelFormal = PixelFormat.Rgba, 
						PixelType = PixelType.Float, 
						MagFilter = TextureMagFilter.Linear, 
						MinFilter = TextureMinFilter.Linear, 
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,	
					},
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(WindowWidth * AppContext.Count, WindowHeight), 
						InternalFormat = (PixelInternalFormat)All.DepthComponent24, 
						PixelFormal = PixelFormat.DepthComponent, 
						PixelType = PixelType.UnsignedInt, 
						MagFilter = TextureMagFilter.Linear, 
						MinFilter = TextureMinFilter.Linear, 
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,	
					}));
		 * /

		public void Resize(int width, int height)
		{
			this.m_Width = width;
			this.m_Height = height;

			m_FrameBuffer = new FrameBuffer("Frame Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(WindowWidth * AppContext.Count, WindowHeight),
						InternalFormat = PixelInternalFormat.Rgba16f,
						PixelFormal = PixelFormat.Rgba,
						PixelType = PixelType.Float,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Normal", FramebufferAttachment.ColorAttachment1Ext)
					{
						Size = new System.Drawing.Size(WindowWidth * AppContext.Count, WindowHeight),
						InternalFormat = PixelInternalFormat.Rgba16f,
						PixelFormal = PixelFormat.Rgba,
						PixelType = PixelType.Float,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(WindowWidth * AppContext.Count, WindowHeight),
						InternalFormat = (PixelInternalFormat)All.DepthComponent24,
						PixelFormal = PixelFormat.DepthComponent,
						PixelType = PixelType.UnsignedInt,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}));
		}

		public void Begin()
		{

			GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBOHandle);
			GL.DrawBuffers(2, buffers);

			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			//GL.Viewport(0, 0, width, height);
		}

		public void End()
		{
			//GL.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
			GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
		}		 
	}
	*/
}
