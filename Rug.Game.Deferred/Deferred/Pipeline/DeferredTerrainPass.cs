﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Deferred.Data.Terrain;
using Rug.Game.Shaders.Deferred;
using System;

namespace Rug.Game.Deferred.Pipeline
{
	public class DeferredTerrainPass : PipelineBase, IResource
	{
		protected Deferred_Terrain Effect;
		protected Deferred_TerrainSimple EffectSimple;

		private bool m_IsLoaded; 

		#region Properties

		public LodQuadTree Terrain { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		


		#endregion

		public DeferredTerrainPass(ITerrainPatchDataProvider terrainProvider)
		{
			Name = "Terrain Pass"; 

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Deferred_Terrain"] as Deferred_Terrain;
			}

			if (EffectSimple == null)
			{
				EffectSimple = SharedEffects.Effects["Deferred_TerrainSimple"] as Deferred_TerrainSimple;
			}

			//Terrain = new LodQuadTree(new TestTerrainPatchDataProvider(7), 256); // 128); 
			Terrain = new LodQuadTree(terrainProvider, 256); // 128); 
		}

        public override void Update(View3D view)
		{
		
		}

		public override void Render(View3D view)
		{
			GLState.EnableDepthMask = true; 
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = false; // Disable(EnableCap.Blend);
			GLState.EnableDepthTest = true; //  Enable(EnableCap.DepthTest);
			GLState.EnableCullFace = true; // Enable(EnableCap.CullFace);
			//GLState.EnableAlphaTest = true; // Enable(EnableCap.AlphaTest);
			//GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true; // Enable(EnableCap.DepthTest);
			GLState.FrontFace(FrontFaceDirection.Cw); 
			GLState.Apply(view);			

			Terrain.Render(view, Effect, EffectSimple);
		}		

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			Terrain.LoadResources();

			if (m_IsLoaded == false)
			{


				m_IsLoaded = true;				
			}
		}

		public void UnloadResources()
		{
			Terrain.UnloadResources();

			if (m_IsLoaded == true)
			{


				m_IsLoaded = false;
			}			
		}
	}
}