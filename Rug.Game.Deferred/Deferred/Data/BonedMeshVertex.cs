﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Deferred.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct BonedMeshVertex
	{		
		public Vector3 Position;
		public Vector3 Normal;
		public Vector4 Tangent;
		public Vector2 TextureCoords;
		public Color4 Color;
		public Vector4 Material;

		public Vector4 Indices;
		public Vector4 Weights;

		public enum Elements : int { Position = 0, Normal = 1, Tangent = 2, TextureCoords = 3, Color = 4, Material = 5, Indices = 6, Weights = 7, };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int NormalOffset;
		public static readonly int TangentOffset;
		public static readonly int TextureCoordsOffset;
		public static readonly int ColorOffset;
		public static readonly int MaterialOffset;

		public static readonly int IndicesOffset;
		public static readonly int WeightsOffset;
		

		static BonedMeshVertex()
		{
			Stride = BlittableValueType<BonedMeshVertex>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Position");
			NormalOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Normal");
			TangentOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Tangent");
			TextureCoordsOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "TextureCoords");
			ColorOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Color");
			MaterialOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Material");

			IndicesOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Indices");
			WeightsOffset = (int)Marshal.OffsetOf(typeof(BonedMeshVertex), "Weights");

			if (WeightsOffset + BlittableValueType<Vector4>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.Normal, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
			GL.VertexAttribPointer((int)Elements.Tangent, 4, VertexAttribPointerType.Float, false, Stride, TangentOffset);
			GL.VertexAttribPointer((int)Elements.TextureCoords, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
			GL.VertexAttribPointer((int)Elements.Material, 4, VertexAttribPointerType.Float, false, Stride, MaterialOffset);

			GL.VertexAttribPointer((int)Elements.Indices, 4, VertexAttribPointerType.Float, false, Stride, IndicesOffset);
			GL.VertexAttribPointer((int)Elements.Weights, 4, VertexAttribPointerType.Float, false, Stride, WeightsOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.Normal);
			GL.EnableVertexAttribArray((int)Elements.Tangent);
			GL.EnableVertexAttribArray((int)Elements.TextureCoords);
			GL.EnableVertexAttribArray((int)Elements.Color);
			GL.EnableVertexAttribArray((int)Elements.Material);

			GL.EnableVertexAttribArray((int)Elements.Indices);
			GL.EnableVertexAttribArray((int)Elements.Weights);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.Normal);
			GL.DisableVertexAttribArray((int)Elements.Tangent);
			GL.DisableVertexAttribArray((int)Elements.TextureCoords);
			GL.DisableVertexAttribArray((int)Elements.Color);
			GL.DisableVertexAttribArray((int)Elements.Material);
			
			GL.DisableVertexAttribArray((int)Elements.Indices);
			GL.DisableVertexAttribArray((int)Elements.Weights);
		}


		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return BonedMeshVertex.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
				GL.VertexAttribPointer((int)Elements.Tangent + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, TangentOffset);
				GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.Material + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, MaterialOffset);

				GL.VertexAttribPointer((int)Elements.Indices + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, IndicesOffset);
				GL.VertexAttribPointer((int)Elements.Weights + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, WeightsOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Tangent + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Material + baseLocation);

				GL.EnableVertexAttribArray((int)Elements.Indices + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Weights + baseLocation);

				baseLocation += 8;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
				GL.VertexAttribPointer((int)Elements.Tangent + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, TangentOffset);
				GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.Material + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, MaterialOffset);

				GL.VertexAttribPointer((int)Elements.Indices + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, IndicesOffset);
				GL.VertexAttribPointer((int)Elements.Weights + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, WeightsOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Tangent + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Material + baseLocation);

				GL.EnableVertexAttribArray((int)Elements.Indices + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Weights + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Normal + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Tangent + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.TextureCoords + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Material + baseLocation, devisor);

				GL.Arb.VertexAttribDivisor((int)Elements.Indices + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Weights + baseLocation, devisor);

				baseLocation += 8;
			}

			#endregion
		}

		#endregion
	}
}
