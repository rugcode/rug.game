﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Deferred.Data
{
	public class LightingMesh : IResource
	{
		private bool m_IsLoaded = false;
		
		private string m_Name; 

		private VertexBuffer m_Vertices; 
		private IndexBuffer m_Indices; 

		private LightingMeshVertex[] m_VertsData;
		private ushort[] m_IndicesData; 

		private Vector3 m_Center = Vector3.Zero;

		#region IResource Members

		public Matrix4 ObjectMatrix = Matrix4.Identity;
		public Matrix4 NormalMatrix = Matrix4.Identity;

		public Vector3 Center
		{
			get { return m_Center; } // Vector3.Zero; } // m_Center; }
			set
			{
				m_Center = value;

				ObjectMatrix = Matrix4.CreateTranslation(m_Center);

				CalculateNormalMatrix();   
			}
		}

		public float LightSize = 3f; 

		public int IndexCount 
		{ 
			get { return m_Indices.ResourceInfo.Count; } 
		} 

		public DrawElementsType IndexType 
		{ 
			get { return m_Indices.IndexType; } 
		} 

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		

		#endregion

		public LightingMesh(string name, LightingMeshVertex[] verts, ushort[] indices)
		{
			m_Name = name;

			m_VertsData = verts; 
			m_IndicesData = indices;

			m_Vertices = new VertexBuffer(m_Name + " Vertices", ResourceMode.Static, new VertexBufferInfo(LightingMeshVertex.Format, verts.Length, BufferUsageHint.StaticDraw));
			m_Indices = new IndexBuffer(m_Name + " Indices", ResourceMode.Static, new IndexBufferInfo(DrawElementsType.UnsignedShort, sizeof(ushort), indices.Length, BufferUsageHint.StaticDraw)); 
		}

		public void CalculateNormalMatrix()
		{
			Quaternion q = GLHelper.CreateFromMatrix(ref ObjectMatrix);

			q = Quaternion.Invert(q);

			NormalMatrix = GLHelper.CreateFromQuaternion(ref q);
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				m_Vertices.LoadResources();

				DataStream stream;
				m_Vertices.MapBuffer(BufferAccess.WriteOnly, out stream);
				stream.WriteRange(m_VertsData); 
				m_Vertices.UnmapBuffer(); 

				m_Indices.LoadResources();

				m_Indices.MapBuffer(BufferAccess.WriteOnly, out stream);
				stream.WriteRange(m_IndicesData);
				m_Indices.UnmapBuffer(); 

				m_IsLoaded = true;
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_Vertices.UnloadResources();
				m_Indices.UnloadResources(); 

				m_IsLoaded = false;
			}	
		}

		public void Bind()
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Vertices.ResourceHandle);
			LightingMeshVertex.Bind();

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_Indices.ResourceHandle);
		}

		public void Unbind()
		{
			// Opto Remove
			//GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			LightingMeshVertex.Unbind();

			// Opto Remove
			//GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
		}

		public static LightingMesh Sphere(string name, float _radius, int _segmentsW, int _segmentsH, Color3 color) 
		{
			float r = color.R;
			float g = color.G;
			float b = color.B;

			int numVertices = (_segmentsH + 1) * (_segmentsW + 1);
			LightingMeshVertex[] vertices = new LightingMeshVertex[numVertices];

			int numIndices = (_segmentsH - 1) * _segmentsW * 6;
			ushort[] indices = new ushort[numIndices];
			int triIndex = 0;
			int vertcount = 0;

			ushort v1, v2, v3, v4;

			for (int j = 0; j <= _segmentsH; ++j)
			{
				float horangle = 3.1415f * j / _segmentsH;
				float z = -_radius * (float)Math.Cos(horangle);
				float ringradius = _radius * (float)Math.Sin(horangle);

				for (int i = 0; i <= _segmentsW; ++i)
				{
					float verangle = 2f * 3.1415f * i / _segmentsW;
					float x = ringradius * (float)Math.Cos(verangle);
					float y = ringradius * (float)Math.Sin(verangle);
					float normLen = 1f / (float)Math.Sqrt(x * x + y * y + z * z);

					vertices[vertcount].Position = new Vector3(x, y, z);
					vertices[vertcount].Normal = new Vector3(x, y, z) * normLen;
					vertices[vertcount].Color = new Color3(r, g, b);

					vertcount++; 

					if (i > 0 && j > 0)
					{
						v1 = (ushort)((_segmentsW + 1) * j + i);
						v2 = (ushort)((_segmentsW + 1) * j + i - 1);
						v3 = (ushort)((_segmentsW + 1) * (j - 1) + i - 1);
						v4 = (ushort)((_segmentsW + 1) * (j - 1) + i);

						if (j == _segmentsH)
						{
							indices[triIndex++] = v1;
							indices[triIndex++] = v3;
							indices[triIndex++] = v4;
						}
						else if (j == 1)
						{
							indices[triIndex++] = v1;
							indices[triIndex++] = v2;
							indices[triIndex++] = v3;
						}
						else
						{
							indices[triIndex++] = v1;
							indices[triIndex++] = v2;
							indices[triIndex++] = v3;
							indices[triIndex++] = v1;
							indices[triIndex++] = v3;
							indices[triIndex++] = v4;
						}
					}
				}
			}

			return new LightingMesh(name, vertices, indices); 
		}
	}
}
