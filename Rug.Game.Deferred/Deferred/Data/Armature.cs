﻿using OpenTK;
using Rug.Cmd;
using System.Collections.Generic;

namespace Rug.Game.Deferred.Data
{
	public class Armature
	{
		public int Index; 

		public Armature Parent; 

		public string Name; 
		public Matrix4 Transform;

		//public ushort[] VertexIndices;
		//public float[] Weights;

		public XSkinWeights Weights; 

		public Matrix4 MatrixOffset;

		public List<Armature> Children = new List<Armature>();

		public Matrix4 Current = Matrix4.Identity;
		public Matrix4 CombinedTransform = Matrix4.Identity;

		internal void Update()
		{
			CombineTransforms(Matrix4.Identity); 
		}

		private void CombineTransforms(Matrix4 parentTransform)
		{
			//Matrix4 L = Current; //  this.MatrixOffset;
			//Matrix4 C = this.CombinedTransform;

			this.CombinedTransform = Current * Transform * parentTransform;

			foreach (Armature arm in Children)
			{
				arm.CombineTransforms(this.CombinedTransform); 
			}						
		}
	}

	public class ArmatureBones
	{
		public readonly Armature Root;

		public readonly List<Armature> Bones;

		public readonly Matrix4[] BoneMatrix;

		public readonly string[] BoneNames; 

		public ArmatureBones(Armature root)
		{
			Root = root; 

			Bones = new List<Armature>();

			AddChildren(root); 

			BoneMatrix = new Matrix4[Bones.Count];
			BoneNames = new string[Bones.Count];

			for (int i = 0; i < BoneMatrix.Length; i++)
			{
				BoneMatrix[i] = Matrix4.Identity; 			
				Bones[i].Current = Matrix4.Identity; 
				BoneNames[i] = Bones[i].Name;

				RC.WriteLine(BoneNames[i]); 
			}

			Update();
		}

		public void Update()
		{		
			Root.Update();

			for (int i = 0; i < BoneMatrix.Length; i++)
			{
				BoneMatrix[i] = Bones[i].MatrixOffset * Bones[i].CombinedTransform;
			}
		}

		private void AddChildren(Armature root)
		{
			root.Index = Bones.Count;
			Bones.Add(root);
			
			foreach (Armature child in root.Children)
			{
				AddChildren(child);
			}
		}
	}
}
