// http://www.xbdev.net/xna/skinnedmodels/index.php
// XLoader.cs

using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;


public enum XTypes 
{
    X_BASE						= 0xFB,
    X_DATA						= 0xFC,
    X_REFERENCE					= 0xFD,
    X_UNKNOWN               	= 0xFE,         // unknown
    X_TEMPLATE					= 0xFF,			// template
    X_ERROR                 	= 0x0,
    X_HEADER					= 0x1,			// Header
    X_VECTOR					= 0x2,			// Vector
    X_COORDS2D					= 0x3,			// Coords2d
    X_QUATERNION				= 0x4,			// Quaternion
    X_MATRIX4X4					= 0x5,			// Matrix4x4
    X_COLORRGBA					= 0x6,			// ColorRGBA
    X_COLORRGB					= 0x7,			// ColorRGB
    X_INDEXEDCOLOR				= 0x8,			// IndexedColor
    X_BOOLEAN					= 0x9,			// Boolean
    X_BOOLEAN2D					= 0xA,			// Boolean2d
    X_MATERIAL					= 0xB,			// Material
    X_TEXTUREFILENAME			= 0xC,			// TextureFilename
    X_MESHFACE					= 0xD,			// MeshFace
    X_MESHFACEWRAPS				= 0xE,			// MeshFaceWraps
    X_MESHTEXTURECOORDS			= 0xF,			// MeshTextureCoords
    X_MESHNORMALS				= 0x10,			// MeshNormals 
    X_MESHVERTEXCOLORS			= 0x11,			// MeshVertexColors
    X_MESHMATERIALLIST			= 0x12,			// MeshMaterialList
    X_MESH						= 0x13,			// Mesh
    X_FRAMETRANSFORMMATRIX		= 0x14,			// FrameTransformMatrix
    X_FRAME						= 0x15,			// Frame
    X_FLOATKEYS					= 0x16,			// FloatKeys
    X_TIMEDFLOATKEYS			= 0x17,			// TimedFloatKeys
    X_ANIMATIONKEY				= 0x18,			// AnimationKey
    X_ANIMATIONOPTIONS			= 0x19,			// AnimationOptions
    X_ANIMATION					= 0x1A,			// Animation
    X_ANIMATIONSET				= 0x1B,			// AnimationSet
    X_SKINWEIGHTS           	= 0x1C,         // SkinWeights
    X_XSKINMESHHEADER       	= 0x1D,         // XSkinMeshHeader
    X_XOF						= 0x1E,			// xof
    X_VERTEXDUPLICATIONINDICES	= 0x1F,			// VertexDuplicationIndices
    X_EFFECTINSTANCE			= 0x20,			// EffectInstance
    X_EFFECTPARAMSTRING			= 0x21,			// EffectParamString
    X_EFFECTPARAMDWORD			= 0x22,			// EffectParamDWord
    X_EFFECTPARAMFLOATS			= 0x23,			// EffectParamFloats
    X_PATCH						= 0x24,			// Patch
    X_PATCHMESH					= 0x25,			// PatchMesh
    X_MATERIALWRAP				= 0x26,
    X_EFFECTDWORD				= 0x27,
    X_EFFECTFLOATS				= 0x28,
    X_EFFECTSTRING				= 0x29,
};


public class stDefines
{
    public stDefines(XTypes id, string name, stGUID guid)
    {
        this.id   = id;
        this.name = name;
        this.guid = guid;
    }
    public XTypes  id;
    public string name;
    public stGUID guid;
};

public class LookUp
{
	public static stDefines[] xLookUp = 
	{
		new stDefines( XTypes.X_HEADER                    ,"Header"                  ,new stGUID(0x3D82AB43,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_VECTOR                    ,"Vector"                  ,new stGUID(0x3D82AB5E,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_COORDS2D                  ,"Coords2d"                ,new stGUID(0xF6F23F44,0x7686,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_QUATERNION                ,"Quaternion"              ,new stGUID(0x10DD46A3,0x775B,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MATRIX4X4                 ,"Matrix4x4"               ,new stGUID(0xF6F23F45,0x7686,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_COLORRGBA                 ,"ColorRGBA"               ,new stGUID(0x35FF44E0,0x6C7C,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_COLORRGB                  ,"ColorRGB"                ,new stGUID(0xD3E16E81,0x7835,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_INDEXEDCOLOR              ,"IndexedColor"            ,new stGUID(0x1630B820,0x7842,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_BOOLEAN                   ,"Boolean"                 ,new stGUID(0x4885AE61,0x78E8,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_BOOLEAN2D                 ,"Boolean2d"               ,new stGUID(0x4885AE63,0x78E8,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MATERIALWRAP              ,"MaterialWrap"            ,new stGUID(0x4885AE60,0x78E8,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_TEXTUREFILENAME           ,"TextureFilename"         ,new stGUID(0xA42790E1,0x7810,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MATERIAL                  ,"Material"                ,new stGUID(0x3D82AB4D,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_MESHFACE                  ,"MeshFace"                ,new stGUID(0x3D82AB5F,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_MESHFACEWRAPS             ,"MeshFaceWraps"           ,new stGUID(0x4885AE62,0x78E8,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MESHTEXTURECOORDS         ,"MeshTextureCoords"       ,new stGUID(0xF6F23F40,0x7686,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MESHMATERIALLIST          ,"MeshMaterialList"        ,new stGUID(0xF6F23F42,0x7686,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MESHNORMALS               ,"MeshNormals"             ,new stGUID(0xF6F23F43,0x7686,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MESHVERTEXCOLORS          ,"MeshVertexColors"        ,new stGUID(0x1630B821,0x7842,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_MESH                      ,"Mesh"                    ,new stGUID(0x3D82AB44,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_FRAMETRANSFORMMATRIX      ,"FrameTransformMatrix"    ,new stGUID(0xF6F23F41,0x7686,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_FRAME                     ,"Frame"                   ,new stGUID(0x3D82AB46,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_FLOATKEYS                 ,"FloatKeys"               ,new stGUID(0x10DD46A9,0x775B,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_TIMEDFLOATKEYS            ,"TimedFloatKeys"          ,new stGUID(0xF406B180,0x7B3B,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_ANIMATION                 ,"Animation"               ,new stGUID(0x3D82AB4F,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_ANIMATIONSET              ,"AnimationSet"            ,new stGUID(0x3D82AB50,0x62DA,0x11CF,0xAB,0x39,0x00,0x20,0xAF,0x71,0xE4,0x33)	),
		new stDefines( XTypes.X_ANIMATIONKEY              ,"AnimationKey"            ,new stGUID(0x10DD46A8,0x775B,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_ANIMATIONOPTIONS          ,"AnimationOptions"        ,new stGUID(0xE2BF56C0,0x840F,0x11CF,0x8F,0x52,0x00,0x40,0x33,0x35,0x94,0xA3)	),
		new stDefines( XTypes.X_EFFECTDWORD               ,"EffectDWord"             ,new stGUID(0x622C0ED0,0x956E,0x4DA9,0x90,0x8A,0x2A,0xF9,0x4F,0x3C,0xE7,0x16)	),
		new stDefines( XTypes.X_EFFECTFLOATS              ,"EffectFloats"            ,new stGUID(0xF1CFE2B3,0x0DE3,0x4E28,0xAF,0xA1,0x15,0x5A,0x75,0x0A,0x28,0x2D)	),
		new stDefines( XTypes.X_EFFECTINSTANCE            ,"EffectInstance"          ,new stGUID(0xE331F7E4,0x0559,0x4CC2,0x8E,0x99,0x1C,0xEC,0x16,0x57,0x92,0x8F)	),
		new stDefines( XTypes.X_EFFECTPARAMDWORD          ,"EffectParamDWord"        ,new stGUID(0xE13963BC,0xAE51,0x4C5D,0xB0,0x0F,0xCF,0xA3,0xA9,0xD9,0x7C,0xE5)	),
		new stDefines( XTypes.X_EFFECTPARAMFLOATS         ,"EffectParamFloats"       ,new stGUID(0x3014B9A0,0x62F5,0x478C,0x9B,0x86,0xE4,0xAC,0x9F,0x4E,0x41,0x8B)	),
		new stDefines( XTypes.X_EFFECTPARAMSTRING         ,"EffectParamString"       ,new stGUID(0x1DBC4C88,0x94C1,0x46EE,0x90,0x76,0x2C,0x28,0x81,0x8C,0x94,0x81)	),
		new stDefines( XTypes.X_EFFECTSTRING              ,"EffectString"            ,new stGUID(0xD55B097E,0xBDB6,0x4C52,0xB0,0x3D,0x60,0x51,0xC8,0x9D,0x0E,0x42)	),
		new stDefines( XTypes.X_XSKINMESHHEADER           ,"XSkinMeshHeader"         ,new stGUID(0x3CF169CE,0xFF7C,0x44AB,0x93,0xC0,0xF7,0x8F,0x62,0xD1,0x72,0xE2)	),
		new stDefines( XTypes.X_VERTEXDUPLICATIONINDICES  ,"VertexDuplicationIndices",new stGUID(0xB8D65549,0xD7C9,0x4995,0x89,0xCF,0x53,0xA9,0xA8,0xB0,0x31,0xE3)),
		new stDefines( XTypes.X_SKINWEIGHTS               ,"SkinWeights"             ,new stGUID(0x6F0D123B,0xBAD2,0x4167,0xA0,0xD0,0x80,0x22,0x4F,0x25,0xFA,0xBB)	),
	};
}


public class IsSameGUIDC
{
	public static bool IsSameGUID(stGUID a, stGUID b)
	{
		if (a.m_data1==b.m_data1 &&
			a.m_data2==b.m_data2 &&
			a.m_data3==b.m_data3)
		{
			for (int i=0; i<8; i++)
			{
				if (a.m_data4[i]!=b.m_data4[i])
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
}

public class GetIDFromGUIDC
{
	public static int GetIDFromGUID(stGUID guid)
	{
		Debug.Assert(guid!=null);
		int itemId = -1;
		int numTypes = LookUp.xLookUp.Length; //sizeof(xTypes)/sizeof(stDefines);
		for (int ii=0; ii<numTypes; ii++)
		{
			if (IsSameGUIDC.IsSameGUID(LookUp.xLookUp[ii].guid, guid))
			{
				itemId = (int)LookUp.xLookUp[ii].id;
				break;
			}
		}
		return itemId;
	}
}


//----------------------------------------------------------------------------//

public class ReadHEADER
{
    public ReadHEADER(List<byte> objData, ref stXBase xBase)
    {	
        Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_DATA);

	    /*
	    Header {
		    1;
		    0;
		    1;
	    }
	    */
        int offset = 0;
	    UInt16 major = BitConverter.ToUInt16(objData.ToArray(), offset);	offset+=2;
	    UInt16 minor = BitConverter.ToUInt16(objData.ToArray(), offset);	offset+=2;
        UInt32 flags = BitConverter.ToUInt32(objData.ToArray(), offset);    offset+=4;

        Debug.Assert(objData.Count == offset);
    }
};

//----------------------------------------------------------------------------//

public class ReadMATRIX
{
    public ReadMATRIX(List<byte> objData, int offset, ref Matrix4 mat)
    {
        int startOffset = offset;
        mat.M11 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M12 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M13 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M14 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M21 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M22 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M23 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M24 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M31 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M32 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M33 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M34 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M41 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M42 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M43 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M44 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;

        Debug.Assert(mat.M11 > -999999 && mat.M11 < 9999999);
        
        Debug.Assert(64 == (offset-startOffset));
    }
}

public class ReadXFRAMETRANSFORMMATRIX
{
	public ReadXFRAMETRANSFORMMATRIX(List<byte> objData, ref Matrix4 mat)
    {
        Debug.Assert(mat!= null);

	    /*
	    FrameTransformMatrix {
		    0.000001,0.000000,1.000000,0.000000,
		    1.000000,-0.000000,-0.000001,0.000000,
		    0.000000,1.000000,-0.000000,0.000000,
		    0.233847,39.526119,-1.626864,1.000000;;
	    }
	    */

        /*
        int offset = 0;
	    for (int i=0; i<16; i++)
	    {
            float val = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
		    mat[i] = val;
	    }
        */
        
        int offset = 0;
        mat.M11 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M12 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M13 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M14 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M21 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M22 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M23 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M24 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M31 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M32 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M33 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M34 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M41 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M42 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M43 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        mat.M44 = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
        

        Debug.Assert(objData.Count == offset);
    }
}

//----------------------------------------------------------------------------//

public class ReadXMATERIAL
{
    public ReadXMATERIAL(List<byte> objData, ref stXMaterial xBase, ref string objName)
    {
	    Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_MATERIAL);

	    stXMaterial material = (stXMaterial)xBase;
	    Debug.Assert(material!=null);
	    int offset = 0;


	    //IDirect3DDevice9* pDevice = xData.m_pD3DDevice;
	    //Debug.Assert(pDevice);

	    /*
	    Material body {
		    0.819608;0.756863;0.588235;1.000000;;
		    0.000000;
		    0.000000;0.000000;0.000000;;
		    0.000000;0.000000;0.000000;;

		    TextureFilename {
		    "BODY.jpg";
		    }
	    }

	    or

	    Material {
		    0.109804;0.584314;0.694118;1.000000;;
		    0.000000;
		    0.109804;0.584314;0.694118;;
		    0.000000;0.000000;0.000000;;
	    }
	    */


	    if (objName.Length>0)
	    {
			    Debug.Assert(objName.Length>1);
			    Debug.Assert(objName.Length<127);
			    material.m_name = objName;
	    }


	    // Face Colour
	    for (int i=0; i<4; i++)
	    {
		    float val = BitConverter.ToSingle(objData.ToArray(), offset);		            offset+=4;
		    Debug.Assert(val>=0.0f && val<=1.0f);
		    material.m_faceColourRGBA[i] = val;
	    }

        float power = BitConverter.ToSingle(objData.ToArray(), offset);                     offset += 4;

	    for (int i=0; i<3; i++)
	    {
		    float specularColour = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;
	    }

	    for (int i=0; i<3; i++)
	    {
		    float emissiveColour = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;
	    }

	    Debug.Assert(objData.Count == offset);
    }
}

//----------------------------------------------------------------------------//

public class ReadXMESHMATERIALLIST
{
    public ReadXMESHMATERIALLIST(List<byte> objData, ref stXMaterialList xBase, ref stXData xData)
    {
	    Debug.Assert(xData!=null);
	    Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_MESHMATERIALLIST);

	    stXMaterialList matList = (stXMaterialList)xBase;
	    Debug.Assert(matList!=null);

        int offset = 0;

	    /*
	    MeshMaterialList  {
		    1;
		    3;
		    0,
		    0,
		    0;
		    { body }
	    }

	    or 

	    MeshMaterialList  {
		    1;
		    0;
		    ;

		    Material {...}		{ INLINE }
	    }

	    */



	    UInt32 numMaterials = BitConverter.ToUInt32(objData.ToArray(), offset);		offset+=4;
	    Debug.Assert(numMaterials>0 && numMaterials<1000);
	    matList.m_numMaterials = (int)numMaterials;

	    matList.m_matRefs = new stXMaterialList.stMatRefs[numMaterials];
	    for (int i=0; i<numMaterials; i++)
	    {
	        matList.m_matRefs[i] = new stXMaterialList.stMatRefs();
	    }

        UInt32 numFaceIndexes = BitConverter.ToUInt32(objData.ToArray(), offset); offset += 4;
	    Debug.Assert(numFaceIndexes>=0 && numFaceIndexes<100000);
	    matList.m_numIndexes = (int)numFaceIndexes;
	    matList.m_indexes = new int[numFaceIndexes];

	    for (int i=0; i<numFaceIndexes; i++)
	    {
            UInt32 matIndex = BitConverter.ToUInt32(objData.ToArray(), offset); offset += 4;
		    Debug.Assert(matIndex>=0 && matIndex<100); // Sanity check
		    matList.m_indexes[i] = (int)matIndex;
	    }

	    // To-Do - Sort out the material indexes to match the big list of materials
	    // after all the parsing has finished

	    Debug.Assert(objData.Count == offset);
    }
}


//----------------------------------------------------------------------------//

public class ReadXFRAME
{
    public ReadXFRAME(List<byte> objData, ref stXFrame xBase, ref string strName)
    {
	    Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_FRAME);

	    stXFrame xFrame = (stXFrame)xBase;
	    Debug.Assert(xFrame!=null);

	    if (strName.Length>0)
	    {
            Debug.Assert(strName.Length < 127);
		    xFrame.m_name = strName;
	    }

	    Debug.Assert(objData.Count == 0);
    }
}

//----------------------------------------------------------------------------//

public class ReadXANIMATIONSET
{
    public ReadXANIMATIONSET(List<byte> objData, ref stXAnimSet xBase, ref string objName)
    {
	    Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_ANIMATIONSET);

	    stXAnimSet xAnimSet = (stXAnimSet)xBase;
	    Debug.Assert(xAnimSet!=null);

        int offset = 0;
	    if (objName.Length>0)
	    {
            int len = objName.Length;
		    Debug.Assert(len<127);
		    xAnimSet.m_name = objName;
	    }

        Debug.Assert(objData.Count == offset);
    }
}

//----------------------------------------------------------------------------//

public class ReadXANIMATIONKEY
{
public ReadXANIMATIONKEY(List<byte> objData, ref stXAnimationKey aKey)
{
    Debug.Assert(aKey != null);
    Debug.Assert(aKey.GetXType() == XTypes.X_ANIMATIONKEY);

	//stXAnimation xAnimation = (stXAnimation)xBase;
	//Debug.Assert(xAnimation!=null);

	//Debug.Assert(xAnimation.m_animKey==null);
	//stXAnimationKey aKey = new stXAnimationKey();
	//xAnimation.m_animKey.Add( aKey );


	Debug.Assert(aKey!=null);

    int offset = 0;
	UInt32 keyType = BitConverter.ToUInt32(objData.ToArray(), offset);		offset += 4;
	Debug.Assert(keyType>0 && keyType<10000); // Sanity Check
	Debug.Assert(keyType==4);

    int nKeys = (int)BitConverter.ToUInt32(objData.ToArray(), offset); offset += 4;
	Debug.Assert(nKeys>0 && nKeys<10000); // Sanity Check
	aKey.m_numFrames = nKeys;

	aKey.m_mat = new Matrix4[nKeys];


	for (int i=0; i<nKeys; i++)
	{
		UInt32 time = BitConverter.ToUInt32(objData.ToArray(), offset);		    offset += 4;
		Debug.Assert(time>=0 && time<99999);


		UInt32 nValues = BitConverter.ToUInt32(objData.ToArray(), offset);		offset += 4;
		Debug.Assert(nValues==16);

        new ReadMATRIX(objData,  offset, ref aKey.m_mat[i]);    offset += 4*16;
        /*
		D3DXMATRIX& mat = aKey.m_mat[i];

		for (int k=0; k<16; k++)
		{
			float val = BitConverter.ToSingle(objData.ToArray(), offset);		offset += 4;
			Debug.Assert(val>-999999  && val <9999999);
			mat[k] = val;
		}
         * */
	}

	Debug.Assert(objData.Count == offset);
}
}


//----------------------------------------------------------------------------//

public class ReadXANIMATIONOPTIONS
{
public ReadXANIMATIONOPTIONS(List<byte> objData, ref stXBase xBase)
{
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_ANIMATIONSET);

	/*
	  AnimationOptions {
		   1;
		   0;
		  }
	*/

    int offset = 0;
	UInt32 openclosed		    = BitConverter.ToUInt32(objData.ToArray(), offset);		offset += 4;
	UInt32 positionquality      = BitConverter.ToUInt32(objData.ToArray(), offset);		offset += 4;

	Debug.Assert(objData.Count == offset);
}
}


//----------------------------------------------------------------------------//

public class ReadXANIMATION
{
public ReadXANIMATION(List<byte> objData, ref stXAnimation xBase, ref string objName)
{
	/*
	 Animation Name {		// Name {OPTIONAL}
		 {Bip01_L_Arm1}		// {OPTIONAL}
		 AnimationKey {
		  4;
		  24;
		  1,
		  2;
		  }
		  {Bip01_L_Arm1}	// {OPTIONAL}
	  }
	*/

    int offset = 0;
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_ANIMATION);

	stXAnimation xAnimation = (stXAnimation)xBase;
	Debug.Assert(xAnimation!=null);

	// Has Name?
	if (objName.Length>0)
	{
        Debug.Assert(objName.Length< 127);
		xAnimation.m_name = objName;;
	}

	Debug.Assert(objData.Count == offset);
}
}


//----------------------------------------------------------------------------//

public class ReadXMESH
{
    public ReadXMESH(List<byte> objData, ref stXMesh xBase, ref string objName)
    {
	    Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_MESH);

	    stXMesh xMesh = (stXMesh)xBase;
	    Debug.Assert(xMesh!=null);

	    /*
	    Mesh  {
		    3;
		    -71.449913;79.331818;18.395760;,
		    -71.449913;76.044464;-0.248118;,
		    -61.984173;77.705307;9.144111;;
		    2;
		    3;0,1,2;,
		    3;3,4,5;;

		    MeshNormals					{ OPTIONAL }
		    MeshMaterialList			{ OPTIONAL }
		    MeshTextureCoords			{ OPTIONAL }
		    XSkinMeshHeader				{ OPTIONAL }
		    SkinWeights					{ OPTIONAL }
	    }

	    or

	    Mesh  {
		    0;
		    ;
		    0;
		    ;
									    { OPTIONAL }
	    }
	    */

        int offset = 0;
	    UInt32 numVerts = BitConverter.ToUInt32(objData.ToArray(), offset);		offset+=4;
	    Debug.Assert(numVerts>=0 && numVerts<100000);

	    xMesh.m_numVerts = (int)numVerts;
	    xMesh.m_verts = new Vector3[xMesh.m_numVerts];

	    for (UInt32 i=0; i<numVerts; i++)
	    {
            xMesh.m_verts[i].X = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
            xMesh.m_verts[i].Y = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
            xMesh.m_verts[i].Z = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
	    }
    	

	    UInt32 numTris = BitConverter.ToUInt32(objData.ToArray(), offset);		        offset+=4;
	    Debug.Assert(numTris>=0 && numTris<100000);

	    xMesh.m_numTris = (int)numTris;
	    xMesh.m_tris = new stXMesh.stTriData[numTris + 100]; // 100 is a saftey check


	    for (UInt32 i=0; i<numTris; i++)
	    {
	        xMesh.m_tris[i] = new stXMesh.stTriData();
	        
		    UInt32 indicesCount = BitConverter.ToUInt32(objData.ToArray(), offset);	    offset+=4;
		    Debug.Assert(indicesCount==3);

		    for (UInt32 k=0; k<indicesCount; k++)
		    {
                UInt32 val = BitConverter.ToUInt32(objData.ToArray(), offset); offset += 4;
			    Debug.Assert(val>=0 && val<numVerts); // Sanity Check
			    Debug.Assert(k < 3);
			    Debug.Assert(i < numTris);
			    xMesh.m_tris[i].vertIndx[k] = (UInt32)val;	
		    }
	    }

	    Debug.Assert(objData.Count == offset);
    }
}

//----------------------------------------------------------------------------//

class ReadXMESHTEXTURECOORDS
{
public ReadXMESHTEXTURECOORDS(List<byte> objData, ref stXBase xBase)
{
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_MESH);

	stXMesh xMesh = (stXMesh)xBase;
	Debug.Assert(xMesh!=null);

	/*
	MeshTextureCoords  {
		84;
		0.805716;0.725244;,
		0.812639;0.764477;,
		0.808024;0.746014;;
		}
	*/

    int offset = 0;
	UInt32 numTexs = BitConverter.ToUInt32(objData.ToArray(), offset);		offset+=4;
	Debug.Assert(numTexs>0 && numTexs<100000);

	xMesh.m_numTexCoords = (int)numTexs;
	Debug.Assert(xMesh.m_texCoords==null);
	xMesh.m_texCoords = new Vector2[ numTexs ];

	for (UInt32 i=0; i<numTexs; i++)
	{
		float tu = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;
		float tv = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;

		Debug.Assert(tu>-99999.0f && tu<99999.0f);
		Debug.Assert(tv>-99999.0f && tv<99999.0f);
		xMesh.m_texCoords[i].X = tu;
		xMesh.m_texCoords[i].Y = tv;
	}

	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadSTRING
{
    public static string Read(List<byte> objData, int offset)
    {
        string strData = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count);
        
        char[] chars = strData.ToCharArray();
        int end = -1;
        for (int i=0; i<chars.Length; i++)
        {
            if (chars[i]=='\0' || chars[i]==14 || chars[i]==19)
            {
                end = i;
                break;
            }
        }
        if (end<0) end = chars.Length;
        Debug.Assert(end>-1);
        Debug.Assert(end<128);
        string str = strData.Substring(0, end);
        return str;
    }
};

//----------------------------------------------------------------------------//

public class ReadXSKINWEIGHTS
{
public ReadXSKINWEIGHTS(List<byte> objData, ref stXBase xBase)
{
	Debug.Assert(objData!=null);
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_MESH);

	stXMesh mesh = (stXMesh)xBase;
	Debug.Assert(mesh!=null);
	stXSkinWeights skinWeights = new stXSkinWeights();
	mesh.AddSkinWeight(skinWeights);

    int offset = 0;
	/*
	SkinWeights {
		"head_1";
		4;
		0,
		1,
		2,
		3;
		0.000000,
		0.000000,
		0.000000,
		0.000000;
		0.000383,-0.000000,0.364556,0.000000,
		0.000424,0.364556,0.000000,0.000000,
		-0.364556,0.000424,0.000383,0.000000,
		-0.610790,-24.768661,0.062114,1.000000;;
	}
	*/

    
    
    skinWeights.m_name = ReadSTRING.Read(objData, offset);     offset+=skinWeights.m_name.Length+1;
    
    //skinWeights.m_name = ReadSTRING::Read(Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count); offset+=skinWeights.m_name.Length;
    //skinWeights.m_name = BitConverter.ToString(objData.ToArray(), offset);  offset += skinWeights.m_name.Length;
    Debug.Assert(skinWeights.m_name.Length<64);

	UInt32 numIndxs = BitConverter.ToUInt32(objData.ToArray(), offset);		offset+=4;
	Debug.Assert(numIndxs>0 && numIndxs<100000);

	skinWeights.m_numIndexes = (int)numIndxs;
	skinWeights.m_indexes  = new int[numIndxs];
	skinWeights.m_weights  = new float[numIndxs];

	for (int i=0; i<numIndxs; i++)
	{
        int val = (int)BitConverter.ToUInt32(objData.ToArray(), offset); offset += 4;
		Debug.Assert(val>=0 && val<10000);
		skinWeights.m_indexes[i] = val;
	}

	for (int i=0; i<numIndxs; i++)
	{
        float val = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
		skinWeights.m_weights[i] = val;
	}

	// Read Matrix 4x4
	/*
	for (int i=0; i<16; i++)
	{
        float val = BitConverter.ToSingle(objData.ToArray(), offset); offset += 4;
		skinWeights.m_mat[i] = val;
	}
    */
    new ReadMATRIX(objData, offset, ref skinWeights.m_mat);   offset+=4*16;

	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadXSKINMESHHEADER
{
public ReadXSKINMESHHEADER(List<byte> objData, ref stXBase xBase)
{
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_MESH);

	/*
	XSkinMeshHeader {
		2;
		0;
		2;
	}
	*/

    int offset = 0;
	UInt16 nMaxSkinWeightsPerVertex	= BitConverter.ToUInt16(objData.ToArray(), offset);		offset+=2;
	UInt16 nMaxSkinWeightsPerFace	= BitConverter.ToUInt16(objData.ToArray(), offset);		offset+=2;
    UInt16 nBones                   = BitConverter.ToUInt16(objData.ToArray(), offset);     offset+=2;

	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadXVERTEXDUPLICATIONINDICES
{
public ReadXVERTEXDUPLICATIONINDICES(List<byte> objData, ref stXBase xBase)
{
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_MESH);

	/*
	 VertexDuplicationIndices {
		 24;
		 8;
		 0,
		 1,
		 2,
		 3,
	 }
	 */
    
    int offset = 0;
    
	UInt32 nIndices = BitConverter.ToUInt32(objData.ToArray(), offset);				    offset+=4;
	Debug.Assert(nIndices>0 && nIndices<5000); // Sanity check

	UInt32 nOriginalVertices = BitConverter.ToUInt32(objData.ToArray(), offset);		offset+=4;
	Debug.Assert(nOriginalVertices>0 && nOriginalVertices<5000); // Sanity check

	for (UInt32 i=0; i<nIndices; i++)
	{
        UInt32 indices = BitConverter.ToUInt32(objData.ToArray(), offset);              offset += 4;
	}

	Debug.Assert(objData.Count == offset);
}
}


//----------------------------------------------------------------------------//

public class ReadXMESHNORMALS
{
    public ReadXMESHNORMALS(List<byte> objData, ref stXBase xBase)
    {
	    Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_MESH);

	    stXMesh xMesh = (stXMesh)xBase;
	    Debug.Assert(xMesh!=null);

        int offset = 0;

	    /*
	    MeshNormals  {
		    3;
			    0.000248;-0.984808;0.173645;,
			    0.000248;-0.984808;0.173645;,
			    0.000497;-0.984808;0.173645;;
		    3;
			    3;0,1,2;,
			    3;3,4,5;,
			    3;6,7,8;;
	    }
	    */

	    UInt32 numNormals = BitConverter.ToUInt32(objData.ToArray(), offset);		offset+=4;
	    Debug.Assert(numNormals>=0 && numNormals<100000);

	    Debug.Assert(xMesh.m_numTris>0 || numNormals==0);
	    Debug.Assert(xMesh.m_numNormals==0);


        Vector3[] normals = new Vector3[numNormals * 3]; // 3 Sides Triangle

	    for (int i=0; i<numNormals; i++)
	    {
			    float xval = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;
			    float yval = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;
			    float zval = BitConverter.ToSingle(objData.ToArray(), offset);		offset+=4;

			    Debug.Assert(xval>-99999.0f && xval<999999.0f);
			    Debug.Assert(yval>-99999.0f && yval<999999.0f);
			    Debug.Assert(zval>-99999.0f && zval<999999.0f);

			    normals[i].X = xval;
			    normals[i].Y = yval;
			    normals[i].Z = zval;		
	    }


        UInt32 numIndices = BitConverter.ToUInt32(objData.ToArray(), offset);      offset += 4;
	    Debug.Assert(numIndices>=0 && numIndices<100000);

	    xMesh.m_numNormals = (int)numIndices;
    	
	    for (int i=0; i<numIndices; i++)
	    {
		    UInt32 indicesCount = BitConverter.ToUInt32(objData.ToArray(), offset);	    offset+=4;
		    Debug.Assert(indicesCount==3);

		    for (int k=0; k<3; k++)
		    {
                UInt32 val = BitConverter.ToUInt32(objData.ToArray(), offset); offset += 4;
			    Debug.Assert(val>=0 && val<numNormals);
			    Debug.Assert(i>=0   && i<xMesh.m_numTris);

			    xMesh.m_tris[i].normals[k] = normals[val];
		    }		
	    }


	    normals = null;

	    Debug.Assert(objData.Count == offset);
    }
}

//----------------------------------------------------------------------------//

public class ReadXTEXTUREFILENAME
{
public ReadXTEXTUREFILENAME(List<byte> objData, ref stXBase xBase, ref stXData xData)
{
	Debug.Assert(xBase!=null);
	Debug.Assert(xBase.GetXType()==XTypes.X_MATERIAL);
	/*
	 TextureFilename {
		  "wall1_d.psd";
		 }
	*/

	stXMaterial material = (stXMaterial)xBase;
	Debug.Assert(material!=null);
	Debug.Assert(xData!=null);
	int offset = 0;

    //string fileName = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count);    offset += fileName.Length;
    string fileName = ReadSTRING.Read(objData, offset);   offset += fileName.Length+1;
	int len = fileName.Length;

	Debug.Assert(len>0 && len<128);
	Debug.Assert(material.m_hasTexture==false);
	material.m_textureFileName = fileName;
	material.m_hasTexture = true;


    int ccc = objData.Count;
    byte[] ddd = objData.ToArray();
    
	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadXEFFECTINSTANCE
{
public ReadXEFFECTINSTANCE(List<byte> objData, ref stXBase xBase)
{
    Debug.Assert(objData != null);
    Debug.Assert(xBase != null);
	//Debug.Assert(xBase.GetType()==X_MATERIAL);
	
	/*
	 EffectInstance {
		"Glow.fx";

		.. various effect things
	}
	*/
	int offset = 0;

    string str = ReadSTRING.Read(objData, offset); offset+=str.Length+1;
    //string str = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count);    offset+=str.Length;
    //string str = BitConverter.ToString(objData.ToArray(), offset);      offset += str.Length;
	int len = str.Length;
	Debug.Assert(len>0 && len<256);
	string effectFileName = str;

	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadXEFFECTPARAMSTRING
{
public ReadXEFFECTPARAMSTRING(List<byte> objData, ref stXBase xBase)
{
    Debug.Assert(objData != null);
    Debug.Assert(xBase != null);
	//Debug.Assert(xBase.GetType()==X_MATERIAL);

	/*
	EffectParamString {
	   "Tex0";
	   "wall1_d.psd";
	  }
	*/


    int offset = 0;

    string str = ReadSTRING.Read(objData, offset); offset += str.Length+1;
    //string str = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count); offset += str.Length;
    
    //string paramName = BitConverter.ToString(objData.ToArray(), offset);            offset += paramName.Length;
    string paramName = str;
    Debug.Assert(paramName.Length > 0 && paramName.Length < 128);


    str = ReadSTRING.Read(objData, offset); offset += str.Length+1;
    //str = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count); offset += str.Length;
    string valueName = str;
    //string valueName = BitConverter.ToString(objData.ToArray(), offset);            offset += valueName.Length;
    Debug.Assert(valueName.Length > 0 && valueName.Length < 128);
    

	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadXEFFECTPARAMDWORD
{
public ReadXEFFECTPARAMDWORD(List<byte> objData, ref stXBase xBase)
{
	Debug.Assert(xBase!=null);
	//Debug.Assert(xBase.GetType()==X_MATERIAL);

	/*
	  EffectParamDWord {
		   "LightDir";
		   0;
		  }
	*/
    int offset = 0;

    string str = ReadSTRING.Read(objData, offset); offset += str.Length+1;
    //string str = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count); offset += str.Length;
    string paramName = str;
    //string paramName = BitConverter.ToString(objData.ToArray(), offset);        offset += paramName.Length;
    Debug.Assert(paramName.Length > 0 && paramName.Length < 128);

    UInt32 value = BitConverter.ToUInt32(objData.ToArray(), offset);            offset += 4;

	Debug.Assert(objData.Count == offset);
}
}

//----------------------------------------------------------------------------//

public class ReadXEFFECTPARAMFLOATS
{
public ReadXEFFECTPARAMFLOATS(List<byte> objData, ref stXBase xBase)
{
    Debug.Assert(xBase != null);
	//Debug.Assert(xBase.GetType()==X_MATERIAL);

	/*
	  EffectParamFloats {
		   "GlowColor";
		   4;
		   0.180392,
		   0.611765,
		   0.815686,
		   1.000000;
		  }
	*/

    int offset = 0;

    string str = ReadSTRING.Read(objData, offset); offset += str.Length+1;
    //string str = Encoding.ASCII.GetString(objData.ToArray(), offset, objData.Count); offset += str.Length;
    string paramName = str;
    //string paramName = BitConverter.ToString(objData.ToArray(), offset);        offset += paramName.Length;
    Debug.Assert(paramName.Length > 0 && paramName.Length < 128);

    UInt32 numFloats = BitConverter.ToUInt32(objData.ToArray(), offset);        offset += 4;

	for (UInt32 i=0; i<numFloats; i++)
	{
        float val = BitConverter.ToSingle(objData.ToArray(), offset);           offset += 4;
	}

	Debug.Assert(objData.Count == offset);
}
}


//----------------------------------------------------------------------------//

public class stXBase
{
    public virtual XTypes GetXType()    { return XTypes.X_BASE; }
	~stXBase()                          {}
};

//----------------------------------------------------------------------------//

public class stXSkinWeights
{
	public stXSkinWeights()
	{
        m_name = "NONAMESET";
		m_numIndexes	= 0;
		m_indexes		= null;
		m_weights		= null;
		m_mat = Matrix4.Identity;
	}
	~stXSkinWeights()
	{
		m_indexes = null;
		m_weights = null;
	}

	public string	    m_name;
    public int          m_numIndexes;
    public int[]        m_indexes;
    public float[]      m_weights;
	public Matrix4 m_mat;
};

//----------------------------------------------------------------------------//

public class stXMaterialList : stXBase
{
	public override XTypes GetXType() { return XTypes.X_MESHMATERIALLIST; }

	public stXMaterialList()
	{
		m_numMaterials = 0;
		m_numIndexes   = 0;
		m_indexes	   = null;
		m_matRefs	   = null;
		m_refsAdded	   = 0;
	}
	~stXMaterialList()
	{
		m_indexes = null;
        m_matRefs = null;
	}

	public class stMatRefs
	{
		public stMatRefs(){indx=-1; matId=-1;}
        public int indx;
        public int matId;
	};

	public void AddMaterialReference(int matId)
	{
		Debug.Assert(m_refsAdded<m_numMaterials);
		m_matRefs[m_refsAdded].indx  = m_refsAdded;
		m_matRefs[m_refsAdded].matId = matId;
		m_refsAdded++;
	}

    public int          m_numMaterials;
	public int  		m_numIndexes;
    public int[]        m_indexes;
    public stMatRefs[]  m_matRefs;
    public int          m_refsAdded;

};

//----------------------------------------------------------------------------//


public class stXMesh : stXBase
{
	public override XTypes GetXType() { return XTypes.X_MESH; }

	public stXMesh()
	{
		m_numVerts			= 0;
		m_verts				= null;
		m_numTris			= 0;
		m_tris				= null;
		m_matList			= null;
		m_numNormals		= 0;
		m_numTexCoords		= 0;
		m_texCoords			= null;
	}
	~stXMesh()
	{
		m_verts             = null;
		m_tris	            = null;
		m_texCoords         = null;
		m_matList           = null;
		m_skinWeights       = null;
	}

	public void AddSkinWeight(stXSkinWeights skinWeights)
	{
		Debug.Assert(skinWeights!=null);
		m_skinWeights.Add(skinWeights);
	}

	public int					    m_numVerts;
    public Vector3[]                m_verts;

	public class stTriData
	{
		public int	     matId;
        public UInt32[]  vertIndx   = new UInt32[3];
        public Vector3[] normals    = new Vector3[3];
	};
    public int                      m_numTris;
    public stTriData[]              m_tris;

    public int                      m_numTexCoords;
    public Vector2[]                m_texCoords;

    public int                      m_numNormals;

    public List<stXSkinWeights>     m_skinWeights = new List<stXSkinWeights>();
    public stXMaterialList          m_matList = null;


};

//----------------------------------------------------------------------------//

public class stXFrame : stXBase
{
	public override XTypes GetXType() { return XTypes.X_FRAME; }

    static int frameCount = 0;
	public stXFrame()
	{
	   // Debug.Assert(false); // Check strings formating
		frameCount++;
        m_name = String.Format("NOFRAMENAME{0:d6}", frameCount);
		m_mat = Matrix4.Identity;
		m_matTrans = Matrix4.Identity;
		m_matAnim = Matrix4.Identity;
	}

	~stXFrame()
	{
	    m_next  = null;
	    m_child = null;
	    m_mesh  = null;
	}

	public void AddChild(stXFrame xFrame)
	{
        Debug.Assert(xFrame!=null);
        m_child.Add(xFrame);
	}

	public void AddMesh(stXMesh xMesh)
	{
		Debug.Assert(xMesh!=null);
		m_mesh.Add(xMesh);
	}


	public List<stXFrame>   m_next      = new List<stXFrame>();
    public List<stXFrame>   m_child     = new List<stXFrame>();

    public List<stXMesh>    m_mesh      = new List<stXMesh>();
	public Matrix4 m_mat;
	public Matrix4 m_matTrans;
	public Matrix4 m_matAnim;
	public string		    m_name;
};

//----------------------------------------------------------------------------//

public class stXMaterial : stXBase
{
	public override XTypes GetXType() { return XTypes.X_MATERIAL; }
	
	static int materialCount = 0;
	public stXMaterial()
	{
		m_id	            = -1;
		m_textureFileName   = "";
		m_hasTexture        = false;
		m_name              = "";
		m_name = String.Format("RandomName{0:d6}", materialCount);
		materialCount++;
		
		//m_texture           = null;
	}
	~stXMaterial()
	{
	}

	// REMOVED BY PTEW 
	/*
	public void LoadTexture(ContentManager contentManger, string folder)
	{
		if (!m_hasTexture) return;
		
		Debug.Assert(contentManger!=null);

        // We have to strip of any path and file format information from the
        // filename, i.e. C://myfile//image1.bmp" -> "image1"
		int len = m_textureFileName.Length;
        Debug.Assert(len>=1 && len<127);
        
        int lastDot = m_textureFileName.LastIndexOf(".");
        int slashes = m_textureFileName.LastIndexOf("\\");
        if (slashes<0) slashes = 0;
        if (lastDot<0) lastDot = m_textureFileName.Length;
        
        string fileName = m_textureFileName.Substring(slashes, lastDot);
        
		Debug.Assert(fileName.Length>=0 && fileName.Length<127);

		string fullFileName = folder + fileName;
		Debug.Assert(fullFileName.Length>1 && fullFileName.Length<255);
									
		m_texture = contentManger.Load<Texture2D>(fullFileName);
        if (m_texture==null)
        {
            string fileNameNoExt = Path.GetFileNameWithoutExtension(fullFileName);
            m_texture = contentManger.Load<Texture2D>(fileNameNoExt);
        }
		Debug.Assert(m_texture!=null);
	}
	*/ 

	public int		    m_id;
    public string       m_name;
    public float[]      m_faceColourRGBA = new float[4];
    public bool         m_hasTexture;
    public string       m_textureFileName;

    //public Texture2D    m_texture       = null;
};

//----------------------------------------------------------------------------//

public class stMaterialManager
{
	public stMaterialManager()
	{
	}

	~stMaterialManager()
	{
		m_materials = null;
	}

	public stXMaterial AddMaterial()
	{
		m_materials.Add( new stXMaterial() );
		m_materials[ m_materials.Count - 1 ].m_id = m_materials.Count - 1;
		
		return m_materials[ m_materials.Count - 1 ];
	}

    public stXMaterial FindMaterial(string matName)
	{
        for (int i = 0; i < m_materials.Count; i++)
        {
            if (m_materials[i]!=null && m_materials[i].m_name == matName)
            {
                return m_materials[i];
            }
        }
        return null;
	}

    public stXMaterial GetMaterial(int id)
	{
	    for (int i=0; i<m_materials.Count; i++)
	    {
	        if (m_materials[i]!=null && m_materials[i].m_id == id)
	        {
	            return m_materials[i];
	        }
	    }
	    return null;
	}

	// REMOVED BY PTEW 
	/* 
	public void LoadTextures(ContentManager contentManger, string folder)
	{
	    for (int i=0; i<m_materials.Count; i++)
	    {
	        m_materials[i].LoadTexture(contentManger, folder);
	    }
	}
	*/ 

	List<stXMaterial> m_materials = new List<stXMaterial>();
};

//----------------------------------------------------------------------------//

public class stXAnimationKey : stXBase
{
    public override XTypes GetXType() { return XTypes.X_ANIMATIONKEY; }

    public stXAnimationKey()
    {
        //m_mat = Matrix.Identity;
    }
    
	~stXAnimationKey()
	{
	}

	public int			m_numFrames;
	public Matrix4[] m_mat;
};

//----------------------------------------------------------------------------//

public class stXAnimation : stXBase
{
	public override XTypes GetXType() { return XTypes.X_ANIMATION; }

    static int skinCount = 0;
    static int animCount = 0;
	public stXAnimation()
	{
		m_name = String.Format("NOANIMNAMESET{0:d6}", animCount);
        animCount++;
		m_skinName = String.Format("NOSKINNAME{0:d6}", skinCount);
		skinCount++;
	}
	~stXAnimation()
	{
		m_animKey = null;
	}

	public string				    m_name;
	public string				    m_skinName;
	public List<stXAnimationKey>    m_animKey    = new List<stXAnimationKey>();
};

//----------------------------------------------------------------------------//

public class stXAnimSet : stXBase
{
	public override XTypes GetXType() { return XTypes.X_ANIMATIONSET; }
	
	static int animSetCount = 0;
	
	public stXAnimSet()
	{
		m_name = String.Format("NONAME{0:d6}", animSetCount);
		animSetCount++;
		//m_anims=null;
	}
	~stXAnimSet()
	{
		m_anims = null;
	}

	public void AddAnimation(stXAnimation anim)
	{
		Debug.Assert(anim!=null);
		m_anims.Add(anim);
	}

	public string 			    m_name;
	public List<stXAnimation>	m_anims = new List<stXAnimation>();
};

//----------------------------------------------------------------------------//

public class stXData : stXBase
{
    public override XTypes GetXType() { return XTypes.X_DATA; }
    
	public stXData()
	{
		//m_animSet		= null;
		m_frameNo		= 0;
        m_curAnimSet    = null;
        //m_pD3DDevice    = null;
		m_fileName      = "NOFILENAMESET";
	}

	~stXData()
	{
        m_animSet           = null;
        //m_pD3DDevice        = null;
        m_materialManager   = null;
	}

	public stMaterialManager	m_materialManager   = new stMaterialManager();

	public stXFrame			    m_frame             = new stXFrame();
	public List<stXAnimSet>	    m_animSet           = new List<stXAnimSet>();
	
	public int				    m_frameNo;
    public stXAnimSet           m_curAnimSet        = null;
	//IDirect3DDevice9*	        m_pD3DDevice;
	public string		        m_fileName;
}

//----------------------------------------------------------------------------//



public class stMyXParser : stXParser
{
	stXData m_xData = null;

	public void Start(string fileName, stXData xData)
	{
		Debug.Assert(xData!=null);
		Debug.Assert(fileName.Length>0);

		m_xData = xData;
		stXFileData top = new stXFileData();
		top.m_name = "top";
		top.m_data = xData;

		StartParsingXFile(fileName, top);
	}

	public override void ParseObject(ref stXFileData    pParentObjData,
					                 ref stXFileData    pObjData,
					                 int            depth,
					                 List<byte>     data)
	{

		ParseMyData(ref m_xData, ref pParentObjData, ref pObjData, data);

		if (!pObjData.m_reference)
			Debug.Assert(pObjData.m_data!=null);

		// Must call the base
		base.ParseObject(ref pParentObjData,
			             ref pObjData,
						 depth,
						 data);
	}

    void ParseMyData(ref stXData xData, ref stXFileData pParentObjData, ref stXFileData pObjData, List<byte> objData)
	{
        Debug.Assert(pParentObjData!=null);
        Debug.Assert(pObjData != null);
        Debug.Assert(objData != null);

        Debug.Assert(pParentObjData.GetData() != null);	// Should have a valid parent
        Debug.Assert(pObjData.GetData() == null);  // Shouldn't have any data in this yet


        stXBase xBaseParent = (stXBase)pParentObjData.GetData();

        // By default set children of this object to use our parent
        pObjData.m_data = pParentObjData;


        stXBase xBase = (stXBase)pParentObjData.GetData();
        Debug.Assert(xBase!=null);

        Debug.Assert(pObjData.GetGUID()!=null);
        int itemId = GetIDFromGUIDC.GetIDFromGUID(pObjData.GetGUID());

        if (pObjData.m_reference)
        {
            Debug.Assert(itemId < 0);
            itemId = (int)XTypes.X_REFERENCE;
        }

        Debug.Assert(itemId > -1); // If we want to parse every single bit of data?
        // Comment out to skip unknown template data


        switch (itemId)
        {
            // Global
            case (int)XTypes.X_HEADER:                      new ReadHEADER(objData, ref xBase);     break;


            case (int)XTypes.X_FRAME:
            {
                stXFrame xFrameParent = null;
                if (xBase.GetXType() == XTypes.X_DATA)  xFrameParent = ((stXData)xBase).m_frame;
                if (xBase.GetXType() == XTypes.X_FRAME) xFrameParent = (stXFrame)xBase;

                stXFrame xFrame = new stXFrame();
                new ReadXFRAME(objData, ref xFrame, ref pObjData.m_name);

                xFrameParent.AddChild(xFrame);

                pObjData.m_data = xFrame;
            }
            break;

            case (int)XTypes.X_MATERIAL:
            {
                Debug.Assert(xBase.GetXType() == XTypes.X_DATA ||
                             xBase.GetXType() == XTypes.X_MESHMATERIALLIST);

                stXMaterial xMat = ((stXData)xData).m_materialManager.AddMaterial();
                new ReadXMATERIAL(objData, ref xMat, ref pObjData.m_name);
                pObjData.m_data = xMat;

                if (xBase.GetXType() == XTypes.X_MESHMATERIALLIST)
                {
                    stXMaterialList matList = (stXMaterialList)xBase;
                    matList.AddMaterialReference(xMat.m_id);
                }
            }
            break;

            case (int)XTypes.X_MESH:
            {
                stXFrame xFrameParent = null;
                if (xBase.GetXType() == XTypes.X_DATA)  xFrameParent = ((stXData)xBase).m_frame;
                if (xBase.GetXType() == XTypes.X_FRAME) xFrameParent = (stXFrame)xBase;

                stXMesh xMesh = new stXMesh();
                new ReadXMESH(objData, ref xMesh, ref pObjData.m_name);

                xFrameParent.AddMesh(xMesh);

                pObjData.m_data = xMesh;
            }
            break;


            // Only for XFrame
            case (int)XTypes.X_FRAMETRANSFORMMATRIX:  
            {
                Debug.Assert(xBase.GetXType()==XTypes.X_FRAME);
                stXFrame xFrame = (stXFrame)xBase;
                Debug.Assert(xFrame!=null);
                
                new ReadXFRAMETRANSFORMMATRIX(objData, ref xFrame.m_mat);
            }
            break;


            // Only for MESH
            case (int)XTypes.X_MESHNORMALS:  new ReadXMESHNORMALS(objData, ref xBase); break;

            case (int)XTypes.X_MESHMATERIALLIST:
            {
                Debug.Assert(xBase!=null);
                Debug.Assert(xBase.GetXType() == XTypes.X_MESH);

                stXMesh mesh = (stXMesh)xBase;
                Debug.Assert(mesh!=null);

                Debug.Assert(mesh.m_matList == null);

                stXMaterialList matList = new stXMaterialList();
                mesh.m_matList = matList;

                new ReadXMESHMATERIALLIST(objData, ref matList, ref xData);
                pObjData.m_data = matList;
            }
            break;

            case (int)XTypes.X_MESHTEXTURECOORDS:           new ReadXMESHTEXTURECOORDS(objData, ref xBase); break;
            case (int)XTypes.X_SKINWEIGHTS:                 new ReadXSKINWEIGHTS(objData, ref xBase); break;
            case (int)XTypes.X_XSKINMESHHEADER:             new ReadXSKINMESHHEADER(objData, ref xBase); break;
            case (int)XTypes.X_VERTEXDUPLICATIONINDICES:    new ReadXVERTEXDUPLICATIONINDICES(objData, ref xBase); break;


                // Only for XMaterial
            case (int)XTypes.X_TEXTUREFILENAME:             new ReadXTEXTUREFILENAME(objData, ref xBase, ref xData); break;
            case (int)XTypes.X_EFFECTINSTANCE:              new ReadXEFFECTINSTANCE(objData, ref xBase); break;
            case (int)XTypes.X_EFFECTPARAMSTRING:           new ReadXEFFECTPARAMSTRING(objData, ref xBase); break;
            case (int)XTypes.X_EFFECTPARAMDWORD:            new ReadXEFFECTPARAMDWORD(objData, ref xBase); break;
            case (int)XTypes.X_EFFECTPARAMFLOATS:           new ReadXEFFECTPARAMFLOATS(objData, ref xBase); break;

            case (int)XTypes.X_ANIMATIONSET:
            {
                stXAnimSet xAnimSet = new stXAnimSet();
                stXData xxData = (stXData)xBase;
                xxData.m_animSet.Add(xAnimSet);

                if (xBase.GetXType() == XTypes.X_DATA)
                {
                }
                else if (xBase.GetXType() == XTypes.X_ANIMATIONSET)
                {
                }
                else
                {
                    Debug.Assert(false);
                }

                new ReadXANIMATIONSET(objData, ref xAnimSet, ref pObjData.m_name);
                pObjData.m_data = xAnimSet;
            }
            break;

            // Only XAnimation
            case (int)XTypes.X_ANIMATIONKEY: 
            {
                Debug.Assert(xBase != null);
                Debug.Assert(xBase.GetXType() == XTypes.X_ANIMATION);
                stXAnimation xAnimation = (stXAnimation)xBase;
                Debug.Assert(xAnimation!=null);
                
                stXAnimationKey aKey = new stXAnimationKey();
                xAnimation.m_animKey.Add(aKey);
                new ReadXANIMATIONKEY(objData, ref aKey);
            }
            break;

            // Only AnimationSet
            case (int)XTypes.X_ANIMATION:
            {
                Debug.Assert(xBase!=null);
                Debug.Assert(xBase.GetXType() == XTypes.X_ANIMATIONSET);

                stXAnimSet xAnimSet = (stXAnimSet)xBase;
                Debug.Assert(xAnimSet!=null);

                stXAnimation anim = new stXAnimation();
                xAnimSet.AddAnimation(anim);

                new ReadXANIMATION(objData, ref anim, ref pObjData.m_name);
                pObjData.m_data = anim;
            }
            break;

            case (int)XTypes.X_REFERENCE:
            {
                Debug.Assert(pObjData!=null);
                Debug.Assert(pObjData.m_reference);
                Debug.Assert(pObjData.m_name.Length>0);

                switch (xBase.GetXType())
                {
                    case XTypes.X_MESHMATERIALLIST:
                    {
                        stXMaterial xMat = xData.m_materialManager.FindMaterial(pObjData.m_name);
                        Debug.Assert(xMat!=null);

                        stXMaterialList xMaterialList = (stXMaterialList)xBase;
                        Debug.Assert(xMaterialList!=null);
                        xMaterialList.AddMaterialReference(xMat.m_id);
                    }
                    break;

                    case XTypes.X_ANIMATION:
                    {
                        stXAnimation xAnimation = (stXAnimation)xBase;
                        Debug.Assert(xAnimation!=null);

                        string skinName = pObjData.m_name;
                        Debug.Assert(skinName.Length>0);
                        int len = skinName.Length;
                        Debug.Assert(len > 0 && len < 127);
                        Debug.Assert(xAnimation.m_skinName[0] == 'N' &&
                                     xAnimation.m_skinName[1] == 'O'); // i.e. NOSKINNAME
                        xAnimation.m_skinName = skinName;
                    }
                    break;

                    default: { Debug.Assert(false); } break;
                }

            }
            break;

            default:
            {
            }
            break;
            
        }

	}
};











