// http://www.xbdev.net/xna/skinnedmodels/index.php

using System.Diagnostics;
using System.IO;

//**To Test tokeniser**
//
//TokenParser tp = new TokenParser("..\\..\\templates.r");
//while (tp.GetNextToken() != null)
//{
//    System.Console.WriteLine(tp.GetToken().GetTokenType() + " : " + tp.GetToken().m_data);
//}

/******************************************************************************/

public class stToken
{
    public stToken() 
    { 
        m_tokenType = (TokenType)(-1); 
        m_data      = null; 
    }
    ~stToken()
    {
        m_data = "";
    }

    public enum TokenType
    {
        TokenString,		// "str.."
        TokenNumber,		// 0123456789.
        TokenOpenB,			// {
        TokenCloseB,		// }
        TokenComma,			// ,
        TokenSemiColon,		// ;
        TokenColon,			// :
        TokenOpenSquare,	// [
        TokenCloseSquare	// ]
    };

    public TokenType   m_tokenType;
    public string      m_data;

    public TokenType GetTokenType() {return m_tokenType;}

	public static void SetToken(stToken token, TokenType tokenType, string strData)
	{
		token.m_tokenType = tokenType;
        token.m_data = "";
		if (strData.Length>0)
		{
            token.m_data = strData;
		}
	}
};


/******************************************************************************/

public class TokenParser
{
    static string s_validChars = "abcdefghijklmnopqrstuvwxyz"   +
							     "ABCDEFGHIJKLMNOPQRSTUVWXYZ"   +
							     "1234567890"                   +
							     "\\,.:;{} \t\r\n<>-\"[]_/=()";

    public static bool CheckValidChars(string word, string vals)
    {
	    Debug.Assert(word!=null);
	    Debug.Assert(vals!=null);
	    for (int wv=0; wv<word.Length; wv++)
	    {
		    bool ok=false;
            for(int vv=0; vv<vals.Length; vv++)
		    {
			    if (word[wv]==vals[vv])
			    {
				    ok=true;
				    break;
			    }
		    }

		    if (!ok)
		    {
			    return false;
		    }
	    }
	    return true;
    }

    public static bool CheckValidChar(string letter, string vals)
    {
	    Debug.Assert(vals!=null);
	    bool ok=false;
        for (int vv=0; vv<vals.Length; vv++)
	    {
		    if (letter[0]==vals[vv])
		    {
			    ok=true;
			    break;
		    }
	    }

	    if (!ok) return false;

	    return true;
    }

	public TokenParser(string fileName)
	{
		Debug.Assert(fileName.Length>0 && fileName.Length<256);

        StreamReader reader = new StreamReader(fileName);
        string text = reader.ReadToEnd();
        Init(text);
	}

    public stToken GetToken()
    {
        return m_token;
    }

    stToken PeekNextToken()
    {
	    int prevIndx	    = m_indx;
	    stToken prevToken   = m_token;
	    m_tokenPeek         = GetNextToken();
    	
	    m_indx  = prevIndx;
	    m_token = prevToken;
	    return m_tokenPeek;
    }

    public stToken GetNextToken()
    {
        int sizeData = m_fileData.Length;
        Debug.Assert(sizeData > 0 && sizeData < 2000000);

        if (m_indx>(sizeData-1))
	    {
		    return null;
	    }

	    const int MAX_STRING_LEN = 128;
	    string buf = "";
	    int  bufLen = 0;
	    bool foundToken = false;

	    while (m_indx<sizeData)
	    {
		    char letter = m_fileData[m_indx];
		    m_indx++;

		    switch (letter)
		    {
			    case '"':
			    {
				    buf="";
				    bufLen = 0;
				    while (m_fileData[m_indx]!=0 && m_fileData[m_indx]!='"')
				    {
					    buf += m_fileData[m_indx];
					    bufLen++;
					    Debug.Assert(bufLen<MAX_STRING_LEN-1);
					    m_indx++;
				    }
				    m_indx++;
                    stToken.SetToken(m_token, stToken.TokenType.TokenString, buf);
			    }
			    break;

                case '{': stToken.SetToken(m_token, stToken.TokenType.TokenOpenB, "");          break;
                case '}': stToken.SetToken(m_token, stToken.TokenType.TokenCloseB, "");         break;
                case ',': stToken.SetToken(m_token, stToken.TokenType.TokenComma, "");          break;
                case ';': stToken.SetToken(m_token, stToken.TokenType.TokenSemiColon, "");      break;
                case ':': stToken.SetToken(m_token, stToken.TokenType.TokenColon, "");          break;
                case '[': stToken.SetToken(m_token, stToken.TokenType.TokenOpenSquare, "");     break;
                case ']': stToken.SetToken(m_token, stToken.TokenType.TokenCloseSquare, "");    break;

			    case '\n':
			    {
				    m_lineNo++;
				    continue;
			    }
			    //break;

			    case '/': // Comment
			    {
				    Debug.Assert(m_fileData[m_indx]=='/');
				    while (m_fileData[m_indx]!=0 && !CheckValidChar(m_fileData.Substring(m_indx), "\r\n"))
				    {
					    m_indx++;
				    }

					if (m_fileData[m_indx] == '\r')
						m_indx++;

				    if (m_fileData[m_indx]=='\n')
					    continue;
			    }
			    break;

			    case (char)0:
			    {
				    Debug.Assert(false);
			    }
			    break;

                case '\r':
                case '\t':
                case ' ':
			    {
				    continue;
			    }
			    //break;

			    default:
			    {
                    buf = "";
				    buf += letter;
				    while (m_fileData[m_indx]!=0 && !CheckValidChar(m_fileData.Substring(m_indx,1), "{},[];: \r\n\t"))
				    {
					    buf += m_fileData[m_indx];
					    m_indx++;
				    }

				    bool isNum = CheckValidChars(buf, "-0123456789.");
                    stToken.TokenType tokenType = (isNum ? stToken.TokenType.TokenNumber : stToken.TokenType.TokenString);
				    stToken.SetToken(m_token, tokenType, buf);
			    }
                break;
		    }

		    foundToken = true;
		    break;
	    }
	    Debug.Assert(m_token.m_tokenType>=0);

	    if (!foundToken)
	    {
		    return null;
	    }

	    return m_token;
    }


    private void Init(string fileData)
	{
		bool valid = CheckValidChars(fileData, s_validChars);
		Debug.Assert(valid);

		m_fileData  = fileData;
		m_indx		= 0;
		m_lineNo	= 1;
	}

    string  m_fileData      = "";
    int     m_indx          = 0;
    int     m_lineNo        = 0;
    stToken m_token         = new stToken();
    stToken m_tokenPeek     = new stToken();
}

