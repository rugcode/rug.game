// http://www.xbdev.net/xna/skinnedmodels/index.php
// XRender.cs

using System.Collections.Generic;


public class XTris
{
    public class Tri
    {
        public Tri(int matId) {m_matId=matId;}
        public List<int>   m_indices = new List<int>();
        public int         m_matId;
    };
    public void AddTri(int indices, int matId)
    {
        int found = -1;
        for (int i=0; i<m_tris.Count; i++)
        {
            if (m_tris[i].m_matId==matId)
            {
                found=i;
                break;
            }
        }
        if (found>-1)
        {   
            m_tris[found].m_indices.Add(indices);
        }
        else
        {
            m_tris.Add( new Tri(matId) );
            m_tris[m_tris.Count-1].m_indices.Add(indices);
        }
    }
    public List<Tri> m_tris = new List<Tri>();
};

/* 
public class XRender
{
    static int     s_numMeshesRendered     = 0;
    static int     s_materialSwaps         = 0;
    static int     s_numTriCount           = 0;
    static bool    s_staticPose            = false;
    
    float          m_animFrame             = 0.0f;

    const int MAX_BONE_WEIGHTS             = 7;
   
    public GameDraw gameDraw;
   
    
// Public
    public XRender(ref GameDraw gameDraw)
    {
        this.gameDraw = gameDraw;
    }

    public void Render(stXData xFile)
    {
        s_numMeshesRendered = 0;
        s_materialSwaps     = 0;
        s_numTriCount       = 0;

        Debug.Assert(xFile!=null);

        stXFrame xFrame = xFile.m_frame;

        RenderFrame(ref xFile, ref xFrame);
    }
    
    public void Update(ref stXData xFile)
    {
	    if (xFile.m_animSet.Count > 0)
	    {
            xFile.m_curAnimSet = xFile.m_animSet[0]; // BUG*** BUG***   use first one as the current animset

		    stXAnimation boneAnim = xFile.m_curAnimSet.m_anims[0]; // check top anim for loop count
		    if (boneAnim!=null)
		    {
			    Debug.Assert(boneAnim.m_animKey.Count>0);
			    if (boneAnim.m_animKey.Count>0)
			    {
				    m_animFrame += 0.1f;
                    xFile.m_frameNo = (int)m_animFrame;


				    int maxFrames = boneAnim.m_animKey[0].m_numFrames;


				    if (xFile.m_frameNo >= maxFrames)
				    {
					    xFile.m_frameNo = 0;
					    m_animFrame = 0.0f;
				    }
			    }
		    }
	    }

        BuildFrameMatHierachy(ref xFile, null, ref xFile.m_frame);
    }
    
    
// Private

    private void RenderFrame(ref stXData xFile, ref stXFrame xBase)
    {
    	Debug.Assert(xBase!=null);
	    Debug.Assert(xBase.GetXType()==XTypes.X_FRAME);


	    stXFrame xFrame = (stXFrame)xBase;
	    Debug.Assert(xFrame!=null);
    	
	    for (int nn=-1; nn<xFrame.m_next.Count; nn++)
	    {
	        stXFrame xFrameNext = null;
	        if (nn==-1) xFrameNext = xFrame;
	        else        xFrameNext = xFrame.m_next[nn];
	        
		    if (xFrameNext.m_child.Count>0)
		    {
		        for (int i=0; i<xFrameNext.m_child.Count; i++)
		        {
		            stXFrame frameChild = xFrameNext.m_child[i];
			        Debug.Assert(xBase!=frameChild);
			        RenderFrame(ref xFile, ref frameChild);
			    }
		    }

            int numMeshes = xFrame.m_mesh.Count;
            for (int i=0; i<numMeshes; i++)
            {
                stXMesh xMesh = xFrameNext.m_mesh[i];
                RenderMesh(ref xFile, ref xFrameNext, ref xMesh);
            }
	    }
    }
    
    stXAnimation FindAnimation(ref stXAnimSet xAnimSet, string boneName)
    {
        for (int i=0; i<xAnimSet.m_anims.Count; i++)
        {
	        stXAnimation tmp = xAnimSet.m_anims[i];

		    if ( boneName == tmp.m_skinName )
		    {
			    return tmp;
		    }
	    }
	    return null;
    }

    stXFrame FindFrame(stXFrame xFrame, string boneName)
    {
        for (int i=-1; i<xFrame.m_next.Count; i++)
        {
            stXFrame tmp = null;
            if (i==-1) tmp = xFrame;
            else       tmp = xFrame.m_next[i];

	        for (int cc=0; cc<tmp.m_child.Count; cc++)
	        {
                stXFrame foundFrame = FindFrame(tmp.m_child[cc], boneName);
                if (foundFrame!=null)
                {
                    return foundFrame;
                }
	        }

		    if ( boneName == tmp.m_name )
		    {
			    return tmp;
		    }
	    }
	    return null;
    }

    private void BuildFrameMatHierachy(ref stXData xFile, stXFrame xFrameParent, ref stXFrame xFrame)
    {
        Debug.Assert(xFile!=null);
        Debug.Assert(xFrame!=null);

        Matrix parentTransMat;
        Matrix parentAnimMat;
        parentTransMat = Matrix.Identity;
        parentAnimMat  = Matrix.Identity;
        if (xFrameParent!=null)
        {
            parentTransMat = xFrameParent.m_matTrans;
            parentAnimMat  = xFrameParent.m_matAnim;
        }

 
        for (int nn = -1; nn < xFrame.m_next.Count; nn++)
        {
            stXFrame xFrameNext = null;
            if (nn==-1) xFrameNext = xFrame;
            else        xFrameNext = xFrame.m_next[nn];
            
            // Default Mat
            Matrix animMat = xFrameNext.m_mat;

            // If we have an Anim, replace our default one with it
            string frameName = xFrameNext.m_name;
            stXAnimSet xAnimSet = xFile.m_curAnimSet;
            if (xAnimSet!=null && !s_staticPose)
            {
                stXAnimation anim = FindAnimation(ref xAnimSet, frameName);
                if (anim!=null)
                {
                    int frameNo = xFile.m_frameNo;

                    Debug.Assert(anim!=null);
                    Debug.Assert(anim.m_animKey.Count>0);
                    int numFrames0 = anim.m_animKey[0].m_numFrames;
                    Debug.Assert(numFrames0 > 0 && numFrames0 > frameNo);

                    animMat = anim.m_animKey[0].m_mat[frameNo];

                    /*
                    static float offf = 5.0f;

                    if (strcmp(frameName, "Box03")==0)
                    {
                        animMat._42 += offf;
                    }

                    static float offf2 = 100.0f;

                    if (strcmp(frameName, "Line01")==0)
                    {
                        animMat._42 += offf2;
                    }
                    * /
                }
            }

            xFrameNext.m_matTrans = xFrameNext.m_mat * parentTransMat;
            xFrameNext.m_matAnim  = animMat * parentAnimMat;

            if (xFrameNext.m_child.Count>0)
            {
                for (int cc=0; cc<xFrameNext.m_child.Count; cc++)
                {
                    stXFrame childXFrame = xFrameNext.m_child[cc];
                    BuildFrameMatHierachy(ref xFile, xFrameNext, ref childXFrame);
                    xFrameNext.m_child[cc] = childXFrame;
                }
            }
        }
    }
	
    private class stVertex
    {
	    public Vector3 pos;
	    public Vector3 norm;
	    public Vector2 tex;

	    public Vector3[]    skinPos = new Vector3[MAX_BONE_WEIGHTS];
        public float[]      weight  = new float[MAX_BONE_WEIGHTS];
	    public int count;
    };

    private void RenderMesh(ref stXData xFile, ref stXFrame xFrame, ref stXMesh xMesh)
    {
    
    	if (xMesh.m_numVerts==0 || xMesh.m_numTris==0) return;

	    s_numMeshesRendered++;
	    s_numTriCount+=xMesh.m_numTris;
 

	    int numVerts			    = xMesh.m_numVerts;
	    stVertex[] vertex	        = new stVertex[numVerts + 5]; // +5 is safety buffer

        int numNorms			    = xMesh.m_numNormals;
    	
        int numTexCoords		    = xMesh.m_numTexCoords;

	    int numTris			        = xMesh.m_numTris;
	    stXMesh.stTriData[]  tris	= xMesh.m_tris;

	    UInt32[] triIndices	        = new UInt32[numTris * 3 + 100]; // +100 is a sanity check


	    for (int i=0; i<numTris; i++)
	    {
		    for (int k=0; k<3; k++)
		    {
			    triIndices[i*3 + k] = tris[i].vertIndx[k];
		    }
	    }


	    for (int i=0; i<numVerts; i++)
	    {
	        vertex[i] = new stVertex();
	        
		    vertex[i].pos.X		= xMesh.m_verts[i].X;
		    vertex[i].pos.Y		= xMesh.m_verts[i].Y;
		    vertex[i].pos.Z		= xMesh.m_verts[i].Z;
		    vertex[i].norm      = new Vector3(0,0,0);
		    vertex[i].count     = 0;

		    if (numTexCoords>0)
		    {
			    Debug.Assert(numTexCoords==numVerts);
			    vertex[i].tex = xMesh.m_texCoords[i];
		    }
	    }

	    // We aren't doing per face waterHeights at the moment, so we just
	    // average the waterHeights across the vertices's from the faces
	    if (xMesh.m_numNormals>0)
	    for (int i=0; i<numTris; i++)
	    {
		    for (int k=0; k<3; k++)
		    {
			    Debug.Assert(xMesh.m_numNormals == numTris);

			    int faceCorner	= (int)tris[i].vertIndx[k];
			    Vector3 normVec	= tris[i].waterHeights[k];

			    Debug.Assert(faceCorner>=0 && faceCorner<numVerts);
			    vertex[faceCorner].norm += normVec;
			    vertex[faceCorner].count++;
			    
			    Debug.Assert(vertex[faceCorner].count<MAX_BONE_WEIGHTS);
		    }
	    }

        if (xMesh.m_numNormals>0)
	    for (int i=0; i<numVerts; i++)
	    {
		    if (vertex[i].count>0)
		    {
			    vertex[i].norm *= 1.0f/(vertex[i].count);
			    vertex[i].count = 0;
		    }
	    }


        int numSkinWeights = xMesh.m_skinWeights.Count;
        for (int ss=0; ss<numSkinWeights; ss++)
        {
	        stXSkinWeights skinWeights = xMesh.m_skinWeights[ss];
		    string skinName = skinWeights.m_name;

		    stXFrame xTopFrame = xFile.m_frame;

		    stXFrame xFrameFound = FindFrame(xTopFrame, skinName);
            Debug.Assert(xFrameFound != null);
            if (xFrameFound == null)
		    {
			    continue;
		    }

            Matrix matFrame  = xFrameFound.m_matAnim;


		    int[] indexes    = skinWeights.m_indexes;
		    float[] weights  = skinWeights.m_weights;
            Matrix   matSkin = skinWeights.m_mat;


    #if true // Draw Bones and them animating
		    {
		    Matrix  matBone   = xFrameFound.m_matAnim;
		    Vector3 bonePosF  = new Vector3(matBone.M41, matBone.M42, matBone.M43);
		    gameDraw.DrawCross3D(bonePosF, Color.Red);

            for (int kk=0; kk<xFrameFound.m_child.Count; kk++)
            {
                stXFrame childs    = xFrameFound.m_child[kk];
			    Matrix   childMat  = childs.m_matAnim;
			    Vector3  bonePosTo = new Vector3(childMat.M41, childMat.M42, childMat.M43);
			    
                gameDraw.DrawLine3D(bonePosF, bonePosTo, Color.Red);
		    }
		    }
    #endif


    #if false // Static Default Pose of Bones - Force animation to always be the first frame
	        {
	        Matrix matBone    = xFrameFound.m_matTrans;
	        Vector3 bonePosF  = new Vector3(matBone.M41, matBone.M42, matBone.M43);
	        gameDraw.DrawCross3D(bonePosF, Color.Red);

	        for (int kk=0; kk<xFrameFound.m_child.Count; kk++)
            {
                stXFrame childs     = xFrameFound.m_child[kk];
		        Matrix   childMat   = childs.m_matTrans;
		        Vector3  bonePosTo  = new Vector3(childMat.M41, childMat.M42, childMat.M43);
			    
                gameDraw.DrawLine3D(bonePosF, bonePosTo, Color.Purple);
	        }
	        }
    #endif


		    for (int i=0; i<skinWeights.m_numIndexes; i++)
		    {
			    int   index      = indexes[i];
			    float weight     = weights[i];

			    Debug.Assert(index>=0 && index<numVerts);
    			

			    Debug.Assert(vertex[index].count<MAX_BONE_WEIGHTS);

			    Matrix matTrans = (matSkin * matFrame);

			    vertex[index].weight[vertex[index].count] = weight;
                vertex[index].skinPos[vertex[index].count] = Vector3.Transform(vertex[index].pos , matTrans);
			    vertex[index].count++;
		    }

	    }


	    for (int i=0; i<numVerts; i++)
	    {
		    int numWeights = vertex[i].count;
		    if (numWeights==0)
		    {
			    vertex[i].pos = Vector3.Transform(vertex[i].pos, xFrame.m_matAnim);
		    }
		    else
		    {
                Vector3 pos = new Vector3(0, 0, 0);
			    for (int k=0; k<numWeights; k++)
			    {
				    pos += vertex[i].skinPos[k] * vertex[i].weight[k];
			    }

			    vertex[i].pos = pos;
		    }
	    }



    #if false // Draw Normals
	    for (int i=0; i<numVerts; i++)
	    {
	        gameDraw.DrawLine3D(vertex[i].pos, 
						        vertex[i].pos + vertex[i].norm,
						        Color.Red);
	    }
    #endif


    #if true
	    for (int i=0; i<numTris; i++)
	    {
		    Debug.Assert(triIndices[i]>=0 && (int)triIndices[i]<numVerts);
	    }
    #endif
 

        XTris xTris = new XTris();
            
        stMaterialManager matManager = xFile.m_materialManager;
	    if (xMesh.m_matList!=null)
	    {
		    stXMaterialList matList = xMesh.m_matList;
		    int numFaces  = matList.m_numIndexes;
		    int[] matIds  = matList.m_indexes;
		    Debug.Assert(numFaces==numTris);
		    
		    
		    for (int i=0; i<numFaces; i++)
		    {
			    int matId = matIds[i];
			    Debug.Assert(matId>=0 && matId<1000); // Sanity check
			    xTris.AddTri(i, matId);
			}
		}
		else
		{
		    for (int i=0; i<numTris; i++)
		    {
			    xTris.AddTri(i, -1);
			}
		}
			    
			    
        gameDraw.graphics.GraphicsDevice.RenderState.FillMode = FillMode.Solid;
        gameDraw.graphics.GraphicsDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

        s_materialSwaps += xTris.m_tris.Count;
        
    #if true // Draw the object mesh-skin
        for (int i=0; i<xTris.m_tris.Count; i++)
        {
            Texture2D curTexture = null;
            int matId = xTris.m_tris[i].m_matId;
            if (matId>-1)
            {
                stXMaterial xMat = matManager.GetMaterial(matId);
                Debug.Assert(xMat!=null);
                curTexture = xMat.m_texture;
            }
            
            for (int vv=0; vv<xTris.m_tris[i].m_indices.Count; vv++)
            {
                UInt32 ii0 = triIndices[vv*3 + 0];
                UInt32 ii1 = triIndices[vv*3 + 1];
                UInt32 ii2 = triIndices[vv*3 + 2];
			    
			    if (curTexture!=null)
			    {
			        VertexPositionTexture[] vpt = new VertexPositionTexture[3];
			        
			        vpt[0] = new VertexPositionTexture(vertex[ii0].pos, vertex[ii0].tex);
                    vpt[1] = new VertexPositionTexture(vertex[ii1].pos, vertex[ii1].tex);
                    vpt[2] = new VertexPositionTexture(vertex[ii2].pos, vertex[ii2].tex);
			        gameDraw.DrawTexTriangle3D( vpt,
			                                    curTexture);
			    }
			    else
			    {
			        gameDraw.DrawTriangle3D( vertex[ii0].pos,
			                                 vertex[ii1].pos,
			                                 vertex[ii2].pos, 
			                                 Color.Gray);
                }
            }
        }
    #endif

	    triIndices = null;
	    vertex     = null;

    }




};
*/


