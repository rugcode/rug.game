// http://www.xbdev.net/xna/skinnedmodels/index.php

using Rug.Game;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

public class stGUID
{
	/*
		<F1CFE2B3-0DE3-4e28-AFA1-155A750A282D>
		e.g. stGUID("test", F1CFE2B3, 0DE3, 4e28, AF,A1,15,5A,75,0A,28,2D);
	*/
	public stGUID( UInt32  l, 
		           UInt16 w1, UInt16 w2, 
		           byte  b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8)
	{
		m_data1 = l;
		m_data2 = w1;	m_data3 = w2;
		m_data4[0]=b1;
		m_data4[1]=b2;
		m_data4[2]=b3;
		m_data4[3]=b4;
		m_data4[4]=b5;
		m_data4[5]=b6;
		m_data4[6]=b7;
		m_data4[7]=b8;
	}
	
	public stGUID() {m_data1=0; m_data2=0; m_data3=0; /*m_data4={0,0,0,0,0,0,0,0};*/}
    public UInt32   m_data1;
    public UInt16   m_data2;
    public UInt16   m_data3;
    public byte[]   m_data4 = new byte[8];
};

public class stXFileData
{
    public stXFileData() { m_name = ""; m_data = null; m_reference = false; m_guid = null; }
	public stGUID   GetGUID() { return m_guid; }
    public string   GetName() { return m_name; }
    public object   GetData() { return m_data; }
    public stGUID   m_guid;
	public string	m_name;
    public object   m_data;
    public bool     m_reference;
};

public class stTemplateBase
{
	public enum TemplateType
	{
		TemplateUnknown,
		TemplateVariable,
		TemplateRestrictions,
		TemplateArray,
		TemplateData,
		TemplateItem,
	};

	~stTemplateBase(){}
	public virtual TemplateType GetTemplateType()                                           { Debug.Assert(false); return TemplateType.TemplateUnknown; }
    public virtual void         ParseTemplateHeader(TokenParser tokenParser)                { Debug.Assert(false); }
    public virtual int          ReadTemplateData(TokenParser tokenParser, List<byte> data)  { Debug.Assert(false); return 0; }
};


public class stTemplateVariable : stTemplateBase
{
	public enum VariableType
	{
		VariableUnknown,
		VariableTemplate,
		VariableWord,
		VariableDword,
		VariableFloat,
		VariableDouble,
		VariableChar,
		VariableUChar,
		VariableByte,
		VariableString,
		VariableCString,
		VariableUnicode
	};

    public override TemplateType GetTemplateType() { return TemplateType.TemplateVariable; }
    public          VariableType GetVariableType() { return m_variableType; }

	//stTemplateVariable(stXFile* xFile, char* varType, const char* varName)
	~stTemplateVariable() {}
	
	public stTemplateVariable(stXFile xFile, string varType, string varName)
	{
	    Debug.Assert(xFile!=null);
	    Debug.Assert(varType!=null);
	    int len = varName.Length;
	    Debug.Assert(len>0 && len<128);
	    m_variableName = varName;

	    m_templateVariable = null;
	    m_variableType	   = VariableType.VariableUnknown;

	    if		( varType.ToUpper() == "WORD"	) { m_variableType=VariableType.VariableWord;		}
        else if ( varType.ToUpper() == "DWORD"  ) { m_variableType = VariableType.VariableDword; }
        else if ( varType.ToUpper() == "FLOAT"  ) { m_variableType = VariableType.VariableFloat; }
        else if ( varType.ToUpper() == "DOUBLE" ) { m_variableType = VariableType.VariableDouble; }
        else if ( varType.ToUpper() == "CHAR"   ) { m_variableType = VariableType.VariableChar; }
        else if ( varType.ToUpper() == "UCHAR"  ) { m_variableType = VariableType.VariableUChar; }
        else if ( varType.ToUpper() == "BYTE"   ) { m_variableType = VariableType.VariableByte; }
        else if ( varType.ToUpper() == "STRING" ) { m_variableType = VariableType.VariableString; }
        else if ( varType.ToUpper() == "CSTRING") { m_variableType = VariableType.VariableCString; }
        else if ( varType.ToUpper() == "UNICODE") { m_variableType = VariableType.VariableUnicode; }
	    else
	    {
		    m_variableType=VariableType.VariableTemplate;
		    m_templateVariable = xFile.FindTemplate(varType);
		    Debug.Assert(m_templateVariable!=null);
	    }
	}
	
	public override int ReadTemplateData(TokenParser tokenParser, List<byte> data)
	{
	    int bytesRead = 0;
	    stToken token = tokenParser.GetToken();
	    Debug.Assert(token!=null);
	    
	    // ????????? Array.Reverse(b);
	    int startSize = data.Count;

	    switch (m_variableType)
	    {
            case VariableType.VariableTemplate:
		    {
			    Debug.Assert(m_templateVariable!=null);
                bytesRead += m_templateVariable.ReadTemplateData(tokenParser, data);
		    }
		    break;

            case VariableType.VariableWord:
		    {
			    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenNumber);
			    ushort val = System.Convert.ToUInt16(token.m_data);
			    byte[] b = BitConverter.GetBytes(val); 
			    Debug.Assert(val<=0xffff);
                bytesRead += 2;
			    data.AddRange(b);
		    }
		    break;

            case VariableType.VariableDword:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenNumber);
			    UInt32 val = System.Convert.ToUInt32(token.m_data);
			    byte[] b = BitConverter.GetBytes(val);
			    bytesRead += 4;
                data.AddRange(b);
		    }
		    break;

            case VariableType.VariableFloat:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenNumber);
			    float val = System.Convert.ToSingle(token.m_data);
			    byte[] b = BitConverter.GetBytes(val);
			    bytesRead += 4;
                data.AddRange(b);
		    }
		    break;

            case VariableType.VariableDouble:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenNumber);
			    double val = System.Convert.ToDouble(token.m_data);
			    byte[] b = BitConverter.GetBytes(val);
			    bytesRead += 8;
                data.AddRange(b);

			    Debug.Assert(false); // To Check
		    }
		    break;

            case VariableType.VariableChar:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenNumber);
			    char val = System.Convert.ToChar(token.m_data);
			    byte[] b = BitConverter.GetBytes(val);
			    bytesRead += 1;
                data.AddRange(b);

			    Debug.Assert(false); // To Check
		    }
		    break;

            case VariableType.VariableUChar:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenNumber);
			    byte val = System.Convert.ToByte(token.m_data);
			    byte[] b = BitConverter.GetBytes(val);
			    bytesRead += 1;
                data.AddRange(b);

			    Debug.Assert(false); // To Check
		    }
		    break;

            case VariableType.VariableByte:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenNumber);
                byte val = System.Convert.ToByte(token.m_data);
                byte[] b = BitConverter.GetBytes(val);
                bytesRead += 1;
                data.AddRange(b);

			    Debug.Assert(false); // To Check
		    }
		    break;

            case VariableType.VariableString:
		    {
		        // Debug
                //byte[] byteArray = Encoding.ASCII.GetBytes("abce");
                //string sssret = Encoding.ASCII.GetString(byteArray);

		        
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenString);
			    Debug.Assert(token.m_data.Length>1);
                //byte val = System.Convert.ToByte(token.m_data);
                //byte[] b = BitConverter.GetBytes(val);
                byte[] b = Encoding.ASCII.GetBytes(token.m_data);
                bytesRead += token.m_data.Length + 1;
                data.AddRange(b);
                data.Add(0);
                
                //Debug.Assert(false); // Check the null pointer byte
		    }
		    break;

            case VariableType.VariableCString:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenString);
                Debug.Assert(token.m_data.Length > 1);
                byte val = System.Convert.ToByte(token.m_data);
                byte[] b = BitConverter.GetBytes(val);
                bytesRead += token.m_data.Length;
                data.AddRange(b);

                Debug.Assert(false); // Check the null pointer byte and lstring check!
		    }
		    break;

            case VariableType.VariableUnicode:
		    {
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenString);
                Debug.Assert(token.m_data.Length > 1);
                byte val = System.Convert.ToByte(token.m_data);
                byte[] b = BitConverter.GetBytes(val);
                bytesRead += token.m_data.Length;
                data.AddRange(b);

                Debug.Assert(false); // Check the null pointer byte and lstring check!
		    }
		    break;

		    default: { Debug.Assert(false); } break;
	    }

	    // TemplateVariables already read in the next
	    // token and have it waiting.
        if (m_variableType != VariableType.VariableTemplate)
	    {
		    token = tokenParser.GetNextToken();
		    Debug.Assert(token!=null);
	    }

        Debug.Assert(bytesRead == (data.Count-startSize));
        
	    return bytesRead;
	}

	public stTemplateBase       m_templateVariable;
	public VariableType			m_variableType;
	public string			    m_variableName;
};


public class stTemplateArray : stTemplateBase
{
    public override TemplateType GetTemplateType() { return TemplateType.TemplateArray; }

	public stTemplateArray(stTemplateVariable templateVariable, string varName, string arrayName)
	{
		Debug.Assert(arrayName.Length>0);
		m_arrayVarName = arrayName;
		m_fixedSize  = -1;
		m_lookUpSize = -1;
		m_templateVariable = templateVariable;
		Debug.Assert(m_templateVariable!=null);
	}
	
	public stTemplateArray(stTemplateVariable templateVariable, string varName, int fixedSize)
	{
		m_arrayVarName = "";
		m_fixedSize = fixedSize;
		Debug.Assert(fixedSize>0 && fixedSize<10000);
		m_lookUpSize = -1;
		m_templateVariable = templateVariable;
		Debug.Assert(m_templateVariable!=null);
	}

	~stTemplateArray()
	{
	    m_templateVariable = null;
	}

    public override int ReadTemplateData(TokenParser tokenParser, List<byte> data)
	{
		Debug.Assert(m_templateVariable!=null);

		if (m_fixedSize<0 ) { Debug.Assert(m_lookUpSize>-1); }
		if (m_fixedSize>-1) { Debug.Assert(m_lookUpSize<0);  }


		int numItems = 0;
		if (m_fixedSize>-1)  numItems=m_fixedSize;
		if (m_lookUpSize>-1) numItems=m_lookUpSize;
		int bytesRead = 0;

		Debug.Assert(numItems>=0 && numItems<99999); // Sanity check
        stToken token = null;
        
		for (int i=0; i<numItems; i++)
		{
			bytesRead += m_templateVariable.ReadTemplateData(tokenParser, data);

			if (i==numItems-1)
			{
				token = tokenParser.GetToken();
                Debug.Assert(token != null);
				Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenSemiColon);
			}
			else
			{
				token = tokenParser.GetToken();
				Debug.Assert(token!=null);
                Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenComma);

				token = tokenParser.GetNextToken();
                Debug.Assert(token != null);
			}
		}

		Debug.Assert(numItems>0);   // Could have a problem here!...0 items?
		                            // Will continue okay, but you have some arrays of size 0 in your .r file

		token = tokenParser.GetToken();
		Debug.Assert(token!=null);
        Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenSemiColon);

		token = tokenParser.GetNextToken();
        Debug.Assert(token!=null);

		return bytesRead;
	}

    public stTemplateVariable   m_templateVariable;
    public string               m_arrayVarName;
    public int                  m_lookUpSize;
	public int					m_fixedSize;
};

public class stTemplateRestrictions : stTemplateBase
{
	public override TemplateType GetTemplateType() { return TemplateType.TemplateRestrictions; }
	public List<stTemplateBase>	m_templateItems = new List<stTemplateBase>(); // Open(stTemplate),Closed(size 0),Restricted(list restricted stTemplates)
};


public class stArrayLookUp
{
	public stArrayLookUp() { varName=""; value=0; }
	public string			varName;
	public UInt32	        value;
};

public class stTemplateItem : stTemplateBase
{
	public override TemplateType GetTemplateType() { return TemplateType.TemplateItem; }

	public stTemplateItem(string name, stGUID guid)
	{
		Debug.Assert(name.Length>1);
		m_name = name;

		if (guid!=null)
		{
			m_guid = guid;
		}
	}
	~stTemplateItem()
	{
	    m_vars = null;
	}

	public override void ParseTemplateHeader(TokenParser tokenParser)	{ Debug.Assert(false); }
	
	public override int ReadTemplateData(TokenParser tokenParser, List<byte> data)
	{
		int startSize = data.Count;
		int bytesRead = 0;

	    /*
	    Each time a WORD or DWORD variable is read, its stored in the list so if our
	    ArrayVariable needs it, we can look it up
	    */
	    List< stArrayLookUp > arrayLookUp = new List< stArrayLookUp >();
	    //arrayLookUp.Resize(m_vars.Size());

	    for (int i=0; i<m_vars.Count; i++)
	    {
            arrayLookUp.Add(new stArrayLookUp());
            
		    switch (m_vars[i].GetTemplateType())
		    {
			    /*
			    TemplateVariables and TemplateArrays do the same thing, but the array one
			    needs extra stuff if the array size is specified earlier in TemplateVariable
			    */
			    case TemplateType.TemplateVariable:
			    {
				    stTemplateVariable templateVariable = (stTemplateVariable)m_vars[i];
				    Debug.Assert(templateVariable!=null);

				    int startDataSize = data.Count;
				    bytesRead += templateVariable.ReadTemplateData(tokenParser, data);
				    Debug.Assert(data.Count > startDataSize);

				    stToken token = tokenParser.GetToken();
				    Debug.Assert(token!=null);

				    token = tokenParser.GetNextToken();
				    Debug.Assert(token!=null);


				    if (templateVariable.GetVariableType()==stTemplateVariable.VariableType.VariableDword ||
					    templateVariable.GetVariableType()==stTemplateVariable.VariableType.VariableWord )
				    {
					    arrayLookUp[i].varName = templateVariable.m_variableName;
					    Debug.Assert(arrayLookUp[i].varName.Length>1);

					    switch (templateVariable.GetVariableType())
					    {
						    case stTemplateVariable.VariableType.VariableDword:
						    {
							    arrayLookUp[i].value = BitConverter.ToUInt32(data.ToArray(), startDataSize);
							    Debug.Assert(arrayLookUp[i].value>=0 && arrayLookUp[i].value<=0xffffffff);
						    }
						    break;

						    case stTemplateVariable.VariableType.VariableWord:
						    {
						        arrayLookUp[i].value = (UInt32)BitConverter.ToUInt16(data.ToArray(), startDataSize);
							    Debug.Assert(arrayLookUp[i].value>=0 && arrayLookUp[i].value<=0xffff);
						    }
						    break;

						    default: {Debug.Assert(false);} break;
					    }
				    }
			    }
			    break;

			    case TemplateType.TemplateArray:
			    {
				    stTemplateArray templateArray = (stTemplateArray)m_vars[i];
				    Debug.Assert(templateArray!=null);

				    if (templateArray.m_fixedSize<0)
				    {
					    string varName = templateArray.m_arrayVarName;
					    Debug.Assert(varName.Length>1);
					    bool foundLookUp = false;
					    for (int k=0; k<arrayLookUp.Count; k++)
					    {
						    if (arrayLookUp[k].varName.Length>1)
						    {
							    if (arrayLookUp[k].varName==varName)
							    {
								    templateArray.m_lookUpSize = (Int32)arrayLookUp[k].value;
								    Debug.Assert(templateArray.m_lookUpSize>-1 && templateArray.m_lookUpSize<100000);
								    foundLookUp = true;
								    break;
							    }
						    }
					    }
					    Debug.Assert(foundLookUp);
				    }

				    bytesRead += templateArray.ReadTemplateData(tokenParser, data);

				    stToken token = tokenParser.GetToken();
				    Debug.Assert(token!=null);
			    }
			    break;

			    case TemplateType.TemplateRestrictions:
			    {
			    }
			    break;

			    default: {Debug.Assert(false);} break;
		    }
	    }

	    Debug.Assert(bytesRead == data.Count-startSize);
	    return bytesRead;
	}

	public string				m_name;
    public stGUID               m_guid;
    public List<stTemplateBase> m_vars = new List<stTemplateBase>();
};


public class stXFile
{
	public stXFile()
	{
	    m_templates = new List< stTemplateItem >();
	    m_xParser   = null;
	    
        //m_templates.Resize(MAX_TEMPLATES);
        //for (int i=0; i<m_templates.Size(); i++) { m_templates[i]=NULL; }
        //m_numTemplates = 0;
        //m_xParser = NULL;
	}
	~stXFile()
	{
	    m_templates = null;
        //for (int i=0; i<m_numTemplates; i++)
        //{
        //    DBG_ASSERT(m_templates[i]);
        //    delete m_templates[i];
        //    m_templates[i] = NULL;
        //}
	}

	public void AddTemplate(stTemplateBase newTemplate)
	{
		Debug.Assert(newTemplate!=null);
		Debug.Assert(newTemplate.GetTemplateType()==stTemplateBase.TemplateType.TemplateItem ||
                     newTemplate.GetTemplateType() == stTemplateBase.TemplateType.TemplateData);

		if (!Exists(newTemplate))
		{
			m_templates.Add( (stTemplateItem)newTemplate );
		}
	}

	public bool Exists(stTemplateBase newTemplate)
	{
		Debug.Assert(newTemplate!=null);
		Debug.Assert(newTemplate.GetTemplateType()==stTemplateBase.TemplateType.TemplateItem ||
				     newTemplate.GetTemplateType()==stTemplateBase.TemplateType.TemplateData);
		string newTempName = ((stTemplateItem)newTemplate).m_name;
		Debug.Assert(newTempName.Length>1);

		for (int i=0; i<m_templates.Count; i++)
		{
			Debug.Assert(m_templates[i]!=null);
			string existingTempName = m_templates[i].m_name;

			if (newTempName == existingTempName)
			{
				return true;
			}
		}
		return false;
	}

	public stTemplateBase FindTemplate(string nameTemp)
	{
		for (int i=0; i<m_templates.Count; i++)
		{
			stTemplateItem templateItem = m_templates[i];
			Debug.Assert(templateItem!=null);
			Debug.Assert(templateItem.GetTemplateType()==stTemplateBase.TemplateType.TemplateItem ||
					     templateItem.GetTemplateType()==stTemplateBase.TemplateType.TemplateData);
			string templateName = templateItem.m_name;
			Debug.Assert(templateName.Length>1);

			if (templateName == nameTemp)
			{
				return templateItem;
			}
		}
		return null;
	}

	public stTemplateItem	GetTemplateItem(int indx)   { return m_templates[indx]; }
	public int				GetNumTemplateItems()       { return m_templates.Count; }

	public stXParser		m_xParser = null;

	private List< stTemplateItem >  m_templates;

};



public class stXParser
{
	public stXParser(){ m_parent=null; m_depth=0; m_checkBaseIsCalled=0; }

	stXFileData 	m_parent;
	int	            m_depth;
	string		    m_fileName;
	int				m_checkBaseIsCalled;

	private void ParseChildObjects(stXFileData pObjData,
						           int depth)
	{
		m_parent = pObjData;
		m_depth  = depth+1;
		m_checkBaseIsCalled--;
	}


    public void StartParsingXFile(string fileName)
    {
        StartParsingXFile(fileName, null);
    }
    
	public void StartParsingXFile(string fileName, stXFileData objData)
	{
		m_fileName	= fileName;
		m_parent    = objData;

		stXFile xFile = new stXFile();
		xFile.m_xParser = this;
		ParseXTxtFile(xFile, fileName);
		xFile = null;
	}

	public virtual void ParseObject( ref stXFileData pParentObjData,
							         ref stXFileData pObjData,
							         int depth,
							         List<byte> data)
	{
		ParseChildObjects(pObjData, depth);
	}


    static void ParseXTxtFile(stXFile xFile, string fileName)
    {
	    Debug.Assert(xFile!=null);
	    Debug.Assert(fileName!=null);

	    // Read "Common & Standard" Template Defines from our internal .r file list
	    ReadTemplateList(xFile);

	    // Parse the .r File
        TokenParser tokenParser = new TokenParser(fileName);

	    // Check for the xof starting string, and strip it off
	    ReadXOF(tokenParser);

	    // Keep parsing the .r file till we've done it all
	    while (tokenParser.GetNextToken()!=null)
	    {
		    ParseData(tokenParser, xFile);
	    }
    }
    
    static void ReadTemplateList(stXFile xFile)
    {
	    /*
	    Reads in a .r file which is just a big list of all the common
	    template definitions, so we have all the templates incase the .r doesn't
	    include the default ones.
	    */
	    Debug.Assert(xFile!=null);

        TokenParser tokenParser = new TokenParser(Helper.ResolvePath("~/Data/Assets/templates.x"));

	    while (tokenParser.GetNextToken()!=null)
	    {
		    ParseData(tokenParser, xFile);
	    }
    }

    static void ReadXOF(TokenParser tokenParser)
    {
	    /*
	    xof 0303txt 0064
	    */

	    stToken token = null;

	    token = tokenParser.GetNextToken(); //"xof"
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);
	    Debug.Assert(token.m_data.Length>0);
	    Debug.Assert(token.m_data.ToLower()=="xof");

	    token = tokenParser.GetNextToken(); //"0303txt"
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);

	    token = tokenParser.GetNextToken(); //"0064"
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenNumber);
    }
    
    
    static void ParseData(TokenParser tokenParser, stXFile xFile)
    {
        Debug.Assert(xFile!=null);
	    stToken token = tokenParser.GetToken();
	    Debug.Assert(token!=null);

	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString ||  // Template or TemplateInstance
		             token.GetTokenType()==stToken.TokenType.TokenOpenB);    // Data Refernce { scene_etc }

	    while (token.GetTokenType()!=stToken.TokenType.TokenCloseB)
	    {
		    // Data Reference Object (can have multiple ones)
		    // e.g. { scene_root } 
		    //      { scene_2 }
		    if (token.GetTokenType()==stToken.TokenType.TokenOpenB)
		    {
    			
			    //const int memBufSize = 1024;
			    //unsigned char* data = new unsigned char[memBufSize];
			    //memset(data, 0xff, memBufSize);
			    List<byte> data = new List<byte>();
    			

			    ReadXDATAREFERENCE(tokenParser, xFile, data);

			    //#ifdef ENABLE_PARSECALLBACK
			    // Setup callback system so our read in data can be used anywhere
			    Debug.Assert(xFile.m_xParser!=null);
			    stXParser xParser		   = xFile.m_xParser;
			    stXFileData  originalData  = xParser.m_parent;
			    int originalDepth          = xParser.m_depth;
			    stXFileData thisData = new stXFileData();
                thisData.m_name         = ReadSTRING.Read(data, 0);
			    //thisData.m_name			= Encoding.ASCII.GetString(data.ToArray());
			    thisData.m_guid			= new stGUID(); //new stGUID(0,1,2,3,4,5,6,7,8,9,0);
			    thisData.m_reference	= true;  // only difference to what is below
			    xParser.m_checkBaseIsCalled = 1;
			    xParser.ParseObject( ref xParser.m_parent, 
								     ref thisData, 
								     xParser.m_depth, 
								     data);

			    // Check that the ParseChildObject has been called from within
			    // ParseObject (also make sure its only called once)
			    Debug.Assert(xParser.m_checkBaseIsCalled==0);

			    xParser.m_parent = originalData;
			    xParser.m_depth  = originalDepth;
			    //#endif //ENABLE_PARSECALLBACK
    			
			    data = null;
    			

			    continue;
		    }

		    string strTemplateName = token.m_data;
		    Debug.Assert(strTemplateName.Length>1);


		    if ("template" == strTemplateName)
		    {
			    ReadXTEMPLATE(tokenParser, xFile);
		    }
		    else
		    {
			    stTemplateBase templateBase = xFile.FindTemplate(strTemplateName);
			    Debug.Assert(templateBase!=null);
			    Debug.Assert(templateBase.GetTemplateType()==stTemplateBase.TemplateType.TemplateItem);
			    stTemplateItem templateItem = (stTemplateItem)templateBase;

    			
			    token = tokenParser.GetNextToken();
			    Debug.Assert(token!=null);
			    string objName = "";

			    switch (token.GetTokenType())
			    {
				    case stToken.TokenType.TokenString:
				    {
					    string name = token.m_data;
					    Debug.Assert(name.Length>1 && name.Length<128);
					    objName = name;

					    token = tokenParser.GetNextToken();
					    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenOpenB);
				    }
				    break;

				    case stToken.TokenType.TokenOpenB:
				    {
				    }
				    break;

				    default: { Debug.Assert(false); } break;
			    }

			    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenOpenB);

    			
			    token = tokenParser.GetNextToken();
			    Debug.Assert(token!=null);
    			

    			
			    // Raw Template Data Here
			    //const int memBufSize = 1024*1024*5; // 5 Meg!
			    //unsigned char* data = new unsigned char[memBufSize];
			    //memset(data, 0xff, memBufSize);
			    List<byte> data = new List<byte>();

			    int totalSizeData = templateItem.ReadTemplateData(tokenParser, data);

			    // Maybe put a specific mark at the end of the data?...i.e. "0xE0DABCDE"

			    //#ifdef ENABLE_PARSECALLBACK
			    // Setup callback system so our read in data can be used anywhere
			    Debug.Assert(xFile.m_xParser!=null);
			    stXParser xParser		   = xFile.m_xParser;
			    stXFileData originalData   = xParser.m_parent;
			    int originalDepth          = xParser.m_depth;
			    stXFileData thisData = new stXFileData();
			    thisData.m_name = objName;
			    thisData.m_guid = templateItem.m_guid;
			    xParser.m_checkBaseIsCalled = 1;
			    xParser.ParseObject( ref xParser.m_parent, 
								     ref thisData, 
								     xParser.m_depth, 
								     data);

			    // Check that the ParseChildObject has been called from within
			    // ParseObject (also make sure its only called once)
			    Debug.Assert(xParser.m_checkBaseIsCalled==0);
			    //#endif //ENABLE_PARSECALLBACK


			    token = tokenParser.GetToken();
			    Debug.Assert(token!=null);
    			

			    if (token.GetTokenType()==stToken.TokenType.TokenSemiColon)
			    {
				    Debug.Assert(false); // Should only have 1 semi-colon at the end of the array
				    token = tokenParser.GetNextToken();
			    }


			    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString || 
					         token.GetTokenType()==stToken.TokenType.TokenCloseB ||
					         token.GetTokenType()==stToken.TokenType.TokenOpenB);

			    switch (token.GetTokenType())
			    {
				    case stToken.TokenType.TokenString:
				    case stToken.TokenType.TokenOpenB:
				    {
					    ParseData(tokenParser, xFile);
				    }
				    break;

				    case stToken.TokenType.TokenCloseB:
				    {
				    }
				    break;

				    default: { Debug.Assert(false); } break;
			    }

			    //#ifdef ENABLE_PARSECALLBACK
			    xParser.m_parent = originalData;
			    xParser.m_depth  = originalDepth;
			    //#endif //ENABLE_PARSECALLBACK
			    data = null;
    			

			    token = tokenParser.GetNextToken();
			    if (token==null)
			    {
				    break;
			    }

			    Debug.Assert(token!=null);
			    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString || 
				             token.GetTokenType()==stToken.TokenType.TokenCloseB ||
					         token.GetTokenType()==stToken.TokenType.TokenOpenB );

		    }
	    }
    }


    static stGUID ReadGUID(string str)
    {
	    Debug.Assert(str.Length>1);
	    // <10DD46A3-775B-11cf-8F52-0040333594A3>
	    Debug.Assert(str.Length>7 && str.Length<39);
	    Debug.Assert(str[0]=='<');
	    Debug.Assert(str[str.Length-1]=='>');

        //Debug.Assert(false); // To-Do
        UInt32 a,b,c;
        UInt32 x0,x1;
        UInt32 y0,y1,y2,y3,y4,y5; 
	    //sscanf(str, "<%8x-%4x-%4x-%2x%2x-%2x%2x%2x%2x%2x%2x>", &a, &b, &c, &x0, &x1, &y0,&y1,&y2,&y3,&y4,&y5);

        string guidStr = str;
        guidStr = guidStr.Remove(0, 1);
        guidStr = guidStr.Remove(guidStr.Length - 1, 1);

        string[] guidParts = guidStr.Split('-');
        Debug.Assert(guidParts.Length==5);


        a = UInt32.Parse(guidParts[0], System.Globalization.NumberStyles.HexNumber, null);
        b = UInt32.Parse(guidParts[1], System.Globalization.NumberStyles.HexNumber, null);
        c = UInt32.Parse(guidParts[2], System.Globalization.NumberStyles.HexNumber, null);
                

        string tmp;
        tmp = guidParts[3].Substring(0,2);
        x0 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);

        tmp = guidParts[3].Substring(2, 2);
        x1 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);

        tmp = guidParts[4].Substring(0, 2);     y0 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);
        tmp = guidParts[4].Substring(2, 2);     y1 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);
        tmp = guidParts[4].Substring(4, 2);     y2 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);
        tmp = guidParts[4].Substring(6, 2);     y3 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);
        tmp = guidParts[4].Substring(8, 2);     y4 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);
        tmp = guidParts[4].Substring(10,2);     y5 = UInt32.Parse(tmp, System.Globalization.NumberStyles.HexNumber, null);


	    // Some sanity checks
	    Debug.Assert(a>=0 && a<=0xFFFFFFFF);
	    Debug.Assert(b>=0 && b<=0xFFFF);
	    Debug.Assert(c>=0 && c<=0xFFFF);
	    UInt32[] vals = {x0,x1,y0,y1,y2,y3,y4,y5};
	    for (int i=0; i<8; i++)
	    {
		    Debug.Assert(vals[i]>=0 && vals[i]<=0xFF);
	    }
        
	    stGUID guid = new stGUID();

	    guid.m_data1 = a;
	    guid.m_data2 = (UInt16)b;
        guid.m_data3 = (UInt16)c;		
	    guid.m_data4[0] = (byte)x0;
        guid.m_data4[1] = (byte)x1;
        guid.m_data4[2] = (byte)y0;
        guid.m_data4[3] = (byte)y1;
        guid.m_data4[4] = (byte)y2;
        guid.m_data4[5] = (byte)y3;
        guid.m_data4[6] = (byte)y4;
        guid.m_data4[7] = (byte)y5;

	    return guid;
    }


    /******************************************************************************/

    static void ReadXTEMPLATE(TokenParser tokenParser, stXFile xFile)
    {
	    /*
	    e.g.
	    template Quaternion {
		     <10DD46A3-775B-11cf-8F52-0040333594A3>
		     FLOAT s;
		     Vector v;
		    }
	    */

	    Debug.Assert(xFile!=null);

	    stToken token = null;

    	
	    token = tokenParser.GetNextToken(); //Name Template
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);
	    Debug.Assert(token.m_data.Length>1);
	    string templateName = token.m_data;
	    Debug.Assert(templateName.Length>1 && templateName.Length<128);

    	

	    token = tokenParser.GetNextToken(); //Open Braces
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenOpenB);

	    token = tokenParser.GetNextToken(); // GUID
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);
	    string guidStr = token.m_data;
	    stGUID guid = ReadGUID(guidStr);
    	
	    stTemplateItem newTemplate = new stTemplateItem(templateName, guid);


	    token = tokenParser.GetNextToken(); //Template Type

	    while (token.GetTokenType()!=stToken.TokenType.TokenCloseB)
	    {
		    switch (token.GetTokenType())
		    {
			    case stToken.TokenType.TokenString:
			    {
				    Debug.Assert(token.m_data.Length>1);
				    Debug.Assert(token.m_data.Length>1 && token.m_data.Length<128);
				    string strType = token.m_data; // Find template of this type
    				

				    bool isArray = false;
				    if (strType.ToLower() == "array")
				    {
					    isArray = true;

					    token = tokenParser.GetNextToken();
					    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);
    					
					    Debug.Assert(token.m_data.Length>1);
					    Debug.Assert(token.m_data.Length>0 && token.m_data.Length<128);
					    strType = token.m_data;
				    }


				    token = tokenParser.GetNextToken(); // Template Variable Name
				    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);
				    Debug.Assert(token.m_data.Length>0);
				    Debug.Assert(token.m_data.Length>0 && token.m_data.Length<128);
				    string varName = token.m_data;


				    stTemplateBase templateVariable = new stTemplateVariable(xFile, strType, varName);
				    Debug.Assert(templateVariable!=null);


				    if (isArray)
				    {
					    token = tokenParser.GetNextToken();
					    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenOpenSquare);

					    token = tokenParser.GetNextToken();
					    Debug.Assert(token.m_data.Length>1);

					    switch (token.GetTokenType())
					    {
						    case stToken.TokenType.TokenNumber:
						    {
							    int arraySize = System.Convert.ToInt32(token.m_data);
							    Debug.Assert(arraySize>0 && arraySize<10000);

							    templateVariable = new stTemplateArray((stTemplateVariable)templateVariable, varName, arraySize);
							    Debug.Assert(templateVariable!=null);
						    }
						    break;

						    case stToken.TokenType.TokenString:
						    {
							    string arraySizeName = token.m_data;

							    int found = -1;
							    int numMembers = newTemplate.m_vars.Count;
							    for (int k=0; k<numMembers; k++)
							    {
								    stTemplateBase tempVar = newTemplate.m_vars[k];
								    Debug.Assert(tempVar!=null);

								    if (tempVar.GetTemplateType()==stTemplateBase.TemplateType.TemplateVariable)
								    {
									    string memberName = ((stTemplateVariable)tempVar).m_variableName;
									    if (arraySizeName == memberName)
									    {
										    found = k;
										    break;
									    }
								    }
							    }
							    Debug.Assert(found>-1);
    	
							    templateVariable = new stTemplateArray((stTemplateVariable)templateVariable, varName, arraySizeName);	
							    Debug.Assert(templateVariable!=null);
						    }
						    break;

						    default: {Debug.Assert(false);} break;
					    }

    				
					    // Only support single arrays at the moment!, check its only a
					    // single array here.
					    token = tokenParser.GetNextToken();
					    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenCloseSquare);
				    }

				    token = tokenParser.GetNextToken();
				    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenSemiColon);


				    Debug.Assert(templateVariable!=null);
				    newTemplate.m_vars.Add( templateVariable );

			    }
			    break;


			    // List of restrictions, default is closed (0 restrictions)
			    case stToken.TokenType.TokenOpenSquare:
			    {
				    token = tokenParser.GetNextToken();
				    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString ||
						         token.GetTokenType()==stToken.TokenType.TokenNumber);

				    string tokenStr = token.m_data;
				    Debug.Assert(tokenStr.Length>1);

				    stTemplateRestrictions tempRestrictions = new stTemplateRestrictions();

				    stTemplateBase baseTemp = (stTemplateBase)tempRestrictions;
				    Debug.Assert(baseTemp!=null);
				    newTemplate.m_vars.Add( baseTemp );

				    bool isOpen = ("..." == tokenStr);
				    if (isOpen)
				    {
					    tempRestrictions.m_templateItems.Add( null );
					    //tempRestrictions.m_templateItems[0] = NULL;

					    token = tokenParser.GetNextToken();
				    }
				    else
				    {

					    while (token.GetTokenType()!=stToken.TokenType.TokenCloseSquare)
					    {
						    // e.g.
						    // [Animation <3d82ab4f-62da-11cf-ab39-0020af71e433>]
						    // or
						    // [Material]

						    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenString);

						    string restTemplateName = token.m_data;
						    Debug.Assert(restTemplateName.Length>1);
						    if (restTemplateName[0]!='<')
						    {
							    stTemplateBase templateBase = xFile.FindTemplate(restTemplateName);
							    Debug.Assert(templateBase!=null);
						    }

						    token = tokenParser.GetNextToken();
						    switch (token.GetTokenType())
						    {
							    case stToken.TokenType.TokenString:
							    case stToken.TokenType.TokenCloseSquare:
							    {
							    }
							    break;

							    case stToken.TokenType.TokenComma:
							    {
								    token = tokenParser.GetNextToken();
								    Debug.Assert(false);
							    }
							    break;

							    default: {Debug.Assert(false);} break;
						    }
					    }
				    }

				    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenCloseSquare);

			    }
			    break;

			    default: {Debug.Assert(false);} break;
		    }

		    //Debug.Assert(token.GetType()==stToken.TokenType.TokenSemiColon);

		    token = tokenParser.GetNextToken(); //Template Type
	    }// While Loop


	    xFile.AddTemplate(newTemplate);

	    token = tokenParser.GetToken();
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenCloseB);
    }


    static void ReadXDATAREFERENCE(TokenParser tokenParser, stXFile xBase, List<byte> data)
    {
	    // e.g. { scene_root }
	    stToken token = tokenParser.GetToken();
	    Debug.Assert(token.GetTokenType()==stToken.TokenType.TokenOpenB);

	    token = tokenParser.GetNextToken();
	    Debug.Assert(token!=null);
        Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenString);

	    string tokenRefName = token.m_data;
	    Debug.Assert(tokenRefName.Length>1);

	    int len = tokenRefName.Length;
	    Debug.Assert(len>0 && len<1023);
        //byte val = System.Convert.ToByte(tokenRefName);
        //byte[] b = BitConverter.GetBytes(val);
        byte[] b = Encoding.ASCII.GetBytes(tokenRefName);
        data.AddRange(b);
        data.Add(0);

	    token = tokenParser.GetNextToken();
	    Debug.Assert(token!=null);
        Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenCloseB);

	    token = tokenParser.GetNextToken();
	    if (token!=null)
	    {
            Debug.Assert(token.GetTokenType() == stToken.TokenType.TokenOpenB ||
                         token.GetTokenType() == stToken.TokenType.TokenCloseB ||
                         token.GetTokenType() == stToken.TokenType.TokenString);
	    }
    }
};








