﻿using OpenTK;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;

namespace Rug.Game.Deferred.Data
{
	public class DynamicHexGrid : IResourceManager
	{
		private bool m_Disposed = true; 

		private VertexBuffer m_Buffer;
		private DynamicGridVert[] m_Verts;

		public VertexBuffer Buffer { get { return m_Buffer; } }

		public int Count { get { return m_Verts.Length; } }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

		public DynamicHexGrid(int size)
		{
			var xsize = (float)size;
			var ysize = (float)size;
			var width = 800f;
			var height = 800f;
			var hw = width / 2f;
			var hh = height / 2f;

			List<Vector2> position_2f = new List<Vector2>();
			List<Vector2> texcoord_2f = new List<Vector2>();
			List<Vector3> barycentric_3f = new List<Vector3>();

			for (var x = 0; x <= xsize; x++)
			{
				var x1 = Clamp((x - 0.5f) / xsize, 0, 1);
				var x2 = Clamp((x + 0.0f) / xsize, 0, 1);
				var x3 = Clamp((x + 0.5f) / xsize, 0, 1);
				var x4 = Clamp((x + 1.0f) / xsize, 0, 1);

				for (var y = 0; y < ysize; y += 2)
				{
					var t = (y + 0) / ysize;
					var m = (y + 1) / ysize;
					var b = (y + 2) / ysize;
					
					position_2f.Add(new Vector2(x1 * width - hw, t * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, t * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, t * height - hh));
					position_2f.Add(new Vector2(x4 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x1 * width - hw, b * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, b * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x4 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, b * height - hh));					

					
					texcoord_2f.Add(new Vector2(x1, t)); 
					texcoord_2f.Add(new Vector2(x3, t)); 
					texcoord_2f.Add(new Vector2(x2, m));
					texcoord_2f.Add(new Vector2(x2, m)); 
					texcoord_2f.Add(new Vector2(x3, t)); 
					texcoord_2f.Add(new Vector2(x4, m));
					texcoord_2f.Add(new Vector2(x1, b)); 
					texcoord_2f.Add(new Vector2(x2, m)); 
					texcoord_2f.Add(new Vector2(x3, b));
					texcoord_2f.Add(new Vector2(x2, m)); 
					texcoord_2f.Add(new Vector2(x4, m));
					texcoord_2f.Add(new Vector2(x3, b));
										
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0)); 
					barycentric_3f.Add(new Vector3(0, 0, 1));
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0)); 
					barycentric_3f.Add(new Vector3(0, 0, 1));
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0)); 
					barycentric_3f.Add(new Vector3(0, 0, 1));
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0));
					barycentric_3f.Add(new Vector3(0, 0, 1));
					
				}
			}

			m_Verts = new DynamicGridVert[position_2f.Count];

			for (int i = 0; i < position_2f.Count; i++)
			{
				m_Verts[i] = new DynamicGridVert()
				{
					Position = position_2f[i],
					TexCoord = texcoord_2f[i],
					Barycentric = barycentric_3f[i],
				};
			}

			m_Buffer = new VertexBuffer("Hex grid", ResourceMode.Static,
				new VertexBufferInfo(DynamicGridVert.Format, m_Verts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw)); 
		}

		private float Clamp(float value, int min, int max)
		{
			return Math.Min(Math.Max(value, min), max); 
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				m_Buffer.LoadResources();

				DataStream stream; 

				m_Buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

				stream.WriteRange(m_Verts); 

				m_Buffer.UnmapBuffer(); 

				m_Disposed = false;
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Buffer.UnloadResources(); 

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources();
		}

		#endregion

		public Rug.Game.Core.Textures.BitmapTexture2D Texture { get; set; }
	}
}
