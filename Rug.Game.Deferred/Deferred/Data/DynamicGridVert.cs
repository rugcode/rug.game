﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Deferred.Data
{
	/* 
	position_2f: position_2f,
	texcoord_2f: texcoord_2f,
	barycentric_3f: barycentric_3f,
	 */

	[StructLayout(LayoutKind.Sequential)]
	public struct DynamicGridVert
	{		
		public Vector2 Position;
		public Vector2 TexCoord;
		public Vector3 Barycentric;

		public enum Elements : int { Position = 0, TexCoord = 1, Barycentric = 2, };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int TexCoordOffset;
		public static readonly int BarycentricOffset;

		static DynamicGridVert()
		{
			Stride = BlittableValueType<DynamicGridVert>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(DynamicGridVert), "Position");
			TexCoordOffset = (int)Marshal.OffsetOf(typeof(DynamicGridVert), "TexCoord");
			BarycentricOffset = (int)Marshal.OffsetOf(typeof(DynamicGridVert), "Barycentric");

			if (BarycentricOffset + BlittableValueType<Vector3>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.TexCoord, 2, VertexAttribPointerType.Float, false, Stride, TexCoordOffset);
			GL.VertexAttribPointer((int)Elements.Barycentric, 3, VertexAttribPointerType.Float, false, Stride, BarycentricOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.TexCoord);
			GL.EnableVertexAttribArray((int)Elements.Barycentric);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.TexCoord);
			GL.DisableVertexAttribArray((int)Elements.Barycentric);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return DynamicGridVert.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.TexCoord + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TexCoordOffset);
				GL.VertexAttribPointer((int)Elements.Barycentric + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, BarycentricOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TexCoord + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Barycentric + baseLocation);

				baseLocation += 3;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.TexCoord + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TexCoordOffset);
				GL.VertexAttribPointer((int)Elements.Barycentric + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, BarycentricOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TexCoord + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Barycentric + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.TexCoord + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Barycentric + baseLocation, devisor);

				baseLocation += 3;
			}

			#endregion
		}

		#endregion
	}
}
