﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Deferred.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct LightingMeshVertex
	{		
		public Vector3 Position;
		public Vector3 Normal;
		public Color3 Color;

		public enum Elements : int { Position = 0, Normal = 1, Color = 2 };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int NormalOffset;
		public static readonly int ColorOffset;

		static LightingMeshVertex()
		{
			Stride = BlittableValueType<LightingMeshVertex>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(LightingMeshVertex), "Position");
			NormalOffset = (int)Marshal.OffsetOf(typeof(LightingMeshVertex), "Normal");
			ColorOffset = (int)Marshal.OffsetOf(typeof(LightingMeshVertex), "Color");

			if (ColorOffset + BlittableValueType<Color3>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.Normal, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
			GL.VertexAttribPointer((int)Elements.Color, 3, VertexAttribPointerType.Float, false, Stride, ColorOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.Normal);
			GL.EnableVertexAttribArray((int)Elements.Color);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.Normal);
			GL.DisableVertexAttribArray((int)Elements.Color);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return LightingMeshVertex.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{				
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				baseLocation += 3;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Normal + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);

				baseLocation += 3;
			}

			#endregion
		}

		#endregion
	}
}
