﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Deferred.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct TerrainVertex
	{		
		public Vector3 Position;
		//public Vector3 Normal;
		public Color4 Color;
		public Vector2 TextureCoords;

		public enum Elements : int { Position = 0, Color = 1, TextureCoords = 2, };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		//public static readonly int NormalOffset;
		public static readonly int ColorOffset;
		public static readonly int TextureCoordsOffset;

		static TerrainVertex()
		{
			Stride = BlittableValueType<TerrainVertex>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(TerrainVertex), "Position");
			//NormalOffset = (int)Marshal.OffsetOf(typeof(TerrainVertex), "Normal");
			ColorOffset = (int)Marshal.OffsetOf(typeof(TerrainVertex), "Color");
			TextureCoordsOffset = (int)Marshal.OffsetOf(typeof(TerrainVertex), "TextureCoords");

			if (TextureCoordsOffset + BlittableValueType<Vector2>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			//GL.VertexAttribPointer((int)Elements.Normal, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
			GL.VertexAttribPointer((int)Elements.TextureCoords, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			//GL.EnableVertexAttribArray((int)Elements.Normal);
			GL.EnableVertexAttribArray((int)Elements.Color);
			GL.EnableVertexAttribArray((int)Elements.TextureCoords);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			//GL.DisableVertexAttribArray((int)Elements.Normal);
			GL.DisableVertexAttribArray((int)Elements.Color);
			GL.DisableVertexAttribArray((int)Elements.TextureCoords);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return TerrainVertex.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);

				baseLocation += 3;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.TextureCoords + baseLocation, devisor);

				baseLocation += 3;
			}

			#endregion
		}

		#endregion
	}
}
