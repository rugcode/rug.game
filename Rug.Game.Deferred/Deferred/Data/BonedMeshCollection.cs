﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Deferred.Data
{
	public class BonedMeshCollection : ResourceSet<BonedMesh>
	{
		public BonedMeshCollection(string name, ResourceMode mode)
			: base(name, mode)
		{

		}
	}
}
