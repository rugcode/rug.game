﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Deferred.Data
{
	public class LightingMeshCollection : ResourceSet<LightingMesh>
	{
		public LightingMeshCollection(string name, ResourceMode mode)
			: base(name, mode)
		{

		}
	}
}
