﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Rug.Game.Deferred.Data
{
	public class XSkinMeshLoader
	{
		public static XFrame LoadObject(string path)
		{
			TokenParser parser = new TokenParser(path);

			stToken token = parser.GetNextToken();

			CheckName(parser, ref token, "Frame", false); 

			return ParseFrame(parser, ref token);
		}

		public static void LoadObject(XFrame root, XMesh mesh, out ArmatureBones armature)
		{
			armature = new ArmatureBones(LoadArmature(root, mesh));		
		}

		private static Armature LoadArmature(XFrame root, XMesh mesh)
		{
			Armature armature = new Armature();

			armature.Name = root.Name;
			armature.Parent = null;
			armature.MatrixOffset = Matrix4.Identity;
			armature.Transform = root.Matrix;

			if (mesh.SkinWeights.ContainsKey(root.Name) == true)
			{
				armature.Weights = mesh.SkinWeights[root.Name];
				armature.MatrixOffset = armature.Weights.Matrix;
			}

			foreach (XFrame frame in root.Children)
			{
				Armature child = LoadArmature(frame, mesh);

				child.Parent = armature;

				armature.Children.Add(child); 
			}

			return armature;
		}

		public static void LoadObject(XMesh mesh, ArmatureBones bones, Vector4 material, out BonedMeshVertex[] verts, out ushort[] indicies)
		{
			List<BonedMeshVertex> vertBuilder = new List<BonedMeshVertex>();
			List<ushort> i = new List<ushort>();
			Color4 color = new Color4(1f, 1f, 1f, 0f); 
			//Vector4 material = new Vector4(0.75f, 8f, 1f, 0f);
			ushort index = 0;

			if (mesh.TextureCoords == null)
			{
				mesh.TextureCoords = new Vector2[mesh.Verts.Length]; 
			}

			int faceIndex = 0; 
			for (int f = 0; f < mesh.FaceIndexCount.Length; f++)
			{
				int faceParts = mesh.FaceIndexCount[f]; 

				if (faceParts == 3)
				{
					int faceParts0 = mesh.FaceIndicies[faceIndex++]; 
					int faceParts1 = mesh.FaceIndicies[faceIndex++]; 
					int faceParts2 = mesh.FaceIndicies[faceIndex++];

					BonedMeshVertex v1 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts0],
						Normal = mesh.Normals[faceParts0],
						TextureCoords = mesh.TextureCoords[faceParts0],
						Color = color,
						Material = material,
					};

					BonedMeshVertex v2 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts1],
						Normal = mesh.Normals[faceParts1],
						TextureCoords = mesh.TextureCoords[faceParts1],
						Color = color,
						Material = material,
					};

					BonedMeshVertex v3 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts2],
						Normal = mesh.Normals[faceParts2],
						TextureCoords = mesh.TextureCoords[faceParts2],
						Color = color,
						Material = material,
					};

					v1.Tangent = calcTangentVector(v1.Position, v2.Position, v3.Position,
													v1.TextureCoords, v2.TextureCoords, v3.TextureCoords,
													v1.Normal);

					v2.Tangent = calcTangentVector(v2.Position, v3.Position, v1.Position,
													v2.TextureCoords, v3.TextureCoords, v1.TextureCoords,
													v2.Normal);

					v3.Tangent = calcTangentVector(v3.Position, v1.Position, v2.Position,
													v3.TextureCoords, v1.TextureCoords, v2.TextureCoords,
													v3.Normal);

					CalculateWeightsAndIndicies(bones, faceParts0, ref v1);
					CalculateWeightsAndIndicies(bones, faceParts1, ref v2);
					CalculateWeightsAndIndicies(bones, faceParts2, ref v3); 

					vertBuilder.Add(v1);
					vertBuilder.Add(v2);
					vertBuilder.Add(v3);
					i.Add(index++);
					i.Add(index++);
					i.Add(index++);
				}
				else if (faceParts == 4)
				{
					int faceParts0 = mesh.FaceIndicies[faceIndex++];
					int faceParts1 = mesh.FaceIndicies[faceIndex++];
					int faceParts2 = mesh.FaceIndicies[faceIndex++];
					int faceParts3 = mesh.FaceIndicies[faceIndex++];

					BonedMeshVertex v1 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts0],
						Normal = mesh.Normals[faceParts0],
						TextureCoords = mesh.TextureCoords[faceParts0],
						Color = color,
						Material = material,
					};

					BonedMeshVertex v2 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts1],
						Normal = mesh.Normals[faceParts1],
						TextureCoords = mesh.TextureCoords[faceParts1],
						Color = color,
						Material = material,
					};

					BonedMeshVertex v3 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts2],
						Normal = mesh.Normals[faceParts2],
						TextureCoords = mesh.TextureCoords[faceParts2],
						Color = color,
						Material = material,
					};

					BonedMeshVertex v4 = new BonedMeshVertex()
					{
						Position = mesh.Verts[faceParts3],
						Normal = mesh.Normals[faceParts3],
						TextureCoords = mesh.TextureCoords[faceParts3],
						Color = color,
						Material = material,
					};

					v1.Tangent = calcTangentVector(v1.Position, v2.Position, v3.Position,
													v1.TextureCoords, v2.TextureCoords, v3.TextureCoords,
													v1.Normal);

					v2.Tangent = calcTangentVector(v2.Position, v3.Position, v1.Position,
													v2.TextureCoords, v3.TextureCoords, v1.TextureCoords,
													v2.Normal);

					v3.Tangent = calcTangentVector(v3.Position, v1.Position, v2.Position,
													v3.TextureCoords, v1.TextureCoords, v2.TextureCoords,
													v3.Normal);
					
					v4.Tangent = calcTangentVector(v4.Position, v1.Position, v3.Position,
													v4.TextureCoords, v1.TextureCoords, v3.TextureCoords,
													v4.Normal);

					CalculateWeightsAndIndicies(bones, faceParts0, ref v1);
					CalculateWeightsAndIndicies(bones, faceParts1, ref v2);
					CalculateWeightsAndIndicies(bones, faceParts2, ref v3);
					CalculateWeightsAndIndicies(bones, faceParts3, ref v4); 

					ushort baseIndex = index; 

					vertBuilder.Add(v1);
					vertBuilder.Add(v2);
					vertBuilder.Add(v3);
					vertBuilder.Add(v4);

					i.Add((ushort)(baseIndex + 0));
					i.Add((ushort)(baseIndex + 1));
					i.Add((ushort)(baseIndex + 2));

					i.Add((ushort)(baseIndex + 2));
					i.Add((ushort)(baseIndex + 3));
					i.Add((ushort)(baseIndex + 0));

					index += 4; 
				}
			}

			verts = vertBuilder.ToArray();
			indicies = i.ToArray(); 
		}

		private static void CalculateWeightsAndIndicies(ArmatureBones bones, int index, ref BonedMeshVertex vert)
		{
			List<int> indices = new List<int>();
			List<float> weights = new List<float>(); 

			for (int i = 0; i < bones.Bones.Count; i++)
			{
				Armature arm = bones.Bones[i]; 

				if (arm.Weights != null)
				{
					XSkinWeights armWeights = arm.Weights;

					for (int j = 0; j < armWeights.Indexes.Length; j++)
					{
						if (armWeights.Indexes[j] == index)
						{
							indices.Add(i);
							weights.Add(armWeights.Weights[j]); 
						}
					}
				}
			}

			while (indices.Count > 4)
			{
				float min = float.MaxValue;
				int currentIndex = -1; 

				for (int i = 0; i < indices.Count; i++)
				{
					if (weights[i] < min)
					{
						currentIndex = i;
					}
				}

				if (currentIndex < 0)
				{
					throw new Exception(); 
				}

				indices.RemoveAt(currentIndex);
				weights.RemoveAt(currentIndex); 
			}

			while (indices.Count < 4)
			{
				indices.Add(0);
				weights.Add(0); 
			}

			vert.Indices = new Vector4((int)indices[0], (int)indices[1], (int)indices[2], (int)indices[3]);
			vert.Weights = new Vector4(weights[0], weights[1], weights[2], weights[3]);
		}

		private static Vector2 ParseVector2(string str)
		{
			string[] parts = str.Split(new char[] { ' ' } , StringSplitOptions.RemoveEmptyEntries); 

			return new Vector2(float.Parse(parts[0], CultureInfo.InvariantCulture), 
							   1 - float.Parse(parts[1], CultureInfo.InvariantCulture)); 
		}

		private static Vector3 ParseVector3(string str)
		{
			string[] parts = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			return new Vector3(float.Parse(parts[0], CultureInfo.InvariantCulture),
							   float.Parse(parts[1], CultureInfo.InvariantCulture),
							   float.Parse(parts[2], CultureInfo.InvariantCulture)); 
		}		
		
		private static void ParseFacePart(string part, out int vi, out int ti, out int ni)
		{
			string[] parts = part.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

			vi = int.Parse(parts[0], CultureInfo.InvariantCulture);
			ti = int.Parse(parts[1], CultureInfo.InvariantCulture);
			ni = int.Parse(parts[2], CultureInfo.InvariantCulture); 
		}

		static Vector4 calcTangentVector(Vector3 pos1, Vector3 pos2,
										Vector3 pos3, Vector2 texCoord1,
										Vector2 texCoord2, Vector2 texCoord3,
										Vector3 normal)
		{
			// Given the 3 vertices (position and texture coordinates) of a triangle
			// calculate and return the triangle's tangent vector.

			// Create 2 vectors in object space.
			//
			// edge1 is the vector from vertex positions pos1 to pos2.
			// edge2 is the vector from vertex positions pos1 to pos3.
			Vector3 edge1 = new Vector3(pos2.X - pos1.X, pos2.Y - pos1.Y, pos2.Z - pos1.Z);
			Vector3 edge2 = new Vector3(pos3.X - pos1.X, pos3.Y - pos1.Y, pos3.Z - pos1.Z);

			edge1.Normalize();
			edge2.Normalize();

			// Create 2 vectors in tangent (texture) space that point in the same
			// direction as edge1 and edge2 (in object space).
			//
			// texEdge1 is the vector from texture coordinates texCoord1 to texCoord2.
			// texEdge2 is the vector from texture coordinates texCoord1 to texCoord3.
			Vector2 texEdge1 = new Vector2(texCoord2.X - texCoord1.X, texCoord2.Y - texCoord1.Y);
			Vector2 texEdge2 = new Vector2(texCoord3.X - texCoord1.X, texCoord3.Y - texCoord1.Y);

			texEdge1.Normalize();
			texEdge2.Normalize();

			// These 2 sets of vectors form the following system of equations:
			//
			//  edge1 = (texEdge1.r * tangent) + (texEdge1.g * bitangent)
			//  edge2 = (texEdge2.r * tangent) + (texEdge2.g * bitangent)
			//
			// Using matrix notation this system looks like:
			//
			//  [ edge1 ]     [ texEdge1.r  texEdge1.g ]  [ tangent   ]
			//  [       ]  =  [                        ]  [           ]
			//  [ edge2 ]     [ texEdge2.r  texEdge2.g ]  [ bitangent ]
			//
			// The solution is:
			//
			//  [ tangent   ]        1     [ texEdge2.g  -texEdge1.g ]  [ edge1 ]
			//  [           ]  =  -------  [                         ]  [       ]
			//  [ bitangent ]      det A   [-texEdge2.r   texEdge1.r ]  [ edge2 ]
			//
			//  where:
			//        [ texEdge1.r  texEdge1.g ]
			//    A = [                        ]
			//        [ texEdge2.r  texEdge2.g ]
			//
			//    det A = (texEdge1.r * texEdge2.g) - (texEdge1.g * texEdge2.r)
			//
			// From this solution the tangent space basis vectors are:
			//
			//    tangent = (1 / det A) * ( texEdge2.g * edge1 - texEdge1.g * edge2)
			//  bitangent = (1 / det A) * (-texEdge2.r * edge1 + texEdge1.r * edge2)
			//     normal = cross(tangent, bitangent)

			Vector3 t = new Vector3(0, 0, 0);
			Vector3 b = new Vector3(0, 0, 0);
			Vector3 n = new Vector3(normal.X, normal.Y, normal.Z);

			float det = (texEdge1.X * texEdge2.Y) - (texEdge1.Y * texEdge2.X);

			if (closeEnough(det, 0.0f) == true)
			{
				t = new Vector3(1.0f, 0.0f, 0.0f);
				b = new Vector3(0.0f, 1.0f, 0.0f);
			}
			else
			{
				det = 1.0f / det;

				t.X = (texEdge2.Y * edge1.X - texEdge1.Y * edge2.X) * det;
				t.Y = (texEdge2.Y * edge1.Y - texEdge1.Y * edge2.Y) * det;
				t.Z = (texEdge2.Y * edge1.Z - texEdge1.Y * edge2.Z) * det;

				b.X = (-texEdge2.X * edge1.X + texEdge1.X * edge2.X) * det;
				b.Y = (-texEdge2.X * edge1.Y + texEdge1.X * edge2.Y) * det;
				b.Z = (-texEdge2.X * edge1.Z + texEdge1.X * edge2.Z) * det;

				t.Normalize();
				b.Normalize();
			}

			// Calculate the handedness of the local tangent space.
			// The bitangent vector is the cross product between the triangle face
			// normal vector and the calculated tangent vector. The resulting bitangent
			// vector should be the same as the bitangent vector calculated from the
			// set of linear equations above. If they point in different directions
			// then we need to invert the cross product calculated bitangent vector. We
			// store this scalar multiplier in the tangent vector's 'a' component so
			// that the correct bitangent vector can be generated in the normal mapping
			// shader's vertex shader.

			Vector3 bitangent = Vector3.Cross(n, t);
			float handedness = (Vector3.Dot(bitangent, b) < 0.0f) ? -1.0f : 1.0f;

			Vector4 tangent = new Vector4(); 

			tangent.X = t.X;
			tangent.Y = t.Y;
			tangent.Z = t.Z;
			tangent.W = handedness; // *-1f;

			return tangent; 
		}

		const float EPSILON = 1e-6f;

		static bool closeEnough(float f1, float f2)
		{
			// Determines whether the two floating-point values f1 and f2 are
			// close enough together that they can be considered equal.			
			return Math.Abs((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < EPSILON;
		}

		#region Parsing 

		private static XFrame ParseFrame(TokenParser parser, ref stToken token)
		{
			CheckName(parser, ref token, "Frame", true);

			XFrame frame = new XFrame();

			frame.Name = ParseString(parser, ref token);

			CheckOpenBracket(parser, ref token);

			CheckName(parser, ref token, "FrameTransformMatrix", false);

			frame.Matrix = ParseMatrix(parser, ref token);

			while (token.GetTokenType() != stToken.TokenType.TokenCloseB) 
			{
				string type = ParseString(parser, ref token, false); 

				switch (type)
				{
					case "Frame":
						XFrame child = ParseFrame(parser, ref token);

						frame.Children.Add(child);
						child.Parent = frame; 

						break;
					case "Mesh":
						frame.Mesh = ParseMesh(parser, ref token);
						break;
					case "MeshNormals":
						frame.Mesh.Normals = ParseMeshNormals(parser, ref token);
						break;
					case "MeshTextureCoords":
						frame.Mesh.TextureCoords = ParseMeshTextureCoords(parser, ref token);
						break;
					case "MeshMaterialList":
						ParseMeshMaterialList(parser, ref token);
						break;
					case "XSkinMeshHeader":
						ParseXSkinMeshHeader(parser, ref token);
						break;
					case "SkinWeights":
						XSkinWeights weights = ParseSkinWeights(parser, ref token);

						frame.Mesh.SkinWeights.Add(weights.Name, weights);
						break;
					default:
						throw new Exception("type == " + type);
				}
			}

			CheckCloseBracket(parser, ref token);

			return frame;
		}

		private static string ParseString(TokenParser parser, ref stToken token)
		{
			return ParseString(parser, ref token, true);
		}

		private static string ParseString(TokenParser parser, ref stToken token, bool next)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenString)
			{
				throw new Exception(String.Format("Expected string got '{0}'", token.GetTokenType().ToString()));
			}

			string str = token.m_data;

			if (next == true)
			{
				token = parser.GetNextToken();
			}

			return str;
		}

		private static void SkipObject(TokenParser parser, ref stToken token)
		{
			while (token.GetTokenType() != stToken.TokenType.TokenOpenB)
			{				
				token = parser.GetNextToken();
			}

			int depth = 0;

			while (token.GetTokenType() != stToken.TokenType.TokenCloseB || depth > 0)
			{
				token = parser.GetNextToken();

				if (token.GetTokenType() != stToken.TokenType.TokenOpenB)
				{
					depth++; 
				}

				if (token.GetTokenType() != stToken.TokenType.TokenCloseB)
				{
					depth--;
				}
			}

			CheckCloseBracket(parser, ref token); 
		}

		private static XMesh ParseMesh(TokenParser parser, ref stToken token)
		{
			CheckName(parser, ref token, "Mesh", true);

			CheckOpenBracket(parser, ref token);

			XMesh mesh = new XMesh();
			mesh.NumberOfVerts = ParseInt(parser, ref token);
			mesh.Verts = new Vector3[mesh.NumberOfVerts]; 

			CheckSemiColon(parser, ref token);

			for (int i = 0, e = mesh.NumberOfVerts; i < e; i++)
			{				
				float x = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token); 
				
				float y = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token); 
				
				float z = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token);

				mesh.Verts[i] = new Vector3(x, y, z); 

				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token); 
				}
				else
				{
					CheckComma(parser, ref token); 
				}
			}

			mesh.NumberOfFaces = ParseInt(parser, ref token);
			mesh.FaceIndexCount = new ushort[mesh.NumberOfFaces];
			List<ushort> indicies = new List<ushort>(); 

			CheckSemiColon(parser, ref token);

			for (int i = 0, e = mesh.NumberOfFaces; i < e; i++)
			{				
				int faceIndexCount = ParseInt(parser, ref token);

				mesh.FaceIndexCount[i] = (ushort)faceIndexCount;

				CheckSemiColon(parser, ref token); 

				for (int j = 0; j < faceIndexCount; j++)
				{
					indicies.Add((ushort)ParseInt(parser, ref token));

					CheckSemiColon(parser, ref token); 
				}

				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token); 
				}
				else
				{
					CheckComma(parser, ref token); 
				}
			}

			mesh.FaceIndicies = indicies.ToArray(); 

			while (token.GetTokenType() != stToken.TokenType.TokenCloseB)
			{
				string type = ParseString(parser, ref token, false);

				switch (type)
				{
					case "MeshNormals":
						mesh.Normals = ParseMeshNormals(parser, ref token);
						break;
					case "MeshTextureCoords":
						mesh.TextureCoords = ParseMeshTextureCoords(parser, ref token);
						break;
					case "MeshMaterialList":
						ParseMeshMaterialList(parser, ref token);
						break;
					case "XSkinMeshHeader": 
						ParseXSkinMeshHeader(parser, ref token);
						break;
					case "SkinWeights":
						XSkinWeights weights = ParseSkinWeights(parser, ref token);

						mesh.SkinWeights.Add(weights.Name, weights); 
						break;
					default:
						throw new Exception("type == " + type);
				}
			}

			CheckCloseBracket(parser, ref token);

			return mesh;
		}

		private static Vector2[] ParseMeshTextureCoords(TokenParser parser, ref stToken token)
		{
			CheckName(parser, ref token, "MeshTextureCoords", true);

			CheckOpenBracket(parser, ref token);

			int numberOfVerts = ParseInt(parser, ref token);

			Vector2[] coords = new Vector2[numberOfVerts]; 

			CheckSemiColon(parser, ref token);

			for (int i = 0, e = numberOfVerts; i < e; i++)
			{
				float x = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token);

				float y = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token);
								
				coords[i] = new Vector2(x, y); 

				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token);
				}
				else
				{
					CheckComma(parser, ref token);
				}
			}

			CheckCloseBracket(parser, ref token);

			return coords; 
		}

		private static XSkinWeights ParseSkinWeights(TokenParser parser, ref stToken token)
		{		
			CheckName(parser, ref token, "SkinWeights", true);

			CheckOpenBracket(parser, ref token);

			XSkinWeights weights = new XSkinWeights();

			weights.Name = ParseString(parser, ref token); 
			CheckSemiColon(parser, ref token);
			
			int number = ParseInt(parser, ref token);
			CheckSemiColon(parser, ref token);

			weights.Indexes = new int[number];
			weights.Weights = new float[number]; 

			for (int i = 0, e = number; i < e; i++)
			{
				weights.Indexes[i] = ParseInt(parser, ref token);
				
				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token);
				}
				else
				{
					CheckComma(parser, ref token);
				}
			}

			for (int i = 0, e = number; i < e; i++)
			{
				weights.Weights[i] = ParseFloat(parser, ref token);

				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token);
				}
				else
				{
					CheckComma(parser, ref token);
				}
			}

			float[] m = new float[16];

			for (int i = 0; i < 16; i++)
			{
				m[i] = ParseFloat(parser, ref token);

				if (i < 15)
				{
					CheckComma(parser, ref token);
				}
				else if (i == 15)
				{
					CheckSemiColon(parser, ref token);
				}
			}

			
			weights.Matrix = new Matrix4(m[0], m[1], m[2], m[3], 
										 m[4], m[5], m[6], m[7], 
										 m[8], m[9], m[10], m[11], 
										 m[12], m[13], m[14], m[15]);
			
			/*
			weights.Matrix = new Matrix4(m[0], m[4], m[8], m[12],
										 m[1], m[5], m[9], m[13],
										 m[2], m[6], m[10], m[14],
										 m[3], m[7], m[11], m[15]); 
			*/ 
			CheckSemiColon(parser, ref token);

			CheckCloseBracket(parser, ref token);

			return weights; 
		}

		private static void ParseXSkinMeshHeader(TokenParser parser, ref stToken token)
		{
			CheckName(parser, ref token, "XSkinMeshHeader", true);

			CheckOpenBracket(parser, ref token);

			ParseInt(parser, ref token);
			CheckSemiColon(parser, ref token); 

			ParseInt(parser, ref token);
			CheckSemiColon(parser, ref token); 

			ParseInt(parser, ref token);
			CheckSemiColon(parser, ref token); 

			CheckCloseBracket(parser, ref token); 
		}

		private static Vector3[] ParseMeshNormals(TokenParser parser, ref stToken token)
		{
			CheckName(parser, ref token, "MeshNormals", true);

			CheckOpenBracket(parser, ref token);

			int numberOfVerts = ParseInt(parser, ref token);

			Vector3[] normals = new Vector3[numberOfVerts]; 

			CheckSemiColon(parser, ref token);

			for (int i = 0, e = numberOfVerts; i < e; i++)
			{
				float x = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token);

				float y = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token);
				
				float z = ParseFloat(parser, ref token);
				CheckSemiColon(parser, ref token);

				normals[i] = new Vector3(x, y, z); 

				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token);
				}
				else
				{
					CheckComma(parser, ref token);
				}
			}

			int numberOfFaces = ParseInt(parser, ref token);

			CheckSemiColon(parser, ref token);

			for (int i = 0, e = numberOfFaces; i < e; i++)
			{
				int faceIndexCount = ParseInt(parser, ref token);

				CheckSemiColon(parser, ref token);

				for (int j = 0; j < faceIndexCount; j++)
				{
					float number = ParseFloat(parser, ref token);

					CheckSemiColon(parser, ref token);
				}

				if (i == e - 1)
				{
					CheckSemiColon(parser, ref token);
				}
				else
				{
					CheckComma(parser, ref token);
				}
			}

			CheckCloseBracket(parser, ref token);

			return normals; 
		}

		private static void ParseMeshMaterialList(TokenParser parser, ref stToken token)
		{
			CheckName(parser, ref token, "MeshMaterialList", true);

			CheckOpenBracket(parser, ref token);

			int materials = ParseInt(parser, ref token);

			CheckSemiColon(parser, ref token);

			int faces = ParseInt(parser, ref token);

			CheckSemiColon(parser, ref token);

			for (int i = 0; i < materials; i++)
			{
				for (int j = 0, e = faces; j < e; j++)
				{
					float number = ParseFloat(parser, ref token);

					if (j == e - 1)
					{
						CheckSemiColon(parser, ref token);
					}
					else 
					{
						CheckComma(parser, ref token);
					}
				}

				CheckSemiColon(parser, ref token);
			}

			//CheckSemiColon(parser, ref token);

			for (int i = 0; i < materials; i++)
			{
				ParseMaterial(parser, ref token); 
			}

			CheckCloseBracket(parser, ref token); 
		}

		private static void ParseMaterial(TokenParser parser, ref stToken token)
		{
			/* 
          Material Material {
             0.640000; 0.640000; 0.640000; 1.000000;;
             96.078431;
             0.500000; 0.500000; 0.500000;;
             0.000000; 0.000000; 0.000000;;
          }
			 */

			CheckName(parser, ref token, "Material", true);

			ParseString(parser, ref token); 

			CheckOpenBracket(parser, ref token);

			ParseColorRGBA(parser, ref token);
			CheckSemiColon(parser, ref token); 
			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token); 
			ParseColorRGB(parser, ref token);
			CheckSemiColon(parser, ref token); 
			ParseColorRGB(parser, ref token);
			CheckSemiColon(parser, ref token); 

			CheckCloseBracket(parser, ref token);
		}

		private static void ParseColorRGBA(TokenParser parser, ref stToken token)
		{
			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token); 

			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token); 

			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token);

			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token);			
		}

		private static void ParseColorRGB(TokenParser parser, ref stToken token)
		{
			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token);

			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token);

			ParseFloat(parser, ref token);
			CheckSemiColon(parser, ref token);
		}

		private static void CheckComma(TokenParser parser, ref stToken token)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenComma)
			{
				throw new Exception(String.Format("Expected ',' got '{0}'", token.GetTokenType().ToString()));
			}

			token = parser.GetNextToken();
		}

		private static void CheckSemiColon(TokenParser parser, ref stToken token)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenSemiColon)
			{
				throw new Exception(String.Format("Expected ';' got '{0}'", token.GetTokenType().ToString()));
			}

			token = parser.GetNextToken();
		}

		private static int ParseInt(TokenParser parser, ref stToken token)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenNumber)
			{
				throw new Exception(String.Format("Expected int got '{0}'", token.GetTokenType().ToString()));
			}

			int number = int.Parse(token.m_data);

			token = parser.GetNextToken();

			return number;
		}

		private static float ParseFloat(TokenParser parser, ref stToken token)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenNumber)
			{
				throw new Exception(String.Format("Expected float got '{0}'", token.GetTokenType().ToString()));
			}

			float number = float.Parse(token.m_data);

			token = parser.GetNextToken();

			return number;
		}

		private static void CheckOpenBracket(TokenParser parser, ref stToken token)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenOpenB)
			{
				throw new Exception(String.Format("Expected '{{' got '{0}'", token.GetTokenType().ToString()));
			}

			token = parser.GetNextToken();
		}

		private static void CheckCloseBracket(TokenParser parser, ref stToken token)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenCloseB)
			{
				throw new Exception(String.Format("Expected '}}' got '{0}'", token.GetTokenType().ToString()));
			}

			token = parser.GetNextToken();
		}

		private static void CheckName(TokenParser parser,ref stToken token, string name, bool next)
		{
			if (token.GetTokenType() != stToken.TokenType.TokenString && token.m_data != name)
			{
				throw new Exception(String.Format("Incorrect name '{0}' expected '{1}'", token.m_data, name));
			}

			if (next == true)
			{
				token = parser.GetNextToken();
			}
		}

		private static Matrix4 ParseMatrix(TokenParser parser, ref stToken token)
		{
			/* 
			  FrameTransformMatrix {
				 1.000000, 0.000000, 0.000000, 0.000000,
				 0.000000,-0.000000,-1.000000, 0.000000,
				 0.000000, 1.000000,-0.000000, 0.000000,
				 0.000000, 0.000000, 0.000000, 1.000000;;
			  }
			 */

			CheckName(parser, ref token, "FrameTransformMatrix", true);

			CheckOpenBracket(parser, ref token); 

			float[] m = new float[16];

			for (int i = 0; i < 16; i++)
			{
				m[i] = ParseFloat(parser, ref token); 

				if (i < 15)
				{					 
					CheckComma(parser, ref token); 
				}
				else if (i == 15)
				{
					CheckSemiColon(parser, ref token); 					
				}
			}

			CheckSemiColon(parser, ref token);

			CheckCloseBracket(parser, ref token); 
			
			
			return new Matrix4(m[0], m[1], m[2], m[3], 
								m[4], m[5], m[6], m[7], 
								m[8], m[9], m[10], m[11], 
								m[12], m[13], m[14], m[15]);
			
			/*
			return new Matrix4(m[0], m[4], m[8], m[12],
								m[1], m[5], m[9], m[13],
								m[2], m[6], m[10], m[14],
								m[3], m[7], m[11], m[15]); 
			 */ 
		}

		#endregion
	}	

	public class XMesh
	{
		public int NumberOfVerts;
		public Vector3[] Verts;
		public int NumberOfFaces;
		public ushort[] FaceIndexCount;
		public ushort[] FaceIndicies;
		public Vector3[] Normals;
		public Vector2[] TextureCoords;
		public Dictionary<string, XSkinWeights> SkinWeights = new Dictionary<string, XSkinWeights>(); 
	}

	public class XSkinWeights
	{
		public string Name;
		public int[] Indexes;
		public float[] Weights;
		public Matrix4 Matrix;
	}

	public class XFrame
	{		
		public XFrame Parent;
		
		public string Name; 
		public Matrix4 Matrix;
		
		public List<XFrame> Children = new List<XFrame>();

		public XMesh Mesh; 
	}
}
