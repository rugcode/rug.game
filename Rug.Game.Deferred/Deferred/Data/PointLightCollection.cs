﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;

namespace Rug.Game.Deferred.Data
{
	public class PointLightCollection : IResource, ICollection<PointLight>
	{		
		private readonly string m_Name;		
		private readonly ResourceMode m_Mode;
		private readonly List<PointLight> m_Lights = new List<PointLight>();
		private ShaderStorageBuffer m_Buffer;

		private bool m_IsLoaded = false;

		#region IResource Members

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Subset; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_Mode; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public PointLight this[int index]
		{
			get { return m_Lights[index]; }
			set { m_Lights[index] = value; }
		}

		public event EventHandler<ResourceEventArgs> LoadStateChanged;

		public ShaderStorageBuffer Buffer { get { return m_Buffer; } }

		public bool IsDirty { get; set; } 

		

		public PointLightCollection(string name, ResourceMode mode, int maxCount)
		{
			m_Name = name;
			m_Mode = mode;

			m_Buffer = new ShaderStorageBuffer(name + " Buffer", mode, new ShaderStorageBufferInfo(PointLight.Format, maxCount, OpenTK.Graphics.OpenGL.BufferUsageHint.DynamicDraw)); 
		}

		public void LoadResources()
		{
			if (m_IsLoaded == true)
			{
				return; 
			}
			
			m_Buffer.LoadResources();

			m_IsLoaded = m_Buffer.IsLoaded;

			Update(); 

			OnLoadStateChanged(m_IsLoaded);
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == false)
			{
				return;
			}

			m_Buffer.UnloadResources();

			m_IsLoaded = m_Buffer.IsLoaded;

			OnLoadStateChanged(m_IsLoaded);
		}

		private void OnLoadStateChanged(bool isLoaded)
		{
			if (m_IsLoaded != isLoaded)
			{
				m_IsLoaded = isLoaded;

				if (LoadStateChanged != null)
				{
					LoadStateChanged(this, new ResourceEventArgs(this));
				}
			}
		}

		public void Update()
		{
			if (IsDirty && IsLoaded)
			{
				UploadData();
				IsDirty = false; 
			}
		}

		public void UploadData()
		{
			DataStream stream;
			m_Buffer.MapBuffer(BufferAccess.WriteOnly, out stream);
			stream.WriteRange(m_Lights.ToArray(), 0, Count);
			m_Buffer.UnmapBuffer();		
		}

		#endregion

		#region ICollection<IResource> Members

		public void Add(PointLight item)
		{
			m_Lights.Add(item);
		}

		public void Clear()
		{
			m_Lights.Clear();
		}

		public bool Contains(PointLight item)
		{
			return m_Lights.Contains(item);
		}

		public void CopyTo(PointLight[] array, int arrayIndex)
		{
			m_Lights.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { return m_Lights.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove(PointLight item)
		{
			return m_Lights.Remove(item);
		}

		#endregion

		#region IEnumerable<IResource> Members

		public IEnumerator<PointLight> GetEnumerator()
		{
			return m_Lights.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return (m_Lights as System.Collections.IEnumerable).GetEnumerator();
		}

		#endregion

	}
}
