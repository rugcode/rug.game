﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;

namespace Rug.Game.Deferred.Data.Terrain
{
	public class TerrainPatch : IResource
	{		
		private int m_BlockSize;
		private VertexBuffer m_Buffer;
		private uint m_NodeID;		
		private bool m_HasPatchFinishedLoading;
		private LodQuadTreeNode m_CurrentNode;
		private LodQuadTree m_Tree;

		public VertexBuffer Buffer
		{
			get { return m_Buffer; }
		}

		public uint NodeID
		{
			get { return m_NodeID; }
		}

		public LodQuadTree Tree
		{
			get { return m_Tree; }
		}

		public string Name
		{
			get { return "Patch: " + m_NodeID; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.VertexBuffer; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Dynamic; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return m_Buffer.ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_Buffer.ResourceHandle; }
		}

		public bool IsLoaded
		{
			get { return m_Buffer.IsLoaded; }
		}

		public bool IsVisible { get; set; }

		public bool HasPatchFinishedLoading { get { return m_HasPatchFinishedLoading; } } 

		public LodQuadTreeNode CurrentNode { get { return m_CurrentNode; } }

		public UpscaleWinding Winding { get; set; } 

		
		public float Distance;

		public TerrainPatch(LodQuadTree tree)
		{
			m_Tree = tree; 

			m_BlockSize = tree.BlockSize;

			int totalSize = (m_BlockSize + 1) * (m_BlockSize + 1);

			m_Buffer = new VertexBuffer("Patch", ResourceMode.Dynamic, new VertexBufferInfo(TerrainVertex.Format, totalSize, OpenTK.Graphics.OpenGL.BufferUsageHint.StreamDraw)); 
		}

		public void LoadResources()
		{
			m_Buffer.LoadResources(); 
		}

		public void UnloadResources()
		{
			m_Buffer.UnloadResources(); 
		}

		public void AssignNode(LodQuadTreeNode node)
		{
			if (m_CurrentNode != null)
			{
				m_CurrentNode.Patch = null; 
			}

			m_CurrentNode = node;

			if (m_CurrentNode == null)
			{
				m_NodeID = 0;
			}
			else
			{
				m_NodeID = m_CurrentNode.ID;

				m_CurrentNode.Patch = this; 
			}

			m_HasPatchFinishedLoading = false; 
		}

		public void UnassignNode()
		{
			if (m_CurrentNode != null)
			{
				m_CurrentNode.Patch = null;
			}

			m_NodeID = 0;
			m_CurrentNode = null; 

			m_HasPatchFinishedLoading = true; 
		}

		public void LoadPatchData()
		{
			m_Tree.DataProvider.Write(this, m_NodeID);

			m_HasPatchFinishedLoading = true; 
		}

		public void Bind()
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Buffer.ResourceHandle);
			TerrainVertex.Bind();			
		}

		public void Unbind()
		{
			TerrainVertex.Unbind();
		}

		public void DeterminWinding()
		{		
			if (CurrentNode == null) 
			{
				Winding = UpscaleWinding.None; 
				return; 
			}

			UpscaleWinding winding = UpscaleWinding.None; 

			if (CurrentNode.North != null)
			{
				if (CurrentNode.North.AreChildrenVisible == false &&
					(CurrentNode.North.Patch == null || 
					(CurrentNode.North.Patch != null && CurrentNode.North.Patch.IsVisible == false)))
				{
					winding |= UpscaleWinding.North; 
				}
			}

			if (CurrentNode.South != null)
			{
				if (CurrentNode.South.AreChildrenVisible == false &&
					(CurrentNode.South.Patch == null ||
					(CurrentNode.South.Patch != null && CurrentNode.South.Patch.IsVisible == false)))
				{
					winding |= UpscaleWinding.South;
				}
			}

			if (CurrentNode.East != null)
			{
				if (CurrentNode.East.AreChildrenVisible == false &&
					(CurrentNode.East.Patch == null ||
					(CurrentNode.East.Patch != null && CurrentNode.East.Patch.IsVisible == false)))
				{
					winding |= UpscaleWinding.East;
				}
			}

			if (CurrentNode.West != null)
			{
				if (CurrentNode.West.AreChildrenVisible == false &&
					(CurrentNode.West.Patch == null ||
					(CurrentNode.West.Patch != null && CurrentNode.West.Patch.IsVisible == false)))
				{
					winding |= UpscaleWinding.West;
				}
			}

			this.Winding = winding; 
		}
	}
}
