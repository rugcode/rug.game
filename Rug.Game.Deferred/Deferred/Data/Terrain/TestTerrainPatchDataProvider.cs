﻿

/* 
namespace Rug.Game.Deferred.Data.Terrain
{
	public class TestTerrainPatchDataProvider : ITerrainPatchDataProvider
	{
		Random m_Rand = new Random(09655); // 3453);
		//PixelBuffer m_Buffer;

		float[,] m_HeightMap;
		//Vector3[,] m_Normals;

		int m_TotalWidth;
		private int m_MaxLevels;
		float m_MapScale = 2048f; // 1024f; // 512f; // 128f; // 64f; // 32f; 
		//float m_HeightScale = 400f;
		float m_HeightScale = 20f;
		float m_HeightOffset = 300f; // -200f;
		//bool m_ReadFromDisc = false; 

		TerrainVertex[] m_Verts; 
		Leaf[] m_LeafPath = new Leaf[10];
		Color4[] m_LevelColors = new Color4[] { 
			new Color4(178, 51, 51, 0), 
			new Color4(170, 96, 52, 0), 
			new Color4(168, 137, 52, 0), 
			new Color4(154, 165, 51, 0), 
			new Color4(110, 163, 50, 0), 
			new Color4(68, 160, 49, 0), 
			new Color4(49, 158, 70, 0), 
			new Color4(49, 155, 109, 0), 
			new Color4(48, 153, 144, 0),
			new Color4(48, 125, 153, 0),
			new Color4(48, 125, 153, 0),
			new Color4(48, 87, 153, 0),
		};
		private int m_MaxStepSize;
		private Texture2D m_NormalMap;
		private Texture2D m_TangentMap;

		public float MapScale
		{
			get { return m_MapScale; }
		}

		public int BlockSize
		{
			get { return 32; }
		}

		public Texture2D NormalMap
		{
			get { return m_NormalMap; } 
		}

		public Texture2D TangentMap
		{
			get { return m_TangentMap; }
		}

		public TestTerrainPatchDataProvider(int levels)
		{
			m_MaxLevels = levels;
			m_MaxStepSize = (int)Math.Pow(2, m_MaxLevels);
			m_TotalWidth = (BlockSize * m_MaxStepSize) + 1;

			//m_Buffer = new PixelBuffer(m_TotalWidth, m_TotalWidth, 1);

			//m_Buffer.MakeNoise(4, 0.5f, 5f); //(float)m_Rand.NextDouble(), (float)m_Rand.NextDouble());			
			//m_Buffer.MakeNoise((uint)m_Rand.Next(1, 400), (float)m_Rand.NextDouble(), (float)m_Rand.NextDouble() * 10f); //(float)m_Rand.NextDouble(), (float)m_Rand.NextDouble());			
			//m_Buffer.MakeNoise(42, 0.777f, 6.87f); //(float)m_Rand.NextDouble(), (float)m_Rand.NextDouble());			
			RiggedMultifractal dunesFrac = new RiggedMultifractal();
			dunesFrac.NumberOfOctaves = 5;
			dunesFrac.Lacunarity = (float)m_Rand.NextDouble() * 2f; // 1.36f;
			dunesFrac.Gain = 1.7f; //1.5f;
			dunesFrac.HScale = 0.008f; // 0.008f; // 0.02f;

			RiggedMultifractal hillsFrac = new RiggedMultifractal();
			hillsFrac.NumberOfOctaves = 4;
			hillsFrac.Lacunarity = (float)m_Rand.NextDouble() * 2f; // 1.36f;
			hillsFrac.Gain = 6.8f;
			hillsFrac.HScale = 0.001f; // 0.001f; 

			m_HeightMap = new float[m_TotalWidth, m_TotalWidth];

			string heightMapFilePath = Helper.ResolvePath("~/Data/HeightMap.hm");

			if (File.Exists(heightMapFilePath) == false)
			{
				//double xOffset = 1536, yOffset = 1536;
				double xOffset = 2048, yOffset = 2048; 

				for (int g = 0; g < m_TotalWidth; g++)
				{
					for (int r = 0; r < m_TotalWidth; r++)
					{
						//m_HeightMap[r, g] = frac.RidgedMF((double)r / (double)m_TotalWidth, (double)g / (double)m_TotalWidth) * m_HeightScale; // (float)(m_Rand.NextDouble() * 3f);
						m_HeightMap[r, g] = -(hillsFrac.RidgedMF((double)r + xOffset, (double)g + yOffset) + dunesFrac.RidgedMF((double)r + xOffset, (double)g + yOffset)); // (float)(m_Rand.NextDouble() * 3f);
					}
				}

				using (FileStream file = new FileStream(heightMapFilePath, FileMode.Create, FileAccess.Write))
				using (BinaryWriter writer = new BinaryWriter(file))
				{
					for (int g = 0; g < m_TotalWidth; g++)
					{
						for (int r = 0; r < m_TotalWidth; r++)
						{
							//m_HeightMap[r, g] = frac.RidgedMF((double)r / (double)m_TotalWidth, (double)g / (double)m_TotalWidth) * m_HeightScale; // (float)(m_Rand.NextDouble() * 3f);
							writer.Write(m_HeightMap[r, g]);
						}
					}
				}
			}
			else
			{
				using (FileStream file = new FileStream(heightMapFilePath, FileMode.Open, FileAccess.Read))
				using (BinaryReader reader = new BinaryReader(file))
				{
					for (int g = 0; g < m_TotalWidth; g++)
					{
						for (int r = 0; r < m_TotalWidth; r++)
						{
							//m_HeightMap[r, g] = frac.RidgedMF((double)r / (double)m_TotalWidth, (double)g / (double)m_TotalWidth) * m_HeightScale; // (float)(m_Rand.NextDouble() * 3f);
							m_HeightMap[r, g] = reader.ReadSingle(); // *-1f;
						}
					}
				}
			}
			

			m_HeightOffset = -m_HeightMap[m_TotalWidth / 2, m_TotalWidth / 2]; 

			//m_Buffer.CopyTo(m_HeightMap, m_HeightScale, 0); 

			m_Verts = new TerrainVertex[(BlockSize + 1) * (BlockSize + 1)];

			m_NormalMap = new Texture2D("Terrain Normal Map", Resources.ResourceMode.Static, new Texture2DInfo()
			{
				 Border = 0, 
				 InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgb16f, 
				 MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear, 
				 MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear, 
				 PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgb, 
				 PixelType = OpenTK.Graphics.OpenGL.PixelType.Float, 
				 Size = new System.Drawing.Size((BlockSize * m_MaxStepSize), (BlockSize * m_MaxStepSize)),
				 WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				 WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});


			m_TangentMap = new Texture2D("Terrain Tangent Map", Resources.ResourceMode.Static, new Texture2DInfo()
			{
				Border = 0,
				InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba16f,
				MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear,
				MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear,
				PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba,
				PixelType = OpenTK.Graphics.OpenGL.PixelType.Float,
				Size = new System.Drawing.Size((BlockSize * m_MaxStepSize), (BlockSize * m_MaxStepSize)),
				WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			}); 
		}

		public void PopulateNormalMap()
		{
			int size = (BlockSize * m_MaxStepSize);

			Vector3[] waterHeights = new Vector3[size * size]; 
			
			int index = 0;
			for (int g = 0; g < size; g++)
			{
				for (int r = 0; r < size; r++)
				{
					waterHeights[index++] = FetchNormalVector(new Vector2(r, g), 1, 1, false, false); 
				}
			}	

			GCHandle data_ptr = GCHandle.Alloc(waterHeights, GCHandleType.Pinned);

			IntPtr ptr = data_ptr.AddrOfPinnedObject();

			m_NormalMap.UploadImage(ptr); 
			
			data_ptr.Free();	
		}

		
		public void PopulateTangentMap()
		{
			int size = (BlockSize * m_MaxStepSize);

			Vector4[] tangents = new Vector4[size * size];
			
			float hToN = 1f / (float)(BlockSize * m_MaxStepSize); 

			int index = 0;
			for (int g = 0; g < size; g++)
			{
				for (int r = 0; r < size; r++)
				{
					//waterHeights[index++] = FetchNormalVector(new Vector2(r, g), 1, 1, false, false);

					Vector4 tangent = new Vector4();
					calcTangentVector(new Vector2(r, g),
										new Vector2((float)r * hToN, (float)g * hToN),
										new Vector2((float)(r + 1) * hToN, (float)g * hToN),
										new Vector2((float)r * hToN, (float)(g + 1) * hToN),
										1, 1,
										FetchNormalVector(new Vector2(r, g), 1, 1, false, false), ref tangent);

					tangents[index++] = tangent;
				}
			}

			GCHandle data_ptr = GCHandle.Alloc(tangents, GCHandleType.Pinned);

			IntPtr ptr = data_ptr.AddrOfPinnedObject();

			m_TangentMap.UploadImage(ptr);

			data_ptr.Free();
		}
		

		public Maths.BoundingBox GetBounds(uint id)
		{
			/* 
			uint level;
			LodQuadTree.UnpackLeafPath(id, m_LeafPath, out level);

			float scaleX = m_MapScale;
			float scaleY = m_MapScale; 

			float sizeX = BlockSize * scaleX;
			float sizeY = BlockSize * scaleY;

			float fx = sizeX * -0.5f;
			float fy = sizeY * -0.5f; 					

			for (int i = 1; i <= level; i++)
			{
				scaleX *= 0.5f;
				scaleY *= 0.5f;

				sizeX /= 2;
				sizeY /= 2;

				switch (m_LeafPath[i - 1])
				{
					case Leaf.NorthWest:
						break;
					case Leaf.NorthEast:
						fx += BlockSize * scaleX;
						break;
					case Leaf.SouthWest:
						fy += BlockSize * scaleY;
						break;
					case Leaf.SouthEast:
						fx += BlockSize * scaleX;
						fy += BlockSize * scaleY;
						break;
					default:
						throw new Exception("Error, Invalid Leaf");
				}
			}

			return new Maths.BoundingBox(new Vector3(fx, 0f, fy), new Vector3(fx + sizeX + (1f * scaleX), 200f, fy + sizeY + (1f * scaleY))); 
			* /

			uint level;
			LodQuadTree.UnpackLeafPath(id, m_LeafPath, out level);

			float scaleX = m_MapScale;
			float scaleY = m_MapScale;

			float sizeX = (BlockSize) * scaleX;
			float sizeY = (BlockSize) * scaleY;

			float fx = sizeX * -0.5f;
			float fy = sizeY * -0.5f;

			int stepX = m_MaxStepSize, stepY = m_MaxStepSize;
			int x0 = 0, y0 = 0;

			for (int i = 1; i <= level; i++)
			{
				scaleX *= 0.5f;
				scaleY *= 0.5f;

				stepX /= 2;
				stepY /= 2;

				switch (m_LeafPath[i - 1])
				{
					case Leaf.NorthWest:
						break;
					case Leaf.NorthEast:
						fx += BlockSize * scaleX;
						x0 += BlockSize * stepX;
						break;
					case Leaf.SouthWest:
						fy += BlockSize * scaleY;
						y0 += BlockSize * stepY;
						break;
					case Leaf.SouthEast:
						fx += BlockSize * scaleX;
						fy += BlockSize * scaleY;
						x0 += BlockSize * stepX;
						y0 += BlockSize * stepY;
						break;
					default:
						throw new Exception("Error, Invalid Leaf");
				}
			}

			int index = 0;

			Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
			Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue); 

			int width = BlockSize + 1;

			for (int g = 0; g < BlockSize + 1; g++)
			{
				int hy = y0 + (g * stepY);

				for (int r = 0; r < BlockSize + 1; r++)
				{
					int hx = x0 + (r * stepX);

					Vector3 vec = new Vector3(fx + ((float)r * scaleX), Sample(hx, hy), fy + ((float)g * scaleY));

					min = Vector3.ComponentMin(min, vec);
					max = Vector3.ComponentMax(max, vec); 

					index++;
				}
			}

			return new Maths.BoundingBox(min, max); 
		}

		public void Write(TerrainPatch patch, uint id)
		{
			DataStream stream;
			patch.Buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);
			
			uint level;
			LodQuadTree.UnpackLeafPath(id, m_LeafPath, out level);

			float scaleX = m_MapScale;
			float scaleY = m_MapScale;

			float sizeX = (BlockSize) * scaleX;
			float sizeY = (BlockSize) * scaleY;

			float fx = sizeX * -0.5f;
			float fy = sizeY * -0.5f;

			int stepX = m_MaxStepSize, stepY = m_MaxStepSize; 
			int x0 = 0, y0 = 0; 

			for (int i = 1; i <= level; i++)
			{
				scaleX *= 0.5f;
				scaleY *= 0.5f;
				
				stepX /= 2; 
				stepY /= 2; 

				switch (m_LeafPath[i - 1])
				{
					case Leaf.NorthWest:
						break;
					case Leaf.NorthEast:
						fx += BlockSize * scaleX;
						x0 += BlockSize * stepX; 
						break;
					case Leaf.SouthWest:
						fy += BlockSize * scaleY;
						y0 += BlockSize * stepY; 
						break;
					case Leaf.SouthEast:
						fx += BlockSize * scaleX;
						fy += BlockSize * scaleY;
						x0 += BlockSize * stepX; 
						y0 += BlockSize * stepY; 
						break;
					default:
						throw new Exception("Error, Invalid Leaf");
				}
			}

			Color4 color = m_LevelColors[(int)level];

			color.A = 0f;

			float nScaleXZ = scaleX;
			float nScaleY = 1f;

			int index = 0;

			int width = BlockSize + 1;

			float hToN = 1f / (float)(BlockSize * m_MaxStepSize); 

			for (int g = 0; g < BlockSize + 1; g++)
			{
				int hy = y0 + (g * stepY); 

				for (int r = 0; r < BlockSize + 1; r++)
				{
					int hx = x0 + (r * stepX);
					m_Verts[index].Color = color;
					m_Verts[index].Position = new Vector3(fx + ((float)r * scaleX), Sample(hx, hy), fy + ((float)g * scaleY));
					m_Verts[index].TextureCoords = new Vector2((float)hx * hToN, (float)hy * hToN);
					//m_Verts[index].Normal = FetchNormalVector(new Vector2(hx, hy), stepX, stepY, false, false);

					/* Vector4 tangent = new Vector4();
					calcTangentVector(new Vector2(fx + ((float)r * scaleX), fy + ((float)g * scaleY)),
										new Vector2((float)hx * hToN, (float)hy * hToN),
										new Vector2((float)(hx + stepX) * hToN, (float)hy * hToN),
										new Vector2((float)hx * hToN, (float)(hy + stepY) * hToN),
										stepX, stepY,
										FetchNormalVector(new Vector2(hx, hy), stepX, stepY, false, false), ref tangent);					
					*/
					/* 
					if ((r < 1 || r >= BlockSize) ||
						(g < 1 || g >= BlockSize) || 
						stepX == 1)
					{
						m_Verts[index].Color = color;
						m_Verts[index].Position = new Vector3(fx + ((float)r * scaleX), Sample(hx, hy), fy + ((float)g * scaleY));
						m_Verts[index].TextureCoords = new Vector2((float)hx * hToN, (float)hy * hToN);
						m_Verts[index].Normal = FetchNormalVector(new Vector2(hx, hy), stepX, stepY, false, false);
					}
					else
					{
						Vector3 peak = Vector3.Zero; 

						for (int sy = 0; sy < stepY; sy++)
						{
							for (int sx = 0; sx < stepX; sx++)
							{
								peak += new Vector3(fx + ((float)r * scaleX) + sx, Sample(hx + sx, hy + sy), fy + ((float)g * scaleY) + sy);
							}
						}

						peak /= (float)(stepX * stepY);

						m_Verts[index].Color = color;
						m_Verts[index].Position = peak;
						m_Verts[index].TextureCoords = new Vector2((float)hx * hToN, (float)hy * hToN);
						m_Verts[index].Normal = FetchNormalVector(new Vector2(hx, hy), stepX, stepY, false, false);
					}
					*/ 
					/* 
					float sx = m_HeightMap[hx < m_TotalWidth - 1 ? hx + 1 : hx, hy] - m_HeightMap[hx > 0 ? hx - 1 : hx, hy];

					if (hx == 0 || hx == width - 1)
					{
						sx *= 2;
					}

					float sy = m_HeightMap[hx, hy < m_TotalWidth - 1 ? hy + 1 : hy] - m_HeightMap[hx, hy > 0 ? hy - 1 : hy];

					if (g == 0 || g == m_TotalWidth - 1)
					{
						sy *= 2;
					}

					Vector3 normal = new Vector3(-sx * nScaleY, -2 * nScaleXZ, sy * nScaleY);

					normal.Normalize();

					m_Verts[index].Normal = normal;	
					* /					

					index++;
				}
			}

			/* 
			uint level;
			LodQuadTree.UnpackLeafPath(id, m_LeafPath, out level);

			float yScale = 8f;
			float xzScale = 8f;
			Color4 color = new Color4(0.2f, 0.9f, 0.1f, 0f);
			float fx = -128f;
			float fy = -128f;
			int x0 = 0;
			int y0 = 0;
			int xStep = 2;
			int yStep = 2;

			if (level == 0)
			{
				fx = -128f;
				fy = -128f;
				color = new Color4(0.2f, 0.9f, 0.1f, 0f);
				yScale = 8f;
				xzScale = 8f;
			}
			else 
			{
				for (int i = 1; i < level; i++)
				{
					color = new Color4(0.9f, 0.2f, 0.1f, 0f);
					
					yScale *= 0.5f;
					xzScale *= 0.5f;

					xStep /= 2;
					yStep /= 2;

					switch (m_LeafPath[i - 1])
					{
						case Leaf.NorthWest:							
							break;
						case Leaf.NorthEast:
							x0 += BlockSize * xStep;
							break;
						case Leaf.SouthWest:
							y0 += BlockSize * yStep;
							break;
						case Leaf.SouthEast:
							x0 += BlockSize * xStep;
							y0 += BlockSize * yStep;
							break;
						default:
							throw new Exception("Error, Invalid Leaf"); 
					}
				}
			}
			
			int index = 0;
			
			int width = BlockSize + 1;

			for (int g = 0; g < BlockSize + 1; g++)
			{
				for (int r = 0; r < BlockSize + 1; r++)
				{
					m_Verts[index].Color = color;
					m_Verts[index].Position = new Vector3(fx + ((float)r * xzScale), m_HeightMap[r, g], fy + ((float)g * xzScale));

					float sx = m_HeightMap[r < width - 1 ? r + 1 : r, g] - m_HeightMap[r > 0 ? r - 1 : r, g];
					
					if (r == 0 || r == width - 1)
					{
						sx *= 2;
					}

					float sy = m_HeightMap[r, g < width - 1 ? g + 1 : g] - m_HeightMap[r, g > 0 ? g - 1 : g];
					
					if (g == 0 || g == width - 1)
					{
						sy *= 2;
					}

					Vector3 normal = new Vector3(-sx * yScale, -2 * xzScale, sy * yScale);
					
					normal.Normalize();

					m_Verts[index].Normal = normal;
					

					index++; 
				}
			}
			* / 

			stream.WriteRange(m_Verts, 0, index);

			patch.Buffer.UnmapBuffer(); 
		}

		public bool HasChildren(uint id)
		{
			return LodQuadTree.UnpackLeafLevel(id) < m_MaxLevels;
		}

		float Sample(int r, int g)
		{
			r = r < 0 ? 0 : r >= m_TotalWidth ? m_TotalWidth - 1 : r; 
			g = g < 0 ? 0 : g >= m_TotalWidth ? m_TotalWidth - 1 : g;

			return m_HeightMap[r, g] + m_HeightOffset; 
		}

		Vector3 FetchNormalVector(Vector2 tc, int vPixelSizex, int vPixelSizey, bool readFromTexture, bool useSobelFilter)
		{
			if (readFromTexture == true)
			{
				// Use the simple pre-computed look-up
				//float3 n = texNormalMap.Sample( DefaultSampler, tc ).rgb;
				//return normalize( n * 2.0f - 1.0f );
				return Vector3.UnitY; 
			}
			else
			{
				if (useSobelFilter == true)
				{   
					/*
					Coordinates are laid out as follows:
			
						0,0 | 1,0 | 2,0
						----+-----+----
						0,1 | 1,1 | 2,1
						----+-----+----
						0,2 | 1,2 | 2,2
					* /
			
					// Compute the necessary offsets:
					Vector2 o00 = tc + new Vector2(-vPixelSizex, -vPixelSizey);
					Vector2 o10 = tc + new Vector2(0.0f, -vPixelSizey);
					Vector2 o20 = tc + new Vector2(vPixelSizex, -vPixelSizey);

					Vector2 o01 = tc + new Vector2(-vPixelSizex, 0.0f);
					Vector2 o21 = tc + new Vector2(vPixelSizex, 0.0f);

					Vector2 o02 = tc + new Vector2(-vPixelSizex, vPixelSizey);
					Vector2 o12 = tc + new Vector2(0.0f, vPixelSizey);
					Vector2 o22 = tc + new Vector2(vPixelSizex, vPixelSizey);

					// Use of the sobel filter requires the eight samples
					// surrounding the current pixel:
					float h00 = Sample((int)o00.X, (int)o00.Y);
					float h10 = Sample((int)o10.X, (int)o10.Y);
					float h20 = Sample((int)o20.X, (int)o20.Y);

					float h01 = Sample((int)o01.X, (int)o01.Y);
					float h21 = Sample((int)o21.X, (int)o21.Y);

					float h02 = Sample((int)o02.X, (int)o02.Y);
					float h12 = Sample((int)o12.X, (int)o12.Y);
					float h22 = Sample((int)o22.X, (int)o22.Y);
			
					// The Sobel X kernel is:
					//
					// [ 1.0  0.0  -1.0 ]
					// [ 2.0  0.0  -2.0 ]
					// [ 1.0  0.0  -1.0 ]
			
					float Gx = h00 - h20 + 2.0f * h01 - 2.0f * h21 + h02 - h22;
						
					// The Sobel Y kernel is:
					//
					// [  1.0    2.0    1.0 ]
					// [  0.0    0.0    0.0 ]
					// [ -1.0   -2.0   -1.0 ]
			
					float Gy = h00 + 2.0f * h10 + h20 - h02 - 2.0f * h12 - h22;
			
					// Generate the missing Z component - tangent
					// space waterHeights are +Z which makes things easier
					// The 0.5f leading coefficient can be used to control
					// how pronounced the bumps are - less than 1.0 enhances
					// and greater than 1.0 smoothes.
					float Gz = 0.5f * (float)Math.Sqrt( 1.0f - Gx * Gx - Gy * Gy );

					// Make sure the returned normal is of unit length
					Vector3 normal = new Vector3( 2.0f * Gx, Gz, 2.0f * Gy);

					normal.Normalize();

					return normal;
				}
				else
				{
					// Determine the offsets
					Vector2 o1 = new Vector2(vPixelSizex, 0.0f);
					Vector2 o2 = new Vector2(0.0f, vPixelSizey);

					// Take three samples to determine two vectors that can be
					// use to generate the normal at this pixel
					float h0 = Sample((int)tc.X, (int)tc.Y);
					float h1 = Sample((int)(tc + o1).X, (int)(tc + o1).Y);
					float h2 = Sample((int)(tc + o2).X, (int)(tc + o2).Y);

					Vector3 v01 = new Vector3(o1.X, h0 - h1, o1.Y);
					Vector3 v02 = new Vector3(o2.X, h0 - h2, o2.Y);

					Vector3 n = Vector3.Cross(v01, v02); 

					// Can be useful to scale the Z component to tweak the
					// amount bumps show up, less than 1.0 will make them
					// more apparent, greater than 1.0 will smooth them out
					//n.Z *= 0.5f;
					//n.Y *= -1f;

					n.Normalize();

					//n = Vector3.UnitY; 

					return n;
				}
			}
		}

		const float EPSILON = 1e-6f;

		static bool closeEnough(float f1, float f2)
		{
			// Determines whether the two floating-point values f1 and f2 are
			// close enough together that they can be considered equal.			
			return Math.Abs((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < EPSILON;
		}

		void calcTangentVector(Vector2 tc,
										Vector2 texCoord1, Vector2 texCoord2, Vector2 texCoord3,
										int vPixelSizex, int vPixelSizey,
										Vector3 normal, ref Vector4 tangent)
		{
			Vector2 o1 = new Vector2(vPixelSizex, 0.0f);
			Vector2 o2 = new Vector2(0.0f, vPixelSizey);

			// Take three samples to determine two vectors that can be
			// use to generate the normal at this pixel
			float h0 = Sample((int)tc.X, (int)tc.Y);
			float h1 = Sample((int)(tc + o1).X, (int)(tc + o1).Y);
			float h2 = Sample((int)(tc + o2).X, (int)(tc + o2).Y);

			Vector3 pos1 = new Vector3(tc.X, h0, tc.Y);
			Vector3 pos2 = new Vector3(tc.X + o1.X, h1, tc.Y + o1.Y);
			Vector3 pos3 = new Vector3(tc.X + o2.X, h2, tc.Y + o2.Y);

			// Given the 3 vertices (position and texture coordinates) of a triangle
			// calculate and return the triangle's tangent vector.

			// Create 2 vectors in object space.
			//
			// edge1 is the vector from vertex positions pos1 to pos2.
			// edge2 is the vector from vertex positions pos1 to pos3.
			Vector3 edge1 = new Vector3(pos2.X - pos1.X, pos2.Y - pos1.Y, pos2.Z - pos1.Z);
			Vector3 edge2 = new Vector3(pos3.X - pos1.X, pos3.Y - pos1.Y, pos3.Z - pos1.Z);

			edge1.Normalize();
			edge2.Normalize();

			// Create 2 vectors in tangent (texture) space that point in the same
			// direction as edge1 and edge2 (in object space).
			//
			// texEdge1 is the vector from texture coordinates texCoord1 to texCoord2.
			// texEdge2 is the vector from texture coordinates texCoord1 to texCoord3.
			Vector2 texEdge1 = new Vector2(texCoord2.X - texCoord1.X, texCoord2.Y - texCoord1.Y);
			Vector2 texEdge2 = new Vector2(texCoord3.X - texCoord1.X, texCoord3.Y - texCoord1.Y);

			texEdge1.Normalize();
			texEdge2.Normalize();

			// These 2 sets of vectors form the following system of equations:
			//
			//  edge1 = (texEdge1.r * tangent) + (texEdge1.g * bitangent)
			//  edge2 = (texEdge2.r * tangent) + (texEdge2.g * bitangent)
			//
			// Using matrix notation this system looks like:
			//
			//  [ edge1 ]     [ texEdge1.r  texEdge1.g ]  [ tangent   ]
			//  [       ]  =  [                        ]  [           ]
			//  [ edge2 ]     [ texEdge2.r  texEdge2.g ]  [ bitangent ]
			//
			// The solution is:
			//
			//  [ tangent   ]        1     [ texEdge2.g  -texEdge1.g ]  [ edge1 ]
			//  [           ]  =  -------  [                         ]  [       ]
			//  [ bitangent ]      det A   [-texEdge2.r   texEdge1.r ]  [ edge2 ]
			//
			//  where:
			//        [ texEdge1.r  texEdge1.g ]
			//    A = [                        ]
			//        [ texEdge2.r  texEdge2.g ]
			//
			//    det A = (texEdge1.r * texEdge2.g) - (texEdge1.g * texEdge2.r)
			//
			// From this solution the tangent space basis vectors are:
			//
			//    tangent = (1 / det A) * ( texEdge2.g * edge1 - texEdge1.g * edge2)
			//  bitangent = (1 / det A) * (-texEdge2.r * edge1 + texEdge1.r * edge2)
			//     normal = cross(tangent, bitangent)

			Vector3 t = new Vector3(0, 0, 0);
			Vector3 b = new Vector3(0, 0, 0);
			Vector3 n = new Vector3(normal.X, normal.Y, normal.Z);

			float det = (texEdge1.X * texEdge2.Y) - (texEdge1.Y * texEdge2.X);

			if (closeEnough(det, 0.0f) == true)
			{
				t = new Vector3(1.0f, 0.0f, 0.0f);
				b = new Vector3(0.0f, 1.0f, 0.0f);
			}
			else
			{
				det = 1.0f / det;

				t.X = (texEdge2.Y * edge1.X - texEdge1.Y * edge2.X) * det;
				t.Y = (texEdge2.Y * edge1.Y - texEdge1.Y * edge2.Y) * det;
				t.Z = (texEdge2.Y * edge1.Z - texEdge1.Y * edge2.Z) * det;

				b.X = (-texEdge2.X * edge1.X + texEdge1.X * edge2.X) * det;
				b.Y = (-texEdge2.X * edge1.Y + texEdge1.X * edge2.Y) * det;
				b.Z = (-texEdge2.X * edge1.Z + texEdge1.X * edge2.Z) * det;

				t.Normalize();
				b.Normalize();
			}

			// Calculate the handedness of the local tangent space.
			// The bitangent vector is the cross product between the triangle face
			// normal vector and the calculated tangent vector. The resulting bitangent
			// vector should be the same as the bitangent vector calculated from the
			// set of linear equations above. If they point in different directions
			// then we need to invert the cross product calculated bitangent vector. We
			// store this scalar multiplier in the tangent vector's 'a' component so
			// that the correct bitangent vector can be generated in the normal mapping
			// shader's vertex shader.

			Vector3 bitangent = Vector3.Cross(n, t);
			float handedness = (Vector3.Dot(bitangent, b) < 0.0f) ? -1.0f : 1.0f;

			tangent.X = t.X;
			tangent.Y = t.Y;
			tangent.Z = t.Z;
			tangent.W = handedness; // *-1f;
		}
	}
}
*/ 