﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Maths;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Shaders.Deferred;
using System;
using System.Collections.Generic;
using System.Drawing;


namespace Rug.Game.Deferred.Data.Terrain
{
	public enum Leaf : int 
	{
		Parent = 0, 
		NorthWest = 1, 
		NorthEast = 2, 
		SouthWest = 3, 
		SouthEast = 4, 
	}

	public enum Cardinal
	{
		North,
		South,
		West,
		East,
	}

	public enum UpscaleWinding : int
	{
		None = 0, 
		North = 1,
		South = 2, 
		West = 4, 		
		East = 8,
	}

	public class LodQuadTree : IResource
	{
		public static bool UpdatePatches = true;
		public static bool ShowShouldSplit = false;
		public static bool ShowAllowSplit = false;

		#region Private Members

		private bool m_IsLoaded = false; 

		private ITerrainPatchDataProvider m_DataProvider;

		private int m_BlockSize; 

		private LodQuadTreeNode m_Root;

		private PatchWindingBuffer[] m_PatchWindings = new PatchWindingBuffer[(int)(UpscaleWinding.North | UpscaleWinding.South | UpscaleWinding.East | UpscaleWinding.West) + 1]; 

		private List<TerrainPatch> m_InactivePatches = new List<TerrainPatch>(); 
		private List<TerrainPatch> m_ActivePatches = new List<TerrainPatch>();

		#endregion 

		#region public Properties

		public string Name
		{
			get { return "Quad Tree"; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Dynamic; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		

		public int BlockSize { get { return m_BlockSize; } }

		public ITerrainPatchDataProvider DataProvider { get { return m_DataProvider; } } 

		#endregion

		public LodQuadTree(ITerrainPatchDataProvider dataProvider, int intialPatchCount)
		{
			m_DataProvider = dataProvider; 

			m_BlockSize = m_DataProvider.BlockSize; 

			BuildQuadTree();

			LinkQuadTree(); 

			AllocatePatches(intialPatchCount);

			AllocateIndexBuffers();

			AssignPatchToNode(m_Root);
			
			MakeChildrenVisible(m_Root);
				
			LodQuadTreeNode sub = m_Root.NorthWest;

			int count = 0;

			while (sub != null && count++ < 2)
			{
				MakeChildrenVisible(sub);

				sub = sub.NorthEast; 				
			}

			// thresh = 4f; 
			// A = 1f / (near * (m23+1)/m22);
			// T = (2 * thresh) / 600f
			// C = A / T 
			// d = Pow(Sum(HeightDelta), 2) * Pow(C, 2)


			m_Rocks1 = new BitmapTexture2D("Rocks2", "~/Data/Assets/sand2.png", ResourceMode.Static, new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.Rgba,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.Linear,
				PixelFormat = PixelFormat.Bgra,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.Repeat,
				WrapT = TextureWrapMode.Repeat,
				Size = new Size(1024, 1024)
			});

			m_Rocks1_N = new BitmapTexture2D("Rocks2_n", "~/Data/Assets/sand2_n.png", ResourceMode.Static, new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.Rgba,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.Linear,
				PixelFormat = PixelFormat.Bgra,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.Repeat,
				WrapT = TextureWrapMode.Repeat,
				Size = new Size(1024, 1024)
			});

			m_Rocks1_S = new BitmapTexture2D("Rocks2_s", "~/Data/Assets/sand2_s.png", ResourceMode.Static, new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.R8,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.Linear,
				PixelFormat = PixelFormat.Red,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.Repeat,
				WrapT = TextureWrapMode.Repeat,
				Size = new Size(1024, 1024)
			});

			Environment.Resources.Add(m_Rocks1);
			Environment.Resources.Add(m_Rocks1_N);
			Environment.Resources.Add(m_Rocks1_S);
		}

		private void AssignPatchToNode(LodQuadTreeNode node)
		{
			TerrainPatch patch = m_InactivePatches[m_InactivePatches.Count - 1];

			m_InactivePatches.RemoveAt(m_InactivePatches.Count - 1);

			patch.AssignNode(node);

			m_ActivePatches.Add(patch);

			if (patch.IsLoaded == true)
			{
				patch.LoadPatchData(); 
			}

			patch.IsVisible = true; 
		}		

		private void MakeChildrenVisible(LodQuadTreeNode node)
		{
			if (node.HasChildren == false)
			{
				return; 
			}

			if (m_InactivePatches.Count < 4)
			{
				return;
			}
			
			AssignPatchToNode(node.NorthEast);
			AssignPatchToNode(node.NorthWest);
			AssignPatchToNode(node.SouthEast);
			AssignPatchToNode(node.SouthWest);

			node.Patch.IsVisible = false;
			node.AreChildrenVisible = true;
		}

		private void UnassignPatchToNode(LodQuadTreeNode node)
		{
			TerrainPatch patch = node.Patch;

			if (patch != null)
			{
				patch.UnassignNode();

				if (m_ActivePatches.Remove(patch))
				{
					m_InactivePatches.Add(patch);
				}
			}

			patch.IsVisible = false;
		}	

		private void CollapseChildren(LodQuadTreeNode node)
		{
			if (node.HasChildren == false)
			{
				return;
			}

			if (node.NorthEast.AreChildrenVisible == true)
			{
				CollapseChildren(node.NorthEast); 
			}

			UnassignPatchToNode(node.NorthEast);

			if (node.NorthWest.AreChildrenVisible == true)
			{
				CollapseChildren(node.NorthWest);
			}

			UnassignPatchToNode(node.NorthWest);

			if (node.SouthEast.AreChildrenVisible == true)
			{
				CollapseChildren(node.SouthEast);
			}

			UnassignPatchToNode(node.SouthEast);

			if (node.SouthWest.AreChildrenVisible == true)
			{
				CollapseChildren(node.SouthWest);
			}

			UnassignPatchToNode(node.SouthWest);

			node.Patch.IsVisible = true;
			node.AreChildrenVisible = false;
		}

		private void BuildQuadTree()
		{
			m_Root = new LodQuadTreeNode(0, Leaf.Parent, 0, m_DataProvider.GetBounds(0), null, m_BlockSize);

			List<LodQuadTreeNode> remainingNodes = new List<LodQuadTreeNode>();

			remainingNodes.Add(m_Root);

			while (remainingNodes.Count > 0)
			{
				LodQuadTreeNode node = remainingNodes[0];

				if (m_DataProvider.HasChildren(node.ID) == true)
				{
					node.HasChildren = true; 

					uint level = node.Level + 1;
					uint nwID = PackID(node.ID, level, Leaf.NorthWest);
					LodQuadTreeNode nw = new LodQuadTreeNode(nwID, Leaf.NorthWest, level, m_DataProvider.GetBounds(nwID), node, m_BlockSize);

					uint neID = PackID(node.ID, level, Leaf.NorthEast);
					LodQuadTreeNode ne = new LodQuadTreeNode(neID, Leaf.NorthEast, level, m_DataProvider.GetBounds(neID), node, m_BlockSize);

					uint swID = PackID(node.ID, level, Leaf.SouthWest);
					LodQuadTreeNode sw = new LodQuadTreeNode(swID, Leaf.SouthWest, level, m_DataProvider.GetBounds(swID), node, m_BlockSize);

					uint seID = PackID(node.ID, level, Leaf.SouthEast);
					LodQuadTreeNode se = new LodQuadTreeNode(seID, Leaf.SouthEast, level, m_DataProvider.GetBounds(seID), node, m_BlockSize);

					node.NorthWest = nw;
					node.NorthEast = ne;
					node.SouthWest = sw;
					node.SouthEast = se;
				
					remainingNodes.Add(nw);
					remainingNodes.Add(ne);
					remainingNodes.Add(sw);
					remainingNodes.Add(se);
				}

				remainingNodes.Remove(node);
			}
		}

		private void LinkQuadTree()
		{
			List<LodQuadTreeNode> remainingNodes = new List<LodQuadTreeNode>();

			remainingNodes.Add(m_Root);

			while (remainingNodes.Count > 0)
			{
				LodQuadTreeNode node = remainingNodes[0];

				// always link nodes to the counterpart on the same or lower (less res) level 
				if (node.Parent != null)
				{
					if (node.Leaf == Leaf.NorthWest)
					{
						node.North = GetParentCardinal(node.Parent, Cardinal.North, Leaf.SouthWest);
						node.South = node.Parent.SouthWest;
						node.East = node.Parent.NorthEast;
						node.West = GetParentCardinal(node.Parent, Cardinal.West, Leaf.NorthEast);
						
					}

					if (node.Leaf == Leaf.NorthEast)
					{
						node.North = GetParentCardinal(node.Parent, Cardinal.North, Leaf.SouthEast);
						node.South = node.Parent.SouthEast;
						node.East = GetParentCardinal(node.Parent, Cardinal.East, Leaf.NorthWest);
						node.West = node.Parent.NorthWest;											
					}

					if (node.Leaf == Leaf.SouthWest)
					{
						node.North = node.Parent.NorthWest;
						node.South = GetParentCardinal(node.Parent, Cardinal.South, Leaf.NorthWest);
						node.East = node.Parent.SouthEast;
						node.West = GetParentCardinal(node.Parent, Cardinal.West, Leaf.SouthEast);
					}

					if (node.Leaf == Leaf.SouthEast)
					{
						node.North = node.Parent.NorthEast;
						node.South = GetParentCardinal(node.Parent, Cardinal.South, Leaf.NorthEast);
						node.West = node.Parent.SouthWest;
						node.East = GetParentCardinal(node.Parent, Cardinal.East, Leaf.SouthWest);
					}
				}
				else
				{
					m_Root.North = null;
					m_Root.South = null;
					m_Root.East = null;
					m_Root.West = null; 
				}

				if (node.HasChildren == true)
				{
					remainingNodes.Add(node.NorthEast);
					remainingNodes.Add(node.NorthWest);
					remainingNodes.Add(node.SouthEast);
					remainingNodes.Add(node.SouthWest);
				}

				remainingNodes.Remove(node);
			}
		}

		private LodQuadTreeNode GetParentCardinal(LodQuadTreeNode node, Cardinal cardinal, Leaf leaf)
		{
			if (node == null)
			{
				return null;
			}

			//LodQuadTreeNode parent = node.Parent;

			//if (parent == null)
			//{
				//return null;
			//}

			LodQuadTreeNode neighbour = node[cardinal];

			if (neighbour == null)
			{
				return null;
			}

			return neighbour[leaf];
		}

		private void AllocatePatches(int patchCount)
		{
			for (int i = 0; i < patchCount; i++) 
			{
				m_InactivePatches.Add(new TerrainPatch(this)); 
			}
		}

		private void AllocateIndexBuffers()
		{
			/* 
			 *   N N N N
			 * W C C C C E
			 * W C C C C E
			 * W C C C C E
			 * W C C C C E
			 *   S S S S 
			 */

			for (int i = 0; i < m_PatchWindings.Length; i++)
			{
				m_PatchWindings[i] = new PatchWindingBuffer((UpscaleWinding)i, m_BlockSize); 
			}
		}

		List<LodQuadTreeNode> m_TreeNodesToProcess = new List<LodQuadTreeNode>();
		List<TerrainPatch> m_PatchesToProcess = new List<TerrainPatch>();
		private BitmapTexture2D m_Rocks1;
		private BitmapTexture2D m_Rocks1_N;
		private BitmapTexture2D m_Rocks1_S;

		public void Render(View3D view, Deferred_Terrain effect, Deferred_TerrainSimple simpleEffect)
		{
			if (m_ActivePatches.Count == 0) 
			{
				return; 				
			}

			bool anyPatchChange = false; 

			if (UpdatePatches == true) // && view.HasMoved == true)
			{
				Vector3 cameraCenter = view.Camera.Center * -1f;

				m_TreeNodesToProcess.Add(m_Root);

				//float distScale = (16f * m_DataProvider.MapScale);
				//float distScale = (4f * m_DataProvider.MapScale);
				//float distScale = (32f * m_DataProvider.MapScale);
				float distScale = (64f * m_DataProvider.MapScale); 

				while (m_TreeNodesToProcess.Count > 0)
				{
					LodQuadTreeNode node = m_TreeNodesToProcess[0];
					float nodeScale = node.Scale;

					//bool shouldChildrenBeVisible = (view.Camera.Center - node.Center).LengthFast / (float)(node.Scale) > 1f;
					//bool shouldChildrenBeVisible = ((cameraCenter - node.Center).LengthFast - (node.Radius * 1.5f)) < 512f / ((float)node.Level + 1);
					//bool shouldChildrenBeVisible = ((cameraCenter - node.Center).LengthFast - (node.Radius * 1.25f)) < 128f / ((float)node.Level + 1);

					BoundingBox bounds = node.Bounds;
					//Vector3 dist;
					//Maths.Collision.ClosestPointBoxPoint(ref bounds, ref cameraCenter, out dist);
					//bool shouldChildrenBeVisible = dist.LengthFast < 1024f / ((float)node.Level + 1);

					float dist =
					Collision.DistanceBoxPoint(ref bounds, ref cameraCenter);
					//bool shouldChildrenBeVisible = dist * 1.5f < 1024f / ((float)node.Level + 1);
					//bool shouldChildrenBeVisible_Dist = dist * 1.5f < 512f / ((float)node.Level + 1);
					//bool shouldChildrenBeVisible_Dist = dist * 1.5f < 1024f / ((float)node.Level + 1);
					//bool shouldChildrenBeVisible_Dist = dist * 1.5f < distScale / ((float)node.Level + 1) || 
					//									bounds.Contains(ref cameraCenter) == ContainmentType.Contains;
					bool shouldChildrenBeVisible_Dist = dist < distScale / (((float)node.Level + 1) * ((float)node.Level + 1)) ||
														bounds.Contains(ref cameraCenter) == ContainmentType.Contains;


					//bool shouldChildrenBeVisible = dist.LengthFast < 128f / ((float)node.Level + 1);

					bool shouldChildrenBeVisible_NeighboursWillAllow = node.NeighboursWillAllowChildrenVisible();
					bool shouldChildrenBeVisible_NeighboursRequireSplit = node.NeighboursRequireSplit(); 
					
					/* 
					if (node.AreChildrenVisible == false &&
						((shouldChildrenBeVisible_Dist == true &&
						node.NeighboursWillAllowChildrenVisible()) ||
						node.NeighboursRequireSplit() == true))
					{
						MakeChildrenVisible(node);
					}
					else if (node.AreChildrenVisible == true && shouldChildrenBeVisible_Dist == false)
					{
						CollapseChildren(node);
					}
					*/

					//if (view.Frustum.CheckBounds(ref bounds) == false)
					//{
					//	m_TreeNodesToProcess.Remove(node);
					//	continue;
					//}

					if (node.AreChildrenVisible == false &&
						((shouldChildrenBeVisible_Dist == true && shouldChildrenBeVisible_NeighboursWillAllow) ||
						shouldChildrenBeVisible_NeighboursRequireSplit == true))
					{
						MakeChildrenVisible(node);
						anyPatchChange = true;
					}
					else if (node.AreChildrenVisible == true && 
							shouldChildrenBeVisible_Dist == false &&
							shouldChildrenBeVisible_NeighboursRequireSplit == false)
					{
						CollapseChildren(node);
						anyPatchChange = true;
					}

					if (node.AreChildrenVisible == false)
					{
						node.Patch.Distance = dist; 

						if (ShowAllowSplit == false)
						{
							node.Patch.IsVisible = view.Frustum.CheckBounds(ref bounds);
						}
						else
						{
							node.Patch.IsVisible = true;
						}
					}
					else
					{						
						m_TreeNodesToProcess.Add(node.NorthWest);
						m_TreeNodesToProcess.Add(node.NorthEast);
						m_TreeNodesToProcess.Add(node.SouthWest);
						m_TreeNodesToProcess.Add(node.SouthEast);						
					}

					m_TreeNodesToProcess.Remove(node);
				}
			
				//m_PatchesToProcess.AddRange(m_ActivePatches);
			}

			if (anyPatchChange == true) // view.HasMoved == true)
			{
				//while (m_PatchesToProcess.Count > 0)
				foreach (TerrainPatch patch in m_ActivePatches)
				{
					if (patch.IsVisible == false)
					{
						continue;
					}

					// check neighbours for correct windings
					//TerrainPatch patch = m_PatchesToProcess[m_PatchesToProcess.Count - 1];

					patch.DeterminWinding();

					//m_PatchesToProcess.RemoveAt(m_PatchesToProcess.Count - 1); 
				}
			}

			bool simpleMode = true;
			
			//simpleEffect.Begin(ref view.World, ref view.Projection, ref view.NormalWorld, m_DataProvider.NormalMap); 

		
			//GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

			if (ShowShouldSplit == true) GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);			

			int flipCount = 0; 
			int renderedPatches = 0; 

			//foreach (TerrainPatch patch in m_ActivePatches)
			for (int i = m_ActivePatches.Count - 1; i >= 0; i--)
			{
				TerrainPatch patch = m_ActivePatches[i]; 

				if (patch.IsVisible == false)
				{
					continue;
				}

				bool shouldRenderSimple = patch.Distance > 256f; // 128f; //  64f; // 256f;

				if (simpleMode != shouldRenderSimple || renderedPatches == 0)
				{
					simpleMode = shouldRenderSimple;

					if (simpleMode == true)
					{
						if (renderedPatches > 0)
						{
							effect.End();
						}

						simpleEffect.Begin(ref view.World, ref view.Projection, ref view.NormalWorld, m_DataProvider.NormalMap); 
					}
					else
					{
						if (renderedPatches > 0)
						{
							simpleEffect.End();
						}
	
						effect.Begin(ref view.World, ref view.Projection, ref view.NormalWorld, m_DataProvider.NormalMap, m_DataProvider.TangentMap, m_Rocks1, m_Rocks1_N, m_Rocks1_S);
					}

					flipCount++; 
				}


				//bool renderWireFrame = (ShowAllowSplit && patch.CurrentNode.NeighboursWillAllowChildrenVisible() == false) ||
				//						(ShowShouldSplit && patch.CurrentNode.NeighboursRequireSplit() == true); 

				//if (renderWireFrame == true) GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

				renderedPatches++;

				patch.Bind();

				//PatchWindingBuffer winding = m_PatchWindings[(int)(UpscaleWinding.East | UpscaleWinding.North | UpscaleWinding.South | UpscaleWinding.West)];
				PatchWindingBuffer winding = m_PatchWindings[(int)patch.Winding];

				winding.Bind();

				if (simpleMode == true)
				{
					simpleEffect.Render(winding.IndexCount, winding.IndexType);
				}
				else
				{
					effect.Render(winding.IndexCount, winding.IndexType);
				}

				patch.Unbind();

				winding.Unbind();

				//if (renderWireFrame == true) GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			}

			if (ShowShouldSplit == true) GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
			//GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

			//ArtworkEnvironment.Form.Text = "Patches " + renderedPatches.ToString(); 

			if (renderedPatches > 0)
			{
				if (simpleMode == true)
				{
					simpleEffect.End();
				}
				else
				{
					effect.End();
				}
			}
		}

		public void LoadResources()
		{
			if (m_IsLoaded == true)
			{
				return; 
			}

			if (m_PatchWindings[0].IndexType == DrawElementsType.UnsignedInt)
			{
				uint[] indices = new uint[(m_BlockSize + 1) * (m_BlockSize + 1) * 6];				

				for (int i = 0; i < m_PatchWindings.Length; i++)
				{
					m_PatchWindings[i].LoadResources();
					m_PatchWindings[i].FillBuffer(indices);
				}
			}
			else 
			{
				ushort[] indices = new ushort[(m_BlockSize + 1) * (m_BlockSize + 1) * 6];

				for (int i = 0; i < m_PatchWindings.Length; i++)
				{
					m_PatchWindings[i].LoadResources();
					m_PatchWindings[i].FillBuffer(indices);
				}
			}

			for (int i = 0; i < m_InactivePatches.Count; i++)
			{
				m_InactivePatches[i].LoadResources();
			}

			for (int i = 0; i < m_ActivePatches.Count; i++)
			{
				m_ActivePatches[i].LoadResources();

				m_ActivePatches[i].LoadPatchData(); 
			}

			m_DataProvider.NormalMap.LoadResources();

			m_DataProvider.PopulateNormalMap();

			m_DataProvider.TangentMap.LoadResources();

			m_DataProvider.PopulateTangentMap();


			m_IsLoaded = true; 
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == false)
			{
				return; 
			}

			for (int i = 0; i < m_PatchWindings.Length; i++)
			{
				m_PatchWindings[i].UnloadResources();
			}

			for (int i = 0; i < m_InactivePatches.Count; i++)
			{
				m_InactivePatches[i].UnloadResources();
			}

			for (int i = 0; i < m_ActivePatches.Count; i++)
			{
				m_ActivePatches[i].UnloadResources();
			}

			m_DataProvider.NormalMap.UnloadResources();
			m_DataProvider.TangentMap.UnloadResources();

			m_IsLoaded = false; 
		}

		public static uint PackID(uint parent, uint level, Leaf leaf)
		{
			if (level > 9)
			{
				throw new Exception("Level out of range '" + level + "'"); 
			}

			int shift = (int)level * 3;
			uint leafID = unchecked((uint)((int)leaf << shift));

			return parent | leafID; 
		}

		public static Leaf UnpackLeafID(uint id, uint level)
		{
			if (level > 9)
			{
				throw new Exception("Level out of range '" + level + "'");
			}

			int shift = (int)level * 3;
			int mask = 7 << shift;

			return (Leaf)((id & mask) >> shift);
		}

		public static Leaf UnpackLastLeafID(uint id, out uint level)
		{
			level = 1;
			Leaf leaf = Leaf.Parent; 

			while (level < 10)
			{
				int shift = (int)level * 3;
				int mask = 7 << shift;

				Leaf next = (Leaf)((id & mask) >> shift);

				if (next == Leaf.Parent)
				{
					level--; 
					return leaf;
				}

				leaf = next; 

				level++; 
			}

			return leaf; 
		}

		public static void UnpackLeafPath(uint id, Leaf[] leafs, out uint level)
		{
			level = 1;
			int index = 0; 

			while (level < 10)
			{
				int shift = (int)level * 3;
				int mask = 7 << shift;

				Leaf next = (Leaf)((id & mask) >> shift);

				if (next == Leaf.Parent)
				{
					level--; 
					return;
				}

				leafs[index++] = next; 

				level++;
			}
		}

		public static uint UnpackLeafLevel(uint id)
		{
			uint level = 1;

			while (level < 10)
			{
				int shift = (int)level * 3;
				int mask = 7 << shift;

				Leaf next = (Leaf)((id & mask) >> shift);

				if (next == Leaf.Parent)
				{
					return level - 1;
				}

				level++;
			}

			return level - 1; 
		}
	}
}
