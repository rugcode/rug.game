﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using Rug.Game.Shaders.Terrain;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game.Deferred.Data.Terrain
{
	public class TerrainErrosion : IResourceManager
	{
		private bool m_Disposed = true; 
		List<IResource> m_Resources = new List<IResource>();
		List<IResourceManager> m_ResourcesManagers = new List<IResourceManager>();

		List<Texture2D> m_Texture2Ds = new List<Texture2D>(); 

		private Terrain_Copy m_Terrain_Copy;
		private Terrain_Copy m_Terrain_CopyX4;
		private Terrain_Diffuse_Soil m_Terrain_Diffuse_Soil;
		private Terrain_Errode m_Terrain_Errode;
		private Terrain_Normals m_Terrain_Normals;
		private Terrain_Water_Cycle m_Terrain_Water_Cycle;
		private Terrain_Water_Diffuse m_Terrain_Water_Diffuse;
		private Terrain_Water_Momentum m_Terrain_Water_Momentum;
		private Terrain_Water_Normals m_Terrain_Water_Normals;
		private Terrain_Water_Flow m_Terrain_Water_Flow;
		private Terrain_Render m_Terrain_Render;
		private Terrain_Water_Render m_Terrain_Water_Render;

		private Terrain_Process m_Heights;
		private Terrain_Process m_Normals;
		private Terrain_Process m_Temp;
		private Terrain_Process m_Water_Temp;
		private Terrain_Process m_Water_Last;
		private Terrain_Process m_Water_Current;
		private Terrain_Process m_Water_Normals;
		private Terrain_Process m_Water_Flows;
		
		private HexGrid m_HexGrid;
		private Grid m_Grid; 

		private TextureBox m_Debug;

		private int count = 0;
		private BitmapTexture2D m_Rocks;
		private BitmapTexture2D m_Rocks_N;
		private BitmapTexture2D m_Grass;
		private BitmapTexture2D m_Grass_N;
		private TextureBox m_Debug2;
		private TextureBox m_Debug3;
		private TextureBox m_Debug4;
		private BitmapTexture2D m_HeightsInit;
		private BitmapTexture2D m_TestImage;
		private BitmapTexture2D m_Blank;

		public Vector2 MousePos { get; set; }
		public float EditSize { get; set; }
		public float Create { get; set; }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

		public TerrainErrosion(int size)
		{
			m_Terrain_Copy = SharedEffects.Effects["Terrain_Copy"] as Terrain_Copy;
			m_Terrain_CopyX4 = SharedEffects.Effects["Terrain_CopyX4"] as Terrain_Copy;
			m_Terrain_Diffuse_Soil = SharedEffects.Effects["Terrain_Diffuse_Soil"] as Terrain_Diffuse_Soil;
			m_Terrain_Errode = SharedEffects.Effects["Terrain_Errode"] as Terrain_Errode;
			m_Terrain_Normals = SharedEffects.Effects["Terrain_Normals"] as Terrain_Normals;
			m_Terrain_Water_Cycle = SharedEffects.Effects["Terrain_Water_Cycle"] as Terrain_Water_Cycle;
			m_Terrain_Water_Diffuse = SharedEffects.Effects["Terrain_Water_Diffuse"] as Terrain_Water_Diffuse;
			m_Terrain_Water_Momentum = SharedEffects.Effects["Terrain_Water_Momentum"] as Terrain_Water_Momentum;
			m_Terrain_Water_Normals = SharedEffects.Effects["Terrain_Water_Normals"] as Terrain_Water_Normals;
			m_Terrain_Water_Flow = SharedEffects.Effects["Terrain_Water_Flow"] as Terrain_Water_Flow;
			m_Terrain_Render = SharedEffects.Effects["Terrain_Render"] as Terrain_Render;
			m_Terrain_Water_Render = SharedEffects.Effects["Terrain_Water_Render"] as Terrain_Water_Render;

			m_HeightsInit = LoadImage("Heights", "~/Data/Assets/Test.png");
			m_TestImage = LoadImage("Test Image", "~/Data/Assets/TestColours2.png");
			m_Blank = LoadImage("Blank", "~/Data/Assets/Test_blank.png"); 

			m_Heights = Processor("Heights", size);			
			m_Normals = Processor("Normals", size);		
			//this.normals = new Water(width, height, framework, programs, this.heights, this.waterHeights);
			//this.shadow = new Shadow(width, height, framework, programs, this, grid);
			m_Temp = Processor("Temp Other", size);

			m_Water_Temp = Processor("Water Other", size);
			m_Water_Last = Processor("Water Last", size);
			m_Water_Current = Processor("Water Current", size);
			m_Water_Normals = Processor("Water Normals", size);
			m_Water_Flows = Processor_Nearest("Water Flows", size / 4); // / 4);
			/* 
			detailNormals: 'rock_texture.png',
	        rock_normals: 'rock2_normals.png',
			grass: 'grass_material.png',
			grass_normals: 'grass_normals.png',
			*/
			
			m_Rocks = LoadImage("Rocks", "~/Data/Assets/erode/rock_texture.png");
			m_Rocks_N = LoadImage("Rocks_N", "~/Data/Assets/erode/rock2_normals.png");
			m_Grass = LoadImage("Grass", "~/Data/Assets/erode/grass_material.png");
			m_Grass_N = LoadImage("Grass_N", "~/Data/Assets/erode/grass_normals.png");

			m_Resources.Add(m_Rocks);
			m_Resources.Add(m_Rocks_N);
			m_Resources.Add(m_Grass);
			m_Resources.Add(m_Grass_N);

			m_HexGrid = new HexGrid(size);
			m_Grid = new Grid(size);
			m_Debug = new TextureBox();
			m_Debug2 = new TextureBox();
			m_Debug3 = new TextureBox();
			m_Debug4 = new TextureBox();

			m_Box = new TextureBox();
			m_Box.FlipVertical = true; 

			m_Resources.Add(m_HeightsInit);
			m_Resources.Add(m_TestImage);
			m_Resources.Add(m_Blank);

			m_ResourcesManagers.Add(m_Box);
			m_ResourcesManagers.Add(m_HexGrid);
			m_ResourcesManagers.Add(m_Grid);
			m_ResourcesManagers.Add(m_Debug);
			m_ResourcesManagers.Add(m_Debug2);
			m_ResourcesManagers.Add(m_Debug3);
			m_ResourcesManagers.Add(m_Debug4); 

			m_ResourcesManagers.Add(m_Heights);
			m_ResourcesManagers.Add(m_Water_Current);
			//m_Texture2Ds.Add(m_Water_Current);
			m_ResourcesManagers.Add(m_Normals);
			//m_Texture2Ds.Add(m_Normals);
			m_ResourcesManagers.Add(m_Temp);
			//m_Texture2Ds.Add(m_Temp);
			m_ResourcesManagers.Add(m_Water_Temp);
			//m_Texture2Ds.Add(m_Water_Temp);
			m_ResourcesManagers.Add(m_Water_Last);
			//m_Texture2Ds.Add(m_Water_Last);
			m_ResourcesManagers.Add(m_Water_Normals);
			//m_Texture2Ds.Add(m_Water_Normals);
			m_ResourcesManagers.Add(m_Water_Flows);
			//m_Texture2Ds.Add(m_Water_Flows);			
		}
		
		private BitmapTexture2D LoadImage(string name, string path)
		{
			return new BitmapTexture2D(name, path, ResourceMode.Static, new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.Rgba32f,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.Linear,
				PixelFormat = PixelFormat.Bgra,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.Repeat,
				WrapT = TextureWrapMode.Repeat,
				Size = new Size(512, 512),
				MipMap = true,
			});
		}

		private Terrain_Process Processor(string name, int size)
		{
			//return new BitmapTexture2D(name, "~/Data/Assets/Test_blank.png", ResourceMode.Static, new Texture2DInfo()
			//return new BitmapTexture2D(name, "~/Data/Assets/TestColours2.png", ResourceMode.Static, new Texture2DInfo()
			/* Texture2D texture = new Texture2D(name, ResourceMode.Static, new Texture2DInfo()
			{
				Border = 0,
				InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba32f,
				MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear,
				MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear,
				PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
				PixelType = OpenTK.Graphics.OpenGL.PixelType.Float,
				Size = new System.Drawing.Size(size, size),
				WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});	
			*/
			
			return new Terrain_Process(size, true);

			/*
			{
				Border = 0,
				InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba32f,
				MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear,
				MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear,
				PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba,
				PixelType = OpenTK.Graphics.OpenGL.PixelType.Float,
				Size = new System.Drawing.Size(size, size),
				WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});
			*/
		}

		private Terrain_Process Processor_Nearest(string name, int size)
		{
			/* 
			Texture2D texture = new Texture2D(name, ResourceMode.Static, new Texture2DInfo()
			{
				Border = 0,
				InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba32f,
				MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Nearest,
				MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Nearest,
				PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba,
				PixelType = OpenTK.Graphics.OpenGL.PixelType.Float,
				Size = new System.Drawing.Size(size, size),
				WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});
			*/

			return new Terrain_Process(size, false);; 

			/*
			{
				Border = 0,
				InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba32f,
				MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Nearest,
				MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Nearest,
				PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba,
				PixelType = OpenTK.Graphics.OpenGL.PixelType.Float,
				Size = new System.Drawing.Size(size, size),
				WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});
			*/
		}


		/* 
		water_diffuse 
		 - axis, 0, 1
		 - source, current
		 - heights, heights
		> temp 


		water_diffuse 
		 - axis, 1, 0
		 - source, temp
		 - heights, heights
		> current


		water_momentum 
		 - last, last
		 - current, current
		 - ground', heights
		> temp 

		copy 
		 - source, current
		> last


		water_cycle
		 - rain, rain
		 - evaporate, evaporate
		 - normals, tmp
		> current

		water_normals
		 - ground, heights
		 - normals, current
		> waterHeights

		flows
		 - flows, flows
		 - normals, current
		> temp 

		copy 
		 - source, temp
		> flows

		errode
		 - ground, heights
		 - normals, current
		 - factor, erode
		> other_temp 


		diffuse_soil
		 - ground, other_temp
		 - normals, current
		> heights
		 */

		int step = 0;
		private TextureBox m_Box; 

		public void Update(View3D view, float rain, float evaporate, float erodeFactor)
		{
			Viewport viewport = GLState.Viewport;

			GLState.EnableCullFace = false;
			GLState.EnableDepthMask = false;
			GLState.EnableDepthTest = false;
			GLState.EnableAlphaTest = false;
			GLState.EnableBlend = false;

			GLState.Apply(view); 

			if (step == 0)
			{
				m_Terrain_Copy.Render(view, m_Box.Vertices, m_HeightsInit, m_Heights);
			}

			//if (count > 10)
			//{
			//    return; 
			//}

			//count++; 

			//if (step % 4 == 0)
			{

				#region Process Water

				m_Terrain_Water_Diffuse.Render(view, m_Box.Vertices, new OpenTK.Vector2(0, 1), m_Heights.ColorTexture, m_Water_Current.ColorTexture, m_Water_Temp);
				m_Terrain_Water_Diffuse.Render(view, m_Box.Vertices, new OpenTK.Vector2(1, 0), m_Heights.ColorTexture, m_Water_Temp.ColorTexture, m_Water_Current);

				//m_Terrain_Copy.Render(view, m_Box.Vertices, m_Water_Temp.ColorTexture, m_Water_Current);

				m_Terrain_Water_Momentum.Render(view, m_Box.Vertices, m_Water_Current.ColorTexture, m_Water_Last.ColorTexture, m_Heights.ColorTexture, m_Water_Temp);

				//m_Terrain_Copy.Render(view, m_Water_Temp, m_Water_Last);

				Terrain_Process temp = m_Water_Last;
				m_Water_Last = m_Water_Current;
				m_Water_Current = m_Water_Last; 

				//m_Terrain_Copy.Render(view, m_Water_Temp, m_Water_Current);

				m_Terrain_Water_Cycle.Render(view, m_Box.Vertices, rain, evaporate, Create, EditSize, MousePos, m_Water_Temp.ColorTexture, m_Water_Current);

				//m_Terrain_Copy.Render(view, m_Water_Temp, m_Water_Current);

				//Texture2D temp = m_Water_Temp;
				//m_Water_Temp = m_Water_Current;
				//m_Water_Current = m_Water_Temp; 

				m_Terrain_Water_Normals.Render(view, m_Box.Vertices, m_Heights.ColorTexture, m_Water_Current.ColorTexture, m_Water_Normals);

				m_Terrain_Copy.Render(view, m_Box.Vertices, m_Water_Flows.ColorTexture, m_Water_Temp);

				m_Terrain_Water_Flow.Render(view, m_Box.Vertices, m_Water_Current.ColorTexture, m_Water_Flows.ColorTexture, m_Water_Flows);

				//m_Terrain_Copy.Render(view, m_Box.Vertices, m_Water_Temp.ColorTexture, m_Water_Flows);

				// m_Terrain_CopyX4.Render(view, m_Water_Temp, m_Water_Flows);

				#endregion

				m_Terrain_Errode.Render(view, m_Box.Vertices, erodeFactor, m_Heights.ColorTexture, m_Water_Current.ColorTexture, m_Temp);

				//m_Terrain_Copy.Render(view, m_Temp, m_Heights);

				m_Terrain_Diffuse_Soil.Render(view, m_Box.Vertices, m_Temp.ColorTexture, m_Water_Current.ColorTexture, m_Heights);

				m_Terrain_Normals.Render(view, m_Box.Vertices, m_Heights.ColorTexture, m_Normals);

				//m_Terrain_Diffuse_Soil.Render(view, m_Heights, m_Water_Current, m_Temp);
				//m_Terrain_Copy.Render(view, m_Temp, m_Heights);

			}

			step++; 

			//GL.Finish();


			GLState.EnableCullFace = true;
			GLState.EnableDepthMask = true;
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = true;
			GLState.ClearColor(Color.Black);
			GLState.Viewport = viewport;
			GLState.Apply(view); 
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);		
						
			m_Terrain_Render.Render(view, 
				ref view.Projection, ref view.View,
				m_HexGrid.Buffer, m_HexGrid.Count,
				EditSize, MousePos, 
				m_Heights.ColorTexture, m_Normals.ColorTexture, m_Water_Current.ColorTexture,
				m_Rocks, m_Rocks_N,
				m_Grass, m_Grass_N); 				

			m_Terrain_Water_Render.Render(view,
				ref view.Projection, ref view.View,
				m_Grid.Buffer, m_Grid.Count,
				m_Heights.ColorTexture, m_Water_Current.ColorTexture, m_Water_Normals.ColorTexture, m_Grass_N, m_Water_Flows.ColorTexture);

			GLState.EnableCullFace = false;
			GLState.EnableDepthMask = false;
			GLState.EnableDepthTest = false; 
			GLState.Apply(view);
			GL.Clear(ClearBufferMask.DepthBufferBit);		

			m_Debug.Rectangle = new RectangleF(-1, -1, 0.25f, 0.25f);
			m_Debug.Texture = m_Water_Current.ColorTexture; // m_Heights;
			m_Debug.Render(view);

			m_Debug2.Rectangle = new RectangleF(-0.75f, -1, 0.25f, 0.25f);
			m_Debug2.Texture = m_Water_Flows.ColorTexture; // m_Heights;
			m_Debug2.Render(view);

			m_Debug3.Rectangle = new RectangleF(-1, -0.75f, 0.25f, 0.25f);
			m_Debug3.Texture = m_Heights.ColorTexture; // m_Heights;
			m_Debug3.Render(view);

			m_Debug4.Rectangle = new RectangleF(-0.75f, -0.75f, 0.25f, 0.25f);
			m_Debug4.Texture = m_Normals.ColorTexture; // m_Heights;
			m_Debug4.Render(view); 

			/* 
			lightview: inv_lightview,
                normals: terrain.water.normals.result,
                heights: terrain.heights.result,
                water_heights: terrain.water.current.result,
                detail_normals: programs.grass_normals,
                shadowmap: terrain.shadow.water_map.result,
                flows: terrain.water.flows.result,
			*/ 
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				step = 0; 
				/* 
				m_HexGrid.LoadResources();
				m_Grid.LoadResources(); 
				m_Debug.LoadResources();
				m_Debug2.LoadResources();
				m_Debug3.LoadResources();
				m_Debug4.LoadResources(); 
				*/ 

				foreach (IResource res in m_Resources)
				{
					res.LoadResources();

					if (!(res is BitmapTexture2D) && (res is Texture2D))
					{
						(res as Texture2D).CreateBlank(); 
					}
				}

				foreach (IResourceManager res in m_ResourcesManagers)
				{
					res.LoadResources();
				}
				
				/* 
				foreach (Texture2D texture in m_Texture2Ds)
				{
					m_Terrain_Copy.Render(null, m_Blank, texture);
				}
				*/ 
				//m_Terrain_Copy.Render(null, m_HeightsInit, m_Heights);
				//m_Terrain_Copy.Render(null, m_TestImage, m_Water_Current);

				//m_Water_Temp.LoadResources();
				//m_Water_Temp.CreateBlank(); 
				//m_Heights.LoadResources();
				//m_Heights.CreateBlank(); 
				//m_Rocks.LoadResources();

				m_Disposed = false;
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				/* 
				m_HexGrid.UnloadResources();
				m_Grid.UnloadResources(); 
				m_Debug.UnloadResources();
				m_Debug2.UnloadResources();
				m_Debug3.UnloadResources();
				m_Debug4.UnloadResources(); 
				*/ 

				foreach (IResource res in m_Resources)
				{
					res.UnloadResources();
				}

				foreach (IResourceManager res in m_ResourcesManagers)
				{
					res.UnloadResources();
				}

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			foreach (IResource manager in m_Resources)
			{
				manager.UnloadResources();
			}
		}

		#endregion
	}
}
