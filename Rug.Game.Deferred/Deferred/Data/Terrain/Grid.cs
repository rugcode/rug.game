﻿using OpenTK;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;

namespace Rug.Game.Deferred.Data.Terrain
{
	public class Grid : IResourceManager
	{
		private bool m_Disposed = true; 

		private VertexBuffer m_Buffer;
		private GridVert[] m_Verts;

		public VertexBuffer Buffer { get { return m_Buffer; } }

		public int Count { get { return m_Verts.Length; } }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

		public Grid(int size)
		{
			/* 
			var xsize = (float)size;
			var ysize = (float)size;
			var width = 10f;
			var height = 10f;
			var hw = width / 2f;
			var hh = height / 2f;

			List<Vector2> position_2f = new List<Vector2>();
			List<Vector2> texcoord_2f = new List<Vector2>();
			List<Vector3> barycentric_3f = new List<Vector3>();
			List<Vector4> cell_uv_4f = new List<Vector4>();

			for (var r = 0; r <= xsize; r++)
			{
				var x1 = Clamp((r - 0.5f) / xsize, 0, 1);
				var x2 = Clamp((r + 0.0f) / xsize, 0, 1);
				var x3 = Clamp((r + 0.5f) / xsize, 0, 1);
				var x4 = Clamp((r + 1.0f) / xsize, 0, 1);

				for (var g = 0; g < ysize; g += 2)
				{
					var t = (g + 0) / ysize;
					var m = (g + 1) / ysize;
					var b = (g + 2) / ysize;
					
					position_2f.Add(new Vector2(x1 * width - hw, t * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, t * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, t * height - hh));
					position_2f.Add(new Vector2(x4 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x1 * width - hw, b * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, b * height - hh));
					position_2f.Add(new Vector2(x2 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x4 * width - hw, m * height - hh));
					position_2f.Add(new Vector2(x3 * width - hw, b * height - hh));					

					
					texcoord_2f.Add(new Vector2(x1, t)); 
					texcoord_2f.Add(new Vector2(x3, t)); 
					texcoord_2f.Add(new Vector2(x2, m));
					texcoord_2f.Add(new Vector2(x2, m)); 
					texcoord_2f.Add(new Vector2(x3, t)); 
					texcoord_2f.Add(new Vector2(x4, m));
					texcoord_2f.Add(new Vector2(x1, b)); 
					texcoord_2f.Add(new Vector2(x2, m)); 
					texcoord_2f.Add(new Vector2(x3, b));
					texcoord_2f.Add(new Vector2(x2, m)); 
					texcoord_2f.Add(new Vector2(x4, m));
					texcoord_2f.Add(new Vector2(x3, b));
										
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0)); 
					barycentric_3f.Add(new Vector3(0, 0, 1));
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0)); 
					barycentric_3f.Add(new Vector3(0, 0, 1));
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0)); 
					barycentric_3f.Add(new Vector3(0, 0, 1));
					barycentric_3f.Add(new Vector3(1, 0, 0)); 
					barycentric_3f.Add(new Vector3(0, 1, 0));
					barycentric_3f.Add(new Vector3(0, 0, 1));
					
				}
			}
			*/

			float xsize = size;
			float ysize = size;
			float width = 10f;
			float height = 10f;
			float cell_width = 4f;
			float cell_height = 4f;            

			/* 
            var position_2f = [];
            var texcoord_2f = [];
            var barycentric_3f = [];
            var cell_uv_4f = [];
			*/

			List<Vector2> position_2f = new List<Vector2>();
			List<Vector2> texcoord_2f = new List<Vector2>();
			List<Vector3> barycentric_3f = new List<Vector3>();
			List<Vector4> cell_uv_4f = new List<Vector4>();

            //var width = args.width;
            //var height = args.height;
            //var xsize = args.xsize;
            //var ysize = args.ysize;

			float cell_count_x = (float)xsize / cell_width;
			float cell_count_y = (float)ysize / cell_height;

			for (float x = 0; x < xsize; x++)
			{
				float left = x / xsize;
				float right = (x + 1) / xsize;
				float pleft = (left * width) - width / 2;
				float pright = (right * width) - width / 2;
                float cell_x = (float)Math.Floor(x / cell_width) / cell_count_x;
				float cell_l = (x % cell_width) / cell_width;
				float cell_r = ((x % cell_width) + 1) / cell_width;

				for (float y = 0; y < ysize; y++)
				{
					float bottom = y / ysize;
					float top = (y + 1) / ysize;
					float pbottom = (bottom * height) - height / 2;
					float ptop = (top * height) - height / 2;
					float cell_y = (float)Math.Floor(y / cell_height) / cell_count_y;
					float cell_b = (y % cell_height) / cell_height;
					float cell_t = ((y % cell_height) + 1) / cell_height;

					position_2f.Add(new Vector2(pleft, ptop));
					position_2f.Add(new Vector2(pleft, pbottom));
					position_2f.Add(new Vector2(pright, ptop));

					position_2f.Add(new Vector2(pleft, pbottom));
					position_2f.Add(new Vector2(pright, pbottom));
					position_2f.Add(new Vector2(pright, ptop));


					texcoord_2f.Add(new Vector2(left, top));
					texcoord_2f.Add(new Vector2(left, bottom));
					texcoord_2f.Add(new Vector2(right, top));

					texcoord_2f.Add(new Vector2(left, bottom));
					texcoord_2f.Add(new Vector2(right, bottom));
					texcoord_2f.Add(new Vector2(right, top));


					cell_uv_4f.Add(new Vector4(cell_x, cell_y, cell_l, cell_t));
					cell_uv_4f.Add(new Vector4(cell_x, cell_y, cell_l, cell_b));
					cell_uv_4f.Add(new Vector4(cell_x, cell_y, cell_r, cell_t));

					cell_uv_4f.Add(new Vector4(cell_x, cell_y, cell_l, cell_b));
					cell_uv_4f.Add(new Vector4(cell_x, cell_y, cell_r, cell_b));
					cell_uv_4f.Add(new Vector4(cell_x, cell_y, cell_r, cell_t));

					barycentric_3f.Add(new Vector3(1, 0, 0));
					barycentric_3f.Add(new Vector3(0, 1, 0));
					barycentric_3f.Add(new Vector3(0, 0, 1));

					barycentric_3f.Add(new Vector3(1, 0, 0));
					barycentric_3f.Add(new Vector3(0, 1, 0));
					barycentric_3f.Add(new Vector3(0, 0, 1));
				}
            }

			m_Verts = new GridVert[position_2f.Count];

			for (int i = 0; i < position_2f.Count; i++)
			{
				m_Verts[i] = new GridVert()
				{
					Position = position_2f[i],
					TexCoord = texcoord_2f[i],
					Barycentric = barycentric_3f[i],
					CellUV = cell_uv_4f[i],
				};
			}

			m_Buffer = new VertexBuffer("Grid", ResourceMode.Static,
				new VertexBufferInfo(GridVert.Format, m_Verts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw)); 
		}

		private float Clamp(float value, int min, int max)
		{
			return Math.Min(Math.Max(value, min), max); 
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				m_Buffer.LoadResources();

				DataStream stream; 

				m_Buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

				stream.WriteRange(m_Verts); 

				m_Buffer.UnmapBuffer(); 

				m_Disposed = false;
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop(); 
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Buffer.UnloadResources(); 

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources();
		}

		#endregion
	}
}
/* 
 framework.HexGrid = Class({
        __extends__: framework.Drawable,
        __init__: function(params){
            var args = extend({
                xsize: 10,
                ysize: 10,
                width: 1,
                height: 1,
            }, params);

            var position_2f = [];
            var texcoord_2f = [];
            var barycentric_3f = [];

            var width = args.width;
            var height = args.height;
            var xsize = args.xsize;
            var ysize = args.ysize;
            var hw = width/2;
            var hh = height/2;

            for(var r=0; r<=xsize; r++){
                var x1 = clamp((r-0.5)/xsize, 0, 1);
                var x2 = clamp((r+0.0)/xsize, 0, 1);
                var x3 = clamp((r+0.5)/xsize, 0, 1);
                var x4 = clamp((r+1.0)/xsize, 0, 1);
                for(var g=0; g<ysize; g+=2){
                    var t = (g+0)/ysize;
                    var m = (g+1)/ysize;
                    var b = (g+2)/ysize;

                    position_2f.push(
                        x1*width-hw, t*height-hh,
                        x3*width-hw, t*height-hh,
                        x2*width-hw, m*height-hh,

                        x2*width-hw, m*height-hh,
                        x3*width-hw, t*height-hh,
                        x4*width-hw, m*height-hh,    
                        
                        x1*width-hw, b*height-hh,
                        x2*width-hw, m*height-hh,
                        x3*width-hw, b*height-hh,    
                        
                        x2*width-hw, m*height-hh,
                        x4*width-hw, m*height-hh,
                        x3*width-hw, b*height-hh
                    );
                    
                    texcoord_2f.push(
                        x1, t,    x3, t,   x2, m,
                        x2, m,    x3, t,   x4, m,    
                        
                        x1, b,    x2, m,   x3, b,
                        x2, m,    x4, m,   x3, b
                    );

                    barycentric_3f.push(
                        1, 0, 0,    0, 1, 0,    0, 0, 1,
                        1, 0, 0,    0, 1, 0,    0, 0, 1,
                        
                        1, 0, 0,    0, 1, 0,    0, 0, 1,
                        1, 0, 0,    0, 1, 0,    0, 0, 1
                    );
                }
            }

            this.vbo = new framework.VBO({
                position_2f: position_2f,
                texcoord_2f: texcoord_2f,
                barycentric_3f: barycentric_3f,
            });
        },
    });
*/ 