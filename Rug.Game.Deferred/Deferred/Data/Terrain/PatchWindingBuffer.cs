﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;

namespace Rug.Game.Deferred.Data.Terrain
{
	public class PatchWindingBuffer : IResource
	{
		private UpscaleWinding m_WindingPattern;
		private int m_BlockSize;
		private IndexBuffer m_Buffer;
		private int m_IndexCount; 

		public string Name
		{
			get { return "Upscale Winding: " + m_WindingPattern; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.IndexBuffer; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return m_Buffer.ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_Buffer.ResourceHandle; }
		}

		public bool IsLoaded
		{
			get { return m_Buffer.IsLoaded; }
		}

		public int IndexCount { get { return m_IndexCount; } }
		public DrawElementsType IndexType { get { return m_Buffer.ResourceInfo.IndexType; } } 

		

		public PatchWindingBuffer(UpscaleWinding windingPattern, int blockSize)
		{			
			m_WindingPattern = windingPattern;
			m_BlockSize = blockSize;

			int count = (m_BlockSize + 1) * (m_BlockSize + 1) * 6;

			if (count < ushort.MaxValue)
			{
				//m_Buffer = new IndexBuffer("Upscale Winding: " + m_WindingPattern, Resources.ResourceMode.Static, new IndexBufferInfo(OpenTK.Graphics.OpenGL.DrawElementsType.UnsignedInt, sizeof(uint), count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
				m_Buffer = new IndexBuffer("Upscale Winding: " + m_WindingPattern, ResourceMode.Static, new IndexBufferInfo(OpenTK.Graphics.OpenGL.DrawElementsType.UnsignedShort, sizeof(ushort), count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
			}
			else
			{
				m_Buffer = new IndexBuffer("Upscale Winding: " + m_WindingPattern, ResourceMode.Static, new IndexBufferInfo(OpenTK.Graphics.OpenGL.DrawElementsType.UnsignedInt, sizeof(uint), count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
			}
		}
 
		internal void FillBuffer(uint[] indices)
		{				
			// uint[] indices = new uint[(m_BlockSize + 1) * (m_BlockSize + 1) * 6]; 

			int index = 0; 

			// fill the center
			for (int row = 1; row < m_BlockSize - 1; row++)
			{
				for (int col = 1; col < m_BlockSize - 1; col++)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}
			}

			// fill north side
			if ((m_WindingPattern & UpscaleWinding.North) == UpscaleWinding.North)
			{
				#region Upwind North

				int row = 0;
				int col = 0;

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));

				for (col = 2; col < m_BlockSize - 2; col += 2)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				#endregion 
			}
			else
			{
				#region No Upwind

				int row = 0;
				int col = 0;

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				for (col = 1; col < m_BlockSize - 1; col++)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}

			// fill south side
			if ((m_WindingPattern & UpscaleWinding.South) == UpscaleWinding.South)
			{
				#region Upwind South

				int row = m_BlockSize - 1;
				int col = 0;

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));

				for (col = 2; col < m_BlockSize - 2; col += 2)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 2));

				#endregion
			}
			else
			{
				#region No Upwind

				int row = m_BlockSize - 1;
				int col = 0;

				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				for (col = 1; col < m_BlockSize - 1; col++)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}

			// fill west side 
			if ((m_WindingPattern & UpscaleWinding.West) == UpscaleWinding.West)
			{
				#region Upwind West

				int col = 0;
				int row = 0;

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));

				for (row = 2; row < m_BlockSize - 2; row += 2)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));

				#endregion
			}
			else
			{
				#region No Upwind

				int col = 0;
				int row = 0;

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				for (row = 1; row < m_BlockSize - 1; row++)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}

			// fill east side 
			if ((m_WindingPattern & UpscaleWinding.East) == UpscaleWinding.East)
			{
				#region Upwind East

				int col = m_BlockSize - 1;
				int row = 0;

				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));

				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

				for (row = 2; row < m_BlockSize - 2; row += 2)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}
			else
			{
				#region No Upwind

				int col = m_BlockSize - 1;
				int row = 0;

				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				for (row = 1; row < m_BlockSize - 1; row++)
				{
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (uint)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (uint)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				#endregion
			}

			m_IndexCount = index; 

			DataStream stream;
			m_Buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(indices, 0, index);

			m_Buffer.UnmapBuffer(); 
		}

		internal void FillBuffer(ushort[] indices)
		{
			// ushort[] indices = new ushort[(m_BlockSize + 1) * (m_BlockSize + 1) * 6]; 

			int index = 0;

			// fill the center
			for (int row = 1; row < m_BlockSize - 1; row++)
			{
				for (int col = 1; col < m_BlockSize - 1; col++)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}
			}

			// fill north side
			if ((m_WindingPattern & UpscaleWinding.North) == UpscaleWinding.North)
			{
				#region Upwind North

				int row = 0;
				int col = 0;

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));

				for (col = 2; col < m_BlockSize - 2; col += 2)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				#endregion
			}
			else
			{
				#region No Upwind

				int row = 0;
				int col = 0;

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				for (col = 1; col < m_BlockSize - 1; col++)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}

			// fill south side
			if ((m_WindingPattern & UpscaleWinding.South) == UpscaleWinding.South)
			{
				#region Upwind South

				int row = m_BlockSize - 1;
				int col = 0;

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));

				for (col = 2; col < m_BlockSize - 2; col += 2)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 2));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 2));

				#endregion
			}
			else
			{
				#region No Upwind

				int row = m_BlockSize - 1;
				int col = 0;

				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				for (col = 1; col < m_BlockSize - 1; col++)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}

			// fill west side 
			if ((m_WindingPattern & UpscaleWinding.West) == UpscaleWinding.West)
			{
				#region Upwind West

				int col = 0;
				int row = 0;

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));

				for (row = 2; row < m_BlockSize - 2; row += 2)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));

				#endregion
			}
			else
			{
				#region No Upwind

				int col = 0;
				int row = 0;

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				for (row = 1; row < m_BlockSize - 1; row++)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}

			// fill east side 
			if ((m_WindingPattern & UpscaleWinding.East) == UpscaleWinding.East)
			{
				#region Upwind East

				int col = m_BlockSize - 1;
				int row = 0;

				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));

				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

				for (row = 2; row < m_BlockSize - 2; row += 2)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row + 2) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

				#endregion
			}
			else
			{
				#region No Upwind

				int col = m_BlockSize - 1;
				int row = 0;

				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				for (row = 1; row < m_BlockSize - 1; row++)
				{
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));

					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col));
					indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
					indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));
				}

				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col));
				indices[index++] = (ushort)(((row) * (m_BlockSize + 1)) + (col + 1));
				indices[index++] = (ushort)(((row + 1) * (m_BlockSize + 1)) + (col + 1));

				#endregion
			}

			m_IndexCount = index;

			DataStream stream;
			m_Buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(indices, 0, index);

			m_Buffer.UnmapBuffer();
		}

		public void LoadResources()
		{
			m_Buffer.LoadResources(); 
		}

		public void UnloadResources()
		{
			m_Buffer.UnloadResources(); 
		}

		public void Bind()
		{
			m_Buffer.Bind(); 
		}

		public void Unbind()
		{
			m_Buffer.Unbind(); 
		}
	}
}
