﻿using Rug.Game.Core.Maths;
using System;


namespace Rug.Game.Deferred.Data.Terrain
{
	public class LodQuadTreeNode 
	{
		private uint m_ID;
		private uint m_Level;
		private BoundingBox m_Bounds;
		private LodQuadTreeNode m_Parent;
		private OpenTK.Vector3 m_Center;
		private float m_Scale;
		private float m_Radius;
		private Terrain.Leaf m_Leaf;

		public uint ID { get { return m_ID; } }
		public uint Level { get { return m_Level; } }
		public Leaf Leaf { get { return m_Leaf; } }
		public BoundingBox Bounds { get { return m_Bounds; } }
		public LodQuadTreeNode Parent { get { return m_Parent; } }

		public bool HasChildren { get; set; }
		public OpenTK.Vector3 Center { get { return m_Center; } }

		public bool AreChildrenVisible { get; set; }

		public float Scale { get { return m_Scale; } }
		public float Radius { get { return m_Radius; } }		

		public LodQuadTreeNode NorthWest { get; set; }
		public LodQuadTreeNode NorthEast { get; set; }
		public LodQuadTreeNode SouthWest { get; set; }
		public LodQuadTreeNode SouthEast { get; set; }

		public LodQuadTreeNode North { get; set; }
		public LodQuadTreeNode South { get; set; }
		public LodQuadTreeNode East { get; set; }
		public LodQuadTreeNode West { get; set; }

		public TerrainPatch Patch { get; set; }

		public LodQuadTreeNode this[Leaf leaf]
		{
			get 
			{
				switch (leaf)
				{
					case Leaf.Parent:
						return m_Parent; 
					case Leaf.NorthWest:
						return NorthWest;
					case Leaf.NorthEast:
						return NorthEast;
					case Leaf.SouthWest:
						return SouthWest;
					case Leaf.SouthEast:
						return SouthEast;
					default:
						throw new Exception("Invlaid Leaf '" + leaf + "'"); 
				}	
			}
		}

		public LodQuadTreeNode this[Cardinal cardinal]
		{
			get 
			{
				switch (cardinal)
				{
					case Cardinal.North:
						return North; 
					case Cardinal.South:
						return South; 
					case Cardinal.West:
						return West; 
					case Cardinal.East:
						return East; 
					default:
						throw new Exception("Invlaid Cardinal '" + cardinal + "'"); 
				}
			}			
		}

		public LodQuadTreeNode(uint id, Leaf leaf, uint level, BoundingBox bounds, LodQuadTreeNode parent, int blockSize)
		{
			m_ID = id;
			m_Leaf = leaf; 
			m_Level = level;  
			m_Bounds = bounds;
			m_Parent = parent;

			m_Center = m_Bounds.Minimum + ((m_Bounds.Maximum - m_Bounds.Minimum) * 0.5f);
			m_Radius = (m_Bounds.Maximum - m_Bounds.Minimum).Length * 0.5f; 

			m_Scale = (float)Math.Pow(2, (int)level + 1) * (float)blockSize;
		}

		public bool NeighboursWillAllowChildrenVisible()
		{
			bool canShow = true;

			// are all the Neighbours of this node either split or visible
			if (North != null)
			{
				if (North.AreChildrenVisible == false &&
					North.Patch == null)
				{
					canShow &= false;
				}
			}

			if (South != null)
			{
				if (South.AreChildrenVisible == false &&
					South.Patch == null)
				{
					canShow &= false;
				}
			}

			if (West != null)
			{
				if (West.AreChildrenVisible == false &&
					West.Patch == null)
				{
					canShow &= false;
				}
			}

			if (East != null)
			{
				if (East.AreChildrenVisible == false &&
					East.Patch == null)
				{
					canShow &= false;
				}
			}

			return canShow; 
		}

		public bool NeighboursRequireSplit()
		{
			bool canShow = false;

			// are any the Neighbours of this node split twice

			if (North != null)
			{
				if (North.AreChildrenVisible == true &&
					North.AreSubChildrenVisible(Cardinal.North) == true)
				{					
					canShow |= true;
				}
			}

			if (South != null)
			{
				if (South.AreChildrenVisible == true &&
					South.AreSubChildrenVisible(Cardinal.South) == true)
				{
					canShow |= true;
				}
			}

			if (West != null)
			{
				if (West.AreChildrenVisible == true &&
					West.AreSubChildrenVisible(Cardinal.West) == true)
				{
					canShow |= true;
				}
			}

			if (East != null)
			{
				if (East.AreChildrenVisible == true &&
					East.AreSubChildrenVisible(Cardinal.East) == true)
				{
					canShow |= true;
				}
			}

			return canShow;
		}

		public bool AreSubChildrenVisible(Cardinal from)
		{
			Leaf leafA;
			Leaf leafB;

			ReflectLeafChildren(from, out leafA, out leafB);

			LodQuadTreeNode nodeA = this[leafA];
			LodQuadTreeNode nodeB = this[leafB];

			return (nodeA != null && nodeA.AreChildrenVisible) || 
					(nodeB != null && nodeB.AreChildrenVisible);
		}

		public static Leaf ReflectLeaf(Leaf leaf, Cardinal cardinal)
		{
			switch (leaf)
			{
				case Leaf.NorthWest:
					switch (cardinal)
					{
						case Cardinal.North:
							return Leaf.SouthWest;
						case Cardinal.South:
							return Leaf.SouthWest;
						case Cardinal.West:
							return Leaf.NorthEast;
						case Cardinal.East:
							return Leaf.NorthEast;
						default:
							break;
					}
					break;
				case Leaf.NorthEast:
					switch (cardinal)
					{
						case Cardinal.North:
							return Leaf.SouthEast;
						case Cardinal.South:
							return Leaf.SouthEast;
						case Cardinal.West:
							return Leaf.NorthWest;
						case Cardinal.East:
							return Leaf.NorthWest;
						default:
							break;
					}
					break;
				case Leaf.SouthWest:
					switch (cardinal)
					{
						case Cardinal.North:
							return Leaf.NorthWest;
						case Cardinal.South:
							return Leaf.NorthWest;
						case Cardinal.West:
							return Leaf.SouthEast;
						case Cardinal.East:
							return Leaf.SouthEast;
						default:
							break;
					}
					break;
				case Leaf.SouthEast:
					switch (cardinal)
					{
						case Cardinal.North:
							return Leaf.NorthEast;
						case Cardinal.South:
							return Leaf.NorthEast;
						case Cardinal.West:
							return Leaf.SouthWest;
						case Cardinal.East:
							return Leaf.SouthWest;
						default:
							break;
					}
					break;
				default:
					break;
			}

			throw new Exception("Unknow Reflection"); 
		}


		public static void ReflectLeafChildren(Cardinal cardinal, out Leaf leafA, out Leaf leafB)
		{
			leafA = Leaf.Parent;
			leafB = Leaf.Parent; 

			switch (cardinal)
			{
				case Cardinal.North:
					leafA = Leaf.SouthWest;
					leafB = Leaf.SouthEast;
					return; 
				case Cardinal.South:
					leafA = Leaf.NorthWest;
					leafB = Leaf.NorthEast;
					return;
				case Cardinal.West:
					leafA = Leaf.NorthEast;
					leafB = Leaf.SouthEast;
					return;
				case Cardinal.East:
					leafA = Leaf.NorthWest;
					leafB = Leaf.SouthWest;
					return;
				default:
					break;
			}
				
			throw new Exception("Unknow Reflection");
		}
	}
}
