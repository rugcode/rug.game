﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Deferred.Data.Terrain
{
	/* 
	position_2f: position_2f,
	texcoord_2f: texcoord_2f,
	barycentric_3f: barycentric_3f,
	 */

	[StructLayout(LayoutKind.Sequential)]
	public struct GridVert
	{		
		public Vector2 Position;
		public Vector2 TexCoord;
		public Vector3 Barycentric;
		public Vector4 CellUV;

		public enum Elements : int { Position = 0, TexCoord = 1, Barycentric = 2, CellUV  = 3 };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int TexCoordOffset;
		public static readonly int BarycentricOffset;
		public static readonly int CellUVOffset;

		static GridVert()
		{
			Stride = BlittableValueType<GridVert>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(GridVert), "Position");
			TexCoordOffset = (int)Marshal.OffsetOf(typeof(GridVert), "TexCoord");
			BarycentricOffset = (int)Marshal.OffsetOf(typeof(GridVert), "Barycentric");
			CellUVOffset = (int)Marshal.OffsetOf(typeof(GridVert), "CellUV");

			if (CellUVOffset + BlittableValueType<Vector4>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.TexCoord, 2, VertexAttribPointerType.Float, false, Stride, TexCoordOffset);
			GL.VertexAttribPointer((int)Elements.Barycentric, 3, VertexAttribPointerType.Float, false, Stride, BarycentricOffset);
			GL.VertexAttribPointer((int)Elements.CellUV, 4, VertexAttribPointerType.Float, false, Stride, CellUVOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.TexCoord);
			GL.EnableVertexAttribArray((int)Elements.Barycentric);
			GL.EnableVertexAttribArray((int)Elements.CellUV);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.TexCoord);
			GL.DisableVertexAttribArray((int)Elements.Barycentric);
			GL.DisableVertexAttribArray((int)Elements.CellUV);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return GridVert.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.TexCoord + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TexCoordOffset);
				GL.VertexAttribPointer((int)Elements.Barycentric + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, BarycentricOffset);
				GL.VertexAttribPointer((int)Elements.CellUV + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, CellUVOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TexCoord + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Barycentric + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.CellUV + baseLocation);

				baseLocation += 4;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.TexCoord + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TexCoordOffset);
				GL.VertexAttribPointer((int)Elements.Barycentric + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, BarycentricOffset);
				GL.VertexAttribPointer((int)Elements.CellUV + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, CellUVOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TexCoord + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Barycentric + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.CellUV + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.TexCoord + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Barycentric + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.CellUV + baseLocation, devisor);

				baseLocation += 4;
			}

			#endregion
		}

		#endregion
	}
}
