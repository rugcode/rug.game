﻿using Rug.Game.Core.Maths;
using Rug.Game.Core.Textures;

namespace Rug.Game.Deferred.Data.Terrain
{
	public interface ITerrainPatchDataProvider
	{
		int BlockSize { get; }

		float MapScale { get; }

		Texture2D NormalMap { get; }

		Texture2D TangentMap { get; }

		BoundingBox GetBounds(uint id); 

		void Write(TerrainPatch patch, uint id);

		bool HasChildren(uint p);

		void PopulateNormalMap();

		void PopulateTangentMap();
	}
}
