﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.OverPaint.Pipeline
{
	public class OverPaintFinal : PipelineBase, IResource
	{
		private bool m_IsLoaded;
		private TextureBox m_Box;

		#region Properties

		public Texture2D ColorTexture
		{
			get { return m_Box.Texture; }
			set { m_Box.Texture = value; }
		}

		public Texture2D DepthTexture { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}
		
		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}
		
		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		

		#endregion

		public OverPaintFinal()
		{
			Name = "Over Paint Final"; 

			m_Box = new TextureBox();

			m_Box.FlipHorizontal = false;
			m_Box.FlipVertical = true; 

		}

        public override void Update(View3D view)
		{
			
		}

		public override void Render(View3D view)
		{
			GLState.EnableBlend = false; 
			GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GLState.CullFace(CullFaceMode.Back);
			GLState.EnableCullFace = false;
			GLState.Apply(view); 
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			
			m_Box.Render(view); 
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false) 
			{
				m_Box.LoadResources();

				m_IsLoaded = true;
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_Box.UnloadResources();

				m_IsLoaded = false; 
			}
		}
	}
}
