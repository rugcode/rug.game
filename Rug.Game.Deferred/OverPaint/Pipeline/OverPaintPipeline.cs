﻿using Rug.Game.Core.MeshData;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.OverPaint.Pipeline
{
	public class OverPaintPipeline : PipelineManager, IResource, IResizablePipeline
	{
		private bool m_IsLoaded = false;
		private OverPaintCoordsPass m_CoordPass;

		#region Properties

		public OverPaintMeshesPipeline MeshesPipeline;

		public Viewport Viewport { get; set; } 

		public int Width { get; set; }
		public int Height { get; set; }
		public int MSAA { get; set; } 

		public MeshCollection Meshes { get; set; }
		
		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}		

		

		public bool ShowMaterials { get { return m_CoordPass.ShowMaterials; } set { m_CoordPass.ShowMaterials = value; } }

		public bool ShowNormals { get { return m_CoordPass.ShowNormals; } set { m_CoordPass.ShowNormals = value; } }

		#endregion

		public OverPaintPipeline(int width, int height, int msaa)
		{
			Name = "Over Paint Render";

			Width = width;
			Height = height;
			MSAA = msaa;

			Viewport = new Viewport(0, 0, Width, Height);
		}

		public void CreateDefault()
		{
			MeshesPipeline = new OverPaintMeshesPipeline(Width, Height, MSAA);
			MeshesPipeline.Initiate(); 
			this.Add(MeshesPipeline);

			m_CoordPass = new OverPaintCoordsPass();
			m_CoordPass.Meshes = Meshes;
			MeshesPipeline.Add(m_CoordPass);

			OverPaintFinal final = new OverPaintFinal();

			final.ColorTexture = MeshesPipeline.ColorTexture;
			final.DepthTexture = MeshesPipeline.DepthTexture;

			this.Add(final);			
		}

		public override void Render(View3D view)
		{
			base.Render(view);
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width;
			Height = height;

			Viewport = new Viewport(0, 0, Width, Height);

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(width, height, samples);
				}
			}
		}

		public void DownloadContents(IntPtr imagePtr)
		{
			MeshesPipeline.ColorTexture.DownloadImage(imagePtr); 
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.LoadResources(); 
					}
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.UnloadResources();
					}
				}

				m_IsLoaded = false; 
			}
		}

		#endregion
	}
}
