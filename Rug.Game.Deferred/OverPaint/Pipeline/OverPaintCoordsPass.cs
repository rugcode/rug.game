﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.MeshData;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Shaders.OverPaint;
using System;
using System.Collections.Generic;

namespace Rug.Game.OverPaint.Pipeline
{
	public class OverPaintCoordsPass : PipelineBase, IResource
	{
		protected OverPaint_Coords Effect;
		private Dictionary<int, LodDebugTexture2D> m_PixelSizeTextures = new Dictionary<int, LodDebugTexture2D>(); 

		private bool m_IsLoaded; 

		#region Properties

		public MeshCollection Meshes { get; set; }

		public override PipelineMode PipelineMode
		{
			get { return PipelineMode.Render | PipelineMode.Update; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public bool ShowMaterials { get; set; }

		public bool ShowNormals { get; set; }

		

		#endregion

		public OverPaintCoordsPass()
		{
			Name = "Over Paint Coords Pass"; 

			if (Effect == null)
			{
				Effect = SharedEffects.Effects["OverPaint_Coords"] as OverPaint_Coords;
			}
		}

        public override void Update(View3D view)
		{
		
		}

		public static uint UpperPowerOf2(uint v)
		{
			v--;
			v |= v >> 1;
			v |= v >> 2;
			v |= v >> 4;
			v |= v >> 8;
			//v |= v >> 16;
			v++;

			return v;
		}

		public override void Render(View3D view)
		{
			GLState.EnableDepthMask = true; 
			GLState.EnableDepthTest = true;
			GLState.EnableBlend = false;
			GLState.EnableDepthTest = true;
			GLState.EnableCullFace = true; 
			GLState.EnableDepthTest = true;
			GLState.FrontFace(FrontFaceDirection.Ccw); 
			GLState.Apply(view);

			if (Meshes != null)
			{
				Effect.Begin(ref view.World, ref view.Projection, ref view.NormalWorld);

				int total = Meshes.Count;
				int upper = (int)UpperPowerOf2((uint)total);
				
				float meshInc = 1f / upper; 
				float floatId = 0;

				foreach (Mesh mesh in Meshes)
				{
					mesh.FloatID = floatId; 

					if (mesh.IsVisible == true)
					{
						RenderMesh(mesh);
					}

					floatId += meshInc; 
				}

				Effect.End();
			}
		}

		protected virtual void RenderMesh(Mesh mesh) 
		{
			mesh.Bind();

			int size; 

			if (ShowMaterials == false)
			{
				size = mesh.DiffuseTexture.ResourceInfo.Size.Width;
			}
			else if (ShowNormals == false)
			{
				size = mesh.NormalTexture.ResourceInfo.Size.Width;
			}
			else
			{
				size = mesh.MaterialTexture.ResourceInfo.Size.Width;	
			}

			size *= 2; 

			LodDebugTexture2D pixelSizeTexture;

			if (m_PixelSizeTextures.TryGetValue(size, out pixelSizeTexture) == false)
			{
				pixelSizeTexture = new LodDebugTexture2D("LOD " + size.ToString(), size);

				pixelSizeTexture.LoadResources();

				m_PixelSizeTextures.Add(size, pixelSizeTexture); 
			}

			Effect.Render(ref mesh.ObjectMatrix, ref mesh.NormalMatrix, mesh.IndexCount, mesh.IndexType, pixelSizeTexture, mesh.FloatID);

			mesh.Unbind(); 
		}

		public override void Begin()
		{
			throw new NotImplementedException();
		}

		public override void End()
		{
			throw new NotImplementedException();
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				foreach (LodDebugTexture2D texture in m_PixelSizeTextures.Values)
				{
					texture.LoadResources(); 
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				foreach (LodDebugTexture2D texture in m_PixelSizeTextures.Values)
				{
					texture.UnloadResources();
				}

				m_IsLoaded = false;
			}			
		}		
	}
}

