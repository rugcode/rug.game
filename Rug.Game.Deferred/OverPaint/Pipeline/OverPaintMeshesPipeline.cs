﻿using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;

using Rug.Game.Core.Resources;

namespace Rug.Game.OverPaint.Pipeline
{
	public class OverPaintMeshesPipeline : PipelineManager, IResource, IResizablePipeline
	{
		private FrameBuffer m_FrameBuffer;
		private bool m_IsLoaded = false;

		#region Properties

		public int Width { get; set; }
		public int Height { get; set; }

		public FrameBufferTexture2D ColorTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0Ext]; } }

		public FrameBufferTexture2D DepthTexture { get { return m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt]; } }

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public new ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public Viewport Viewport { get; set; }

		

		private bool m_HighQuality;
		private int MSAA;

		#endregion

		public OverPaintMeshesPipeline(int width, int height, int msaa)
		{
			Name = "OverPaint Meshes";

			Width = width;
			Height = height;
			MSAA = msaa;

			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);
		}

		public void Initiate()
		{
			PixelInternalFormat internalFormat = PixelInternalFormat.Rgba32f;
			PixelType pixelType = PixelType.Float; 

			m_FrameBuffer = new FrameBuffer("Meshes Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = internalFormat,
						PixelFormat = PixelFormat.Rgba,
						PixelType = pixelType,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					},					
					new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
					{
						Size = new System.Drawing.Size(Width * MSAA, Height * MSAA),
						InternalFormat = (PixelInternalFormat)All.DepthComponent24,
						PixelFormat = PixelFormat.DepthComponent,
						PixelType = PixelType.UnsignedInt,
						MagFilter = TextureMagFilter.Linear,
						MinFilter = TextureMinFilter.Linear,
						WrapS = TextureWrapMode.ClampToBorder,
						WrapT = TextureWrapMode.ClampToBorder,
					}));

			// ArtworkEnvironment.ResolutionDependentResources.Add(m_FrameBuffer);
		}

		public override void Render(View3D view)
		{
			//GL.BindFramebuffer(FramebufferTarget.FramebufferExt, FBOHandle);
			//GL.DrawBuffers(2, buffers);


			Viewport backup = GLState.Viewport;
			GLState.Viewport = Viewport;
			//GLState.Apply(view); 
			
			m_FrameBuffer.Bind();

			//GLState.ClearColor(new Color4(0f, 0f, 0f, 1f)); 

			GLState.EnableDepthMask = true;
			GLState.EnableDepthTest = true; 
			//GLState.ClearColor(Color4.DarkOrange);
			GLState.ClearColor(Color4.Black);
			GLState.ClearDepth(1.0f);

			GLState.EnableBlend = false;
			GLState.EnableDepthTest = true;
			GLState.EnableCullFace = true;
			GLState.EnableAlphaTest = true; // (EnableCap.AlphaTest);
			GLState.AlphaFunc(AlphaFunction.Greater, 0.5f);
			GLState.EnableDepthTest = true;

			GLState.Apply(Width * MSAA, Height * MSAA);

			//GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit | ClearBufferMask.AccumBufferBit);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);			
			//GL.ClearBuffer(ClearBuffer.Color, 2, new float[] { 0f, 8f, 0f, 0f}); 

			base.Render(view);

			m_FrameBuffer.Unbind();

			GLState.EnableAlphaTest = false;

			GLState.Viewport = backup;

			//GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
		}

		public void Resize(int width, int height, MultiSamples samples)
		{
			Width = width;
			Height = height;

			this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

			foreach (IPipeline pipeline in this)
			{
				if (pipeline is IResizablePipeline)
				{
					IResizablePipeline res = pipeline as IResizablePipeline;

					res.Resize(width * MSAA, height * MSAA, MultiSamples.X1);
				}
			}
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				this.Viewport = new Viewport(0, 0, Width * MSAA, Height * MSAA);

				System.Drawing.Size size = new System.Drawing.Size(Width * MSAA, Height * MSAA);

				m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = size;
				m_FrameBuffer[FramebufferAttachment.DepthAttachmentExt].ResourceInfo.Size = size;

				m_FrameBuffer.LoadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.LoadResources();
					}
				}

				m_IsLoaded = true; 
			}
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_FrameBuffer.UnloadResources();

				foreach (IPipeline pipeline in this)
				{
					if (pipeline is IResource)
					{
						IResource res = pipeline as IResource;

						res.UnloadResources();
					}
				}

				m_IsLoaded = false; 
			}
		}

		#endregion
	}
}
