﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Deferred.Shaders.OverPaint
{
	public class OverPaint_Coords : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/OverPaint/OverPaint_Coords";
		private int uObjectMatrix;
		private int uNormalMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uNormalWorldMatrix;
		private int uMeshID;
		private int uPixelSizeTexture;

		#endregion

		public override string Name
		{
			get { return "Over Paint: Coords"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public OverPaint_Coords() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorldMatrix);
		}

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, int indexCount, DrawElementsType indexType, Texture2D pixelSize, float meshID)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);

			GL.Uniform1(uMeshID, meshID);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, pixelSize);

			//GLState.ActiveTexture(TextureUnit.Texture1);
			//GL.BindTexture(TextureTarget.Texture2D, 0);

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");			
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");

			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			uMeshID = GL.GetUniformLocation(ProgramHandle, "meshID");

			uPixelSizeTexture = GL.GetUniformLocation(ProgramHandle, "pixelSizeTexture");

			GL.Uniform1(uPixelSizeTexture, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

	}
}
