#version 410

uniform sampler2D pixelSizeTexture;

uniform float meshID = 0; 

in vec3 normal_var;
in vec2 uv_var;
in vec3 tangent_var;
in vec3 bitangent_var;
in vec4 color_var;
in vec4 material_var;

layout(location = 0) out vec4 colorFrag;

void main()
{	
	float pixelSize = texture(pixelSizeTexture, uv_var).r; 

	colorFrag = vec4(uv_var.x, uv_var.y, meshID, pixelSize);
}