#version 410

uniform mat4 objectMatrix;
uniform mat4 normalMatrix;
uniform mat4 worldMatrix;
uniform mat4 normalWorldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec3 position;
//layout(location = 1) in vec3 normal;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 textureCoords;

//out vec3 normal_var;
out vec4 color_var;
//out vec3 ncolor_var;
out vec2 textureCoords_var; 
//out vec4 material_var;
//out float depth; 

void main()
{
	color_var = color; 
	//material_var = vec4(0.75, 20, 1, 0.2); 
	textureCoords_var = textureCoords; 

    vec4 localSpace = objectMatrix *  vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;
	//depth = gl_Position.z; 

	//ncolor_var = normal * 0.5 + 0.5; 
    //vec4 normalLok = normalMatrix * vec4(normal, 1);
    //normal_var = (normalWorldMatrix * vec4(normal, 1)).xyz; // (normalWorldMatrix * normalLok).xyz; 
	//normal_var = normal; 
}