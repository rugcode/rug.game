﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using System;


namespace Rug.Game.Shaders.Deferred
{	
	public class Deferred_Final : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/Final";

		//private int m_Texture;
		private int uLightDir;
		private int uLambertMap;
		private int uColorTexture;
		private int uNormalTexture;
		private int uDepthTexture;
		private int uPointLightTexture;
		private int uShadowTexture;
		private int uWorldMatrix;
		private int uPerspectiveInvMatrix;
		private int uWorldMatrixInv;
		private int uTime;
		private int uColorFactor;
		private int uScreenToWorld;

		#endregion

		public override string Name
		{
			get { return "Deferred: Final"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Final() { }

		public void Render(View3D view, Texture2D colorTexture, Texture2D normalTexture, Texture2D depthTexture, Texture2D lightingTexture, Texture2D lambertMap, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, colorTexture);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, normalTexture);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, depthTexture);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, lightingTexture);
            GLState.BindTexture(TextureUnit.Texture4, TextureTarget.Texture2D, lambertMap);

			//GL.ActiveTexture(TextureUnit.Texture5);
			//GL.BindTexture(TextureTarget.Texture2D, shadowTexture.ResourceHandle);

			//Vector3 lightVec = Vector3.TransformVector(view.LightDirection, view.ProjectionInverse); // pretty good 
			//Vector3 lightVec = Vector3.TransformVector(view.LightDirection, view.WorldInverse * view.ProjectionInverse);
			Vector3 lightVec = view.LightDirection;
			lightVec.Normalize();

			//GL.Uniform3(uLightDir, view.LightDirection.X, view.LightDirection.Y, view.LightDirection.Z);
			GL.Uniform3(uLightDir, lightVec.X, lightVec.Y, lightVec.Z);

			GL.UniformMatrix4(uWorldMatrix, false, ref view.NormalWorld);
			
			Matrix4 worldMatrixInv = view.NormalWorld;
			worldMatrixInv.Invert();

			GL.UniformMatrix4(uWorldMatrixInv, false, ref worldMatrixInv);

			GL.UniformMatrix4(uPerspectiveInvMatrix, false, ref view.ProjectionInverse);

			Matrix4 screenToWorld = view.View * view.Projection;
			screenToWorld.Invert();

			GL.UniformMatrix4(uScreenToWorld, false, ref screenToWorld); 

			float dayTime_Sin = ((float)Math.Sin(view.DayTime * Math.PI) * 0.5f) + 0.5f;

			GL.Uniform1(uTime, dayTime_Sin);
			GL.Uniform1(uColorFactor, (1f - dayTime_Sin) * 0.5f);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			// Opto Remove
			//GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
			SimpleVertex.Unbind();

			// Opto Remove
			//GL.ActiveTexture(TextureUnit.Texture0);
			//GL.BindTexture(TextureTarget.Texture2D, 0);

			//GL.ActiveTexture(TextureUnit.Texture1);
			//GL.BindTexture(TextureTarget.Texture2D, 0);

			//GL.ActiveTexture(TextureUnit.Texture2);
			//GL.BindTexture(TextureTarget.Texture2D, 0);

			//GL.ActiveTexture(TextureUnit.Texture3);
			//GL.BindTexture(TextureTarget.Texture2D, 0);

			//GL.ActiveTexture(TextureUnit.Texture4);
			//GL.BindTexture(TextureTarget.Texture2D, 0);

			//GL.ActiveTexture(TextureUnit.Texture5);
			//GL.BindTexture(TextureTarget.Texture2D, 0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uLightDir = GL.GetUniformLocation(ProgramHandle, "lightDir");
			uLambertMap = GL.GetUniformLocation(ProgramHandle, "lambertTexture");
			uColorTexture = GL.GetUniformLocation(ProgramHandle, "colorTexture");
			uNormalTexture = GL.GetUniformLocation(ProgramHandle, "normalTexture");
			uDepthTexture = GL.GetUniformLocation(ProgramHandle, "depthTexture");
			uPointLightTexture = GL.GetUniformLocation(ProgramHandle, "pointLightTexture");
			uShadowTexture = GL.GetUniformLocation(ProgramHandle, "shadowTexture");

			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveInvMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveInvMatrix");

			uWorldMatrixInv = GL.GetUniformLocation(ProgramHandle, "worldMatrixInv");
			uScreenToWorld = GL.GetUniformLocation(ProgramHandle, "screenToWorld"); 

			uTime = GL.GetUniformLocation(ProgramHandle, "time");
			uColorFactor = GL.GetUniformLocation(ProgramHandle, "colorFactor");

			GL.Uniform1(uColorTexture, 0);
			GL.Uniform1(uNormalTexture, 1);
			GL.Uniform1(uDepthTexture, 2);
			GL.Uniform1(uPointLightTexture, 3);
			GL.Uniform1(uLambertMap, 4);
			GL.Uniform1(uShadowTexture, 5);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)UIVertex.Elements.Position, "position"); // "pos"

			GL.BindAttribLocation(ProgramHandle, (int)UIVertex.Elements.TextureCoords, "uv"); // "tex"
			
			GL.BindAttribLocation(ProgramHandle, (int)UIVertex.Elements.Color, "col");
			*/
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
