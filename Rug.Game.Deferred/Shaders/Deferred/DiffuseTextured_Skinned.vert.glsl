#version 410

uniform mat4 objectMatrix;
uniform mat4 perspectiveMatrix;
uniform mat4 worldMatrix;

uniform mat4 normalMatrix;
uniform mat4 normalWorldMatrix;

uniform mat4 boneMatrices[30];

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 tangent;
layout(location = 3) in vec2 uv;
layout(location = 4) in vec4 color;
layout(location = 5) in vec4 material;

layout(location = 6) in vec4 indices;
layout(location = 7) in vec4 weights;

out vec3 normal_var;
out vec3 tangent_var;
out vec3 bitangent_var;
out vec2 uv_var;
out vec4 color_var;
out vec4 material_var;

void main()
{
	color_var = color; 
	material_var = material; 

	vec4 pos = vec4(position, 1); 
	vec3 tang = tangent.xyz;

	vec4 transformedPosition = vec4(0.0);
    vec3 transformedNormal = vec3(0.0);
	vec3 transformedTangent = vec3(0.0);

    vec4 curIndex = indices;
    vec4 curWeight = weights;

    for (int i = 0; i < 4; i++)
    {
        mat4 m44 = boneMatrices[int(curIndex.x)];
        
        // transform the offset by bone i
        transformedPosition += m44 * pos * curWeight.x;

        mat3 m33 = mat3(m44[0].xyz,
                        m44[1].xyz,
                        m44[2].xyz);

        // transform normal by bone i
        transformedNormal += m33 * normal * curWeight.x;
		transformedTangent += m33 * tang * curWeight.x;

        // shift over the index/weight variables, this moves the index and 
        // weight for the current bone into the .x component of the index 
        // and weight variables
        curIndex = curIndex.yzwx;
        curWeight = curWeight.yzwx;
    }

	
    vec4 localSpace = objectMatrix *  transformedPosition;
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;

    vec4 normalLok = normalMatrix * vec4(transformedNormal, 1);
    normal_var = (normalWorldMatrix * normalLok).xyz; 

	vec4 tangentLok = normalMatrix * vec4(transformedTangent, tangent.w);
    tangent_var = (normalWorldMatrix * tangentLok).xyz;
	bitangent_var = cross(tangent_var, normal_var);
	normalize(bitangent_var);
    uv_var = uv;
}