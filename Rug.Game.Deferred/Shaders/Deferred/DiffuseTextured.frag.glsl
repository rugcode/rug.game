#version 410

#include ~/Shaders/Deferred/_Utils.glsl

uniform sampler2D textureNormal;
uniform sampler2D textureDiffuse;
uniform sampler2D textureMaterial;
uniform sampler2D materialLookup;

in vec3 normal_var;
in vec2 uv_var;
in vec3 tangent_var;
in vec3 bitangent_var;
in vec4 color_var;
in vec4 material_var;

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;
layout(location = 2) out vec4 materialFrag;

void main()
{
	vec4 normal = texture(textureNormal, uv_var); 

	vec4 difColor = texture(textureDiffuse, uv_var);
	
	vec4 materialSettings = material_var; // texture(materialLookup, vec2(texture(textureMaterial, uv_var).r, 0)); 

	//colorFrag = vec4((difColor.rgb * color_var.rgb) * (1.0 - materialSettings.a), 1.0);
	colorFrag = vec4((color_var.rgb) * (1.0 - materialSettings.a), 1.0);

#section !ShowBump 	
	materialFrag = materialSettings; 	
#end 

#section ShowBump 
	materialFrag = vec4(normal.a); 
#end

	vec3 ts_normal = DecodeNormal(normal.xyz); 
	vec3 ws_normal = tangent_var * ts_normal.x + bitangent_var * ts_normal.y + normal_var * ts_normal.z;

    normalFrag = vec4(EncodeNormal(ws_normal), 1f); 
}