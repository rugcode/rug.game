﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Shaders.Deferred
{	
	public class Deferred_TerrainSimple : BasicEffectBase
	{
		#region Private Members

		//private string m_ShaderLocation = @"~/Data/Shaders/Deferred/Color";
		private string m_ShaderLocation = @"~/Shaders/Deferred/TerrainSimple";
		private int uObjectMatrix;
		private int uNormalMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uNormalWorldMatrix;
		private int uTextureNormal;

		#endregion

		public override string Name
		{
			get { return "Deferred: Terrain Simple"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_TerrainSimple() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix, 
						  Texture2D normalMap)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorldMatrix);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, normalMap);

            Matrix4 ident = Matrix4.CreateRotationX(0);

			Matrix4 normal = ident;

			normal.Invert();
			normal.Transpose();

			GL.UniformMatrix4(uObjectMatrix, false, ref ident);
			GL.UniformMatrix4(uNormalMatrix, false, ref normal);
		}

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, int indexCount, DrawElementsType indexType)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void Render(int indexCount, DrawElementsType indexType)
		{
			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			uTextureNormal = GL.GetUniformLocation(ProgramHandle, "textureNormal");

			GL.Uniform1(uTextureNormal, 0);

			//uTextureFilter = GL.GetUniformLocation(ProgramHandle, "textureNormal");

			//GL.Uniform1(uTextureNormal, 0);

			//uint createWeightTexture(4, float B, float C)

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.Position, "position");

			//GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.Normal, "normal");

			GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.Color, "color");

			GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.TextureCoords, "textureCoords");
			*/ 

			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");

			GL.BindFragDataLocation(ProgramHandle, 1, "normalFrag");

			GL.BindFragDataLocation(ProgramHandle, 2, "materialFrag");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

		float MitchellNetravali(float x, float B, float C)
		{
			float ax = Math.Abs(x);

			if (ax < 1)
			{
				return ((12 - 9 * B - 6 * C) * ax * ax * ax +
						(-18 + 12 * B + 6 * C) * ax * ax + (6 - 2 * B)) / 6;
			}
			else if ((ax >= 1) && (ax < 2))
			{
				return ((-B - 6 * C) * ax * ax * ax +
						(6 * B + 30 * C) * ax * ax + (-12 * B - 48 * C) *
						ax + (8 * B + 24 * C)) / 6;
			}
			else
			{
				return 0;
			}
		}

		public const int GL_FLOAT_RGBA_NV = ((int)0x8883);

		// Create a 1D float texture encoding weight for cubic filter
		uint createWeightTexture(int size, float B, float C)
		{
			float[] img = new float[size * 4];
			int ptr = 0;
			
			for (int i = 0; i < size; i++)
			{
				float x = i / (float)(size - 1);
				img[ptr++] = MitchellNetravali(x + 1, B, C);
				img[ptr++] = MitchellNetravali(x, B, C);
				img[ptr++] = MitchellNetravali(1 - x, B, C);
				img[ptr++] = MitchellNetravali(2 - x, B, C);
			}
			
			uint texid;

			GL.GenTextures(1, out texid);
			TextureTarget target = TextureTarget.TextureRectangle; // .TextureRectangleNv;
			GL.BindTexture(target, texid);
			GL.TexParameter(target, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.TexParameter(target, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(target, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge); // GL_TEXTURE_WRAP_S
			GL.TexParameter(target, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
			GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
			GL.TexImage2D(target, 0, PixelInternalFormat.Rgba16f, size, 1, 0, PixelFormat.Rgba, PixelType.Float, img); // GL_FLOAT_RGBA_NV

			return texid;
		}
	}
}
