#version 410

uniform mat4 objectMatrix;
uniform mat4 worldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 tex;
//layout(location = 2) in vec4 col;

layout(location = 2) in vec4 instance_pos;

out vec4 output_col;
out vec2 output_tex;

void main()
{
    vec4 localSpace = objectMatrix * instance_pos;
    vec4 worldSpace = worldMatrix * localSpace;

	vec4 vertex_pos = perspectiveMatrix * vec4(worldSpace.xyz + pos, 1);

	//vec4 vertex_pos = perspectiveMatrix * vec4(pos, 1); 

	gl_Position = vertex_pos;

	output_col = vec4(1, 1, 1, 1);
	output_tex = tex;
}
