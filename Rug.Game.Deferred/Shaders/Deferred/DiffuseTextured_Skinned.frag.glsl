#version 410

uniform sampler2D textureNormal;
uniform sampler2D textureDiffuse;
uniform sampler2D textureMaterial;

in vec3 normal_var;
in vec2 uv_var;
in vec3 tangent_var;
in vec3 bitangent_var;
in vec4 color_var;
in vec4 material_var;

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;
layout(location = 2) out vec4 materialFrag;

void main()
{
	vec4 difColor = texture(textureDiffuse, uv_var);
	
	colorFrag = vec4(difColor.rgb * color_var.rgb, difColor.a * color_var.a);
	
	//materialFrag = texture(textureMaterial, uv_var); // material_var; 
	materialFrag = material_var; 
	//materialFrag = vec4(materialFrag.r, (materialFrag.g * 16.0) * (materialFrag.g * 16.0), materialFrag.b, materialFrag.a); 

	vec3 ts_normal = texture(textureNormal, uv_var).xyz * 2.0 - 1.0; 
	vec3 ws_normal = tangent_var * ts_normal.x + bitangent_var * ts_normal.y + normal_var * ts_normal.z;
    normalFrag = vec4(ws_normal * 0.5 + 0.5, difColor.a); 
}