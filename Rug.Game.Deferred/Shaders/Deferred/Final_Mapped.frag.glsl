#version 410
const vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
const vec4  kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
const vec4  kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);

const vec4  kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
const vec4  kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
const vec4  kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);


const float strength = 0.90;
const float falloff = 0.0002;
const float rad = 0.01;
const float invSamples = -1.38/10.0;

uniform sampler2D lambertTexture; 
uniform sampler2D pointLightTexture; 
uniform sampler2D colorTexture; 
uniform sampler2D normalTexture; 
uniform sampler2D materialTexture; 
uniform sampler2D depthTexture; 
uniform sampler2D shadowTexture;
uniform sampler2D colorCastTexture;

uniform samplerCube ambiantMap; 
uniform samplerCube specularMap; 
uniform samplerCube reflectionMap; 
uniform samplerCube fogMap; 

uniform mat4 perspectiveInvMatrix;
uniform mat4 normalWorldInv;

uniform mat4 light1Matrix;

uniform float time;
uniform float colorFactor;
uniform float indirectAmbiant;
uniform vec3 camPos; 

uniform float colorCastAmount; 

uniform float blurKernel[5]; 

in vec2 uv_var;
in vec3 lightDir_var;

out vec4 colorFrag;


vec3 RGBToYIQ(vec3 color) 
{
	vec4 color4 = vec4(color, 1);

	// Convert to YIQ
    float   YPrime  = dot (color4, kRGBToYPrime);
    float   I      = dot (color4, kRGBToI);
    float   Q      = dot (color4, kRGBToQ);

	return vec3(YPrime, I, Q); 
}

vec3 YIQToRGB(vec3 yiq) 
{
    // Convert back to RGB
    vec4 yIQ = vec4(yiq, 0.0);

	vec3 color = vec3(0.0); 

    color.r = dot (yIQ, kYIQToR);
    color.g = dot (yIQ, kYIQToG);
    color.b = dot (yIQ, kYIQToB);

	return color; 
}

vec3 ContrastSaturationBrightness(vec3 color, float brt, float sat, float con)
{
	// Increase or decrease theese values to adjust r, g and b color channels seperately
	const float AvgLumR = 0.5;
	const float AvgLumG = 0.5;
	const float AvgLumB = 0.5;
	
	const vec3 LumCoeff = vec3(0.2125, 0.7154, 0.0721);
	
	vec3 AvgLumin = vec3(AvgLumR, AvgLumG, AvgLumB);
	vec3 brtColor = color * brt;
	vec3 intensity = vec3(dot(brtColor, LumCoeff));
	vec3 satColor = mix(intensity, brtColor, sat);
	vec3 conColor = mix(AvgLumin, satColor, con);
	return conColor;
}

float DepthToZPosition(float depth, float near, float far) 
{ 
    return 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));	
}

#define SIGMA 10.0
#define BSIGMA 0.5
// #define BSIGMA 0.3
#define MSIZE 5
// #define MSIZE 15 

float normpdf(in float x, in float sigma)
{
	return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

float normpdf2(in vec2 v, in float sigma)
{
	return 0.39894*exp(-0.5*dot(v,v)/(sigma*sigma))/sigma;
}

float normpdf4(in vec4 v, in float sigma)
{
	return 0.39894*exp(-0.5*dot(v,v)/(sigma*sigma))/sigma;
}

vec4 blurShadow(float screenDepth)
{

	float depth = DepthToZPosition(screenDepth, 0.1, 6000.0);
	float depthThreash = 5.0; 

#section LowQualityBlur
	// ************ LOW QULAITY BUT FASTER BLUR ************ 

	vec2 texelSize = 1.0 / vec2(textureSize(shadowTexture, 0));
	
	float uBlurSize = 5.0; 

	vec4 fResult =  vec4(0); 

//	ideally use a fixed size noise and blur so that this loop can be unrolled
	//fResult = vec4(0.0);
	vec2 hlim = vec2(float(-uBlurSize) * 0.5 + 0.5);

#section !UnrollBlur
	
	float validCount = 0; 

	for (int x = 0; x < 5.0; ++x) {
		for (int y = 0; y < 5.0; ++y) {
			vec2 offset = vec2(float(x), float(y));

			offset += hlim;

			offset *= texelSize;
#section DoBlurDepthTest
			float sampleDepth = DepthToZPosition(texture(depthTexture, uv_var + offset).x, 0.1, 6000.0);
			
			//if (abs(depth - sampleDepth) <  depthThreash)

			float conditionFactor = 1.0 - clamp(sign(abs(depth - sampleDepth) - depthThreash), 0.0, 1.0);		
#end

#section !DoBlurDepthTest
			float conditionFactor = 1.0; 
#end

			fResult += texture(shadowTexture, uv_var + offset) * conditionFactor;

			validCount += conditionFactor; 
		}
	}

	fResult = fResult / validCount;
#end

#section UnrollBlur
	// NO SIGNIFICANT BENIFIT IS GOTTON FROM UN-ROLLING 
	fResult += texture(shadowTexture, uv_var + ((vec2(0.0, 0.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(0.0, 1.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(0.0, 2.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(0.0, 3.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(0.0, 4.0) + hlim) * texelSize));

	fResult += texture(shadowTexture, uv_var + ((vec2(1.0, 0.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(1.0, 1.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(1.0, 2.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(1.0, 3.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(1.0, 4.0) + hlim) * texelSize));

	fResult += texture(shadowTexture, uv_var + ((vec2(2.0, 0.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(2.0, 1.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(2.0, 2.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(2.0, 3.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(2.0, 4.0) + hlim) * texelSize));

	fResult += texture(shadowTexture, uv_var + ((vec2(3.0, 0.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(3.0, 1.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(3.0, 2.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(3.0, 3.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(3.0, 4.0) + hlim) * texelSize));

	fResult += texture(shadowTexture, uv_var + ((vec2(4.0, 0.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(4.0, 1.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(4.0, 2.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(4.0, 3.0) + hlim) * texelSize));
	fResult += texture(shadowTexture, uv_var + ((vec2(4.0, 4.0) + hlim) * texelSize));

	fResult = fResult / (uBlurSize * uBlurSize);
#end

	return fResult;
#end

#section !LowQualityBlur 
	vec2 iResolution = 1.0 / vec2(textureSize(shadowTexture, 0));

	vec4 c = texture(shadowTexture, uv_var);

	//declare stuff
	const int kSize = (MSIZE-1)/2;
	//float kernel[MSIZE];
	vec4 final_colour = vec4(0.0);
	
	//create the 1-D kernel
	float Z = 0.0;

#section !UniformBlurKernal
	float kernel[MSIZE];
	for (int j = 0; j <= kSize; ++j)
	{
		kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), SIGMA);
	}
#end

	vec4 cc;
	float factor;
	float bZ = 1.0/normpdf(0.0, BSIGMA);
	float sampleDepth; 

	//read out the texels
	for (int i=-kSize; i <= kSize; ++i)
	{
		for (int j=-kSize; j <= kSize; ++j)
		{
			vec2 offset = (vec2(float(i),float(j)) * iResolution.xy); 
#section DoBlurDepthTest		
			sampleDepth = DepthToZPosition(texture(depthTexture, uv_var + offset).x, 0.1, 6000.0);
			
			//if (abs(depth - sampleDepth) <  depthThreash)

			float conditionFactor = 1.0 - clamp(sign(abs(depth - sampleDepth) - depthThreash), 0.0, 1.0);
#end

#section !DoBlurDepthTest
			float conditionFactor = 1.0;
#end

			cc = texture(shadowTexture, uv_var + offset);
#section !UniformBlurKernal
			factor = normpdf2(cc.xy-c.xy, BSIGMA)*bZ*kernel[kSize+j]*kernel[kSize+i];
#end

#section UniformBlurKernal
			factor = normpdf2(cc.xy-c.xy, BSIGMA)*bZ*blurKernel[kSize+j]*blurKernel[kSize+i];
#end
			factor *= conditionFactor; 

			Z += factor;
			final_colour += factor*cc;
			
		}
	}
	
	final_colour = final_colour / Z;

	//final_colour.y = 1.0;

	final_colour.z = c.z; 

	return final_colour; 
#end
}


void main()
{		
	float depthScreen = texture(depthTexture, uv_var).x;

	float depth = depthScreen * 2.0 - 1.0;
	vec4 packed_Color = texture(colorTexture, uv_var);
	vec4 material = texture(materialTexture, uv_var);
	vec3 normal = normalize(texture(normalTexture, uv_var).xyz * 2.0 - 1.0);
	
#section DynamicLights
	vec4 pLight = texture(pointLightTexture, uv_var);
#end 

#section !DynamicLights
	vec4 pLight = vec4(0, 0, 0, 0); 
#end

#section Shadows	
	vec4 shadow4 = blurShadow(depthScreen); // texture(shadowTexture,uv_var);
#end

#section !Shadows
	vec4 shadow4 = vec4(1.0); 
#end

	float ambientOcclusion = shadow4.y;
	float shadow = shadow4.x;
	float debugTerm = shadow4.z;


	vec3 col = packed_Color.rgb; 
	float emissive = 1.0 - material.b;
	float emissive_inv = material.b; 
	float specAmmount = material.r;  
	float specFunc = (material.g * 16.0) * (material.g * 16.0);    
	float ambiantAmmount = material.b;  
	float gloss = material.a; 
	
	vec3 pos = vec3(0.0, 0.0, 0.0);
	pos.xy = uv_var * 2.0 - 1.0;
	pos.z = depth;
	vec4 worldPos = perspectiveInvMatrix * vec4(pos, 1.0);
	worldPos.xyz /= worldPos.w;	
	


	vec3 lightDir = normalize(lightDir_var);


	vec3 finalSum =  vec3(1.0, 1.0, 1.0); 
	vec3 lighting = vec3(1.0, 1.0, 1.0); 

#section !Material

#section Textured
	finalSum = col; 
#end

#section Ambient
	// AMBIANT LIGHTING
	// *********************************************************************
	//float lambert = clamp(((dot(normal, lightDir) * 0.5) + 0.5) * shadow, 0.0, 1.0);
	//float lambert = clamp(((dot(normal, lightDir) * 0.5) + 0.5), 0.0, shadow);
	float lambert = (dot(normal, lightDir) * 0.5) + 0.5;
	
	float diffuseShadow = indirectAmbiant + (1.0 - indirectAmbiant) * shadow; 

	// ambiant light would intergrate shadow also
	vec3 ambiantLight = texture(lambertTexture, vec2(time, 1.0 - (lambert * diffuseShadow * ambientOcclusion))).xyz * ambiantAmmount; 

	// LIGHTING SUM 
	// *********************************************************************	
	lighting = ambiantLight + pLight.xyz;

	finalSum = (finalSum * emissive) + (finalSum * lighting * emissive_inv); 
#end

#section Specular
	// SPECULAR HIGHLIGHT
	// *********************************************************************
	vec3 eyeVecNormal = normalize(-worldPos.xyz);
	vec3 reflectVec = normalize(reflect(-lightDir, normal));	
	float specular = pow(max(dot(eyeVecNormal, reflectVec), 0.0), specFunc) * specAmmount;

	finalSum = finalSum + clamp(specular * shadow, 0.0, 1.0) + pLight.w;
#end	


#section Gloss			
	// REFLECTION MAP (GLOSS)
	// *********************************************************************	
	vec3 camRay = normalize(worldPos.xyz); 
	
	vec3 reflectedDirection = reflect(camRay, normal); 

	// rotate the reflection vector into world space
	reflectedDirection = (normalWorldInv * vec4(reflectedDirection, 0)).xyz; 

	vec3 relectionMapColor = texture(reflectionMap, reflectedDirection).rgb * gloss;	

	finalSum = finalSum + relectionMapColor;
#end	

#section ColorCast
	float colorCastFactor = clamp(pow(1.0 - clamp((1.0 - depthScreen) * 400, 0.0, 1.0), 2.0), 0.0, 1.0);	

	vec4 colorCastColor = texture(colorCastTexture, vec2(time, colorCastFactor));

#section ColorCast_PreserveChroma	
	vec3 colorCastColor_YIQ = RGBToYIQ(colorCastColor.rgb); 
	
	vec3 finalSum_YIQ = RGBToYIQ(finalSum); 

	float finalSum_Y = finalSum_YIQ.x; 

	finalSum_YIQ = mix(finalSum_YIQ, colorCastColor_YIQ, colorCastAmount * colorCastColor.a);

	finalSum_YIQ.x = finalSum_Y; 

	finalSum = YIQToRGB(finalSum_YIQ); 	
#end

#section !ColorCast_PreserveChroma
	finalSum = mix(finalSum, colorCastColor.rgb, colorCastAmount * colorCastColor.a);
#end

#end 

//#section Fog
	// FOG 
	// *********************************************************************	
	worldPos = normalWorldInv * worldPos; 
	vec4 fogColor = texture(reflectionMap, normalize(worldPos).xyz); 
	float fogFactor = pow(1.0 - clamp((1.0 - depthScreen) * 20000, 0.0, 1.0), 2.0);			
	fogColor = mix(texture(fogMap, normalize(worldPos).xyz), fogColor, (clamp(depthScreen, 0.9, 1.0) - 0.9) * 10);


	// FINAL SUM 
	// *********************************************************************	
	//vec4 final = mix(vec4((col * emissive) + (col * lighting) + (relectionMapColor) + specular, 1.0), fogColor, fogFactor); 
	finalSum = mix(vec4(finalSum, 1.0), fogColor, fogFactor).rgb; 

//#end

	// FINAL FILTER
	// *********************************************************************	
	//colorFrag = vec4(ContrastSaturationBrightness(final.rgb, 1, 1.1, 1.2), 1.0); 
	colorFrag = vec4(finalSum.rgb, 1.0);
#end

#section Material
	colorFrag = vec4(material.rgb, 1.0); 
#end

#section Normal	
	vec4 normalFrag = normalWorldInv * vec4(normal.xyz, 1.0);
	
	colorFrag = vec4(normalFrag.xyz * 0.5 + 0.5, 1); 
#end
	
	//colorFrag.b = debugTerm;
	//colorFrag = vec4(shadow4.y);
	//colorFrag.a = 1.0;
	//colorFrag = vec4(normal.xyz * 0.5 + 0.5, 1); 

	/* 
	if (depthScreen < 0) 
	{ 
		colorFrag = vec4(-depthScreen * 0.01, 0, 0, 1);
	}
	else 
	{
		colorFrag = vec4(0, depthScreen * 0.01, 0, 1);
	}
	*/ 

	//colorFrag = vec4(shadow, shadow, shadow, 1); 

	// calculate the liminance and put in alpha channel 
	colorFrag.w = dot(colorFrag.xyz, vec3(0.299, 0.587, 0.114));	
}
