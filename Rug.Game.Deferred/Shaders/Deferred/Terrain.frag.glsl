#version 410

#include ~/Shaders/Deferred/_Utils.glsl

uniform mat4 normalMatrix;
uniform mat4 normalWorldMatrix;
uniform sampler2D textureNormal;
uniform sampler2D textureTangent;

uniform sampler2D surfaceDiffuse;
uniform sampler2D surfaceNormal;
uniform sampler2D surfaceSpec; 

//in vec3 normal_var;
in vec4 color_var;
//in vec3 ncolor_var;
//in vec4 material_var;
in vec2 textureCoords_var; 

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;
layout(location = 2) out vec4 materialFrag;

uniform float myTexResolution = 0.5 / 1024;
/* 
uniform float myTexResolution = 1024; // 1024; 

vec4 getNormal(vec2 p)
{
    p = p * myTexResolution + 0.5;

    vec2 i = floor(p);
    vec2 f = p - i;
    f = f * f * f * (f * (f * 6.0 - 15.0) + 10.0);
    p = i + f;

    p = (p - 0.5) / myTexResolution;
    return texture(textureNormal, p);
}
*/ 

vec4 convolve3x3PS(sampler2D sampler, 
	vec2 UV, 
	float W00, float W01, float W02,
	float W10, float W11, float W12,
	float W20, float W21, float W22,
	float Sum,
	float StepHoriz,
	float StepVert)
{
  vec2 ox = vec2(StepHoriz, 0.0);
  vec2 oy = vec2(0.0, StepVert);
  vec2 PP = UV.xy - oy;
  vec4 C00 = texture(sampler, PP - ox);
  vec4 C01 = texture(sampler, PP);
  vec4 C02 = texture(sampler, PP + ox);

  PP = UV.xy;

  vec4 C10 = texture(sampler, PP - ox);
  vec4 C11 = texture(sampler, PP);
  vec4 C12 = texture(sampler, PP + ox);

  PP = UV.xy + oy;

  vec4 C20 = texture(sampler, PP - ox);
  vec4 C21 = texture(sampler, PP);
  vec4 C22 = texture(sampler, PP + ox);

  vec4 Ci = C00 * W00;

  Ci += C01 * W01;
  Ci += C02 * W02;
  Ci += C10 * W10;
  Ci += C11 * W11;
  Ci += C12 * W12;
  Ci += C20 * W20;
  Ci += C21 * W21;
  Ci += C22 * W22;

  return (Ci / Sum);
}

void main()
{	
	vec3 normal = convolve3x3PS(textureNormal, textureCoords_var, 
/* 
				0.05, 0.125, 0.05,
				0.125, 0.25, 0.125,
				0.05, 0.125, 0.05,
				1.15,
*/ 
				1, 2, 1,
				2, 4, 2,
				1, 2, 1,
				16,
				myTexResolution,
				myTexResolution).xyz;

	vec4 ts_normal = vec4(normal, 1); 
	
	//vec4 tangentLok = normalMatrix * convolve3x3PS(textureTangent, textureCoords_var, 
	vec4 tangentLok = convolve3x3PS(textureTangent, textureCoords_var, 
/* 
			0.05, 0.125, 0.05,
			0.125, 0.25, 0.125,
			0.05, 0.125, 0.05,
			1.15,
*/ 
			1, 2, 1,
			2, 4, 2,
			1, 2, 1,
			16,
			myTexResolution,
			myTexResolution);

    vec3 tangent_var = (tangentLok).xyz;
	vec3 bitangent_var = cross(tangent_var, ts_normal.xyz);
	bitangent_var = normalize(bitangent_var);

	float depth = clamp((gl_FragCoord.z / gl_FragCoord.w) / 256.0, 0, 1);
	float fade = 1 - ((1 - depth) * clamp(normal.y * -1, 0, 1)); 

	//vec4 color_var = vec4(0.596, 0.494, 0.424, 0);
	//vec3 difColor = texture(surfaceDiffuse, textureCoords_var * 4096).rgb;		
	//colorFrag = vec4(mix(difColor.rgb, color_var.rgb, fade), color_var.a);
	
	colorFrag = color_var;

	vec3 s_normal = DecodeNormal(texture(surfaceNormal, textureCoords_var * 4096).xyz); // 8192
	vec3 ws_normal = tangent_var * s_normal.x + bitangent_var * s_normal.y + ts_normal.xyz * s_normal.z;
    
	normalFrag = mix(vec4(ws_normal, 1), vec4(ts_normal.xyz, 1), fade); 
	normalFrag.y *= -1.0; 
	normalFrag.xyz = EncodeNormal((normalWorldMatrix * normalFrag).xyz);

	float surfaceSpecR = texture(surfaceSpec, textureCoords_var * 4096).r; 

	// r = spec ammount | g = spec function | b = ambiant ammount | a = gloss 
	materialFrag = mix(vec4(surfaceSpecR, 30, 1, 0.001), vec4(0.02, 30, 1, 0.001), fade);
}