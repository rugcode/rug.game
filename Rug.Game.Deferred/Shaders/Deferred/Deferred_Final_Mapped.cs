﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;


namespace Rug.Game.Shaders.Deferred
{
	public enum FinalType
	{
		None = 0,
		Textured = 1,
		Ambient = 2,
		Specular = 4,
		Gloss = 8,
		DynamicLights = 16, 
		ColorCast = 32,
		Shadows = 64,

		All = Textured | Ambient | Specular | Gloss | DynamicLights | ColorCast | Shadows,

		Material = All + 1,
		Normal = All + 2,
	}

	public class Deferred_Final_Mapped : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/Final_Mapped";

		private int uLightDir;
		private int uLambertMap;
		private int uColorTexture;
		private int uNormalTexture;
		private int uDepthTexture;
		private int uPointLightTexture;
		private int uShadowTexture;
		private int uNormalMatrix;
		private int uPerspectiveInvMatrix;
		private int uNormalWorldInv;
		private int uTime;
		private int uColorFactor;
		private int uReflectionMap;
		private int uMaterialTexture;
		private int uFogMap;
		
		private FinalType m_FinalType;
		private int uCamPos;
		private int uColorCastTexture;
		private int uColorCastAmount;
		private int uIndirectAmbiant;
		private int uBlurKernel;
		//private int uAmbientAmmount;
		//private int uSpecularAmmount;
		//private int uGlossAmmount;

		#endregion

		public override string Name
		{
			get { return "Deferred: Final (" + m_FinalType.ToString() + ")"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Final_Mapped(params string[] defines) { Defines = defines; }

		public Deferred_Final_Mapped(FinalType finalType)
		{			
			m_FinalType = finalType;

			List<string> defines = new List<string>();

			if (finalType == FinalType.Material)
			{
				defines.Add("Material");
			}
			else if (finalType == FinalType.Normal)
			{
				defines.Add("Normal");
			}
			else
			{
				if ((m_FinalType & FinalType.Textured) == FinalType.Textured)
				{
					defines.Add("Textured");
				}

				if ((m_FinalType & FinalType.Ambient) == FinalType.Ambient)
				{
					defines.Add("Ambient");
				}

				if ((m_FinalType & FinalType.Specular) == FinalType.Specular)
				{
					defines.Add("Specular");
				}

				if ((m_FinalType & FinalType.Gloss) == FinalType.Gloss)
				{
					defines.Add("Gloss");
				}

				if ((m_FinalType & FinalType.DynamicLights) == FinalType.DynamicLights)
				{
					defines.Add("DynamicLights"); 
				}

				if ((m_FinalType & FinalType.ColorCast) == FinalType.ColorCast)
				{
					defines.Add("ColorCast");
				}

				if ((m_FinalType & FinalType.Shadows) == FinalType.Shadows)
				{
					defines.Add("Shadows");
				}
			}

			defines.Add("!LowQualityBlur");
			defines.Add("!UnrollBlur");
			defines.Add("UniformBlurKernal");
			defines.Add("DoBlurDepthTest");			

			Defines = defines.ToArray(); 
		}

		public void Render(View3D view, 
							Texture2D colorTexture, Texture2D normalTexture,
							Texture2D materialTexture, Texture2D depthTexture, 
							Texture2D lightingTexture, Texture2D shadowTexture,					
							Texture2D lambertMap, 
							Texture2D colorCastTexture, float colorCastAmount, 
							float indirectAmbiant, 
							TextureDDS reflectionsMap, TextureDDS fogMap,
							VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, colorTexture);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, normalTexture);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, materialTexture);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, depthTexture);
            GLState.BindTexture(TextureUnit.Texture4, TextureTarget.Texture2D, lightingTexture);
            GLState.BindTexture(TextureUnit.Texture5, TextureTarget.Texture2D, lambertMap);
	
            GLState.BindTexture(TextureUnit.Texture6, reflectionsMap);
            GLState.BindTexture(TextureUnit.Texture7, fogMap);

            GLState.BindTexture(TextureUnit.Texture8, TextureTarget.Texture2D, shadowTexture);
            GLState.BindTexture(TextureUnit.Texture9, TextureTarget.Texture2D, colorCastTexture);

			GL.Uniform1(uColorCastAmount, colorCastAmount); 

			GL.Uniform3(uCamPos, view.Camera.Center.X, view.Camera.Center.Y, view.Camera.Center.Z); 

			Vector3 lightVec = view.LightDirection;
			lightVec.Normalize();

			GL.Uniform3(uLightDir, lightVec.X, lightVec.Y, lightVec.Z);
			
			GL.Uniform1(uIndirectAmbiant, indirectAmbiant); 

			GL.UniformMatrix4(uNormalMatrix, false, ref view.NormalWorld);
			GL.UniformMatrix4(uPerspectiveInvMatrix, false, ref view.ProjectionInverse);

			Matrix4 normalWorldInv = view.NormalWorld;
			normalWorldInv.Invert();
			GL.UniformMatrix4(uNormalWorldInv, false, ref normalWorldInv);

			GL.Uniform1(uTime, view.DayTime);
			GL.Uniform1(uColorFactor, (1f - view.DayTime) * 0.5f);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uLightDir = GL.GetUniformLocation(ProgramHandle, "lightDir");
			uLambertMap = GL.GetUniformLocation(ProgramHandle, "lambertTexture");
			uColorTexture = GL.GetUniformLocation(ProgramHandle, "colorTexture");
			uMaterialTexture = GL.GetUniformLocation(ProgramHandle, "materialTexture");
			
			uNormalTexture = GL.GetUniformLocation(ProgramHandle, "normalTexture");
			
			uDepthTexture = GL.GetUniformLocation(ProgramHandle, "depthTexture");
			uPointLightTexture = GL.GetUniformLocation(ProgramHandle, "pointLightTexture");
			uShadowTexture = GL.GetUniformLocation(ProgramHandle, "shadowTexture");
			uColorCastTexture = GL.GetUniformLocation(ProgramHandle, "colorCastTexture");

			uColorCastAmount = GL.GetUniformLocation(ProgramHandle, "colorCastAmount");

			uReflectionMap = GL.GetUniformLocation(ProgramHandle, "reflectionMap");
			uFogMap = GL.GetUniformLocation(ProgramHandle, "fogMap");
			
			uPerspectiveInvMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveInvMatrix");

			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			uNormalWorldInv = GL.GetUniformLocation(ProgramHandle, "normalWorldInv");

			uIndirectAmbiant = GL.GetUniformLocation(ProgramHandle, "indirectAmbiant"); 

			uTime = GL.GetUniformLocation(ProgramHandle, "time");
			uColorFactor = GL.GetUniformLocation(ProgramHandle, "colorFactor");
			uCamPos = GL.GetUniformLocation(ProgramHandle, "camPos");

			uBlurKernel = GL.GetUniformLocation(ProgramHandle, "blurKernel");

			GL.Uniform1(uColorTexture, 0);
			GL.Uniform1(uNormalTexture, 1);
			GL.Uniform1(uMaterialTexture, 2);
			GL.Uniform1(uDepthTexture, 3);
			GL.Uniform1(uPointLightTexture, 4);
			GL.Uniform1(uLambertMap, 5);
			GL.Uniform1(uReflectionMap, 6);
			GL.Uniform1(uFogMap, 7);
			GL.Uniform1(uShadowTexture, 8);
			GL.Uniform1(uColorCastTexture, 9);

			GL.UseProgram(0);

			/*
			const int kSize = (MSIZE-1)/2;
			float kernel[MSIZE];
			vec4 final_colour = vec4(0.0);
	
			//create the 1-D kernel
			float Z = 0.0;
			for (int j = 0; j <= kSize; ++j)
			{
				kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), SIGMA);
			}
			*/

			ResizeBlurKernel(); 
		}

		//#define SIGMA 10.0
		//#define BSIGMA 0.5
		//#define MSIZE 5

		float normpdf(float x, float sigma)
		{
			return 0.39894f*(float)Math.Exp(-0.5f*x*x/(sigma*sigma))/sigma;
		}

		bool ResizeBlurKernel()
		{
			int KernelSize = 5; 
			int kSize = (KernelSize-1)/2;
			float SIGMA = 10.0f; 

			// generate kernel:
			float[] kernel = new float[KernelSize];
			
			for (int j = 0; j <= kSize; ++j)
			{
				kernel[kSize+j] = kernel[kSize-j] = normpdf((float)j, SIGMA);
			}

			// upload the kernel:
			GL.UseProgram(ProgramHandle);

			GL.Uniform1(uBlurKernel, KernelSize, kernel);

			//OOGL_CALL(glUniform1i(shaderSsao_->getUniformLocation("uKernelSize"), ssaoKernelSize));
			//OOGL_CALL(glUniform3fv(shaderSsao_->getUniformLocation("uKernelOffsets"), ssaoKernelSize_, (const GLfloat*) kernel));

			GL.UseProgram(0);

			return true;
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
