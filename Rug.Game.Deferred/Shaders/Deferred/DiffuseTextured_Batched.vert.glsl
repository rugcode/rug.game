#version 410

//uniform mat4 objectMatrix;
uniform mat4 perspectiveMatrix;
uniform mat4 worldMatrix;

//uniform mat4 normalMatrix;
uniform mat4 normalWorldMatrix;


layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 tangent;
layout(location = 3) in vec2 uv;
layout(location = 4) in vec4 color;
layout(location = 5) in vec4 material;

layout(location = 6) in mat4 objectMatrix;
layout(location = 10) in mat4 normalMatrix;

out vec3 normal_var;
out vec3 tangent_var;
out vec3 bitangent_var;
out vec2 uv_var;
out vec4 color_var;
out vec4 material_var;


void main()
{
	color_var = color; 
	material_var = material; 

    vec4 localSpace = objectMatrix *  vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;

    vec4 normalLok = normalMatrix * vec4(normal, 1);
    normal_var =(normalWorldMatrix * normalLok).xyz; 

	vec4 tangentLok = normalMatrix * tangent;

    tangent_var = (normalWorldMatrix * tangentLok).xyz;
	bitangent_var = cross(tangent_var, normal_var);

	bitangent_var = normalize(bitangent_var);
    uv_var = uv;
}