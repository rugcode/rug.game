#version 410

uniform vec3 lightDir;
uniform mat4 normalMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 col;

out vec2 uv_var;
out vec3 lightDir_var;

void main()
{  
    gl_Position = vec4(position, 1);
    uv_var = uv;
  
    lightDir_var = (normalMatrix * vec4(lightDir.xyz,1.0)).xyz;         
}
