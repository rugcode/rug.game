#version 410

uniform sampler2D flatTexture; 
uniform sampler2D textureNormal; 

uniform mat4 normalWorldMatrix;

in vec4 output_col;
in vec2 output_tex;

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;
layout(location = 2) out vec4 materialFrag;

void main()
{
	vec3 normal = texture(textureNormal, output_col).xyz; 
	vec4 ts_normal = normalWorldMatrix * vec4(normal, 1);  

	vec4 difColor = texture(flatTexture, output_tex) * output_col;	

	colorFrag = difColor * vec4(0.494, 0.596, 0.424, 0);
    normalFrag = vec4(ts_normal.xyz * 0.5 + 0.5, 1); 

	// r = spec ammount | g = spec function | b = ambiant ammount | a = gloss 
	materialFrag = vec4(0.2, 30, 1, 0.1);

	vec4 difColor = texture(surfaceDiffuse, output_tex) * output_col;	

	colorFrag = difColor;

	vec3 s_normal =  texture(textureNormal, output_tex).rgb * 2.0 - 1.0; // 8192
	//vec3 ws_normal = tangent_var * s_normal.x + bitangent_var * s_normal.y + ts_normal.xyz * s_normal.z;
    normalFrag = vec4(s_normal * 0.5 + 0.5, 1); 
	//normalFrag = vec4(ts_normal.xyz * 0.5 + 0.5, 1); 

	//float surfaceSpecR = texture(flatTexture, textureCoords_var).r; 
	materialFrag = vec4(0.02, 30, 1, 0.001);
}