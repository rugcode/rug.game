#version 410

#include ~/Shaders/Deferred/_Utils.glsl

uniform mat4 normalMatrix;
uniform mat4 normalWorldMatrix;
uniform sampler2D textureNormal;

in vec4 color_var;
in vec2 textureCoords_var; 

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;
layout(location = 2) out vec4 materialFrag;

uniform float myTexResolution = 0.5 / 1024;

vec4 convolve3x3PS(vec2 UV, 
  float W00, float W01, float W02,
  float W10, float W11, float W12,
  float W20, float W21, float W22,
  float Sum,
  float StepHoriz,
  float StepVert)
{
  vec2 ox = vec2(StepHoriz, 0.0);
  vec2 oy = vec2(0.0, StepVert);
  vec2 PP = UV.xy - oy;
  vec4 C00 = texture(textureNormal, PP - ox);
  vec4 C01 = texture(textureNormal, PP);
  vec4 C02 = texture(textureNormal, PP + ox);

  PP = UV.xy;

  vec4 C10 = texture(textureNormal, PP - ox);
  vec4 C11 = texture(textureNormal, PP);
  vec4 C12 = texture(textureNormal, PP + ox);

  PP = UV.xy + oy;

  vec4 C20 = texture(textureNormal, PP - ox);
  vec4 C21 = texture(textureNormal, PP);
  vec4 C22 = texture(textureNormal, PP + ox);

  vec4 Ci = C00 * W00;

  Ci += C01 * W01;
  Ci += C02 * W02;
  Ci += C10 * W10;
  Ci += C11 * W11;
  Ci += C12 * W12;
  Ci += C20 * W20;
  Ci += C21 * W21;
  Ci += C22 * W22;

  return (Ci / Sum);
}

void main()
{	
	vec3 normal = convolve3x3PS(textureCoords_var, 
	/* 
				  0.05, 0.125, 0.05,
				  0.125, 0.25, 0.125,
				  0.05, 0.125, 0.05,
				  1.15,
	*/ 
					1, 2, 1,
					2, 4, 2,
					1, 2, 1,
					16,
				  myTexResolution,
				  myTexResolution).xyz;

	normal.y *= -1.0;
	vec4 ts_normal = normalWorldMatrix * vec4(normal, 1);  
	
	colorFrag = color_var; // vec4(0.596, 0.494, 0.424, 0);
	normalFrag = vec4(EncodeNormal(ts_normal.xyz), 1); 
	
	// r = spec ammount | g = spec function | b = ambiant ammount | a = gloss 
	materialFrag = vec4(0.02, 30, 1, 0.001);
}