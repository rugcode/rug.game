#version 410

in vec3 normal_var;
in vec3 color_var;

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;

void main()
{
    normalFrag = vec4(normal_var.x * 0.5 + 0.5, normal_var.y * 0.5 + 0.5, normal_var.z * 0.5 + 0.5, 1.0);
    colorFrag = vec4(color_var, 1.0);
}

/* 
in VertexData 
{
	vec3 normal_var;
	vec3 color_var; 
} inVert;

void main()
{
    gl_FragData[1]  = vec4(inVert.normal_var.x*0.5+0.5, inVert.normal_var.y*0.5+0.5, inVert.normal_var.z  *0.5 + 0.5, 1.0);
	//gl_FragData[1]  = vec4(inVert.normal_var.x, inVert.normal_var.y, inVert.normal_var.z, 1.0);
    gl_FragData[0] = vec4(inVert.color_var,1.0); // vec4(1,1,1,1.0); // 
}
*/ 