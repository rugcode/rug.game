#version 410

uniform mat4 objectMatrix;
uniform mat4 normalMatrix;
uniform mat4 worldMatrix;
uniform mat4 normalWorldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 textureCoords;

out vec4 color_var;
out vec2 textureCoords_var; 

void main()
{
	color_var = color; 
	textureCoords_var = textureCoords; 

    vec4 localSpace = objectMatrix *  vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;
}