#version 410

in vec4 color_var;

layout(location = 0) out vec4 colorFrag;
layout(location = 1) out vec4 normalFrag;
layout(location = 2) out vec4 materialFrag;

void main()
{
	//vec3 col = packed_Color.rgb; 
	//float emissive = packed_Color.a;
	//float emissive_inv = 1 - emissive; 
	//float specAmmount = material.r;  
	//float specFunc = material.g;    
	//float ambiantAmmount = material.b;  
	//float gloss = material.a; 

	// r,g,b = albedo | a = emissive
    colorFrag = color_var;

	colorFrag.a = 0; 

	//colorFrag.a = 0.5; 
	//colorFrag.a = 0.6; 

	// r = spec ammount | g = spec function | b = ambiant ammount | a = gloss 
	//materialFrag = vec4(0.5, 8, 0.5, 0); 
	//materialFrag = vec4(0.2, 20, 0.3, 0); 
	materialFrag = vec4(0.2, 2, 0, gl_FragCoord.z); 
	normalFrag = vec4(0, 0, -1, 1); 
}