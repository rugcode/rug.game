﻿#define EPSILON 1e-6
__constant sampler_t sampler =  CLK_NORMALIZED_COORDS_FALSE |
                                CLK_ADDRESS_CLAMP_TO_EDGE |
                                CLK_FILTER_NEAREST;

#define IMAGE_WIDTH 320
#define IMAGE_HEIGHT 240 

float Sample(int x, int y, global float* heightMap)
{
	x = x < 0 ? 0 : x >= IMAGE_WIDTH ? IMAGE_WIDTH - 1 : x;
	y = y < 0 ? 0 : y >= IMAGE_HEIGHT ? IMAGE_HEIGHT - 1 : y;

	return heightMap[x + y * IMAGE_WIDTH];
}

float3 FetchNormalVector(float2 tc, float vPixelSizex, float vPixelSizey, global float* heightMap)
{	
	// Determine the offsets
	float2 off1 = (float2)(vPixelSizex, 0.0);
	float2 off2 = (float2)(0.0, vPixelSizey);

	// Take three samples to determine two vectors that can be
	// use to generate the normal at this pixel
	float h0 = Sample(convert_int(tc.x), convert_int(tc.y), heightMap);
	float h1 = Sample(convert_int((tc + off1).x), convert_int((tc + off1).y), heightMap);
	float h2 = Sample(convert_int((tc + off2).x), convert_int((tc + off2).y), heightMap);

	float3 v01 = (float3)(off1.x, h0 - h1, off1.y);
	float3 v02 = (float3)(off2.x, h0 - h2, off2.y);

	float3 n = cross((float4)(v01, 1.0), (float4)(v02, 1.0)).xyz;

	n = normalize(n); 	

	return n;
}


// Populate the normal buffer 
kernel void PopulateNormalMap(
    global float* heightMap,
	global float4* normals,
	const int bufferLength)
{
	int i = get_global_id(0);

	if(i >= bufferLength)
		return;

	int x = i % IMAGE_WIDTH; 
	int y = i / IMAGE_WIDTH; 

	float3 normal = FetchNormalVector((float2)(x, y), 1.0, 1.0, heightMap);

	//heightMap[i] = 0.5; 
	//vstore3(normal, i * 3, normals); 
	//normals[i] = normal; 
	normals[i] = (float4)(normal, 0.0); 
}


// find highest elevation angle in height map along a line
float Raycast(int x0, int y0, float angle, int length, 
				global float* heightMap)
{
	int baserow = y0;
	int basecol = x0;

	float startingheight = Sample(baserow, basecol, heightMap);

	int x1 = convert_int(x0 + cos(angle) * length);
	int y1 = convert_int(y0 + sin(angle) * length);

	bool steep = abs(y1 - y0) > abs(x1 - x0);

	float t; 

	if (steep == true)
	{
		t = x0; 
		x0 = y0; 
		y0 = t; 

		t = x1; 
		x1 = y1; 
		y1 = t; 
	}

	if (x0 > x1)
	{
		t = x0; 
		x0 = x1; 
		x1 = t;

		t = y0; 
		y0 = y1; 
		y1 = t;
	}

	int deltax = x1 - x0;
	int deltay = abs(y1 - y0);
	int error = deltax / 2;
	int y = y0;
	int ystep = (y0 < y1) ? 1 : -1;
	float maxelevation = 0;

	for (int x = x0; x <= x1; x++)
	{
		int row, col;

		if (steep == true)
		{
			row = x;
			col = y;
		}
		else
		{
			row = y;
			col = x;
		}

		if (baserow != row || basecol != col) // ignore first pixel
		{
			float distance = native_sqrt(convert_float((row - baserow) * (row - baserow) + (col - basecol) * (col - basecol)));
			maxelevation = max(maxelevation, (Sample(col, row, heightMap) - startingheight) / distance);

			error = error - deltay;

			if (error < 0)
			{
				y = y + ystep;
				error = error + deltax;
			}
		}
	}

	return maxelevation;
}


kernel void PopulateShadowMap(
	global float* heightMap,
	global float4* normals,	
	write_only image2d_t shadow,	
	int bufferLength, 
	const float angleStep)
{
	int i = get_global_id(0);

	if(i >= bufferLength)
		return;

	int x = i % IMAGE_WIDTH; 
	int y = i / IMAGE_WIDTH; 

	
	int raycount = 6; 
	int raylength = 32; 
	float strength = 0.01;

	/* 

	int w = IMAGE_WIDTH;
	int h = IMAGE_HEIGHT;

	float xsum = 0.0;
	float ysum = 0.0;

	float averagex = 0.0;
	float averagey = 0.0;
	float averagetotal = 0.0;

	for (float a = 0; a < M_PI_2; a += angleStep)
	{
		float xdir = cos(a);
		float ydir = sin(a);

		xsum += fabs(xdir);
		ysum += fabs(ydir);

		float ray = Raycast(y, x, a, raylength, heightMap);

		averagetotal += ray;

		averagex += xdir * ray;
		averagey += ydir * ray;
	}

	averagex /= xsum;
	averagey /= ysum;

	//float3 normal = vload3(i * 3, normals);
	float4 normal = normals[i];

	normal.x -= strength * averagex;
	normal.z -= strength * averagey;

	//vstore3(normal, i * 3, normals); 
	normals[i] = normal; 

	// compute average ao
	averagetotal /= raycount;
	*/ 

	int2 coord = (int2)(x, y);

	//shadow[i] = clamp(strength * averagetotal, 0.0, 1.0);
	//write_imagef(shadow, coord, (float4)(clamp(strength * averagetotal, 0.0, 1.0)));
	write_imagef(shadow, coord, (float4)(clamp(strength * heightMap[i], 0.0, 1.0)));

}

bool CloseEnough(float f1, float f2)
{
	// Determines whether the two floating-point values f1 and f2 are
	// close enough together that they can be considered equal.			
	return fabs((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < EPSILON;
}

float4 CalcTangentVector(
	float2 tc,
	float2 texCoord1, float2 texCoord2, float2 texCoord3,
	int vPixelSizex, int vPixelSizey,
	float4 normal, 
	global float* heightMap)
{
	float2 off1 = (float2)(vPixelSizex, 0.0f);
	float2 off2 = (float2)(0.0f, vPixelSizey);

	// Take three samples to determine two vectors that can be
	// use to generate the normal at this pixel

	//float h0 = Sample((int)tc.X, (int)tc.Y, heightMap);
	//float h1 = Sample((int)(tc + o1).X, (int)(tc + o1).Y, heightMap);
	//float h2 = Sample((int)(tc + o2).X, (int)(tc + o2).Y, heightMap);

	float h0 = Sample(convert_int(tc.x), convert_int(tc.y), heightMap);
	float h1 = Sample(convert_int((tc + off1).x), convert_int((tc + off1).y), heightMap);
	float h2 = Sample(convert_int((tc + off2).x), convert_int((tc + off2).y), heightMap);


	float3 pos1 = (float3)(tc.x, h0, tc.y);
	float3 pos2 = (float3)(tc.x + off1.x, h1, tc.y + off1.y);
	float3 pos3 = (float3)(tc.x + off2.x, h2, tc.y + off2.y);

	// Given the 3 vertices (position and texture coordinates) of a triangle
	// calculate and return the triangle's tangent vector.

	// Create 2 vectors in object space.
	//
	// edge1 is the vector from vertex positions pos1 to pos2.
	// edge2 is the vector from vertex positions pos1 to pos3.
	float3 edge1 = (float3)(pos2.x - pos1.x, pos2.y - pos1.y, pos2.z - pos1.z);
	float3 edge2 = (float3)(pos3.x - pos1.x, pos3.y - pos1.y, pos3.z - pos1.z);

	edge1 = normalize(edge1);
	edge2 = normalize(edge2);

	// Create 2 vectors in tangent (texture) space that point in the same
	// direction as edge1 and edge2 (in object space).
	//
	// texEdge1 is the vector from texture coordinates texCoord1 to texCoord2.
	// texEdge2 is the vector from texture coordinates texCoord1 to texCoord3.
	float2 texEdge1 = (float2)(texCoord2.x - texCoord1.x, texCoord2.y - texCoord1.y);
	float2 texEdge2 = (float2)(texCoord3.x - texCoord1.x, texCoord3.y - texCoord1.y);

	texEdge1 = normalize(texEdge1);
	texEdge2 = normalize(texEdge2);

	float3 t = (float3)(0, 0, 0);
	float3 b = (float3)(0, 0, 0);
	float3 n = (float3)(normal.x, normal.y, normal.z);

	float det = (texEdge1.x * texEdge2.y) - (texEdge1.y * texEdge2.x);

	if (CloseEnough(det, 0.0f) == true)
	{
		t = (float3)(1, 0, 0);
		b = (float3)(0, 1, 0);
	}
	else
	{
		det = 1.0f / det;

		t.x = (texEdge2.y * edge1.x - texEdge1.y * edge2.x) * det;
		t.y = (texEdge2.y * edge1.y - texEdge1.y * edge2.y) * det;
		t.z = (texEdge2.y * edge1.z - texEdge1.y * edge2.z) * det;

		b.x = (-texEdge2.x * edge1.x + texEdge1.x * edge2.x) * det;
		b.y = (-texEdge2.x * edge1.y + texEdge1.x * edge2.y) * det;
		b.z = (-texEdge2.x * edge1.z + texEdge1.x * edge2.z) * det;

		t = normalize(t);
		b = normalize(b);
	}

	// Calculate the handedness of the local tangent space.
	// The bitangent vector is the cross product between the triangle face
	// normal vector and the calculated tangent vector. The resulting bitangent
	// vector should be the same as the bitangent vector calculated from the
	// set of linear equations above. If they point in different directions
	// then we need to invert the cross product calculated bitangent vector. We
	// store this scalar multiplier in the tangent vector's 'w' component so
	// that the correct bitangent vector can be generated in the normal mapping
	// shader's vertex shader.

	float3 bitangent = cross(n, t);

	float handedness = (dot(bitangent, b) < 0.0f) ? -1.0f : 1.0f;
	
	return (float4)(t, handedness); 
}

kernel void PopulateTangentMap(
	global float* heightMap,
	global float4* normals,	
	write_only image2d_t tangents,
	int bufferLength)
{
	int i = get_global_id(0);

	if(i >= bufferLength)
		return;

	int x = i % IMAGE_WIDTH; 
	int y = i / IMAGE_WIDTH; 

	float hToNX = 1.0 / convert_float(IMAGE_WIDTH);
	float hToNY = 1.0 / convert_float(IMAGE_WIDTH);

	int index = 0;

	float4 tangent = CalcTangentVector((float2)(x, y),
						(float2)(convert_float(x * hToNX), convert_float(y * hToNY)),
						(float2)(convert_float((x + 1) * hToNX), convert_float(y * hToNY)),
						(float2)(convert_float(x * hToNX), convert_float((y + 1) * hToNY)),
						1, 1,
						//vload3(i * 3, normals), heightMap);
						normals[i], heightMap);

	//vstore4(tangent, i * 4, tangents);
 	
	int2 coord = (int2)(x, y);
	write_imagef(tangents, coord, tangent);
}

kernel void WriteNormalMap(
	global float4* normals,
	write_only image2d_t normalsMap,
	int bufferLength)
{
	int i = get_global_id(0);

	if(i >= bufferLength)
		return;

	int x = i % IMAGE_WIDTH; 
	int y = i / IMAGE_WIDTH; 

	//float3 normal = vload3(i * 3, normals);
	float4 normal = normals[i];

	//vstore3(normal, i * 3, normalsMap);

	int2 coord = (int2)(x, y);
	write_imagef(normalsMap, coord, (float4)(normal.xyz, 1.0));
}