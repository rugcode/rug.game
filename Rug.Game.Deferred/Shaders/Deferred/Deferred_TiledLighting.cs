﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.Deferred
{
	public enum Deferred_TiledLighting_Mode
	{
		Lighting = 0, LightCount = 1, DepthRange = 2
	}

	public class Deferred_TiledLighting : ComputeEffectBase
	{
		private Deferred_TiledLighting_Mode m_Mode;

		private int uOutTexture;
		private int uNormal;
		private int uDiffuse;
		private int uMaterial;
		private int uLights;
		private int uView;
		private int uProj;
		private int uViewProj;
		private int uInvViewProj;
		private int uInvProj;
		private int uFramebufferDim;
		private int uDepth;
		private int uWorldMatrixInv;
		private int uPerspectiveInvMatrix;
		private int uNumActiveLights;
		private int uCameraPosition;
		private int uCameraRangeZ;		

		public override string ShaderLocation { get { return "~/Shaders/Deferred/TiledLighting"; } }
		
		public override string Name
		{
			get { return "Deferred: Tiled Lighting Compute Shader"; }
		}

		public Deferred_TiledLighting(Deferred_TiledLighting_Mode mode)
		{
			m_Mode = mode;

			Defines = new string[] { mode.ToString() }; // Deferred_TiledLighting_Mode.Lighting.ToString(),
		}

		public void Render(View3D view, 
							int width, int height, uint tilesX, uint tilesY,
							Texture2D output, Texture2D normal, Texture2D material, Texture2D depthTexture,
							ShaderStorageBuffer lights, int numberOfLights)
		{
			Bind(); 

			GL.BindImageTexture(0, output.ResourceHandle, 0, false, 0, TextureAccess.WriteOnly, SizedInternalFormat.Rgba8); //  SizedInternalFormat.Rgba32f);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, normal);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, material);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, depthTexture);

			GL.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, 4, lights.ResourceHandle);

			GL.Uniform1(uNumActiveLights, 1, new int[] { numberOfLights });

			GL.UniformMatrix4(uView, false, ref view.View);
			GL.UniformMatrix4(uProj, false, ref view.Projection);

			Matrix4 viewProj = view.View * view.Projection;

			GL.UniformMatrix4(uViewProj, false, ref viewProj);

			Matrix4 invViewProj = Matrix4.Invert(viewProj);

			GL.UniformMatrix4(uInvViewProj, false, ref invViewProj);

			Matrix4 invProj = view.ProjectionInverse;

			GL.UniformMatrix4(uInvProj, false, ref invProj);
			GL.UniformMatrix4(uPerspectiveInvMatrix, false, ref view.ProjectionInverse);

			Vector2 framebufferDim = new Vector2(width, height);
			
			GL.Uniform2(uFramebufferDim, ref framebufferDim);

			Matrix4 worldMatrixInv = view.NormalWorld;
			worldMatrixInv.Invert();

			GL.UniformMatrix4(uWorldMatrixInv, false, ref worldMatrixInv);

			GL.Uniform3(uCameraPosition, ref view.Camera.Center);

			GL.Uniform2(uCameraRangeZ, ref view.RangeZ); 
			

			Dispatch(tilesX, tilesY, 1);

			Unbind(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			Bind();

			uOutTexture = GL.GetUniformLocation(ProgramHandle, "outTexture");
			uNormal = GL.GetUniformLocation(ProgramHandle, "normal");
			uDiffuse = GL.GetUniformLocation(ProgramHandle, "diffuse");
			uMaterial = GL.GetUniformLocation(ProgramHandle, "material");
			uDepth = GL.GetUniformLocation(ProgramHandle, "depthBuffer"); 
			uLights = GL.GetUniformLocation(ProgramHandle, "lights");
			uNumActiveLights = GL.GetUniformLocation(ProgramHandle, "numActiveLights");

			uView = GL.GetUniformLocation(ProgramHandle, "view");
			uProj = GL.GetUniformLocation(ProgramHandle, "proj");
			uViewProj = GL.GetUniformLocation(ProgramHandle, "viewProj");
			uInvViewProj = GL.GetUniformLocation(ProgramHandle, "invViewProj");
			uInvProj = GL.GetUniformLocation(ProgramHandle, "invProj");
			uWorldMatrixInv = GL.GetUniformLocation(ProgramHandle, "worldMatrixInv"); 
			uFramebufferDim = GL.GetUniformLocation(ProgramHandle, "framebufferDim");
			uPerspectiveInvMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveInvMatrix");
			uCameraPosition = GL.GetUniformLocation(ProgramHandle, "cameraPosition");
			uCameraRangeZ = GL.GetUniformLocation(ProgramHandle, "cameraRangeZ"); 

			GL.Uniform1(uOutTexture, 0);
			GL.Uniform1(uNormal, 1);
			GL.Uniform1(uMaterial, 2);
			GL.Uniform1(uDepth, 3);
			GL.Uniform1(uLights, 4);			
			
			Unbind(); 
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}


	}
}
