﻿#version 410

uniform mat4 depthMVP;
uniform sampler2D heights;

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;
layout(location = 2) in vec3 barycentric;
    
void main()
{
	float y = texture2D(heights, texcoord).x;

    gl_Position = depthMVP * vec4(position.x, (y * 160.0) - 20.0, -position.y, 1.0);
}
