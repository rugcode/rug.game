﻿#version 430 core

#define MAX_LIGHTS 1024
#define MAX_LIGHTS_PER_TILE 40

#define WORK_GROUP_SIZE 16

#define numActiveLights 64

struct PointLight
{
    vec3 position;
    float radius;
    vec3 color;
    float intensity;
};

layout (binding = 0, rgba32f) uniform writeonly image2D outTexture;
//layout (binding = 1, rgba16f) uniform readonly image2D normal;
layout (binding = 1) uniform sampler2D normal;
layout (binding = 2, rgba32f) uniform readonly image2D material;
layout (binding = 3) uniform sampler2D depthBuffer;

layout (std430, binding = 4) buffer BufferObject
{
    PointLight pointLights[];
};

uniform mat4 view;
uniform mat4 proj;
uniform mat4 viewProj;
uniform mat4 invViewProj;
uniform mat4 invProj;
uniform mat4 worldMatrixInv; 
uniform mat4 perspectiveInvMatrix;

uniform vec2 framebufferDim;
//uniform int numActiveLights;

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE) in;

shared uint minDepth = 0x7F7FFFFF;
shared uint maxDepth = 0;
shared uint pointLightIndex[MAX_LIGHTS];
shared uint pointLightCount = 0;

vec3 ReconstructWP(float z, vec2 uv_f)
{
    vec4 sPos = vec4(uv_f * 2.0 - 1.0, z, 1.0);
    //sPos = perspectiveInvMatrix * sPos;
	sPos = invViewProj * sPos;
	//sPos = invProj * sPos;

    return (sPos.xyz / sPos.w);
}

vec4 ConvertProjToView( vec4 p )
{
    p = invProj * p;
    p /= p.w;
    return p;
}

// calculate the number of tiles in the horizontal direction
uint GetNumTilesX()
{
    return uint(( ( framebufferDim.x + WORK_GROUP_SIZE - 1 ) / float(WORK_GROUP_SIZE) ));
}

// calculate the number of tiles in the vertical direction
uint GetNumTilesY()
{
    return uint(( ( framebufferDim.y + WORK_GROUP_SIZE - 1 ) / float(WORK_GROUP_SIZE) ));
}


vec4 CreatePlaneEquation( vec4 b, vec4 c )
{
    vec4 n;

    // normalize(cross( b.xyz-a.xyz, c.xyz-a.xyz )), except we know "a" is the origin
     n.xyz = normalize(cross( b.xyz, c.xyz ));

    // -(n dot a), except we know "a" is the origin
    n.w = 0;

    return n;
}

float GetSignedDistanceFromPlane( vec4 p, vec4 eqn )
{
    // dot( eqn.xyz, p.xyz ) + eqn.w, , except we know eqn.w is zero 
    // (see CreatePlaneEquation above)
    return dot( eqn.xyz, p.xyz );
}

vec4 CalculateLighting(PointLight p, vec3 wPos, vec3 wNormal) // , vec4 wSpec, vec4 wGlow)
{
    vec3 direction = p.position - wPos;

    if(length(direction) > p.radius)
	{
        return vec4(0.0f, 0.0f, 0.0f, 0.0f);
	}

    float attenuation = 1.0f - length(direction) / (p.radius);

    direction = normalize(direction);

	float dir = dot(direction, wNormal);

	//if (dir < 0)
	//{
	//	return vec4(1.0, 0.0, 0.0, 0.0);
	//}
	//else 
	//{
		float diffuseFactor = max(0.0f, dir) * attenuation;

		return vec4(p.color * diffuseFactor, 0.0); //  * p.intensity;
	//}
}

vec4 DirectIllumination(PointLight p, vec3 wPos, vec3 wNormal)
{
	vec3 lightColour = p.color; 
	vec3 lightCentre = p.position; 
	float lightRadius = 20; //p.radius * 0.001; 
	float cutoff = 0.001; // p.radius * 0.01; // * lightRadius; 

    // calculate normalized light vector and distance to sphere light surface
    float r = lightRadius;
    vec3 L = lightCentre - wPos;
    float dist = length(L);
    float d = max(dist - r, 0);
    L /= dist;
     
    // calculate basic attenuation
    float denom = d/r + 1;
    float attenuation = 1 / (denom*denom);
     
    // scale and bias attenuation such that:
    //   attenuation == 0 at extent of max influence
    //   attenuation == 1 when d == 0
    attenuation = (attenuation - cutoff) / (1 - cutoff);
    attenuation = max(attenuation, 0);
     
    float dotValue = max(dot(L, wNormal), 0);

    return vec4(lightColour * dotValue * attenuation, 0.0);
}


vec4 DirectIlluminationWrong(PointLight p, vec3 wPos, vec3 wNormal)
{
	vec3 lightColour = p.color; 
	//vec3 lightCentre = (view * vec4(p.position, 1)).xyz; 
	vec3 lightCentre = p.position; 
	float lightRadius = p.radius; //p.radius * 0.001; 
	float cutoff = 0.001; // p.radius * 0.01; // * lightRadius; 

    // calculate normalized light vector and distance to sphere light surface
    float r = lightRadius;
    vec3 L = lightCentre - wPos; // (viewProj * vec4(wPos, 1)).xyz;
	//vec3 L = lightCentre - (view * vec4(wPos, 1)).xyz;
    float dist = length(L);
	//L /= dist; 
	L = normalize(L); 

	float intensity = (max(lightRadius - dist, 0) / lightRadius);

	//float lambert = clamp(abs(dot(normalize(L), -wNormal)), 0.0, 1.0);	

	float dotValue = clamp(dot(wNormal, L), 0.0, 1.0);
	//float dotValue = clamp(dot((view * vec4(wNormal, 0)).xyz, L), 0.0, 1.0);

	/* 	
	float dotValue = dot(-wNormal, L); // , 0.0, 1.0);

	if (dotValue < 0) 
	{ 
		//return vec4(lightColour * dotValue * intensity, 0.0);
		lightColour = vec3(1, 0, 0); 
	} 
	else if (dotValue == 0)  
	{
		lightColour = vec3(0, 1, 0); 
	}
	else 
	{
		lightColour = vec3(1, 1, 1); 
	}
	*/ 

	return vec4(lightColour * intensity * dotValue, 0.0);

	//return vec4((L * 0.5 + 0.5), 0); 

	//return vec4(wNormal * 0.5 + 0.5, 0); 
	
}

/* 

// As below, we separate this for diffuse/specular parts for convenience in deferred lighting
void AccumulatePhongBRDF(float3 normal,
                         float3 lightDir,
                         float3 viewDir,
                         float3 lightContrib,
                         float specularPower,
                         inout float3 litDiffuse,
                         inout float3 litSpecular)
{
    // Simple Phong
    float NdotL = dot(normal, lightDir);
    [flatten] if (NdotL > 0.0f) {
        float3 r = reflect(lightDir, normal);
        float RdotV = max(0.0f, dot(r, viewDir));
        float specular = pow(RdotV, specularPower);

        litDiffuse += lightContrib * NdotL;
        litSpecular += lightContrib * specular;
    }
}

// Accumulates separate "diffuse" and "specular" components of lighting from a given
// This is not possible for all BRDFs but it works for our simple Phong example here
// and this separation is convenient for deferred lighting.
// Uses an in-out for accumulation to avoid returning and accumulating 0
void AccumulateBRDFDiffuseSpecular(SurfaceData surface, PointLight light,
                                   inout float3 litDiffuse,
                                   inout float3 litSpecular)
{
    float3 directionToLight = light.positionView - surface.positionView;
    float distanceToLight = length(directionToLight);

    [branch] if (distanceToLight < light.attenuationEnd) {
        float attenuation = linstep(light.attenuationEnd, light.attenuationBegin, distanceToLight);
        directionToLight *= rcp(distanceToLight);       // A full normalize/RSQRT might be as fast here anyways...
        
        AccumulatePhongBRDF(surface.normal, directionToLight, normalize(surface.positionView),
            attenuation * light.color, surface.specularPower, litDiffuse, litSpecular);
    }
}

// Uses an in-out for accumulation to avoid returning and accumulating 0
void AccumulateBRDF(SurfaceData surface, PointLight light,
                    inout float3 lit)
{
    float3 directionToLight = light.positionView - surface.positionView;
    float distanceToLight = length(directionToLight);

    [branch] if (distanceToLight < light.attenuationEnd) {
        float attenuation = linstep(light.attenuationEnd, light.attenuationBegin, distanceToLight);
        directionToLight *= rcp(distanceToLight);       // A full normalize/RSQRT might be as fast here anyways...

        float3 litDiffuse = float3(0.0f, 0.0f, 0.0f);
        float3 litSpecular = float3(0.0f, 0.0f, 0.0f);
        AccumulatePhongBRDF(surface.normal, directionToLight, normalize(surface.positionView),
            attenuation * light.color, surface.specularPower, litDiffuse, litSpecular);
        
        lit += surface.albedo.rgb * (litDiffuse + surface.specularAmount * litSpecular);
    }
}

*/ 

float DepthToZPosition(float depth, float near, float far) 
{ 
	float z_n = 2.0 * depth - 1.0;
    float z_e = 2.0 * near * far / (far + near - z_n * (far - near));

	return z_e; 
	//return near / (far - depth * (far - near)) * far; 
}

void main()
{
    ivec2 pixelPos = ivec2(gl_GlobalInvocationID.xy);
    vec2 tilePos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy) / framebufferDim;

	ivec2 position = ivec2(gl_GlobalInvocationID.xy);
	vec2 screenNormalized = vec2(position) / vec2(framebufferDim); // framebufferDim is the size of the depth and color textures

    //vec3 normalColor = normalize(imageLoad(normal, pixelPos).xyz * 2.0 - 1.0); 
	vec3 normalColor = normalize(texture2D(normal, screenNormalized).xyz * 2.0 - 1.0); 
	vec4 materialColor = imageLoad(material, pixelPos);

    //float d = materialColor.w;

	float d = texture2D(depthBuffer, screenNormalized).x;

	//float d = DepthToZPosition(uintBitsToFloat(d), 0.1, 30000.0);

    //uint depth = floatBitsToUint(DepthToZPosition(d, 0.1, 30000.0));
	uint depth = floatBitsToUint(d);

	if (d > 0 && d < 1) 
	{
		atomicMin(minDepth, depth);
		atomicMax(maxDepth, depth);
	}

    barrier();

	//float minDepthZ = float(minDepth / float(0xFFFFFFFF)) * 2.0 - 1.0; // DepthToZPosition(float(minDepth / float(0xFFFFFFFF)), 0.1, 30000.0);
	//float maxDepthZ = float(maxDepth / float(0xFFFFFFFF)) * 2.0 - 1.0; // DepthToZPosition(float(maxDepth / float(0xFFFFFFFF)), 0.1, 30000.0);

	float minDepthZ = DepthToZPosition(uintBitsToFloat(minDepth), 0.1, 30000.0);
	float maxDepthZ = DepthToZPosition(uintBitsToFloat(maxDepth), 0.1, 30000.0);

	//float minDepthZ = uintBitsToFloat(minDepth) * 2.0 - 1.0;
	//float maxDepthZ = uintBitsToFloat(maxDepth) * 2.0 - 1.0;

	//total tiles = tileScale * 2
	vec2 tileScale = framebufferDim * (1.0f / float(2 * WORK_GROUP_SIZE));
	vec2 tileBias = tileScale - vec2(gl_WorkGroupID.xy);

	/*
	vec4 c1 = vec4(-proj[0][0] * tileScale.x, 0.0f, tileBias.x, 0.0f);
	vec4 c2 = vec4(0.0f, -proj[1][1] * tileScale.y, tileBias.y, 0.0f);
	vec4 c4 = vec4(0.0f, 0.0f, 1.0f, 0.0f);

	// Derive frustum planes
	vec4 frustumPlanes[6];
	// Sides
	//right
	frustumPlanes[0] = c4 - c1;
	//left
	frustumPlanes[1] = c4 + c1;
	//bottom
	frustumPlanes[2] = c4 - c2;
	//top
	frustumPlanes[3] = c4 + c2;
	// Near/far
	//frustumPlanes[4] = vec4(0.0f, 0.0f, -1.0f, minDepthZ);
	//frustumPlanes[5] = vec4(0.0f, 0.0f, -1.0f, maxDepthZ);

	frustumPlanes[4] = vec4(0.0f, 0.0f, -1.0f, -minDepthZ);
	frustumPlanes[5] = vec4(0.0f, 0.0f, -1.0f,  maxDepthZ);

	for(int i = 0; i < 4; i++)
	{
		frustumPlanes[i] *= 1.0f / length(frustumPlanes[i].xyz);
	}

	//DO CULLING HERE
	for (uint lightIndex = gl_LocalInvocationIndex; lightIndex < numActiveLights; lightIndex += WORK_GROUP_SIZE)
	{
		PointLight p = pointLights[lightIndex];

		if (lightIndex < numActiveLights)
		{
			bool inFrustum = true;
			for (uint i = 0; i < 6; i++)
			{
				float dd = dot(frustumPlanes[i], view * vec4(p.position, 1.0f));

				inFrustum = inFrustum && (dd >= -p.radius); // radius_length
			}

			if (inFrustum)
			{
				uint id = atomicAdd(pointLightCount, 1);

				pointLightIndex[id] = lightIndex;
			}
		}
	}
	*/ 


	//vec2 tileScale = vec2(1280, 720) * (1.0f / float( 2 * WORK_GROUP_SIZE));
    //vec2 tileBias = tileScale - vec2(gl_WorkGroupID.xy);

    vec4 col1 = vec4(-proj[0][0]  * tileScale.x, proj[0][1], tileBias.x, proj[0][3]); 

    vec4 col2 = vec4(proj[1][0], -proj[1][1] * tileScale.y, tileBias.y, proj[1][3]);

    vec4 col4 = vec4(proj[3][0], proj[3][1], -1.0f, proj[3][3]); 

    vec4 frustumPlanes[6];

    //Left plane
    frustumPlanes[0] = col4 + col1;

    //right plane
    frustumPlanes[1] = col4 - col1;

    //top plane
    frustumPlanes[2] = col4 - col2;

    //bottom plane
    frustumPlanes[3] = col4 + col2;

    //near
    //frustumPlanes[4] = vec4(0.0, 0.0, -1.0, -minDepthZ);

    //far
    //frustumPlanes[5] = vec4(0.0, 0.0, -1.0, maxDepthZ);

	frustumPlanes[4] = vec4(0.0, 0.0, -1.0, -minDepthZ);
    frustumPlanes[5] = vec4(0.0, 0.0, -1.0,  maxDepthZ);

    for(int i = 0; i < 4; i++)
    {
        frustumPlanes[i] *= 1.0 / length(frustumPlanes[i].xyz);
    }

	//DO CULLING HERE
	for (uint lightIndex = gl_LocalInvocationIndex; lightIndex < numActiveLights; lightIndex += WORK_GROUP_SIZE)
	{
		PointLight p = pointLights[lightIndex];

		if (lightIndex < numActiveLights)
		{
			bool inFrustum = true;
			for (uint i = 0; i < 6; i++)
			{
				float dd = dot(frustumPlanes[i], view * vec4(p.position, 1.0f));

				inFrustum = inFrustum && (dd >= -p.radius); // radius_length
			}

			if (inFrustum)
			{
				uint id = atomicAdd(pointLightCount, 1);

				pointLightIndex[id] = lightIndex;
			}
		}
	}

	barrier();

	/*
    int threadsPerTile = WORK_GROUP_SIZE * WORK_GROUP_SIZE;

    for (uint i = 0; i < MAX_LIGHTS; i+= threadsPerTile)
    {
        uint il = gl_LocalInvocationIndex + i;

        if (il < MAX_LIGHTS)
        {
            PointLight p = pointLights[il];

            vec4 viewPos = view * vec4(p.position, 1.0f);
            float r = p.radius;

            if (viewPos.z + minDepthZ < r && viewPos.z - maxDepthZ < r)
            {

            if( ( GetSignedDistanceFromPlane( viewPos, frustumEqn[0] ) < r ) &&
                ( GetSignedDistanceFromPlane( viewPos, frustumEqn[1] ) < r ) &&
                ( GetSignedDistanceFromPlane( viewPos, frustumEqn[2] ) < r ) &&
                ( GetSignedDistanceFromPlane( viewPos, frustumEqn[3] ) < r) )

                {
                    uint id = atomicAdd(pointLightCount, 1);
                    pointLightIndex[id] = il;
                }
            }

        }
    }

    barrier();
	*/ 
	vec4 color = vec4(0.0); 

#section LightCount
	float lightCount = clamp(float(pointLightCount), 0.0, 32.0) / 32.0;
	
	color += vec4(lightCount, lightCount, lightCount, 1.0);	
#end

#section DepthRange
	float depthRange =  clamp((maxDepthZ - minDepthZ) * 1000.0, 0.0, 1.0); 	
	
	color += vec4(depthRange, depthRange, depthRange, 1.0);	
#end
	//if (minDepthZ > 0)  
	//{
	//	color = vec4(1.0, 0.0, 0.0, 1.0);
	//}

	//vec4 color = vec4(minDepthZ * 100.0, minDepthZ * 100.0, minDepthZ * 100.0, 1.0);
	//vec4 color = vec4(maxDepthZ * 100.0, maxDepthZ * 100.0, maxDepthZ * 100.0, 1.0);

	//if (gl_LocalInvocationID.x == 0 || gl_LocalInvocationID.y == 0 || gl_LocalInvocationID.x == 16 || gl_LocalInvocationID.y == 16)
	//{
    //    imageStore(outTexture, pixelPos, vec4(0.2, 0.2, 0.2, 1.0));
	//}
    //else
    //{
        //imageStore(outTexture, pixelPos, color);
        //imageStore(outTexture, pixelPos, vec4(maxDepthZ));
        //imageStore(outTexture, pixelPos, vec4(pointLightCount / 128.0f));
        //imageStore(outTexture, pixelPos, vec4(vec2(tilePos.xy), 0.0f, 1.0f));
    //}	

   // vec4 diffuseColor = imageLoad(diffuse, pixelPos);
    //vec4 specularColor = imageLoad(specular, pixelPos);
    //vec4 glowColor = imageLoad(glowMatID, pixelPos);

    //vec2 uv = vec2(pixelPos.x / 1280.0f, pixelPos.y / 720.0f);
	
#section Lighting

	d  = d * 2.0 - 1.0;
    vec3 wp = ReconstructWP(d, screenNormalized);
    //vec4 color = vec4(0.0, 0.0, 0.0, 1.0);

	vec3 worldNormal = normalize((worldMatrixInv * vec4(normalColor, 1.0)).xyz);

    for (int i = 0; i < pointLightCount; i++)
    {	
		//color += CalculateLighting(pointLights[pointLightIndex[i]], wp, normalColor); // , specularColor, glowColor);
		//color += CalculateLighting(pointLights[pointLightIndex[i]], wp, (invViewProj * vec4(normalColor, 0)).xyz); // , specularColor, glowColor);
		//color += CalculateLighting(pointLights[pointLightIndex[i]], wp, (invViewProj * vec4(normalColor, 0)).xyz); // , specularColor, glowColor);
        //color += DirectIllumination(pointLights[pointLightIndex[i]], wp, (worldMatrixInv * vec4(normalColor, 0)).xyz); // , specularColor, glowColor);
		//color += DirectIllumination(pointLights[pointLightIndex[i]], wp, normalColor); // , specularColor, glowColor);
		//color += DirectIlluminationWrong(pointLights[pointLightIndex[i]], wp, (perspectiveInvMatrix * vec4(normalColor, 1.0)).xyz); // , specularColor, glowColor);
		color += DirectIlluminationWrong(pointLights[pointLightIndex[i]], wp, worldNormal); // , specularColor, glowColor);
		//color += DirectIlluminationWrong(pointLights[pointLightIndex[i]], wp, normalColor); // , specularColor, glowColor);
		//color += DirectIlluminationWrong(pointLights[pointLightIndex[i]], wp, normalColor); // , specularColor, glowColor);
    }

    //barrier();

    //if (gl_LocalInvocationID.x == 0 || gl_LocalInvocationID.y == 0 || gl_LocalInvocationID.x == 16 || gl_LocalInvocationID.y == 16)
	//{
        //imageStore(outTexture, pixelPos, vec4(0.2, 0.2, 0.2, 1.0));
	//}
    //else
    //{
	//color.x = lightCount; 
  
        //imageStore(outTexture, pixelPos, vec4(maxDepthZ));
        //imageStore(outTexture, pixelPos, vec4(lightCount, lightCount, lightCount, 1.0));
        //imageStore(outTexture, pixelPos, vec4(vec2(tilePos.xy), 0.0f, 1.0f));
    //}
#end

	//color = vec4(normalColor * 0.5 + 0.5, 1.0); 

	imageStore(outTexture, pixelPos, color);
}