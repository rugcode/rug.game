﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;

namespace Rug.Game.Shaders.Deferred
{	
	public class Deferred_Color : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/Color";
		//private string m_ShaderLocation = @"~/Data/Shaders/Deferred/DiffuseTextured";
		private int uObjectMatrix;
		private int uNormalMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uNormalWorldMatrix;

		#endregion

		public override string Name
		{
			get { return "Deferred: Color"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Color() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorldMatrix);
		}

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, int indexCount, DrawElementsType indexType)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Position, "position"); 

			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Normal, "normal");
			
			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.TextureCoords, "uv"); 

			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Color, "color");
			*/ 
			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");
			
			GL.BindFragDataLocation(ProgramHandle, 1, "normalFrag"); 
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
