﻿#version 430 core

#define EPSILON 1e-6
#define MAX_LIGHTS 1024
#define MAX_LIGHTS_PER_TILE 64

#define WORK_GROUP_SIZE 16

struct PointLight
{
    vec3 position;
    float radius;
    vec3 color;
    float specAmount;
};

//layout (binding = 0, rgba32f) uniform writeonly image2D outTexture;
layout (binding = 0, rgba8) uniform writeonly image2D outTexture;
//layout (binding = 1, rgba16f) uniform readonly image2D normal;
layout (binding = 1) uniform sampler2D normal;
//layout (binding = 2, rgba32f) uniform readonly image2D material;
layout (binding = 2) uniform sampler2D material;
layout (binding = 3) uniform sampler2D depthBuffer;

layout (std430, binding = 4) buffer BufferObject
{
    PointLight pointLights[];
};

uniform mat4 view;
uniform mat4 proj;
uniform mat4 viewProj;
uniform mat4 invViewProj;
uniform mat4 invProj;
uniform mat4 worldMatrixInv; 
uniform mat4 perspectiveInvMatrix;
uniform vec3 cameraPosition; 
uniform vec2 cameraRangeZ; 

uniform vec2 framebufferDim;
uniform int numActiveLights;

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = WORK_GROUP_SIZE) in;

shared uint minDepth = 0x7F7FFFFF;
shared uint maxDepth = 0;
shared uint pointLightIndex[MAX_LIGHTS_PER_TILE];
shared uint pointLightCount = 0;

vec3 ReconstructWP(float z, vec2 uv_f)
{
    vec4 sPos = vec4(uv_f * 2.0 - 1.0, z, 1.0);
	
	sPos = invViewProj * sPos;

    return (sPos.xyz / sPos.w);
}


vec4 DirectIllumination_Diffuse(PointLight p, vec3 wPos, vec3 wNormal, vec3 viewDir, float ambiantAmmount)
{	  
	vec3 Dir = p.position - wPos; 
	float Dist = length(Dir);
	vec3 L = Dir / Dist; 
    vec3 E = viewDir;
    vec3 R = normalize(-reflect(L, wNormal)); 

	float radius = p.radius * 0.8; 

	// calculate basic attenuation
    float denom = Dist / radius + 1;
    float attenuationRaw = 1 / (denom*denom);

	float cutoff = 0.225;

    float attenuation = (attenuationRaw - cutoff) / (1 - cutoff);
    attenuation = max(attenuation, 0);

	vec3 lightContrib = p.color * attenuation; 

    //calculate Diffuse Term: 
    vec3 Idiff = lightContrib * ambiantAmmount * max(dot(wNormal,L), 0.0);
    Idiff = clamp(Idiff, 0.0, 1.0); 
 
    return vec4(Idiff, 0);
}

vec4 DirectIllumination_DiffuseSpec(PointLight p, vec3 wPos, vec3 wNormal, vec3 viewDir, float specAmmount, float specFunc, float ambiantAmmount)
{
	  
	vec3 Dir = p.position - wPos; 
	float Dist = length(Dir);
	vec3 L = Dir / Dist; 
    vec3 E = viewDir;
    vec3 R = normalize(-reflect(L, wNormal)); 

	float radius = p.radius * 0.8; 

	// calculate basic attenuation
    float denom = Dist / radius + 1;
    float attenuationRaw = 1 / (denom*denom);
     
    // scale and bias attenuation such that:
    //   attenuation == 0 at extent of max influence
    //   attenuation == 1 when d == 0
	//float cutoff = 0.25;
		
	float cutoff = 0.225;

	float attenuation = (attenuationRaw - cutoff) / (1 - cutoff);
	attenuation = max(attenuation, 0);

	vec3 lightContrib = p.color * attenuation; 

	//calculate Diffuse Term: 
	vec3 Idiff = lightContrib * ambiantAmmount * max(dot(wNormal,L), 0.0);
	Idiff = clamp(Idiff, 0.0, 1.0); 
    
	float Ispec = 0;

	if (p.specAmount > EPSILON) 
	{ 
		//cutoff = 0.2;
		cutoff = 0.2;

		// scale and bias attenuation such that:
		//   attenuation == 0 at extent of max influence
		//   attenuation == 1 when d == 0
		attenuation = (attenuationRaw - cutoff) / (1 - cutoff);
		attenuation = max(attenuation, 0);

		//vec3 Ispec = lightContrib * pow(max(dot(R, E), 0.0), specFunc) * specAmmount * attenuation;
		Ispec = pow(max(dot(R, E), 0.0), specFunc) * specAmmount * p.specAmount * attenuation;
		Ispec = clamp(Ispec, 0.0, 1.0); 
	}

    return vec4(Idiff, Ispec);
}

/*
vec3 projectOnPlane(in vec3 p, in vec3 pc, in vec3 pn)
{
    float distance = dot(pn, p-pc);
    return p - distance*pn;
}

int sideOfPlane(in vec3 p, in vec3 pc, in vec3 pn){
   if (dot(p-pc,pn)>=0.0) return 1; else return 0;
}

vec3 linePlaneIntersect(in vec3 lp, in vec3 lv, in vec3 pc, in vec3 pn){
   return lp+lv*(dot(pn,pc-lp)/dot(pn,lv));
}

void areaLight(in int i, in vec3 N, in vec3 V, in float shininess,
                inout vec4 ambient, inout vec4 diffuse, inout vec4 specular)
{
    vec3 right = normalize(vec3(gl_ModelViewMatrix*gl_LightSource[i].ambient));
    vec3 pnormal = normalize(gl_LightSource[i].spotDirection);
    vec3 up = normalize(cross(right,pnormal));

    //width and height of the area light:
    float width = 1.0; 
    float height = 4.0;

    //project onto plane and calculate direction from center to the projection.
    vec3 projection = projectOnPlane(V,vec3(gl_LightSource[i].position.xyz),pnormal);// projection in plane
    vec3 dir = projection-vec3(gl_LightSource[i].position.xyz);

    //calculate distance from area:
    vec2 diagonal = vec2(dot(dir,right),dot(dir,up));
    vec2 nearest2D = vec2(clamp( diagonal.x,-width,width  ),clamp(  diagonal.y,-height,height));
    vec3 nearestPointInside = vec3(gl_LightSource[i].position.xyz)+(right*nearest2D.x+up*nearest2D.y);

    float dist = distance(V,nearestPointInside);//real distance to area rectangle

    vec3 L = normalize(nearestPointInside - V);
    float attenuation = calculateAttenuation(i, dist);

    float nDotL = dot(pnormal,-L);

    if (nDotL > 0.0 && sideOfPlane(V,vec3(gl_LightSource[i].position.xyz),pnormal) == 1) //looking at the plane
    {   
        //shoot a ray to calculate specular:
        vec3 R = reflect(normalize(-V), N);
        vec3 E = linePlaneIntersect(V,R,vec3(gl_LightSource[i].position.xyz),pnormal);

        float specAngle = dot(R,pnormal);
        if (specAngle > 0.0){
	    vec3 dirSpec = E-vec3(gl_LightSource[i].position.xyz);
    	    vec2 dirSpec2D = vec2(dot(dirSpec,right),dot(dirSpec,up));
          vec2 nearestSpec2D = vec2(clamp( dirSpec2D.x,-width,width  ),clamp(  dirSpec2D.y,-height,height));
    	    float specFactor = 1.0-clamp(length(nearestSpec2D-dirSpec2D)*shininess,0.0,1.0);
          specular += gl_LightSource[i].specular * attenuation * specFactor * specAngle;   
        }
        diffuse  += gl_LightSource[i].diffuse  * attenuation * nDotL;  
    }
   
    ambient  += gl_LightSource[i].ambient * attenuation;
}
*/ 

float DepthToZPosition(float depth, float near, float far) 
{ 
    return 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));	
}

void main()
{
    ivec2 pixelPos = ivec2(gl_GlobalInvocationID.xy);
    vec2 tilePos = vec2(gl_WorkGroupID.xy * gl_WorkGroupSize.xy) / framebufferDim;

	ivec2 position = ivec2(gl_GlobalInvocationID.xy);
	vec2 screenNormalized = vec2(position) / vec2(framebufferDim); 

	vec3 normalColor = normalize(texture2D(normal, screenNormalized).xyz * 2.0 - 1.0); 
	vec4 materialColor = texture2D(material, screenNormalized); 

	float depthScreen = texture2D(depthBuffer, screenNormalized).x;

	// take the raw screen depth and map it into linear space then bit cast to unit
    uint depth = floatBitsToUint(DepthToZPosition(depthScreen, cameraRangeZ.x, cameraRangeZ.y));

	// if the depth value is in a usefull range then get the atomic min/max for the tile
	if (depthScreen > EPSILON && depthScreen < 1.0 - EPSILON) 
	{
		// get the atomic min/max for the tile
		atomicMin(minDepth, depth);
		atomicMax(maxDepth, depth);
	}

	// wait for all threads
    barrier();
	memoryBarrierShared(); 

	// recast into float
	float minDepthZ = uintBitsToFloat(minDepth);
	float maxDepthZ = uintBitsToFloat(maxDepth);

	// total tiles = tileScale * 2
	vec2 tileScale = framebufferDim * (1.0f / float(2 * WORK_GROUP_SIZE));
	vec2 tileBias = tileScale - vec2(gl_WorkGroupID.xy);

    vec4 col1 = vec4(-proj[0][0] * tileScale.x, proj[0][1], tileBias.x, proj[0][3]); 

    vec4 col2 = vec4(proj[1][0], -proj[1][1] * tileScale.y, tileBias.y, proj[1][3]);

    vec4 col4 = vec4(proj[3][0], proj[3][1], -1.0f, proj[3][3]); 

    vec4 frustumPlanes[6];

    //Left plane
    frustumPlanes[0] = col4 + col1;

    //right plane
    frustumPlanes[1] = col4 - col1;

    //top plane
    frustumPlanes[2] = col4 - col2;

    //bottom plane
    frustumPlanes[3] = col4 + col2;

	//near
	frustumPlanes[4] = vec4(0.0, 0.0, -1.0, -minDepthZ);

	//far
	frustumPlanes[5] = vec4(0.0, 0.0, 1.0,  maxDepthZ); 

    for(int i = 0; i < 4; i++)
    {
        frustumPlanes[i] *= 1.0 / length(frustumPlanes[i].xyz);
    }

	// cull all the lights that do not affect this tile
	for (uint lightIndex = gl_LocalInvocationIndex; lightIndex < numActiveLights; lightIndex += WORK_GROUP_SIZE)
	{
		PointLight p = pointLights[lightIndex];

		// transform the light position by the view matrix
		vec4 lightPosition = view * vec4(p.position, 1.0f); 

		bool inFrustum = true;
		for (uint i = 0; i < 6; i++)
		{
			// get the distance from the plane				
			float dd = dot(frustumPlanes[i], lightPosition);

			// is it within the plane
			inFrustum = inFrustum && (dd >= -p.radius);
		}

		// is the light within all the planes
		if (inFrustum)
		{							
			// add the light to the light list for this tile
			uint id = atomicAdd(pointLightCount, 1);

			if (id < MAX_LIGHTS_PER_TILE) 
			{
				pointLightIndex[id] = lightIndex;
			}
		}
	}

	// wait for all threads to finish
	barrier();
	memoryBarrierShared(); 

	uint tilePointLights = clamp(pointLightCount, 0, MAX_LIGHTS_PER_TILE - 1); 
	
	// we start with a empty color 
	vec4 color = vec4(0.0); 

#section LightCount
	float maxResonableLightCount = clamp(float(numActiveLights), 1.0, float(MAX_LIGHTS_PER_TILE)); 
	float lightCount = clamp(float(tilePointLights), 0.0, maxResonableLightCount) / maxResonableLightCount;
	
	color += vec4(lightCount, lightCount, lightCount, 0);	
#end

#section DepthRange
	float depthRange =  clamp((maxDepthZ - minDepthZ) * 1000.0, 0.0, 1.0); 	
	
	color += vec4(depthRange, depthRange, depthRange, 0);	
#end
	
#section Lighting

	// get the screen point in camera space	
    vec3 worldPoint = ReconstructWP(depthScreen * 2.0 - 1.0, screenNormalized);    

	// get the normal direction in camera space	
	vec3 worldNormal = normalize((worldMatrixInv * vec4(normalColor, 1.0)).xyz);

	// get the direction of the ray from this pixel
	vec3 viewDir = normalize(ReconstructWP(0, screenNormalized) - worldPoint); 

	// unpack the material properties
	float specAmmount = materialColor.r;  
	float specFunc = (materialColor.g * 16.0) * (materialColor.g * 16.0);    
	float ambiantAmmount = materialColor.b;  

	// for all the lights that affect this tile
    for (int i = 0; i < tilePointLights; i++)
    {	
		color += DirectIllumination_DiffuseSpec(pointLights[pointLightIndex[i]], worldPoint, worldNormal, viewDir, specAmmount, specFunc, ambiantAmmount);
		//color += DirectIllumination_Diffuse(pointLights[pointLightIndex[i]], worldPoint, worldNormal, viewDir, ambiantAmmount);
    }

#end
	
	// store the result
	imageStore(outTexture, pixelPos, color);
}