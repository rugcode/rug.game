#version 410

uniform mat4 objectMatrix;
uniform mat4 normalMatrix;
uniform mat4 worldMatrix;
uniform mat4 normalWorldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 3) in vec2 uv;
layout(location = 4) in vec3 color;

out vec3 normal_var;
out vec3 color_var;

void main()
{
    vec4 localSpace = objectMatrix * vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;  
  
    vec4 normalLok = normalMatrix * vec4(normal, 1);    
	normal_var = (normalWorldMatrix * normalLok).xyz; 
    color_var = color;
}

/* 
in vec3 position;
in vec3 normal;
in vec2 uv;
in vec3 color;

uniform mat4 objectMatrix;
uniform mat4 normalMatrix;
uniform mat4 worldMatrix;
uniform mat4 normalWorldMatrix;
uniform mat4 perspectiveMatrix;

out VertexData 
{
	vec3 normal_var;
	vec3 color_var; 
} outVert;

//varying vec3 normal_var;
//varying vec3 color_var;

void main()
{
    vec4 localSpace = objectMatrix * vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;  // vec4(position, 1); // 
  
    vec4 normalLok = normalMatrix * vec4(normal, 1);
    //normal_var = normal.xyz; // (normalWorldMatrix* normalLok ).xyz; // normalWorldMatrix*
	outVert.normal_var = (normalWorldMatrix * normalLok).xyz; // normalWorldMatrix*
    outVert.color_var = color;
}
*/ 