﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
 

namespace Rug.Game.Shaders.Deferred
{
	public class Deferred_Shadow_Final : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/Shadow_Final";

		private int uNormalTexture;
		private int uDepthTexture;
		private int uPerspectiveInvMatrix;
		private int uAONoiseTexture;
		private int uShadowTexture1;
		private int uLight1Matrix;
		private int uShadowTextureSize1;
		private int uShadowTexture2;
		private int uShadowTexture3;
		private int uLight2Matrix;
		private int uLight3Matrix;
		private int uShadowTextureSize2;
		private int uShadowTextureSize3;
		private int uShadowStarts;
		private int uShadowEnds;
		
		private int ssaoKernelSize = 32;
		private int uKernelSize;
		private int uKernelOffsets;
		private int uTanHalfFov;
		private int uAspectRatio;
		private int uPerspectiveMatrix;
		private int uProjectionInvMatrix;

		private Texture2D m_NoiseTexture = new Texture2D("Noise", ResourceMode.Static, new Texture2DInfo()
		{
			 Border = 0, 
			 InternalFormat = PixelInternalFormat.Rgb16f, 
			 PixelFormat = PixelFormat.Rgb, 
			 MagFilter = TextureMagFilter.Nearest,
			 MinFilter = TextureMinFilter.Nearest, 
			 PixelType = PixelType.Float,
			 Size = new System.Drawing.Size(4, 4),
			 WrapS = TextureWrapMode.Repeat, 
			 WrapT = TextureWrapMode.Repeat, 
		});

		private int uViewProjMatrix;

		#endregion

		public override string Name
		{
			get { return "Deferred: Shadow Final"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Shadow_Final()
		{			
			List<string> defines = new List<string>();

			Defines = defines.ToArray(); 
		}
		
		public void Render(View3D view, 
						    Texture2D normalTexture,
							Texture2D depthTexture,
							Texture2D shadowTexture1,
							Texture2D shadowTexture2,
							Texture2D shadowTexture3,
							Matrix4[] lightMatrix,
							float[] depthStarts,
							float[] depthEnds,
							Texture2D AONoiseTexture,
							VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, normalTexture);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, depthTexture);

			/* 
			if (m_InternalFormat == PixelInternalFormat.DepthComponent ||
				m_InternalFormat == PixelInternalFormat.DepthComponent16Sgix ||
				m_InternalFormat == PixelInternalFormat.DepthComponent16 ||
				m_InternalFormat == PixelInternalFormat.DepthComponent24Sgix ||
				m_InternalFormat == PixelInternalFormat.DepthComponent32 ||
				m_InternalFormat == PixelInternalFormat.DepthComponent32Sgix ||
				m_InternalFormat == PixelInternalFormat.DepthStencil ||
				m_InternalFormat == PixelInternalFormat.Depth24Stencil8 ||
				m_InternalFormat == PixelInternalFormat.DepthComponent32f ||
				m_InternalFormat == PixelInternalFormat.Depth32fStencil8)
			{
			 * */ 

			//}

            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, m_NoiseTexture);

            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, shadowTexture1);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRToTexture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)DepthFunction.Less); // Lequal);

            GLState.BindTexture(TextureUnit.Texture4, TextureTarget.Texture2D, shadowTexture2);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRToTexture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)DepthFunction.Less); // Lequal);

            GLState.BindTexture(TextureUnit.Texture5, TextureTarget.Texture2D, shadowTexture3);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRToTexture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)DepthFunction.Less); // Lequal);

			Matrix4 projInv = view.Projection;

			projInv.Invert();

			GL.UniformMatrix4(uProjectionInvMatrix, false, ref projInv);

			GL.UniformMatrix4(uPerspectiveMatrix, false, ref view.Projection);
			
			Matrix4 viewProj = view.View * view.Projection;
			
			GL.UniformMatrix4(uViewProjMatrix, false, ref viewProj); // view.ProjectionInverse);

			viewProj.Invert();

			GL.UniformMatrix4(uPerspectiveInvMatrix, false, ref viewProj); // view.ProjectionInverse);		

			GL.UniformMatrix4(uLight1Matrix, false, ref lightMatrix[0]);
			GL.UniformMatrix4(uLight2Matrix, false, ref lightMatrix[1]);
			GL.UniformMatrix4(uLight3Matrix, false, ref lightMatrix[2]);

			GL.Uniform3(uShadowStarts, depthStarts[0], depthStarts[1], depthStarts[2]);
			GL.Uniform3(uShadowEnds, depthEnds[0], depthEnds[1], depthEnds[2]); 

			GL.Uniform2(uShadowTextureSize1, 1f / (float)shadowTexture1.ResourceInfo.Size.Width, 1f / (float)shadowTexture1.ResourceInfo.Size.Height);
			GL.Uniform2(uShadowTextureSize2, 1f / (float)shadowTexture2.ResourceInfo.Size.Width, 1f / (float)shadowTexture2.ResourceInfo.Size.Height);
			GL.Uniform2(uShadowTextureSize3, 1f / (float)shadowTexture3.ResourceInfo.Size.Width, 1f / (float)shadowTexture3.ResourceInfo.Size.Height);

			GL.Uniform1(uTanHalfFov, (float)Math.Tan(view.Camera.FOV / 2.0f));
			GL.Uniform1(uAspectRatio, view.Camera.AspectRatio); 

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();

			shadowTexture1.Bind();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.None);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)DepthFunction.Less);
			shadowTexture1.Unbind();

			shadowTexture2.Bind();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.None);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)DepthFunction.Less);
			shadowTexture2.Unbind();

			shadowTexture3.Bind();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.None);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)DepthFunction.Less);
			shadowTexture3.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uNormalTexture = GL.GetUniformLocation(ProgramHandle, "normalTexture");
			uDepthTexture = GL.GetUniformLocation(ProgramHandle, "depthTexture");
			uAONoiseTexture = GL.GetUniformLocation(ProgramHandle, "AONoiseTexture");

			uProjectionInvMatrix = GL.GetUniformLocation(ProgramHandle, "projectionInvMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");
			uPerspectiveInvMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveInvMatrix");
			uViewProjMatrix = GL.GetUniformLocation(ProgramHandle, "viewProjMatrix");

			uShadowTexture1 = GL.GetUniformLocation(ProgramHandle, "shadowTexture1");
			uShadowTexture2 = GL.GetUniformLocation(ProgramHandle, "shadowTexture2");
			uShadowTexture3 = GL.GetUniformLocation(ProgramHandle, "shadowTexture3");
			
			uLight1Matrix = GL.GetUniformLocation(ProgramHandle, "light1Matrix");
			uLight2Matrix = GL.GetUniformLocation(ProgramHandle, "light2Matrix");
			uLight3Matrix = GL.GetUniformLocation(ProgramHandle, "light3Matrix");			

			uShadowTextureSize1 = GL.GetUniformLocation(ProgramHandle, "shadowTextureSize1");
			uShadowTextureSize2 = GL.GetUniformLocation(ProgramHandle, "shadowTextureSize2");
			uShadowTextureSize3 = GL.GetUniformLocation(ProgramHandle, "shadowTextureSize3");

			uShadowStarts = GL.GetUniformLocation(ProgramHandle, "shadowStarts");
			uShadowEnds = GL.GetUniformLocation(ProgramHandle, "shadowEnds");

			uKernelSize = GL.GetUniformLocation(ProgramHandle, "kernelSize");
			uKernelOffsets = GL.GetUniformLocation(ProgramHandle, "kernelOffsets");
			uTanHalfFov = GL.GetUniformLocation(ProgramHandle, "tanHalfFov");
			uAspectRatio = GL.GetUniformLocation(ProgramHandle, "aspectRatio"); 

			GL.Uniform1(uNormalTexture, 0);
			GL.Uniform1(uDepthTexture, 1);
			GL.Uniform1(uAONoiseTexture, 2);
			GL.Uniform1(uShadowTexture1, 3);
			GL.Uniform1(uShadowTexture2, 4);
			GL.Uniform1(uShadowTexture3, 5);

			GL.UseProgram(0);

			ResizeSsaoKernel();

			m_NoiseTexture.LoadResources();
			
			ResizeSsaoNoise();	
		}

		public override void UnloadResources()
		{
			base.UnloadResources();

			m_NoiseTexture.UnloadResources();
		}

		bool ResizeSsaoNoise() 
		{		
			//	generate noise data:
			int noiseDataSize = m_NoiseTexture.ResourceInfo.Size.Width * m_NoiseTexture.ResourceInfo.Size.Height;
			Vector3[] noiseData = new Vector3[noiseDataSize];

			Random rand = new Random(123456);

			double angleDelta = Math.PI * 2.0 / (double)noiseDataSize;

			double angle = 0; 

			for (int i = 0; i < noiseDataSize; ++i) 
			{			
				Vector3 vector = new Vector3(
					GetRand(rand, -1, 1),
					GetRand(rand, -1, 1), 
					//EncodeSin(Math.Sin(angle)),
					//EncodeSin(Math.Cos(angle)),
					GetRand(rand, 0, 1) // 0.0f
				);

				angle += angleDelta;

				vector.Normalize();

				vector.X = vector.X * 0.5f + 0.5f;
				vector.Y = vector.Y * 0.5f + 0.5f;
				vector.Z = vector.Z * 0.5f + 0.5f;

				noiseData[i] = vector; 
			}

			noiseData = RandomizeArray(noiseData, rand); 

		//	reize noise texture & upload:
			
			GCHandle handle = GCHandle.Alloc(noiseData, GCHandleType.Pinned);

			try
			{
				IntPtr pointer = handle.AddrOfPinnedObject();

				m_NoiseTexture.UploadImage(pointer);
			}
			finally
			{
				if (handle.IsAllocated)
				{
					handle.Free();
				}
			}

			return true;
		}

		private float EncodeSin(double p)
		{
			return ((float)p) * 0.5f + 0.5f; 
		}

		public static T[] RandomizeArray<T>(T[] arr, Random rand)
		{
			List<KeyValuePair<int, T>> list = new List<KeyValuePair<int, T>>();
			// Add all strings from array
			// Add new random int each time
			foreach (T s in arr)
			{
				list.Add(new KeyValuePair<int, T>(rand.Next(), s));
			}
			// Sort the list by the random number
			var sorted = from item in list
						 orderby item.Key
						 select item;

			// Allocate new string array
			T[] result = new T[arr.Length];
			// Copy values to array
			int index = 0;
			foreach (KeyValuePair<int, T> pair in sorted)
			{
				result[index] = pair.Value;
				index++;
			}
			// Return copied array
			return result;
		}

		bool ResizeSsaoKernel() 
		{		
			// generate kernel:
			Vector3[] kernel = new Vector3[ssaoKernelSize];
			
			Random rand = new Random(123456); // random number generator

			Vector3 normal = new Vector3(0.0f, 0.0f, 1.0f);

			for (int i = 0; i < ssaoKernelSize; ++i) 
			{			
				Vector3 kernalVector = new Vector3(
					GetRand(rand, -1.0f, 1.0f),
					GetRand(rand, -1.0f, 1.0f),
					GetRand(rand, 0f, 1.0f)
				);
				kernalVector.Normalize();
				
				/* 
				if (Vector3.Dot(normal, kernalVector) < 0.15f)
				{
					i--;
					continue; 
				}
				*/ 
				
				float scale = (float)i / (float)ssaoKernelSize;

				kernalVector *= Vector3.Lerp(new Vector3(0.1f, 0.1f, 0.1f), new Vector3(1.0f, 1.0f, 1.0f), scale * scale).X;

				kernel[i] = kernalVector;
			}

			// upload the kernel:
			GL.UseProgram(ProgramHandle);

			GL.Uniform1(uKernelSize, ssaoKernelSize);

			float[] values = new float[ssaoKernelSize * 3];
			int index = 0; 

			for (int i = 0; i < ssaoKernelSize; i++)
			{
				values[index++] = kernel[i].X;
				values[index++] = kernel[i].Y;
				values[index++] = kernel[i].Z;
			}

			GL.Uniform3(uKernelOffsets, ssaoKernelSize, values);
			
			//OOGL_CALL(glUniform1i(shaderSsao_->getUniformLocation("uKernelSize"), ssaoKernelSize));
			//OOGL_CALL(glUniform3fv(shaderSsao_->getUniformLocation("uKernelOffsets"), ssaoKernelSize_, (const GLfloat*) kernel));

			GL.UseProgram(0);

			return true;
		}

		private float GetRand(Random rand, float min, float max)
		{
			return min + ((float)rand.NextDouble() * (max - min)); 
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{
			
		}

		#endregion
	}
}
