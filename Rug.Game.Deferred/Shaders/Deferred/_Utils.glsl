﻿
vec3 DecodeNormal(vec3 normal) 
{
	return normal.xyz * 2.0 - 1.0; 
}

vec3 EncodeNormal(vec3 normal) 
{
	return normal.xyz * 0.5 + 0.5; 
}

