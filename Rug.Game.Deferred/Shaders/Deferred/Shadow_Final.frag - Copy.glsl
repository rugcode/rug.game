//
//  Shader.fsh
//  displaylist
//
//  Created by Kris Temmerman on 16/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//// core
#version 420 
#define EPSILON 1e-6

const float strength = 1; //  0.09;
const float offsetAO = 30.0;
const float falloff = 0.00001;
const float rad = 0.1; // 0.01;

const int MAX_KERNEL_SIZE = 128;
uniform int kernelSize;
uniform vec3 kernelOffsets[MAX_KERNEL_SIZE];
uniform float radius = 0.01; //1.5; // 0.1; // 1.5;
uniform float power = 2.0;
uniform float distThresh = 1;
in smooth mat3 tangentViewMatrix;
in noperspective vec3 viewRay;
int sampleCount = 16; 
vec2 poissonDisk[16] = vec2[]( 
   vec2( -0.94201624, -0.39906216 ), 
   vec2( 0.94558609, -0.76890725 ), 
   vec2( -0.094184101, -0.92938870 ), 
   vec2( 0.34495938, 0.29387760 ), 
   vec2( -0.91588581, 0.45771432 ), 
   vec2( -0.81544232, -0.87912464 ), 
   vec2( -0.38277543, 0.27676845 ), 
   vec2( 0.97484398, 0.75648379 ), 
   vec2( 0.44323325, -0.97511554 ), 
   vec2( 0.53742981, -0.47373420 ), 
   vec2( -0.26496911, -0.41893023 ), 
   vec2( 0.79197514, 0.19090188 ), 
   vec2( -0.24188840, 0.99706507 ), 
   vec2( -0.81409955, 0.91437590 ), 
   vec2( 0.19984126, 0.78641367 ), 
   vec2( 0.14383161, -0.14100790 ) 
);

// Returns a random number based on a vec3 and an int.
float random(vec3 seed, int i){
	vec4 seed4 = vec4(seed,i);
	float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
	return fract(sin(dot_product) * 43758.5453);
}

const vec2 pixelOffset = vec2(0.000488, 0.000488); 

uniform sampler2D normalTexture; 
uniform sampler2D depthTexture; 
uniform sampler2D AONoiseTexture;

uniform sampler2DShadow shadowTexture1;
uniform sampler2DShadow shadowTexture2;
uniform sampler2DShadow shadowTexture3;

uniform vec2 shadowTextureSize1;
uniform vec2 shadowTextureSize2;
uniform vec2 shadowTextureSize3;

uniform vec3 shadowStarts; 
uniform vec3 shadowEnds; 

uniform mat4 perspectiveInvMatrix;
uniform mat4 perspectiveMatrix;
uniform mat4 projectionInvMatrix;
uniform mat4 viewProjMatrix; 

uniform mat4 light1Matrix;
uniform mat4 light2Matrix;
uniform mat4 light3Matrix;
in vec2 uv_var;
// Ouput data
layout(location = 0) out vec4 fragmentdepth;


float lookup(vec4 shadowCoord, vec2 offset)
{	
	return texture(shadowTexture1, shadowCoord.xyz + vec3(offset, 0.0));
	//return texture(shadowTexture1, shadowCoord.xy + vec2(offset));
}

float DepthToZPosition(float depth, float near, float far) 
{ 
    return 2.0 * near * far / (far + near - (2.0 * depth - 1.0) * (far - near));	
}

float linearizeDepth(in float depth, in mat4 projMatrix) {
	return projMatrix[3][2] / (depth - projMatrix[2][2]);
}

vec3 decodeNormal(in vec3 normal) 
{
	return (vec4(normalize(normal * 2.0 - 1.0), 0) * perspectiveMatrix).xyz; 
}

vec3 calculatePosition(in vec2 coord, in float depth) 
{
	//float linearDepth = linearizeDepth(depth, perspectiveMatrix); 

	vec3 pos = vec3(0.0, 0.0, 0.0);
	pos.xy = coord * 2.0 - 1.0;
	pos.z = depth * 2.0 - 1.0;

	vec4 viewPos = projectionInvMatrix * vec4(pos, 1.0);
	viewPos.xyz /= viewPos.w;	

	return viewPos.xyz; 
}

/*----------------------------------------------------------------------------*/
/*
float ssao(in vec3 originPos, in float radius) {
	float occlusion = 0.0;
	for (int i = 0; i < kernelSize; ++i) { // kernelSize
	//	get sample position:
		vec3 samplePos = (kernelOffsets[i] * radius) + originPos;
		//vec3 samplePos = kernelOffsets[i];
		//	project sample position:
		vec4 offset = viewProjMatrix * vec4(samplePos, 1.0);
		offset.xy /= offset.w; // only need xy
		offset.xy = offset.xy * 0.5 + 0.5; // scale/bias to texcoords		
		//	get sample depth:
		float sampleDepth = texture(depthTexture, offset.xy).r;
		sampleDepth = linearizeDepth(sampleDepth, perspectiveMatrix);		 
		float rangeCheck = smoothstep(0.0, 1.0, radius / abs(originPos.z - sampleDepth));
		occlusion += rangeCheck * step(sampleDepth, samplePos.z);
	}
	
	//occlusion *= 0.5f; 

	occlusion = 1.0 - (occlusion / float(kernelSize));
	return pow(occlusion, power);

}
*/ 

float SampleShadow(vec4 worldPos, float bias, mat4 lightMatrix, vec2 shadowTextureSize, sampler2DShadow shadowTexture) 
{
	float shadowTerm = 0; 

	vec4 lightWorld = lightMatrix * worldPos;

	lightWorld.xyz /= lightWorld.w;
	
	lightWorld /= 2.0;
	lightWorld += 0.5;
	float zLight = lightWorld.z;			

	lightWorld.z -= bias;

	float filterWidth = 1; 

	// 8x8 kernel PCF
	float x, y; 
	vec2 kernalSpan = vec2(2, 2) * filterWidth * shadowTextureSize; 
	float xs = -kernalSpan.x;
	float xe = kernalSpan.x; 
	float ys = -kernalSpan.y;
	float ye= kernalSpan.y;
	vec2 inc = shadowTextureSize * filterWidth;
	for (y = ys; y <= ye; y += inc.y)
	{
		for (x = xs; x <= xe; x += inc.x)
		{
			shadowTerm += texture(shadowTexture, lightWorld.xyz + vec3(x, y, 0));
		}
	}

	shadowTerm /= 25.0;

	return shadowTerm; 
}

float ssao(in mat3 kernelBasis, in vec3 originPos, in float radius) {	

	float occlusion = 0.0;
	for (int i = 0; i < kernelSize; ++i) {
		//get sample position:
		vec3 samplePos = kernelBasis * kernelOffsets[i];
		samplePos = samplePos * radius + originPos;

		//project sample position:
		vec4 offset = perspectiveMatrix * vec4(samplePos, 1.0);
		offset.xy /= offset.w; // only need xy
		offset.xy = offset.xy * 0.5 + 0.5; // scale/bias to texcoords

		//get sample depth:
		float sampleDepth = texture(depthTexture, offset.xy).r;
		sampleDepth = linearizeDepth(sampleDepth, perspectiveMatrix);

		float rangeCheck = smoothstep(0.0, 1.0, radius / abs(originPos.z - sampleDepth));
		occlusion += rangeCheck * step(sampleDepth, samplePos.z);
	}

	occlusion = 1.0 - (occlusion / float(kernelSize));

	return pow(occlusion, power);
}

void main() {

	float depthScreen = texture(depthTexture, uv_var).r;

	if (depthScreen == 1.0)
	{  
		fragmentdepth = vec4(0.5, 0.5, 1.0, 1.0);
	}
	else
	{
		vec3 viewPos = calculatePosition(uv_var, depthScreen); 

		vec3 viewNormal = decodeNormal(texture(normalTexture, uv_var).xyz); 

		float ambientOcclusion = 0; 

		float localRad = radius; // / viewPos.z; 

		for (int i = 0; i < sampleCount; i++) 
		{
			vec2 sampleTexCoord = uv_var + (poissonDisk[i] * localRad); // / dist ? 

			float sampleDepth = texture(depthTexture, sampleTexCoord).r; 

			vec3 samplePos = calculatePosition(sampleTexCoord, sampleDepth); 

			vec3 sampleDir = normalize (samplePos - viewPos); 

			float NdotS = max(dot(viewNormal, sampleDir), 0); 

			float VPdistSP = distance(viewPos, samplePos); 

			float a = 1.0 - smoothstep(distThresh, distThresh * 2.0, VPdistSP); 

			float b = NdotS; 

			ambientOcclusion += (a * b);
		} 

		ambientOcclusion = 1.0 - (ambientOcclusion / sampleCount);
		//ambientOcclusion = (ambientOcclusion / sampleCount);

		fragmentdepth = vec4(ambientOcclusion, ambientOcclusion, ambientOcclusion, 1); // vec4(samplePos.xyz * 0.5 + 0.5, 1.0); // vec4(clamp(shadowTerm, 0.0, 1.0));

		//fragmentdepth = vec4(viewNormal, 1.0); 
		/* 
		
		//vec3 uKernelOffsets[10] = vec3[](vec3(-0.010735935, 0.01647018, 0.0062425877),vec3(-0.06533369, 0.3647007, -0.13746321),vec3(-0.6539235, -0.016726388, -0.53000957),vec3(0.40958285, 0.0052428036, -0.5591124),vec3(-0.1465366, 0.09899267, 0.15571679),vec3(-0.44122112, -0.5458797, 0.04912532),vec3(0.03755566, -0.10961345, -0.33040273),vec3(0.019100213, 0.29652783, 0.066237666),vec3(0.8765323, 0.011236004, 0.28265962),vec3(0.29264435, -0.40794238, 0.15964167));

		vec2 noiseTexCoords = vec2(textureSize(depthTexture, 0)) / vec2(textureSize(AONoiseTexture, 0));
		noiseTexCoords *= uv_var;

		vec3 originPos = calculatePosition(uv_var, depthScreen); 

		vec3 normal = decodeNormal(texture(normalTexture, uv_var).xyz); 

		//float originDepth = texture(depthTexture, uv_var).r;
		//originDepth = linearizeDepth(originDepth, perspectiveMatrix);
		//vec3 originPos = viewRay * originDepth;


		//	get view space normal:
		//vec3 normal = normalize(texture(normalTexture, uv_var).rgb * 2.0 - 1.0);

		//	construct kernel basis matrix:
		vec3 rvec = texture(AONoiseTexture, noiseTexCoords).rgb * 2.0 - 1.0;
		vec3 tangent = normalize(rvec - normal * dot(rvec, normal));
		vec3 bitangent = cross(tangent, normal);
		mat3 kernelBasis = mat3(tangent, bitangent, normal);

		fragmentdepth = vec4(ssao(kernelBasis, originPos, radius));		 
		*/ 
	}
}

/*
void main()
{

	float depthScreen = texture(depthTexture, uv_var).x;
	float linearDepth = DepthToZPosition(depthScreen, 0.1, 6000.0);
	float sampleDepth = linearizeDepth(depthScreen, perspectiveMatrix);

	 if (depthScreen == 1.0)
	 {  
		 fragmentdepth = vec4(0.5, 0.5, 1.0, 1.0);
	 }
	 else
	 {
		vec3 normal = texture(normalTexture, uv_var).xyz * 2.0 - 1.0;

		normal = normalize(normal); 

		float depth = depthScreen * 2.0 - 1.0;

		vec3 pos = vec3(0.0,0.0,0.0);

		pos.xy = uv_var * 2.0 -1.0;

		pos.z = depth;

		vec4 worldPos = perspectiveInvMatrix * vec4(pos, 1.0);

		//worldPos.xyz /= worldPos.w;	
	
		vec4 screenSpacePos = projectionInvMatrix * vec4(pos, 1.0);

		screenSpacePos.xyz /= screenSpacePos.w;

		//DROPSHADOW
		float shadowTerm = 1.0;
		
		float bias1 = 0.000175;
		float bias2 = 0.005; // 0.005;
		float bias3 = 0.005; // 0.1;

		//float nearRange = 1.0 - ((1.0 / 6000.0) * 20.0); 

		/* 
		//MAP 1
		if (linearDepth < shadowStarts.y) // 0.93)
		{	
			shadowTerm = SampleShadow(worldPos, bias1, light1Matrix, shadowTextureSize1, shadowTexture1);			
		} 
		else if (linearDepth < shadowEnds.x)
		{
			float shadowTerm1 = SampleShadow(worldPos, bias1, light1Matrix, shadowTextureSize1, shadowTexture1);
			float shadowTerm2 = SampleShadow(worldPos, bias2, light2Matrix, shadowTextureSize2, shadowTexture2);

			float dist = shadowEnds.x - shadowStarts.y;
			shadowTerm = mix(shadowTerm1, shadowTerm2, (1.0 / dist) * (linearDepth - shadowStarts.y));
		}
		else if (linearDepth < shadowStarts.z) // 0.93)
		{	
			shadowTerm = SampleShadow(worldPos, bias2, light2Matrix, shadowTextureSize2, shadowTexture2);
		}
		else if (linearDepth < shadowEnds.y) // 0.93)
		{	
			float shadowTerm1 = SampleShadow(worldPos, bias2, light2Matrix, shadowTextureSize2, shadowTexture2);
			float shadowTerm2 = SampleShadow(worldPos, bias3, light3Matrix, shadowTextureSize3, shadowTexture3);

			float dist = shadowEnds.y - shadowStarts.z;
			shadowTerm = mix(shadowTerm1, shadowTerm2, (1.0 / dist) * (linearDepth - shadowStarts.z));
		}
		else 
		{	
			shadowTerm = SampleShadow(worldPos, bias3, light3Matrix, shadowTextureSize3, shadowTexture3);			
		}
		*/ 
			/* 		
			float x,y;
			for (y = -3.5 ; y <=3.5 ; y+=1.0)
				for (x = -3.5 ; x <=3.5 ; x+=1.0)
					shadowTerm += lookup(lightWorld, vec2(x,y));
					
			shadowTerm /= 64.0 ;
			*/ 
			/*
			for (float x = -4.0; x < 5.0; x += 1.0) 
			{
				for (float y = -4.0; y < 5.0; y += 1.0) 
				{
					float lightDepth = texture(shadowTexture1, lightWorld.xy + vec2(offset.x * x, offset.y * y)).x;
					
					//float lightDepth = texture2D(shadowTexture1, uv_var).x; // lightWorld.xy).x;
					//float lightDepth = texture2D(shadowTexture1, lightWorld.xy).x; // lightWorld.xy).x;

					/* 
					if (depthScreen > EPSILON && depthScreen < (1.0 - EPSILON)) 
					{
						shadowTerm = 0.0; // clamp(depthScreen * 10.0, 0.0, 1.0);
					}
					else 
					{
						shadowTerm = 1.0; // 
					}
					* / 

					//shadowTerm = clamp(lightDepth * 1000.0, 0.0, 1.0);

					
					if (zLight > lightDepth + bias)
					{
						shadowTerm -= weight;					
						//shadowTerm = 0;					
					}
					 
				}	
			}
			*/
		//}
		/* 
		//MAP 2
		else if (depth < 0.985)
		{	
			vec4 lightWorld = light2Matrix * worldPos;
	
			lightWorld.xyz /= lightWorld.w;
	
			lightWorld /= 2.0;
			lightWorld += 0.5;
			float zLight = lightWorld.z;
			vec2 offset = vec2(0.000244, 0.000244);
			
			for(float x = -1.0; x < 2.0; x += 1.0) 
			{
				for(float y=-1.0; y < 2.0; y+=2.0) 
				{
					float lightDepth = texture2D(shadowTexture2, lightWorld.xy + vec2(offset.x * x,offset.y * y)).x;

					if (zLight >lightDepth)
					{
						shadowTerm -= 0.10;					
					}
				}	
			}
		}
		else
		{		
			vec4 lightWorld = light3Matrix * worldPos;
	
			lightWorld.xyz /= lightWorld.w;
	
			lightWorld /= 2.0;
			lightWorld += 0.5;
			float zLight = lightWorld.z;
			vec2 offset = vec2(0.000244, 0.000244);
		
			for(float x=-1.0; x < 2.0; x+=1.0) 
			{
				for(float y = -1.0; y < 2.0; y += 2.0) 
				{
					float lightDepth = texture2D( shadowTexture3,lightWorld.xy+vec2(offset.x *x,offset.y *y)).x;

					if (zLight >lightDepth)
					{
						shadowTerm -= 0.10;					
					}
				}	
			}
		}
		*/ 
 
		/*
		vec3 pSphere[10] = vec3[](
			vec3(-0.010735935, 0.01647018, 0.0062425877),
			vec3(-0.06533369, 0.3647007, -0.13746321),
			vec3(-0.6539235, -0.016726388, -0.53000957),
			vec3(0.40958285, 0.0052428036, -0.5591124),
			vec3(-0.1465366, 0.09899267, 0.15571679),
			vec3(-0.44122112, -0.5458797, 0.04912532),
			vec3(0.03755566, -0.10961345, -0.33040273),
			vec3(0.019100213, 0.29652783, 0.066237666),
			vec3(0.8765323, 0.011236004, 0.28265962),
			vec3(0.29264435, -0.40794238, 0.15964167));
		
		vec3 fres = normalize(texture2D(AONoiseTexture, uv_var * offsetAO).xyz);
		float occluderDepth, depthDifference;
		vec4 occluderNormal;
		vec3 ray;
		vec2 uv_Sample;
		float radD = rad*depthScreen;

		float bl = 0.0;

		for(int i = 0; i < 10; ++i)
		{
			// get a vector (randomized inside of a sphere with radius 1.0) from a texture and reflect it
			ray = radD * reflect(pSphere[i], fres);
 
			uv_Sample = uv_var + (sign(dot(ray,normal.xyz))) * ray.xy;
			occluderDepth = texture2D(depthTexture, uv_Sample).r;
			occluderNormal = texture2D(normalTexture, uv_Sample) ;
			depthDifference = depthScreen - occluderDepth;
 
 
			bl += step(falloff, depthDifference) * (1.0 - dot(occluderNormal.xyz, normal)) * (1.0 - smoothstep(falloff, strength, depthDifference));
		}
		

		//shadowTerm*=clamp(1 - (bl / 10.0), 0.0, 1.0);		
		shadowTerm = clamp(1.0 - (bl / 10.0), 0.0, 1.0);		
		* /	

		//	get noise texture coords:
		vec2 noiseTexCoords = vec2(textureSize(depthTexture, 0)) / vec2(textureSize(AONoiseTexture, 0));
		noiseTexCoords *= uv_var;
	
		//	get view space origin:
		//float originDepth = texture(uGBufferDepthTex, vertexIn.texcoord).r;
		float originDepth = sampleDepth; // linearizeDepth(originDepth, uProjectionMatrix);
		vec3 originPos = viewRay * originDepth; // viewRay * originDepth;		
		//vec3 originPos = vec3(uv_var, originDepth); // viewRay * originDepth;		
		//vec3 originPos = screenSpacePos.xyz; // worldPos.xyz / worldPos.w; 

		//	get view space normal:
		//vec3 normal = decodeNormal(texture(uGBufferGeometricTex, vertexIn.texcoord).rgb);
		//normal = normalize(tangentViewMatrix * normal); 
		//	construct kernel basis matrix:
		// get random vector
		vec3 rvec = texture(AONoiseTexture, noiseTexCoords).xyz;
		//	construct kernel basis matrix:
		//vec3 rvec = normalize(vec3(texture(AONoiseTexture, noiseTexCoords).rg * 2.0 - 1.0, 0));
		//vec3 tangent = normalize(rvec - normal * dot(rvec, normal));
		//vec3 bitangent = cross(tangent, normal);
		//mat3 kernelBasis = mat3(tangent, bitangent, normal);	
		shadowTerm = ssao(originPos, radius);

		//vec3 samplePos = normalize(kernelBasis * kernelOffsets[0]);
				
		fragmentdepth = vec4(sampleDepth * 0.001, shadowTerm, shadowTerm, 1); // vec4(samplePos.xyz * 0.5 + 0.5, 1.0); // vec4(clamp(shadowTerm, 0.0, 1.0));

	}// end background if

	//gl_FragColor =vec4(texture2D(shadowTexture1, uv_var).xyz ,1.0);
}
*/ 