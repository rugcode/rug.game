#version 410

uniform mat4 objectMatrix;
uniform mat4 normalMatrix;
uniform mat4 worldMatrix;
uniform mat4 perspectiveMatrix;
uniform float lightSize;
uniform vec3 center;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;

out vec3 center_var;
out vec3 color_var;
out vec4 persPos_var;
out vec4 worldPos_var;
out vec3 normal_var;
out float lightSize_var;

void main()
{
    vec4 localSpace = objectMatrix * vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;

    center_var = (worldMatrix * vec4(center, 1.0)).xyz;
	normal_var = (normalMatrix * vec4(normal, 1)).xyz;
  
    color_var = color;

    worldPos_var = worldSpace;
    persPos_var = gl_Position;

	lightSize_var = lightSize;
}