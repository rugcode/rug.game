﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Shaders.Deferred
{	
	public class Deferred_LightingMesh : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/PointLight";
		private int uNormalTexture;
		private int uDepthTexture;
		private int uCenter;
		private int uLightSize;
		private int uObjectMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uPerspectiveInvMatrix;
		private int uNormalWorldMatrix;

		#endregion

		public override string Name
		{
			get { return "Deferred: Lighting Mesh"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_LightingMesh() { }

		public void Begin(ref Matrix4 world, ref Matrix4 projection, ref Matrix4 projectionInv, ref Matrix4 normalWorld, Texture2D normalTexture, Texture2D depthTexture)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			//GL.DepthMask(false);

			GL.UniformMatrix4(uWorldMatrix, false, ref world);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref projection);
			GL.UniformMatrix4(uPerspectiveInvMatrix, false, ref projectionInv);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorld);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, normalTexture);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, depthTexture);
		}

		public void Render(ref Matrix4 objectMatrix, Vector3 center, float lightSize, int indexCount, DrawElementsType indexType)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);

			GL.Uniform3(uCenter, center.X, center.Y, center.Z);
			GL.Uniform1(uLightSize, lightSize);

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, IntPtr.Zero);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uNormalTexture = GL.GetUniformLocation(ProgramHandle, "normalTexture");
			uDepthTexture = GL.GetUniformLocation(ProgramHandle, "depthTexture");
			GL.Uniform1(uNormalTexture, 0);
			GL.Uniform1(uDepthTexture, 1);

			uCenter = GL.GetUniformLocation(ProgramHandle, "center");
			uLightSize = GL.GetUniformLocation(ProgramHandle, "lightSize");
			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");

			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");
			uPerspectiveInvMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveInvMatrix");
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");
   
			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)LightingMeshVertex.Elements.Position, "position");

			GL.BindAttribLocation(ProgramHandle, (int)LightingMeshVertex.Elements.Normal, "normal");

			GL.BindAttribLocation(ProgramHandle, (int)LightingMeshVertex.Elements.Color, "color");
			 * */ 
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

	}
}
