#version 410

const float strength = 0.90;
const float falloff = 0.0002;
const float rad = 0.01;
const float invSamples = -1.38/10.0;

uniform sampler2D lambertTexture; 
uniform sampler2D pointLightTexture; 
uniform sampler2D colorTexture; 
uniform sampler2D normalTexture; 
uniform sampler2D depthTexture; 
uniform sampler2D shadowTexture;

uniform mat4 perspectiveInvMatrix;
//uniform mat4 worldMatrixInv;
uniform mat4 light1Matrix;

uniform float time;
uniform float colorFactor;

in vec2 uv_var;
in vec3 lightDir_var;

out vec4 colorFrag;

void main()
{		
	/* 
	vec4 diffuse = texture2D(colorTexture, uv_var);	 
	vec4 normal = texture2D(normalTexture, uv_var);	 
	vec4 lightCol = texture2D(pointLightTexture, uv_var);		
	float depth = texture2D(depthTexture, uv_var); 

	//gl_FragColor = vec4(1,1,1,1);

	if (depth == 1.0) 
	{
		//gl_FragColor = vec4(1 - depth, depth, 1 - (depth * 3), 1);
		gl_FragColor = diffuse; // vec4(0, 0.2, 0, 1);
	}
	else 
	{
		depth = depth * 2.0 - 1.0; 
		//gl_FragColor = vec4(depth, depth, depth, 1);// * vec4(lightCol.xyz, 1);
		gl_FragColor = vec4(depth, depth, depth, 1) * vec4(lightCol.xyz, 1);
		//gl_FragColor = diffuse; 
		//gl_FragColor = diffuse * lightCol; 
		//gl_FragColor = vec4(lightCol.xyz, 1); 
		//gl_FragColor = normal; 
	}
	*/ 

	
	float depthScreen = texture(depthTexture, uv_var).x;
	
	 if (depthScreen==1.0)
	 {  
		 colorFrag = vec4(1,1,1,1); // 
		 //colorFrag = texture(colorTexture, uv_var);
	 }
	 else
	 {

		float depth = depthScreen * 2.0 - 1.0;
		vec3 col = texture(colorTexture, uv_var).xyz;
		vec3 normal = normalize(texture(normalTexture, uv_var).xyz * 2.0 - 1.0);
		vec3 pLight = texture(pointLightTexture, uv_var).xyz;
		//float shadow = texture2D(shadowTexture,uv_var).x;
		float  shadow = texture(shadowTexture,uv_var).x;
								
		vec3 pos = vec3(0.0, 0.0, 0.0);
		pos.xy = uv_var * 2.0 - 1.0;
		pos.z = depth;
		vec4 worldPos = perspectiveInvMatrix * vec4(pos, 1.0);
		worldPos.xyz /= worldPos.w;	
	
		vec3 lightDir = normalize(lightDir_var);
		float lambert = clamp(dot(normal, lightDir) * 0.5 + 0.5, 0.001, 0.999);
		//vec3 globalLight = texture2D(lambertTexture, vec2(lambert *shadow, time * 0.9)).xyz;
		//vec3 globalLight = texture2D(lambertTexture, vec2(lambert, time * 0.9)).xyz;
		vec3 globalLight = texture(lambertTexture, vec2(lambert, time * 0.9)).xyz;

		vec3 reflectVec = normalize(reflect(-lightDir, normal));
		vec3 eyeVecNormal = normalize(-worldPos.xyz);
		//float specular = pow(max(dot(eyeVecNormal,reflectVec),0.0), 8.0) * 0.1;
		float specular = pow(max(dot(eyeVecNormal,reflectVec), 0.0), 8.0) * 0.75;
		col *= globalLight + (pLight*(1.0 - colorFactor));
		//col = globalLight+(pLight*(1.0-colorFactor));
		//col *= 0;

		//fogFactor = (end - z) / (end - start) 
		float fogFactor = pow(1.0- clamp((1.0 - depthScreen) / 0.001, 0.0, 1.0),2.0);
		
	    //gl_FragColor = vec4(col + specular, 1.0) * (1.0 - fogFactor) + (fogFactor) * vec4(0.8, 0.8, 1.0, 1.0) * (colorFactor);
		colorFrag = vec4(col + specular, 1.0);
		
		// calculate the liminance and put in alpha channel 
		colorFrag.w = dot(colorFrag.xyz, vec3(0.299, 0.587, 0.114));

		colorFrag = vec4(1,1,1,1);

		//gl_FragColor = vec4(pLight, 1.0);
		//gl_FragColor = vec4(specular,specular,specular ,1.0);
		//gl_FragColor = vec4(globalLight ,1.0);
		//gl_FragColor = vec4(texture2D(normalTexture, uv_var).xyz ,1.0);
		//gl_FragColor = vec4((fogFactor)*vec3(0.8,0.8,1.0) ,1.0);
		//gl_FragColor = vec4(col ,1.0);
		//gl_FragColor *= 0.9;
		//gl_FragColor += vec4(texture2D(colorTexture, uv_var).xyz,1.0)*0.1;
		//gl_FragColor = vec4( lightWorld.xy,0.0,1.0);
		//gl_FragColor = vec4(pLight ,1.0);
		//gl_FragColor = vec4(col,1.0);
		//gl_FragColor = vec4( depth, depth, depth,1.0);
		//gl_FragColor = vec4( pos,1.0);}


		//gl_FragColor = vec4(lambert *shadow, lambert *shadow, lambert *shadow, 1.0);
		//gl_FragColor = vec4(fres, 1.0);
    }

	// gl_FragColor  = texture2D(colorTexture, uv_var);
}