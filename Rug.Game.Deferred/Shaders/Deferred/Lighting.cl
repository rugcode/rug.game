﻿#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable

#define EPSILON 1e-6
#define BLOCK_SIZE 16
#define TILE_SIZE 256

#define MAX_LIGHTS 1024

__constant sampler_t sampler =  CLK_NORMALIZED_COORDS_TRUE |
                                CLK_ADDRESS_CLAMP_TO_EDGE |
                                CLK_FILTER_NEAREST;

struct Light
{
	float3 Position;
	float SqrRadius;

	float3 Color;
	float InvSqrRadius; 
};

/* 
int convert_bits_float(float value) 
{ 
	union { int i, float f; } u;
 	u.f = value;
 	return u.i;
};

float convert_bits_int(int value) 
{ 
	union { int i, float f; } u;
 	u.i = value;
 	return u.f;
};
*/ 

bool  PlaneIntersectsPoint(float4 plane, float3 point)
{
    float distance = dot(plane.xyz, point);
    
	distance += plane.w;

    if (distance >= 0.0) 
	{
		return true;   
	}
	else 
	{
		return false; 
	}
}

bool  PlaneIntersectsSphere(float4 plane, float3 point, float radius)
{
    float distance = dot(plane.xyz, point);
    
	distance += plane.w;

    if (distance >= -radius) 
	{
		return true;   
	}
	else 
	{
		return false; 
	}
}

float4 CreatePlane(float3 point1, float3 point2, float3 point3)
{
    float x1 = point2.x - point1.x;
    float y1 = point2.y - point1.y;
    float z1 = point2.z - point1.z;

    float x2 = point3.x - point1.x;
    float y2 = point3.y - point1.y;
    float z2 = point3.z - point1.z;

    float yz = (y1 * z2) - (z1 * y2);
    float xz = (z1 * x2) - (x1 * z2);
    float xy = (x1 * y2) - (y1 * x2);

    float invPyth = 1.0f / (float)(sqrt((yz * yz) + (xz * xz) + (xy * xy)));

	float4 result = (float4)(yz * invPyth, xz * invPyth, xy * invPyth, 0.0);

	result.w = -((result.x * point1.x) + (result.y * point1.y) + (result.z * point1.z));

	return result;
} 


float4 multiply(float4 vec, const float4 matrix0, const float4 matrix1, const float4 matrix2, const float4 matrix3)
{
	/* 
	float4 result = (float4)(
		(((vec.x * matrix.s0) + (vec.y * matrix.s4)) + (vec.z * matrix.s8)) + (vec.w * matrix.s12), 
		(((vec.x * matrix.s1) + (vec.y * matrix.s5)) + (vec.z * matrix.s9)) + (vec.w * matrix.s13), 
		(((vec.x * matrix.s2) + (vec.y * matrix.s6)) + (vec.z * matrix.s10)) + (vec.w * matrix.s14), 
		(((vec.x * matrix.s3) + (vec.y * matrix.s7)) + (vec.z * matrix.s11)) + (vec.w * matrix.s15));
	*/

	float4 result = (float4)(
		(((vec.x * matrix0.x) + (vec.y * matrix1.x)) + (vec.z * matrix2.x)) + (vec.w * matrix3.x), 
		(((vec.x * matrix0.y) + (vec.y * matrix1.y)) + (vec.z * matrix2.y)) + (vec.w * matrix3.y), 
		(((vec.x * matrix0.z) + (vec.y * matrix1.z)) + (vec.z * matrix2.z)) + (vec.w * matrix3.z), 
		(((vec.x * matrix0.w) + (vec.y * matrix1.w)) + (vec.z * matrix2.w)) + (vec.w * matrix3.w));

    return result;
}

float DepthToZPosition(float depth, float near, float far) 
{ 
	return near / (far - depth * (far - near)) * far; 
}

// Populate the titles
kernel void PopulateTiles(
    global struct Light* lights,
	const int lightsCount,

	global unsigned short* cellLightIndices,

	read_only image2d_t depthMap, 	
	read_only image2d_t normalMap, 	
	read_only image2d_t materialMap, 	

	write_only image2d_t lightingMap, 

	const int width, const int height, 
	const int widthOver, const int heightOver, 

	const int cellsX, const int cellsY, 

	const float fov, 
	const float cameraProj_11, 
	const float cameraProj_22, 

	const float gl_ProjectionMatrix_2_z,
	const float gl_ProjectionMatrix_3_z, 

	const float4 projectionMatrix_0,
	const float4 projectionMatrix_1,
	const float4 projectionMatrix_2,
	const float4 projectionMatrix_3)
{
    int gidX = get_global_id(0);
	int gidY = get_global_id(1);
    
	int lidX = get_local_id(0);
	int lidY = get_local_id(1);

	unsigned int groupIndex = (get_local_id(1) * BLOCK_SIZE) + get_local_id(0); // get_group_id(1) * BLOCK_SIZE + get_group_id(0);

	float colx = (1.0 / (float)BLOCK_SIZE) * (float)lidX;
	float coly = (1.0 / (float)BLOCK_SIZE) * (float)lidY;
	//int x = ((gidX * BLOCK_SIZE) + lidX); 
	//int y = ((gidY * BLOCK_SIZE) + lidY); 

	int x = gidX; // gidX; 
	int y = gidY; // gidY; 

	if ((x >= width) || (y >= height)) return; 	

	float normalizedWidth = 1.0 / (float)width; 
	float normalizedHeight = 1.0 / (float)height; 

	float2 sourceCoords = (float2)((float)x * normalizedWidth, (float)y * normalizedHeight);

	float depth = read_imagef(depthMap, sampler, sourceCoords).x; 

	//if (depth < 0.0 + EPSILON || depth > 1.0 - EPSILON) return;

	float4 normal = read_imagef(normalMap, sampler, sourceCoords); 
	float4 material = read_imagef(materialMap, sampler, sourceCoords); 

	//float depth = material.w * (float)maxDepth;  

	__local unsigned int depthMin;
	__local unsigned int depthMax;
	__local unsigned int sTileLightIndices[MAX_LIGHTS];
	__local unsigned int sTileNumLights;

	if (lidX == 0 && lidY == 0) 
	{
		depthMin = 0x7F7FFFFF; // max float
		depthMax = 0;

		sTileNumLights = 0; 
	}

	barrier (CLK_LOCAL_MEM_FENCE);

	atomic_min (&depthMin, as_uint(depth));
	atomic_max (&depthMax, as_uint(depth));

	barrier (CLK_LOCAL_MEM_FENCE);

	//float z_eye = gl_ProjectionMatrix[3].z / (z_window * -2.0 + 1.0 - gl_ProjectionMatrix[2].z);

	//float minDepthZ = (as_float(depthMin) * 2.0) - 1.0; // (gl_ProjectionMatrix_3_z / (as_float(depthMin) * -2.0 + 1.0 - gl_ProjectionMatrix_2_z)) * -1.0;  // * (30000.0 + 0.1)) - 0.1;
    //float maxDepthZ = (as_float(depthMax) * 2.0) - 1.0; // (gl_ProjectionMatrix_3_z / (as_float(depthMax) * -2.0 + 1.0 - gl_ProjectionMatrix_2_z)) * -1.0; //* (30000.0 + 0.1)) - 0.1;

	float rawMinDepthZ = as_float(depthMin);
	float rawMaxDepthZ = as_float(depthMax);

	//float minDepthZ = (gl_ProjectionMatrix_3_z / (rawMinDepthZ));  // * (30000.0 + 0.1)) - 0.1;
    //float maxDepthZ = (gl_ProjectionMatrix_3_z / (rawMaxDepthZ)); //* (30000.0 + 0.1)) - 0.1;

	//float minDepthZ = (gl_ProjectionMatrix_3_z / (rawMinDepthZ * -2.0 + 1.0 - gl_ProjectionMatrix_2_z)) * -1.0;  // * (30000.0 + 0.1)) - 0.1;
    //float maxDepthZ = (gl_ProjectionMatrix_3_z / (rawMaxDepthZ * -2.0 + 1.0 - gl_ProjectionMatrix_2_z)) * -1.0; //* (30000.0 + 0.1)) - 0.1;

	float minDepthZ = DepthToZPosition(rawMinDepthZ, 0.1, 30000.0);
	float maxDepthZ = DepthToZPosition(rawMaxDepthZ, 0.1, 30000.0);
	
	//float minDepthZ = (as_float(depthMin) * 30000.0);
    //float maxDepthZ = (as_float(depthMax) * 30000.0);

	//float minDepthZ = as_float(depthMin);
    //float maxDepthZ = as_float(depthMax);

	//float minDepthZ = gl_ProjectionMatrix_3_z / as_float(depthMin); 
    //float maxDepthZ = gl_ProjectionMatrix_3_z / as_float(depthMax);

	float2 groupId = (float2)(get_group_id(0), get_group_id(1));

	/* 
    // Work out scale/bias from [0, 1]
    // float2 tileScale = (float2)(width, height) * native_recip((float)(2 * BLOCK_SIZE));
	//float2 tileScale = (float2)(widthOver, heightOver) * native_recip((float)(2 * BLOCK_SIZE));

	//float2 screenScale = ((float2)(1.0, 1.0) / (float2)(width, height)) * (float2)(widthOver, heightOver); 
	float2 screenScale = (float2)((2.0 / (float)width) * (float)widthOver, (2.0 / (float)height) * (float)heightOver); 
	//float2 screenScale = (float2)((1.0 / (float)width) * (float)widthOver, (1.0 / (float)height) * (float)heightOver); 

	//float2 tileScale = screenScale / (float2)(cellsX, cellsY); 
	float2 tileScale = (float2)(screenScale.x / (float)cellsX, screenScale.y / (float)cellsY); 
    float2 tileBias = (float2)(-1.0, -1.0) + (tileScale * groupId);
	//float2 tileBias = tileScale * groupId;

    // Now work out composite projection matrix
    // Relevant matrix columns for this tile frusta
	float w = (float)1.0 / tan(fov * 0.5);  // 1/tan(x) == cot(x)
    float h = (float)1.0 / tan((fov * ((float)height / (float)width)) * 0.5);   // 1/tan(x) == cot(x)

	float aspectRatio =	(float)height / (float)width;

	float nearHeight =  1.0 * tan(fov / 2.0) * minTileZ;
	float nearWidth = nearHeight * aspectRatio;

	float farHeight = 1.0 * tan(fov / 2.0) * maxTileZ;
	float farWidth = farHeight * aspectRatio;

	//float3 nearCenter = cameraPosition + (cameraDirection * minTileZ);
	//float3 farCenter = cameraPosition + (cameraDirection * maxTileZ);

	 
	float3 NTL = (float3)((tileBias.x) * nearWidth, (tileBias.y) * nearHeight, minTileZ);
	float3 NTR = (float3)((tileBias.x + tileScale.x) * nearWidth, (tileBias.y) * nearHeight, minTileZ);

	float3 NBL = (float3)((tileBias.x) * nearWidth, (tileBias.y + tileScale.y) * nearHeight, minTileZ);
	float3 NBR = (float3)((tileBias.x + tileScale.x) * nearWidth, (tileBias.y + tileScale.y) * nearHeight, minTileZ);

	float3 FTL = (float3)((tileBias.x) * farWidth, (tileBias.y) * farHeight, maxTileZ);
	float3 FTR = (float3)((tileBias.x + tileScale.x) * farWidth, (tileBias.y) * farHeight, maxTileZ);

	float3 FBL = (float3)((tileBias.x) * farWidth, (tileBias.y + tileScale.y) * farHeight, maxTileZ);
	float3 FBR = (float3)((tileBias.x + tileScale.x) * farWidth, (tileBias.y + tileScale.y) * farHeight, maxTileZ);
	*/

	/*
	float3 NTL = (float3)(tileBias.x, tileBias.y, minTileZ);
	float3 NTR = (float3)(tileBias.x + tileScale.x, tileBias.y, minTileZ);

	float3 NBL = (float3)(tileBias.x, tileBias.y + tileScale.y, minTileZ);
	float3 NBR = (float3)(tileBias.x + tileScale.x, tileBias.y + tileScale.y, minTileZ);

	float3 FTL = (float3)(tileBias.x, tileBias.y, maxTileZ);
	float3 FTR = (float3)(tileBias.x + tileScale.x, tileBias.y, maxTileZ);

	float3 FBL = (float3)(tileBias.x, tileBias.y + tileScale.y, maxTileZ);
	float3 FBR = (float3)(tileBias.x + tileScale.x, tileBias.y + tileScale.y, maxTileZ);
	*/

	/* 
	float nearHeight = 2f * (float)Math.Tan(camera.FOV / 2f) * NearDistance;
	float nearWidth = nearHeight * camera.AspectRatio;

	NearSize = new Vector2(nearWidth, nearHeight);

	float farHeight = 2f * (float)Math.Tan(camera.FOV / 2f) * FarDistance;
	float farWidth = farHeight * camera.AspectRatio;

	FarSize = new Vector2(farWidth, farHeight);

	Vector3 farCenter = CameraPosition + (CameraDirection * FarDistance);

	// ftl = fc + (up * Hfar/2) - (right * Wfar/2)
	FTL = farCenter + (Up * (farHeight / 2f)) - (Right * (farWidth / 2f));

	// ftr = fc + (up * Hfar/2) + (right * Wfar/2)
	FTR = farCenter + (Up * (farHeight / 2f)) + (Right * (farWidth / 2f));

	// fbl = fc - (up * Hfar/2) - (right * Wfar/2)
	FBL = farCenter - (Up * (farHeight / 2f)) - (Right * (farWidth / 2f));

	// fbr = fc - (up * Hfar/2) + (right * Wfar/2)
	FBR = farCenter - (Up * (farHeight / 2f)) + (Right * (farWidth / 2f));


	Vector3 nearCenter = CameraPosition + (CameraDirection * NearDistance);

	// ftl = fc + (up * Hfar/2) - (right * Wfar/2)
	NTL = nearCenter + (Up * (nearHeight / 2f)) - (Right * (nearWidth / 2f));

	// ftr = fc + (up * Hfar/2) + (right * Wfar/2)
	NTR = nearCenter + (Up * (nearHeight / 2f)) + (Right * (nearWidth / 2f));

	// fbl = fc - (up * Hfar/2) - (right * Wfar/2)
	NBL = nearCenter - (Up * (nearHeight / 2f)) - (Right * (nearWidth / 2f));

	// fbr = fc - (up * Hfar/2) + (right * Wfar/2)
	NBR = nearCenter - (Up * (nearHeight / 2f)) + (Right * (nearWidth / 2f));
	*/

	/* 
	float4 frustumPlanes[6];

	frustumPlanes[0] = CreatePlane(NTR, NTL, FTL);  // Top
	frustumPlanes[1] = CreatePlane(NBL, NBR, FBR);	// Bottom
	frustumPlanes[2] = CreatePlane(NTL, NBL, FBL);	// Left
	frustumPlanes[3] = CreatePlane(NBR, NTR, FBR);	// Right
	frustumPlanes[4] = CreatePlane(NTL, NTR, NBR);	// Near
	//frustumPlanes[4] = CreatePlane(NTL, NTR, NBL);	// Near
	frustumPlanes[5] = CreatePlane(FTR, FTL, FBL);	// Far

    //float4 c1 = (float4)(w * tileScale.x, 0.0, tileBias.x, 0.0);
    //float4 c2 = (float4)(0.0, h * tileScale.y, tileBias.y, 0.0);
	//float4 c4 = (float4)(0.0, 0.0, 1.0, 0.0);
	*/ 

	 /* 
	CreatePlane(float3 point1, float3 point2, float3 point3)

    // Derive frustum planes
    float4 frustumPlanes[6];
    // Sides
    frustumPlanes[0] = c4 - c1;
    frustumPlanes[1] = c4 + c1;
    frustumPlanes[2] = c4 - c2;
    frustumPlanes[3] = c4 + c2;

    // Near/far
    frustumPlanes[4] = (float4)(0.0, 0.0,  1.0, -minTileZ);
    frustumPlanes[5] = (float4)(0.0, 0.0, -1.0,  maxTileZ);
    
    // Normalize frustum planes (near/far already normalized)
    //for (unsigned int i = 0; i < 4; ++i) 
	//{
	frustumPlanes[0] *= native_recip(length(frustumPlanes[0].xyz));
	frustumPlanes[1] *= native_recip(length(frustumPlanes[1].xyz));
	frustumPlanes[2] *= native_recip(length(frustumPlanes[2].xyz));
	frustumPlanes[3] *= native_recip(length(frustumPlanes[3].xyz));

	//frustumPlanes[4] *= native_recip(length(frustumPlanes[3].xyz));
	//frustumPlanes[5] *= native_recip(length(frustumPlanes[3].xyz));
    //}   	
	*/ 

	float4 frustumPlanes[6];
 
	float2 tileScale = (float2)(width, height) * (1.0 / (float)(2.0 * BLOCK_SIZE));
 
	float2 tileBias = tileScale - groupId;
 
	float4 col1 = (float4)(-projectionMatrix_0.x * tileScale.x, projectionMatrix_0.y, tileBias.x, projectionMatrix_0.z); 
 
    float4 col2 = (float4)(projectionMatrix_1.x, -projectionMatrix_1.y * tileScale.y, tileBias.y, projectionMatrix_1.z);
 
    float4 col4 = (float4)(projectionMatrix_3.x, projectionMatrix_3.y, -1.0, projectionMatrix_3.z); 
 
	//Left plane
    frustumPlanes[0] = col4 + col1;
 
    //right plane
    frustumPlanes[1] = col4 - col1;
 
    //top plane
    frustumPlanes[2] = col4 - col2;
 
    //bottom plane
    frustumPlanes[3] = col4 + col2;
 
    //near
    frustumPlanes[4] = (float4)(0.0, 0.0, -1.0,  -minDepthZ);
 
    //far
    frustumPlanes[5] = (float4)(0.0, 0.0, -1.0,  maxDepthZ);
 
	for(int i = 0; i < 4; i++)
    {
        frustumPlanes[i] *= 1.0 / length(frustumPlanes[i].xyz);
    }
	
	barrier (CLK_LOCAL_MEM_FENCE);

    // Cull lights for this tile
    for (unsigned int lightIndex = groupIndex; lightIndex < lightsCount; lightIndex += TILE_SIZE) 
	{
        //Light light = lights[lightIndex];
				
		//unsigned int listIndex = atomic_inc(&sTileNumLights);
        		         
        // Cull: point light sphere vs tile frustum
        bool inFrustum = true;
        for (unsigned int i = 0; i < 4; ++i) 
		{
            //float d = dot(frustumPlanes[i], (float4)(lights[lightIndex].Position, 1.0));
            //inFrustum = inFrustum && (d >= -0.01); // lights[lightIndex].SqrRadius);
			float dist = dot(frustumPlanes[i], (float4)(lights[lightIndex].Position, 1.0));
			inFrustum = inFrustum && (-100.0 <= dist);

			//inFrustum = inFrustum && PlaneIntersectsSphere(frustumPlanes[i], lights[lightIndex].Position, 0.2); 
			//inFrustum = inFrustum && PlaneIntersectsPoint(frustumPlanes[i], lights[lightIndex].Position); 

			//inFrustum = true; 
        }

		{
			float dist = dot(frustumPlanes[4], (float4)(lights[lightIndex].Position, 1.0));
			inFrustum = inFrustum && (-100.0 >= dist);
		}

		{
			float dist = dot(frustumPlanes[5], (float4)(lights[lightIndex].Position, 1.0));
			inFrustum = inFrustum && (-100.0 >= dist);
		}

        if (inFrustum == true) 
		{
            // Append light to list
            // Compaction might be better if we expect a lot of lights
            unsigned int listIndex = atomic_inc(&sTileNumLights);
            
            sTileLightIndices[listIndex] = lightIndex;
        }
    }
	
	barrier (CLK_LOCAL_MEM_FENCE);
		
	unsigned int numLights = sTileNumLights;

	float lightsColor = clamp((float)numLights, 0.0, 8.0) / 8.0; 

	int2 destCoord = (int2)(x, y);

	//if (maxTileZ > 0) {

	//write_imagef(lightingMap, destCoord, (float4)(lightsColor, 0.0, 0.0, 1.0)); // lightsColor
	//write_imagef(lightingMap, destCoord, (float4)((maxTileZ - minTileZ) * 0.01, 0.0, 0.0, 1.0)); // lightsColor
	//write_imagef(lightingMap, destCoord, (float4)((maxTileZ - minTileZ) * 100.0, 0.0, 0.0, 1.0)); // lightsColor
	//write_imagef(lightingMap, destCoord, (float4)(lightsColor, lightsColor, (maxDepthZ - minDepthZ) * 0.01, 1.0)); // lightsColor
	//write_imagef(lightingMap, destCoord, (float4)(lightsColor, lightsColor, lightsColor, 1.0)); // lightsColor
		
	write_imagef(lightingMap, destCoord, (float4)(lightsColor, lightsColor, lightsColor, 1.0)); // lightsColor		
	/* 
	if (minDepthZ < 0) 
	{
		write_imagef(lightingMap, destCoord, (float4)(-minDepthZ * 0.001, 0, 0, 1.0)); // lightsColor		
		//write_imagef(lightingMap, destCoord, (float4)(-rawMaxDepthZ * 100.0, 0, 0, 1.0)); // lightsColor
	}
	else
	{
	
		write_imagef(lightingMap, destCoord, (float4)(0.0, minDepthZ * 0.001, 0, 1.0)); // lightsColor
	//write_imagef(lightingMap, destCoord, (float4)(lightsColor, lightsColor, lightsColor, 1.0)); // lightsColor
		//write_imagef(lightingMap, destCoord, (float4)(0, rawMaxDepthZ * 100.0, 0, 1.0)); // lightsColor
	}
	*/
	//}
	//else { 
	//	write_imagef(lightingMap, destCoord, (float4)(0.0, 0.0, lightsColor, 1.0));
	//}
}
