﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.Deferred
{
	public class Deferred_Particles : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/Particles";

		private int m_Texture;
		private int m_TextureNormal;
		private int m_TextureTangent;

		private int uNormalMatrix;
		private int uNormalWorldMatrix;

		private int uObjectMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;

		#endregion

		public override string Name
		{
			get { return "Deferred: Particles"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Particles() { }

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix, 
							Texture2D texture, Texture2D normalTexture, VertexArrayObject vao, int instanceCount)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}
			
			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);

			GL.UniformMatrix4(uObjectMatrix, false, ref normalWorldMatrix);
			GL.UniformMatrix4(uObjectMatrix, false, ref normalWorldMatrix);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, texture);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, normalTexture);
            GLState.ClearTexture(TextureUnit.Texture2, TextureTarget.Texture2D);
			
			vao.Bind();

			GL.DrawArraysInstanced(PrimitiveType.TriangleStrip, 0, 4, instanceCount);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			vao.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Texture = GL.GetUniformLocation(ProgramHandle, "flatTexture");
			m_TextureNormal = GL.GetUniformLocation(ProgramHandle, "textureNormal");
			m_TextureTangent = GL.GetUniformLocation(ProgramHandle, "textureTangent");

			GL.Uniform1(m_Texture, 0);
			GL.Uniform1(m_TextureNormal, 1);
			GL.Uniform1(m_TextureTangent, 2);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");		

			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			//GL.BindAttribLocation(ProgramHandle, (int)UIVertex.Elements.Position, "pos");

			//GL.BindAttribLocation(ProgramHandle, (int)UIVertex.Elements.TextureCoords, "tex");

			//GL.BindAttribLocation(ProgramHandle, (int)UIVertex.Elements.Color, "col");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
