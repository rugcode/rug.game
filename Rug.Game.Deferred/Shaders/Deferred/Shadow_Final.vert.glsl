#version 420

uniform float tanHalfFov; 
uniform float aspectRatio; 

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;

// tanHalfFov_ = sml::tan(fov_ / 2.0f);

out smooth vec2 uv_var;
out smooth mat3 tangentViewMatrix;
out noperspective vec3 viewRay; 

void main()
{  
	gl_Position = vec4(position, 1);
	// uv_var = position.xy * 0.5 + 0.5; 
	// 
	uv_var = uv;	
	vec3 normal = vec3(0.0, 0.0, -1.0);
	vec3 tangent = vec3(1.0, 0.0, 0.0);
	vec3 bitangent = vec3(0.0, 1.0, 0.0);
	tangentViewMatrix = mat3(tangent, bitangent, normal);

	viewRay = vec3(
//		(uv_var.x * 2.0 - 1.0) * tanHalfFov * aspectRatio,
//		(uv_var.y * 2.0 - 1.0) * tanHalfFov, 
		position.x * tanHalfFov * aspectRatio,
		position.y * tanHalfFov, 
		1.0
	);
}
