#version 410

uniform sampler2D textureDiffuse;

//in vec3 normal_var;
in vec2 uv_var;
//in vec3 tangent_var;
//in vec3 bitangent_var;
in vec4 color_var;
//in vec4 material_var;

layout(location = 0) out vec4 colorFrag;

void main()
{
	vec4 difColor = texture(textureDiffuse, uv_var);
	
	//colorFrag = vec4(1, 1, 1, 1); 
	colorFrag = vec4(difColor.rgb * color_var.rgb, difColor.a * color_var.a); //  // 
	
	//materialFrag = material_var; 
}