﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.Deferred
{
	public class Deferred_DiffuseTextured : BasicEffectBase
	{
		#region Private Members

		//private string m_ShaderLocation = @"~/Data/Shaders/Deferred/Color";
		private string m_ShaderLocation = @"~/Shaders/Deferred/DiffuseTextured";
		private int uObjectMatrix;
		private int uNormalMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uNormalWorldMatrix;
		private int uTextureDiffuse;
		private int uTextureNormal;
		private int uTextureMaterial;
		private int uMaterialLookup;

		#endregion

		public override string Name
		{
			get { return "Deferred: Diffuse Textured"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_DiffuseTextured(bool showBump) 
		{
			if (showBump == true)
			{
				Defines = new string[] { "ShowBump" }; 
			}
		}

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorldMatrix);
		}

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, int indexCount, DrawElementsType indexType, 
			Texture2D diffuse,
			Texture2D normal,
			Texture2D material,
			Texture2D materialLookup)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
			
			//GL.Uniform1(uTextureDiffuse, diffuse.ResourceHandle);
			//GL.Uniform1(uTextureNormal, normal.ResourceHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, diffuse);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, normal);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, material);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, materialLookup);

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");			
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");

			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			uTextureDiffuse = GL.GetUniformLocation(ProgramHandle, "textureDiffuse");
			uTextureNormal = GL.GetUniformLocation(ProgramHandle, "textureNormal");
			uTextureMaterial = GL.GetUniformLocation(ProgramHandle, "textureMaterial");
			uMaterialLookup = GL.GetUniformLocation(ProgramHandle, "materialLookup");

			GL.Uniform1(uTextureDiffuse, 0);
			GL.Uniform1(uTextureNormal, 1);
			GL.Uniform1(uTextureMaterial, 2);
			GL.Uniform1(uMaterialLookup, 3);
			
			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Position, "position");

			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Normal, "normal");
			
			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Tangent, "tangent");

			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.TextureCoords, "uv");

			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Color, "color");
			
			GL.BindAttribLocation(ProgramHandle, (int)MeshVertex.Elements.Material, "material");
			*/

			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");

			GL.BindFragDataLocation(ProgramHandle, 1, "normalFrag");

			GL.BindFragDataLocation(ProgramHandle, 2, "materialFrag");

			//GL.BindFragDataLocation(ProgramHandle, 3, "depthFrag");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

	}
}
