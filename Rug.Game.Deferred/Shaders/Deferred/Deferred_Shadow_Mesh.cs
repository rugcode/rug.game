﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;

namespace Rug.Game.Shaders.Deferred
{
	public class Deferred_Shadow_Mesh : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/Shadow_Write";

		private Matrix4 m_WorldMatrix;
		private Matrix4 m_PerspectiveMatrix; 

		private int uDepthMVP;		

		#endregion

		public override string Name
		{
			get { return "Deferred: Shadow Mesh"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Shadow_Mesh() { }

		public void Begin(Matrix4 worldMatrix, Matrix4 perspectiveMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			m_WorldMatrix = worldMatrix;
			m_PerspectiveMatrix = perspectiveMatrix;			
		}

		public void Render(ref Matrix4 depthMVP, int indexCount, DrawElementsType indexType)
		{
			//Matrix4 depthMVP = m_PerspectiveMatrix * m_WorldMatrix * objectMatrix;

			GL.UniformMatrix4(uDepthMVP, false, ref depthMVP);

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uDepthMVP = GL.GetUniformLocation(ProgramHandle, "depthMVP");			

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			GL.BindFragDataLocation(ProgramHandle, 0, "fragmentdepth");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
