﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.Deferred
{
	public class Deferred_Decal_Color : BasicEffectBase
	{
		#region Private Members

		//private string m_ShaderLocation = @"~/Data/Shaders/Deferred/Color";
		private string m_ShaderLocation = @"~/Shaders/Deferred/Decal_Color";
		private int uObjectMatrix;
		private int uNormalMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uNormalWorldMatrix;
		private int uTextureDiffuse;

		#endregion

		public override string Name
		{
			get { return "Deferred: Decal Color"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Decal_Color() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorldMatrix);
		}

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, int indexCount, DrawElementsType indexType, Texture2D diffuse)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, diffuse); 

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");			
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");

			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			uTextureDiffuse = GL.GetUniformLocation(ProgramHandle, "textureDiffuse");

			GL.Uniform1(uTextureDiffuse, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
