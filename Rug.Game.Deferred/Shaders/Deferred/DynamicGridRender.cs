﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Data;

namespace Rug.Game.Shaders.Terrain
{
	public class DynamicGridRender : BasicEffectBase 
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Deferred/DynamicGridRender";
		private int uDepthMVP;
		private int uHeights;

		#endregion

		public override string Name
		{
			get { return "DynamicGridRender"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public DynamicGridRender() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);
		}

		public void Render(View3D view, 
							ref Matrix4 depthMVP, 
							VertexBuffer verts, int count,	
							Texture2D heights)
		{
			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uDepthMVP, false, ref depthMVP);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, heights);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			DynamicGridVert.Bind();
			//GLState.CullFace(CullFaceMode.Front);
			//GLState.EnableCullFace = true;
			//GLState.EnableDepthTest = true;
			//GLState.EnableDepthMask = true;
			GLState.Apply(view); 

			GL.DrawArrays(PrimitiveType.Triangles, 0, count);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			DynamicGridVert.Unbind();

		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uDepthMVP = GL.GetUniformLocation(ProgramHandle, "depthMVP");
			uHeights = GL.GetUniformLocation(ProgramHandle, "heights");
			
			GL.Uniform1(uHeights, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			GL.BindFragDataLocation(ProgramHandle, 0, "fragmentdepth");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
