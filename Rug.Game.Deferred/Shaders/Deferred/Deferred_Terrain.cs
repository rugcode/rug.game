﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;


namespace Rug.Game.Shaders.Deferred
{	
	public class Deferred_Terrain : BasicEffectBase
	{
		#region Private Members

		//private string m_ShaderLocation = @"~/Data/Shaders/Deferred/Color";
		private string m_ShaderLocation = @"~/Shaders/Deferred/Terrain";
		private int uObjectMatrix;
		private int uNormalMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
		private int uNormalWorldMatrix;
		private int uTextureNormal;
		private int uTextureTangent;
		private int uSurfaceDiffuse;
		private int uSurfaceNormal;
		private int uSurfaceSpec;

		#endregion

		public override string Name
		{
			get { return "Deferred: Terrain"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Deferred_Terrain() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Matrix4 normalWorldMatrix, 
						  Texture2D normalMap, Texture2D tangentMap,
						  Texture2D surfaceDiffuse, Texture2D surfaceNormal, Texture2D surfaceSpec)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uNormalWorldMatrix, false, ref normalWorldMatrix);

			
            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, normalMap);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, tangentMap);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, surfaceDiffuse);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, surfaceNormal);
            GLState.BindTexture(TextureUnit.Texture4, TextureTarget.Texture2D, surfaceSpec);
			
			Matrix4 ident = Matrix4.CreateRotationX(0);

			Matrix4 normal = ident;

			normal.Invert();
			normal.Transpose();

			GL.UniformMatrix4(uObjectMatrix, false, ref ident);
			GL.UniformMatrix4(uNormalMatrix, false, ref normal);
		}

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, int indexCount, DrawElementsType indexType)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);

			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void Render(int indexCount, DrawElementsType indexType)
		{
			GL.DrawElements(PrimitiveType.Triangles, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");
			uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");

			uTextureNormal = GL.GetUniformLocation(ProgramHandle, "textureNormal");
			uTextureTangent = GL.GetUniformLocation(ProgramHandle, "textureTangent");

			uSurfaceDiffuse = GL.GetUniformLocation(ProgramHandle, "surfaceDiffuse");
			uSurfaceNormal = GL.GetUniformLocation(ProgramHandle, "surfaceNormal");
			uSurfaceSpec = GL.GetUniformLocation(ProgramHandle, "surfaceSpec");

			GL.Uniform1(uTextureNormal, 0);
			GL.Uniform1(uTextureTangent, 1);

			GL.Uniform1(uSurfaceDiffuse, 2);
			GL.Uniform1(uSurfaceNormal, 3);
			GL.Uniform1(uSurfaceSpec, 4);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.Position, "position");

			//GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.Normal, "normal");

			GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.Color, "color");

			GL.BindAttribLocation(ProgramHandle, (int)TerrainVertex.Elements.TextureCoords, "textureCoords");
			*/ 

			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");

			GL.BindFragDataLocation(ProgramHandle, 1, "normalFrag");

			GL.BindFragDataLocation(ProgramHandle, 2, "materialFrag");
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
