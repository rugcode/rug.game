﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Water_Flow : BasicEffectBase
	{
		private int uFlowOut;		
		private int uWater;
		private int uFlow;
		
		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Water_Flow"; } }
		
		public override string Name
		{
			get { return "Terrain: Water Flow"; }
		}

		public Terrain_Water_Flow()
		{
			Defines = new string[] { }; 
		}

		public void Render(View3D view, VertexBuffer verts, Texture2D water, Texture2D flow, Terrain_Process flowOut)
		{
			flowOut.Begin(view);

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, water);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, flow);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			flowOut.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uWater = GL.GetUniformLocation(ProgramHandle, "water");
			uFlow = GL.GetUniformLocation(ProgramHandle, "flows");

			GL.Uniform1(uWater, 0);
			GL.Uniform1(uFlow, 1);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}
	}
}
