﻿#version 430 core
#define WORK_GROUP_SIZE 32
#define IMAGE_SIZE 512

uniform sampler2D ground, water;
uniform vec2 axis;

vec2 viewport = vec2(IMAGE_SIZE, IMAGE_SIZE); 

out vec4 colorFrag;

vec2 exchange(vec4 g1, float x, float y)
{
    vec2 uv = (gl_FragCoord.xy+vec2(x, y))/viewport;
    vec4 g2 = texture2D(ground, uv);
    float diff = (g2.x-g1.x)/4.0;
    float sdiff, gdiff;
    sdiff = clamp(diff, -g1.z/2.0, g2.z/2.0)*0.08;
    if(abs(diff) > 0.001){
        gdiff = diff*0.001;
    }
    else{
        gdiff = 0.0;
    }
    return vec2(gdiff, sdiff);
}

void main(void){
    vec2 uv = gl_FragCoord.xy/viewport;
    vec4 g = texture2D(ground, uv);
    vec2 c = (
        exchange(g,  1.0,  0.0) +
        exchange(g, -1.0,  0.0) +
        exchange(g,  0.0,  1.0) +
        exchange(g,  0.0, -1.0)
    );
    colorFrag = g + vec4(0.0, c, 0.0);
}