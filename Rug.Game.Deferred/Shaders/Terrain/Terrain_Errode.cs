﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;


namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Errode : BasicEffectBase
	{
		private int uGroundOut;
		private int uGround;
		private int uWater;

		private int uFactor;

		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Errode"; } }
		
		public override string Name
		{
			get { return "Terrain: Errode"; }
		}

		public Terrain_Errode()
		{
			Defines = new string[] { }; 
		}

		public void Render(View3D view, VertexBuffer verts, float factor, Texture2D ground, Texture2D water, Terrain_Process groundOut)
		{
			groundOut.Begin(view);

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, ground);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, water);

			GL.Uniform1(uFactor, factor);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			groundOut.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uGround = GL.GetUniformLocation(ProgramHandle, "ground");
			uWater = GL.GetUniformLocation(ProgramHandle, "water");

			uFactor = GL.GetUniformLocation(ProgramHandle, "factor");

			GL.Uniform1(uGround, 0);
			GL.Uniform1(uWater, 1);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}
	}
}
