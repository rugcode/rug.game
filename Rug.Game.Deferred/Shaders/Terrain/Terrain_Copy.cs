﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;


namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Copy : BasicEffectBase
	{
		private int uDest;
		private int uSource;
		private int uTextSize;
		private bool m_Times4; 

		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Copy"; } }
		
		public override string Name
		{
			get { return "Terrain: Copy" + (m_Times4 ? " (Times4)" : ""); }
		}

		public Terrain_Copy(bool times4)
		{
			m_Times4 = times4;

			if (m_Times4 == true)
			{
				Defines = new string[] { "Times4" };
			}
			else 
			{
				Defines = new string[] { };
			}
		}

		public void Render(View3D view, VertexBuffer verts, Texture2D source, Terrain_Process dest)
		{
			dest.Begin(view); 

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, source);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			dest.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uSource = GL.GetUniformLocation(ProgramHandle, "source");
		
			GL.Uniform1(uSource, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}
	}
}
