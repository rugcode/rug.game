﻿#version 410

uniform mat4 proj;
uniform mat4 view;
uniform sampler2D heights;
uniform sampler2D water_heights;

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;
layout(location = 2) in vec3 barycentric;
layout(location = 3) in vec4 cell_uv;

out float h;
out vec2 uv;
out vec4 v_position;
out vec4 v_cell_uv;

void main()
{
    float h1 = texture2D(heights, texcoord).x;
    float h2 = texture2D(water_heights, texcoord).x;

    h = h2;

    //vec4 pos = view * vec4(position.x, h1 + h2 - 0.00000, -position.y, 1.0);
	vec4 pos = view * vec4(position.x, h1 + h2 - 0.000001, -position.y, 1.0);
    
	gl_Position = proj * view * vec4(position.x, h1 + h2 - 0.000001, -position.y, 1.0);

    uv = texcoord;
    v_position = pos;
    v_cell_uv = cell_uv;
}