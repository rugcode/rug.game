﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Data.Terrain;

namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Render : BasicEffectBase 
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Terrain/Terrain_Render";
		private int uObjectMatrix;
		private int uView;
		private int uProj;
		private int uHeights;
		private int uNormals;
		private int uWater;
		private int uRock;
		private int uRockNormals;
		private int uGrass;
		private int uGrassNormals;
		private int uInvRot;
		private int uInvProj;
		private int uInvView;
		private int uMousePos;
		private int uEditSize;

		#endregion

		public override string Name
		{
			get { return "Terrain: Render"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Terrain_Render() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uView, false, ref worldMatrix);
			GL.UniformMatrix4(uProj, false, ref perspectiveMatrix);
		}
		
		public void Render(View3D view3D, 
							ref Matrix4 proj, ref Matrix4 view, VertexBuffer verts, int count,
							float editsize, Vector2 mousepos, 		
							Texture2D heights, 
							Texture2D normals,
							Texture2D water,
							Texture2D rock,
							Texture2D rockNormals,
							Texture2D grass,
							Texture2D grassNormals)
		{
			GL.UseProgram(ProgramHandle);
			
			GL.UniformMatrix4(uProj, false, ref proj);

			GL.UniformMatrix4(uView, false, ref view);

			Matrix4 invView = view;
			invView.Invert();

			Matrix4 invProj = proj;
			invProj.Invert();

			Matrix3 invRot = From(invView);

			GL.UniformMatrix3(uInvRot, false, ref invRot);
			GL.UniformMatrix4(uInvProj, false, ref invProj);
			GL.UniformMatrix4(uInvView, false, ref invView);

			GL.Uniform2(uMousePos, mousepos);
			GL.Uniform1(uEditSize, editsize);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, heights);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, normals);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, water);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, rock);
            GLState.BindTexture(TextureUnit.Texture4, TextureTarget.Texture2D, rockNormals);
            GLState.BindTexture(TextureUnit.Texture5, TextureTarget.Texture2D, grass);
            GLState.BindTexture(TextureUnit.Texture6, TextureTarget.Texture2D, grassNormals);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			HexGridVert.Bind();
			GLState.CullFace(CullFaceMode.Front);
			GLState.EnableCullFace = true;
			GLState.EnableDepthTest = true;
			GLState.EnableDepthMask = true;
			GLState.ApplyAll(view3D); 

			GL.DrawArrays(PrimitiveType.Triangles, 0, count);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			HexGridVert.Unbind();

		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uView = GL.GetUniformLocation(ProgramHandle, "view");
			uProj = GL.GetUniformLocation(ProgramHandle, "proj");

			uHeights = GL.GetUniformLocation(ProgramHandle, "heights");
			uNormals = GL.GetUniformLocation(ProgramHandle, "normals");
			uWater = GL.GetUniformLocation(ProgramHandle, "water");
			uRock = GL.GetUniformLocation(ProgramHandle, "rock");
			uRockNormals = GL.GetUniformLocation(ProgramHandle, "rock_normals");
			uGrass = GL.GetUniformLocation(ProgramHandle, "grass");
			uGrassNormals = GL.GetUniformLocation(ProgramHandle, "grass_normals");

			uEditSize = GL.GetUniformLocation(ProgramHandle, "editsize");
			uMousePos = GL.GetUniformLocation(ProgramHandle, "mousepos");

			uInvRot = GL.GetUniformLocation(ProgramHandle, "inv_rot");
			uInvProj = GL.GetUniformLocation(ProgramHandle, "inv_proj");
			uInvView = GL.GetUniformLocation(ProgramHandle, "inv_view");

			GL.Uniform1(uHeights, 0);
			GL.Uniform1(uNormals, 1);
			GL.Uniform1(uWater, 2);
			GL.Uniform1(uRock, 3);
			GL.Uniform1(uRockNormals, 4);
			GL.Uniform1(uGrass, 5);
			GL.Uniform1(uGrassNormals, 6);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)DebugVertex.Elements.Position, "position");

			GL.BindAttribLocation(ProgramHandle, (int)DebugVertex.Elements.Color, "color");
			*/

			// heights, waterHeights, normals, detailNormals, rock_normals, grass, grass_normals
			// proj, view

			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");
			
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

		private Matrix3 From(Matrix4 mat)
		{
			mat.Invert();
			mat.Transpose();

			float a0 = mat.M11, b0 = mat.M12, c0 = mat.M13;
			float a1 = mat.M21, b1 = mat.M22, c1 = mat.M23;
			float a2 = mat.M31, b2 = mat.M32, c2 = mat.M33;

			return new Matrix3(
				a0, b0, c0,
				a1, b1, c1,
				a2, b2, c2
			);
		}
	}
}
