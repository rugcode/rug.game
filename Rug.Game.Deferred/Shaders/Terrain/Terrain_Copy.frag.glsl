﻿#version 430 core
#define IMAGE_SIZE 512
#define WORK_GROUP_SIZE 32

uniform sampler2D source; 

in vec4 output_col;
in vec2 output_tex;

out vec4 colorFrag;

void main()
{
    colorFrag = texture(source, output_tex);
}