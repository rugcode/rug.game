﻿#version 430 core
#define WORK_GROUP_SIZE 32
#define IMAGE_SIZE 512

vec2 viewport = vec2(IMAGE_SIZE, IMAGE_SIZE); 

out vec4 colorFrag;

uniform sampler2D heights, source;
uniform vec2 axis;

vec3 exchange(float t1, float h1, vec2 off){
    vec2 uv = (gl_FragCoord.xy+off)/viewport;
    float t2 = texture2D(heights, uv).x;
    float h2 = texture2D(source, uv).x;
    float f1 = t1+h1;
    float f2 = t2+h2;
    float diff = (f2-f1)/2.0;
    diff = clamp(diff*0.65, -h1/2.0, h2/2.0);
    return vec3(diff, -off*diff);
}

void main(void){
    vec2 uv = gl_FragCoord.xy/viewport;
    float t = texture2D(heights, uv).x;
    vec3 h = texture2D(source, uv).xyz;
    vec3 r = h + exchange(t, h.x, axis) + exchange(t, h.x, -axis);
    colorFrag = vec4(r, 1.0);
}
