﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;

namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Process : IResourceManager
	{
		private FrameBuffer m_FrameBuffer;
		private bool m_Disposed = true;

		#region Properties

		public Viewport Viewport { get; set; }

		public int Size { get; set; }		

		public bool Linear { get; set; }

		public FrameBufferTexture2D ColorTexture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0Ext]; } }

		public bool Disposed
		{
			get { return m_Disposed; }
		}

        public int Count { get { return 1; } }

        public int Total { get { return 1; } }

		#endregion

		public Terrain_Process(int size, bool linear)
		{
			Size = size;
			Linear = linear; 

			this.Viewport = new Viewport(0, 0, Size, Size);

			Initiate(); 
		}

		public void Initiate()
		{
			PixelInternalFormat internalFormat = PixelInternalFormat.Rgba32f; // PixelInternalFormat.Rgba16f; 
			PixelType pixelType = pixelType = PixelType.Float; // PixelType.HalfFloat; 

			m_FrameBuffer = new FrameBuffer("Meshes Buffer", ResourceMode.Static,
				new FrameBufferInfo(
					new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
					{
						Size = new System.Drawing.Size(Size, Size),
						InternalFormat = internalFormat,
						PixelFormat = PixelFormat.Rgba,
						PixelType = pixelType,
						MagFilter = Linear ? TextureMagFilter.Linear : TextureMagFilter.Nearest,
						MinFilter = Linear ? TextureMinFilter.Linear : TextureMinFilter.Nearest,
						WrapS = TextureWrapMode.Clamp, // TextureWrapMode.Repeat,
						WrapT = TextureWrapMode.Clamp, // TextureWrapMode.Repeat,
					}));
		}

		public void Begin(View3D view)
		{
			GLState.Viewport = Viewport;

			m_FrameBuffer.Bind();

			GLState.Apply(Size, Size);
		}

		public void End()
		{
			m_FrameBuffer.Unbind();
		}

		#region IResource Members

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				this.Viewport = new Viewport(0, 0, Size, Size);

				System.Drawing.Size size = new System.Drawing.Size(Size, Size);

				m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = size;

				m_FrameBuffer.LoadResources();

				m_Disposed = false; 
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop(); 
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_FrameBuffer.UnloadResources();

				m_Disposed = true; 
			}
		}

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
