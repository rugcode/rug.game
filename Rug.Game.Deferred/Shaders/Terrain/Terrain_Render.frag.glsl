﻿#version 410

uniform sampler2D heights;
uniform sampler2D normals;
//uniform sampler2D occlusions;
//uniform sampler2D shadowmap;
uniform sampler2D water;

uniform sampler2D rock;
uniform sampler2D rock_normals;
uniform sampler2D grass;
uniform sampler2D grass_normals;

uniform vec2 mousepos;
//uniform vec2 viewport;
uniform mat4 inv_proj;
uniform mat4 inv_view;
uniform mat3 inv_rot;
uniform float editsize;

in vec2 uv;
in vec3 bc;
in vec3 v_position;

layout(location = 0) out vec4 colorFrag;


vec4 cubic(float x)
{
    float x2 = x * x;
    float x3 = x2 * x;
    vec4 w;
    w.x =   -x3 + 3*x2 - 3*x + 1;
    w.y =  3*x3 - 6*x2       + 4;
    w.z = -3*x3 + 3*x2 + 3*x + 1;
    w.w =  x3;
    return w / 6.f;
}

vec4 filter(sampler2D text, vec2 texcoord, vec2 texscale)
{
    float fx = fract(texcoord.x);
    float fy = fract(texcoord.y);
    texcoord.x -= fx;
    texcoord.y -= fy;

    vec4 xcubic = cubic(fx);
    vec4 ycubic = cubic(fy);

    vec4 c = vec4(texcoord.x - 0.5, texcoord.x + 1.5, texcoord.y - 0.5, texcoord.y + 1.5);
    vec4 s = vec4(xcubic.x + xcubic.y, xcubic.z + xcubic.w, ycubic.x + ycubic.y, ycubic.z + ycubic.w);
    vec4 offset = c + vec4(xcubic.y, xcubic.w, ycubic.y, ycubic.w) / s;

    vec4 sample0 = texture2D(text, vec2(offset.x, offset.z) * texscale);
    vec4 sample1 = texture2D(text, vec2(offset.y, offset.z) * texscale);
    vec4 sample2 = texture2D(text, vec2(offset.x, offset.w) * texscale);
    vec4 sample3 = texture2D(text, vec2(offset.y, offset.w) * texscale);

    float sx = s.x / (s.x + s.y);
    float sy = s.z / (s.z + s.w);

    return mix(
        mix(sample3, sample2, sx),
        mix(sample1, sample0, sx), sy);
}
    
vec3 get_world_normal(vec2 coord)
{
    vec2 frag_coord = coord; // / viewport;

    frag_coord = (frag_coord - 0.5) * 2.0;

    vec4 device_normal = vec4(frag_coord, 0.0, 1.0);
    vec3 eye_normal = normalize((inv_proj * device_normal).xyz);
    vec3 world_normal = normalize(inv_rot * eye_normal);

    return world_normal;
}

struct SHC
{
    vec3 L00, L1m1, L10, L11, L2m2, L2m1, L20, L21, L22;
};

SHC beach = SHC(
    vec3( 0.6841148,  0.6929004,  0.7069543),
    vec3( 0.3173355,  0.3694407,  0.4406839),
    vec3(-0.1747193, -0.1737154, -0.1657420),
    vec3(-0.4496467, -0.4155184, -0.3416573),
    vec3(-0.1690202, -0.1703022, -0.1525870),
    vec3(-0.0837808, -0.0940454, -0.1027518),
    vec3(-0.0319670, -0.0214051, -0.0147691),
    vec3( 0.1641816,  0.1377558,  0.1010403),
    vec3( 0.3697189,  0.3097930,  0.2029923)
);

vec3 shLight(vec3 normal, SHC l)
{
    float x = normal.x;
    float y = normal.y;
    float z = normal.z;

    const float C1 = 0.429043;
    const float C2 = 0.511664;
    const float C3 = 0.743125;
    const float C4 = 0.886227;
    const float C5 = 0.247708;

    return (
        C1 * l.L22 * (x * x - y * y) +
        C3 * l.L20 * z * z +
        C4 * l.L00 -
        C5 * l.L20 +
        2.0 * C1 * l.L2m2 * x * y +
        2.0 * C1 * l.L21  * x * z +
        2.0 * C1 * l.L2m1 * y * z +
        2.0 * C2 * l.L11  * x +
        2.0 * C2 * l.L1m1 * y +
        2.0 * C2 * l.L10  * z
    );
}
    
vec3 Kr = vec3(0.18867780436772762, 0.4978442963618773, 0.6616065586417131); // air

vec3 absorb(float dist, vec3 color, float factor)
{
    return color - color * pow(Kr, vec3(factor / dist));
}
   
void main()
{
    vec3 base_normal = normalize(texture2D(normals, uv).xyz);
    vec3 tangent = normalize(cross(base_normal, vec3(0.0, 0.0, 1.0)));
    vec3 bitangent = normalize(cross(tangent, base_normal));
    mat3 orthobasis = mat3(tangent, base_normal, bitangent);

    vec3 mousevec = get_world_normal(mousepos);
    vec4 eyepos = inv_view * vec4(0.0, 0.0, 0.0, 1.0);
    float u = dot(vec3(0.0, 1.0, 0.0), -eyepos.xyz) / dot(vec3(0.0, 1.0, 0.0), mousevec);
    vec3 intersection = eyepos.xyz + mousevec * u;
    float dist = distance((inv_view * vec4(v_position, 1.0)).xz, intersection.xz) * pow(editsize, 3.0);
    vec3 selection;
    
	if(dist > mix(0.99, 0.25, editsize / 20.0) && dist < 1.0)
	{
        selection = vec3(0.12, 0.92, 0.0);
    }
    else
	{
        selection = vec3(1.0, 1.0, 1.0);
    }
    
    vec3 w = filter(water, uv * 512.0, vec2(1.0 / 512.0)).xyz; // texture2D(water, uv).xyz;
    float rock_factor = 20.0;
    //vec3 rock_color = pow(texture2D(rock, uv*rock_factor).rgb, vec3(0.5)) * 1.5;
    vec3 rock_color = texture2D(rock, uv * rock_factor).rgb * 0.8;
    vec3 rock_normal = orthobasis * normalize((texture2D(rock_normals, uv * rock_factor).xyz - 0.5) * vec3(2.0, 3.0, 2.0));
        
    float grass_factor = 8.0;
    vec3 grass_color = texture2D(grass, uv * grass_factor).rgb;
    vec3 grass_normal = orthobasis * normalize((texture2D(grass_normals, uv * grass_factor).xyz - 0.5) * vec3(2.0, 1.0, 2.0));

    vec3 dirt = vec3(85.0 / 255.0, 34.0 / 255.0, 0.0);
    vec3 soil = mix(grass_color, dirt, sqrt(clamp(w.x / 0.0003 + length(w.yz) / 0.015, 0.0, 1.0)));
        
    vec4 ground = filter(heights, uv * 512.0, vec2(1.0 / 512.0)); // texture2D(heights, uv);
    float mix_factor = clamp(ground.z * 500.0, 0.0, 1.0);
    vec3 color = mix(rock_color, soil, mix_factor).xyz;
    vec3 normal = mix(rock_normal, grass_normal, mix_factor).xyz;

    //float occlusion = mix(0.0, 1.0, texture2D(occlusions, uv).x);
    //float shadowed = mix(0.3, 1.0, texture2D(shadowmap, uv).x);
    vec3 diffuse = shLight(normal, beach);
    
	//vec3 exident = diffuse * occlusion * shadowed * color;
	vec3 exident = diffuse * color;
    
	float d = length(v_position);
    
	//vec3 incident = absorb(d, exident * selection, 2.5) + pow(Kr * d * 0.7, vec3(2.0));
	//vec3 incident = absorb(d, exident, 2.5) + pow(Kr * d * 0.7, vec3(2.0));
	vec3 incident = absorb(d, exident * selection, 2.5) + pow(Kr * d * 0.05, vec3(2.0));
	//vec3 incident = exident;

    colorFrag = vec4(pow(incident, vec3(1.0 / 1.8)), 1.0);
	//colorFrag = vec4(base_normal * 0.5 + 0.5, 1.0);
	//colorFrag = vec4(1.0, 0.0, 0.5, 1.0);
	//colorFrag = ground;
	//colorFrag = vec4(w, 1.0);
}
