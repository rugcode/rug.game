﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;


namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Water_Cycle : BasicEffectBase
	{
		private int uWaterOut;
		private int uGround;
		private int uWater;

		private int uRain;
		private int uEvaporate;
		private int uInvRot;
		private int uInvProj;
		private int uInvView;
		private int uCreate;
		private int uEditSize;
		private int uMousePos;

		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Water_Cycle"; } }
		
		public override string Name
		{
			get { return "Terrain: Water Cycle"; }
		}

		public Terrain_Water_Cycle()
		{
			Defines = new string[] { }; 
		}

		public void Render(View3D view, VertexBuffer verts, 
			float rain, float evaporate, float create, float editsize, Vector2 mousepos, 
			Texture2D water, Terrain_Process waterOut)
		{
			waterOut.Begin(view);

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, water);

			Matrix4 proj = view.Projection;
			Matrix4 viewMat = view.View;

			Matrix4 invView = viewMat;
			invView.Invert();

			Matrix4 invProj = proj;
			invProj.Invert();

			Matrix3 invRot = From(invView);

			GL.UniformMatrix3(uInvRot, false, ref invRot);
			GL.UniformMatrix4(uInvProj, false, ref invProj);
			GL.UniformMatrix4(uInvView, false, ref invView);

			GL.Uniform2(uMousePos, mousepos);
			GL.Uniform1(uRain, rain);
			GL.Uniform1(uEvaporate, evaporate);
			GL.Uniform1(uCreate, create);
			GL.Uniform1(uEditSize, editsize);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			waterOut.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uWater = GL.GetUniformLocation(ProgramHandle, "water");

			uRain = GL.GetUniformLocation(ProgramHandle, "rain");
			uEvaporate = GL.GetUniformLocation(ProgramHandle, "evaporate");
			uCreate = GL.GetUniformLocation(ProgramHandle, "create");
			uEditSize = GL.GetUniformLocation(ProgramHandle, "editsize");
			uMousePos = GL.GetUniformLocation(ProgramHandle, "mousepos");

			uInvRot = GL.GetUniformLocation(ProgramHandle, "inv_rot");
			uInvProj = GL.GetUniformLocation(ProgramHandle, "inv_proj");
			uInvView = GL.GetUniformLocation(ProgramHandle, "inv_view");

			GL.Uniform1(uWater, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}

		private Matrix3 From(Matrix4 mat)
		{
			mat.Invert();
			mat.Transpose();

			float a0 = mat.M11, b0 = mat.M12, c0 = mat.M13;
			float a1 = mat.M21, b1 = mat.M22, c1 = mat.M23;
			float a2 = mat.M31, b2 = mat.M32, c2 = mat.M33;

			return new Matrix3(
				a0, b0, c0,
				a1, b1, c1,
				a2, b2, c2
			);
		}
	}
}
