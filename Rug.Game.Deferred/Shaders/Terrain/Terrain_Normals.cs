﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;


namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Normals : BasicEffectBase
	{
		private int uNormalOut;
		private int uGround;

		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Normals"; } }
		
		public override string Name
		{
			get { return "Terrain: Normals"; }
		}

		public Terrain_Normals()
		{
			Defines = new string[] { }; 
		}

		public void Render(View3D view, VertexBuffer verts, Texture2D ground, Terrain_Process normalOut)
		{
			normalOut.Begin(view);

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, ground);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			normalOut.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uGround = GL.GetUniformLocation(ProgramHandle, "ground");

			GL.Uniform1(uGround, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}
	}
}
