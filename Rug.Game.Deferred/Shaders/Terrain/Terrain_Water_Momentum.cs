﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Water_Momentum : BasicEffectBase
	{
		private int uDest;
		private int uCurrent;
		private int uLast;
		private int uGround;

		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Water_Momentum"; } }
		
		public override string Name
		{
			get { return "Terrain: Water Momentum"; }
		}

		public Terrain_Water_Momentum()
		{
			Defines = new string[] { }; 
		}

		public void Render(View3D view, VertexBuffer verts, Texture2D current, Texture2D last, Texture2D ground, Terrain_Process dest)
		{
			dest.Begin(view);

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, current);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, last);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, ground);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			dest.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uCurrent = GL.GetUniformLocation(ProgramHandle, "current");
			uLast = GL.GetUniformLocation(ProgramHandle, "last");
			uGround = GL.GetUniformLocation(ProgramHandle, "ground");

			GL.Uniform1(uCurrent, 0);
			GL.Uniform1(uLast, 1);
			GL.Uniform1(uGround, 2);

			GL.UseProgram(0); 
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}
	}
}
