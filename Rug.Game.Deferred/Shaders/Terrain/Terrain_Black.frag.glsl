﻿#version 430 core

#define WORK_GROUP_SIZE 32 

layout (binding = 0, rgba32f) uniform writeonly image2D dest;

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = 1) in;

void main()
{
	ivec2 position = ivec2(gl_GlobalInvocationID.xy);	

	imageStore(dest, position, vec4(0.0,0.0,0.0,0.0));
}
