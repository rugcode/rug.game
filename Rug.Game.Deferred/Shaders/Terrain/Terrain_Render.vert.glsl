﻿#version 410

uniform mat4 proj;
uniform mat4 view;
uniform sampler2D heights;
//uniform vec2 viewport;

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;
layout(location = 2) in vec3 barycentric;
    
out vec2 uv;
out vec3 bc;
out vec3 v_position;

const float pi = 3.1415926535897931;
const float pih = pi * 0.5;
               
void main()
{
    uv = texcoord;
    bc = barycentric;
    
	float y = texture2D(heights, texcoord).x;
    
	v_position = (view * vec4(position.x, y, -position.y, 1.0)).xyz;

    gl_Position = proj * view * vec4(position.x, y, -position.y, 1.0);
}
