﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;


namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Water_Diffuse : BasicEffectBase
	{
		private int uDest;
		private int uHeights;
		private int uSource;
		private int uAxis;

		public override string ShaderLocation { get { return "~/Shaders/Terrain/Terrain_Water_Diffuse"; } }
		
		public override string Name
		{
			get { return "Terrain: Water Diffuse"; }
		}

		public Terrain_Water_Diffuse()
		{
			Defines = new string[] { }; 
		}

		public void Render(View3D view, VertexBuffer verts, Vector2 axis, Texture2D heights, Texture2D source, Terrain_Process dest)
		{
			dest.Begin(view);

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, heights);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, source);

			GL.Uniform2(uAxis, ref axis);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			dest.End(); 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uHeights = GL.GetUniformLocation(ProgramHandle, "heights");
			uSource = GL.GetUniformLocation(ProgramHandle, "source");

			uAxis = GL.GetUniformLocation(ProgramHandle, "axis");

			GL.Uniform1(uHeights, 0);
			GL.Uniform1(uSource, 1);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			
		}

		protected override void OnUnloadResources()
		{
			
		}
	}
}
