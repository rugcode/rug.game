﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using Rug.Game.Deferred.Data.Terrain;

namespace Rug.Game.Shaders.Terrain
{
	public class Terrain_Water_Render : BasicEffectBase 
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Terrain/Terrain_Water_Render";
		private int uObjectMatrix;
		private int uView;
		private int uProj;

		private int uViewport;
		private int uInvRot;
		private int uInvProj;
		private int uInvView;
		private int uLightDir;

		private int uHeights;
		private int uWaterHeights;
		private int uNormals;
		private int uDetailNormals;
		private int uFlows;


		#endregion

		public override string Name
		{
			get { return "Terrain: Water Render"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Terrain_Water_Render() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uView, false, ref worldMatrix);
			GL.UniformMatrix4(uProj, false, ref perspectiveMatrix);
		}
		
		public void Render(View3D view3D, 
							ref Matrix4 proj, ref Matrix4 view, VertexBuffer verts, int count, 							
							Texture2D heights, 
							Texture2D waterHeights,
							Texture2D normals,
							Texture2D detailNormals,
							Texture2D flows)
		{
			                
			GL.UseProgram(ProgramHandle);
			
			GL.UniformMatrix4(uProj, false, ref proj);
			GL.UniformMatrix4(uView, false, ref view);

			Matrix4 invView = view; 
			invView.Invert();

			Matrix4 invProj = proj;
			invProj.Invert();

			Matrix3 invRot = From(invView);

			GL.UniformMatrix3(uInvRot, false, ref invRot);
			GL.UniformMatrix4(uInvProj, false, ref invProj);
			GL.UniformMatrix4(uInvView, false, ref invView);

			GL.Uniform2(uViewport, GLState.Viewport.Size);
			GL.Uniform3(uLightDir, ref view3D.LightDirection);

			/* 
			uView = GL.GetUniformLocation(ProgramHandle, "view");
			uProj = GL.GetUniformLocation(ProgramHandle, "proj");
			uInvRot = GL.GetUniformLocation(ProgramHandle, "inv_rot");
			uInvProj = GL.GetUniformLocation(ProgramHandle, "inv_proj");
			uInvView = GL.GetUniformLocation(ProgramHandle, "inv_view");

			uViewport = GL.GetUniformLocation(ProgramHandle, "viewport");
			uLightDir = GL.GetUniformLocation(ProgramHandle, "lightdir");
			 * 
			GL.Uniform1(uHeights, 0);
			GL.Uniform1(uWaterHeights, 1);
			GL.Uniform1(uNormals, 2);
			GL.Uniform1(uDetailNormals, 3);
			GL.Uniform1(uFlows, 4);
			*/

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, heights);
            GLState.BindTexture(TextureUnit.Texture1, TextureTarget.Texture2D, waterHeights);
            GLState.BindTexture(TextureUnit.Texture2, TextureTarget.Texture2D, normals);
            GLState.BindTexture(TextureUnit.Texture3, TextureTarget.Texture2D, detailNormals);
            GLState.BindTexture(TextureUnit.Texture4, TextureTarget.Texture2D, flows);

			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			GridVert.Bind();
			GLState.CullFace(CullFaceMode.Front);
			GLState.EnableCullFace = true;
			GLState.EnableDepthTest = true;
			GLState.EnableDepthMask = true;
			GLState.ApplyAll(view3D);


			//GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

			GL.DrawArrays(PrimitiveType.Triangles, 0, count);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			//GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

			GL.UseProgram(0);

			GridVert.Unbind();

		}

		private Matrix3 From(Matrix4 mat)
		{
			mat.Invert();
			mat.Transpose();

			float a0 = mat.M11, b0 = mat.M12, c0 = mat.M13;
			float a1 = mat.M21, b1 = mat.M22, c1 = mat.M23;
			float a2 = mat.M31, b2 = mat.M32, c2 = mat.M33;

			return new Matrix3(
                a0, b0, c0, 
                a1, b1, c1, 
                a2, b2, c2
            );
		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			/* 
			uniform mat4 proj;
			uniform mat4 view;
			uniform sampler2D heights;
			uniform sampler2D water_heights;

			uniform vec2 viewport; 
			uniform mat3 inv_rot;
			uniform mat4 inv_proj; 
			uniform mat4 inv_view;
			uniform vec3 lightdir; 

			//uniform float editsize;
			uniform sampler2D waterHeights;
			uniform sampler2D detail_normals;
			//uniform sampler2D shadowmap;
			uniform sampler2D flows;
			 */

			uView = GL.GetUniformLocation(ProgramHandle, "view");
			uProj = GL.GetUniformLocation(ProgramHandle, "proj");
			uInvRot = GL.GetUniformLocation(ProgramHandle, "inv_rot");
			uInvProj = GL.GetUniformLocation(ProgramHandle, "inv_proj");
			uInvView = GL.GetUniformLocation(ProgramHandle, "inv_view");

			uViewport = GL.GetUniformLocation(ProgramHandle, "viewport");
			uLightDir = GL.GetUniformLocation(ProgramHandle, "lightdir");

			uHeights = GL.GetUniformLocation(ProgramHandle, "heights");
			uWaterHeights = GL.GetUniformLocation(ProgramHandle, "water_heights");

			uNormals = GL.GetUniformLocation(ProgramHandle, "normals");		
			uDetailNormals = GL.GetUniformLocation(ProgramHandle, "detail_normals");
			uFlows = GL.GetUniformLocation(ProgramHandle, "flows");

			GL.Uniform1(uHeights, 0);
			GL.Uniform1(uWaterHeights, 1);
			GL.Uniform1(uNormals, 2);
			GL.Uniform1(uDetailNormals, 3);
			GL.Uniform1(uFlows, 4);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			/* 
			GL.BindAttribLocation(ProgramHandle, (int)DebugVertex.Elements.Position, "position");

			GL.BindAttribLocation(ProgramHandle, (int)DebugVertex.Elements.Color, "color");
			*/

			// heights, waterHeights, normals, detailNormals, rock_normals, grass, grass_normals
			// proj, view

			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");
			
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
