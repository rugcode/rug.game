﻿#version 430 core
#define WORK_GROUP_SIZE 32
#define IMAGE_SIZE 128

vec2 viewport = vec2(IMAGE_SIZE, IMAGE_SIZE); 

in vec2 output_tex;

out vec4 colorFrag;

uniform sampler2D water, flows;

void main(void){
   // vec2 uv = gl_FragCoord.xy/viewport;
    vec2 pos = texture2D(flows, output_tex).xy;
    vec3 w = texture2D(water, output_tex).xyz;
    vec2 vel = (w.yz*0.001)/(w.x*0.1+0.001);
    colorFrag = vec4(pos+vel, 0.0, 1.0);
}