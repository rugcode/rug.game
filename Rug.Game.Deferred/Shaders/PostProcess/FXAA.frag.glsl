﻿// FXAA shader, GLSL code adapted from:
// http://horde3d.org/wiki/index.php5?title=Shading_Technique_-_FXAA
// Whitepaper describing the technique:
// http://developer.download.nvidia.com/assets/gamedev/files/sdk/11/FXAA_WhitePaper.pdf
#version 410

uniform sampler2D textureSampler;

// The inverse of the texture dimensions along X and Y
uniform vec2 texcoordOffset;

in vec4 vertColor;
in vec2 vertTexcoord;

layout(location = 0) out vec4 colorFrag;

void main() 
{
  // The parameters are hardcoded for now, but could be
  // made into uniforms to control from the program.
  float FXAA_SPAN_MAX = 8.0;
  float FXAA_REDUCE_MUL = 1.0/8.0;
  float FXAA_REDUCE_MIN = (1.0/128.0);

  vec4 rgbNW = texture(textureSampler, vertTexcoord.xy + (vec2(-1.0, -1.0) * texcoordOffset));
  vec4 rgbNE = texture(textureSampler, vertTexcoord.xy + (vec2(+1.0, -1.0) * texcoordOffset));
  vec4 rgbSW = texture(textureSampler, vertTexcoord.xy + (vec2(-1.0, +1.0) * texcoordOffset));
  vec4 rgbSE = texture(textureSampler, vertTexcoord.xy + (vec2(+1.0, +1.0) * texcoordOffset));
  vec4 rgbM  = texture(textureSampler, vertTexcoord.xy);
	
  vec3 luma = vec3(0.299, 0.587, 0.114);
  float lumaNW = rgbNW.w; // dot(rgbNW, luma);
  float lumaNE = rgbNE.w; // dot(rgbNE, luma);
  float lumaSW = rgbSW.w; // dot(rgbSW, luma);
  float lumaSE = rgbSE.w; // dot(rgbSE, luma);
  float lumaM  = rgbM.w; // dot( rgbM, luma);
	
  float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
  float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));
	
  vec2 dir;
  dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
  dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));
	
  float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25 * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);
	  
  float rcpDirMin = 1.0/(min(abs(dir.x), abs(dir.y)) + dirReduce);
	
  dir = min(vec2(FXAA_SPAN_MAX,  FXAA_SPAN_MAX), 
        max(vec2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX), dir * rcpDirMin)) * texcoordOffset;
		
  vec3 rgbA = (1.0/2.0) * (
              texture(textureSampler, vertTexcoord.xy + dir * (1.0/3.0 - 0.5)).xyz +
              texture(textureSampler, vertTexcoord.xy + dir * (2.0/3.0 - 0.5)).xyz);
  vec3 rgbB = rgbA * (1.0/2.0) + (1.0/4.0) * (
              texture(textureSampler, vertTexcoord.xy + dir * (0.0/3.0 - 0.5)).xyz +
              texture(textureSampler, vertTexcoord.xy + dir * (3.0/3.0 - 0.5)).xyz);
  
  float lumaB = dot(rgbB, luma);


  if((lumaB < lumaMin) || (lumaB > lumaMax))
  {
    colorFrag.xyz=rgbA;
  } 
  else 
  {
    colorFrag.xyz=rgbB;
  } 

  colorFrag.a = 1.0;
    
  colorFrag *= vertColor;
}