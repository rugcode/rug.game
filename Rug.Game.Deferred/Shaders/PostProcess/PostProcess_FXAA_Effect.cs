﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Shaders.PostProcess
{
	public class PostProcess_FXAA_Effect : BasicEffectBase
	{
		#region Private Members
		
		private string m_ShaderLocation = @"~/Shaders/PostProcess/FXAA";

		private int m_Texture;
		private int uTexcoordOffset;

		#endregion

		public override string Name
		{
			get { return "Post Process: FXAA"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public PostProcess_FXAA_Effect() { }

		public void Render(Texture2D texture, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			//RC.WriteLine("Simple_Box");

			GL.UseProgram(ProgramHandle);

			GL.Uniform2(uTexcoordOffset, 1f / (float)texture.ResourceInfo.Size.Width, 1f / (float)texture.ResourceInfo.Size.Height);

			/*
			ErrorCode error = GL.GetError();

			if (error == ErrorCode.InvalidValue)
			{
				RC.WriteLine("Simple_Box: " + error);

				UnloadResources();
				LoadResources();
				GL.UseProgram(ProgramHandle);

				error = GL.GetError();

				RC.WriteLine("Simple_Box: " + error);
			}			
			*/

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, texture);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Texture = GL.GetUniformLocation(ProgramHandle, "textureSampler");

			GL.Uniform1(m_Texture, 0);

			uTexcoordOffset = GL.GetUniformLocation(ProgramHandle, "texcoordOffset");

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
