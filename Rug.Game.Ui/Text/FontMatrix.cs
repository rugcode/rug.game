﻿using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;

namespace Rug.Game.Core.Text
{
	public enum FontType { XLarge = 0, Large = 1, Heading = 2, Regular = 3, Small = 4, Monospaced = 5 }

	public class FontMatrix : IResource
	{
		public static readonly char[] AllChars = new char[] { 
				'0','1','2','3','4','5','6','7','8','9', 
				'a', 'b', 'c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
				'A', 'B', 'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
				'!','"','$','%','^','&','*','(',')','-','+','_','=','`','¬','\\','|',',','.','/','<','>','?',';',':','\'','@',
				'~','#','[',']','{','}','°','³',' ',
			};
	
		#region Private Members

		private bool m_Disposed = true;
		private int m_TextureSize = 512;

		private System.Drawing.Rectangle[] m_BiggestCharSize = new System.Drawing.Rectangle[6]; 
		private Dictionary<char, System.Drawing.RectangleF>[] m_CharLookup = new Dictionary<char, System.Drawing.RectangleF>[6];
		private Texture2D m_Texture;

		private FontResource m_XLarge = new FontResource(FontType.XLarge);
		private FontResource m_Large = new FontResource(FontType.Large);
		private FontResource m_Heading = new FontResource(FontType.Heading);
		private FontResource m_Regular = new FontResource(FontType.Regular);
		private FontResource m_Small = new FontResource(FontType.Small);
		private FontResource m_Monospaced = new FontResource(FontType.Monospaced);

		#endregion

		#region Public Properties / Members

		[Browsable(false)]
		public int TextureSize { get { return m_TextureSize; } set { m_TextureSize = value; } }

		[Browsable(false)]
		public Texture2D Texture { get { return m_Texture; } }
		
		[Browsable(true)]
		public FontResource XLarge { get { return m_XLarge; } set { m_XLarge = value; } }
		[Browsable(true)]
		public FontResource Large { get { return m_Large; } set { m_Large = value; } }
		[Browsable(true)]
		public FontResource Heading { get { return m_Heading; } set { m_Heading = value; } }
		[Browsable(true)]
		public FontResource Regular { get { return m_Regular; } set { m_Regular = value; } }
		[Browsable(true)]
		public FontResource Small { get { return m_Small; } set { m_Small = value; } }
		[Browsable(true)]
		public FontResource Monospaced { get { return m_Monospaced; } set { m_Monospaced = value; } }

		[Browsable(false)]
		private Bitmap m_Bitmap;

		[Browsable(false)]
		public System.Drawing.Rectangle this[FontType type]
		{
			get
			{
				return m_BiggestCharSize[(int)type]; 
			}
		}

		[Browsable(false)]
		public System.Drawing.RectangleF this[FontType type, char @char]
		{
			get 
			{
				System.Drawing.RectangleF rect;

				Dictionary<char, System.Drawing.RectangleF> charLookup;

				charLookup = m_CharLookup[(int)type];
				
				if (charLookup.TryGetValue(@char, out rect))
				{
					return rect;
				}
				else
				{
					return charLookup['?'];
				}				
			}			
		}

		#endregion

		public FontMatrix() { }

		#region IResourceManager Members

		[Browsable(false)]
		public bool Disposed { get { return m_Disposed;  } }

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				XLarge.LoadResources();
				Large.LoadResources();
				Heading.LoadResources(); 
				Regular.LoadResources();
				Small.LoadResources();
				Monospaced.LoadResources(); 

				RenderToTexture(); 

				m_Disposed = false; 
			}
		}

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				XLarge.UnloadResources();
				Large.UnloadResources();
				Heading.UnloadResources();				
				Regular.UnloadResources();
				Small.UnloadResources();
				Monospaced.UnloadResources(); 

				m_Texture.UnloadResources();

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources();

			if (m_Bitmap != null)
			{
				m_Bitmap.Dispose();
				m_Bitmap = null; 
			}
		}

		#endregion

		#region Render To Bitmap

		public void RenderToTexture()
		{
			if (m_Bitmap == null)
			{
				using (Bitmap bmp = RenderToBitmap())
				{
					RenderToTexture(bmp);
				}
			}
			else
			{
				RenderToTexture(m_Bitmap);
			}
		}

		public void RenderToTexture(Bitmap bmp)
		{
			BitmapData data = null;
				
			try 
			{
				data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

				m_Texture = new Texture2D("Font Texture", ResourceMode.Static, new Texture2DInfo()
				{
					InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba, 
					Size = new Size(bmp.Width, bmp.Height),
					PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Bgra, 
					PixelType = OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, 
					MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Nearest,
					MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Nearest, 
					WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
					WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge, 
				});

				m_Texture.LoadResources(); 

				m_Texture.UploadImage(data.Scan0); 	
			}
			finally 
			{
				if (data != null) { bmp.UnlockBits(data); }
			}
		}


		public Bitmap RenderToBitmap()
		{
			int bmpSize = m_TextureSize;
			float tSize = 1f / (float)bmpSize;

			Bitmap bmp = new Bitmap(bmpSize, bmpSize, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			using (Graphics graphics = Graphics.FromImage(bmp))
			{
				graphics.Clear(Color.Transparent);

				FontResource[] resources = new FontResource[] { XLarge, Large, Heading, Regular, Small, Monospaced };

				float x = 0f, y = 0f;

				bool first = true;
				foreach (FontResource res in resources)
				{
					if (res.Disposed)
					{
						res.LoadResources(); 
					}

					graphics.TextRenderingHint = res.RenderingHint;

					float w = 0, h = 0;
					float hScaleFactor = 1.1f; 

					Dictionary<char, System.Drawing.RectangleF> lookup = new Dictionary<char, System.Drawing.RectangleF>();

					using (StringFormat measureFormat = new StringFormat())
					{						
						measureFormat.FormatFlags = StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.NoWrap | StringFormatFlags.NoClip;
						measureFormat.Alignment = StringAlignment.Near;
						measureFormat.LineAlignment = StringAlignment.Near;
						measureFormat.Trimming = StringTrimming.Character;

						measureFormat.SetMeasurableCharacterRanges(new CharacterRange[] { new CharacterRange(0, 1) });

						foreach (char @char in AllChars)
						{
							using (Region region = graphics.MeasureCharacterRanges(@char.ToString(), res.Font, new RectangleF(0, 0, 10000, 10000), measureFormat)[0])
							{
								RectangleF size = region.GetBounds(graphics);

								w = Math.Max(w, size.Width);
								h = Math.Max(h, size.Height); 
							}
						}

						System.Drawing.Rectangle maxCharBounds = new System.Drawing.Rectangle(0, 0, (int)(w + 0.5f), (int)(h * hScaleFactor));

						if (first == true)
						{
							graphics.FillRectangle(Brushes.White, maxCharBounds);

							x += maxCharBounds.Width;

							int diff = 8 - (maxCharBounds.Width % 8);
						
							x += diff; 

							first = false;
						}

						float xScale = 1f;

						foreach (char @char in AllChars)
						{
							if (@char == ' ')
							{
								float spaceWidth;

								if (res.FontType == FontType.Monospaced)
								{
									spaceWidth = w;
								}
								else
								{
									spaceWidth = maxCharBounds.Width * 0.33f;
								}

								lookup.Add(@char, new System.Drawing.RectangleF(x * tSize, y * tSize, spaceWidth * tSize * xScale, (float)maxCharBounds.Height * tSize));

								x += (int)(spaceWidth + 0.5f);

								int diff = 8 - ((int)(spaceWidth + 0.5f) % 8);

								x += diff; 
							}
							else
							{
								SizeF currentSize;
								float xOffset; 

								using (Region region = graphics.MeasureCharacterRanges(@char.ToString(), res.Font, new RectangleF(0, 0, 10000, 10000), measureFormat)[0])
								{
									RectangleF size = region.GetBounds(graphics);
									xOffset = size.X; 
									currentSize = size.Size; 
								}

								graphics.DrawString(@char.ToString(), res.Font, Brushes.White, new PointF(x - xOffset, y), measureFormat);

								if (res.FontType == FontType.Monospaced)
								{
									currentSize.Width = w;
								}

								lookup.Add(@char, new System.Drawing.RectangleF(x * tSize, y * tSize, currentSize.Width * tSize * xScale, (float)maxCharBounds.Height * tSize));
								
								x += (int)(currentSize.Width + 0.5f);

								int diff = 8 - ((int)(currentSize.Width + 0.5f) % 8);

								x += diff; 
							}

							if (x + maxCharBounds.Width + (8 - (maxCharBounds.Width % 8)) > bmpSize)
							{
								x = 0f;
								y += maxCharBounds.Height;

								int diff = 8 - (maxCharBounds.Height % 8);

								y += diff; 
							}
						}

						x = 0f;
						y += maxCharBounds.Height;

						int diffy = 8 - (maxCharBounds.Height % 8);

						y += diffy; 

						m_BiggestCharSize[(int)res.FontType] = maxCharBounds;
						m_CharLookup[(int)res.FontType] = lookup;
					}
				}
			}

			//bmp.Save(Helper.ResolvePath(@"~/test.png"), ImageFormat.Png);

			return bmp; 
		}

		#endregion

		#region Write / Read

		public void Write(string imagePath)
		{
			using (FileStream stream = new FileStream(imagePath, FileMode.Create))
			{
				using (GZipStream gzip = new GZipStream(stream, CompressionMode.Compress))
				{
					using (Bitmap bmp = RenderToBitmap())
					{
						using (BinaryWriter writer = new BinaryWriter(gzip))
						{
							writer.Write("FontMatrix001");
							writer.Write(m_TextureSize);

							FontResource[] resources = new FontResource[] { XLarge, Large, Heading, Regular, Small, Monospaced };

							writer.Write(resources.Length);
						
							for (int i = 0; i < resources.Length; i++)
							{
								resources[i].Write(writer);

								StructHelper.WriteStructure(writer, m_BiggestCharSize[i]);
							
								Dictionary<char, System.Drawing.RectangleF> lookup = m_CharLookup[i];

								writer.Write(lookup.Count);

								foreach (KeyValuePair<char, System.Drawing.RectangleF> rect in lookup)
								{
									writer.Write(rect.Key);

									StructHelper.WriteStructure(writer, rect.Value);
								}
							}

							using (Stream imageStream = new MemoryStream())
							{
								bmp.Save(imageStream, ImageFormat.Png);

								writer.Write((int)imageStream.Length);
								
								imageStream.Position = 0; 

								Helper.CopyStream(imageStream, gzip);
							}
						}
					}
				}
			}
		}

		public static FontMatrix Read(string imagePath)
		{
            using (FileStream stream = new FileStream(imagePath, FileMode.Open))
            {
                return Read(stream);
            }
        }

        internal static FontMatrix Read(Stream stream)
        {
            using (GZipStream gzip = new GZipStream(stream, CompressionMode.Decompress))
            {
                FontMatrix matrix = new FontMatrix();

                using (BinaryReader reader = new BinaryReader(gzip))
                {
                    string formatName = reader.ReadString();

                    if (formatName != "FontMatrix001")
                    {
                        throw new Exception("Font Matrix format is incorrect '" + formatName + "'");
                    }

                    matrix.TextureSize = reader.ReadInt32();

                    matrix.XLarge = new FontResource();
                    matrix.Large = new FontResource();
                    matrix.Heading = new FontResource();
                    matrix.Regular = new FontResource();
                    matrix.Small = new FontResource();
                    matrix.Monospaced = new FontResource();

                    FontResource[] resources = new FontResource[] { matrix.XLarge, matrix.Large, matrix.Heading, matrix.Regular, matrix.Small, matrix.Monospaced };
                    reader.ReadInt32();

                    for (int i = 0; i < resources.Length; i++)
                    {
                        resources[i].Read(reader);

                        matrix.m_BiggestCharSize[i] = StructHelper.ReadStructure<System.Drawing.Rectangle>(reader);

                        int count = reader.ReadInt32();

                        Dictionary<char, System.Drawing.RectangleF> lookup = new Dictionary<char, System.Drawing.RectangleF>(count);

                        for (int c = 0; c < count; c++)
                        {
                            char @char = reader.ReadChar();

                            System.Drawing.RectangleF rect = StructHelper.ReadStructure<System.Drawing.RectangleF>(reader);

                            lookup.Add(@char, rect);
                        }

                        matrix.m_CharLookup[i] = lookup;
                    }

                    int length = reader.ReadInt32();

                    using (Stream imageStream = new MemoryStream(reader.ReadBytes(length)))
                    {
                        Bitmap bmp = (Bitmap)Bitmap.FromStream(imageStream);

                        matrix.m_Bitmap = bmp;
                    }
                }

                return matrix;
            }
        }

		#endregion

		#region IResource Members

		public string Name
		{
			get { return "Font Matrix"; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get
			{
				if (m_Texture == null)
				{
					return null;
				}
				else
				{
					return m_Texture.ResourceInfo;
				}
			}
		}

		public uint ResourceHandle
		{
			get 
			{
				if (m_Texture == null)
				{
					return 0;
				}
				else
				{
					return m_Texture.ResourceHandle;
				}
			}
		}

		public bool IsLoaded
		{
			get { return m_Disposed == false; }
		}

		

		#endregion
    }
}
