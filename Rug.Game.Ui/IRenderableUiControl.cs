﻿using Rug.Game.Core.Rendering;

namespace Rug.Game.Ui
{
	public interface IRenderableUiControl
	{
		bool IsVisible { get; set; }

		bool IsParentVisible { get; }

		void Render(View3D view); 
	}
}
