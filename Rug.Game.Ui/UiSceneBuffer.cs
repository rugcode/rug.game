﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Ui
{
	public class UiSceneBuffer : IResource
	{
		#region Private Members

		private bool m_Disposed = true;

		private int m_MaxLineCount = 0;
		private int m_MaxLineIndicesCount = 0;

		private int m_MaxTriangleCount = 0;
		private int m_MaxTriangleIndicesCount = 0;

		private int m_LineCount = 0;
		private int m_LineIndicesCount = 0;

		private int m_TriangleCount = 0;
		private int m_TriangleIndicesCount = 0;

		private VertexBuffer m_Lines;
		private IndexBuffer m_LineIndices;

		private VertexBuffer m_Triangles;
		private IndexBuffer m_TriangleIndices;

		private string m_Name;
		private bool m_IsLoaded;

		private bool m_IsMapped = false;

		#endregion

		#region Properties

		public int MaxLineCount { get { return m_MaxLineCount; } }
		public int MaxLineIndicesCount { get { return m_MaxLineIndicesCount; } }

		public int MaxTriangleCount { get { return m_MaxTriangleCount; } }
		public int MaxTriangleIndicesCount { get { return m_MaxTriangleIndicesCount; } }

		public int LineCount { get { return m_LineCount; } }
		public int LineIndicesCount { get { return m_LineIndicesCount; } }

		public int TriangleCount { get { return m_TriangleCount; } }
		public int TriangleIndicesCount { get { return m_TriangleIndicesCount; } }

		public VertexBuffer Lines { get { return m_Lines; } }

		public IndexBuffer LineIndices { get { return m_LineIndices; } }

		public VertexBuffer Triangles { get { return m_Triangles; } }

		public IndexBuffer TriangleIndices { get { return m_TriangleIndices; } }

		public bool IsValid
		{
			get
			{
				return m_Lines.IsValid &&
						m_LineIndices.IsValid &&
						m_Triangles.IsValid &&
						m_TriangleIndices.IsValid;
			}
		}

		#endregion

		#region IResource Members

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Dynamic; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public DrawElementsType IndexType { get { return m_LineIndices.IndexType; } }

		#endregion

		public UiSceneBuffer(string name, BufferUsageHint usage)
		{
			m_Name = name;

			m_Lines = new VertexBuffer(Name + " Lines", ResourceMode.Dynamic, new VertexBufferInfo(SimpleVertex.Format, 4, usage));
			m_LineIndices = new IndexBuffer(Name + " Line Indices", ResourceMode.Dynamic, new IndexBufferInfo(DrawElementsType.UnsignedInt, sizeof(uint), 8, usage));

			m_Triangles = new VertexBuffer(Name + " Triangles", ResourceMode.Dynamic, new VertexBufferInfo(SimpleVertex.Format, 4, usage));
			m_TriangleIndices = new IndexBuffer(Name + " Triangle Indices", ResourceMode.Dynamic, new IndexBufferInfo(DrawElementsType.UnsignedInt, sizeof(uint), 6, usage)); 
		}

		#region Resize

		public void Resize(int lineCount, int lineIndicesCount, int triangleCount, int triangleIndicesCount)
		{
			
			if (m_Disposed == false &&
				m_MaxLineCount >= lineCount &&
				m_MaxLineIndicesCount >= lineIndicesCount &&
				m_MaxTriangleCount >= triangleCount &&
				m_MaxTriangleIndicesCount >= triangleIndicesCount)
			{
				return;
			}
			 
			bool wasDisposed = m_Disposed;

			UnloadResources();

			m_MaxLineCount = lineCount < 2 ? 2 : lineCount;
			m_MaxLineIndicesCount = lineIndicesCount < 2 ? 2 : lineIndicesCount;

			m_MaxTriangleCount = triangleCount < 3 ? 3 : triangleCount;
			m_MaxTriangleIndicesCount = triangleIndicesCount < 3 ? 3 : triangleIndicesCount;

			if (wasDisposed == false)
			{
				LoadResources();
			}
		}

		#endregion

		public void MapStreams(BufferAccess mode, out DataStream Lines, out DataStream LineIndices, out DataStream Triangles, out DataStream TriangleIndices)
		{
			if (m_IsMapped == true)
			{
				throw new Exception("Scene buffer is already mapped"); 
			}

			m_Lines.MapBuffer(mode, out Lines);
			m_LineIndices.MapBuffer(mode, out LineIndices);
			m_Triangles.MapBuffer(mode, out Triangles);
			m_TriangleIndices.MapBuffer(mode, out TriangleIndices);

			m_IsMapped = true; 
		}

		public void UnmapStreams(int lineCount, int lineIndicesCount, int triangleCount, int triangleIndicesCount)
		{
			if (m_IsMapped == false)
			{
				return; 
			}

			try
			{
				bool error = false;

				if (m_MaxLineCount < lineCount)
				{
					RC.WriteError(01, "Line count is greater than Max (" + lineCount + " / " + m_MaxLineCount + ")");

					error = true;
				}

				if (m_MaxLineIndicesCount < lineIndicesCount)
				{
					RC.WriteError(01, "Line Indices count is greater than Max (" + lineIndicesCount + " / " + m_MaxLineIndicesCount + ")");

					error = true;
				}

				if (m_MaxTriangleCount < triangleCount)
				{
					RC.WriteError(01, "Triangle count is greater than Max (" + triangleCount + " / " + m_MaxTriangleCount + ")");

					error = true;
				}

				if (m_MaxTriangleIndicesCount < triangleIndicesCount)
				{
					RC.WriteError(01, "Triangle Indices count is greater than Max (" + triangleIndicesCount + " / " + m_MaxTriangleIndicesCount + ")");

					error = true;
				}

				m_LineCount = lineCount;
				m_LineIndicesCount = lineIndicesCount;
				m_TriangleCount = triangleCount;
				m_TriangleIndicesCount = triangleIndicesCount;

				if (error == true)
				{
					UnloadResources();
					LoadResources();
				}
				else
				{
					m_Lines.UnmapBuffer();
					m_LineIndices.UnmapBuffer();
					m_Triangles.UnmapBuffer();
					m_TriangleIndices.UnmapBuffer();
				}
			}
			catch (AccessViolationException ex)
			{
				RC.WriteException(01, "Access Violation Exception", ex);				
			}

			m_IsMapped = false; 
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				m_Lines.ResourceInfo.Count = m_MaxLineCount;
				m_Lines.LoadResources();

				m_LineIndices.ResourceInfo.Count = m_MaxLineIndicesCount;
				m_LineIndices.LoadResources();

				m_Triangles.ResourceInfo.Count = m_MaxTriangleCount;
				m_Triangles.LoadResources();

				m_TriangleIndices.ResourceInfo.Count = m_MaxTriangleIndicesCount;
				m_TriangleIndices.LoadResources();

				m_Disposed = false;
			}
		}

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Lines.UnloadResources();
				m_LineIndices.UnloadResources();
				m_Triangles.UnloadResources();
				m_TriangleIndices.UnloadResources();

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources();
		}

		#endregion
	}
}
