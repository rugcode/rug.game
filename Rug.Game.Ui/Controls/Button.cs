﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Text;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Controls
{
	public class Button : UiControlBase
	{
		private string m_Text = "";
		private FontType m_FontType = FontType.Small;
		private bool m_Enabled = true;

		public string Text { get { return m_Text; } set { m_Text = value; } }

		public FontType FontType { get { return m_FontType; } set { m_FontType = value; } }

		public bool Enabled { get { return m_Enabled; } set { m_Enabled = value; } }

		public event EventHandler Click;

		public Button()
		{
			InteractionType = ControlInteractionType.Click; 
		}

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false;

			if (Enabled == false)
			{
				return; 
			}

			switch (mouseState)
			{
				case MouseState.ClickEnd:
					if (Bounds.Contains(mousePos.X, mousePos.Y))
					{
						DoClick();
						shouldUpdate = true; 
					}
					break;
				default:
					base.DoMouseInteraction(mouseState, mouseButtons, mousePos, out shouldUpdate);
					break;
			}			
		}

		public void DoClick()
		{
			if (Click != null)
			{
				Click(this, EventArgs.Empty);
			}
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control)
		{
			base.EndPick(mousePos, pickType, control);
		}

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);

			int textIndexCount, textTriangleCount;
			TextRenderHelper.GetTotalElementCounts(m_Text, out textIndexCount, out textTriangleCount);

			TriangleVerts += textTriangleCount;
			TriangleIndices += textIndexCount;
		}

		public override void WriteVisibleElements(View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			ControlStyle = m_Enabled == false ? DisplayMode.Dimmed : DisplayMode.Auto;

			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
										LineVerts, ref LineVertsCount,
										LinesIndices, ref LinesIndicesCount,
										TriangleVerts, ref TriangleVertsCount,
										TriangleIndices, ref TriangleIndicesCount);

			if (String.IsNullOrEmpty(m_Text) == true)
			{
				return; 
			}

			float x, y, w, h;
			float px = view.PixelSize.X;
			float py = view.PixelSize.Y;

			SizeF stringSize = TextRenderHelper.MessureString(m_Text, m_FontType, view, 1f);

			UiStyleHelper.CovertToVertCoords(Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			Color4 textColor = UiStyleHelper.GetControlTextColor(this, ControlStyle);

			TextRenderHelper.WriteString(view, Bounds,
													m_Text, m_FontType, new Vector3(px * (Size.Width - stringSize.Width) * 0.5f, -py * (Size.Height - stringSize.Height) * 0.5f, ZIndexForLines_Float), 1f, textColor,
													TriangleVerts, ref TriangleVertsCount,
													TriangleIndices, ref TriangleIndicesCount);
		}
	}
}
