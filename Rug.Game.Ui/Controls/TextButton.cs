﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Text;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Controls
{
	public class TextButton : UiControlBase
	{
		private string m_Text = "";
		private FontType m_FontType = FontType.Small;
		private ContentAlignment m_TextAlign = ContentAlignment.MiddleCenter;
		private System.Windows.Forms.Padding m_Padding = new System.Windows.Forms.Padding(0);
		private bool m_FixedSize = false;
		private bool m_Enabled = true;
		private TextButtonGroup m_Group = null; 

		public string Text { get { return m_Text; } set { m_Text = value; } }

		public bool Enabled { get { return m_Enabled; } set { m_Enabled = value; } }

		public FontType FontType { get { return m_FontType; } set { m_FontType = value; } }

		public ContentAlignment TextAlign { get { return m_TextAlign; } set { m_TextAlign = value; } }

		public System.Windows.Forms.Padding Padding { get { return m_Padding; } set { m_Padding = value; } }

		public bool FixedSize { get { return m_FixedSize; } set { m_FixedSize = value; } }

		public TextButtonGroup Group
		{
			get
			{
				return m_Group;
			}
			set
			{
				if (m_Group != null)
				{
					m_Group.FocusList.Remove(this);
					m_Group.FocusList.Sort();

					if (m_Group.CurrentFocus == this)
					{
						m_Group.SetDefaultFocus(); 
					}
				}

				m_Group = value;

				if (m_Group != null)
				{
					m_Group.FocusList.Add(this);
					m_Group.FocusList.Sort(); 
				}
			}
		}

		public event EventHandler Click;				

		public TextButton()
		{
			InteractionType = ControlInteractionType.Click; 
		}

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false;

			if (m_Enabled == false) return;

			switch (mouseState)
			{
				case MouseState.ClickEnd:
					if (Bounds.Contains(mousePos.X, mousePos.Y))
					{
						DoClick();
						shouldUpdate = true; 
					}
					break;
				default:
					base.DoMouseInteraction(mouseState, mouseButtons, mousePos, out shouldUpdate);
					break;
			}			
		}

		public void DoClick()
		{
			if (m_Enabled == false) return;

			if (Click != null)
			{
				Click(this, EventArgs.Empty);
			}
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control)
		{
			base.EndPick(mousePos, pickType, control);

			if (IsFocused == true && Group != null)
			{
				Group.Focus(this); 
			}
		}

		public override void DoKeyInteraction(KeyInteractionType keyInteractionType, char @char, System.Windows.Forms.KeyEventArgs eventArgs, out bool shouldUpdate)
		{
			if (keyInteractionType == KeyInteractionType.KeyUp && 
				(eventArgs.KeyCode == System.Windows.Forms.Keys.Enter ||
				eventArgs.KeyCode == System.Windows.Forms.Keys.Return))
			{
				DoClick();

				shouldUpdate = true; 
			}
			else if (m_Group != null && keyInteractionType == KeyInteractionType.KeyUp && 
					(eventArgs.KeyCode == System.Windows.Forms.Keys.Up ||
					 eventArgs.KeyCode == System.Windows.Forms.Keys.Down))
			{
				m_Group.MoveFocus(eventArgs.KeyCode == System.Windows.Forms.Keys.Up);

				shouldUpdate = true; 
			}
			else
			{
				base.DoKeyInteraction(keyInteractionType, @char, eventArgs, out shouldUpdate);
			}
		}

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			int textIndexCount, textTriangleCount;
			TextRenderHelper.GetTotalElementCounts(m_Text, out textIndexCount, out textTriangleCount);

			TriangleVerts += textTriangleCount;
			TriangleIndices += textIndexCount;
		}

		public override void WriteVisibleElements(View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			SizeF stringSize = TextRenderHelper.MessureString(m_Text, m_FontType, view, 1f);

			if (m_FixedSize == false)
			{
				this.Size = new System.Drawing.Size(m_Padding.Horizontal + (int)stringSize.Width, m_Padding.Vertical + (int)stringSize.Height);
			}

			m_Bounds = UiStyleHelper.LayoutControlBounds(RemainingBounds, Location, Size, Anchor, Docking, out RemainingBounds);

			float x, y;
			float px = view.PixelSize.X;
			float py = view.PixelSize.Y;

			PointF textLocation = UiStyleHelper.LayoutTextBounds(Bounds, stringSize, TextAlign, Padding);

			UiStyleHelper.CovertToVertCoords_Relitive(textLocation, view.PixelSize, out x, out y);

			Color4 textColor = UiStyleHelper.GetControlBackColor(this, m_Enabled ? DisplayMode.Auto : DisplayMode.Dimmed);

			TextRenderHelper.WriteString(view, Bounds,
											m_Text, m_FontType, new Vector3(x, y, ZIndexForLines_Float), 1f, textColor,
											TriangleVerts, ref TriangleVertsCount,
											TriangleIndices, ref TriangleIndicesCount);
		}
	}
}
