﻿using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Controls
{
	public class GradiantPanel : Panel
	{
		public Color4 BackgroundTop { get; set; }
		public Color4 BackgroundBottom { get; set; }
		public int FadeSize { get; set; } 

		public GradiantPanel()
		{
			InteractionType = ControlInteractionType.NonSelectable;
			ShowBackground = false;
			ShowBorder = false; 
		}

		public override void WriteVisibleElements(Rug.Game.Core.Rendering.View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			RectangleF remBounds = RemainingBounds;

			RectangleF bounds = UiStyleHelper.LayoutControlBounds(remBounds, Location, new Size(this.Size.Width, Math.Min(FadeSize, this.Size.Height)), Anchor, Docking, out remBounds);

			remBounds = bounds;

			RectangleF boundsA = UiStyleHelper.LayoutControlBounds(remBounds, Location, new Size(this.Size.Width, Math.Min(FadeSize, this.Size.Height)), Anchor, System.Windows.Forms.DockStyle.Top, out remBounds);

			UiStyleHelper.CovertToVertCoords(boundsA, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			WriteTrianglesForBounds(x, y, w, h, BackgroundTop, BackgroundBottom, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);

			UiStyleHelper.CovertToVertCoords(remBounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			WriteTrianglesForBounds(x, y, w, h, BackgroundBottom, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);

			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds, LineVerts, ref LineVertsCount, LinesIndices, ref LinesIndicesCount, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);			
		}

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			TriangleVerts += 8;
			TriangleIndices += 12;

			base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);
		}
	}
}
