﻿using System;
using System.Collections.Generic;

namespace Rug.Game.Ui.Controls
{
	public class TextButtonGroup
	{
		public List<TextButton> FocusList = new List<TextButton>();
		public TextButton CurrentFocus = null;

		public void SetDefaultFocus()
		{
			if (FocusList.Count > 0)
			{
				CurrentFocus = FocusList[0];
				CurrentFocus.Scene.FocusControl(CurrentFocus);
				CurrentFocus.IsFocused = true; 
			}
		}

		public UiControl SetCurrentFocus()
		{
			if (CurrentFocus != null)
			{
				CurrentFocus.Scene.FocusControl(CurrentFocus);
				CurrentFocus.IsFocused = true; 
			}

			return CurrentFocus;
		}

		public void Focus(TextButton button)
		{
			int index = 0;
			if (CurrentFocus != null)
			{
				index = FocusList.IndexOf(button);

				if (index >= 0)
				{
					CurrentFocus.IsFocused = false;

					CurrentFocus = button;
					
					CurrentFocus.Scene.FocusControl_NoPick(CurrentFocus); 

					CurrentFocus.IsFocused = true;
				}
			}
		}

		public void MoveFocus(bool up)
		{
			int index = 0;
			if (CurrentFocus != null)
			{
				CurrentFocus.IsFocused = false;
				index = Math.Max(0, FocusList.IndexOf(CurrentFocus));
			}

			if (FocusList.Count > 1)
			{
				if (up == true)
				{
					index--;
					if (index < 0)
					{
						index = FocusList.Count - 1;
					}
				}
				else
				{
					index++;
					if (index >= FocusList.Count)
					{
						index = 0;
					}
				}

				CurrentFocus = FocusList[index];
				CurrentFocus.Scene.FocusControl(CurrentFocus); 
				CurrentFocus.IsFocused = true;				
			}
		}

		public void HandelEnter()
		{
			if (CurrentFocus != null)
			{
				CurrentFocus.DoClick();
			}
		}
	}
}
