﻿using OpenTK.Graphics;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using System.Drawing;

namespace Rug.Game.Ui.Controls
{
	public class ImageButton : Button, IRenderableUiControl, IResourceManager
	{
		private TextureBox m_ImageBox;

		public Texture2D Image { get { return m_ImageBox.Texture; } set { m_ImageBox.Texture = value; } }

		public RectangleF ImageCrop { get { return m_ImageBox.TextureRectangle; } set { m_ImageBox.TextureRectangle = value; } }

        public Color4 NormalColor { get; set; }

        public Color4 SelectedColor { get; set; }

        public Color4 HoverColor { get; set; }

        public Color4 DimmedColor { get; set; }

        public Color4 HighlightColor { get; set; }

        public bool ColorOverride { get; set; } 

		public ImageButton()
		{
			m_ImageBox = new TextureBox(); 
		}

		public override void WriteVisibleElements(Core.Rendering.View3D view, RectangleF ClientBounds, ref RectangleF RemainingBounds, Core.Buffers.DataStream LineVerts, ref int LineVertsCount, Core.Buffers.DataStream LinesIndices, ref int LinesIndicesCount, Core.Buffers.DataStream TriangleVerts, ref int TriangleVertsCount, Core.Buffers.DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds, LineVerts, ref LineVertsCount, LinesIndices, ref LinesIndicesCount, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);

			m_ImageBox.Depth = ZIndexForOver_Float;

            if (ColorOverride == true)
            {
                switch (UiStyleHelper.GetModeForControl(this, this.ControlStyle))
                {
                    case DisplayMode.Normal:
                        m_ImageBox.Color = NormalColor;
                        break;
                    case DisplayMode.Focused:
                        m_ImageBox.Color = HoverColor;
                        break;
                    case DisplayMode.Hovering:
                        m_ImageBox.Color = HoverColor;
                        break;
                    case DisplayMode.Highlight:
                        m_ImageBox.Color = HighlightColor;
                        break;
                    case DisplayMode.Dimmed:
                        m_ImageBox.Color = DimmedColor;
                        break;
                    default:
                        m_ImageBox.Color = UiStyleHelper.GetControlTextColor(this, DisplayMode.Auto);
                        break;
                }                
            }
            else
            {
                m_ImageBox.Color = UiStyleHelper.GetControlTextColor(this, DisplayMode.Auto);
            }

			m_ImageBox.FlipVertical = true;

			m_ImageBox.Rectangle = UiStyleHelper.CovertToVertRectangle(Bounds, view.WindowSize, view.PixelSize); 
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_ImageBox.Disposed; }
		}

		public void LoadResources()
		{
			m_ImageBox.LoadResources();
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			m_ImageBox.UnloadResources(); 
		}

		public void Dispose()
		{
			m_ImageBox.Dispose(); 
		}

		public void Render(Core.Rendering.View3D view)
		{
			m_ImageBox.Render(view); 
		}

		#endregion
	}
}
