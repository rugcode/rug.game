﻿using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using System;

namespace Rug.Game.Ui
{
	public class UiTextBuffer
	{
		private int m_Size;
		private int m_Count;
		private SimpleVertex[] m_Verts;
		private uint[] m_Indices;
		private uint m_Offset; 

		public int Size { get { return m_Size; } }
		public int Count { get { return m_Count; } }

		public uint Offset { get { return m_Offset; } }

		public UiTextBuffer(int size)
		{
			m_Size = size;
			m_Count = 0;
			m_Offset = 0; 

			m_Verts = new SimpleVertex[4 * size];
			m_Indices = new uint[6 * size];
		}

		public void Reset()
		{
			m_Count = 0;
			m_Offset = 0; 
		}

		public void Write(SimpleVertex[] verts, uint[] indices, int count, 
						  DataStream TriangleVerts, ref int TriangleVertsCount, 
						  DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			if (m_Count + count >= m_Size)
			{
				Flush(TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);
			}

			//Buffer.BlockCopy(verts, 0, m_Verts, 4 * m_Count, 4 * count);
			//Buffer.BlockCopy(indices, 0, m_Indices, 6 * m_Count, 6 * count);
			Array.Copy(verts, 0, m_Verts, 4 * m_Count, 4 * count);
			Array.Copy(indices, 0, m_Indices, 6 * m_Count, 6 * count);

			m_Offset += (uint)(4 * count);

			m_Count += count; 
		}

		public void Flush(DataStream TriangleVerts, ref int TriangleVertsCount,
						  DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			if (m_Count > 0)
			{
				TriangleVerts.WriteRange(m_Verts, 0, 4 * m_Count);
				TriangleIndices.WriteRange(m_Indices, 0, 6 * m_Count);

				TriangleVertsCount += 4 * m_Count;
				TriangleIndicesCount += 6 * m_Count; 

				Reset();
			}
		}

	}
}
