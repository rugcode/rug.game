﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Rendering;
using System.Drawing;

namespace Rug.Game.Ui
{
	public enum ControlBorder : int { Auto = -1, None = 0, Top = 1, Bottom = 2, Left = 4, Right = 8, All = Top | Bottom | Left | Right }
 
	public class UiControlBase : UiControl
	{
		#region Private Members
		
		private bool m_IsVisible = true;
		protected System.Drawing.RectangleF m_Bounds;
		private Size m_Size = new Size(100, 100);
		private Point m_Location = new Point(100, 100);

		private System.Windows.Forms.DockStyle m_Docking = System.Windows.Forms.DockStyle.None;
		private System.Windows.Forms.AnchorStyles m_Anchor = System.Windows.Forms.AnchorStyles.None;
		private DisplayMode m_ControlStyle = DisplayMode.Auto;

		private bool m_ShowBackground = true;
		private bool m_ShowBorder = true;
		private ControlBorder m_Borders = ControlBorder.Auto;

		private Color4 m_BackgroundColor = Color.Transparent;
		private Color4 m_LineColor = Color.Transparent;
		private Color4 m_TextColor = Color.Transparent;

		#endregion

		#region Properties

		public DisplayMode ControlStyle { get { return m_ControlStyle; } protected set { m_ControlStyle = value; } }		
		public System.Windows.Forms.DockStyle Docking { get { return m_Docking; } set { m_Docking = value; } }		
		public System.Windows.Forms.AnchorStyles Anchor { get { return m_Anchor; } set { m_Anchor = value; } }

		public Size Size { get { return m_Size; } set { m_Size = value; } }
		public Point Location { get { return m_Location; } set { m_Location = value; } }

		public ControlBorder Borders { get { return m_Borders; } set { m_Borders = value; } }	

		public override bool IsVisible { get { return m_IsVisible; } set { m_IsVisible = value; } }
		
		public bool IsParentVisible
		{
			get
			{
				UiControl par = Parent;

				while (par != null)
				{
					if (par.IsVisible == false)
					{
						return false;
					}
					par = par.Parent;
				}

				return true;
			}
		}

		public override System.Drawing.RectangleF Bounds { get { return m_Bounds; } }

		public bool ShowBackground { get { return m_ShowBackground; } set { m_ShowBackground = value; } }
		public bool ShowBorder { get { return m_ShowBorder; } set { m_ShowBorder = value; } }

		public Color4 BackgroundColor { get { return m_BackgroundColor; } set { m_BackgroundColor = value; } }
		public Color4 LineColor { get { return m_LineColor; } set { m_LineColor = value; } }
		public Color4 TextColor { get { return m_TextColor; } set { m_TextColor = value; } } 

		#endregion

		public UiControlBase() { }

		#region Elements
		
		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			if (m_ShowBorder == true)
			{
				LineVerts += 4;
				LinesIndices += 8;
			}

			if (m_ShowBackground == true)
			{
				TriangleVerts += 4;
				TriangleIndices += 6;
			}
		}

		public override void WriteVisibleElements(View3D view,
													System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds,
													DataStream LineVerts, ref int LineVertsCount, 
													DataStream LinesIndices, ref int LinesIndicesCount, 
													DataStream TriangleVerts, ref int TriangleVertsCount, 
													DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			m_Bounds = UiStyleHelper.LayoutControlBounds(RemainingBounds, Location, Size, Anchor, Docking, out RemainingBounds);

			UiStyleHelper.CovertToVertCoords(m_Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			if (m_ShowBorder == true)
			{
				Color4 lineColor;

				if (LineColor != (Color4)Color.Transparent)
				{
					lineColor = LineColor;
				}
				else
				{
					lineColor = UiStyleHelper.GetControlLineColor(this, m_ControlStyle);
				}

				WriteLinesForBounds(x, y, w, h, lineColor, LineVerts, ref LineVertsCount, LinesIndices, ref LinesIndicesCount);
			}

			if (m_ShowBackground == true)
			{
				Color4 backColor;

				if (BackgroundColor != (Color4)Color.Transparent) 
				{
					backColor = BackgroundColor; 
				}
				else 
				{
					backColor = UiStyleHelper.GetControlBackColor(this, m_ControlStyle);
				}

				WriteTrianglesForBounds(x, y, w, h, backColor, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);
			}
		}

		#endregion

		#region Write Helpers

		protected void WriteLinesForBounds(float x, float y, float w, float h, Color4 lineColor, 
											DataStream LineVerts, ref int LineVertsCount, 
											DataStream LinesIndices, ref int LinesIndicesCount)
		{
			uint i = (uint)LineVertsCount;

			if ((m_Borders == ControlBorder.Bottom) || (m_Borders == ControlBorder.Auto && Docking == System.Windows.Forms.DockStyle.Top))
			{
				// place a single line at the bottom
				LineVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) }
				});

				LinesIndices.WriteRange(new uint[] { 
					i + 0, i + 1, 
				});

				LineVertsCount += 2;
				LinesIndicesCount += 2;
			}
			else if ((m_Borders == ControlBorder.Top) || (m_Borders == ControlBorder.Auto && Docking == System.Windows.Forms.DockStyle.Bottom))
			{
				// place a single line at the top
				LineVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },
				});

				LinesIndices.WriteRange(new uint[] { 
					i + 0, i + 1, 
				});

				LineVertsCount += 2;
				LinesIndicesCount += 2;
			}
			else if ((m_Borders == ControlBorder.Right) || (m_Borders == ControlBorder.Auto && Docking == System.Windows.Forms.DockStyle.Left))
			{
				// place a single line at the right
				LineVerts.WriteRange(new SimpleVertex[] { 					
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },					
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) }
				});

				LinesIndices.WriteRange(new uint[] { 
					i + 0, i + 1, 
				});

				LineVertsCount += 2;
				LinesIndicesCount += 2;
			}
			else if ((m_Borders == ControlBorder.Left) || (m_Borders == ControlBorder.Auto && Docking == System.Windows.Forms.DockStyle.Right))
			{
				// place a single line at the left
				LineVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },					
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },					
				});

				LinesIndices.WriteRange(new uint[] { 
					i + 0, i + 1, 
				});

				LineVertsCount += 2;
				LinesIndicesCount += 2;
			}
			else if ((m_Borders == ControlBorder.All) || (m_Borders == ControlBorder.Auto && Docking != System.Windows.Forms.DockStyle.Fill))
			{
				// place a single line at the all lines
				LineVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) }
				});

				LinesIndices.WriteRange(new uint[] { 
					i + 0, i + 1, 
					i + 1, i + 3, 
					i + 3, i + 2,
 					i + 2, i + 0,
				});

				LineVertsCount += 4;
				LinesIndicesCount += 8;
			}
			else if (m_Borders != ControlBorder.None) 
			{		
				// place a single line at the all lines
				LineVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },		// top left
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },	// top right
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) },	// bottom left 
					new SimpleVertex() { Color = lineColor, Position = new Vector3(x + w, y + h, ZIndexForLines_Float), TextureCoords = new Vector2(0, 0) }	// bottom right
				});


				uint[] indices = new uint[8]; 
				int local_i = 0;

				if ((m_Borders & ControlBorder.Top) == ControlBorder.Top)				
				{
					indices[local_i++] = i + 0;
					indices[local_i++] = i + 1;
				}

				if ((m_Borders & ControlBorder.Right) == ControlBorder.Right) 
				{
					indices[local_i++] = i + 1;
					indices[local_i++] = i + 3;
				}

				if ((m_Borders & ControlBorder.Bottom) == ControlBorder.Bottom) 
				{
					indices[local_i++] = i + 3;
					indices[local_i++] = i + 2;
				}

				if ((m_Borders & ControlBorder.Left) == ControlBorder.Left)
				{
					indices[local_i++] = i + 2;
					indices[local_i++] = i + 0;
				}

				LinesIndices.WriteRange(indices, 0, local_i); 

				LineVertsCount += 4;
				LinesIndicesCount += local_i;
			}
		}

		protected void WriteTrianglesForBounds(float x, float y, float w, float h, Color4 backColor,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			WriteTrianglesForBounds(x, y, w, h, ZIndex_Float, backColor,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
		} 

		protected void WriteTrianglesForBounds(float x, float y, float w, float h, float z, Color4 backColor,
												DataStream TriangleVerts, ref int TriangleVertsCount, 
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = backColor, Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = backColor, Position = new Vector3(x + w, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = backColor, Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = backColor, Position = new Vector3(x + w, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			uint i = (uint)TriangleVertsCount;

			TriangleIndices.WriteRange(new uint[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2
			});

			TriangleVertsCount += 4;
			TriangleIndicesCount += 6;
		}

		protected void WriteTrianglesForBounds(float x, float y, float w, float h, Color4 backColorTop, Color4 backColorBottom,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			WriteTrianglesForBounds(x, y, w, h, ZIndex_Float, backColorTop, backColorBottom,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
		} 

		protected void WriteTrianglesForBounds(float x, float y, float w, float h, float z, Color4 backColorTop, Color4 backColorBottom,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = backColorTop, Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = backColorTop, Position = new Vector3(x + w, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = backColorBottom, Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = backColorBottom, Position = new Vector3(x + w, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			uint i = (uint)TriangleVertsCount;

			TriangleIndices.WriteRange(new uint[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2
			});

			TriangleVertsCount += 4;
			TriangleIndicesCount += 6;
		} 

		#endregion
			
	}
}
