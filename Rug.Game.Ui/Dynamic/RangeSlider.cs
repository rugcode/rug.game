﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Dynamic
{
    public delegate void RangeSliderValueChangedEvent(RangeSlider sender, Vector2 value);
    public enum RangeSliderInteractionType { None, Lower, Upper, Center } 

	public class RangeSlider : UiControlBase, IDynamicUiControl
	{
		private float m_MinValue = 0f;
		private float m_MaxValue = 100f;

        private Vector2 m_CenterMoveState; 

		private float m_Value1 = 0f;
        private float m_Value2 = 0f;        

        private float m_Percent1;
        private float m_Percent2;
        private float m_CenterPercent; 

		private SliderScale m_SliderScale = SliderScale.Linear;
		private BarAlignment m_BarAlignment = BarAlignment.Horizontal;
		private bool m_InvertHighlight = false;

        private RangeSliderInteractionType m_RangeSliderInteractionType = RangeSliderInteractionType.None; 

		public float MinValue { get { return m_MinValue; } set { m_MinValue = value; } }
		public float MaxValue { get { return m_MaxValue; } set { m_MaxValue = value; } }

        public Vector2 Value
        {
            get { return new Vector2(m_Value1, m_Value2); }
            set
            {
                m_Value1 = value.X;
                m_Value2 = value.Y;
                
                OnValueChanged(); 
            }
        }

		public float Value1
		{
			get { return m_Value1; } 
			set 
			{
				if (m_Value1 != value)
				{
					m_Value1 = value;
					OnValueChanged(); 
				}
			} 
		}

        public float Value2
		{
			get { return m_Value2; } 
			set 
			{
				if (m_Value2 != value)
				{
					m_Value2 = value;
					OnValueChanged(); 
				}
			} 
		}

		public float SliderPosition1
		{
			get
			{
				return m_Percent1; 
			}
			set
			{
				CalculateValue1(value); 
				
				OnValueChanged();
			}
		}

        public float SliderPosition2
		{
			get
			{
				return m_Percent2; 
			}
			set
			{
				CalculateValue2(value); 
				
				OnValueChanged();
			}
		}

		public BarAlignment BarAlignment { get { return m_BarAlignment; } set { m_BarAlignment = value; } }

		public bool InvertHighlight { get { return m_InvertHighlight; } set { m_InvertHighlight = value; } }

		public SliderScale SliderScale 
        { 
            get { return m_SliderScale; } 
            set 
            { 
                m_SliderScale = value;

                CalculatePercent(m_Value1, m_Value2);
            } 
        }

        public event RangeSliderValueChangedEvent ValueChanged;

        public RangeSlider()
		{
			InteractionType = ControlInteractionType.Drag;
			ControlStyle = DisplayMode.Normal; 
		}


		public void SetValue_NoEvent(Vector2 vector2)
		{
			m_Value1 = vector2.X;
			m_Value2 = vector2.Y;

			CalculatePercent(m_Value1, m_Value2); 
		}

        public float ClipByRange(float value)
        {
            return m_Value1 + (Math.Max(Math.Min(value, 1f), 0f) * (m_Value2 - m_Value1));
        }

        public float ValueInRange(float value)
        {
            float scaledValue = (value - m_Value1) / (m_Value2 - m_Value1);

            return Math.Max(Math.Min(scaledValue, 1f), 0f);
        }

		private void OnValueChanged()
		{
			CalculatePercent(m_Value1, m_Value2); 

			if (ValueChanged != null)
			{
				ValueChanged(this, Value);
			}
		}

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false; 
			switch (mouseState)
			{
				case MouseState.DragStart:
                    DeterminInteractionType(mousePos, mouseButtons);                         
					CalculateValue(mousePos);
                    break;
				case MouseState.Dragging:
				case MouseState.DragEnd:
					CalculateValue(mousePos);
					break;
				default:
					base.DoMouseInteraction(mouseState, mouseButtons, mousePos, out shouldUpdate);
					break;
			}			
		}

        private void DeterminInteractionType(Vector2 mousePos, System.Windows.Forms.MouseButtons mouseButtons)
        {
            switch (m_BarAlignment)
            {
                case BarAlignment.Horizontal:
                    {
                        float position1 = m_Bounds.X + (m_Percent1 * m_Bounds.Width); 
                        float position2 = m_Bounds.X + (m_Percent2 * m_Bounds.Width);
                        float center = m_Bounds.X + (m_CenterPercent * m_Bounds.Width);

                        if (mousePos.X < Bounds.X)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Lower;
                        }
                        else if (mousePos.X > Bounds.X + Bounds.Width)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Upper;
                        }
                        else if (mousePos.X <= position1)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Lower;
                        }
                        else if (mousePos.X >= position2)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Upper;
                        }
                        else if (mouseButtons == System.Windows.Forms.MouseButtons.Right)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Center;

                            m_CenterMoveState = new Vector2(m_CenterPercent, (m_Percent2 - m_Percent1) * 0.5f); 
                        }
                        else if (mousePos.X < center)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Lower;
                        }
                        else
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Upper;
                        } 
                    }
                    break;
                case BarAlignment.Vertical:
                    {
                        float position1 = m_Bounds.Y + (m_Percent1 * m_Bounds.Height); 
                        float position2 = m_Bounds.Y + (m_Percent2 * m_Bounds.Height);
                        float center = m_Bounds.Y + (m_CenterPercent * m_Bounds.Height);

                        if (mousePos.Y < Bounds.Y)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Upper;
                        }
                        else if (mousePos.Y > Bounds.Y + Bounds.Height)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Lower;
                        }
                        else if (mousePos.Y <= position1)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Upper;
                        }
                        else if (mousePos.Y >= position2)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Lower;
                        }
                        else if (mouseButtons == System.Windows.Forms.MouseButtons.Right)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Center;

                            m_CenterMoveState = new Vector2(m_CenterPercent, (m_Percent2 - m_Percent1) * 0.5f); 
                        }
                        else if (mousePos.Y < center)
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Upper;
                        }
                        else
                        {
                            m_RangeSliderInteractionType = RangeSliderInteractionType.Lower;
                        }

                        if (m_InvertHighlight == true)
                        {
                            if (m_RangeSliderInteractionType == RangeSliderInteractionType.Upper)
                            {
                                m_RangeSliderInteractionType = RangeSliderInteractionType.Lower; 
                            }
                            else if (m_RangeSliderInteractionType == RangeSliderInteractionType.Lower)
                            {
                                m_RangeSliderInteractionType = RangeSliderInteractionType.Upper; 
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

		private void CalculateValue(Vector2 mousePos)
		{
			float oldValue1 = m_Value1;
            float oldValue2 = m_Value2;

            float position = 0; 

			switch (m_BarAlignment)
			{
				case BarAlignment.Horizontal:
                    if (mousePos.X < Bounds.X) position = 0;
                    else if (mousePos.X > Bounds.X + Bounds.Width) position = 1;
                    else position = (1f / Bounds.Width) * (mousePos.X - Bounds.X);

                    if (m_InvertHighlight == true)
                    {
                        position = 1f - position; 
                    }

                    CalculateValue(position, m_RangeSliderInteractionType); 
					break;
				case BarAlignment.Vertical:
					if (mousePos.Y < Bounds.Y)  position = 1;
                    else if (mousePos.Y > Bounds.Y + Bounds.Height) position = 0;
                    else position = 1f - ((1f / Bounds.Height) * (mousePos.Y - Bounds.Y));

                    if (m_InvertHighlight == true)
                    {
                        position = 1f - position;
                    }

                    CalculateValue(position, m_RangeSliderInteractionType);
					break;
				default:
					break;
			}

			if (oldValue1 != m_Value1 ||
                oldValue2 != m_Value2)
			{
				OnValueChanged();
			}
		}

        private void CalculateValue(float position, RangeSliderInteractionType type)
        {
            switch (type)
            {
                case RangeSliderInteractionType.Lower:
                    CalculateValue1(position);
                    break;
                case RangeSliderInteractionType.Upper:
                    CalculateValue2(position);
                    break;
                case RangeSliderInteractionType.Center:
                    CalculateCenteredRange(position); 
                    break;
                default:
                    break;
            }
        }

        private void CalculateValue1(float position)
        {
            m_Value1 = CalculateValue_Inner(position);
        }

        private void CalculateValue2(float position)
        {
            m_Value2 = CalculateValue_Inner(position);
        }

        private void SwapIfNeeded()
        {
            if (m_Value2 < m_Value1)
            {
                float temp = m_Value1;

                m_Value1 = m_Value2; 
                m_Value2 = temp;
            }
        }

        private float CalculateValue_Inner(float position)
        {
            switch (m_SliderScale)
            {
                case SliderScale.Linear:
                    return m_MinValue + (position * (m_MaxValue - m_MinValue));
                case SliderScale.Logarithmic:
                    {
                        double minv = 1;
                        double maxv = 1000;

                        double valueScaleLog = 1f / (maxv - minv);
                        double maxvLog = Math.Log(maxv);
                        float valueScale = m_MaxValue - m_MinValue;

                        return m_MinValue + (float)(((Math.Exp(maxvLog * position) - minv) * valueScaleLog) * valueScale);
                    }
                case SliderScale.LogarithmicInverse:
                    {
                        double minv = 1;
                        double maxv = 1000;

                        double valueScaleLog = 1f / (maxv - minv);
                        double maxvLog = Math.Log(maxv);
                        float valueScale = m_MaxValue - m_MinValue;

                        return m_MinValue + (float)(1 - ((Math.Exp(maxvLog * (1 - position)) - minv) * valueScaleLog) * valueScale);
                    }
                default:
                    return 0f;
            }
        }

        private void CalculateCenteredRange(float position)
        {
            m_Value1 = CalculateValue_Inner(Math.Max(Math.Min(position - m_CenterMoveState.Y, 1f), 0f));
            m_Value2 = CalculateValue_Inner(Math.Max(Math.Min(position + m_CenterMoveState.Y, 1f), 0f));
        }

		private void CalculatePercent(float value1, float value2)
		{
            SwapIfNeeded(); 

			switch (m_SliderScale)
			{
				case SliderScale.Linear:
					m_Percent1 = (1f / (m_MaxValue - m_MinValue)) * (m_Value1 - m_MinValue);
                    m_Percent2 = (1f / (m_MaxValue - m_MinValue)) * (m_Value2 - m_MinValue);
					break;
				case SliderScale.Logarithmic:
					{
						double minv = 1;
						double maxv = 1000;

						double valueScaleLog = 1f / (maxv - minv);
						double maxvLog = Math.Log(maxv);
						float valueScale = m_MaxValue - m_MinValue;

						m_Percent1 = (float)(Math.Log(((((m_Value1 - m_MinValue) / valueScale) / valueScaleLog) + minv)) / maxvLog);
                        m_Percent2 = (float)(Math.Log(((((m_Value2 - m_MinValue) / valueScale) / valueScaleLog) + minv)) / maxvLog);
					}
					break;
				case SliderScale.LogarithmicInverse:
					{
						double minv = 1;
						double maxv = 1000;

						double valueScaleLog = 1f / (maxv - minv);
						double maxvLog = Math.Log(maxv);
						float valueScale = m_MaxValue - m_MinValue;

						m_Percent1 = 1f - (float)(Math.Log((((1 - ((m_Value1 - m_MinValue) / valueScale)) / valueScaleLog) + minv)) / maxvLog);
                        m_Percent2 = 1f - (float)(Math.Log((((1 - ((m_Value2 - m_MinValue) / valueScale)) / valueScaleLog) + minv)) / maxvLog);
					}
					break;
				default:
					break;
			}

            m_CenterPercent = m_Percent1 + ((m_Percent2 - m_Percent1) * 0.5f); 
		}

		#region IDynamicUiControl Members

		public void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
            TriangleVerts += 4;
			TriangleIndices += 6;
		}

		public void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			float percent1;
            float percent2;
			RectangleF barBounds;

			if (m_BarAlignment == Dynamic.BarAlignment.Horizontal)
			{
				percent1 = m_Bounds.Width * m_Percent1;
                percent2 = m_Bounds.Width * m_Percent2;

				if (m_InvertHighlight == false)
				{
                    barBounds = new RectangleF(m_Bounds.X + percent1, m_Bounds.Y, percent2 - percent1, m_Bounds.Height - 1);
				}
				else
				{
                    barBounds = new RectangleF(m_Bounds.X + percent1, m_Bounds.Y, percent2 - percent1, m_Bounds.Height - 1);
				}
			}
			else
			{
				percent1 = m_Bounds.Height * m_Percent1; 
                percent2 = m_Bounds.Height * m_Percent2; 

				if (m_InvertHighlight == false)
				{
					barBounds = new RectangleF(m_Bounds.X, m_Bounds.Y + (m_Bounds.Height - percent1), m_Bounds.Width, percent2 - percent1);
				}
				else
				{
                    barBounds = new RectangleF(m_Bounds.X, m_Bounds.Y + percent1, m_Bounds.Width, percent2 - percent1);
				}
			}

			UiStyleHelper.CovertToVertCoords(barBounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);
			
			Color4 backColor;

			if (this.IsFocused)
			{
                backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Open);
			}
			else
			{
                backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Focused);
			}

			WriteTrianglesForBounds(x, y, w, h, ZIndexForOver_Float, backColor, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);
		}

		#endregion

	}
}
