﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using System;

namespace Rug.Game.Ui.Dynamic
{
	public enum BarAlignment { Horizontal, Vertical }

	public class ProgressBar : UiControlBase, IDynamicUiControl
	{
		private float m_MaxValue = 100f;
		private float m_Value = 0f;
		private BarAlignment m_BarAlignment = BarAlignment.Horizontal; 

		public float MaxValue { get { return m_MaxValue; } set { m_MaxValue = value; } }

		public float Value { get { return m_Value; } set { m_Value = value; } }

		public BarAlignment BarAlignment { get { return m_BarAlignment; } set { m_BarAlignment = value; } }

		public override bool BeginPick(Vector2 mousePos, out UiControl control)
		{
			control = null; 
			
			return false;
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control) { }

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);
		}

		public override void WriteVisibleElements(View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
										LineVerts, ref LineVertsCount,
										LinesIndices, ref LinesIndicesCount,
										TriangleVerts, ref TriangleVertsCount,
										TriangleIndices, ref TriangleIndicesCount);			
		}

		#region IDynamicUiControl Members

		public void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			TriangleVerts += 4;
			TriangleIndices += 6;
		}

		public void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			float percent;
			System.Drawing.RectangleF barBounds;

			if (m_BarAlignment == Dynamic.BarAlignment.Horizontal)
			{
				percent = (m_Bounds.Width / m_MaxValue) * Math.Max(Math.Min(m_Value, m_MaxValue), 0);

				barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y, percent, m_Bounds.Height);
			}
			else 
			{
				percent = (m_Bounds.Height / m_MaxValue) * Math.Max(Math.Min(m_Value, m_MaxValue), 0);

				barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y + (m_Bounds.Height - percent), m_Bounds.Width, percent);
			}
						
			UiStyleHelper.CovertToVertCoords(barBounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			Color4 backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Hovering);

			WriteTrianglesForBounds(x, y, w, h, ZIndexForOver_Float, backColor, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);
		}

		#endregion
	}
}
