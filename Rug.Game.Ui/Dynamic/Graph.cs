﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Rendering;

namespace Rug.Game.Ui.Dynamic
{
	public class Graph : UiControlBase, IDynamicUiControl
	{
		#region Private Members
		private float m_MaxValue = 1f;
		private float m_MinValue = 0f;
		private float[] m_Values;

		private SimpleVertex[] m_Verts;
		private uint[] m_Indices;

		private int m_NumberOfDataPoints;
		private int m_CurrentIndex = 0;

		private Color4 m_LineColor = new Color4(1f, 0f, 0f, 1f);
		private Color4 m_ZeroLineColor = new Color4(1f, 1f, 1f, 0.5f);
		private Color4 m_MinorTickLineColor = new Color4(1f, 1f, 1f, 0.1f);
		private Color4 m_MajorTickLineColor = new Color4(1f, 1f, 1f, 0.5f);		

		private bool m_FixedYRange = false;
		private bool m_Scrolling = true;
		private bool m_ScaleEveryFrame = false;
		private bool m_IsNested = true;
		private bool m_ShowTicks = false;
		private int m_TickSpace = 10;
		private int m_MajorTicksEvery = 10;

        // m_InertialScale is a float that runs between zero and one; if it's 0, then no scaling takes place;
        // if it's one then the thing is rescaled completely each time, and if it's less than 1, there's an inertial effect
        private float m_InertialScale = 1.0f;
        private int m_NumberOfDataPointsToRender;
        private bool m_FixedXRange;
		#endregion

		#region Public Properties

        // davidglo addition
        public bool FixedXRange {get {return m_FixedXRange; } set { m_FixedXRange = value; } }
        public int NumberOfDataPointsToRender 
        {
            get { return m_NumberOfDataPointsToRender; } 
            set 
            {
                if (value > m_NumberOfDataPoints)
                {
                    m_NumberOfDataPointsToRender = m_NumberOfDataPoints;
                }
                else
                {
                    m_NumberOfDataPointsToRender = value;
                }
            }
        }
        public float InertialScale { get { return m_InertialScale; } set { m_InertialScale = value; } }
        
        // davidglo end addition

		public bool FixedRange { get { return m_FixedYRange; } set { m_FixedYRange = value; } }
		public float MinValue { get { return m_MinValue; } set { m_MinValue = value; } }
		public float MaxValue { get { return m_MaxValue; } set { m_MaxValue = value; } }

		public float[] Values { get { return m_Values; } }

		public new Color4 LineColor { get { return m_LineColor; } set { m_LineColor = value; } }
		public Color4 ZeroLineColor { get { return m_ZeroLineColor; } set { m_ZeroLineColor = value; } }
		public Color4 MinorTickLineColor { get { return m_MinorTickLineColor; } set { m_MinorTickLineColor = value; } }
		public Color4 MajorTickLineColor { get { return m_MajorTickLineColor; } set { m_MajorTickLineColor = value; } }

		public bool ScaleEveryFrame { get { return m_ScaleEveryFrame; } set { m_ScaleEveryFrame = value; } }

		public bool Scrolling { get { return m_Scrolling; } set { m_Scrolling = value; } }

		public bool IsNested { get { return m_IsNested; } }

		public bool ShowTicks { get { return m_ShowTicks; } set { m_ShowTicks = value; } }

		public int TickSpace { get { return m_TickSpace; } set { m_TickSpace = value; } }

        public int TickOffset { get; set; } 

		public int MajorTicksEvery { get { return m_MajorTicksEvery; } set { m_MajorTicksEvery = value; } }

        public int MajorTickOffset { get; set; }

		#endregion

		public Graph(int numberOfDataPoints)
		{
			m_NumberOfDataPoints = numberOfDataPoints;
            m_NumberOfDataPointsToRender = numberOfDataPoints;
			m_Values = new float[numberOfDataPoints];

			m_Verts = new SimpleVertex[m_NumberOfDataPoints + 1 + 2];
			m_Indices = new uint[(2 * m_NumberOfDataPoints) + 2]; 

            m_FixedXRange = true;
		}

		#region Add Value

		public void AddValue(float value)
		{
			if (m_FixedYRange == false)
			{
				if (value > m_MaxValue)
				{
					m_MaxValue = value;
				}

				if (value < m_MinValue)
				{
					m_MinValue = value;
				}
			}

			m_Values[m_CurrentIndex] = value;

			m_CurrentIndex += 1;

			if (m_CurrentIndex >= m_NumberOfDataPoints) // what to do if the current index exceeds the number of data points
			{
				m_CurrentIndex = 0;
			}

            if (m_FixedXRange == false && m_CurrentIndex >= m_NumberOfDataPointsToRender)
            {
                m_CurrentIndex = 0;
            }
		}

		#endregion

		#region Clear

		public void Clear()
		{
			for (int i = 0; i < m_NumberOfDataPoints; i++)
			{
				m_Values[i] = m_MinValue;
			}

			m_CurrentIndex = 0;
		}

		#endregion

		#region Control Picking

		public override bool BeginPick(Vector2 mousePos, out UiControl control)
		{
			control = null;

			return false;
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control) { }

		#endregion

		#region Write Elements

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			m_IsNested = Parent != null && Parent is MultiGraph; 

			if (m_IsNested == false)
			{
				base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);
			}

			if (m_ShowTicks == true)
			{
				int ticks = (m_Values.Length / m_TickSpace) - 1;

				if (ticks > 0)
				{
					LineVerts += ticks * 2;
					LinesIndices += ticks * 2;
				}
			}
		}

		public override void WriteVisibleElements(View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			if (m_IsNested == false)
			{
				base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
											LineVerts, ref LineVertsCount,
											LinesIndices, ref LinesIndicesCount,
											TriangleVerts, ref TriangleVertsCount,
											TriangleIndices, ref TriangleIndicesCount);
			}

			if (m_ShowTicks == true)
			{
				int ticks = (m_Values.Length / m_TickSpace) - 1;

				if (ticks > 0)
				{
					float x, y, w, h;

					if (m_IsNested == true)
					{
						UiStyleHelper.CovertToVertCoords(Parent.Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);
					}
					else
					{
						UiStyleHelper.CovertToVertCoords(Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);						
					}

					int i = LineVertsCount;
					float xInc = w / (ticks + 1);
					float xOffset = x + ((xInc / (float)TickSpace) * (float)TickOffset);

					for (int j = 0; j < ticks; j++)
					{						
						Color4 lineColor;

                        if ((j + 1 + MajorTickOffset) % m_MajorTicksEvery == 0)
						{							
							lineColor = m_MajorTickLineColor; 
						}
						else
						{
							lineColor = m_MinorTickLineColor; 
						}

						LineVerts.WriteRange(new SimpleVertex[] { 
							new SimpleVertex() { Color = lineColor, Position = new Vector3(xOffset, y, ZIndexForOver_Float), TextureCoords = new Vector2(0, 0) },
							new SimpleVertex() { Color = lineColor, Position = new Vector3(xOffset, y + h, ZIndexForOver_Float), TextureCoords = new Vector2(0, 0) }
						});

						LinesIndices.WriteRange(new int[] { 
							i + 0, i + 1, 
						});

						LineVertsCount += 2;
						LinesIndicesCount += 2;

						xOffset += xInc;

						i += 2; 
					}
				}
			}
		}

		public virtual void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			LineVerts += m_NumberOfDataPoints + 1 + 2;
			LinesIndices += (2 * m_NumberOfDataPoints) + 2;
		}

		// This Method is a candidate for opto! 
		public virtual void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			float z = ZIndexForLines_Float; 

			if (m_IsNested == true)
			{
				UiStyleHelper.CovertToVertCoords(Parent.Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h); 
			}
			else
			{
				UiStyleHelper.CovertToVertCoords(Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);
			}

			if (m_ScaleEveryFrame == true)
			{
				// this rescales the graph to check what should be the max & min
				float oldMax = m_MaxValue;
                
                m_MaxValue = m_Values[0];
				m_MinValue = m_Values[0];

				for (int i = 1; i < m_Values.Length; ++i)
				{
					if (m_Values[i] > m_MaxValue)
					{
						m_MaxValue = m_Values[i];
					}

					if (m_Values[i] < m_MinValue)
					{
						m_MinValue = m_Values[i];
					}
				}

                if (m_MaxValue < oldMax)
				{
                    m_MaxValue = oldMax - m_InertialScale * (oldMax - m_MaxValue); 
                }
			}

			if ((m_MaxValue - m_MinValue) == 0.0f)
			{
				m_MaxValue = m_MinValue + 0.1f;
			}

			float xDelta = w / m_NumberOfDataPointsToRender;
			float yDelta = h / (m_MaxValue - m_MinValue);
			float yOff = -m_MinValue;
			float value = 0;

			int vIndex = 0;
			int iIndex = 0; 

			if (m_MinValue < 0 && m_MaxValue > 0)
			{
				value = 0;

				value = value < m_MinValue ? m_MinValue : value > m_MaxValue ? m_MaxValue : value;

				value += yOff;

				m_Verts[vIndex++] = new SimpleVertex() { Color = m_ZeroLineColor, Position = new Vector3(x, (y + h) - (yDelta * value), z), TextureCoords = new Vector2(0, 0) };
				m_Verts[vIndex++] = new SimpleVertex() { Color = m_ZeroLineColor, Position = new Vector3(x + w, (y + h) - (yDelta * value), z), TextureCoords = new Vector2(0, 0) };
				m_Indices[iIndex++] = (uint)LineVertsCount++;
				m_Indices[iIndex++] = (uint)LineVertsCount++;

				LinesIndicesCount += 2;
			}

			int lv = LineVertsCount;
			int li = LinesIndicesCount;

			if (m_Scrolling == true)
			{
				for (int i = m_CurrentIndex; i < m_NumberOfDataPointsToRender; i++)
				{
					value = m_Values[i];

					value = value < m_MinValue ? m_MinValue : value > m_MaxValue ? m_MaxValue : value;

					value += yOff;

					m_Verts[vIndex++] = new SimpleVertex() { Color = m_LineColor, Position = new Vector3(x, (y + h) - (yDelta * value), z), TextureCoords = new Vector2(0, 0) };
					m_Indices[iIndex++] = (uint)lv++;
					m_Indices[iIndex++] = (uint)lv;

					x += xDelta;
				}

				for (int i = 0; i < m_CurrentIndex; i++)
				{
					value = m_Values[i];

					value = value < m_MinValue ? m_MinValue : value > m_MaxValue ? m_MaxValue : value;

					value += yOff;

					m_Verts[vIndex++] = new SimpleVertex() { Color = m_LineColor, Position = new Vector3(x, (y + h) - (yDelta * value), z), TextureCoords = new Vector2(0, 0) };
					m_Indices[iIndex++] = (uint)lv++;
					m_Indices[iIndex++] = (uint)lv;

					x += xDelta;
				}
			}
			else
			{
				for (int i = 0; i < m_NumberOfDataPointsToRender; i++)
				{
					value = m_Values[i];

					value = value < m_MinValue ? m_MinValue : value > m_MaxValue ? m_MaxValue : value;

					value += yOff;

					m_Verts[vIndex++] = new SimpleVertex() { Color = m_LineColor, Position = new Vector3(x, (y + h) - (yDelta * value), z), TextureCoords = new Vector2(0, 0) };
					m_Indices[iIndex++] = (uint)lv++;
					m_Indices[iIndex++] = (uint)lv;

					x += xDelta;
				}
			}

			m_Verts[vIndex++] = new SimpleVertex() { Color = m_LineColor, Position = new Vector3(x, (y + h) - (yDelta * value), z), TextureCoords = new Vector2(0, 0) };

			LineVerts.WriteRange(m_Verts, 0, vIndex);
			LinesIndices.WriteRange(m_Indices, 0, iIndex); 

			LineVertsCount += m_NumberOfDataPointsToRender + 1;
			LinesIndicesCount += 2 * m_NumberOfDataPointsToRender;
		}

		#endregion

    }
}
