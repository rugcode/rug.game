﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Text;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Dynamic
{
	public class TextBox : UiControlBase, IDynamicUiControl
	{
		private int m_MaxLength = 30;
		private int m_Carat = 0; 
		private string m_Text = "";

		private FontType m_FontType = FontType.Small;
		private System.Windows.Forms.Padding m_Padding = new System.Windows.Forms.Padding(4);
		private ContentAlignment m_TextAlign = ContentAlignment.MiddleCenter;		

		public int MaxLength { get { return m_MaxLength; } set { m_MaxLength = value; } }

		public int Carat { get { return m_Carat; } set { m_Carat = value; } }
		
		public string Text { get { return m_Text; } set { m_Text = value; } }

		public FontType FontType { get { return m_FontType; } set { m_FontType = value; } }

		public ContentAlignment TextAlign { get { return m_TextAlign; } set { m_TextAlign = value; } }

		public System.Windows.Forms.Padding Padding { get { return m_Padding; } set { m_Padding = value; } }

		public event EventHandler TextChanged; 

		public TextBox()
		{
			InteractionType = ControlInteractionType.Drag;
			ControlStyle = DisplayMode.Normal; 
		}

		private void OnTextChanged()
		{
			if (TextChanged != null)
			{
				TextChanged(this, EventArgs.Empty); 
			}
		}

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false;
			base.DoMouseInteraction(mouseState, mouseButtons, mousePos, out shouldUpdate);
		}

		public override void DoKeyInteraction(KeyInteractionType keyInteractionType, char @char, System.Windows.Forms.KeyEventArgs eventArgs, out bool shouldUpdate)
		{
			shouldUpdate = false; 
			switch (keyInteractionType)
			{
				case KeyInteractionType.KeyPress:
					if (@char == '\b')
					{
						if (m_Text.Length > 0)
						{
							m_Text = m_Text.Substring(0, m_Text.Length - 1);
							OnTextChanged();
						}
					}
					else
					{
						m_Text += @char;
						OnTextChanged();						
					}

					shouldUpdate = true; 
					break;
				case KeyInteractionType.KeyUp:					
					break;
				case KeyInteractionType.KeyDown:
					if (eventArgs.KeyCode == System.Windows.Forms.Keys.Delete)
					{

					}
					break;
				default:
					base.DoKeyInteraction(keyInteractionType, @char, eventArgs, out shouldUpdate);
					break;
			}			
		}

		#region IDynamicUiControl Members

		public void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			int textIndexCount, textTriangleCount;
			TextRenderHelper.GetTotalElementCounts(m_MaxLength, out textIndexCount, out textTriangleCount);

			TriangleVerts += textTriangleCount;
			TriangleIndices += textIndexCount;
		}

		public void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			SizeF stringSize = TextRenderHelper.MessureString(m_Text, m_FontType, view, 1f);

			float x, y;
			float px = view.PixelSize.X;
			float py = view.PixelSize.Y;

			PointF textLocation = UiStyleHelper.LayoutTextBounds(Bounds, stringSize, m_TextAlign, Padding);

			UiStyleHelper.CovertToVertCoords_Relitive(textLocation, view.PixelSize, out x, out y);

			Color4 textColor = UiStyleHelper.GetControlTextColor(this, DisplayMode.Normal);

			TextRenderHelper.WriteString(view, Bounds,
											m_Text, m_FontType, new Vector3(x, y, ZIndexForLines_Float), 1f, textColor,
											TriangleVerts, ref TriangleVertsCount,
											TriangleIndices, ref TriangleIndicesCount);
		}

		#endregion
	}
}
