﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using System;

namespace Rug.Game.Ui.Dynamic
{
	public delegate void SliderValueChangedEvent(Slider sender, float value);


	public class Slider : UiControlBase, IDynamicUiControl
	{
		private float m_MinValue = 0f;
		private float m_MaxValue = 100f;
		private float m_Value = 0f;
		private float m_Percent;
		private bool m_Enabled = true;

		private SliderScale m_SliderScale = SliderScale.Linear;
		private BarAlignment m_BarAlignment = BarAlignment.Horizontal;
		private bool m_InvertHighlight = false;

		public bool Enabled { get { return m_Enabled; } set { m_Enabled = value; } } 

		public float MinValue { get { return m_MinValue; } set { m_MinValue = value; } }
		public float MaxValue { get { return m_MaxValue; } set { m_MaxValue = value; } }			

		public float Value
		{
			get { return m_Value; } 
			set 
			{
				if (m_Value != value)
				{
					m_Value = value;
					OnValueChanged(); 
				}
			} 
		}

		public BarAlignment BarAlignment { get { return m_BarAlignment; } set { m_BarAlignment = value; } }

		public bool InvertHighlight { get { return m_InvertHighlight; } set { m_InvertHighlight = value; } }

		public SliderScale SliderScale 
        { 
            get { return m_SliderScale; } 
            set 
            { 
                m_SliderScale = value;

                CalculatePercent();
            } 
        } 

		public event SliderValueChangedEvent ValueChanged;

		public Slider()
		{
			InteractionType = ControlInteractionType.Drag;
			ControlStyle = DisplayMode.Normal; 
		}

		private void OnValueChanged()
		{
			CalculatePercent(); 

			if (ValueChanged != null)
			{
				ValueChanged(this, m_Value);
			}
		}


		public void SetValue_NoEvent(float value)
		{
			m_Value = value;
			CalculatePercent(); 
		}

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false; 
			switch (mouseState)
			{
				case MouseState.DragStart:
				case MouseState.Dragging:
				case MouseState.DragEnd:
					if (m_Enabled == true) CalculateValue(mousePos);
					break;
				default:
					base.DoMouseInteraction(mouseState, mouseButtons, mousePos, out shouldUpdate);
					break;
			}			
		}		

		private void CalculateValue(Vector2 mousePos)
		{
			float oldValue = m_Value; 

			switch (m_BarAlignment)
			{
				case BarAlignment.Horizontal:
					if (mousePos.X < Bounds.X) m_Value = m_MinValue;
					else if (mousePos.X > Bounds.X + Bounds.Width) m_Value = m_MaxValue;
					else CalculateValue((1f / Bounds.Width) * (mousePos.X - Bounds.X));
					break;
				case BarAlignment.Vertical:
					if (mousePos.Y < Bounds.Y) m_Value = m_MaxValue;
					else if (mousePos.Y > Bounds.Y + Bounds.Height) m_Value = m_MinValue;
					else CalculateValue(1f - ((1f / Bounds.Height) * (mousePos.Y - Bounds.Y))); 
					break;
				default:
					break;
			}

			if (oldValue != m_Value)
			{
				OnValueChanged();
			}
		}

		private void CalculateValue(float position)
		{			
			switch (m_SliderScale)
			{
				case SliderScale.Linear:
					m_Value = m_MinValue + (position * (m_MaxValue - m_MinValue));
					break;
				case SliderScale.Logarithmic:
					{
						double minv = 1;
						double maxv = 1000;

						double valueScaleLog = 1f / (maxv - minv);
						double maxvLog = Math.Log(maxv);
						float valueScale = m_MaxValue - m_MinValue;

						m_Value = m_MinValue + (float)(((Math.Exp(maxvLog * position) - minv) * valueScaleLog) * valueScale);
					}
					break;
				case SliderScale.LogarithmicInverse:
					{
						double minv = 1;
						double maxv = 1000;

						double valueScaleLog = 1f / (maxv - minv);
						double maxvLog = Math.Log(maxv);
						float valueScale = m_MaxValue - m_MinValue;

						m_Value = m_MinValue + (float)(1 - ((Math.Exp(maxvLog * (1 - position)) - minv) * valueScaleLog) * valueScale);
					}
					break;
				default:
					break;
			}
		}

		private void CalculatePercent()
		{
			float value = Math.Max(Math.Min(m_Value, m_MaxValue), m_MinValue);

			switch (m_SliderScale)
			{
				case SliderScale.Linear:
					m_Percent = (1f / (m_MaxValue - m_MinValue)) * (value - m_MinValue);
					break;
				case SliderScale.Logarithmic:
					{
						double minv = 1;
						double maxv = 1000;

						double valueScaleLog = 1f / (maxv - minv);
						double maxvLog = Math.Log(maxv);
						float valueScale = m_MaxValue - m_MinValue;

						m_Percent = (float)(Math.Log(((((value - m_MinValue) / valueScale) / valueScaleLog) + minv)) / maxvLog);
					}
					break;
				case SliderScale.LogarithmicInverse:
					{
						double minv = 1;
						double maxv = 1000;

						double valueScaleLog = 1f / (maxv - minv);
						double maxvLog = Math.Log(maxv);
						float valueScale = m_MaxValue - m_MinValue;

						m_Percent = 1f - (float)(Math.Log((((1 - ((value - m_MinValue) / valueScale)) / valueScaleLog) + minv)) / maxvLog);
					}
					break;
				default:
					break;
			}
		}

		#region IDynamicUiControl Members

		public void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			TriangleVerts += 4;
			TriangleIndices += 6;
		}

		public void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			float percent;
			System.Drawing.RectangleF barBounds;

			if (m_BarAlignment == Dynamic.BarAlignment.Horizontal)
			{
				percent = m_Bounds.Width * m_Percent;

				if (m_InvertHighlight == false)
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y, percent, m_Bounds.Height - 1);
				}
				else
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X + percent, m_Bounds.Y, m_Bounds.Width - percent, m_Bounds.Height - 1);
				}
			}
			else
			{
				percent = m_Bounds.Height * m_Percent;

				if (m_InvertHighlight == false)
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y + (m_Bounds.Height - percent), m_Bounds.Width, percent);
				}
				else
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y, m_Bounds.Width, m_Bounds.Height - percent);
				}
			}

			UiStyleHelper.CovertToVertCoords(barBounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);
			
			Color4 backColor;

			if (m_Enabled == false)
			{
				backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Dimmed);
			}
			else if (this.IsFocused)
			{
				backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Open);
			}
			else
			{
				backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Hovering);
			}

			WriteTrianglesForBounds(x, y, w, h, ZIndexForOver_Float, backColor, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);
		}

		#endregion

	}
}
