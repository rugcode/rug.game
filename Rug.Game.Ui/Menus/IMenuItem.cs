﻿using Rug.Game.Core.Rendering;
using System;

namespace Rug.Game.Ui.Menus
{
	public interface IMenuItem
	{
		bool IsOpen { get; set; }
		event EventHandler Click;
		bool DoClick(); 

		void GetTotalSelfElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices);
		void GetTotalSubElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices);

		float MessureTextWidth(View3D view); 
	}
}
