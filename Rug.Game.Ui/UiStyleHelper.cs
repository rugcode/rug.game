﻿using OpenTK;
using OpenTK.Graphics;
using System.Drawing;
using System.Windows.Forms;

namespace Rug.Game.Ui
{
	public enum DisplayMode : int 
	{ 
		Auto = -1, 
		Normal = 0, 
		Focused = 1, 
		Hovering = 2, 
		Open = 3, 
		Highlight = 4, 
		Dimmed = 5
	}

	public static class UiStyleHelper
	{
		public static UiTheme Theme = new UiTheme(); 

		#region Layout Control Bounds
		
		public static System.Drawing.RectangleF LayoutControlBounds(System.Drawing.RectangleF ParentBounds, 
													 Point Location, Size Size, 
													 AnchorStyles Anchor, DockStyle Docking,
													 out System.Drawing.RectangleF RemainingBounds) 
		{
			float x, y, w, h;

			switch (Docking)
			{
				case DockStyle.Top:
					x = ParentBounds.X;
					y = ParentBounds.Y;
					w = ParentBounds.Width;
					h = Size.Height;

					RemainingBounds = new System.Drawing.RectangleF(ParentBounds.X, ParentBounds.Y + Size.Height, ParentBounds.Width, ParentBounds.Height - Size.Height); 
					break;
				case DockStyle.Bottom:
					x = ParentBounds.X;
					y = ParentBounds.Bottom - Size.Height;
					w = ParentBounds.Width;
					h = Size.Height;

					RemainingBounds = new System.Drawing.RectangleF(ParentBounds.X, ParentBounds.Y, ParentBounds.Width, ParentBounds.Height - Size.Height); 
					break;
				case DockStyle.Left:
					x = ParentBounds.X;
					y = ParentBounds.Y;
					w = Size.Width;
					h = ParentBounds.Height;

					RemainingBounds = new System.Drawing.RectangleF(ParentBounds.X + Size.Width, ParentBounds.Y, ParentBounds.Width - Size.Width, ParentBounds.Height); 
					break;
				case DockStyle.Right:
					x = ParentBounds.Right - Size.Width;
					y = ParentBounds.Y;
					w = Size.Width;
					h = ParentBounds.Height;

					RemainingBounds = new System.Drawing.RectangleF(ParentBounds.X, ParentBounds.Y, ParentBounds.Width - Size.Width, ParentBounds.Height); 
					break;
				case DockStyle.Fill:
					x = ParentBounds.X;
					y = ParentBounds.Y;
					w = ParentBounds.Width;
					h = ParentBounds.Height;

					RemainingBounds = ParentBounds; 
					break;
				default:
					x = ParentBounds.X + Location.X;
					y = ParentBounds.Y + Location.Y;
					w = Size.Width;
					h = Size.Height;

					RemainingBounds = ParentBounds; 
					break;
			}

			System.Drawing.RectangleF bounds = new System.Drawing.RectangleF(x, y, w, h);

			return bounds;
		}

		public static PointF LayoutTextBounds(System.Drawing.RectangleF Bounds, SizeF stringSize, ContentAlignment TextAlign, Padding Padding)
		{
			float width = Bounds.Width - Padding.Horizontal;
			float height = Bounds.Height - Padding.Vertical; 

			switch (TextAlign)
			{
				case ContentAlignment.BottomCenter:
					return new PointF(Padding.Left + ((width - stringSize.Width) * 0.5f), (height - (stringSize.Height + Padding.Bottom))); 
				case ContentAlignment.BottomLeft:
					return new PointF(Padding.Left, (height - (stringSize.Height + Padding.Bottom))); 
				case ContentAlignment.BottomRight:
					return new PointF((Bounds.Width - (stringSize.Width + Padding.Right)), (height - (stringSize.Height + Padding.Bottom))); 
				case ContentAlignment.MiddleCenter:
					return new PointF(Padding.Left + ((width - stringSize.Width) * 0.5f), Padding.Top + ((height - stringSize.Height) * 0.5f)); 					
				case ContentAlignment.MiddleLeft:
					return new PointF(Padding.Left, Padding.Top + ((height - stringSize.Height) * 0.5f)); 
				case ContentAlignment.MiddleRight:
					return new PointF((Bounds.Width - stringSize.Width), Padding.Top + ((height - stringSize.Height) * 0.5f)); 
				case ContentAlignment.TopCenter:
					return new PointF(Padding.Left + ((width - stringSize.Width) * 0.5f), Padding.Top); 
				case ContentAlignment.TopLeft:
					return new PointF(Padding.Left, Padding.Top);
				case ContentAlignment.TopRight:
					return new PointF((Bounds.Width - stringSize.Width), Padding.Top);
				default:
					return new PointF(Padding.Left + ((width - stringSize.Width) * 0.5f), Padding.Top + ((height - stringSize.Height) * 0.5f));
			}	
		}

		#endregion

		#region Covert To Vert Coords

		public static RectangleF CovertToVertRectangle(System.Drawing.RectangleF Bounds, Vector2 WindowSize, Vector2 PixelSize)
		{
			float x = (Bounds.X - (WindowSize.X / 2)) * PixelSize.X;
			float y = -(Bounds.Y - (WindowSize.Y / 2)) * PixelSize.Y;
			float w = Bounds.Width * PixelSize.X;
			float h = -Bounds.Height * PixelSize.Y;

			return new RectangleF(x, y, w, h); 
		}

		public static void CovertToVertCoords(System.Drawing.RectangleF Bounds, Vector2 WindowSize, Vector2 PixelSize, out float x, out float y, out float w, out float h)
		{
			x = (Bounds.X - (WindowSize.X / 2)) * PixelSize.X;
			y = -(Bounds.Y - (WindowSize.Y / 2)) * PixelSize.Y;
			w = Bounds.Width * PixelSize.X;
			h = -Bounds.Height * PixelSize.Y;
		}

        public static Vector2 CovertToVertCoords(PointF Location, Vector2 WindowSize, Vector2 PixelSize)
        {
            return new Vector2((Location.X - (WindowSize.X / 2)) * PixelSize.X, -(Location.Y - (WindowSize.Y / 2)) * PixelSize.Y);
        }

        public static Vector2 CovertToVertCoords(Vector2 Location, Vector2 WindowSize, Vector2 PixelSize)
        {
            return new Vector2((Location.X - (WindowSize.X / 2)) * PixelSize.X, -(Location.Y - (WindowSize.Y / 2)) * PixelSize.Y);
        }

		public static void CovertToVertCoords(PointF Location, Vector2 WindowSize, Vector2 PixelSize, out float x, out float y)
		{
			x = (Location.X - (WindowSize.X / 2)) * PixelSize.X;
			y = -(Location.Y - (WindowSize.Y / 2)) * PixelSize.Y;
		}

		public static void CovertToVertCoords(float px, float py, Vector2 WindowSize, Vector2 PixelSize, out float x, out float y)
		{
			x = (px - (WindowSize.X / 2)) * PixelSize.X;
			y = -(py - (WindowSize.Y / 2)) * PixelSize.Y;
		}

		public static void CovertToVertCoords_Relitive(System.Drawing.RectangleF Bounds, Vector2 PixelSize, out float x, out float y, out float w, out float h)
		{
			x = Bounds.X * PixelSize.X;
			y = -Bounds.Y * PixelSize.Y;
			w = Bounds.Width * PixelSize.X;
			h = -Bounds.Height * PixelSize.Y;
		}

		public static void CovertToVertCoords_Relitive(PointF Location, Vector2 PixelSize, out float x, out float y)
		{
			x = Location.X * PixelSize.X;
			y = -Location.Y * PixelSize.Y;
		}

		public static void CovertToVertCoords_Relitive(float px, float py, Vector2 PixelSize, out float x, out float y)
		{
			x = px * PixelSize.X;
			y = -py * PixelSize.Y;
		}

		#endregion

		#region Color Styles 

		public static DisplayMode GetModeForControl(UiControl control, DisplayMode mode)
		{
			DisplayMode selectedMode = mode;

			if (mode == DisplayMode.Auto)
			{
				if (control.IsHovering == true)
				{
					selectedMode = DisplayMode.Hovering;
				}
				else if (control.IsFocused == true)
				{
					selectedMode = DisplayMode.Focused;
				}
				else
				{
					selectedMode = DisplayMode.Normal;
				}
			}

			return selectedMode; 
		}
		
		public static Color4 GetControlLineColor(UiControl control, DisplayMode mode)
		{
			return Theme[GetModeForControl(control, mode)].LineColor; 
		}

		public static Color4 GetControlBackColor(UiControl control, DisplayMode mode)
		{
			return Theme[GetModeForControl(control, mode)].BackColor; 
		}

		public static Color4 GetControlTextColor(UiControl control, DisplayMode mode)
		{
			return Theme[GetModeForControl(control, mode)].TextColor; 
		}

		#endregion
	}
}