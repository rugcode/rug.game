﻿
namespace Rug.Game.Ui
{
	public interface IUiControlContainer
	{
		void AttachDynamicControls();
		void DetachDynamicControls();
	}
}
