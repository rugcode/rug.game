﻿using System;
using System.Windows.Forms;

namespace Rug.Game.Ui.Dialogs
{
	public delegate void DialogResultEventHandler(object sender, DialogResultEventArgs args); 

	public class DialogResultEventArgs : EventArgs
	{
		public DialogResult Result;  
	}
}
