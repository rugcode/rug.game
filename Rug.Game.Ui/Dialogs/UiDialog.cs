﻿using OpenTK;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Text;
using Rug.Game.Ui.Controls;
using System;

namespace Rug.Game.Ui.Dialogs
{
	public abstract class UiDialog : UiControlBase, IUiControlContainer
	{
		#region Private Members

		private Panel m_HeaderPanel;
		private Button m_CloseButton;
		private UiDialogHeader m_Header;
		private System.Windows.Forms.DialogResult m_DialogResult;

		#endregion

		#region Public Events

		public event DialogResultEventHandler Closing;
		
		public event EventHandler Opening;

		#endregion

		#region Public Properties

		public readonly UiControlCollection<UiDialog, UiControl> Controls;		
		
		public string Title { get { return m_Header.Text; } set { m_Header.Text = value; } }

		public System.Windows.Forms.DialogResult DialogResult { get { return m_DialogResult; } set { m_DialogResult = value; } }

		public int HeaderSize { get { return m_HeaderPanel.Size.Height; } }

		public bool ShowCloseButton
		{
			get
			{
				return m_CloseButton.IsVisible; 
			}
			set
			{				
				m_CloseButton.IsVisible = value; 
			}
		}

		#endregion

		#region Constructor

		public UiDialog()
		{
			Controls = new UiControlCollection<UiDialog, UiControl>(this);

			m_HeaderPanel = new Panel();

			m_Header = new UiDialogHeader(this);

			m_CloseButton = new Button();
			
			this.IsVisible = false; 
		}

		#endregion

		#region Initialize Controls

		public virtual void InitializeControls()
		{
			int index = 20;

			
			m_HeaderPanel.Docking = System.Windows.Forms.DockStyle.Top; 
			m_HeaderPanel.Location = new System.Drawing.Point(0, 0);
			m_HeaderPanel.Size = new System.Drawing.Size(this.Size.Width, 24);
			m_HeaderPanel.RelitiveZIndex = index++;
			m_HeaderPanel.IsVisible = true;
			m_HeaderPanel.Parent = this;
			m_HeaderPanel.ShowBorder = false;	
			//Controls.Add(m_HeaderPanel);

			index = 80;
			
			m_CloseButton.Docking = System.Windows.Forms.DockStyle.Right;
			m_CloseButton.Location = new System.Drawing.Point(this.Size.Width - 24, 0);
			m_CloseButton.Size = new System.Drawing.Size(24, 24);
			m_CloseButton.RelitiveZIndex = index++;
			m_CloseButton.FontType = FontType.Small;
			m_CloseButton.Text = "X";
			m_CloseButton.Click += new EventHandler(CloseButton_Click);
			m_CloseButton.ShowBorder = true;
			m_CloseButton.Borders = ControlBorder.Right | ControlBorder.Bottom | ControlBorder.Top; 

			m_HeaderPanel.Controls.Add(m_CloseButton);
			
			//Header.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
			m_Header.Docking = System.Windows.Forms.DockStyle.Top;
			m_Header.Location = new System.Drawing.Point(0, 0);
			m_Header.Size = new System.Drawing.Size(this.Size.Width - 24, 24);
			m_Header.RelitiveZIndex = index++;
			m_Header.Text = "Dialog";
			m_Header.FontType = Rug.Game.Core.Text.FontType.Small;
			m_Header.IsVisible = true;
			m_Header.ShowBorder = true;
			m_Header.Borders = ControlBorder.Right | ControlBorder.Left | ControlBorder.Bottom | ControlBorder.Top; 

			m_HeaderPanel.Controls.Add(m_Header); 
		}

		#endregion

		#region Pick

		public override bool BeginPick(Vector2 mousePos, out UiControl control)
		{
			if (m_HeaderPanel.BeginPick(mousePos, out control) == true)
			{
				return true; 
			}

			foreach (UiControl ctrl in Controls)
			{
				if (ctrl.IsVisible == true && 
					ctrl.BeginPick(mousePos, out control))
				{
					return true; 
				}
			}

			control = null; 
			
			return false; 
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control) { }

		#endregion 

		#region Write Elements

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);

			m_HeaderPanel.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);

			foreach (UiControl ctrl in Controls)
			{
				ctrl.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);
			}
		}

		public override void WriteVisibleElements(View3D view,
													System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds,
													DataStream LineVerts, ref int LineVertsCount, 
													DataStream LinesIndices, ref int LinesIndicesCount, 
													DataStream TriangleVerts, ref int TriangleVertsCount, 
													DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
									LineVerts, ref LineVertsCount, 
									LinesIndices, ref LinesIndicesCount, 
									TriangleVerts, ref TriangleVertsCount, 
									TriangleIndices, ref TriangleIndicesCount);

			System.Drawing.RectangleF innerBounds = Bounds;

			m_HeaderPanel.WriteVisibleElements(view, ClientBounds, ref innerBounds,
			                        LineVerts, ref LineVertsCount,
			                        LinesIndices, ref LinesIndicesCount,
			                        TriangleVerts, ref TriangleVertsCount,
			                        TriangleIndices, ref TriangleIndicesCount);

			foreach (UiControl ctrl in Controls)
			{
				if (ctrl.IsVisible == false)
				{
					continue; 
				}

				ctrl.WriteVisibleElements(view, ClientBounds, ref innerBounds,
											LineVerts, ref LineVertsCount,
											LinesIndices, ref LinesIndicesCount,
											TriangleVerts, ref TriangleVertsCount,
											TriangleIndices, ref TriangleIndicesCount);
			}
		}

		#endregion

		#region IUiControlContainer Members

		public void AttachDynamicControls()
		{
			Controls.AttachDynamicControls();
		}

		public void DetachDynamicControls()
		{
			Controls.DetachDynamicControls();
		}

		#endregion

		#region Open / Close Events

		protected abstract void OnOpen();

		protected abstract void OnClose();

		public void Show()
		{
			this.IsVisible = true;
			
			OnOpen();

			if (Opening != null) 
			{
				Opening(this, EventArgs.Empty); 
			}
		}

		public void Close()
		{
			this.IsVisible = false; 

			OnClose();

			if (Closing != null)
			{
				DialogResultEventArgs args = new DialogResultEventArgs();
				
				args.Result = m_DialogResult;

				Closing(this, args); 
			}
		}

		#endregion

		void CloseButton_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel; 

			Close(); 
		}
	}
}
