﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Rendering;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Dialogs.ColorPicker
{
	class GradiantBox : UiControlBase, IDynamicUiControl
	{
		#region Members 

		private ColorRange m_DrawStyle = ColorRange.Hue;
		private Point m_Marker = new Point(0, 0);

		private HSL m_HSL;
		private Color m_RGB;
		
		#endregion 

		#region Properties

		public ColorRange DrawStyle
		{
			get { return m_DrawStyle; }
			set 
			{
				m_DrawStyle = value;
				ResetMarker();
				Invalidate();
			}
		}

		public HSL HSL
		{
			get { return m_HSL; }
			set 
			{ 
				m_HSL = value;
				m_RGB = value.Color; 
				ResetMarker();
				Invalidate();
			}
		}

		public Color RGB
		{
			get { return m_RGB; }
			set 
			{ 
				m_RGB = value;
				m_HSL = new HSL(value); 
				ResetMarker();
				Invalidate();
			}
		}

		#endregion 

		#region Events

		public new event EventHandler Scroll;

		#endregion

		#region Constructor 

		public GradiantBox()
		{
			InteractionType = ControlInteractionType.Click | ControlInteractionType.Drag; 
			
			m_HSL = new HSL();
			m_HSL.H = 1.0;
			m_HSL.S = 1.0;
			m_HSL.L = 1.0;
			
			m_RGB = m_HSL.Color;

			m_DrawStyle = ColorRange.Luminance;

			this.ShowBackground = false; 
		}

		#endregion 

		private void Invalidate()
		{
			if (this.Scene != null)
			{
				this.Scene.Invalidate(); 
			}
		}

		#region  Marker to color / Color to Marker

		/// <summary>
		/// Returns the graphed color at the r,g position on the control
		/// </summary>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <returns></returns>
		private HSL GetColor(int x, int y)
		{

			HSL _hsl = new HSL();

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					_hsl.H = m_HSL.H;
					_hsl.S = (double)x / (Size.Width - 2);
					_hsl.L = 1.0 - (double)y / (Size.Height - 2);
					break;
				case ColorRange.Saturation:
					_hsl.S = m_HSL.S;
					_hsl.H = (double)x / (Size.Width - 2);
					_hsl.L = 1.0 - (double)y / (Size.Height - 2);
					break;
				case ColorRange.Luminance:
					_hsl.L = m_HSL.L;
					_hsl.H = (double)x / (Size.Width - 2);
					_hsl.S = 1.0 - (double)y / (Size.Height - 2);
					break;
				case ColorRange.Red:
					_hsl = new HSL(Color.FromArgb(m_RGB.R, HSL.ClipColor(255 * (1.0 - (double)y / (Size.Height - 2))), HSL.ClipColor(255 * (double)x / (Size.Width - 2))));
					break;
				case ColorRange.Green:
					_hsl = new HSL(Color.FromArgb(HSL.ClipColor(255 * (1.0 - (double)y / (Size.Height - 2))), m_RGB.G, HSL.ClipColor(255 * (double)x / (Size.Width - 2))));
					break;
				case ColorRange.Blue:
					_hsl = new HSL(Color.FromArgb(HSL.ClipColor(255 * (double)x / (Size.Width - 2)), HSL.ClipColor(255 * (1.0 - (double)y / (Size.Height - 2))), m_RGB.B));
					break;
			}

			return _hsl;
		}

		/// <summary>
		/// Sets the marker position to the active color
		/// </summary>
		private void ResetMarker()
		{
			double x = 0, y = 0;

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					x = m_HSL.S * (Size.Width - 2);
					y = (1.0 - m_HSL.L) * (Size.Height - 2);
					break;
				case ColorRange.Saturation:
					x = (m_HSL.H * (Size.Width - 2));
					y = ((1.0 - m_HSL.L) * (Size.Height - 2));
					break;
				case ColorRange.Luminance:
					x = (m_HSL.H * (Size.Width - 2));
					y = ((1.0 - m_HSL.S) * (Size.Height - 2));
					break;
				case ColorRange.Red:
					x = (((double)m_RGB.B / 255) * (Size.Width - 2));
					y = ((1.0 - ((double)m_RGB.G / 255)) * (Size.Height - 2));
					break;
				case ColorRange.Green:
					x = (((double)m_RGB.B / 255) * (Size.Width - 2));
					y = ((1.0 - ((double)m_RGB.R / 255)) * (Size.Height - 2));
					break;
				case ColorRange.Blue:
					x = ((double)m_RGB.R / 255) * (Size.Width - 2f);
					y = ((1.0 - ((double)m_RGB.G / 255)) * (Size.Height - 2));
					break;
			}

			m_Marker.X = (int)(x);
			m_Marker.Y = (int)(y);

		}

		/// <summary>
		/// Resets the controls color (both HSL and RGB variables) based on the current marker position
		/// </summary>
		private void ResetHSLRGB()
		{
			int red, green, blue;

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					m_HSL.S = (double)m_Marker.X / (Size.Width - 2);
					m_HSL.L = 1.0 - (double)m_Marker.Y / (Size.Height - 2);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Saturation:
					m_HSL.H = (double)m_Marker.X / (Size.Width - 2);
					m_HSL.L = 1.0 - (double)m_Marker.Y / (Size.Height - 2);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Luminance:
					m_HSL.H = (double)m_Marker.X / (Size.Width - 2);
					m_HSL.S = 1.0 - (double)m_Marker.Y / (Size.Height - 2);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Red:
					blue = HSL.ClipColor(255 * (double)m_Marker.X / (Size.Width - 2));
					green = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (Size.Height - 2)));
					m_RGB = Color.FromArgb(m_RGB.R, green, blue);
					m_HSL = new HSL(m_RGB);
					break;
				case ColorRange.Green:
					blue = HSL.ClipColor(255 * (double)m_Marker.X / (Size.Width - 2));
					red = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (Size.Height - 2)));
					m_RGB = Color.FromArgb(red, m_RGB.G, blue);
					m_HSL = new HSL(m_RGB);
					break;
				case ColorRange.Blue:
					red = HSL.ClipColor(255 * (double)m_Marker.X / (Size.Width - 2));
					green = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (Size.Height - 2)));
					m_RGB = Color.FromArgb(red, green, m_RGB.B);
					m_HSL = new HSL(m_RGB);
					break;
			}
		}

		private void SetMarkerPostion(Point p)
		{
			Point last = m_Marker;
			m_Marker = p;

			ResetHSLRGB();

			if (Scroll != null)
			{
				//	Notify anyone who cares that the controls marker (selected color) has changed
				Scroll(this, null);
			}		
		}


		#endregion

		#region Stash

		/* 	

		#endregion 

		#region Render 

		/// <summary>
		/// Calls all the functions neccessary to redraw the entire control.
		/// </summary>
		private void Redraw_Control(Graphics g)
		{		
			// do we need to redraw the gradiant 
			if (m_RedrawGradiant || m_Gradiant == null)
			{
				// is the gradiant image valid? 
				if ((m_Gradiant == null) ||
					(m_Gradiant.Width != this.Width) ||
					(m_Gradiant.Height != this.Height))
				{
					// dispose of it if needed 
					if (m_Gradiant != null) 
					{
						m_Gradiant.Dispose(); 
						m_Gradiant = null; 
					}

					// create the gradiant image 
					m_Gradiant = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb);

				}

				// get a graphics object to draw into the gradiant 
				Graphics g2 = Graphics.FromImage(m_Gradiant);
			
				// fill the gradiant 
				switch (m_DrawStyle)
				{
					case ColorRange.Hue:
						Draw_Style_Hue(g2);
						break;
					case ColorRange.Saturation:
						Draw_Style_Saturation(g2);
						break;
					case ColorRange.Luminance:
						Draw_Style_Luminance(g2);
						break;
					case ColorRange.Red:
						Draw_Style_Red(g2);
						break;
					case ColorRange.Green:
						Draw_Style_Green(g2);
						break;
					case ColorRange.Blue:
						Draw_Style_Blue(g2);
						break;
				}

				// draw the border 
				DrawBorder(g2);				

				// we do not need to redraw it
				m_RedrawGradiant = false; 

				// dispose of the temp graphics 
				g2.Dispose(); 
			}

			// draw the gradiant to the control 
			g.DrawImage(m_Gradiant, new Point(0, 0)); 

			// draw the marker 
			DrawMarker(g, m_Marker.X, m_Marker.Y, true);
		}

		/// <summary>
		/// Draws the marker (circle) inside the box
		/// </summary>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="Unconditional"></param>
		private void DrawMarker(Graphics g, int r, int g, bool Unconditional)	
		{
			// clip the r and g 
			r = r < 0 ? 0 : r > this.Width - 4 ? this.Width - 4 : r;
			g = g < 0 ? 0 : g > this.Height - 4 ? this.Height - 4 : g;

			// set the clip region to within the border 
			g.SetClip(new Rectangle(2, 2, this.Width - 4, this.Height - 4)); 			

			//	Get the color for the current marker location 
			HSL _hsl = GetColor(r, g);

			Pen pen;

			// make sure the marker will stand out 
			if (_hsl.L > 0.5)
				pen = ResourceManager.Pens[Color.Black, 1f];
			else
				pen = ResourceManager.Pens[Color.White, 1f];

			//	Draw the marker : 11 r 11 circle
			g.DrawEllipse(pen, r - 3, g - 3, 10, 10);						

			// reset the clipping area 
			g.SetClip(this.ClientRectangle); 			
		}


		/// <summary>
		/// Draws the border around the control.
		/// </summary>
		private void DrawBorder(Graphics g)
		{		
			Pen pencil;

			// pencil = ResourceManager.Pens[SystemColors.Control, 1f];
			pencil = ResourceManager.Pens[SystemColors.ControlDark, 1f];
			//	Draw top line
			g.DrawLine(pencil, this.Width - 2, 0, 0, 0);
			//	Draw left hand line
			g.DrawLine(pencil, 0, 0, 0, this.Height - 2);

			pencil = ResourceManager.Pens[SystemColors.ControlLightLight, 1f];
			//	Draw right hand line
			g.DrawLine(pencil, this.Width - 1, 0, this.Width - 1, this.Height - 1);
			//	Draw bottome line
			g.DrawLine(pencil, this.Width - 1, this.Height - 1, 0, this.Height - 1);

			//	Draw inner black rectangle
			// pencil = ResourceManager.Pens[SystemColors.ControlDarkDark, 1f];
			pencil = ResourceManager.Pens[Color.Black, 1f];
			g.DrawRectangle(pencil, 1, 1, this.Width - 3, this.Height - 3);
		}

		#endregion 

		#region  Marker to color / Color to Marker
		/// <summary>
		/// Returns the graphed color at the r,g position on the control
		/// </summary>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <returns></returns>
		private HSL GetColor(int r, int g)
		{

			HSL _hsl = new HSL();

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					_hsl.H = m_HSL.H;
					_hsl.S = (double)r / (this.Width - 4);
					_hsl.L = 1.0 - (double)g / (this.Height - 4);
					break;
				case ColorRange.Saturation:
					_hsl.S = m_HSL.S;
					_hsl.H = (double)r / (this.Width - 4);
					_hsl.L = 1.0 - (double)g / (this.Height - 4);
					break;
				case ColorRange.Luminance:
					_hsl.L = m_HSL.L;
					_hsl.H = (double)r / (this.Width - 4);
					_hsl.S = 1.0 - (double)g / (this.Height - 4);
					break;
				case ColorRange.Red:
					_hsl = new HSL(Color.FromArgb(m_RGB.R, HSL.ClipColor(255 * (1.0 - (double)g / (this.Height - 4))), HSL.ClipColor(255 * (double)r / (this.Width - 4))));
					break;
				case ColorRange.Green:
					_hsl = new HSL(Color.FromArgb(HSL.ClipColor(255 * (1.0 - (double)g / (this.Height - 4))), m_RGB.G, HSL.ClipColor(255 * (double)r / (this.Width - 4))));
					break;
				case ColorRange.Blue:
					_hsl = new HSL(Color.FromArgb(HSL.ClipColor(255 * (double)r / (this.Width - 4)), HSL.ClipColor(255 * (1.0 - (double)g / (this.Height - 4))), m_RGB.B));
					break;
			}

			return _hsl;
		}

		/// <summary>
		/// Sets the marker position to the active color
		/// </summary>
		private void ResetMarker()
		{
			double r = 0, g = 0;

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					r = m_HSL.S *(this.Width - 4); 
					g = (1.0 - m_HSL.L) * (this.Height - 4);
					break;
				case ColorRange.Saturation:
					r = (m_HSL.H * (this.Width - 4));
					g = ((1.0 - m_HSL.L) * (this.Height - 4));
					break;
				case ColorRange.Luminance:
					r = (m_HSL.H * (this.Width - 4));
					g = ((1.0 - m_HSL.S) * (this.Height - 4));
					break;
				case ColorRange.Red:
					r = (((double)m_RGB.B / 255) * (this.Width - 4));
					g = ((1.0 - ((double)m_RGB.G / 255)) * (this.Height - 4));
					break;
				case ColorRange.Green:
					r = (((double)m_RGB.B / 255) * (this.Width - 4));
					g = ((1.0 - ((double)m_RGB.R / 255)) * (this.Height - 4));
					break;
				case ColorRange.Blue:
					r = ((double)m_RGB.R / 255) * (this.Width - 4f);
					g = ((1.0 - ((double)m_RGB.G / 255)) * (this.Height - 4));
					break;
			}

			m_Marker.X = (int)(r);
			m_Marker.Y = (int)(g);

		}

		/// <summary>
		/// Resets the controls color (both HSL and RGB variables) based on the current marker position
		/// </summary>
		private void ResetHSLRGB()
		{
			int red, green, blue;

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					m_HSL.S = (double)m_Marker.X / (this.Width - 4);
					m_HSL.L = 1.0 - (double)m_Marker.Y / (this.Height - 4);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Saturation:
					m_HSL.H = (double)m_Marker.X / (this.Width - 4);
					m_HSL.L = 1.0 - (double)m_Marker.Y / (this.Height - 4);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Luminance:
					m_HSL.H = (double)m_Marker.X / (this.Width - 4);
					m_HSL.S = 1.0 - (double)m_Marker.Y / (this.Height - 4);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Red:
					blue = HSL.ClipColor(255 * (double)m_Marker.X / (this.Width - 4));
					green = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (this.Height - 4)));
					m_RGB = Color.FromArgb(m_RGB.R, green, blue);
					m_HSL = new HSL(m_RGB);
					break;
				case ColorRange.Green:
					blue = HSL.ClipColor(255 * (double)m_Marker.X / (this.Width - 4));
					red = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (this.Height - 4)));
					m_RGB = Color.FromArgb(red, m_RGB.G, blue);
					m_HSL = new HSL(m_RGB);
					break;
				case ColorRange.Blue:
					red = HSL.ClipColor(255 * (double)m_Marker.X / (this.Width - 4));
					green = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (this.Height - 4)));
					m_RGB = Color.FromArgb(red, green, m_RGB.B);
					m_HSL = new HSL(m_RGB);
					break;
			}
		}

		private void SetMarkerPostion(Point index)
		{
			Point last = m_Marker;
			m_Marker = index;

			ResetHSLRGB();

			if (Scroll != null)	//	Notify anyone who cares that the controls marker (selected color) has changed
				Scroll(this, null);

			if (m_Gradiant != null)
			{
				Graphics g = this.CreateGraphics();

				int r = last.X;
				int g = last.Y;

				if (r < 0)
				{
					r = 0;
				}

				if (r > this.Width - 4)
				{
					r = this.Width - 4;
				}

				if (g < 0)
				{
					g = 0;
				}

				if (g > this.Height - 4)
				{
					g = this.Height - 4;
				}

				Rectangle rect = new Rectangle(r - 4, g - 4, 12, 12);

				g.DrawImage(m_Gradiant, rect, rect, GraphicsUnit.Pixel);

				DrawMarker(g, index.X, index.Y, true);

				g.Dispose();

			}
			else
			{
				Invalidate();
			}
		}

		#endregion 
		*/

		#endregion

		#region Draw Gradiant
		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Hue value.
		/// </summary>
		private void Draw_Style_Hue(float x, float y, float w, float h, float z,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			HSL hsl_start = new HSL();
			HSL hsl_end = new HSL();
			hsl_start.H = m_HSL.H;
			hsl_end.H = m_HSL.H;
			hsl_start.S = 0.0;
			hsl_end.S = 1.0;

			hsl_start.L = 0;
			hsl_end.L = hsl_start.L;

			Color4 botLeft, botRight, topLeft, topRight;

			botLeft = hsl_start.Color4;
			botRight = hsl_end.Color4;

			hsl_start.L = 1;
			hsl_end.L = hsl_start.L;

			topLeft = hsl_start.Color4;
			topRight = hsl_end.Color4;

			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = topLeft, Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = topRight, Position = new Vector3(x + w, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = botLeft, Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = botRight, Position = new Vector3(x + w, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			int i = TriangleVertsCount;

			TriangleIndices.WriteRange(new int[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2
			});

			TriangleVertsCount += 4;
			TriangleIndicesCount += 6;

			//Graphics g = this.CreateGraphics();


			/* for (int i = 0; i < this.Height - 4; i++)				//	For each horizontal line in the control:
			{
				hsl_start.L = 1.0 - (double)i / (this.Height - 4);	//	Calculate luminance at this line (Hue and Saturation are constant)
				hsl_end.L = hsl_start.L;

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), hsl_start.Color, hsl_end.Color, 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose();
			}*/ 
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Saturation value.
		/// </summary>
		private void Draw_Style_Saturation(float x, float y, float w, float h, float z,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			//Graphics g = this.CreateGraphics();

			HSL hsl_start = new HSL();
			HSL hsl_end = new HSL();
			hsl_start.S = m_HSL.S;
			hsl_end.S = m_HSL.S;
			hsl_start.L = 1.0;
			hsl_end.L = 0.0;

			float sx = x, sw = w / 6; 

			for (int c = 0; c < 6; c++)
			{
				Color4 botLeft, botRight, topLeft, topRight;

				hsl_start.H = (1d / 6d) * (double)c;
				hsl_end.H = (1d / 6d) * (double)c; // hsl_start.H;

				botLeft = hsl_start.Color4;
				topLeft = hsl_end.Color4;

				hsl_start.H = (1d / 6d) * (double)(c + 1);
				hsl_end.H = (1d / 6d) * (double)(c + 1); // hsl_start.H;

				botRight = hsl_start.Color4;
				topRight = hsl_end.Color4;

				TriangleVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = botLeft, Position = new Vector3(sx, y, z), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = botRight, Position = new Vector3(sx + sw, y, z), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = topLeft, Position = new Vector3(sx, y + h, z), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = topRight, Position = new Vector3(sx + sw, y + h, z), TextureCoords = new Vector2(0, 0) }
				});

				int i = TriangleVertsCount;

				TriangleIndices.WriteRange(new int[] { 
					i + 0, i + 1, i + 2,
					i + 1, i + 3, i + 2
				});

				TriangleVertsCount += 4;
				TriangleIndicesCount += 6;

				sx += sw;
			}

			/* 
			for (int i = 0; i < this.Width - 4; i++)		//	For each vertical line in the control:
			{
				hsl_start.H = (double)i / (this.Width - 4);	//	Calculate Hue at this line (Saturation and Luminance are constant)
				hsl_end.H = hsl_start.H;

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, 1, this.Height - 4), hsl_start.Color, hsl_end.Color, 90, false);
				g.FillRectangle(br, new Rectangle(i + 2, 2, 1, this.Height - 4));
				br.Dispose();
			}
			*/ 
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Luminance or Brightness value.
		/// </summary>
		private void Draw_Style_Luminance(float x, float y, float w, float h, float z,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			//Graphics g = this.CreateGraphics();

			HSL hsl_start = new HSL();
			HSL hsl_end = new HSL();
			hsl_start.L = m_HSL.L;
			hsl_end.L = m_HSL.L;
			hsl_start.S = 1.0;
			hsl_end.S = 0;

			float sx = x, sw = w / 6;

			for (int c = 0; c < 6; c++)
			{
				Color4 botLeft, botRight, topLeft, topRight;

				hsl_start.H = (1d / 6d) * (double)c;
				hsl_end.H = (1d / 6d) * (double)c; //  hsl_start.H;

				botLeft = hsl_start.Color4;
				topLeft = hsl_end.Color4;

				hsl_start.H = (1d / 6d) * (double)(c + 1);
				hsl_end.H = (1d / 6d) * (double)(c + 1); //  hsl_start.H;

				botRight = hsl_start.Color4;
				topRight = hsl_end.Color4;

				TriangleVerts.WriteRange(new SimpleVertex[] { 
					new SimpleVertex() { Color = botLeft, Position = new Vector3(sx, y, z), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = botRight, Position = new Vector3(sx + sw, y, z), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = topLeft, Position = new Vector3(sx, y + h, z), TextureCoords = new Vector2(0, 0) },
					new SimpleVertex() { Color = topRight, Position = new Vector3(sx + sw, y + h, z), TextureCoords = new Vector2(0, 0) }
				});

				int i = TriangleVertsCount;

				TriangleIndices.WriteRange(new int[] { 
					i + 0, i + 1, i + 2,
					i + 1, i + 3, i + 2
				});

				TriangleVertsCount += 4;
				TriangleIndicesCount += 6;

				sx += sw;
			}

			/* 
			for (int i = 0; i < this.Width - 4; i++)		//	For each vertical line in the control:
			{
				hsl_start.H = (double)i / (this.Width - 4);	//	Calculate Hue at this line (Saturation and Luminance are constant)
				hsl_end.H = hsl_start.H;

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, 1, this.Height - 4), hsl_start.Color, hsl_end.Color, 90, false);
				g.FillRectangle(br, new Rectangle(i + 2, 2, 1, this.Height - 4));
				br.Dispose();
			}
			*/ 
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Red value.
		/// </summary>
		private void Draw_Style_Red(float x, float y, float w, float h, float z,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			int red = m_RGB.R;

			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = Color.FromArgb(red, 255, 0), Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(red, 255, 255), Position = new Vector3(x + w, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(red, 0, 0), Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(red, 0, 255), Position = new Vector3(x + w, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			int i = TriangleVertsCount;

			TriangleIndices.WriteRange(new int[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2
			});

			TriangleVertsCount += 4;
			TriangleIndicesCount += 6;

			/* 
			for (int i = 0; i < this.Height - 4; i++)				//	For each horizontal line in the control:
			{
				//	Calculate Green at this line (Red and Blue are constant)
				int green = HSL.ClipColor(255 - (255 * (double)i / (this.Height - 4)));

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), Color.FromArgb(red, green, 0), Color.FromArgb(red, green, 255), 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose();
			}
			*/ 
		}

		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Green value.
		/// </summary>
		private void Draw_Style_Green(float x, float y, float w, float h, float z,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			int green = m_RGB.G;

			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = Color.FromArgb(255, green, 0), Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(255, green, 255), Position = new Vector3(x + w, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(0, green, 0), Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(0, green, 255), Position = new Vector3(x + w, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			int i = TriangleVertsCount;

			TriangleIndices.WriteRange(new int[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2
			});

			TriangleVertsCount += 4;
			TriangleIndicesCount += 6;

			/* 
			for (int i = 0; i < this.Height - 4; i++)	//	For each horizontal line in the control:
			{
				//	Calculate Red at this line (Green and Blue are constant)
				int red = HSL.ClipColor(255 - (255 * (double)i / (this.Height - 4)));

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), Color.FromArgb(red, green, 0), Color.FromArgb(red, green, 255), 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose();
			}
			*/ 
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Blue value.
		/// </summary>
		private void Draw_Style_Blue(float x, float y, float w, float h, float z,
												DataStream TriangleVerts, ref int TriangleVertsCount,
												DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			int blue = m_RGB.B;

			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = Color.FromArgb(0, 255, blue), Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(255, 255, blue), Position = new Vector3(x + w, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(0, 0, blue), Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = Color.FromArgb(255, 0, blue), Position = new Vector3(x + w, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			int i = TriangleVertsCount;

			TriangleIndices.WriteRange(new int[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2
			});

			TriangleVertsCount += 4;
			TriangleIndicesCount += 6;

			/* 
			for (int i = 0; i < this.Height - 4; i++)	//	For each horizontal line in the control:
			{
				//	Calculate Green at this line (Red and Blue are constant)
				int green = HSL.ClipColor(255 - (255 * (double)i / (this.Height - 4)));

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), Color.FromArgb(0, green, blue), Color.FromArgb(255, green, blue), 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose();
			}
			*/
		}

		#endregion

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false;

			switch (mouseState)
			{
				case MouseState.DragStart:
				case MouseState.Dragging:
				case MouseState.DragEnd:
				case MouseState.ClickEnd:
					if (Bounds.Contains(mousePos.X, mousePos.Y))
					{
						Point relitive = new Point((int)(mousePos.X - Bounds.X), (int)(mousePos.Y - Bounds.Y)); 
						if ((relitive.X < 1 || relitive.Y < 1) ||
 							((relitive.X > Size.Width - 1) || (relitive.Y > Size.Height - 1))) 
						{
							break; 
						}
						
						relitive.X -= 1;
						relitive.Y -= 1;

						SetMarkerPostion(relitive);

						// set marker 
						// set color 
						shouldUpdate = true;
					}
					break;
				default:
					break;
			}
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control)
		{
			base.EndPick(mousePos, pickType, control);
		}

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);
		}

		public override void WriteVisibleElements(View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
										LineVerts, ref LineVertsCount,
										LinesIndices, ref LinesIndicesCount,
										TriangleVerts, ref TriangleVertsCount,
										TriangleIndices, ref TriangleIndicesCount);

			float x, y, w, h;
			float px = view.PixelSize.X;
			float py = view.PixelSize.Y;

			UiStyleHelper.CovertToVertCoords(Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);	
		}


		#region IDynamicUiControl Members

		public void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			// Main grad box 
			TriangleVerts += 4;
			TriangleIndices += 6;

			// Luminace grad box
			TriangleVerts += 12;
			TriangleIndices += 18;

			// Marker
			LineVerts += 8;
			LinesIndices += 8 * 2; 
		}

		public void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;

			System.Drawing.RectangleF innerBounds = new System.Drawing.RectangleF(m_Bounds.X + 1, m_Bounds.Y + 1, m_Bounds.Width - 2, m_Bounds.Height - 2);

			UiStyleHelper.CovertToVertCoords(innerBounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					Draw_Style_Hue(x, y, w, h, ZIndexForOver_Float,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
					break;
				case ColorRange.Saturation:
					Draw_Style_Saturation(x, y, w, h, ZIndexForOver_Float,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
					break;
				case ColorRange.Luminance:
					Draw_Style_Luminance(x, y, w, h, ZIndexForOver_Float,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
					break;
				case ColorRange.Red:
					Draw_Style_Red(x, y, w, h, ZIndexForOver_Float,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
					break;
				case ColorRange.Green:
					Draw_Style_Green(x, y, w, h, ZIndexForOver_Float,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);
					break;
				case ColorRange.Blue:
					Draw_Style_Blue(x, y, w, h, ZIndexForOver_Float,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);break;
				default:
					break;
			}

			/* 
			float value = m_Value < m_MinValue ? m_MinValue : m_Value > m_MaxValue ? m_MaxValue : m_Value;

			if (m_BarAlignment == Dynamic.BarAlignment.Horizontal)
			{
				percent = (m_Bounds.Width / (m_MaxValue - m_MinValue)) * (value - m_MinValue);

				if (m_InvertHighlight == false)
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y, percent, m_Bounds.Height - 1);
				}
				else
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X + percent, m_Bounds.Y, m_Bounds.Width - percent, m_Bounds.Height - 1);
				}
			}
			else
			{
				percent = (m_Bounds.Height / (m_MaxValue - m_MinValue)) * (value - m_MinValue);

				if (m_InvertHighlight == false)
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y + (m_Bounds.Height - percent), m_Bounds.Width, percent);
				}
				else
				{
					barBounds = new System.Drawing.RectangleF(m_Bounds.X, m_Bounds.Y, m_Bounds.Width, m_Bounds.Height - percent);
				}
			}

			UiStyleHelper.CovertToVertCoords(barBounds, view.WindowSize, view.PixelSize, out r, out g, out a, out h);

			Color4 backColor;

			if (this.IsFocused)
			{
				backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Open);
			}
			else
			{
				backColor = UiStyleHelper.GetControlBackColor(this, DisplayMode.Hovering);
			}

			WriteTrianglesForBounds(r, g, a, h, ZIndexForOver_Float, backColor, TriangleVerts, ref TriangleVertsCount, TriangleIndices, ref TriangleIndicesCount);
			*/
		}

		#endregion

		/* 
		#region Internal Events 

		private void OnPaint(object sender, PaintEventArgs e)
		{
			Redraw_Control(e.Graphics);
		}	

		private void onMouseDown(object sender, MouseEventArgs e)
		{
			m_Dragging = true;
			SetMarkerPostion(e.Location);
		}

		private void onMouseMove(object sender, MouseEventArgs e)
		{
			if (m_Dragging)
			{
				SetMarkerPostion(e.Location);				
			}
		}

		private void onMouseUp(object sender, MouseEventArgs e)
		{
			m_Dragging = false;
		}

		#endregion 
		*/ 
	}
}
