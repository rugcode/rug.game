﻿using Rug.Game.Ui.Dynamic;

namespace Rug.Game.Ui.Dialogs.ColorPicker
{
	class AlphaSlider : Slider
	{
		public AlphaSlider()
		{
			this.BarAlignment = Dynamic.BarAlignment.Horizontal;
			this.MinValue = 0;
			this.MaxValue = 255; 
		}
	}
}
