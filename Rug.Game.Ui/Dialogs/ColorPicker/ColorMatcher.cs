﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Rendering;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Dialogs.ColorPicker
{
	class ColorMatcher : UiControlBase, IDynamicUiControl
	{
		#region Members 

		private Color m_ActiveColor = Color.White;
		private Color m_OriginalColor = Color.Black;
		
		#endregion 

		#region Properties	

		public Color ActiveColor
		{
			get { return m_ActiveColor; }
			set 
			{ 
				m_ActiveColor = value;
				Invalidate();
			}
		}

		public Color OriginalColor
		{
			get { return m_OriginalColor; }
			set 
			{ 
				m_OriginalColor = value;
				Invalidate();
			}
		}

		#endregion 

		#region Events

		public event EventHandler ColorChanged; 

		#endregion

		#region Constructor 

		public ColorMatcher()
		{
			InteractionType = ControlInteractionType.Click; 			

			this.ShowBackground = false; 
		}

		#endregion 

		private void Invalidate()
		{
			if (this.Scene != null)
			{
				this.Scene.Invalidate(); 
			}
		}

		public override void DoMouseInteraction(MouseState mouseState, System.Windows.Forms.MouseButtons mouseButtons, Vector2 mousePos, out bool shouldUpdate)
		{
			shouldUpdate = false;

			switch (mouseState)
			{
				case MouseState.ClickEnd:
					if (Bounds.Contains(mousePos.X, mousePos.Y))
					{
						Point relitive = new Point((int)(mousePos.X - Bounds.X), (int)(mousePos.Y - Bounds.Y));

						if (relitive.X > this.Size.Width / 2)
						{
							if (ColorChanged != null)
							{
								ColorChanged(this, null);
							}

							shouldUpdate = true;
						}					
					}
					break;
				default:
					break;
			}
		}

		public override void EndPick(Vector2 mousePos, PickType pickType, UiControl control)
		{
			base.EndPick(mousePos, pickType, control);
		}

		public override void GetTotalElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			base.GetTotalElementCounts(ref LineVerts, ref LinesIndices, ref TriangleVerts, ref TriangleIndices);
		}

		public override void WriteVisibleElements(View3D view, System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
										LineVerts, ref LineVertsCount,
										LinesIndices, ref LinesIndicesCount,
										TriangleVerts, ref TriangleVertsCount,
										TriangleIndices, ref TriangleIndicesCount);

			float x, y, w, h;
			float px = view.PixelSize.X;
			float py = view.PixelSize.Y;

			UiStyleHelper.CovertToVertCoords(Bounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);	
		}


		#region IDynamicUiControl Members

		public void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices)
		{
			// box 1 
			TriangleVerts += 4;
			TriangleIndices += 6;

			// box 2 
			TriangleVerts += 4;
			TriangleIndices += 6;
		}

		public void WriteDynamicElements(View3D view, DataStream LineVerts, ref int LineVertsCount, DataStream LinesIndices, ref int LinesIndicesCount, DataStream TriangleVerts, ref int TriangleVertsCount, DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			float x, y, w, h;
			float z = ZIndexForUnder_Float;
			System.Drawing.RectangleF innerBounds = new System.Drawing.RectangleF(m_Bounds.X + 1, m_Bounds.Y + 1, m_Bounds.Width - 2, m_Bounds.Height - 2);

			UiStyleHelper.CovertToVertCoords(innerBounds, view.WindowSize, view.PixelSize, out x, out y, out w, out h);

			Color4 left, right;
			left = m_ActiveColor;
			right = m_OriginalColor; 

			float hw = w * 0.5f; 
			float hx = x + hw;

			TriangleVerts.WriteRange(new SimpleVertex[] { 
				new SimpleVertex() { Color = left, Position = new Vector3(x, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = left, Position = new Vector3(x + hw, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = left, Position = new Vector3(x, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = left, Position = new Vector3(x + hw, y + h, z), TextureCoords = new Vector2(0, 0) },
				
				new SimpleVertex() { Color = right, Position = new Vector3(hx, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = right, Position = new Vector3(hx + hw, y, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = right, Position = new Vector3(hx, y + h, z), TextureCoords = new Vector2(0, 0) },
				new SimpleVertex() { Color = right, Position = new Vector3(hx + hw, y + h, z), TextureCoords = new Vector2(0, 0) }
			});

			int i = TriangleVertsCount;

			TriangleIndices.WriteRange(new int[] { 
				i + 0, i + 1, i + 2,
				i + 1, i + 3, i + 2,

				i + 4, i + 5, i + 6,
				i + 5, i + 7, i + 6
			});

			TriangleVertsCount += 8;
			TriangleIndicesCount += 12;
		}

		#endregion
		// 2 color boxes
	}
}
