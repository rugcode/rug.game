﻿using Rug.Game.Core.Text;
using Rug.Game.Ui.Controls;
using Rug.Game.Ui.Dynamic;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game.Ui.Dialogs.ColorPicker
{
	public class ColorPickerDialog : UiDialog
	{
		#region Members

		private HSL m_HSL;
		private Color m_RGB;

		private GradiantBox m_GradiantBox;
		private ColorSlider m_ColorSlider;
		private ColorMatcher m_Matcher;
		private AlphaSlider m_AlphaSlider;

		private List<ToggleButton> m_ColorRangeButtons = new List<ToggleButton>();

		private Button m_OkButton;
		private Button m_CancelButton; 

		#endregion

		#region Properties

		public event EventHandler ColorChanged;
		private int m_Alpha; 

		public Color OriginalColor
		{
			set
			{
				m_Matcher.OriginalColor = value;
			}
			get
			{
				return m_Matcher.OriginalColor;
			}
		}

		public Color RGB
		{
			get
			{
				return m_RGB;
			}
			set
			{
				m_RGB = value;
				m_HSL = new HSL(value);

				//SetColorText();
				m_Alpha = (int)m_RGB.A;
				m_Matcher.ActiveColor = m_RGB;
				this.m_GradiantBox.RGB = m_RGB;
				this.m_ColorSlider.RGB = m_RGB;
				this.m_AlphaSlider.Value = (float)m_RGB.A; 

			}
		}

		#endregion

		public ColorPickerDialog()
			: base()
		{
			this.Title = "Select a color"; 
		}

		public override void InitializeControls()
		{
			base.InitializeControls();
			
			int index = 3; 

			m_GradiantBox = new GradiantBox();
			m_GradiantBox.Location = new System.Drawing.Point(2, 2);
			m_GradiantBox.Size = new System.Drawing.Size(257, 257);
			m_GradiantBox.RelitiveZIndex = index++;
			m_GradiantBox.Scroll += new EventHandler(m_GradiantBox_Scroll);		
			this.Controls.Add(m_GradiantBox);

			m_ColorSlider = new ColorSlider();
			m_ColorSlider.Location = new System.Drawing.Point(261, 2);
			m_ColorSlider.Size = new System.Drawing.Size(11, 257);
			m_ColorSlider.RelitiveZIndex = index++;
			m_ColorSlider.Scroll += new EventHandler(m_ColorSlider_Scroll);
			this.Controls.Add(m_ColorSlider);

			m_Matcher = new ColorMatcher();
			m_Matcher.Location = new System.Drawing.Point(274, 2); 
			m_Matcher.Size = new System.Drawing.Size(96, 48);
			m_Matcher.RelitiveZIndex = index++;
			m_Matcher.ColorChanged += RevertToPrimary;
			this.Controls.Add(m_Matcher);

			m_AlphaSlider = new AlphaSlider();
			m_AlphaSlider.Location = new System.Drawing.Point(2, 261);
			m_AlphaSlider.Size = new System.Drawing.Size(257, 11);
			m_AlphaSlider.RelitiveZIndex = index++;
			m_AlphaSlider.ValueChanged += new SliderValueChangedEvent(m_AlphaSlider_ValueChanged);
			this.Controls.Add(m_AlphaSlider);			
			
			m_GradiantBox.DrawStyle = ColorRange.Hue;
			m_ColorSlider.DrawStyle = ColorRange.Hue;

			int buttonX, buttonY;
			int buttonWidth, buttonHeight;

			buttonX = 274;
			buttonY = 52; 
			buttonWidth = 96;
			buttonHeight = 26;

			foreach (ColorRange range in new ColorRange[] 
					  {	ColorRange.Hue, ColorRange.Saturation, ColorRange.Luminance, 
						ColorRange.Red, ColorRange.Green, ColorRange.Blue })
			{
				ToggleButton toggle = new ToggleButton();
				toggle.Text = range.ToString();
				toggle.Tag = range; 
				toggle.Value = range == ColorRange.Hue;

				toggle.FontType = FontType.Small;
				toggle.Location = new Point(buttonX, buttonY);
				toggle.Size = new System.Drawing.Size(buttonWidth, buttonHeight);
				toggle.RelitiveZIndex = index++;
				toggle.Click += new EventHandler(toggle_Click);
				
				m_ColorRangeButtons.Add(toggle); 
				this.Controls.Add(toggle);

				buttonY += buttonHeight + 2; 
			}

			buttonY = (m_AlphaSlider.Location.Y + m_AlphaSlider.Size.Height) - buttonHeight; 

			m_OkButton = new Button();
			m_OkButton.Text = "OK";
			m_OkButton.Tag = System.Windows.Forms.DialogResult.OK;
			m_OkButton.FontType = FontType.Small;
			m_OkButton.Location = new Point(buttonX, buttonY);
			m_OkButton.Size = new System.Drawing.Size((buttonWidth / 2) - 1, buttonHeight);
			m_OkButton.RelitiveZIndex = index++;
			m_OkButton.Click += new EventHandler(m_OkButton_Click);
			this.Controls.Add(m_OkButton);

			m_CancelButton = new Button();
			m_CancelButton.Text = "Cancel";
			m_CancelButton.Tag = System.Windows.Forms.DialogResult.Cancel;
			m_CancelButton.FontType = FontType.Small;
			m_CancelButton.Location = new Point(buttonX + (buttonWidth / 2), buttonY);
			m_CancelButton.Size = new System.Drawing.Size(buttonWidth / 2, buttonHeight);
			m_CancelButton.RelitiveZIndex = index++;
			m_CancelButton.Click += new EventHandler(m_CancelButton_Click);
			this.Controls.Add(m_CancelButton);

			this.Size = new System.Drawing.Size(372, HeaderSize + (m_AlphaSlider.Location.Y + m_AlphaSlider.Size.Height) + 2);
		}

		void m_OkButton_Click(object sender, EventArgs e)
		{
			DialogResult = (System.Windows.Forms.DialogResult)(sender as Button).Tag;

			Close(); 
		}

		void m_CancelButton_Click(object sender, EventArgs e)
		{
			DialogResult = (System.Windows.Forms.DialogResult)(sender as Button).Tag;

			Close(); 
		}

		void toggle_Click(object sender, EventArgs e)
		{
			foreach (ToggleButton button in m_ColorRangeButtons)
			{
				button.Value = button == sender;
			}

			ColorRange range = (ColorRange)(sender as ToggleButton).Tag;

			m_ColorSlider.DrawStyle = range;
			m_GradiantBox.DrawStyle = range; 
		}


		#region Color Selector Control Events

		void m_ColorSlider_Scroll(object sender, EventArgs e)
		{
			if (m_ColorSlider.DrawStyle == ColorRange.Red)
			{
				m_RGB = Color.FromArgb(m_Alpha, m_ColorSlider.RGB.R, m_RGB.G, m_RGB.B);
				m_HSL = new HSL(m_RGB);

				m_GradiantBox.RGB = m_RGB;
			}
			else if (m_ColorSlider.DrawStyle == ColorRange.Green)
			{
				m_RGB = Color.FromArgb(m_Alpha, m_RGB.R, m_ColorSlider.RGB.G, m_RGB.B);
				m_HSL = new HSL(m_RGB);

				m_GradiantBox.RGB = m_RGB;
			}
			else if (m_ColorSlider.DrawStyle == ColorRange.Blue)
			{
				m_RGB = Color.FromArgb(m_Alpha, m_RGB.R, m_RGB.G, m_ColorSlider.RGB.B);
				m_HSL = new HSL(m_RGB);

				m_GradiantBox.RGB = m_RGB;
			}
			else
			{
				m_HSL = m_ColorSlider.HSL;
				m_RGB = Color.FromArgb(m_Alpha, m_HSL.Color);

				m_GradiantBox.HSL = m_HSL;
			}

			//SetColorText();

			m_Matcher.ActiveColor = m_RGB;

			OnColorChanged();
		}

		void m_GradiantBox_Scroll(object sender, EventArgs e)
		{
			m_HSL = this.m_GradiantBox.HSL;
			m_RGB = Color.FromArgb(m_Alpha, m_HSL.Color);

			//SetColorText();

			m_ColorSlider.HSL = m_HSL;
			m_Matcher.ActiveColor = m_RGB;

			OnColorChanged();
		}

		private void RevertToPrimary(object sender, EventArgs e)
		{
			m_RGB = m_Matcher.OriginalColor;
			m_HSL = new HSL(m_RGB);

			RGB = m_RGB;

			OnColorChanged(); 
		}

		void m_AlphaSlider_ValueChanged(Slider sender, float value)
		{
			m_Alpha = (int)value;

			RGB = Color.FromArgb(m_Alpha, RGB);

			OnColorChanged();
		}

		private void OnColorChanged()
		{
			if (ColorChanged != null)
			{
				ColorChanged(this, EventArgs.Empty); 
			}
		}
		
		#endregion

		protected override void OnOpen()
		{
			
		}

		protected override void OnClose()
		{
			
		}
	}
}
