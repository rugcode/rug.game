﻿using OpenTK;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;

namespace Rug.Game.Ui
{
	public abstract class UiSubScene : UiControlBase, IScene
	{
		private Vector2 m_MouseOffset;
		private Viewport m_InnerViewport;

		public Vector2 MouseOffset { get { return m_MouseOffset; } }

		public Viewport InnerViewport 
		{ 
			get { return m_InnerViewport; }
			protected set 
			{ 
				m_InnerViewport = value;

				m_MouseOffset = new Vector2(m_InnerViewport.TopLeftX, m_InnerViewport.TopLeftY);
			}
		}

		public UiSubScene()
		{
			m_InnerViewport = new Viewport(0, 0, 1, 1);
			m_MouseOffset = new Vector2(m_InnerViewport.TopLeftX, m_InnerViewport.TopLeftY);
		}

		public override void WriteVisibleElements(View3D view,
													System.Drawing.RectangleF ClientBounds, ref System.Drawing.RectangleF RemainingBounds,
													DataStream LineVerts, ref int LineVertsCount,
													DataStream LinesIndices, ref int LinesIndicesCount,
													DataStream TriangleVerts, ref int TriangleVertsCount,
													DataStream TriangleIndices, ref int TriangleIndicesCount)
		{
			base.WriteVisibleElements(view, ClientBounds, ref RemainingBounds,
									LineVerts, ref LineVertsCount,
									LinesIndices, ref LinesIndicesCount,
									TriangleVerts, ref TriangleVertsCount,
									TriangleIndices, ref TriangleIndicesCount);

			m_InnerViewport.TopLeftX = (int)Bounds.X - (int)view.ViewportOffset.X;
			m_InnerViewport.TopLeftY = (int)Bounds.Y - (int)view.ViewportOffset.Y;

			if (ShowBorder == true)
			{
				m_InnerViewport.Width = (int)Bounds.Width - 1;
				m_InnerViewport.Height = (int)Bounds.Height - 1;
			}
			else
			{
				m_InnerViewport.Width = (int)Bounds.Width;
				m_InnerViewport.Height = (int)Bounds.Height;
			}

			m_MouseOffset = new Vector2(m_InnerViewport.TopLeftX, m_InnerViewport.TopLeftY);
		}

		public virtual void Render(View3D view)
		{

			Viewport ViewportBackup = GLState.Viewport;
			GLState.Viewport = m_InnerViewport; 

			try
			{
				GLState.Apply(view.WindowSize); 
				Render(view, m_InnerViewport);
			}
			finally
			{
				GLState.Viewport = ViewportBackup;
				GLState.Apply(view.WindowSize); 				
			}
		}

		public abstract void Render(View3D view, Viewport viewport);

		public abstract void UnfocusControls();

		public abstract void UnhoverControls();

		public abstract void OnMouseDown(View3D view, Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool shouldSubUpdate);

		public abstract void OnMouseUp(View3D view, Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool shouldSubUpdate);

		public abstract void OnMouseMoved(View3D view, Vector2 mousePosition, out bool shouldSubUpdate);

		public abstract void OnMouseWheel(System.Windows.Forms.MouseEventArgs e, out bool shouldUpdate);

		public abstract void OnKeyDown(System.Windows.Forms.KeyEventArgs args, out bool shouldUpdate);

		public abstract void OnKeyUp(System.Windows.Forms.KeyEventArgs args, out bool shouldUpdate);

		public abstract void OnKeyPress(char @char, out bool shouldUpdate);
	}
}
