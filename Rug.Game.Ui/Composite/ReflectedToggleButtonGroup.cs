﻿using Rug.Game.Ui.Controls;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Rug.Game.Ui.Composite
{
	public class ReflectedToggleButtonGroup
	{
		private string m_Name;
		private List<ToggleButton> m_Buttons = new List<ToggleButton>();
		private FieldInfo m_Field;
		private object m_Instance;

		public string Name
		{
			get { return m_Name; }
		}

		public List<ToggleButton> Buttons
		{
			get { return m_Buttons; }
		}


		public FieldInfo Field
		{
			get { return m_Field; }
		}

		public object Instance
		{
			get { return m_Instance; }
			set { m_Instance = value; }
		}

		public event EventHandler Changed;

		public ReflectedToggleButtonGroup(string name, FieldInfo field)
		{
			m_Name = name;
			m_Field = field; 
		}

		public void Reset()
		{
			if (m_Instance != null)
			{
				object obj = m_Field.GetValue(m_Instance);

				foreach (ToggleButton button in m_Buttons)
				{
					(button as ToggleButton).Value = button.Tag.Equals(obj);
				}
			}
		}

		public void OnChanged(object sender, EventArgs args) 
		{
			foreach (ToggleButton button in m_Buttons)
			{
				(button as ToggleButton).Value = button == sender;
			}

			if (m_Instance != null)
			{
				m_Field.SetValue(m_Instance, (sender as ToggleButton).Tag); 				
			}

			if (Changed != null)
			{
				Changed(this, args);
			}
		}
	}
}
