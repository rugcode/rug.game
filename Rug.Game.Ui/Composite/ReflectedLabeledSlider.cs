﻿using Rug.Game.Ui.Dynamic;
using System;
using System.Reflection;

namespace Rug.Game.Ui.Composite
{
	public class ReflectedLabeledSlider : LabeledSlider
	{
		private object m_Instance;

		private FieldInfo m_FieldInfo;

		private SliderOptionsAttribute m_SliderOptions; 

		public object Instance
		{
			get { return m_Instance; }
			set { m_Instance = value; }
		}
		
		public FieldInfo FieldInfo { get { return m_FieldInfo; } } 

		public ReflectedLabeledSlider(FieldInfo field)
		{
			m_FieldInfo = field;

			object[] objects = m_FieldInfo.GetCustomAttributes(typeof(SliderOptionsAttribute), true);

			if (objects.Length > 0)
			{
				m_SliderOptions = objects[0] as SliderOptionsAttribute;
			}
			else
			{
				throw new ArgumentException(); 
			}
		}

		public void Initiate(object instance)
		{
			m_Instance = instance; 
			
			float min = 0, max = 1, value = 0;
			string format = "N3"; 
			SliderScale scale = SliderScale.Linear; 

			if (m_SliderOptions is SliderOptions_IntAttribute)
			{
				SliderOptions_IntAttribute attrib = (m_SliderOptions as SliderOptions_IntAttribute);
				
				min = attrib.MinValue;
				max = attrib.MaxValue;
				format = attrib.Format;
				scale = attrib.SliderScale;

				if (m_Instance != null)
				{
					value = (float)m_FieldInfo.GetValue(m_Instance);
				}
				else
				{
					value = attrib.DefaultValue; 
				}
			}
			else if (m_SliderOptions is SliderOptions_FloatAttribute)
			{
				SliderOptions_FloatAttribute attrib = (m_SliderOptions as SliderOptions_FloatAttribute);

				min = attrib.MinValue;
				max = attrib.MaxValue;
				format = attrib.Format;
				scale = attrib.SliderScale;

				if (m_Instance != null)
				{
					value = (float)m_FieldInfo.GetValue(m_Instance);
				}
				else
				{
					value = attrib.DefaultValue;
				}
			}

			Initiate(m_SliderOptions.Name, min, max, value, format);
			this.Slider.SliderScale = scale; 
		}

		public void Reset()
		{
			float value = 0;

			if (m_SliderOptions is SliderOptions_IntAttribute)
			{
				if (m_Instance != null)
				{
					value = (int)m_FieldInfo.GetValue(m_Instance);
				}
				else
				{
					value = (m_SliderOptions as SliderOptions_IntAttribute).DefaultValue; 
				}
			}
			else if (m_SliderOptions is SliderOptions_FloatAttribute)
			{
				if (m_Instance != null)
				{
					value = (float)m_FieldInfo.GetValue(m_Instance);
				}
				else
				{
					value = (m_SliderOptions as SliderOptions_FloatAttribute).DefaultValue;
				}
			}

			Slider.SetValue_NoEvent(value); 
		}

		protected override void SliderValueChanged(Slider sender, float value)
		{
			if (m_SliderOptions is SliderOptions_IntAttribute)
			{
				ValueLabel.Text = Convert.ToInt32(value).ToString(Format);
			}
			else if (m_SliderOptions is SliderOptions_FloatAttribute)
			{
				ValueLabel.Text = value.ToString(Format);
			}

			if (m_Instance != null)
			{
				if (m_SliderOptions is SliderOptions_IntAttribute)
				{
					m_FieldInfo.SetValue(m_Instance, Convert.ToInt32(value));
				}
				else if (m_SliderOptions is SliderOptions_FloatAttribute)
				{
					m_FieldInfo.SetValue(m_Instance, value);
				}
			}

			OnValueChanged(sender, value);

			OnChanged(this, EventArgs.Empty);
		}
	}
}
