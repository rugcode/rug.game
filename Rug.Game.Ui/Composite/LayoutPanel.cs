﻿using Rug.Game.Ui.Controls;
using System.Collections.Generic;

namespace Rug.Game.Ui.Composite
{
	public class LayoutPanel : Panel
	{
		private List<ReflectedToggleButtonGroup> m_ButtonGroups = new List<ReflectedToggleButtonGroup>();
		private List<ReflectedLabeledSlider> m_AllSliders = new List<ReflectedLabeledSlider>(); 
		private List<List<UiControlBase>> m_Layout = new List<List<UiControlBase>>();

		private Dictionary<string, UiControlBase> m_NamedControls = new Dictionary<string, UiControlBase>();

		private bool m_Enabled = true;
        private IReflectedPanelListener m_ReflectedPanelListener; 

		public List<ReflectedToggleButtonGroup> ButtonGroups { get { return m_ButtonGroups; } }
		public List<ReflectedLabeledSlider> AllSliders { get { return m_AllSliders; } } 
		public List<List<UiControlBase>> Layout { get { return m_Layout; } }
		
		public Dictionary<string, UiControlBase> NamedControls { get { return m_NamedControls; } }         

		public bool Enabled
		{
			get
			{
				return m_Enabled; 
			}
			set
			{
				m_Enabled = value;

				foreach (ReflectedToggleButtonGroup group in m_ButtonGroups)
				{
					foreach (ToggleButton button in group.Buttons)
					{
						button.Enabled = value;
					}
				}

				foreach (ReflectedLabeledSlider slider in m_AllSliders)
				{
					slider.Slider.Enabled = value;
				}
			}
		}

		public void AttachInstance(object instance)
		{
            m_ReflectedPanelListener = null; 

            if (instance is IReflectedPanelListener)
            {
                m_ReflectedPanelListener = instance as IReflectedPanelListener;
            }

            foreach (ReflectedToggleButtonGroup group in m_ButtonGroups)
            {
                group.Changed -= group_Changed;
            }

            foreach (ReflectedLabeledSlider slider in m_AllSliders)
            {
                slider.ValueChanged -= slider_ValueChanged;
            }

			foreach (ReflectedToggleButtonGroup group in m_ButtonGroups)
			{
				group.Instance = instance;                
				group.Reset();
                group.Changed += group_Changed;
			}

			foreach (ReflectedLabeledSlider slider in m_AllSliders)
			{
				slider.Instance = instance;
				slider.Reset();
                slider.ValueChanged += slider_ValueChanged;
			}
		}

        void slider_ValueChanged(Dynamic.Slider sender, float value)
        {
            if (m_ReflectedPanelListener == null)
            {
                return;
            }

            m_ReflectedPanelListener.ControlValuesChanged();
        }

        void group_Changed(object sender, System.EventArgs e)
        {
            if (m_ReflectedPanelListener == null)
            {
                return;
            }

            m_ReflectedPanelListener.ControlValuesChanged(); 
        }

		public void Resize()
		{
			foreach (LabeledSlider slider in m_AllSliders)
			{
				slider.Size = new System.Drawing.Size(this.Size.Width, slider.Size.Height);

				slider.Resize();
			}

			foreach (List<UiControlBase> layout in m_Layout)
			{
				int buttonSize = (this.Size.Width - 5) / layout.Count;
				int buttonOffset = 5;
				int verticalOffset = layout[0].Location.Y;

				foreach (UiControlBase button in layout)
				{
					button.Size = new System.Drawing.Size(buttonSize - 5, 20);
					button.Location = new System.Drawing.Point(buttonOffset, verticalOffset);
					buttonOffset += buttonSize;
				}
			}
		}
	}
}
