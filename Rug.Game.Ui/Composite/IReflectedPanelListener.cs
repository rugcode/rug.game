﻿
namespace Rug.Game.Ui.Composite
{
    public interface IReflectedPanelListener
    {
        void ControlValuesChanged();
    }
}
