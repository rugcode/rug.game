﻿using Rug.Game.Core.Text;
using Rug.Game.Ui.Controls;
using Rug.Game.Ui.Dynamic;
using System;
using System.Drawing;

namespace Rug.Game.Ui.Composite
{
	public class LabeledSlider : Panel
	{
		private string m_Title;
		private string m_Format;
		private Slider m_Slider;
		private DynamicLabel m_ValueLabel;
		private Label m_TitleLabel; 

		public string Title
		{
			get { return m_Title; }
			set { m_Title = value; }
		}

		public string Format
		{
			get { return m_Format; }
			set { m_Format = value; }
		}

		public Slider Slider { get { return m_Slider; } }

		public DynamicLabel ValueLabel { get { return m_ValueLabel; } }

		public Label TitleLabel { get { return m_TitleLabel; } }

		public event SliderValueChangedEvent ValueChanged;
        public event EventHandler Changed; 

		public LabeledSlider()
		{
			this.ShowBackground = false;
			this.ShowBorder = false;

			m_TitleLabel = new Label();
			m_ValueLabel = new DynamicLabel();
			m_Slider = new Slider();			
		}

		public void Initiate(string text, float min, float max, float value, string format)
		{
			m_Title = text;
			m_Format = format; 

			int index = 1; 

			m_TitleLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			m_TitleLabel.FixedSize = true;
			m_TitleLabel.Size = new System.Drawing.Size(200, 15);
			m_TitleLabel.Location = new System.Drawing.Point(0, 0);
			m_TitleLabel.Text = m_Title;
			m_TitleLabel.FontType = FontType.Small;
			m_TitleLabel.IsVisible = true;
			m_TitleLabel.Padding = new System.Windows.Forms.Padding(5);
			m_TitleLabel.RelitiveZIndex = index++;
			this.Controls.Add(m_TitleLabel);

			m_ValueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			m_ValueLabel.FixedSize = true;
			m_ValueLabel.Size = new System.Drawing.Size(95, 15);
			m_ValueLabel.Location = new System.Drawing.Point(Size.Width - 100, 0);
			m_ValueLabel.MaxLength = 8;
			m_ValueLabel.Text = value.ToString(m_Format);
			m_ValueLabel.FontType = FontType.Small;
			m_ValueLabel.IsVisible = true;
			m_ValueLabel.Padding = new System.Windows.Forms.Padding(5);
			m_ValueLabel.RelitiveZIndex = index++;
			this.Controls.Add(m_ValueLabel);

			m_Slider = new Slider();
			m_Slider.MinValue = min;
			m_Slider.MaxValue = max;
			m_Slider.Size = new System.Drawing.Size(Size.Width - 10, 10);
			m_Slider.Location = new System.Drawing.Point(5, 20);
			m_Slider.IsVisible = true;
			m_Slider.Value = value;
			m_Slider.ValueChanged += new SliderValueChangedEvent(SliderValueChanged);
			m_Slider.RelitiveZIndex = index++;
			this.Controls.Add(m_Slider);

			this.Size = new Size(this.Size.Width, 30); 
		}

		public void Resize()
		{
			m_TitleLabel.Size = new System.Drawing.Size(200, 15);
			m_TitleLabel.Location = new System.Drawing.Point(0, 0);

			m_ValueLabel.Size = new System.Drawing.Size(95, 15);
			m_ValueLabel.Location = new System.Drawing.Point(Size.Width - 100, 0);

			m_Slider.Size = new System.Drawing.Size(Size.Width - 10, 10);
			m_Slider.Location = new System.Drawing.Point(5, 20);

			this.Size = new Size(this.Size.Width, 30); 
		}

		protected virtual void SliderValueChanged(Slider sender, float value)
		{
			m_ValueLabel.Text = value.ToString(m_Format); 

			if (ValueChanged != null)
			{
				ValueChanged(sender, value); 
			}

			if (Changed != null)
			{
				Changed(this, EventArgs.Empty); 
			}
		}

        protected void OnChanged(ReflectedLabeledSlider sender, EventArgs eventArgs)
        {
            if (Changed != null)
            {
                Changed(sender, eventArgs);
            }
        }

        protected void OnValueChanged(Dynamic.Slider sender, float value)
        {
            if (ValueChanged != null)
            {
                ValueChanged(sender, value);
            }
        }
	}
}
