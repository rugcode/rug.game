﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Text;
using System;
using System.IO;

namespace Rug.Game.Ui.Effects
{
	public class UiEffect : IResource, IDisposable
	{
		#region Private Members

		private FontMatrix m_TextFont;
		private UiLineEffect m_LineEffect; 
		private UiBoxesAndTextEffect m_BoxesAndTextEffect; 

		#endregion 

		#region Public Properties

		public FontMatrix TextFont
		{
			get { return m_TextFont; }
		}

		public string Name
		{
			get { return "User Interface"; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Program; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return m_TextFont.ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_TextFont.IsLoaded && m_LineEffect.IsLoaded && m_BoxesAndTextEffect.IsLoaded; }
		}

		

		#endregion

		public UiEffect(string fontPath)
		{
			m_TextFont = LoadFontMatrix(fontPath);

			m_LineEffect = new UiLineEffect(m_TextFont);
			m_BoxesAndTextEffect = new UiBoxesAndTextEffect(m_TextFont); 
		}

        private FontMatrix LoadFontMatrix(string path)
        {
            if (FileHelper.AliasExists(path) == true)
            {
                //using (Stream stream = PackageHelper.GetStream(path))
                Stream stream = PackageHelper.GetStream(path);
                {
                    return FontMatrix.Read(stream);
                }
            }
            else if (FileHelper.FileExists(path) == true)
            {
                return FontMatrix.Read(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find FontMatrix file at path '{0}'", path));
            }
        }

		public void Render(UiSceneBuffer buffer)
		{
			if (IsLoaded == false)
			{
				return;
			}

			m_LineEffect.Render(buffer);

			m_BoxesAndTextEffect.Render(buffer);
		}

		public void LoadResources()
		{
			m_TextFont.LoadResources();
			m_BoxesAndTextEffect.LoadResources();
			m_LineEffect.LoadResources(); 
		}

		public void UnloadResources()
		{
			m_TextFont.UnloadResources();
			m_BoxesAndTextEffect.UnloadResources();
			m_LineEffect.UnloadResources(); 
		}

		#region IDisposable Members

		public void Dispose()
		{
			m_TextFont.Dispose(); 
		}

		#endregion
	}

	public class UiLineEffect : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Ui/Lines";

		private FontMatrix m_TextFont;

		#endregion


		public override string Name
		{
			get { return "User Interface: Lines"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public FontMatrix TextFont { get { return m_TextFont; } }

		public UiLineEffect(FontMatrix font)
		{
			m_TextFont = font;
		}

		public void Render(UiSceneBuffer buffer)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			if (buffer.IsValid == false)
			{
				return; 
			}

			GL.UseProgram(ProgramHandle);
			
			GL.BindBuffer(BufferTarget.ArrayBuffer, buffer.Lines.ResourceHandle);
			SimpleVertex.Bind();

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, buffer.LineIndices.ResourceHandle);

			GL.DrawElements(PrimitiveType.Lines, buffer.LineIndicesCount, buffer.IndexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}

	public class UiBoxesAndTextEffect : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Ui/BoxesAndText";

		private FontMatrix m_TextFont;
		private int m_FontTexture;

		#endregion


		public override string Name
		{
			get { return "User Interface: Boxes And Text"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public FontMatrix TextFont { get { return m_TextFont; } }

		public UiBoxesAndTextEffect(FontMatrix font)
		{
			m_TextFont = font;
		}

		public void Render(UiSceneBuffer buffer)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);
            
			GLState.ActiveTexture(TextureUnit.Texture0);
			GL.BindTexture(TextureTarget.Texture2D, m_TextFont.ResourceHandle);

			GL.BindBuffer(BufferTarget.ArrayBuffer, buffer.Triangles.ResourceHandle);
			SimpleVertex.Bind();

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, buffer.TriangleIndices.ResourceHandle);

			GL.DrawElements(PrimitiveType.Triangles, buffer.TriangleIndicesCount, buffer.IndexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_FontTexture = GL.GetUniformLocation(ProgramHandle, "fontTexture");

			GL.Uniform1(m_FontTexture, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}		

		#endregion
	}
}
