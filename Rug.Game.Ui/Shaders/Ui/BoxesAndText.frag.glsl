#version 410

uniform sampler2D fontTexture; 

in vec4 output_col;
in vec2 output_tex;

out vec4 colorFrag;

void main()
{
    colorFrag = texture(fontTexture, output_tex) * output_col;
}