#version 410

in vec4 output_col;

out vec4 colorFrag;

void main()
{
    colorFrag = output_col;
}