﻿using OpenTK.Graphics;
using System;
using System.Xml;

namespace Rug.Game.Ui
{
	public class UiTheme
	{
		private UiStyle[] m_Styles = new UiStyle[(int)DisplayMode.Dimmed + 1];

		public UiTheme()
		{
			#region Create default styles
 
			m_Styles[(int)DisplayMode.Normal] = new UiStyle()
			{
				LineColor = new Color4(1f, 1f, 1f, 0.5f),
				BackColor = new Color4(0.1f, 0.1f, 0.1f, 0.75f),
				TextColor = new Color4(1f, 1f, 1f, 1f)
			};

			m_Styles[(int)DisplayMode.Focused] = new UiStyle()
			{
				LineColor = new Color4(1f, 1f, 1f, 0.5f),
				BackColor = new Color4(0.4f, 0.4f, 0.4f, 0.75f),
				TextColor = new Color4(0f, 0f, 0f, 1f)
			};

			m_Styles[(int)DisplayMode.Hovering] = new UiStyle()
			{
				LineColor = new Color4(1f, 1f, 1f, 0.5f),
				BackColor = new Color4(0f, 0.33f, 0f, 0.75f),
				TextColor = new Color4(1f, 1f, 1f, 1f)
			};

			m_Styles[(int)DisplayMode.Open] = new UiStyle()
			{
				LineColor = new Color4(1f, 1f, 1f, 0.5f),
				BackColor = new Color4(1f, 1f, 1f, 1f),
				TextColor = new Color4(0f, 0f, 0f, 1f)
			};

			m_Styles[(int)DisplayMode.Highlight] = new UiStyle()
			{
				LineColor = new Color4(0f, 0f, 0f, 1f),
				BackColor = new Color4(1f, 1f, 1f, 1f),
				TextColor = new Color4(0f, 1f, 0f, 1f)
			};

			m_Styles[(int)DisplayMode.Dimmed] = new UiStyle()
			{
				LineColor = new Color4(1f, 1f, 1f, 0.5f),
				BackColor = new Color4(0.1f, 0.1f, 0.1f, 0.75f),
				TextColor = new Color4(1f, 1f, 1f, 0.5f)
			};

			#endregion
		}

		public UiStyle this[DisplayMode mode]
		{
			get
			{
				if (mode == DisplayMode.Auto)
				{
					throw new ArgumentOutOfRangeException("mode", "Display mode Auto is not valid and should be resolved to one of the other modes");
				}

				return m_Styles[(int)mode];
			}

			set
			{
				if (mode == DisplayMode.Auto)
				{
					throw new ArgumentOutOfRangeException("mode", "Display mode Auto is not valid and should be resolved to one of the other modes");
				}

				m_Styles[(int)mode] = value;
			}
		}

		public void Load(XmlNode node)
		{
			for (int i = 0; i < m_Styles.Length; i++)
			{
				XmlNode config = node.SelectSingleNode(((DisplayMode)i).ToString());
				if (config != null) m_Styles[i].Load(config);
			}
		}

		public void Save(XmlElement node)
		{
			for (int i = 0; i < m_Styles.Length; i++)
			{
				XmlElement config = Helper.CreateElement(node, ((DisplayMode)i).ToString());

				node.AppendChild(config);

				m_Styles[i].Save(config);
			}
		}
	}
}
