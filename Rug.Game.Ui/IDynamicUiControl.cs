﻿using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;

namespace Rug.Game.Ui
{
	public interface IDynamicUiControl
	{
		bool IsVisible { get; set; }

		bool IsParentVisible { get; }

		void GetTotalDynamicElementCounts(ref int LineVerts, ref int LinesIndices, ref int TriangleVerts, ref int TriangleIndices);

		void WriteDynamicElements(View3D view, 
									DataStream LineVerts, ref int LineVertsCount, 
									DataStream LinesIndices, ref int LinesIndicesCount, 
									DataStream TriangleVerts, ref int TriangleVertsCount, 
									DataStream TriangleIndices, ref int TriangleIndicesCount);
	}
}
