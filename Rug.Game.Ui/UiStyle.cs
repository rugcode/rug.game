﻿using OpenTK.Graphics;
using System.Xml;

namespace Rug.Game.Ui
{
	public class UiStyle
	{
		public Color4 BackColor;
		public Color4 LineColor;
		public Color4 TextColor;

		public void Load(XmlNode node)
		{
			BackColor = Helper.DeserializeColor4(Helper.GetAttributeValue(node, "BackColor", Helper.SerializeColor4(BackColor)));
			LineColor = Helper.DeserializeColor4(Helper.GetAttributeValue(node, "LineColor", Helper.SerializeColor4(LineColor)));
			TextColor = Helper.DeserializeColor4(Helper.GetAttributeValue(node, "TextColor", Helper.SerializeColor4(TextColor)));
		}

		public void Save(XmlElement node)
		{
			Helper.AppendAttributeAndValue(node, "BackColor", Helper.SerializeColor4(BackColor));
			Helper.AppendAttributeAndValue(node, "LineColor", Helper.SerializeColor4(LineColor));
			Helper.AppendAttributeAndValue(node, "TextColor", Helper.SerializeColor4(TextColor));
		}
	}
}
