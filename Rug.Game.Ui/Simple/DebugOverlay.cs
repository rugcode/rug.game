﻿using OpenTK.Graphics;
using Rug.Cmd;
using Rug.Game.Core.Text;
using Rug.Game.Ui.Controls;
using Rug.Game.Ui.Dynamic;
using System;

namespace Rug.Game.Ui.Simple
{
	public class DebugOverlay : Panel
	{
		#region Private Members

		private ConsoleControl m_Console;
		private Panel m_StatusPanel;				

		#endregion

        public MultiGraph Graphs { get; set; }

		public DebugOverlay()
		{
			this.ShowBackground = true;
			this.ShowBorder = true;
		}

		#region Initialize Controls

		public void InitializeControls()
		{
			int consoleLines = 4;
			this.Size = new System.Drawing.Size(this.Size.Width, (((int)TextRenderHelper.MessureString(" ", FontType.Monospaced, 1f).Height + 1) * (consoleLines + 1)));

			int index = 2;

			m_Console = new ConsoleControl();
			m_Console.NumberOfChars = 98;
			m_Console.NumberOfLines = consoleLines;
			m_Console.ConsoleBuffer = Rug.Game.Environment.ConsoleBuffer;
			m_Console.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
			m_Console.Docking = System.Windows.Forms.DockStyle.Left;
			m_Console.IsVisible = true;
			m_Console.RelitiveZIndex = index;
			m_Console.ShowBackground = false;
			m_Console.ShowBorder = false;

			this.Controls.Add(m_Console);

			CreateStatusBar();
		}

		#endregion

		#region Create Status Bar

		private void CreateStatusBar()
		{
			#region Status Bar

			int index = 10;

			m_StatusPanel = new Panel();

			m_StatusPanel.ShowBackground = false;
			m_StatusPanel.ShowBorder = false;
			m_StatusPanel.Docking = System.Windows.Forms.DockStyle.Fill;
			m_StatusPanel.IsVisible = true;
			m_StatusPanel.RelitiveZIndex = index++;
			m_StatusPanel.Size = new System.Drawing.Size(152, 60);
			m_StatusPanel.BackgroundColor = new Color4(0, 0, 0, 0.8f);

			Controls.Add(m_StatusPanel);

			index = 1;

			Graphs = new MultiGraph();
			Graphs.Location = new System.Drawing.Point(2, 2);
			Graphs.Docking = System.Windows.Forms.DockStyle.Top;
			Graphs.Size = new System.Drawing.Size(150, 48);
			Graphs.IsVisible = true;
			Graphs.RelitiveZIndex = index++;
			Graphs.ShowBackground = false;
			Graphs.ShowBorder = false;
			m_StatusPanel.Controls.Add(Graphs);

			AddGraphs(m_StatusPanel, Graphs, ref index);

			#endregion
		}

		protected virtual void AddGraphs(Panel panel, MultiGraph graph, ref int index)
		{
				
		}

		protected virtual void UpdateGraphs()
		{			
		}

		#endregion

		#region Resize

		public void Resize()
		{

		}

		#endregion

		#region Update Dynamic Controls

		public void Update()
		{
			try
			{
				UpdateGraphs();
			}
			catch (Exception ex)
			{
				RC.Sys.WriteException(55, "Exception updating panels", ex);

				if (RC.Sys.ShouldWrite(ConsoleVerbosity.Debug) == false)
				{
					RC.Sys.WriteStackTrace(ex.StackTrace);
				}

				int innerID = 0;
				Exception e = ex.InnerException;
				while (e != null)
				{
					RC.WriteException(01, "Panel Exception (Inner " + innerID++ + ")", e);

					if (RC.ShouldWrite(ConsoleVerbosity.Debug) == false)
					{
						RC.WriteStackTrace(e.StackTrace);
					}

					e = e.InnerException;
				}
			}
		}		

		#endregion
	}
}
