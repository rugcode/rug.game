﻿using Rug.Game.Core.Rendering;

namespace Rug.Game.Ui.Simple
{	
	public delegate void PanelEvent(string currentPanel);

	public class VisiblePanelController
	{
		#region Private Members

		private string m_CurrentPanel = null;
		private Dialogs m_Dialogs = new Dialogs();
		private View3D m_InterfaceView;

		#endregion

		#region Public Properties and Events

		public string CurrentPanel { get { return m_CurrentPanel; } set { m_CurrentPanel = value; OnPanelChanged(); } }

		public Dialogs Dialogs { get { return m_Dialogs; } }

		public View3D InterfaceView
		{
			get { return m_InterfaceView; }
			set { m_InterfaceView = value; }
		}

		public event PanelEvent PanelChanged;

		#endregion

		private void OnPanelChanged()
		{
			if (PanelChanged != null)
			{
				PanelChanged(m_CurrentPanel);
			}
		}

		public void SetOrToggleCurrentPanel(string screenPanel)
		{
			if (CurrentPanel == screenPanel)
			{
				CurrentPanel = null;
			}
			else
			{
				CurrentPanel = screenPanel;
			}
		}
	}
}
