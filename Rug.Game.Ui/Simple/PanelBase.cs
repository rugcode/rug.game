﻿using Rug.Game.Core.Text;
using Rug.Game.Core.Textures;
using Rug.Game.Ui.Composite;
using Rug.Game.Ui.Controls;
using Rug.Game.Ui.Dynamic;
using Rug.Game.Ui.Menus;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Rug.Game.Ui.Simple
{
	public abstract class PanelBase : Panel
	{
		private static System.Drawing.Point ColourDialogLocation = System.Drawing.Point.Empty;  

		#region Private Members

		private VisiblePanelController m_PanelControler;		

		#endregion

		#region Public Properties

		public abstract string ScreenPanel { get; }

		public VisiblePanelController PanelControler { get { return m_PanelControler; } protected set { m_PanelControler = value; } }

		#endregion

		public PanelBase(VisiblePanelController controler, int index)
		{
			m_PanelControler = controler;

			if (m_PanelControler != null)
			{
				m_PanelControler.PanelChanged += new PanelEvent(m_PanelControler_PanelChanged);
			}

			this.Size = new System.Drawing.Size(300, 300);
			this.Location = new System.Drawing.Point(15, 15);
			this.ShowBackground = true;
			this.ShowBorder = true;
			this.RelitiveZIndex = index;
		}

		void m_PanelControler_PanelChanged(string currentPanel)
		{
			this.IsVisible = ScreenPanel == currentPanel; 
		}

		#region Abstract Methods
		
		public abstract void Initiate();

		public abstract void Align();

		public abstract void AddMenuItems(MenuBar menu);

		public abstract void ResetControlValues();

		public abstract void UpdateControls();

		#endregion

		#region Build Reflected Controls

		protected void BuildReflectedControls(LayoutPanel panel, ref int index, ref int verticalOffset, ref int column, ref int columns, Type type, EventHandler changedEvent)
		{
			Dictionary<ControlGroupAttribute, List<ControlOptionsStub>> groups = new Dictionary<ControlGroupAttribute, List<ControlOptionsStub>>();

			FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

			foreach (FieldInfo info in fields)
			{
				object[] groupAttribs = info.GetCustomAttributes(typeof(ControlGroupAttribute), true);

				if (groupAttribs.Length == 0)
				{
					continue;
				}

				ControlGroupAttribute group = groupAttribs[0] as ControlGroupAttribute;

				object[] optionsAttribs = info.GetCustomAttributes(typeof(ControlOptionsAttribute), true);

				if (optionsAttribs.Length > 0)
				{
					if (groups.ContainsKey(group) == false)
					{
						groups.Add(group, new List<ControlOptionsStub>());
					}

					groups[group].Add(new ControlOptionsStub(optionsAttribs[0] as ControlOptionsAttribute, info));
				}
			}

			List<ControlGroupAttribute> groupKeys = new List<ControlGroupAttribute>(groups.Keys);

			groupKeys.Sort();

			foreach (ControlGroupAttribute key in groupKeys)
			{
				List<ControlOptionsStub> controls = groups[key];

				controls.Sort();

				foreach (ControlOptionsStub stub in controls)
				{
					if (stub.Field.FieldType.IsEnum == true)
					{
						ReflectedToggleButtonGroup group = new ReflectedToggleButtonGroup(stub.Attribute.Name, stub.Field);
						verticalOffset += 10;

						AddButtonSet_NoTitle(panel,
								Enum.GetValues(stub.Field.FieldType),
								Enum.GetNames(stub.Field.FieldType),
								group.OnChanged, ref index, ref verticalOffset, group.Buttons);

						group.Changed += changedEvent;

						List<UiControlBase> layout = new List<UiControlBase>();

                        foreach (ToggleButton button in group.Buttons)
                        {
                            layout.Add(button);
                        }

						panel.ButtonGroups.Add(group);
						panel.Layout.Add(layout);

						//verticalOffset -= 10;
                        verticalOffset += 20;
					}
					else if (stub.Attribute is SliderOptionsAttribute)
					{
						ReflectedLabeledSlider slider;

						AddReflectedSlider(panel, stub.Field, null, changedEvent, ref index, ref verticalOffset, column, columns, out slider);

						panel.AllSliders.Add(slider);

						panel.NamedControls.Add(slider.Title, slider);
					}
				}

				verticalOffset += 20;
			}

			verticalOffset -= 20;
		}

		protected void AddReflectedSlider(Panel panel, FieldInfo field, object instance,
									EventHandler changedEvent,
									ref int index, ref int verticalOffset, int column, int columns,
									out ReflectedLabeledSlider slider)
		{
			int location = (Size.Width / columns) * column;

			slider = new ReflectedLabeledSlider(field);

			slider.Location = new System.Drawing.Point(location, verticalOffset);
			slider.Size = new System.Drawing.Size(panel.Size.Width / columns, 30);
			slider.Changed += changedEvent;
			slider.RelitiveZIndex = index++;

			panel.Controls.Add(slider);

			slider.Initiate(instance);

			verticalOffset += slider.Size.Height;
		}

		protected void AddButtonSet_NoTitle(Panel panel, Array obj, string[] text, EventHandler click, ref int index, ref int verticalOffset, List<ToggleButton> buttons)
		{
			int totalSpace = panel.Size.Width - 5;

			int buttonSize = ((totalSpace - (5 * obj.Length)) / obj.Length);
			int buttonSpace = buttonSize + 5;
			int currentButtonOffset = 5;

			for (int i = 0; i < obj.Length; i++)
			{
				buttons.Add(AddToggleButton(panel, obj.GetValue(i), text[i], click, buttonSize, buttonSpace, ref currentButtonOffset, ref index, ref verticalOffset));
			}
		}

		#endregion

		#region Panel Build Helpers

		protected void AddSlider(Panel panel, string text,
								float min, float max, float value, SliderValueChangedEvent changed,
								ref int index, ref int verticalOffset,
								out Slider slider, out DynamicLabel valueLabel)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = text;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			valueLabel = new DynamicLabel();
			valueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			valueLabel.FixedSize = true;
			valueLabel.Size = new System.Drawing.Size(95, 15);
			valueLabel.Location = new System.Drawing.Point(Size.Width - 100, verticalOffset);
			valueLabel.MaxLength = 12;
			valueLabel.Text = value.ToString("N3");
			valueLabel.FontType = FontType.Small;
			valueLabel.IsVisible = true;
			valueLabel.Padding = new System.Windows.Forms.Padding(5);
			valueLabel.RelitiveZIndex = index++;
			panel.Controls.Add(valueLabel);

			verticalOffset += 20;

			slider = new Slider();
			slider.MinValue = min;
			slider.MaxValue = max;
			slider.Size = new System.Drawing.Size(Size.Width - 10, 10);
			slider.Location = new System.Drawing.Point(5, verticalOffset);
			slider.IsVisible = true;
			slider.Value = value;
			slider.ValueChanged += changed;
			slider.RelitiveZIndex = index++;
			panel.Controls.Add(slider);

			verticalOffset += 10;
		}

        protected void AddSlider(Panel panel, string text,
                                float min, float max, float value, SliderValueChangedEvent changed,
                                ref int index, int xOffset, ref int verticalOffset, int reserveX,
                                out Slider slider, out DynamicLabel valueLabel)
        {
            Label label = new Label();
            label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            label.FixedSize = true;
            label.Size = new System.Drawing.Size(200, 15);
            label.Location = new System.Drawing.Point(xOffset, verticalOffset);
            label.Text = text;
            label.FontType = FontType.Small;
            label.IsVisible = true;
            label.Padding = new System.Windows.Forms.Padding(5);
            label.RelitiveZIndex = index++;
            panel.Controls.Add(label);

            valueLabel = new DynamicLabel();
            valueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            valueLabel.FixedSize = true;
            valueLabel.Size = new System.Drawing.Size(95, 15);
            valueLabel.Location = new System.Drawing.Point(Size.Width - (100 + reserveX + xOffset), verticalOffset);
            valueLabel.MaxLength = 12;
            valueLabel.Text = value.ToString("N3");
            valueLabel.FontType = FontType.Small;
            valueLabel.IsVisible = true;
            valueLabel.Padding = new System.Windows.Forms.Padding(5);
            valueLabel.RelitiveZIndex = index++;
            panel.Controls.Add(valueLabel);

            verticalOffset += 20;

            slider = new Slider();
            slider.MinValue = min;
            slider.MaxValue = max;
            slider.Size = new System.Drawing.Size(Size.Width - (10 + reserveX + xOffset), 10);
            slider.Location = new System.Drawing.Point(5 + xOffset, verticalOffset);
            slider.IsVisible = true;
            slider.Value = value;
            slider.ValueChanged += changed;
            slider.RelitiveZIndex = index++;
            panel.Controls.Add(slider);

            verticalOffset += 10;
        }

        protected void AddSlider(Panel panel, string text,
                                float min, float max, float value, SliderValueChangedEvent changed,
                                ref int index, ref int verticalOffset,
                                out Slider slider, out DynamicLabel valueLabel, 
                                out RangeSlider valueRange, RangeSliderValueChangedEvent rangeChanged)
        {
            Label label = new Label();
            label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            label.FixedSize = true;
            label.Size = new System.Drawing.Size(200, 15);
            label.Location = new System.Drawing.Point(0, verticalOffset);
            label.Text = text;
            label.FontType = FontType.Small;
            label.IsVisible = true;
            label.Padding = new System.Windows.Forms.Padding(5);
            label.RelitiveZIndex = index++;
            panel.Controls.Add(label);

            valueLabel = new DynamicLabel();
            valueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            valueLabel.FixedSize = true;
            valueLabel.Size = new System.Drawing.Size(95, 15);
            valueLabel.Location = new System.Drawing.Point(Size.Width - 100, verticalOffset);
            valueLabel.MaxLength = 12;
            valueLabel.Text = value.ToString("N3");
            valueLabel.FontType = FontType.Small;
            valueLabel.IsVisible = true;
            valueLabel.Padding = new System.Windows.Forms.Padding(5);
            valueLabel.RelitiveZIndex = index++;
            panel.Controls.Add(valueLabel);

            verticalOffset += 20;

            slider = new Slider();
            slider.MinValue = min;
            slider.MaxValue = max;
            slider.Size = new System.Drawing.Size(Size.Width - 10, 10);
            slider.Location = new System.Drawing.Point(5, verticalOffset);
            slider.IsVisible = true;
            slider.Value = value;
            slider.ValueChanged += changed;
            slider.RelitiveZIndex = index++;
            panel.Controls.Add(slider);

            valueRange = new RangeSlider();
            valueRange.MinValue = 0;
            valueRange.MaxValue = 1;
            valueRange.Size = new System.Drawing.Size(Size.Width - 10, 10);
            valueRange.Location = new System.Drawing.Point(5, verticalOffset);
            valueRange.IsVisible = false;            
            valueRange.Value1 = 0;
            valueRange.Value2 = 1;
            valueRange.RelitiveZIndex = index++;
            panel.Controls.Add(valueRange);

            valueRange.ValueChanged += rangeChanged; 

            verticalOffset += 10;
        }

		protected void AddSliderAndButton(Panel panel, string text,
											float min, float max, float value, SliderValueChangedEvent changed,
											EventHandler buttonClicked,
											ref int index, ref int verticalOffset,
											out Slider slider, out DynamicLabel valueLabel)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = text;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			valueLabel = new DynamicLabel();
			valueLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
			valueLabel.FixedSize = true;
			valueLabel.Size = new System.Drawing.Size(95, 15);
			valueLabel.Location = new System.Drawing.Point(Size.Width - 100, verticalOffset);
			valueLabel.MaxLength = 12;
			valueLabel.Text = value.ToString("N3");
			valueLabel.FontType = FontType.Small;
			valueLabel.IsVisible = true;
			valueLabel.Padding = new System.Windows.Forms.Padding(5);
			valueLabel.RelitiveZIndex = index++;
			panel.Controls.Add(valueLabel);

			verticalOffset += 20;

			slider = new Slider();
			slider.MinValue = min;
			slider.MaxValue = max;
			slider.Size = new System.Drawing.Size(Size.Width - 40, 10);
			slider.Location = new System.Drawing.Point(5, verticalOffset);
			slider.IsVisible = true;
			slider.Value = value;
			slider.ValueChanged += changed;
			slider.RelitiveZIndex = index++;
			panel.Controls.Add(slider);

			Button openLocationButton = new Button();
			openLocationButton.Size = new System.Drawing.Size(25, 10);
			openLocationButton.Location = new System.Drawing.Point(Size.Width - 30, verticalOffset);
			openLocationButton.Text = "..";
			openLocationButton.FontType = FontType.Small;
			openLocationButton.IsVisible = true;
			openLocationButton.RelitiveZIndex = index++;
			openLocationButton.Click += buttonClicked;
			panel.Controls.Add(openLocationButton);
		
			verticalOffset += 10;
		}

		protected void AddPathControl(Panel panel, string text,
								string value, EventHandler openFileDialog, EventHandler openLocation,
								ref int index, ref int verticalOffset,
								out DynamicLabel valueLabel)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = text;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			verticalOffset += 20;

			valueLabel = new DynamicLabel();
			valueLabel.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			valueLabel.FixedSize = true;
			valueLabel.Size = new System.Drawing.Size(Size.Width - 70, 15);
			valueLabel.Location = new System.Drawing.Point(5, verticalOffset);
			valueLabel.MaxLength = 100;
			valueLabel.Text = value;
			valueLabel.FontType = FontType.Small;
			valueLabel.IsVisible = true;
			valueLabel.Padding = new System.Windows.Forms.Padding(5);
			valueLabel.RelitiveZIndex = index++;
			panel.Controls.Add(valueLabel);

			Button openFileDialogButton = new Button();
			openFileDialogButton.Size = new System.Drawing.Size(25, 20);
			openFileDialogButton.Location = new System.Drawing.Point(Size.Width - 65, verticalOffset);
			openFileDialogButton.Text = "...";
			openFileDialogButton.FontType = FontType.Small;
			openFileDialogButton.IsVisible = true;
			openFileDialogButton.RelitiveZIndex = index++;
			openFileDialogButton.Click += openFileDialog; 
			panel.Controls.Add(openFileDialogButton);

			Button openLocationButton = new Button();
			openLocationButton.Size = new System.Drawing.Size(30, 20);
			openLocationButton.Location = new System.Drawing.Point(Size.Width - 35, verticalOffset);
			openLocationButton.Text = "Open";
			openLocationButton.FontType = FontType.Small;
			openLocationButton.IsVisible = true;
			openLocationButton.RelitiveZIndex = index++;
			openLocationButton.Click += openLocation;
			panel.Controls.Add(openLocationButton);

			verticalOffset += 25;
		}


		protected void AddTextBoxAndButton(Panel panel, string text,
								string value, 
								ref int index, ref int verticalOffset,
								out TextBox textBox,
								out Button button)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = text;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			verticalOffset += 20;

			textBox = new TextBox();
			textBox.Text = value;
			textBox.IsVisible = true;
			textBox.FontType = FontType.Regular;
			textBox.MaxLength = 80;
			textBox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			textBox.Size = new System.Drawing.Size(Size.Width - 45, 20);
			textBox.Location = new System.Drawing.Point(5, verticalOffset);
			textBox.RelitiveZIndex = index++;
			panel.Controls.Add(textBox);

			button = new Button();
			button.Size = new System.Drawing.Size(30, 20);
			button.Location = new System.Drawing.Point(Size.Width - 35, verticalOffset);
			button.Text = "";
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			panel.Controls.Add(button);

			verticalOffset += 30;
		}

		#endregion

		#region Control Builder Helpers

        protected void AddButtonSet_NoTitle(Panel panel, object[] obj, string[] text, EventHandler click, ref int index, ref int verticalOffset, int totalSpace, List<ToggleButton> buttons)
		{
			int buttonSize = ((totalSpace - (5 * obj.Length)) / obj.Length);
			int buttonSpace = buttonSize + 5;
			int currentButtonOffset = 5;

			for (int i = 0; i < obj.Length; i++)
			{
				buttons.Add(AddToggleButton(panel, obj[i], text[i], click, buttonSize, buttonSpace, ref currentButtonOffset, ref index, ref verticalOffset));
			}

			verticalOffset += 30;
		}

		protected void AddButtonSet_NoTitle(Panel panel, object[] obj, string[] text, EventHandler click, ref int index, ref int verticalOffset, int totalSpace, List<Button> buttons)
		{
			int buttonSize = ((totalSpace - (5 * obj.Length)) / obj.Length);
			int buttonSpace = buttonSize + 5;
			int currentButtonOffset = 5;

			for (int i = 0; i < obj.Length; i++)
			{
				buttons.Add(AddButton(panel, obj[i], text[i], click, buttonSize, buttonSpace, ref currentButtonOffset, ref index, ref verticalOffset));
			}

			verticalOffset += 30;
		}

		protected void AddButtonSet(Panel panel, string labelText, object[] obj, string[] text, EventHandler click, ref int index, ref int verticalOffset, List<ToggleButton> buttons)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = labelText;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			verticalOffset += 20;

			int totalSpace = Size.Width - 5;

			int buttonSize = ((totalSpace - (5 * obj.Length)) / obj.Length);
			int buttonSpace = buttonSize + 5;
			int currentButtonOffset = 5;

			for (int i = 0; i < obj.Length; i++)
			{
				buttons.Add(AddToggleButton(panel, obj[i], text[i], click, buttonSize, buttonSpace, ref currentButtonOffset, ref index, ref verticalOffset));
			}

			verticalOffset += 30;
		}

		protected void AddButtonSet(Panel panel, string labelText, object[] obj, string[] text, EventHandler click, ref int index, ref int verticalOffset, List<Button> buttons)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = labelText;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			verticalOffset += 20;

			int totalSpace = Size.Width - 5;

			int buttonSize = ((totalSpace - (5 * obj.Length)) / obj.Length);
			int buttonSpace = buttonSize + 5;
			int currentButtonOffset = 5;

			for (int i = 0; i < obj.Length; i++)
			{
				buttons.Add(AddButton(panel, obj[i], text[i], click, buttonSize, buttonSpace, ref currentButtonOffset, ref index, ref verticalOffset));
			}

			verticalOffset += 30;
		}

		protected ToggleButton AddToggleButton(Panel panel, object obj, string text, EventHandler click, int buttonSize, int buttonSpace, ref int currentButtonOffset, ref int index, ref int verticalOffset)
		{
			ToggleButton button = new ToggleButton();
			button.Size = new System.Drawing.Size(buttonSize, 20);
			button.Location = new System.Drawing.Point(currentButtonOffset, verticalOffset);
			button.Text = text;
			button.Tag = obj;
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			button.Click += click;
			panel.Controls.Add(button);

			currentButtonOffset += buttonSpace;

			return button;
		}

		protected void AddButtonSet(Panel panel, string labelText, object[] obj, string[] text, EventHandler[] click, ref int index, ref int verticalOffset, List<Button> buttons)
		{
			Label label = new Label();
			label.TextAlign = System.Drawing.ContentAlignment.TopLeft;
			label.FixedSize = true;
			label.Size = new System.Drawing.Size(200, 15);
			label.Location = new System.Drawing.Point(0, verticalOffset);
			label.Text = labelText;
			label.FontType = FontType.Small;
			label.IsVisible = true;
			label.Padding = new System.Windows.Forms.Padding(5);
			label.RelitiveZIndex = index++;
			panel.Controls.Add(label);

			verticalOffset += 20;

			int totalSpace = Size.Width - 5;

			int buttonSize = ((totalSpace - (5 * obj.Length)) / obj.Length);
			int buttonSpace = buttonSize + 5;
			int currentButtonOffset = 5;

			for (int i = 0; i < obj.Length; i++)
			{
				buttons.Add(AddButton(panel, obj[i], text[i], click[i], buttonSize, buttonSpace, ref currentButtonOffset, ref index, ref verticalOffset));
			}

			verticalOffset += 30;
		}

		protected Button AddButton(Panel panel, object obj, string text, EventHandler click, int buttonSize, int buttonSpace, ref int currentButtonOffset, ref int index, ref int verticalOffset)
		{
			Button button = new Button();
			button.Size = new System.Drawing.Size(buttonSize, 20);
			button.Location = new System.Drawing.Point(currentButtonOffset, verticalOffset);
			button.Text = text;
			button.Tag = obj;
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			button.Click += click;
			panel.Controls.Add(button);

			currentButtonOffset += buttonSpace;

			return button;
		}

		protected Button AddButton(Panel panel, string text, EventHandler click, ref int index, ref int verticalOffset)
		{
			Button button = new Button();
			button.Size = new System.Drawing.Size(Size.Width - 5, 20);
			button.Location = new System.Drawing.Point(5, verticalOffset);
			button.Text = text;
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			button.Click += click;
			panel.Controls.Add(button);

			verticalOffset += 30;

			return button;
		}

		protected ToggleButton AddToggleButton(Panel panel, string text, EventHandler click, ref int index, ref int verticalOffset)
		{
			ToggleButton button = new ToggleButton();
			button.Size = new System.Drawing.Size(Size.Width - 5, 20);
			button.Location = new System.Drawing.Point(5, verticalOffset);
			button.Text = text;
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			button.Click += click;
			panel.Controls.Add(button);

			verticalOffset += 30;

			return button;
		}

		protected ImageButton AddImageButton(Panel panel, Texture2D image, EventHandler click, ref int index, ref int verticalOffset)
		{
			ImageButton button = new ImageButton();
			button.Size = new System.Drawing.Size(Size.Width - 5, 20);
			button.Location = new System.Drawing.Point(5, verticalOffset);
			button.Text = "";
			button.Image = image; 
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			button.Click += click;
			panel.Controls.Add(button);

			verticalOffset += 30;

			return button;
		}

		protected ImageToggleButton AddImageToggleButton(Panel panel, Texture2D image, EventHandler click, ref int index, ref int verticalOffset)
		{
			ImageToggleButton button = new ImageToggleButton();
			button.Size = new System.Drawing.Size(Size.Width - 5, 20);
			button.Location = new System.Drawing.Point(5, verticalOffset);
			button.Text = "";
			button.Image = image; 
			button.FontType = FontType.Small;
			button.IsVisible = true;
			button.RelitiveZIndex = index++;
			button.Click += click;
			panel.Controls.Add(button);

			verticalOffset += 30;

			return button;
		}

		#endregion
	}
}
