﻿using OpenTK.Graphics;
using Rug.Game.Core.Text;
using Rug.Game.Ui.Controls;
using Rug.Game.Ui.Dynamic;
using System;
using System.Diagnostics;

namespace Rug.Game.Ui.Simple
{
	public class BasicDebugOverlay : DebugOverlay
	{
		#region Private Members

		private Graph m_FPSGraph;
		private DynamicLabel m_FPSLabel;
		
		private Graph m_DrawCallsGraph;
		private DynamicLabel m_DrawCallsLabel;
		private float m_DrawCallsAccumulator = 0;

		private Graph m_BufferUpdatesGraph;
		private DynamicLabel m_BufferUpdatesLabel;
		private float m_BufferUpdatesAccumulator = 0;
        private Graph m_WorkingSetGraph;
        private DynamicLabel m_WorkingSetLabel;

        private int m_WorkingSetTick = 0;
        private long m_WorkingSet = 0;

		#endregion

		public BasicDebugOverlay() { }

		#region Add Graphs

		protected override void AddGraphs(Panel panel, MultiGraph graph, ref int index)
        {
            m_FPSGraph = new Graph(300)
            {
                ShowTicks = true,
                TickSpace = 10,
                TickOffset = 10, 
                MajorTicksEvery = 6,
                IsVisible = true,
                LineColor = new Color4(0.3f, 1f, 0.3f, 0.75f),
                MajorTickLineColor = Color4.White,
            };
            graph.Graphs.Add(m_FPSGraph);

            m_FPSLabel = new DynamicLabel()
            {
                Padding = new System.Windows.Forms.Padding(5, 2, 5, 2),
                ForeColor = new Color4(0.3f, 1f, 0.3f, 0.75f),
                MaxLength = 16,
                Location = new System.Drawing.Point(154, 0),
                Size = new System.Drawing.Size(80, 20),
                FixedSize = true,
                FontType = FontType.Small,
                TextAlign = System.Drawing.ContentAlignment.TopLeft,
                Docking = System.Windows.Forms.DockStyle.Left,
                IsVisible = true,
                RelitiveZIndex = index++,
            };
            panel.Controls.Add(m_FPSLabel);

            m_DrawCallsGraph = new Graph(300)
            {
                ShowTicks = false,
                TickSpace = 10,
                MajorTicksEvery = 6,
                IsVisible = true,
                LineColor = new Color4(1f, 1f, 0.3f, 0.75f),
            };
            graph.Graphs.Add(m_DrawCallsGraph);


            m_DrawCallsLabel = new DynamicLabel()
            {
                Padding = new System.Windows.Forms.Padding(5, 2, 5, 2),
                ForeColor = new Color4(1f, 1f, 0.3f, 0.75f),
                MaxLength = 19,
                Location = new System.Drawing.Point(154, 0),
                Size = new System.Drawing.Size(80, 20),
                FixedSize = true,
                FontType = FontType.Small,
                TextAlign = System.Drawing.ContentAlignment.TopLeft,
                Docking = System.Windows.Forms.DockStyle.Left,
                IsVisible = true,
                RelitiveZIndex = index++,
            };
            panel.Controls.Add(m_DrawCallsLabel);


            m_BufferUpdatesGraph = new Graph(300)
            {
                ShowTicks = false,
                TickSpace = 10,
                MajorTicksEvery = 6,
                IsVisible = true,
                LineColor = new Color4(1f, 0.3f, 1f, 0.75f),
            };
            graph.Graphs.Add(m_BufferUpdatesGraph);


            m_BufferUpdatesLabel = new DynamicLabel()
            {
                Padding = new System.Windows.Forms.Padding(5, 2, 5, 2),
                ForeColor = new Color4(1f, 0.3f, 1f, 0.75f),
                MaxLength = 16,
                Location = new System.Drawing.Point(154, 0),
                Size = new System.Drawing.Size(80, 20),
                FixedSize = true,
                FontType = FontType.Small,
                TextAlign = System.Drawing.ContentAlignment.TopLeft,
                Docking = System.Windows.Forms.DockStyle.Left,
                IsVisible = true,
                RelitiveZIndex = index++,
            };
            panel.Controls.Add(m_BufferUpdatesLabel);


            m_WorkingSetGraph = new Graph(300)
            {
                ShowTicks = false,
                TickSpace = 10,
                MajorTicksEvery = 6,
                IsVisible = true,
                LineColor = new Color4(0.3f, 0.3f, 1f, 0.75f),
            };
            graph.Graphs.Add(m_WorkingSetGraph);


            m_WorkingSetLabel = new DynamicLabel()
            {
                Padding = new System.Windows.Forms.Padding(5, 2, 5, 2),
                ForeColor = new Color4(0.3f, 0.3f, 1f, 0.75f),
                MaxLength = 16,
                Location = new System.Drawing.Point(154, 0),
                Size = new System.Drawing.Size(80, 20),
                FixedSize = true,
                FontType = FontType.Small,
                TextAlign = System.Drawing.ContentAlignment.TopLeft,
                Docking = System.Windows.Forms.DockStyle.Left,
                IsVisible = true,
                RelitiveZIndex = index++,
            };
            panel.Controls.Add(m_WorkingSetLabel);
        }

		#endregion 

		#region Update Graphs

		protected override void UpdateGraphs()
		{
			m_DrawCallsAccumulator += (float)Rug.Game.Environment.DrawCalls;
			m_BufferUpdatesAccumulator += (float)Rug.Game.Environment.BufferUpdates;

			if (Rug.Game.Environment.FramesClick == true)
			{
                m_FPSGraph.TickOffset -= 1;

                if (m_FPSGraph.TickOffset <= 0)
                {
                    m_FPSGraph.TickOffset += m_FPSGraph.TickSpace;
                    
                    m_FPSGraph.MajorTickOffset += 1;

                    if (m_FPSGraph.MajorTickOffset > m_FPSGraph.MajorTicksEvery)
                    {
                        m_FPSGraph.MajorTickOffset -= m_FPSGraph.MajorTicksEvery; 
                    }
                }

                this.Scene.Invalidate(); 

				m_FPSLabel.Text = "FPS: " + Rug.Game.Environment.FramesPerSecond.ToString("N3");
				m_FPSGraph.AddValue(Rug.Game.Environment.FramesPerSecond);

				m_DrawCallsLabel.Text = "Draw: " + (m_DrawCallsAccumulator / Math.Max(Rug.Game.Environment.FramesPerSecond, 1f)).ToString("N3");
				m_DrawCallsGraph.AddValue(m_DrawCallsAccumulator / Math.Max(Rug.Game.Environment.FramesPerSecond, 1f));

				m_DrawCallsAccumulator = 0;

				m_BufferUpdatesLabel.Text = "Buffer: " + (m_BufferUpdatesAccumulator / Math.Max(Rug.Game.Environment.FramesPerSecond, 1f)).ToString("N3");
				m_BufferUpdatesGraph.AddValue(m_BufferUpdatesAccumulator / Math.Max(Rug.Game.Environment.FramesPerSecond, 1f));

				m_BufferUpdatesAccumulator = 0;

                //if (System.Environment.Is64BitProcess == true)
                if (m_WorkingSetTick++ % 60 == 0)         
                {
                    m_WorkingSet = Process.GetCurrentProcess().WorkingSet64;
                }
                //else 
                //{
                //    workingSet = (double)Process.GetCurrentProcess().WorkingSet; 
                //}

                m_WorkingSetLabel.Text = "Mem: " + Rug.Cmd.CmdHelper.GetMemStringFromBytes(m_WorkingSet, true);

                // m_WorkingSetGraph.MaxValue = (float)(((double)Process.GetCurrentProcess().MaxWorkingSet / 1024.0) / 1024.0)
                m_WorkingSetGraph.AddValue((float)(((double)m_WorkingSet / 1024.0) / 1024.0));  
			}
		}

		#endregion
	}
}
