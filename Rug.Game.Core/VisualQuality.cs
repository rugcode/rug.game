﻿
namespace Rug.Game
{
    public enum VisualQuality : int
    { 
        Disabled = 0, 
        Terrible = 1, 
        Acceptable = 2, 
        Superior = 3, 
        Preposterous = 4,
    }
}
