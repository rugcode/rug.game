﻿using OpenTK;
using Rug.Game.Core.Rendering;
using System.Windows.Forms;

namespace Rug.Game.Input
{
	public interface IInputHandler
	{
		bool Enabled { get; set; } 

		bool HasFocus { get; } 

		void OnKeyDown(KeyEventArgs args, out bool handled);
		void OnKeyUp(KeyEventArgs args, out bool handled);
		void OnKeyPress(char @char, out bool handled);

		void OnMouseDown(View3D view, Vector2 mousePosition, MouseButtons mouseButtons, out bool handled);
		void OnMouseUp(View3D view, Vector2 mousePosition, MouseButtons mouseButtons, out bool handled);
		void OnMouseClick(View3D view, Vector2 mousePosition, MouseButtons mouseButtons, out bool handled);

		void OnMouseMoved(View3D view, Vector2 mousePosition, out bool handled);
		void OnMouseWheel(MouseEventArgs e, out bool handled);

		void Invalidate();
	}
}
