﻿using OpenTK;
using Rug.Game.Core.Rendering;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Rug.Game.Input
{
	public class InputController : IInputHandler
	{
		public readonly List<IInputHandler> InputHandlers = new List<IInputHandler>();
		
		public bool Enabled { get; set; }

		public bool HasFocus { get { return false; } }

		public void Invalidate() { } 

		public void OnKeyDown(KeyEventArgs args, out bool handled)
		{
			handled = false; 

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false || 
					handler.HasFocus == false)
				{
					continue; 
				}

				handler.OnKeyDown(args, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return; 
				}
			}
		}

		public void OnKeyUp(KeyEventArgs args, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false ||
					handler.HasFocus == false)
				{
					continue;
				}

				handler.OnKeyUp(args, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}

		public void OnKeyPress(char @char, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false ||
					handler.HasFocus == false)
				{
					continue;
				}

				handler.OnKeyPress(@char, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}

		public void OnMouseDown(View3D view, Vector2 mousePosition, MouseButtons mouseButtons, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false)
				{
					continue;
				}

				handler.OnMouseDown(view, mousePosition, mouseButtons, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}

		public void OnMouseUp(View3D view, Vector2 mousePosition, MouseButtons mouseButtons, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false)
				{
					continue;
				}

				handler.OnMouseUp(view, mousePosition, mouseButtons, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}

		public void OnMouseClick(View3D view, Vector2 mousePosition, MouseButtons mouseButtons, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false)
				{
					continue;
				}

				handler.OnMouseClick(view, mousePosition, mouseButtons, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}

		public void OnMouseMoved(View3D view, Vector2 mousePosition, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false)
				{
					continue;
				}

				handler.OnMouseMoved(view, mousePosition, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}

		public void OnMouseWheel(MouseEventArgs e, out bool handled)
		{
			handled = false;

			foreach (IInputHandler handler in InputHandlers)
			{
				if (handler.Enabled == false)
				{
					continue;
				}

				handler.OnMouseWheel(e, out handled);

				if (handled == true)
				{
					handler.Invalidate();

					return;
				}
			}
		}
	}
}
