﻿using System.Diagnostics;

namespace Rug.Game
{
	/// <summary>
	/// A mechanism for tracking elapsed time.
	/// </summary>
	public class Clock
	{
		#region Public Interface

		/// <summary>
		/// Initializes a new instance of the <see cref="Clock"/> class.
		/// </summary>
		public Clock()
		{
			m_Frequency = Stopwatch.Frequency;
		}

		public void Start()
		{
			m_Count = Stopwatch.GetTimestamp();
			m_IsRunning = true;
		}

		/// <summary>
		/// Updates the clock.
		/// </summary>
		/// <returns>The time, in seconds, that elapsed since the previous update.</returns>
		public float Update()
		{
			float result = 0.0f;
			if (m_IsRunning)
			{
				long last = m_Count;
				m_Count = Stopwatch.GetTimestamp();
				result = (float)(m_Count - last) / m_Frequency;
			}

			return result;
		}

        public float TimeSinceLastUpdate()
        {
            float result = 0.0f;
            if (m_IsRunning)
            {
                long last = m_Count;
                long next = Stopwatch.GetTimestamp();
                result = (float)(next - last) / m_Frequency;
            }

            return result;
        }

		#endregion

		#region Implementation Detail

		private readonly long m_Frequency;
		private bool m_IsRunning;
		private long m_Count;

		#endregion

        public float SpinTill(float initial, float value)
        {
            float result = initial;
            if (m_IsRunning)
            {
                long count = m_Count; 

                while (result < value)
                {
                    long last = count;
                    count = Stopwatch.GetTimestamp();
                    result += (float)(count - last) / m_Frequency;
                }
            }

            return result;
        }
    }
}
