﻿using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;

namespace Rug.Game.Core.Buffers
{
	public class FrameBufferTexture2D : Texture2D
	{
		private FrameBufferTexture2DInfo m_ResourceInfo; 

		public new FrameBufferTexture2DInfo ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public FrameBufferTexture2D(string name, ResourceMode mode, FrameBufferTexture2DInfo resourceInfo)
			: base(name, mode, resourceInfo)
		{
			m_ResourceInfo = resourceInfo;
		}
	}
}
