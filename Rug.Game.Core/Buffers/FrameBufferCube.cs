﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Core.Buffers
{
    public class FrameBufferCube : IResource
    {
        public string Name { get; private set; }

        public ResourceType ResourceType { get { return ResourceType.FrameBuffer; } } 

        public ResourceMode ResourceMode { get; private set; }

        public TextureCubeInfo ResourceInfo { get; private set; }

        IResourceInfo IResource.ResourceInfo { get { return this.ResourceInfo; } }

        public uint ResourceHandle { get; private set; }

        public uint DepthResourceHandle { get; private set; }

        public bool IsLoaded { get; private set; }

        //public FrameBufferTexture2D DepthBuffer { get; private set; }

        public TextureCube TextureCube { get; private set; } 

        public FrameBufferCube(string name, ResourceMode mode, TextureCubeInfo resourceInfo)
        {
            Name = name;
            ResourceMode = mode;
            ResourceInfo = resourceInfo;

            TextureCube = new TextureCube(name + " Cube", mode, resourceInfo); 

            //depthInfo.TextureTarget

            //DepthBuffer = new FrameBufferTexture2D(depthInfo.Name, mode, depthInfo); 
        }

        public void LoadResources()
        {
            TextureCube.LoadResources(); 

            //DepthBuffer.LoadResources();
            //DepthBuffer.CreateBlank();

            /* 
            uint depthTexture;
            GL.GenTextures(1, out depthTexture);
            DepthResourceHandle = depthTexture;
            GLState.CheckError();

            GL.TexImage2D(TextureTarget.Texture2D, 0, (PixelInternalFormat)All.DepthComponent24, ResourceInfo.Size.Width, ResourceInfo.Size.Height, 0, PixelFormat.DepthComponent, PixelType.UnsignedInt, IntPtr.Zero);
            GLState.CheckError();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
            GLState.CheckError();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
            GLState.CheckError();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GLState.CheckError();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GLState.CheckError();
            GL.BindTexture(TextureTarget.Texture2D, 0);
            */ 

            uint resourceHandle;
            GL.GenFramebuffers(1, out resourceHandle);
            ResourceHandle = resourceHandle;
            GLState.CheckError(); 

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, ResourceHandle);
            GLState.CheckError(); 

            for (int i = 0; i < 6; i++)
            {
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, TextureTarget.TextureCubeMapPositiveX + i, TextureCube.ResourceHandle, 0);
                GLState.CheckError(); 
            }

            //GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, DepthResourceHandle, 0);
            //GLState.CheckError();
            GLHelper.CheckFramebufferExt();

            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
            GLState.CheckError(); 
            
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GLState.CheckError(); 

            IsLoaded = true; 
        }

        public void UnloadResources()
        {
            TextureCube.UnloadResources();

            uint depthResourceHandle = DepthResourceHandle;
            uint resourceHandle = ResourceHandle;

            GL.DeleteTextures(1, ref depthResourceHandle);
            GL.DeleteBuffers(1, ref resourceHandle);

            IsLoaded = false; 
        }

        public void Bind()
        {
            GL.Enable(EnableCap.TextureCubeMapSeamless);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, ResourceHandle);
        }

        public void Bind(int face)
        {
            GL.DrawBuffer(DrawBufferMode.ColorAttachment0 + face);
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + face, TextureTarget.TextureCubeMapPositiveX + face, TextureCube.ResourceHandle, 0);
        }

        public void Unbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            TextureCube.GenerateMipMap(); 
        }

        /* 
            int DepthTexture;

            GL.GenTextures(1, out DepthTexture);
            GL.BindTexture(TextureTarget.TextureCubeMap, DepthTexture);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)WrapS);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)WrapT);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)WrapR);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)MinFilter);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)MagFilter);

            GL.TexImage2D(TextureTarget.TextureCubeMap, 0, PixelInternalFormat.DepthComponent24, Size.Width, Size.Height, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.BindTexture(TextureTarget.TextureCubeMap, 0);

            int FBO;
            GL.GenFramebuffers(1, out FBO);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
            for (int i = 0; i < 6; i++)
            {
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, TextureTarget.TextureCubeMapPositiveX + i, ColorTexture, 0);
            }

            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, DepthTexture, 0);

            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
            fboMsg = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer).ToString();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        */
       
    }
}

/* 
public int[] FBO;
        public int[] ColorTexture;
        public int[] DepthTexture;
        public int DepthMap;
        private string fboMsg;
        private Camera camera;
        public CubeFBO()
        {
            FBO = new int[1];
            ColorTexture = new int[1];
            GL.GenTextures(1, ColorTexture);
            GL.BindTexture(TextureTarget.TextureCubeMap, ColorTexture[0]);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter,
                            (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter,
                   (int)TextureMinFilter.Nearest);
            for (int i = 0; i < 6; i++)
            {
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, PixelInternalFormat.Rgba16f, 512, 512, 0, PixelFormat.Bgra, PixelType.HalfFloat, IntPtr.Zero);
            }
            GL.BindTexture(TextureTarget.TextureCubeMap, 0);
 
            DepthTexture = new int[1];
 
            GL.GenTextures(1, DepthTexture);
            GL.BindTexture(TextureTarget.TextureCubeMap, DepthTexture[0]);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS,
                            (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT,
                            (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter,
                            (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter,
                            (int)TextureMinFilter.Nearest);
            GL.TexImage2D(TextureTarget.TextureCubeMap, 0, PixelInternalFormat.DepthComponent24, 512, 512, 0,
                          PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.BindTexture(TextureTarget.TextureCubeMap, 0);
 
            GL.GenFramebuffers(1, FBO);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO[0]);
            for (int i = 0; i < 6; i++)
            {
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, TextureTarget.TextureCubeMapPositiveX + i, ColorTexture[0], 0);
 
            }
            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, DepthTexture[0], 0);
 
            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
            fboMsg = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer).ToString();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            camera = new Camera();
            camera.Fov = 90;
        }
 
        public Vector3 pos;
        private Matrix4 tempMatrix;
        public void Render()
        {
            GL.Enable(EnableCap.TextureCubeMapSeamless);
            Camera.SetFov(90);  // changes perspective matrix and is later sent to the shader during RenderScene();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO[0]);
            for (int i = 0; i < 6; i++)
            {
                GL.DrawBuffer(DrawBufferMode.ColorAttachment0 + i);
                Background.Render();  // this clears the buffers 
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, TextureTarget.TextureCubeMapPositiveX + i, ColorTexture[0], 0);
 
                switch (i)
                {
                    case 0: Camera.ViewMatrix = Matrix4.LookAt(pos, pos + Vector3.UnitX, Vector3.UnitY);  //pos is the center of the cubemap in world coordinates
                        break;
                    case 1: Camera.ViewMatrix = Matrix4.LookAt(pos, pos - Vector3.UnitX, Vector3.UnitY); // camera view matrix is later sent to the shader during RenderScene();
                        break;
                    case 2: Camera.ViewMatrix = Matrix4.LookAt(pos, pos + Vector3.UnitY, Vector3.UnitZ);
                        break;
                    case 3: Camera.ViewMatrix = Matrix4.LookAt(pos, pos - Vector3.UnitY, -Vector3.UnitZ);
                        break;
                    case 4: Camera.ViewMatrix = Matrix4.LookAt(pos, pos + Vector3.UnitZ, Vector3.UnitY);
                        break;
                    case 5: Camera.ViewMatrix = Matrix4.LookAt(pos, pos - Vector3.UnitZ, Vector3.UnitY);
                        break;
                }
               RenderScene();  
            }
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
         }
 
        public void Destroy()
        {
            GL.DeleteTextures(1, ColorTexture);
            GL.DeleteTextures(1, DepthTexture);
            GL.DeleteBuffers(1, FBO);
        }
 
        public void BindTexture(int texUnit)
        {
            Renderer.ActiveTexture(texUnit);
            GL.BindTexture(TextureTarget.TextureCubeMap, ColorTexture[0]);
        }
 */ 