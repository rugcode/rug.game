﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System.Collections.Generic;

namespace Rug.Game.Core.Buffers
{
	public class VertexArrayObject : IResource
	{
		private string m_Name;
		private ResourceMode m_ResourceMode;
		private VertexArrayObjectInfo m_ResourceInfo;
		private bool m_IsLoaded; 

		private uint m_VAO;
		private uint[] m_BufferHandles;
		private IBuffer[] m_Buffers; 

		#region IResource Members

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.VertexArrayObject; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_ResourceMode; }
		}

		public VertexArrayObjectInfo ResourceInfo
		{
			get { return m_ResourceInfo; } 
		}

		IResourceInfo IResource.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_VAO; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public bool IsValid
		{
			get;
			private set;
		}

		public VertexArrayObject(string name, ResourceMode mode, IBuffer[] buffers)
		{
			m_Name = name;
			m_ResourceMode = mode; 

			List<IBufferInfo> infos = new List<IBufferInfo>(); 

			foreach (IBuffer buffer in buffers) 
			{				
				infos.Add(buffer.ResourceInfo); 
			}

			m_Buffers = buffers; 

			m_ResourceInfo = new VertexArrayObjectInfo(infos.ToArray()); 
		}

		public void LoadResources()
		{
			if (m_IsLoaded == true)
			{
				return; 
			}

			Environment.RefrenceCount++;
			Environment.ResourceNames.Add(this.Name);

			// Create the VAO
			GL.GenVertexArrays(1, out m_VAO);

			ErrorCode error = GL.GetError();

			if (error != ErrorCode.NoError)
			{
				RC.WriteLine(error.ToString());
			}

			GL.BindVertexArray(m_VAO);

			error = GL.GetError();

			if (error != ErrorCode.NoError)
			{
				RC.WriteLine(error.ToString());
			}

			m_BufferHandles = new uint[m_Buffers.Length];

			// Create the buffers for the vertices attributes
			GL.GenBuffers(m_Buffers.Length, m_BufferHandles);

			error = GL.GetError();

			if (error != ErrorCode.NoError)
			{
				RC.WriteLine(error.ToString());
			}

			int vertexAttribArray = 0; 

			for (int i = 0; i < m_Buffers.Length; i++) 
			{
				m_Buffers[i].LoadResources(m_BufferHandles[i]);

				if (m_Buffers[i] is InstanceBuffer)
				{
					m_Buffers[i].ResourceInfo.Format.CreateLayout(ref vertexAttribArray, 1); 
				}
				else if (m_Buffers[i] is VertexBuffer)
				{
					m_Buffers[i].ResourceInfo.Format.CreateLayout(ref vertexAttribArray); 
				}
				else if (m_Buffers[i] is IndexBuffer)
				{
					// do nothing
				}
			}

			error = GL.GetError();

			if (error != ErrorCode.NoError)
			{
				RC.WriteLine(error.ToString());
			}

			// Make sure the VAO is not changed from the outside
			GL.BindVertexArray(0);
			
			m_IsLoaded = true;
			IsValid = true; 

		}

		public void UnloadResources()
		{
			if (m_IsLoaded == false)
			{
				return; 
			}

			Environment.RefrenceCount--;
			Environment.ResourceNames.Remove(this.Name);

			foreach (IBuffer buffer in m_Buffers)
			{
				buffer.UnloadResources(); 
			}

			GL.DeleteVertexArrays(1, ref m_VAO);
			
			IsValid = false;
			m_IsLoaded = false; 
		}

		#endregion

		public void Bind()
		{
			ErrorCode error;

			GL.BindVertexArray(m_VAO);
			
			error = GL.GetError();

			if (error != ErrorCode.NoError)
			{
				RC.WriteLine(error.ToString());
				IsValid = false;
			}
		}

		public void Unbind()
		{
			GL.BindVertexArray(0);

			ErrorCode error = GL.GetError();

			if (error != ErrorCode.NoError)
			{
				RC.WriteLine(error.ToString());
				IsValid = false;
			}
		}
	}
}
