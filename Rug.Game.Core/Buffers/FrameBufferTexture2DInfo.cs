﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Core.Buffers
{
	public class FrameBufferTexture2DInfo : Texture2DInfo
	{
		private string m_Name;
		private FramebufferAttachment m_Attachment;

		public string Name { get { return m_Name; } }

		public FramebufferAttachment Attachment { get { return m_Attachment; } }

		public FrameBufferTexture2DInfo(string name, FramebufferAttachment attachment)
		{
			m_Name = name;
			m_Attachment = attachment; 
		}

		public override string ToString()
		{
			return String.Format("{0}, {1}, {2}", Name, Attachment, base.ToString());
		}
	}
}
