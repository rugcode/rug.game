﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Buffers
{
	public interface IBuffer : IResource
	{
		IBufferInfo ResourceInfo { get; }

		void Bind();

		void Unbind();

		void MapBuffer(BufferAccess mode, out DataStream stream);

		void UnmapBuffer();

		void LoadResources(uint handle);
	}
}
