﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Core.Buffers
{
	public class ShaderStorageBufferInfo : IBufferInfo
	{
		private int m_Stride;
		private IVertexFormat m_Format; 
		private int m_Count;
		private BufferUsageHint m_Usage;

		public int Stride
		{
			get { return m_Stride; }
		}

		public IVertexFormat Format
		{
			get { return m_Format; }
		}

		public BufferUsageHint Usage
		{
			get { return m_Usage; }
		}

		public virtual ResourceType ResourceType
		{
			get { return Resources.ResourceType.ShaderStorageBuffer; }
		}

		public int Count
		{
			get { return m_Count; }
			set { m_Count = value; }
		}

		public ShaderStorageBufferInfo(IVertexFormat format, int count, BufferUsageHint usage)
		{
			m_Format = format;
			m_Stride = m_Format.Stride;
			m_Count = count;
			m_Usage = usage; 
		}		
	
		public void OnLoad()
		{
			GL.BufferData(BufferTarget.ShaderStorageBuffer, (IntPtr)(m_Count * m_Stride), IntPtr.Zero, m_Usage);
		}
	}
}
