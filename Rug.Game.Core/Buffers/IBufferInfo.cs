﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Buffers
{
	public interface IBufferInfo : IResourceInfo
	{
		int Stride { get; }

		IVertexFormat Format { get; }

		BufferUsageHint Usage { get; }

		int Count { get; set; }
	}
}
