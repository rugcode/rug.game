﻿
namespace Rug.Game.Core.Buffers
{
	public interface IVertexFormat
	{
		int Stride { get; } 
	
		void CreateLayout(ref int baseLocation);
		
		void CreateLayout(ref int baseLocation, int devisor);
	}
}
