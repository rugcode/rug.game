﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Buffers
{
	public class InstanceBufferInfo : VertexBufferInfo
	{
		public override ResourceType ResourceType
		{
			get { return Resources.ResourceType.InstanceBuffer; }
		}

		public InstanceBufferInfo(IVertexFormat format, int count, BufferUsageHint usage)
			: base(format, count, usage)
		{
		}
	}
}
