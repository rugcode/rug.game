﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Buffers
{
	public class InstanceBuffer : VertexBuffer
	{		
		private InstanceBufferInfo m_ResourceInfo;

		public new InstanceBufferInfo ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public InstanceBuffer(string name, ResourceMode mode, InstanceBufferInfo resourceInfo) 
			: base(name, mode, resourceInfo)
		{
			m_ResourceInfo = resourceInfo; 
		}		
	}
}
