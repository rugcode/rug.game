﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game.Core.Buffers
{
	public class FrameBuffer : IResource
	{
		private string m_Name;
		private ResourceMode m_Mode = ResourceMode.Static;
		private FrameBufferInfo m_ResourceInfo;
		private bool m_IsLoaded;
		private uint m_Handle;

		private DrawBuffersEnum[] m_DrawBuffers; 
		private FrameBufferTexture2D[] m_Buffers;
		private bool m_UseExternalBuffers; 

		#region IResource Members
		
		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.FrameBuffer; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_Mode; }
		}

		public FrameBufferInfo ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		IResourceInfo IResource.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_Handle; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public FrameBufferTexture2D this[FramebufferAttachment attachment]
		{
			get
			{
				for (int i = 0; i < m_Buffers.Length; i++)
				{
					if (m_Buffers[i].ResourceInfo.Attachment == attachment)
					{
						return m_Buffers[i];
					}
				}

				throw new Exception(attachment.ToString() + " is not present");
			}
		}

		public FrameBuffer(string name, ResourceMode mode, FrameBufferInfo resourceInfo)
		{
			m_UseExternalBuffers = false; 

			m_Name = name; 
			m_Mode = mode;
			m_ResourceInfo = resourceInfo;

			m_Buffers = new FrameBufferTexture2D[m_ResourceInfo.Count];

			List<DrawBuffersEnum> drawBuffers = new List<DrawBuffersEnum>(); 

			for (int i = 0; i < m_Buffers.Length; i++)
			{
				FrameBufferTexture2DInfo info = m_ResourceInfo[i];

				m_Buffers[i] = new FrameBufferTexture2D(info.Name, mode, info);

				if (info.Attachment != FramebufferAttachment.DepthAttachment &&
					info.Attachment != FramebufferAttachment.DepthAttachmentExt &&
					info.Attachment != FramebufferAttachment.DepthStencilAttachment &&
					info.Attachment != FramebufferAttachment.StencilAttachment &&
					info.Attachment != FramebufferAttachment.StencilAttachmentExt)
				{
					drawBuffers.Add((DrawBuffersEnum)(int)info.Attachment);
				}
			}

			m_DrawBuffers = drawBuffers.ToArray(); 
		}

		public FrameBuffer(string name, ResourceMode mode, FrameBufferTexture2D[] buffers, FrameBufferInfo resourceInfo)
		{
			m_UseExternalBuffers = true; 

			m_Name = name;
			m_Mode = mode;
			m_ResourceInfo = resourceInfo;

			m_Buffers = new FrameBufferTexture2D[m_ResourceInfo.Count];

			if (buffers.Length != m_Buffers.Length)
			{
				throw new Exception("Frame buffer '" + name + "' has different buffer count to the resource info"); 
			}

			List<DrawBuffersEnum> drawBuffers = new List<DrawBuffersEnum>();

			for (int i = 0; i < m_Buffers.Length; i++)
			{
				FrameBufferTexture2DInfo info = m_ResourceInfo[i];

				m_Buffers[i] = buffers[i]; 

				if (info.Attachment != FramebufferAttachment.DepthAttachment &&
					info.Attachment != FramebufferAttachment.DepthAttachmentExt &&
					info.Attachment != FramebufferAttachment.DepthStencilAttachment &&
					info.Attachment != FramebufferAttachment.StencilAttachment &&
					info.Attachment != FramebufferAttachment.StencilAttachmentExt)
				{
					drawBuffers.Add((DrawBuffersEnum)(int)info.Attachment);
				}
			}

			m_DrawBuffers = drawBuffers.ToArray();
		}

		public override string ToString()
		{
			return String.Format("{0}, {1}, {2}, ({3})", m_Name, m_Mode, ResourceType, ResourceInfo.ToString());
		}

		public void LoadResources()
		{
			if (m_Handle != 0 || m_IsLoaded == true)
			{
				throw new Exception("Attempt to load " + ResourceType.ToString() + " resource '" + Name + "', the resource is already loaded");
			}

			if (m_UseExternalBuffers == false)
			{
				foreach (FrameBufferTexture2D buffer in m_Buffers)
				{
					buffer.LoadResources();
					buffer.CreateBlank();
				}
			}

			GL.GenFramebuffers(1, out m_Handle);
			GL.BindFramebuffer(FramebufferTarget.FramebufferExt, m_Handle);

			for (int i = 0; i < m_Buffers.Length; i++) 
			{
				GL.FramebufferTexture2D(FramebufferTarget.FramebufferExt, m_ResourceInfo[i].Attachment, m_ResourceInfo[i].TextureTarget, m_Buffers[i].ResourceHandle, 0);
			}

			switch (GL.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
			{
				case FramebufferErrorCode.FramebufferCompleteExt:
					{
						RC.WriteLine(ConsoleVerbosity.Debug, ResourceType.ToString() + " resource '" + Name + "', The framebuffer is complete and valid for rendering.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', There are no attachments.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', Attachments are of different size. All attachments must have the same width and height.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', The color attachments have different format. All color attachments must have the same format.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferUnsupportedExt:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', This particular FBO configuration is not supported by the implementation.");
						break;
					}
				default:
					{
						RC.WriteLine(ResourceType.ToString() + " resource '" + Name + "', Status unknown. (yes, this is really bad.)");
						break;
					}
			}

			Unbind(); 

			m_IsLoaded = true; 
		}

		public void UnloadResources()
		{
			if (m_Handle == 0 || m_IsLoaded == false)
			{
				throw new Exception("Attempt to unload " + ResourceType.ToString() + " resource '" + Name + "', the resource is not loaded");
			}

			GL.DeleteFramebuffers(1, ref m_Handle);

			if (m_UseExternalBuffers == false)
			{
				foreach (FrameBufferTexture2D buffer in m_Buffers)
				{
					buffer.UnloadResources();
				}
			}

			m_Handle = 0;

			m_IsLoaded = false; 
		}

		#endregion

		public void Bind()
		{
            GLState.CheckError(); 
			GL.BindFramebuffer(FramebufferTarget.FramebufferExt, m_Handle);

            GLState.CheckError(); 
			GL.DrawBuffers(m_DrawBuffers.Length, m_DrawBuffers);            
		}

		public void Unbind()
		{
			GL.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
		}

        public void BlitTo(FrameBuffer dest, ReadBufferMode readFrom, DrawBufferMode writeTo, Rectangle sourceRect, Rectangle destRect, ClearBufferMask mask, BlitFramebufferFilter filter)
        {
            //glBindFramebufferEXT(GL_READ_FRAMEBUFFER_EXT, Gbuffers[0].FramebufferID);
            //glBindFramebufferEXT(GL_DRAW_FRAMEBUFFER_EXT, 0);
            //glBlitFramebufferEXT(0, 0, ResolutionX, ResolutionY, 0, 0, ResolutionX, ResolutionY, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
            //glBindFramebufferEXT(GL_READ_FRAMEBUFFER_EXT, 0);

            GLState.CheckError(); 
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, m_Handle);

            GLState.CheckError(); 
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, dest.m_Handle);

            GLState.CheckError(); 
            GL.ReadBuffer(readFrom);
            
            GLState.CheckError(); 
            GL.DrawBuffer(writeTo);

            GLState.CheckError(); 
            GL.BlitFramebuffer(
                sourceRect.X, sourceRect.Y, sourceRect.Right, sourceRect.Bottom,
                destRect.X, destRect.Y, destRect.Right, destRect.Bottom,
                mask, filter);

            GLState.CheckError(); 
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0);
            
            GLState.CheckError(); 
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0);

            GLState.CheckError(); 
        }

        public void BlitTo(FrameBufferCube dest, ReadBufferMode readFrom, int face, Rectangle sourceRect, Rectangle destRect, ClearBufferMask mask, BlitFramebufferFilter filter)
        {
            //glBindFramebufferEXT(GL_READ_FRAMEBUFFER_EXT, Gbuffers[0].FramebufferID);
            //glBindFramebufferEXT(GL_DRAW_FRAMEBUFFER_EXT, 0);
            //glBlitFramebufferEXT(0, 0, ResolutionX, ResolutionY, 0, 0, ResolutionX, ResolutionY, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
            //glBindFramebufferEXT(GL_READ_FRAMEBUFFER_EXT, 0);

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, m_Handle);

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, dest.ResourceHandle);

            GLState.CheckError();
            GL.ReadBuffer(readFrom);

            GLState.CheckError();
            GL.DrawBuffer(DrawBufferMode.ColorAttachment0 + face);

            GLState.CheckError();
            GL.BlitFramebuffer(
                sourceRect.X, sourceRect.Y, sourceRect.Right, sourceRect.Bottom,
                destRect.X, destRect.Y, destRect.Right, destRect.Bottom,
                mask, filter);

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0);

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0);

            GLState.CheckError();
        }


        public void Blit(ReadBufferMode readFrom, DrawBufferMode writeTo, Rectangle sourceRect, Rectangle destRect, ClearBufferMask mask, BlitFramebufferFilter filter)
        {

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, m_Handle);

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, 0);

            GLState.CheckError();
            GL.ReadBuffer(readFrom);

            GLState.CheckError();
            GL.BlitFramebuffer(
                sourceRect.X, sourceRect.Y, sourceRect.Right, sourceRect.Bottom,
                destRect.X, destRect.Y, destRect.Right, destRect.Bottom,
                mask, filter);

            GLState.CheckError();
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, 0);

            GLState.CheckError(); 
        }
    }
}
