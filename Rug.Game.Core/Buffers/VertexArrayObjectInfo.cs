﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Buffers
{
	public class VertexArrayObjectInfo : IResourceInfo
	{
		private IBufferInfo[] m_Buffers;		

		#region IResourceInfo Members

		public IBufferInfo[] Buffers
		{
			get { return m_Buffers; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.VertexArrayObject; }
		}

		public VertexArrayObjectInfo(IBufferInfo[] buffers)
		{
			m_Buffers = buffers;
		}

		public void OnLoad()
		{
			
		}

		#endregion
	}
}
