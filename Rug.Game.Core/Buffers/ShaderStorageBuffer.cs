﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Core.Buffers
{
	public class ShaderStorageBuffer : IBuffer
	{
		private string m_Name;
		private ResourceMode m_Mode = ResourceMode.Static;
		private ShaderStorageBufferInfo m_ResourceInfo;
		private bool m_IsLoaded;
		private uint m_Handle;
		private DataStream m_Stream;
		private bool m_IsMapped;
		private bool m_IsValid;

		#region IResource Members

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.ShaderStorageBuffer; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_Mode; }
		}

		public ShaderStorageBufferInfo ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		IBufferInfo IBuffer.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		IResourceInfo IResource.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_Handle; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public bool IsValid
		{
			get { return m_IsValid; }			
		}

		public ShaderStorageBuffer(string name, ResourceMode mode, ShaderStorageBufferInfo resourceInfo)
		{
			m_Name = name;
			m_Mode = mode;
			m_ResourceInfo = resourceInfo; 
		}

		public void LoadResources()
		{
			if (m_Handle != 0 || m_IsLoaded == true)
			{
				throw new Exception("Attempt to load " + ResourceType.ToString() + " resource '" + Name + "', the resource is already loaded");
			}

			Environment.RefrenceCount++;
			Environment.ResourceNames.Add(this.Name);

			GL.GenBuffers(1, out m_Handle);
			GL.BindBuffer(BufferTarget.ShaderStorageBuffer, m_Handle);

			m_ResourceInfo.OnLoad();

			m_IsLoaded = true; 
		}

		public void LoadResources(uint handle)
		{
			if (m_Handle != 0 || m_IsLoaded == true)
			{
				throw new Exception("Attempt to load " + ResourceType.ToString() + " resource '" + Name + "', the resource is already loaded");
			}

			m_Handle = handle;

			Environment.RefrenceCount++;
			Environment.ResourceNames.Add(this.Name);

			GL.BindBuffer(BufferTarget.ShaderStorageBuffer, m_Handle);

			m_ResourceInfo.OnLoad();

			m_IsLoaded = true; 
		}


		public void UnloadResources()
		{
			if (m_Handle == 0 || m_IsLoaded == false)
			{
				throw new Exception("Attempt to unload " + ResourceType.ToString() + " resource '" + Name + "', the resource is not loaded");
			}

			if (m_IsMapped == true)
			{
				UnmapBuffer(); 
			}

			if (m_Stream != null)
			{
				m_Stream.Dispose();

				m_Stream = null;
			}

			Environment.RefrenceCount--;
			Environment.ResourceNames.Remove(this.Name);

			GL.DeleteBuffers(1, ref m_Handle);
			m_Handle = 0; 

			m_IsLoaded = false;
		}

		#endregion

		public void Bind()
		{
			GL.BindBuffer(BufferTarget.ShaderStorageBuffer, m_Handle); 
		}

		public void Unbind()
		{
		}

		public void MapBuffer(BufferAccess mode, out DataStream stream)
		{
			if (m_IsMapped == true)
			{
				throw new Exception(ResourceType.ToString() + " resource '" + Name + "' is already mapped");
			}

			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Handle);

			IntPtr ptr = GL.MapBufferRange(BufferTarget.ShaderStorageBuffer, IntPtr.Zero, (IntPtr)(m_ResourceInfo.Count * m_ResourceInfo.Stride), BufferAccessMask.MapInvalidateBufferBit | BufferAccessMask.MapWriteBit);

			if (m_Stream == null)
			{			
				m_Stream = new DataStream(ptr, m_ResourceInfo.Count * m_ResourceInfo.Stride, mode);
			}
			else
			{
				m_Stream.Initiate(ptr, m_ResourceInfo.Count * m_ResourceInfo.Stride, mode); 
			}

			stream = m_Stream;

			m_IsMapped = true;
		}

		public void UnmapBuffer()
		{
			if (m_IsMapped == false)
			{
				throw new Exception(ResourceType.ToString() + " resource '" + Name + "' is not mapped");
			}

			GL.BindBuffer(BufferTarget.ShaderStorageBuffer, m_Handle);
			m_IsValid = GL.UnmapBuffer(BufferTarget.ShaderStorageBuffer);
            Rug.Game.Environment.BufferUpdatesAccumulator++;	

			m_IsMapped = false; 
		}
	}
}
