﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Core.Buffers
{
	public class IndexBufferInfo : IBufferInfo
	{
		private int m_Stride;
		private int m_Count;
		private BufferUsageHint m_Usage;
		private DrawElementsType m_IndexType;

		public DrawElementsType IndexType 
		{ 
			get { return m_IndexType; } 
		}

		public int Stride
		{
			get { return m_Stride; }
		}

		public IVertexFormat Format
		{
			get { return null; }
		}

		public BufferUsageHint Usage
		{
			get { return m_Usage; }
		}

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.IndexBuffer; }
		}

		public int Count
		{
			get { return m_Count; }
			set { m_Count = value; }
		}

		public IndexBufferInfo(DrawElementsType indexType, int stride, int count, BufferUsageHint usage)
		{
			m_IndexType = indexType; 
			m_Stride = stride;
			m_Count = count;
			m_Usage = usage; 
		}		
	
		public void OnLoad()
		{
			GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(m_Count * m_Stride), IntPtr.Zero, m_Usage);
		}
	}
}
