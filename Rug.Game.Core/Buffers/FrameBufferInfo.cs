﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace Rug.Game.Core.Buffers
{
	public class FrameBufferInfo : IResourceInfo
	{
		#region Private Members

		private FrameBufferTexture2DInfo[] m_BufferInfos;

		#endregion

		#region Public Properties

		public FrameBufferTexture2DInfo this[FramebufferAttachment attachment]
		{
			get 
			{
				for (int i = 0; i < m_BufferInfos.Length; i++)
				{
					if (m_BufferInfos[i].Attachment == attachment)
					{
						return m_BufferInfos[i]; 
					}
				}

				throw new Exception(attachment.ToString() + " is not present"); 
			}			
		}

		public FrameBufferTexture2DInfo this[int index]
		{
			get { return m_BufferInfos[index]; } 
		}

		public int Count
		{
			get { return m_BufferInfos.Length; } 
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.FrameBuffer; }
		}
	
		#endregion

		public FrameBufferInfo(params FrameBufferTexture2DInfo[] infos)
		{
			if (infos.Length == 0) 
			{
				throw new Exception("At least 1 buffer must be defined for Frame Buffer objects");  
			}

			List<FramebufferAttachment> attachments = new List<FramebufferAttachment>(); 

			foreach (FrameBufferTexture2DInfo info in infos) 
			{
				if (attachments.Contains(info.Attachment) == true) 
				{
					throw new Exception("A buffer with the Attachment '" + info.Attachment + "' has been defined more than once");  
				}

				attachments.Add(info.Attachment); 
			}

			m_BufferInfos = new FrameBufferTexture2DInfo[infos.Length]; 

			infos.CopyTo(m_BufferInfos, 0); 
		}

		public void OnLoad()
		{		
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			bool first = true; 

			foreach (FrameBufferTexture2DInfo info in m_BufferInfos)
			{
				if (first == false)
				{
					sb.Append(", "); 
				}
				
				sb.Append("{ " + info.ToString() + " }");

				first = false; 
			}

			return sb.ToString(); 
		}
	}
}
