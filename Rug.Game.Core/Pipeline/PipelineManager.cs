﻿using Rug.Cmd;
using Rug.Game.Core.Rendering;
using System;
using System.Collections.Generic;

namespace Rug.Game.Core.Pipeline
{
	public class PipelineManager : IPipeline, ICollection<IPipeline>
	{
		#region Private Members

		private bool m_IsEnabled = true;
		private PipelineMode m_Mode = PipelineMode.Update | PipelineMode.Render;
		private List<IPipeline> m_PipelineElements = new List<IPipeline>();
		private string m_Name = "Master";

		#endregion

		#region IPipeline Members

		public virtual string Name
		{
			get
			{
				return m_Name; 
			}
			set
			{
				m_Name = value;
			}
		}

		public virtual bool IsEnabled
		{
			get { return m_IsEnabled; }
			set { m_IsEnabled = value; }
		}

		public PipelineMode PipelineMode
		{
			get { return m_Mode; }
			protected set { m_Mode = value; } 
		}

		public virtual void Update(View3D view)
		{
			foreach (IPipeline pipeline in m_PipelineElements)
			{
				if (pipeline.IsEnabled == true && 
					PipelineHelper.ShouldRunUpdate(pipeline.PipelineMode) == true)
				{
					pipeline.Update(view); 
				}
			}
		}

		public virtual void Render(View3D view)
		{
			for (int i = 0; i < m_PipelineElements.Count; i++) 
			{
				IPipeline pipeline = m_PipelineElements[i];

				if (pipeline.IsEnabled == true && 
					PipelineHelper.ShouldRunBeginEndBlock(pipeline.PipelineMode) == true)
				{
					pipeline.Begin();
				}
			}

			foreach (IPipeline pipeline in m_PipelineElements)
			{
				if (pipeline.IsEnabled == true && 
					PipelineHelper.ShouldRunRender(pipeline.PipelineMode) == true)
				{
					pipeline.Render(view);
				}
			}

			for (int i = m_PipelineElements.Count - 1; i >= 0; i--)
			{
				IPipeline pipeline = m_PipelineElements[i];

				if (pipeline.IsEnabled == true && 
					PipelineHelper.ShouldRunBeginEndBlock(pipeline.PipelineMode) == true)
				{
					pipeline.End();
				}
			}
		}

		public virtual void Begin()
		{
			throw new NotImplementedException();
		}

		public virtual void End()
		{
			throw new NotImplementedException();
		}

		#endregion

		#region ICollection<IPipeline> Members

		public IPipeline this[int index]
		{
			get { return m_PipelineElements[index]; }
		}

		public void Add(IPipeline item)
		{
			m_PipelineElements.Add(item); 
		}

		public void Clear()
		{
			m_PipelineElements.Clear(); 
		}

		public bool Contains(IPipeline item)
		{
			return m_PipelineElements.Contains(item); 
		}

		public void CopyTo(IPipeline[] array, int arrayIndex)
		{
			m_PipelineElements.CopyTo(array, arrayIndex); 
		}

		public int Count
		{
			get { return m_PipelineElements.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove(IPipeline item)
		{
			return m_PipelineElements.Remove(item); 
		}

		#endregion

		#region IEnumerable<IPipeline> Members

		public IEnumerator<IPipeline> GetEnumerator()
		{
			return m_PipelineElements.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return (m_PipelineElements as System.Collections.IEnumerable).GetEnumerator(); 
		}

		#endregion

		public override string ToString()
		{
			return Name + ", Pipeline";
		}

		public void WriteUpdatePipelineToConsole(int indent)
		{
			string prefix = new String(' ', indent) + ConsoleChars.GetLineChar(ConsoleLineStyle.Single, LineChars.BottomLeft);

			RC.WriteLine(new String(' ', indent) + ToString());

			foreach (IPipeline pipeline in m_PipelineElements)
			{
				if (PipelineHelper.ShouldRunUpdate(pipeline.PipelineMode) == true)
				{
					if (pipeline is PipelineManager)
					{
						(pipeline as PipelineManager).WriteUpdatePipelineToConsole(indent + 1);
					}
					else
					{
						if (pipeline.IsEnabled == true)
						{
							RC.WriteLine(ConsoleThemeColor.SubTextGood, prefix + pipeline.ToString());
						}
						else
						{
							RC.WriteLine(ConsoleThemeColor.SubTextBad, prefix + pipeline.ToString());
						}
					}
				}
			}
		}

		public void WriteRenderPipelineToConsole(int indent)
		{
			string prefix = new String(' ', indent) + ConsoleChars.GetLineChar(ConsoleLineStyle.Single, LineChars.BottomLeft); 

			RC.WriteLine(new String(' ', indent) + ToString());

			for (int i = 0; i < m_PipelineElements.Count; i++)
			{
				IPipeline pipeline = m_PipelineElements[i];

				if (PipelineHelper.ShouldRunBeginEndBlock(pipeline.PipelineMode) == true)
				{
					if (pipeline.IsEnabled == true)
					{
						RC.WriteLine(ConsoleThemeColor.SubTextGood, prefix + "Begin " + pipeline.ToString()); 
					}
					else
					{
						RC.WriteLine(ConsoleThemeColor.SubTextBad, prefix + "Begin " + pipeline.ToString()); 
					}
				}
			}

			foreach (IPipeline pipeline in m_PipelineElements)
			{
				if (PipelineHelper.ShouldRunRender(pipeline.PipelineMode) == true)
				{
					if (pipeline is PipelineManager)
					{
						(pipeline as PipelineManager).WriteRenderPipelineToConsole(indent + 1); 
					}
					else 
					{
						if (pipeline.IsEnabled == true)
						{
							RC.WriteLine(ConsoleThemeColor.SubTextGood, prefix + pipeline.ToString());
						}
						else
						{
							RC.WriteLine(ConsoleThemeColor.SubTextBad, prefix + pipeline.ToString());
						}
					}
				}
			}

			for (int i = m_PipelineElements.Count - 1; i >= 0; i--)
			{
				IPipeline pipeline = m_PipelineElements[i];

				if (PipelineHelper.ShouldRunBeginEndBlock(pipeline.PipelineMode) == true)
				{
					if (pipeline.IsEnabled == true)
					{
						RC.WriteLine(ConsoleThemeColor.SubTextGood, prefix + "End " + pipeline.ToString());
					}
					else
					{
						RC.WriteLine(ConsoleThemeColor.SubTextBad, prefix + "End " + pipeline.ToString());
					}
				}
			}
		}
	}
}
