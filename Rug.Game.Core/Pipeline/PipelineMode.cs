﻿
namespace Rug.Game.Core.Pipeline
{
	public enum PipelineMode : int 
	{
		None = 0, 
		Render = 1, 
		Update = 2, 
		BeginEndBlock = 5,
	}
}
