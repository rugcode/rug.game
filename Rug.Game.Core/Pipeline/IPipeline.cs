﻿using Rug.Game.Core.Rendering;

namespace Rug.Game.Core.Pipeline
{
	public interface IPipeline
	{
		bool IsEnabled { get; set; }
		string Name { get; set; } 
		PipelineMode PipelineMode { get; }

        void Update(View3D view);
		void Render(View3D view);
		void Begin();
		void End(); 
	}
}
