﻿
namespace Rug.Game.Core.Pipeline
{
	public static class PipelineHelper
	{
		public static bool ShouldRunRender(PipelineMode mode)
		{
			return (mode & PipelineMode.BeginEndBlock) == PipelineMode.Render; 
		}

		public static bool ShouldRunUpdate(PipelineMode mode)
		{
			return (mode & PipelineMode.Update) == PipelineMode.Update;
		}

		public static bool ShouldRunBeginEndBlock(PipelineMode mode) 
		{
			return (mode & PipelineMode.BeginEndBlock) == PipelineMode.BeginEndBlock; 
		}
	}
}
