﻿
namespace Rug.Game.Core.Pipeline
{
	public interface IResizablePipeline : IPipeline
	{
		void Resize(int width, int height, MultiSamples samples); 
	}
}
