﻿using Rug.Game.Core.Rendering;

namespace Rug.Game.Core.Pipeline
{
	public abstract class PipelineBase : IPipeline
	{
		#region IPipeline Members

		public bool IsEnabled { get; set; }

		public string Name { get; set; }

		public abstract PipelineMode PipelineMode { get; }

		public PipelineBase()
		{
			IsEnabled = true; 
		}

        public abstract void Update(View3D view);

		public abstract void Render(View3D view);

		public abstract void Begin();

		public abstract void End();

		#endregion
	}
}
