﻿
namespace Rug.Game.Ui.Dynamic
{

    public enum SliderScale
    {
        Linear,

        /// <summary>
        /// Greater resolution at the low end
        /// </summary>
        Logarithmic,

        /// <summary>
        /// Greater resolution at the high end
        /// </summary>
        LogarithmicInverse,
    } 
}
