﻿
namespace Rug.Game
{
    public enum MultiSamples : int
    {
        X1 = 0,
        X2 = 1,
        X4 = 2,
        X8 = 3,
        X16 = 4,
    }	
}
