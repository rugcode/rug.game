﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Core.Resources
{
    public class ResourceManagerLoadProcess
    {
        private Stack<ResourceManagerLoadState> m_Stack = new Stack<ResourceManagerLoadState>();
        private IResourceManager m_RootManager;

        public ResourceManagerLoadState CurrentState 
        { 
            get 
            {
                if (m_Stack.Count > 0) 
                {
                    return m_Stack.Peek(); 
                }
                else 
                {
                    return null; 
                }
            }
        }

        public int Total { get; private set; }

        public int Progress { get; private set; }

        public bool IsDone { get { return Progress >= Total; } }

        public ResourceManagerLoadProcess(IResourceManager rootManager)
        {
            m_RootManager = rootManager;
           
            Total = m_RootManager.Total; 
        }


        public void Reset()
        {
            Progress = 0;
            Total = m_RootManager.Total; 
        }

        public int Increment()
        {
            Progress++;

            ResourceManagerLoadState state = CurrentState;

            int index = 0;

            if (state != null)
            {
                index = state.Index++; 
            }

            return index; 
        }

        public void LoadNext()
        {
            if (Progress == 0 && m_Stack.Count == 0)
            {
                Push(m_RootManager); 
            }

            ResourceManagerLoadState state = CurrentState;

            if (state != null)
            {
                state.Manager.LoadNext(this); 
            }
        }

        public void Pop()
        {
            m_Stack.Pop();
        }

        public void Push(IResourceManager resourceManager)
        {
            m_Stack.Push(new ResourceManagerLoadState(resourceManager)); 
        }
    }
}
