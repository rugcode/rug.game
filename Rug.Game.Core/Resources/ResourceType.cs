﻿
namespace Rug.Game.Core.Resources
{
	public enum ResourceType
	{
		VertexBuffer, 
		IndexBuffer, 
		UniformBuffer, 		
		InstanceBuffer, 
		VertexArrayObject,
		ShaderStorageBuffer,
		FrameBuffer, 
		Texture, 
		Program,
		Subset,
		Custom,

		AudioBank,
		AudioSource,
		AudioBuffer,
		AudioListener, 
	}
}
