﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Core.Resources
{
    public class ResourceManagerLoadState
    {
        public IResourceManager Manager { get; private set; }

        public int Index { get; set; }

        public ResourceManagerLoadState(IResourceManager manager)
        {
            Manager = manager; 
        }
    }
}
