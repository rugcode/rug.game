﻿
namespace Rug.Game.Core.Resources
{
	public interface IResource
	{
		string Name { get; } 

		ResourceType ResourceType { get; }

		ResourceMode ResourceMode { get; }

		IResourceInfo ResourceInfo { get; }

		uint ResourceHandle { get; } 

		bool IsLoaded { get; }
		
		void LoadResources();

		void UnloadResources();		
	}
}
