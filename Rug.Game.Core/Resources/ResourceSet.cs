﻿using System;
using System.Collections.Generic;

namespace Rug.Game.Core.Resources
{
	public class ResourceSet : ResourceSet<IResource>
	{
		public ResourceSet(string name, ResourceMode mode)
			: base(name, mode)
		{

		}
	}

	public class ResourceSet<ResType> : IResource, ICollection<ResType> where ResType : IResource
	{
		private readonly string m_Name;		
		private readonly ResourceMode m_Mode;
		private readonly List<ResType> m_Resources = new List<ResType>();

		private bool m_IsLoaded = false;

		#region IResource Members

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.Subset; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_Mode; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public ResType this[int index]
		{
			get { return m_Resources[index]; }
		}

		public event EventHandler<ResourceEventArgs> LoadStateChanged;

		public ResourceSet(string name, ResourceMode mode)
		{
			m_Name = name;
			m_Mode = mode;
		}

		public void LoadResources()
		{
			bool isLoaded = true;

			foreach (IResource res in m_Resources)
			{
				res.LoadResources();

				isLoaded |= res.IsLoaded;
			}

			OnLoadStateChanged(isLoaded);
		}

		public void UnloadResources()
		{
			bool isLoaded = false;

			for (int i = m_Resources.Count - 1; i >= 0; i--)
			{
				IResource res = m_Resources[i];

				res.UnloadResources();

				isLoaded |= res.IsLoaded;
			}

			OnLoadStateChanged(isLoaded);
		}

		private void OnLoadStateChanged(bool isLoaded)
		{
			if (m_IsLoaded != isLoaded)
			{
				m_IsLoaded = isLoaded;

				if (LoadStateChanged != null)
				{
					LoadStateChanged(this, new ResourceEventArgs(this));
				}
			}
		}

		#endregion

		#region ICollection<IResource> Members

		public virtual void Add(ResType item)
		{
			m_Resources.Add(item);
		}

		public virtual void Clear()
		{
			m_Resources.Clear();
		}

		public bool Contains(ResType item)
		{
			return m_Resources.Contains(item);
		}

		public void CopyTo(ResType[] array, int arrayIndex)
		{
			m_Resources.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { return m_Resources.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public virtual bool Remove(ResType item)
		{
			return m_Resources.Remove(item);
		}

		#endregion

		#region IEnumerable<IResource> Members

		public IEnumerator<ResType> GetEnumerator()
		{
			return m_Resources.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return (m_Resources as System.Collections.IEnumerable).GetEnumerator();
		}

		#endregion
	}
}
