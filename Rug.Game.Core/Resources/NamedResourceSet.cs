﻿using System.Collections.Generic;

namespace Rug.Game.Core.Resources
{
	public class NamedResourceSet<T> : ResourceSet<T> where T : IResource
	{
		private Dictionary<string, T> m_Lookup = new Dictionary<string, T>(); 

		public T this[string name]
		{
			get { return m_Lookup[name]; }
		}

		public NamedResourceSet(string name, ResourceMode mode) : base(name, mode)
		{

		}
		public override void Add(T item)
		{
			m_Lookup.Add(item.Name, item);

			base.Add(item);
		}

		public override bool Remove(T item)
		{
			m_Lookup.Remove(item.Name);

			return base.Remove(item);
		}

		public override void Clear()
		{
			m_Lookup.Clear(); 

			base.Clear();
		}
	}
}
