﻿
namespace Rug.Game.Core.Resources
{
	public interface IResourceInfo
	{
		ResourceType ResourceType { get; }
		
		void OnLoad(); 
	}
}
