﻿using System;

namespace Rug.Game.Core.Resources
{
	public interface IResourceManager : IDisposable
	{
		bool Disposed { get; }

        /// <summary>
        /// The number of resources in this manager.
        /// </summary>
        int Count { get; }

        /// <summary>
        /// The total number of resources in this manager and all sub resource managers.
        /// </summary>
        int Total { get; } 

		void LoadResources();

        void LoadNext(ResourceManagerLoadProcess process);        

		void UnloadResources();
	}
}
