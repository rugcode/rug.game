﻿using Rug.Cmd;
using System.Collections.Generic;

namespace Rug.Game.Core.Resources
{
	public class ResourceManager : IResourceManager, IEnumerable<IResourceManager>
	{
		private List<IResourceManager> m_Resources = new List<IResourceManager>(); 

		public bool Disposed { get; private set; }

		public int Count { get { return m_Resources.Count; } }

        public int Total
        {
            get 
            {
                int total = Count; 

                foreach (IResourceManager manager in m_Resources)
                {
                    total += manager.Total;
                }

                return total; 
            }
        }

		public void Add(IResourceManager manager)
		{
			m_Resources.Add(manager); 
		}

		public void Remove(IResourceManager manager)
		{
			m_Resources.Remove(manager);
		}

		public void Clear()
		{
			m_Resources.Clear(); 
		}

		public void LoadResources()
		{
			foreach (IResourceManager manager in m_Resources)
			{
				manager.LoadResources();
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= Count)
            {
                //process.Increment(); 
                process.Pop(); 
            }
            else
            {
                process.Push(m_Resources[process.Increment()]);                
            }
        }

		public void UnloadResources()
		{
			foreach (IResourceManager manager in m_Resources)
			{
                RC.WriteLine("Begin: Unload - " + manager.ToString()); 				
                
                manager.UnloadResources();
                
                RC.WriteLine("End: Unload - " + manager.ToString()); 
			}
		}

		public void Dispose()
		{
			foreach (IResourceManager manager in m_Resources)
			{
				manager.Dispose();
			}
		}

		public IEnumerator<IResourceManager> GetEnumerator()
		{
			return m_Resources.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return (m_Resources as System.Collections.IEnumerable).GetEnumerator();
		}
    }
}
