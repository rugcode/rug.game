﻿using System;

namespace Rug.Game.Core.Resources
{
	public class ResourceEventArgs : EventArgs
	{
		private IResource m_Resource;

		public IResource Resource { get { return m_Resource; } }

		public ResourceEventArgs(IResource resource) 
		{
			m_Resource = resource; 
		}
	}
}
