﻿using Rug.Cmd;
using Rug.Game.Core.Resources;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game
{
	public static class Environment
	{
		public static int DrawCalls = 0;
		public static int DrawCallsAccumulator = 0;

		public static int BufferUpdates = 0;
		public static int BufferUpdatesAccumulator = 0;

		public static float FrameDelta;

		public static int RefrenceCount;
		
		public static readonly List<string> ResourceNames = new List<string>();

		public static Cmd.ConsoleBuffer ConsoleBuffer;

        public static bool LimitFrameRate = true; 

		public static bool FramesClick;
		public static float FramesPerSecond;

		public static ResourceSet Resources = new ResourceSet("Resource Master", ResourceMode.Dynamic);
		public static ResourceSet ResolutionDependentResources = new ResourceSet("Resolution Dependent Resources", ResourceMode.Dynamic);

        public static Icon Icon = null;
        public static double Time; 

		public static void SetupConsole()
		{
			if (ConsoleBuffer == null)
			{
				ConsoleBuffer = new ConsoleBuffer();

				RC.App = ConsoleBuffer;
			}
		}

		public static void ShutdownConsole()
		{
			if (ConsoleBuffer != null)
			{
				ConsoleBuffer = null;

				RC.App = RC.Sys;
			}
		}

    }
}
