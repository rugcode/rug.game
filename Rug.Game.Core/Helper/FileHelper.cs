﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Rug.Game
{
    public static class FileHelper 
    {
        public static bool Exists(string path)
        {
            return AliasExists(path) || FileExists(path); 
        }

        public static bool AliasExists(string path)
        {
            if (path.StartsWith("~/") == false)
            {
                return false; 
            }

            return PackageHelper.AliasExists(path); 
        }

        public static bool FileExists(string path)
        {
            string resolvedPath = Helper.ResolvePath(path);

            return File.Exists(resolvedPath); 
        }

        public static DateTime GetLastWriteTime(string path)
        {
            if (AliasExists(path) == true)
            {
                return PackageHelper.GetLastWriteTime(path);
            }
            else if (FileExists(path) == true)
            {
                return File.GetLastWriteTime(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find file at path '{0}'", path));
            }
        }

        public static void LoadXml(System.Xml.XmlDocument doc, string path)
        {
            if (AliasExists(path) == true)
            {
                //using (Stream stream = PackageHelper.GetStream(path))
                Stream stream = PackageHelper.GetStream(path); 
                {
                    doc.Load(stream);
                }
            }
            else if (FileExists(path) == true)
            {
                doc.Load(Helper.ResolvePath(path)); 
            }
            else
            {
                throw new Exception(string.Format("Could not find XML file at path '{0}'", path)); 
            }
        }

        public static IEnumerable<string> ReadAllLines(string path)
        {
            if (AliasExists(path) == true)
            {
                return PackageHelper.ReadAllLines(path);
            }
            else if (FileExists(path) == true)
            {
                return File.ReadAllLines(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find text file at path '{0}'", path));
            }
        }

        public static byte[] ReadAllBytes(string path)
        {
            if (AliasExists(path) == true)
            {
                return PackageHelper.ReadAllBytes(path);
            }
            else if (FileExists(path) == true)
            {
                return File.ReadAllBytes(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find binary file at path '{0}'", path));
            }
        }

        public static Bitmap LoadBitmap(string path)
        {
            if (AliasExists(path) == true)
            {
                //using (Stream stream = PackageHelper.GetStream(path))
                Stream stream = PackageHelper.GetStream(path); 
                {
                    return (Bitmap)Bitmap.FromStream(stream);
                }
            }
            else if (FileExists(path) == true)
            {
                return (Bitmap)Bitmap.FromFile(Helper.ResolvePath(path));
            }
            else
            {
                throw new Exception(string.Format("Could not find Bitmap file at path '{0}'", path));
            }          
        }

        public static void LoadDDS(string path, bool flipImages, int border,
                                        TextureMinFilter minFilter, TextureMagFilter magFilter, TextureWrapMode wrapS, TextureWrapMode wrapT,
                                        TextureEnvMode envMode,
                                        out uint texturehandle, out TextureTarget dimension)
        {
            if (AliasExists(path) == true)
            {
                DDSLoader.Load(PackageHelper.ReadAllBytes(path), path, flipImages, border, minFilter, magFilter, wrapS, wrapT, envMode, out texturehandle, out dimension);
            }
            else if (FileExists(path) == true)
            {
                DDSLoader.LoadFromDisk(path, flipImages, border, minFilter, magFilter, wrapS, wrapT, envMode, out texturehandle, out dimension);
            }
            else
            {
                throw new Exception(string.Format("Could not find DDS file at path '{0}'", path));
            }             
        }

        public static string GetDirectoryName(string path)
        {
            if (AliasExists(path) == true)
            {
                if (path.EndsWith("/") == true)
                {
                    return path; 
                }

                return path.Substring(0, path.LastIndexOf("/") + 1); 
            }
            else
            {
                return new FileInfo(Helper.ResolvePath(path)).DirectoryName; 
            }
        }
    }
}
