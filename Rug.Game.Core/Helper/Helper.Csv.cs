﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Rug.Game
{
	public static partial class Helper
	{
		public static readonly char DefaultDelimiter = ',';

		public static string[] ReadAllLines(string path)
		{
			return File.ReadAllLines(Helper.ResolvePath(path));
		}

		public static void WriteAllLines(string path, string[] lines)
		{
			Helper.EnsurePathExists(Helper.ResolvePath(path));

			File.WriteAllLines(Helper.ResolvePath(path), lines);
		}

		public static string[] SplitCsv(string line)
		{
			return SplitCsv(line, DefaultDelimiter);
		}

		public static string[] SplitCsv(string line, char seperator)
		{
			return line.Split(seperator);
		}

		public static string ToCsv(params object[] cells)
		{
			return ToCsv(DefaultDelimiter, cells);
		}

		public static string ToCsv(char seperator, params object[] cells)
		{
			StringBuilder sb = new StringBuilder();

			string seperatorString = new string(seperator, 1); 

			foreach (object obj in cells)
			{				
				if (obj is bool)
				{
					sb.Append(((bool)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is int)
				{
					sb.Append(((int)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is uint)
				{
					sb.Append(((uint)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is long)
				{
					sb.Append(((long)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is ulong)
				{
					sb.Append(((ulong)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is short)
				{
					sb.Append(((short)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is ushort)
				{
					sb.Append(((ushort)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is float)
				{
					sb.Append(((float)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is double)
				{
					sb.Append(((double)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is decimal)
				{
					sb.Append(((decimal)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is TimeSpan)
				{
					sb.Append(((TimeSpan)obj).TotalMinutes.ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else 
				{
					sb.Append(obj.ToString() + seperatorString);
				}
			}

			string final = sb.ToString();

			// strip of the final seperator string 
			final = final.Substring(0, final.Length - seperatorString.Length); 

			return final; 
		}
		
		public static ulong ToMicroseconds(ulong ntpValue)
		{
			double timeDouble = (double)ntpValue * (1000000d / (double)uint.MaxValue);

			return (ulong)timeDouble; 
		}

		public static string ToStringBlob(byte[] bytes)
		{
			// if the deafult is to be Base64 encoded
			//return "64x" + System.Convert.ToBase64String(bytes); 

			StringBuilder sb = new StringBuilder((bytes.Length * 2) + 2);

			sb.Append("0x");

			foreach (byte b in bytes)
			{
				sb.Append(b.ToString("X2"));
			}

			return sb.ToString();
		}

        public static string ValueOrDefault(string value, string @default)
        {
            if (IsNullOrEmpty(value) == true)
            {
                return @default; 
            }

            return value; 
        }
    }
}
