﻿using OpenTK;
using System;

namespace Rug.Game
{
    public static partial class Helper
    {
        /**
         * @brief Converts q to Euler angles.
         *
         * @code
         * Quaternion quaternionConjugate = QuaternionConjugate(fusionAhrs.q);
         * EulerAngles euler;
         * QuaternionToEulerAngles(&quaternionConjugate, &euler);
         * printf("%f,%f,%f", euler.angle.pitch, euler.angle.roll, euler.angle.yaw);
         * @endcode
         *
         * @param q OscAddress of q.
         * @param eulerAngles OscAddress of Euler angles to be written to.
         */
        public static Vector3 ToEulerAngles(Quaternion q)
        {
            float qwqw = q.W * q.W; // calculate common terms to avoid repetition

            Vector3 angles = Vector3.Zero;

            angles.Z = (float)Math.Atan2(2.0f * (q.Y * q.Z - q.W * q.X), 2.0f * (qwqw - 0.5f + q.Z * q.Z));
            angles.X =-(float)Math.Asin(2.0f * (q.X * q.Z + q.W * q.Y));
            angles.Y = (float)Math.Atan2(2.0f * (q.X * q.Y - q.W * q.Z), 2.0f * (qwqw - 0.5f + q.X * q.X));

            //float phi = (float)Math.Atan2(2 * (q[2] * q[3] - q[0] * q[1]), 2 * q[0] * q[0] - 1 + 2 * q[3] * q[3]);
            //float theta = (float)-Math.Atan((2.0 * (q[1] * q[3] + q[0] * q[2])) / Math.Sqrt(1.0 - Math.Pow((2.0 * q[1] * q[3] + 2.0 * q[0] * q[2]), 2.0)));
            //float psi = (float)Math.Atan2(2 * (q[1] * q[2] - q[0] * q[3]), 2 * q[0] * q[0] - 1 + 2 * q[1] * q[1]);
            //return new float[] { RadToDeg(phi), RadToDeg(theta), RadToDeg(psi) };


            return angles;
        }
    }
}
