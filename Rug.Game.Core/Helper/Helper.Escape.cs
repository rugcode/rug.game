﻿using System;

namespace Rug.Game
{
	public static partial class Helper
	{
		/// <summary>
		/// Turn a byte array into a readable, escaped string
		/// </summary>
		/// <param name="bytes">bytes</param>
		/// <returns>a string</returns>
		public static string Escape(byte[] bytes)
		{
			// the result is maximum of bytes length * 2 
			char[] chars = new char[bytes.Length * 2];

			int j = 0;

			for (int i = 0; i < bytes.Length; i++)
			{
				char c = (char)bytes[i];

				if (c > '~')
				{
					chars[j++] = '�';
				}
				else if (c >= ' ')
				{
					chars[j++] = c;
				}
				else
				{
					switch (c)
					{
						case '\0':
							chars[j++] = '\\';
							chars[j++] = '0';
							break;

						case '\a':
							chars[j++] = '\\';
							chars[j++] = 'a';
							break;

						case '\b':
							chars[j++] = '\\';
							chars[j++] = 'b';
							break;

						case '\f':
							chars[j++] = '\\';
							chars[j++] = 'f';
							break;

						case '\n':
							chars[j++] = '\\';
							chars[j++] = 'n';
							break;

						case '\r':
							chars[j++] = '\\';
							chars[j++] = 'r';
							break;

						case '\t':
							chars[j++] = '\\';
							chars[j++] = 't';
							break;

						case '\v':
							chars[j++] = '\\';
							chars[j++] = 'v';
							break;

						case '\\':
							chars[j++] = '\\';
							chars[j++] = '\\';
							break;

						default:
							chars[j++] = '�';
							break;
					}
				}
			}

			return new string(chars, 0, j);
		}

		public static bool IsValidEscape(string str)
		{
			bool isEscaped = false;

			// first we count the number of chars we will be returning
			for (int i = 0; i < str.Length; i++)
			{
				char c = str[i];

				// if we are not in  an escape sequence and the char is a escape char 
				if (isEscaped == false && c == '\\')
				{
					// escape 
					isEscaped = true;
				}
				// else if we are escaped 
				else if (isEscaped == true)
				{
					// reset escape state 
					isEscaped = false;

					// check the char against the set of known escape chars 
					switch (char.ToLower(c))
					{
						case '0':
						case 'a':
						case 'b':
						case 'f':
						case 'n':
						case 'r':
						case 't':
						case 'v':
						case '\\':
							// do not increment count
							break;
						default:
							// this is not a valid escape sequence 
							// return false 
							return false; 
					}
				}
				else
				{
					// normal char increment count
				}
			}

			return isEscaped == false;
		}

		/// <summary>
		/// Turn a readable string into a byte array 
		/// </summary>
		/// <param name="str">a string, optionally with escape sequences in it</param>
		/// <returns>a byte array</returns>
		public static byte[] Unescape(string str)
		{
			int count = 0;
			bool isEscaped = false;

			// first we count the number of chars we will be returning
			for (int i = 0; i < str.Length; i++)
			{
				char c = str[i];

				// if we are not in  an escape sequence and the char is a escape char 
				if (isEscaped == false && c == '\\')
				{
					// escape 
					isEscaped = true;

					// increment count 
					count++;
				}
				// else if we are escaped 
				else if (isEscaped == true)
				{
					// reset escape state 
					isEscaped = false;

					// check the char against the set of known escape chars 
					switch (char.ToLower(c))
					{
						case '0':
						case 'a':
						case 'b':
						case 'f':
						case 'n':
						case 'r':
						case 't':
						case 'v':
						case '\\':
							// do not increment count
							break;
						default:
							// this is not a valid escape sequence 
							throw new Exception("Invalid escape sequence at char '" + (i - 1) + "'."); 
					}
				}
				else
				{
					// normal char increment count
					count++;
				}
			}

			if (isEscaped == true)
			{
				throw new Exception("Invalid escape sequence at char '" + (str.Length - 1) + "'."); 
			}

			// reset the escape state
			isEscaped = false;

			// create a byte array for the result
			byte[] chars = new byte[count];

			int j = 0;

			// actually populate the array 
			for (int i = 0; i < str.Length; i++)
			{
				char c = (char)str[i];

				// if we are not in  an escape sequence and the char is a escape char 
				if (isEscaped == false && c == '\\')
				{
					// escape 
					isEscaped = true;
				}
				// else if we are escaped 
				else if (isEscaped == true)
				{
					// reset escape state
					isEscaped = false;

					// check the char against the set of known escape chars 
					switch (char.ToLower(str[i]))
					{
						case '0':
							chars[j++] = (byte)'\0';
							break;

						case 'a':
							chars[j++] = (byte)'\a';
							break;

						case 'b':
							chars[j++] = (byte)'\b';
							break;

						case 'f':
							chars[j++] = (byte)'\f';
							break;

						case 'n':
							chars[j++] = (byte)'\n';
							break;

						case 'r':
							chars[j++] = (byte)'\r';
							break;

						case 't':
							chars[j++] = (byte)'\t';
							break;

						case 'v':
							chars[j++] = (byte)'\v';
							break;

						case '\\':
							chars[j++] = (byte)'\\';
							break;

						default:
							// this is not a valid escape sequence 
							break;
					}
				}
				else
				{
					// normal char
					chars[j++] = (byte)c;
				}
			}

			return chars;
		}
	}
}