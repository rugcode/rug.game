﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Globalization;
using System.Xml;

namespace Rug.Game
{
    public static partial class Helper
    {
        public static void AppendAttributeAndValue(XmlElement element, string name, Vector2 value) { AppendAttributeAndValue(element, name, Helper.SerializeVector2(value)); }
        public static void AppendAttributeAndValue(XmlElement element, string name, Vector3 value) { AppendAttributeAndValue(element, name, Helper.SerializeVector3(value)); }
        public static void AppendAttributeAndValue(XmlElement element, string name, Vector4 value) { AppendAttributeAndValue(element, name, Helper.SerializeVector4(value)); }
        public static void AppendAttributeAndValue(XmlElement element, string name, Color4 value) { AppendAttributeAndValue(element, name, Helper.SerializeColor4(value)); }
        public static void AppendAttributeAndValue(XmlElement element, string name, Quaternion value) { AppendAttributeAndValue(element, name, Helper.SerializeQuaternion(value)); }

        public static Vector2 GetAttributeValue(XmlNode node, string name, Vector2 @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeVector2(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Vector3 GetAttributeValue(XmlNode node, string name, Vector3 @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeVector3(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Vector4 GetAttributeValue(XmlNode node, string name, Vector4 @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeVector4(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Color4 GetAttributeValue(XmlNode node, string name, Color4 @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColor4(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static Quaternion GetAttributeValue(XmlNode node, string name, Quaternion @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeQuaternion(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }



        public static string SerializeColor4(OpenTK.Graphics.Color4 color)
        {
            return string.Format("{0},{1},{2},{3}", color.A.ToString(CultureInfo.InvariantCulture), color.R.ToString(CultureInfo.InvariantCulture), color.G.ToString(CultureInfo.InvariantCulture), color.B.ToString(CultureInfo.InvariantCulture));
        }

        public static OpenTK.Graphics.Color4 DeserializeColor4(string value)
        {
            string[] parts = value.Split(',');

            float a, r, g, b;

            a = float.Parse(parts[0], CultureInfo.InvariantCulture);
            r = float.Parse(parts[1], CultureInfo.InvariantCulture);
            g = float.Parse(parts[2], CultureInfo.InvariantCulture);
            b = float.Parse(parts[3], CultureInfo.InvariantCulture);

            return new OpenTK.Graphics.Color4(r, g, b, a);
        }



        #region Vectors

        public static string SerializeVector2(Vector2 value)
        {
            return String.Format("{0},{1}",
                value.X.ToString(CultureInfo.InvariantCulture),
                value.Y.ToString(CultureInfo.InvariantCulture));
        }

        public static Vector2 DeserializeVector2(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);

            return new Vector2(x, y);
        }

        public static string SerializeVector3(Vector3 value)
        {
            return String.Format("{0},{1},{2}",
                value.X.ToString(CultureInfo.InvariantCulture),
                value.Y.ToString(CultureInfo.InvariantCulture),
                value.Z.ToString(CultureInfo.InvariantCulture));
        }

        public static Vector3 DeserializeVector3(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, z;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            z = float.Parse(pieces[2], CultureInfo.InvariantCulture);

            return new Vector3(x, y, z);
        }

        public static string SerializeVector4(Vector4 value)
        {
            return String.Format("{0},{1},{2},{3}",
                value.X.ToString(CultureInfo.InvariantCulture),
                value.Y.ToString(CultureInfo.InvariantCulture),
                value.Z.ToString(CultureInfo.InvariantCulture),
                value.W.ToString(CultureInfo.InvariantCulture));
        }

        public static Vector4 DeserializeVector4(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, z, w;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            z = float.Parse(pieces[2], CultureInfo.InvariantCulture);
            w = float.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new Vector4(x, y, z, w);
        }

        public static string SerializeQuaternion(Quaternion value)
        {
            return String.Format("{0},{1},{2},{3}",
                value.X.ToString(CultureInfo.InvariantCulture),
                value.Y.ToString(CultureInfo.InvariantCulture),
                value.Z.ToString(CultureInfo.InvariantCulture),
                value.W.ToString(CultureInfo.InvariantCulture));
        }

        public static Quaternion DeserializeQuaternion(string str)
        {
            string[] pieces = str.Split(new char[] { ',' });
            float x, y, z, w;

            x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
            y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
            z = float.Parse(pieces[2], CultureInfo.InvariantCulture);
            w = float.Parse(pieces[3], CultureInfo.InvariantCulture);

            return new Quaternion(x, y, z, w);
        }

        #endregion

    }
}
