﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Xml;

namespace Rug.Game
{
	public static partial class Helper
	{
		public static void AppendAttributeAndValue(XmlElement element, string name, Rectangle value) { AppendAttributeAndValue(element, name, SerializeRectangle(value)); }
		public static void AppendAttributeAndValue(XmlElement element, string name, RectangleF value) { AppendAttributeAndValue(element, name, SerializeRectangleF(value)); }

		public static void AppendAttributeAndValue(XmlElement element, string name, PointF value) { AppendAttributeAndValue(element, name, SerializePointF(value)); }
		public static void AppendAttributeAndValue(XmlElement element, string name, Point value) { AppendAttributeAndValue(element, name, SerializePoint(value)); }

		public static void AppendAttributeAndValue(XmlElement element, string name, Size value) { AppendAttributeAndValue(element, name, Helper.SerializeSize(value)); }
		public static void AppendAttributeAndValue(XmlElement element, string name, SizeF value) { AppendAttributeAndValue(element, name, Helper.SerializeSizeF(value)); }

        public static void AppendAttributeAndValue(XmlElement element, string name, System.Drawing.Color value) { AppendAttributeAndValue(element, name, SerializeColor(value)); }


		public static Rectangle GetAttributeValue(XmlNode node, string name, Rectangle @default)
		{
			if (node.Attributes[name] == null)
			{
				return @default;
			}

			try
			{
				return DeserializeRectangle(node.Attributes[name].Value);
			}
			catch
			{
				return @default;
			}
		}

		public static RectangleF GetAttributeValue(XmlNode node, string name, RectangleF @default)
		{
			if (node.Attributes[name] == null)
			{
				return @default;
			}

			try
			{
				return DeserializeRectangleF(node.Attributes[name].Value);
			}
			catch
			{
				return @default;
			}
		}


		public static Size GetAttributeValue(XmlNode node, string name, Size @default)
		{
			if (node.Attributes[name] == null)
			{
				return @default;
			}

			try
			{
				return DeserializeSize(node.Attributes[name].Value);
			}
			catch
			{
				return @default;
			}
		}

		public static SizeF GetAttributeValue(XmlNode node, string name, SizeF @default)
		{
			if (node.Attributes[name] == null)
			{
				return @default;
			}

			try
			{
				return DeserializeSizeF(node.Attributes[name].Value);
			}
			catch
			{
				return @default;
			}
		}

		public static Point GetAttributeValue(XmlNode node, string name, Point @default)
		{
			if (node.Attributes[name] == null)
			{
				return @default;
			}

			try
			{
				return DeserializePoint(node.Attributes[name].Value);
			}
			catch
			{
				return @default;
			}
		}

		public static PointF GetAttributeValue(XmlNode node, string name, PointF @default)
		{
			if (node.Attributes[name] == null)
			{
				return @default;
			}

			try
			{
				return DeserializePointF(node.Attributes[name].Value);
			}
			catch
			{
				return @default;
			}
		}


        public static System.Drawing.Color GetAttributeValue(XmlNode node, string name, System.Drawing.Color @default)
        {
            if (node.Attributes[name] == null)
            {
                return @default;
            }

            try
            {
                return DeserializeColor(node.Attributes[name].Value);
            }
            catch
            {
                return @default;
            }
        }

        public static string SerializeColor(System.Drawing.Color color)
        {
            return Color.ToARGBHtmlString(color);
        }

        public static System.Drawing.Color DeserializeColor(string color)
        {
            return Color.FromHtmlString(color, false);
        }

        #region Color

        public static class Color
        {
            private readonly static List<string> Colors = new List<string>(Enum.GetNames(typeof(System.Drawing.KnownColor)));

            public static string ToHtmlString(System.Drawing.Color color)
            {
                return "#" + color.R.ToString("X2", CultureInfo.InvariantCulture) + color.G.ToString("X2", CultureInfo.InvariantCulture) + color.B.ToString("X2", CultureInfo.InvariantCulture);
            }

            public static string ToARGBHtmlString(System.Drawing.Color color)
            {
                return "#" + color.A.ToString("X2", CultureInfo.InvariantCulture) + color.R.ToString("X2", CultureInfo.InvariantCulture) + color.G.ToString("X2", CultureInfo.InvariantCulture) + color.B.ToString("X2", CultureInfo.InvariantCulture);
            }

            public static System.Drawing.Color FromHtmlString(string color)
            {
                return FromHtmlString(color, true);
            }

            public static System.Drawing.Color FromHtmlString(string color, bool throwException)
            {
                string str = color;

                if (IsNullOrEmpty(str))
                    throw new ArgumentNullException("color");

                str = str.Trim();

                if (str.StartsWith("#"))
                    str = str.Substring(1);

                if (str.Length == 8)
                {
                    byte a, r, g, b;

                    if ((byte.TryParse(str.Substring(0, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out a)) &&
                        (byte.TryParse(str.Substring(2, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r)) &&
                        (byte.TryParse(str.Substring(4, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g)) &&
                        (byte.TryParse(str.Substring(6, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b)))
                        return System.Drawing.Color.FromArgb(a, r, g, b);
                }

                if (str.Length == 6)
                {
                    byte r, g, b;

                    if ((byte.TryParse(str.Substring(0, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r)) &&
                        (byte.TryParse(str.Substring(2, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g)) &&
                        (byte.TryParse(str.Substring(4, 2), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b)))
                        return System.Drawing.Color.FromArgb(r, g, b);
                }

                if (str.Length == 3)
                {
                    byte r, g, b;

                    if ((byte.TryParse(str.Substring(0, 1), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out r)) &&
                        (byte.TryParse(str.Substring(1, 1), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out g)) &&
                        (byte.TryParse(str.Substring(2, 1), System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out b)))
                        return System.Drawing.Color.FromArgb(r * 16, g * 16, b * 16);
                }

                if (Colors.Contains(str))
                {
                    return System.Drawing.Color.FromName(str);
                }

                if (throwException)
                {
                    throw new Exception("Color is of unknown format '" + color + "'");
                }
                else
                {
                    return System.Drawing.Color.Transparent;
                }
            }
        }

        #endregion

		#region Rectangle Helpers

		public static string SerializeRectangle(Rectangle rect)
		{
			return String.Format("{0},{1},{2},{3}",
				rect.X.ToString(CultureInfo.InvariantCulture),
				rect.Y.ToString(CultureInfo.InvariantCulture),
				rect.Width.ToString(CultureInfo.InvariantCulture),
				rect.Height.ToString(CultureInfo.InvariantCulture));
		}

		public static Rectangle DeserializeRectangle(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			int x, y, width, height;

			x = int.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = int.Parse(pieces[1], CultureInfo.InvariantCulture);
			width = int.Parse(pieces[2], CultureInfo.InvariantCulture);
			height = int.Parse(pieces[3], CultureInfo.InvariantCulture);

			return new Rectangle(x, y, width, height);
		}

		public static string SerializeRectangleF(System.Drawing.RectangleF rect)
		{
			return String.Format("{0},{1},{2},{3}",
				rect.X.ToString(CultureInfo.InvariantCulture),
				rect.Y.ToString(CultureInfo.InvariantCulture),
				rect.Width.ToString(CultureInfo.InvariantCulture),
				rect.Height.ToString(CultureInfo.InvariantCulture));
		}

		public static System.Drawing.RectangleF DeserializeRectangleF(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			float x, y, width, height;

			x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = float.Parse(pieces[1], CultureInfo.InvariantCulture);
			width = float.Parse(pieces[2], CultureInfo.InvariantCulture);
			height = float.Parse(pieces[3], CultureInfo.InvariantCulture);

			return new System.Drawing.RectangleF(x, y, width, height);
		}

		#endregion

		#region Point Helpers

		public static string SerializePoint(Point point)
		{
			return String.Format("{0},{1}",
				point.X.ToString(CultureInfo.InvariantCulture),
				point.Y.ToString(CultureInfo.InvariantCulture));
		}

		public static Point DeserializePoint(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			int x, y;

			x = int.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = int.Parse(pieces[1], CultureInfo.InvariantCulture);

			return new Point(x, y);
		}

		public static string SerializePointF(PointF point)
		{
			return String.Format("{0},{1}",
				point.X.ToString(CultureInfo.InvariantCulture),
				point.Y.ToString(CultureInfo.InvariantCulture));
		}

		public static PointF DeserializePointF(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			float x, y;

			x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = float.Parse(pieces[1], CultureInfo.InvariantCulture);

			return new PointF(x, y);
		}

		#endregion

		#region Size Helpers

		public static string SerializeSize(Size size)
		{
			return String.Format("{0},{1}",
				size.Width.ToString(CultureInfo.InvariantCulture),
				size.Height.ToString(CultureInfo.InvariantCulture));
		}

		public static Size DeserializeSize(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			int x, y;

			x = int.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = int.Parse(pieces[1], CultureInfo.InvariantCulture);

			return new Size(x, y);
		}

		public static string SerializeSizeF(SizeF size)
		{
			return String.Format("{0},{1}",
				size.Width.ToString(CultureInfo.InvariantCulture),
				size.Height.ToString(CultureInfo.InvariantCulture));
		}

		public static SizeF DeserializeSizeF(string str)
		{
			string[] pieces = str.Split(new char[] { ',' });
			float x, y;

			x = float.Parse(pieces[0], CultureInfo.InvariantCulture);
			y = float.Parse(pieces[1], CultureInfo.InvariantCulture);

			return new SizeF(x, y);
		}

		#endregion

        public static void WriteBitmap32(string file, byte[] source, int width, int height)
        {
            using (Bitmap bitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
            {
                BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                byte[] pixels = new byte[bitmap.Width * 4 * bitmap.Height];

                int rowSize = bitmap.Width * 4;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int baseIndex = y * (width * 4) + (x * 4);

                        pixels[baseIndex + 0] = source[baseIndex + 2];
                        pixels[baseIndex + 1] = source[baseIndex + 1];
                        pixels[baseIndex + 2] = source[baseIndex + 0];
                        pixels[baseIndex + 3] = 255;
                    }
                }

                Marshal.Copy(pixels, 0, data.Scan0, pixels.Length);

                bitmap.UnlockBits(data);

                bitmap.Save(file);
            }
        }
	}
}
