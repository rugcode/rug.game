﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Core.Textures
{
	public class Texture2D : IResource
	{
		private string m_Name;
		private ResourceMode m_Mode = ResourceMode.Static;

		private Texture2DInfo m_ResourceInfo; 

		#region Public Properties

		public string Name { get { return m_Name; } }

		public ResourceType ResourceType { get { return Resources.ResourceType.Texture; } }

		public ResourceMode ResourceMode { get { return m_Mode; } }

		public Texture2DInfo ResourceInfo { get { return m_ResourceInfo; } }

		IResourceInfo IResource.ResourceInfo { get { return m_ResourceInfo; } }

        public uint ResourceHandle { get; protected set; }

        public bool IsLoaded { get; protected set; }

		public TextureTarget TextureTarget { get { return m_ResourceInfo.TextureTarget; } }		

		#endregion

		public Texture2D(string name, ResourceMode mode, Texture2DInfo resourceInfo)
		{
			m_Name = name; 
			m_Mode = mode;
			m_ResourceInfo = resourceInfo; 
		}

		public override string ToString()
		{
			return String.Format("{0}, {1}, {2}, ({3})", m_Name, m_Mode, ResourceType, ResourceInfo.ToString());
		}

		public virtual void LoadResources()
		{
			if (ResourceHandle != 0 || IsLoaded == true)
			{
				throw new Exception("Attempt to load " + ResourceType.ToString() + " resource '" + Name + "', the resource is already loaded");
			}

			Environment.RefrenceCount++;
			Environment.ResourceNames.Add(this.Name);

            uint resourceHandle; 
			GL.GenTextures(1, out resourceHandle);
            ResourceHandle = resourceHandle; 

			if (ResourceHandle == 0)
			{
				RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
			}

			GLHelper.CheckTexture();
			GL.BindTexture(TextureTarget, ResourceHandle);
			
            GLHelper.CheckTexture();

			// DO checking 

			IsLoaded = true; 
		}

		public void CreateBlank()
		{
			GL.BindTexture(TextureTarget, ResourceHandle);

			if (ResourceHandle == 0)
			{
				RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
			}

			m_ResourceInfo.OnLoad();

			GL.BindTexture(TextureTarget, 0);
		}

		public void UploadImage(IntPtr intPtr)
		{
			GL.BindTexture(TextureTarget, ResourceHandle);

			if (ResourceHandle == 0)
			{
				RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
			}

			m_ResourceInfo.OnLoad(intPtr);

			GL.BindTexture(TextureTarget, 0);
		}

		public void DownloadImage(IntPtr intPtr)
		{
			GL.BindTexture(TextureTarget, ResourceHandle);

			m_ResourceInfo.OnDownload(intPtr);

			GL.BindTexture(TextureTarget, 0);
		}

		public virtual void UnloadResources()
		{
			if (ResourceHandle == 0 || IsLoaded == false)
			{				
				return; 
			}

			Environment.RefrenceCount--;
			Environment.ResourceNames.Remove(this.Name);

			GL.DeleteTexture(ResourceHandle);
			ResourceHandle = 0;
			IsLoaded = false; 
		}

		public void Bind()
		{
			GL.BindTexture(TextureTarget, ResourceHandle);
		}

		public void Unbind()
		{
			GL.BindTexture(TextureTarget, 0);
		}
	}
}
