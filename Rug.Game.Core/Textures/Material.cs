﻿using System.Drawing;
using System.Xml;

namespace Rug.Game.Core.Textures
{
	public class Material
	{
		public string Name { get; set; }

		public Color DebugColor { get; set; }

		public float Ambient { get; set; }

		public float Specular { get; set; }

		public float SpecularFunction { get; set; }

		public float Gloss { get; set; }

		public Color Color
		{
			get { return Color.FromArgb((int)(Gloss * 255f), (int)(Specular * 255f), (int)SpecularFunction, (int)(Ambient * 255f)); }
		}

		public bool HasBeenStored { get; set; }

		public Material()
		{
			Name = ""; 
		}

		public void Save(XmlElement node)
		{
			Helper.AppendAttributeAndValue(node, "Name", Name);
			Helper.AppendAttributeAndValue(node, "DebugColor", DebugColor);
			Helper.AppendAttributeAndValue(node, "Ambient", Ambient);
			Helper.AppendAttributeAndValue(node, "Specular", Specular);
			Helper.AppendAttributeAndValue(node, "SpecularFunction", SpecularFunction);
			Helper.AppendAttributeAndValue(node, "Gloss", Gloss);
		}

		public static Material Load(XmlNode node)
		{
			Material material = new Material();

			material.HasBeenStored = true;

			material.Name = Helper.GetAttributeValue(node, "Name", "");
			material.DebugColor = Helper.GetAttributeValue(node, "DebugColor", material.DebugColor);
			material.Ambient = Helper.GetAttributeValue(node, "Ambient", material.Ambient);
			material.Specular = Helper.GetAttributeValue(node, "Specular", material.Specular);
			material.SpecularFunction = Helper.GetAttributeValue(node, "SpecularFunction", material.SpecularFunction);
			material.Gloss = Helper.GetAttributeValue(node, "Gloss", material.Gloss);

			return material;
		}
	}
}
