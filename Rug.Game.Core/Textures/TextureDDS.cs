﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Core.Textures
{
	public class TextureDDS : IResource
	{
		private string m_Name;
		private ResourceMode m_Mode = ResourceMode.Static;
		private bool m_IsLoaded = false;
		private uint m_Handle = 0;
		private TextureTarget m_Target; 

		private TextureDDSInfo m_ResourceInfo; 

		#region Public Properties

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.Texture; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_Mode; }
		}

		public TextureDDSInfo ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		IResourceInfo IResource.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_Handle; }
		}

		public TextureTarget TextureTarget
		{
			get { return m_Target; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}		

		#endregion

		public TextureDDS(string name, ResourceMode mode, TextureDDSInfo resourceInfo)
		{
			m_Name = name; 
			m_Mode = mode;
			m_ResourceInfo = resourceInfo; 
		}

		public override string ToString()
		{
			return String.Format("{0}, {1}, {2}, ({3})", m_Name, m_Mode, ResourceType, ResourceInfo.ToString());
		}

		public virtual void LoadResources()
		{
			if (m_Handle != 0 || m_IsLoaded == true)
			{
				throw new Exception("Attempt to load " + ResourceType.ToString() + " resource '" + Name + "', the resource is already loaded");
			}

			m_ResourceInfo.OnLoad(out m_Handle, out m_Target); 

			m_IsLoaded = true; 
		}

		public void UnloadResources()
		{
			if (m_Handle == 0 || m_IsLoaded == false)
			{				
				return; 
			}
			
			GL.DeleteTexture(m_Handle);
			m_Handle = 0;
			m_IsLoaded = false; 
		}

		public void Bind()
		{
			GL.BindTexture(m_Target, m_Handle);
		}

		internal void Unbind()
		{
			GL.BindTexture(m_Target, 0);
		}
	}
}
