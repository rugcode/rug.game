﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Core.Textures
{
    public class TextureCube : Texture2D
	{
        private TextureCubeInfo m_ResourceInfo;

        public new TextureCubeInfo ResourceInfo { get { return m_ResourceInfo; } }

        public TextureCube(string name, ResourceMode mode, TextureCubeInfo resourceInfo)
            : base(name, mode, resourceInfo)
        {
            m_ResourceInfo = resourceInfo; 
        }

        public override void LoadResources()
        {
            if (ResourceHandle != 0 || IsLoaded == true)
            {
                throw new Exception("Attempt to load " + ResourceType.ToString() + " resource '" + Name + "', the resource is already loaded");
            }

            Environment.RefrenceCount++;
            Environment.ResourceNames.Add(this.Name);

            uint resourceHandle;
            GL.GenTextures(1, out resourceHandle);
            ResourceHandle = resourceHandle;

            if (ResourceHandle == 0)
            {
                RC.WriteError(001, "Texture '" + this.Name + "' is not valid");
            }

            GLHelper.CheckTexture();
            GL.BindTexture(TextureTarget, ResourceHandle);

            GLHelper.CheckTexture();

            m_ResourceInfo.OnLoad();
            // DO checking 

            GL.BindTexture(TextureTarget, 0);

            IsLoaded = true;
        }

        public void GenerateMipMap()
        {
            if (ResourceInfo.MipMap == true)
            {
                Bind();
                ResourceInfo.GenerateMipMap();
                Unbind(); 
            }
        }
    }
}
