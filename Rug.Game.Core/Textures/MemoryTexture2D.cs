﻿using OpenTK;
using Rug.Game.Core.Resources;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Textures
{
	public class MemoryTexture2D : Texture2D
	{
		private const float ByteToFloat = 1f / 255f;

		private string m_ImagePath;
		private int m_PixelStride;
		private byte[] m_Data; 

		public string ImagePath { get { return m_ImagePath; } }
		public byte[] ImageData { get; set; } 

		public int Width { get { return this.ResourceInfo.Size.Width; } }
		public int Height { get { return this.ResourceInfo.Size.Height; } }
		public int PixelStride { get { return m_PixelStride; } }
		public byte[] Data { get { return m_Data; } }

		public bool IsDirty { get; set; }

		public bool IsNormalMap { get; set; }
		
		public float NormalScale { get; set; } 

		public MemoryTexture2D(string name, string path, bool isNormalMap, ResourceMode mode, Texture2DInfo resourceInfo)
			: this(name, path, mode, resourceInfo)
		{
			IsNormalMap = isNormalMap; 
		}
		
		public MemoryTexture2D(string name, byte[] imageData, bool isNormalMap, ResourceMode mode, Texture2DInfo resourceInfo)
			: this(name, imageData, mode, resourceInfo)
		{
			IsNormalMap = isNormalMap; 
		}


		public MemoryTexture2D(string name, string path, ResourceMode mode, Texture2DInfo resourceInfo)
			: base(name, mode, resourceInfo)
		{
			NormalScale = 0.075f;

			m_ImagePath = path;

			Initalise();
		}

		public MemoryTexture2D(string name, byte[] imageData, ResourceMode mode, Texture2DInfo resourceInfo)
			: base(name, mode, resourceInfo)
		{
			NormalScale = 0.075f;

			ImageData = imageData;

			Initalise();
		}

		public void Initalise()
		{
			if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgra)
			{
				m_PixelStride = 4;
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Rgb)
			{
				m_PixelStride = 3;
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgr)
			{
				m_PixelStride = 3;
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Red)
			{
				m_PixelStride = 1;
			}
			else
			{
				m_PixelStride = 4;
			}

			if (String.IsNullOrEmpty(m_ImagePath) == false)
			{
                using (Bitmap bitmap = FileHelper.LoadBitmap(m_ImagePath))
				{
					LoadBitmap(bitmap); 
				}
			}
			else if (ImageData != null) 
 			{
				using (MemoryStream stream = new MemoryStream(ImageData))
				using (Bitmap bitmap = new Bitmap(stream))
				{
					LoadBitmap(bitmap);
				}

			}
			else if (m_Data == null)
			{
				m_Data = new byte[this.ResourceInfo.Size.Width * m_PixelStride * this.ResourceInfo.Size.Height];
			}
		}

		public void LoadBitmap(Bitmap bitmap)
		{
			this.ResourceInfo.Size = new Size(bitmap.Width, bitmap.Height);

			BitmapData data;

			if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgra)
			{
				data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgr)
			{
				data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Rgb)
			{
				data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Red)
			{
				data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
			}
			else
			{
				data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
			}

			m_Data = new byte[bitmap.Width * m_PixelStride * bitmap.Height];

			Marshal.Copy(data.Scan0, m_Data, 0, m_Data.Length);

			bitmap.UnlockBits(data);
		}

		public Bitmap ToBitmap()
		{
			System.Drawing.Imaging.PixelFormat pixelFormat; 

			if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgra)
			{
				pixelFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgr)
			{
				pixelFormat = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			}
			else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Red)
			{
				pixelFormat = System.Drawing.Imaging.PixelFormat.Format8bppIndexed;			
			}
			else
			{
				pixelFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;				
			}

			Bitmap bitmap = new Bitmap(this.ResourceInfo.Size.Width, this.ResourceInfo.Size.Height, pixelFormat);

			BitmapData data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, pixelFormat);

			Marshal.Copy(m_Data, 0, data.Scan0, m_Data.Length);

			bitmap.UnlockBits(data);

			return bitmap; 
		}

		public override void LoadResources()
		{
			base.LoadResources();

			Update(); 
		}

		public void BuildNormalMap()
		{

		}

		float Sample(int x, int y)
		{
			x = x < 0 ? 0 : x >= Width ? Width - 1 : x;
			y = y < 0 ? 0 : y >= Height ? Height - 1 : y;

			return ((float)m_Data[(y * (Width * m_PixelStride)) + (x * m_PixelStride) + (m_PixelStride - 1)] * ByteToFloat) * 2.0f - 1.0f;
		}

		Vector3 FetchNormalVector(int x, int y)
		{
			// Determine the offsets
			//Vector2 o1 = new Vector2(1f, 0.0f);
			//Vector2 o2 = new Vector2(0.0f, 1f);

			// Take three samples to determine two vectors that can be
			// use to generate the normal at this pixel
			float h0 = Sample(x, y);
			float h1 = Sample(x + 1, y);
			float h2 = Sample(x, y + 1);

			Vector3 v01 = new Vector3(1f, 0f, h0 - h1);
			Vector3 v02 = new Vector3(0f, 1f, h0 - h2);

			Vector3 n = Vector3.Cross(v01, v02); 

			// Can be useful to scale the Z component to tweak the
			// amount bumps show up, less than 1.0 will make them
			// more apparent, greater than 1.0 will smooth them out
			//n.Z *= 0.5f;
			//n.Y *= -1f;

			n.Z *= NormalScale; 

			n.Normalize();

			return n;
		}

		public void Update()
		{
			if (IsDirty == true && IsNormalMap == true)
			{
				for (int y = 0; y < Height; y++)
				{
					for (int x = 0; x < Width; x++)
					{
						Vector3 normal = FetchNormalVector(x, y);

						int index = (y * (Width * m_PixelStride)) + (x * m_PixelStride);

						m_Data[index++] = (byte)((normal.Z * 0.5f + 0.5f) * 255f);
						m_Data[index++] = (byte)((normal.Y * 0.5f + 0.5f) * 255f);
						m_Data[index++] = (byte)((normal.X * 0.5f + 0.5f) * 255f); 
					}
				}
			}

			IsDirty = false;

			GCHandle handle = GCHandle.Alloc(m_Data, GCHandleType.Pinned);

			try
			{
				IntPtr pointer = handle.AddrOfPinnedObject();

				UploadImage(pointer);
			}
			finally
			{
				if (handle.IsAllocated)
				{
					handle.Free();
				}
			}
		}
	}
}
