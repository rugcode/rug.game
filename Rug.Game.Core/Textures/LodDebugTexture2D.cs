﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Textures
{
	public class LodDebugTexture2D : Texture2D
	{
		public LodDebugTexture2D(string name, int maxSize)
			: base(name, ResourceMode.Static, new Texture2DInfo()
			{
				MagFilter = TextureMagFilter.Linear, 
				 MinFilter = TextureMinFilter.NearestMipmapLinear, 
				 PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Red, 
				 PixelType = PixelType.Float, 
				 InternalFormat = PixelInternalFormat.R16f,
				 WrapS = TextureWrapMode.Repeat, 
				 WrapT = TextureWrapMode.Repeat, 
				 Size = new Size(maxSize, maxSize), 
			})
		{

		}

		public override void LoadResources()
		{
			base.LoadResources();

			float totalSize = (float)ResourceInfo.Size.Width; 

			int size = ResourceInfo.Size.Width;
			int level = 0; 

			do
			{
				AssignMipMap(size, level++, (float)size / totalSize);
				size /= 2;
			}
			while (size > 1);

			AssignMipMap(size, level, (float)size / totalSize);
		}

        private void AssignMipMap(int size, int level, float value)
        {
			float[] array = new float[size * size];

			for (int i = 0; i < array.Length; i++)
			{
				array[i] = value;
			}

			GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);

			try
			{
				IntPtr pointer = handle.AddrOfPinnedObject();

				if (level == 0)
				{
					UploadImage(pointer);
				}
				else
				{
					GL.BindTexture(TextureTarget.Texture2D, ResourceHandle);

					GL.TexImage2D(TextureTarget.Texture2D, level, ResourceInfo.InternalFormat, size, size, 0, ResourceInfo.PixelFormat, ResourceInfo.PixelType, pointer);

					GL.BindTexture(TextureTarget.Texture2D, 0);
				}
			}
			finally
			{
				if (handle.IsAllocated)
				{
					handle.Free();
				}
			}
        }
	}
}
