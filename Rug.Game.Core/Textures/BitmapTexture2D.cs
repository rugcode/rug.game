﻿using Rug.Game.Core.Resources;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Rug.Game.Core.Textures
{
	public class BitmapTexture2D : Texture2D
	{
        public string ImagePath { get; set; } 

        public Bitmap Image { get; set; } 

		public byte[] ImageData { get; set; }

		public BitmapTexture2D(string name, string path, ResourceMode mode, Texture2DInfo resourceInfo)
			: base(name, mode, resourceInfo)
		{
			ImagePath = path; 
		}

        public BitmapTexture2D(string name, Bitmap image, ResourceMode mode, Texture2DInfo resourceInfo)
            : base(name, mode, resourceInfo)
        {
            Image = image;
        }

		public BitmapTexture2D(string name, byte[] imageData, ResourceMode mode, Texture2DInfo resourceInfo)
			: base(name, mode, resourceInfo)
		{
			ImageData = imageData;
		}

		public override void LoadResources()
		{
            if (Image != null)
            {
                LoadBitmap(Image);
            }
			else if (String.IsNullOrEmpty(ImagePath) == false)
			{
				using (Bitmap bitmap = FileHelper.LoadBitmap(ImagePath))
				{
					LoadBitmap(bitmap);
				}
			}
			else
			{
				using (MemoryStream stream = new MemoryStream(ImageData))
				using (Bitmap bitmap = new Bitmap(stream))
				{
					LoadBitmap(bitmap);
				}
			}
		}

		private void LoadBitmap(Bitmap bitmap)
		{
			this.ResourceInfo.Size = new Size(bitmap.Width, bitmap.Height);

			base.LoadResources();

            UploadImage(bitmap);
		}

        public void UploadImage()
        {
            if (Image != null)
            {
                UploadImage(Image);
            }
            else if (String.IsNullOrEmpty(ImagePath) == false)
            {
                using (Bitmap bitmap = FileHelper.LoadBitmap(ImagePath))
                {
                    UploadImage(bitmap);
                }
            }
            else
            {
                using (MemoryStream stream = new MemoryStream(ImageData))
                using (Bitmap bitmap = new Bitmap(stream))
                {
                    UploadImage(bitmap);
                }
            }
        }

        private void UploadImage(Bitmap bitmap)
        {
            BitmapData data;

            if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgra)
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }
            else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Bgr)
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            }
            else if (this.ResourceInfo.PixelFormat == OpenTK.Graphics.OpenGL.PixelFormat.Red)
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            }
            else
            {
                data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            }

            UploadImage(data.Scan0);

            bitmap.UnlockBits(data);
        }
	}
}
