﻿using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Textures
{
	public class MaterialTexture2D : Texture2D
	{
		private string m_ImagePath;

		public string ImagePath 
		{
			get { return m_ImagePath; } 
		}

		public List<Material> Materials { get; set; }

		public bool DebugColors { get; set; } 

		public MaterialTexture2D(string name, List<Material> materials, ResourceMode mode, Texture2DInfo resourceInfo)
			: base(name, mode, resourceInfo)
		{
			Materials = materials; 
		}

		public override void LoadResources()
		{
			LoadMaterials(); 
		}

		private void LoadMaterials()
		{
			this.ResourceInfo.Size = new Size(Materials.Count, 1);

			base.LoadResources();

			byte[] entries = new byte[Materials.Count * 4];

			for (int i = 0; i < Materials.Count; i++)
			{
				Color color = DebugColors ? Materials[i].DebugColor : Materials[i].Color;

				entries[i * 4 + 0] = color.B;
				entries[i * 4 + 1] = color.G;
				entries[i * 4 + 2] = color.R;
				entries[i * 4 + 3] = color.A;
			}

			GCHandle handle = GCHandle.Alloc(entries, GCHandleType.Pinned);

			try
			{
				IntPtr pointer = handle.AddrOfPinnedObject();

				UploadImage(pointer);
			}
			finally
			{
				if (handle.IsAllocated)
				{
					handle.Free();
				}
			}
		}
	}
}
