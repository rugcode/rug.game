﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Core.Textures
{
	public class TextureDDSInfo : IResourceInfo
	{
		#region Private Members

		private int m_Border;

		private TextureMinFilter m_MinFilter;
		private TextureMagFilter m_MagFilter;

		private TextureWrapMode m_WrapS;
		private TextureWrapMode m_WrapT;

		private TextureEnvMode m_EnvironmentMode;
		private string m_FilePath; 

		#endregion

		#region Public Properties

		public int Border
		{
			get { return m_Border; }
			set { m_Border = value; }
		}

		public TextureMinFilter MinFilter
		{
			get { return m_MinFilter; }
			set { m_MinFilter = value; }
		}

		public TextureMagFilter MagFilter
		{
			get { return m_MagFilter; }
			set { m_MagFilter = value; }
		}

		public TextureWrapMode WrapS
		{
			get { return m_WrapS; }
			set { m_WrapS = value; }
		}

		public TextureWrapMode WrapT
		{
			get { return m_WrapT; }
			set { m_WrapT = value; }
		}

		public TextureEnvMode EnvironmentMode
		{
			get { return m_EnvironmentMode; }
			set { m_EnvironmentMode = value; } 
		}

		public string FilePath
		{
			get { return m_FilePath; }
			set { m_FilePath = value; } 
		} 

		#endregion

		#region IResourceInfo Members

		public ResourceType ResourceType
		{
			get { return ResourceType.Texture; }
		}

		public void OnLoad()
		{

		}

		public void OnLoad(out uint texturehandle, out TextureTarget dimension)
		{			
            FileHelper.LoadDDS(m_FilePath, false, Border, MinFilter, MagFilter, WrapS, WrapT, EnvironmentMode, out texturehandle, out dimension); 			
		}

		#endregion

		public override string ToString()
		{			
			return String.Format("{0}", m_FilePath);
		}
	}
}
