﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Core.Textures
{
    public class TextureCubeInfo : Texture2DInfo
    {
        public TextureWrapMode WrapR { get; set; }

        public TextureEnvMode EnvironmentMode { get; set; }

        public override TextureTarget TextureTarget { get { return TextureTarget.TextureCubeMap; } }

		public override void OnLoad()
		{
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)WrapS);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)WrapT);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)WrapR);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)MinFilter);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)MagFilter);

            for (int i = 0; i < 6; i++)
            {
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, InternalFormat, Size.Width, Size.Height, Border, PixelFormat, PixelType, IntPtr.Zero);
            }

            if (MipMap == true)
            {
                GL.GenerateMipmap(GenerateMipmapTarget.TextureCubeMap);
                GLHelper.CheckTexture();
            }
		}

		public void GenerateMipMap()
		{
            if (MipMap == true)
            {                
                GL.GenerateMipmap(GenerateMipmapTarget.TextureCubeMap);
                GLHelper.CheckTexture();
            }
		}

        /* 
		public void OnLoad(out uint texturehandle, out TextureTarget dimension)
		{			
            // FileHelper.LoadDDS(m_FilePath, false, Border, MinFilter, MagFilter, WrapS, WrapT, EnvironmentMode, out texturehandle, out dimension); 			

            int ColorTexture;

            GL.GenTextures(1, out texturehandle);

            GL.BindTexture(TextureTarget.TextureCubeMap, ColorTexture);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)WrapS);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)WrapT);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)WrapR);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)MinFilter);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)MagFilter);

            for (int i = 0; i < 6; i++)
            {
                GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, InternalFormat, Size.Width, Size.Height, Border, PixelFormat, PixelType, IntPtr.Zero);
            }

            GL.BindTexture(TextureTarget.TextureCubeMap, 0);


            int DepthTexture;

            GL.GenTextures(1, out DepthTexture);
            GL.BindTexture(TextureTarget.TextureCubeMap, DepthTexture);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)WrapS);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)WrapT);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapR, (int)WrapR);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)MinFilter);
            GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)MagFilter);

            GL.TexImage2D(TextureTarget.TextureCubeMap, 0, PixelInternalFormat.DepthComponent24, Size.Width, Size.Height, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
            GL.BindTexture(TextureTarget.TextureCubeMap, 0);

            int FBO;
            GL.GenFramebuffers(1, out FBO);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FBO);
            for (int i = 0; i < 6; i++)
            {
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, TextureTarget.TextureCubeMapPositiveX + i, ColorTexture, 0);
            }

            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, DepthTexture, 0);

            GL.DrawBuffer(DrawBufferMode.ColorAttachment0);
            fboMsg = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer).ToString();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}
        */ 

		public override string ToString()
		{
            return String.Format("{0}x{1}, {2}, {3}", Size.Width, Size.Height, PixelFormat, PixelType);
		}
    }
}
