﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Resources;
using System;
using System.Drawing;

namespace Rug.Game.Core.Textures
{
    public enum AnisotropicFiltering
    {
        Off = 0,
        X1 = 1,
        X2 = 2,
        X4 = 4,
        X8 = 8,
        X16 = 16,
        Maximum = -1
    }

	public class Texture2DInfo : IResourceInfo
	{
        public static Texture2DInfo Bitmap32_NearestClampToEdge(Size size)
        {
            return new Texture2DInfo()
            {
                InternalFormat = PixelInternalFormat.Rgba,
                MagFilter = TextureMagFilter.Nearest,
                MinFilter = TextureMinFilter.Nearest,
                PixelFormat = PixelFormat.Bgra,
                PixelType = PixelType.UnsignedByte,
                Border = 0,
                WrapS = TextureWrapMode.ClampToEdge,
                WrapT = TextureWrapMode.ClampToEdge,
                Size = size
            };
        }

		public static Texture2DInfo Bitmap32_LinearRepeat(Size size)
		{
			return new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.Rgba,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.Linear,
				PixelFormat = PixelFormat.Bgra,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.Repeat,
				WrapT = TextureWrapMode.Repeat,
				Size = size
			};
		}

        public static Texture2DInfo Bitmap32_LinearRepeatMip(Size size)
        {
            return new Texture2DInfo()
            {
                InternalFormat = PixelInternalFormat.Rgba,
                MagFilter = TextureMagFilter.Linear,
                MinFilter = TextureMinFilter.LinearMipmapLinear,
                PixelFormat = PixelFormat.Bgra,
                PixelType = PixelType.UnsignedByte,
                Border = 0,
                WrapS = TextureWrapMode.Repeat,
                WrapT = TextureWrapMode.Repeat,
                Size = size,
                MipMap = true,
            };
        }

		public static Texture2DInfo Bitmap32_LinearClampToEdgeMip(Size size)
		{
			return new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.Rgba,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.LinearMipmapLinear,
				PixelFormat = PixelFormat.Bgra,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.ClampToEdge,
				WrapT = TextureWrapMode.ClampToEdge,
				Size = size, 
				MipMap = true,
			};
		}

		public static Texture2DInfo Bitmap32_LinearMirroredRepeatMip(Size size)
		{
			return new Texture2DInfo()
			{
				InternalFormat = PixelInternalFormat.Rgba,
				MagFilter = TextureMagFilter.Linear,
				MinFilter = TextureMinFilter.LinearMipmapLinear,
				PixelFormat = PixelFormat.Bgra,
				PixelType = PixelType.UnsignedByte,
				Border = 0,
				WrapS = TextureWrapMode.MirroredRepeat,
				WrapT = TextureWrapMode.MirroredRepeat,
				Size = size,
				MipMap = true,
			};
		}

		#region Public Properties

        public Size Size { get; set; }

        public int Border { get; set; }

        public PixelFormat PixelFormat { get; set; }

        public PixelType PixelType { get; set; }

        public PixelInternalFormat InternalFormat { get; set; }

        public TextureMinFilter MinFilter { get; set; }

        public TextureMagFilter MagFilter { get; set; }

        public TextureWrapMode WrapS { get; set; }

        public TextureWrapMode WrapT { get; set; } 

		public bool MipMap { get; set; }

        public AnisotropicFiltering AnisotropicFiltering { get; set; } 

		public int Samples { get; set; }

		public virtual TextureTarget TextureTarget
		{
			get
			{
				if (Samples <= 1)
				{
					return TextureTarget.Texture2D;
				}
				else
				{
					return TextureTarget.Texture2DMultisample;                    
				}
			}
		}

		#endregion 

		#region IResourceInfo Members

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.Texture; }
		}

		public virtual void OnLoad()
		{
			if (Samples <= 1)
			{
				GL.TexImage2D(TextureTarget.Texture2D, 0, InternalFormat, Size.Width, Size.Height, Border, PixelFormat, PixelType, IntPtr.Zero);
				GLHelper.CheckTexture();
				GL.TexParameter(TextureTarget, TextureParameterName.TextureMinFilter, (int)MinFilter);
				GLHelper.CheckTexture();
				GL.TexParameter(TextureTarget, TextureParameterName.TextureMagFilter, (int)MagFilter);
				GLHelper.CheckTexture();
				GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapS, (int)WrapS);
				GLHelper.CheckTexture();
				GL.TexParameter(TextureTarget, TextureParameterName.TextureWrapT, (int)WrapT);

                if ((int)AnisotropicFiltering != 0)
                {
                    if (GLState.ContainsExtention("GL_EXT_texture_filter_anisotropic") == true)
                    {
                        int maxAniso = GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt);

                        int aniso = (int)AnisotropicFiltering == -1 ? maxAniso : Math.Min((int)AnisotropicFiltering, maxAniso);

                        GL.TexParameter(TextureTarget, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);

                        GLHelper.CheckTexture();
                    }
                }
			}
			else
			{
				GL.TexImage2DMultisample(TextureTargetMultisample.Texture2DMultisample, Samples, InternalFormat, Size.Width, Size.Height, false);
			}

			GLHelper.CheckTexture();

			if (MipMap == true)
			{
				if (Samples > 1)
				{
					throw new Exception("Cannot generate mipmap on multisample texture");
				}

				GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
				GLHelper.CheckTexture();
			}
		}

		public void OnLoad(IntPtr imagePtr)
		{
			if (Samples > 1)
			{
				throw new Exception("Cannot upload to multisample texture"); 
			}

			GL.TexImage2D(TextureTarget.Texture2D, 0, InternalFormat, Size.Width, Size.Height, Border, PixelFormat, PixelType, imagePtr);
			GLHelper.CheckTexture();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)MinFilter);
			GLHelper.CheckTexture();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)MagFilter);
			GLHelper.CheckTexture();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)WrapS);
			GLHelper.CheckTexture();
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)WrapT);
			GLHelper.CheckTexture();

            if ((int)AnisotropicFiltering != 0)
            {
                if (GLState.ContainsExtention("GL_EXT_texture_filter_anisotropic") == true)
                {
                    int maxAniso = GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt);

                    int aniso = (int)AnisotropicFiltering == -1 ? maxAniso : Math.Min((int)AnisotropicFiltering, maxAniso);

                    GL.TexParameter(TextureTarget, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);

                    GLHelper.CheckTexture();
                }
            }

			if (MipMap == true)
			{
				GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
				GLHelper.CheckTexture();
			}

            Rug.Game.Environment.BufferUpdatesAccumulator++;
		}

		public void GenerateMipMap()
		{
			if (Samples > 1)
			{
				throw new Exception("Cannot generate mipmap on multisample texture");
			}

			if (MipMap == true)
			{
				GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
				GLHelper.CheckTexture();
			}
		}

		public void OnDownload(IntPtr imagePtr)
		{
			if (Samples > 1)
			{
				throw new Exception("Cannot download multisample textures");
			}

			GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat, PixelType == OpenTK.Graphics.OpenGL.PixelType.HalfFloat ? OpenTK.Graphics.OpenGL.PixelType.Float : PixelType, imagePtr);
		}

		#endregion

		public override string ToString()
		{
			return String.Format("{0}x{1}, {2}, {3}", Size.Width, Size.Height, PixelFormat, PixelType);
		}
	}
}
