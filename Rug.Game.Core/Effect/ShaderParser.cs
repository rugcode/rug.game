﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Rug.Game.Core.Effect
{
	public static class ShaderParser
	{
		public static readonly Regex ForeachRegex =
			new Regex(@"\s*\x24(?<IndexKey>\w+)\s+in\s+(?<LoopName>\w+)", 					  
				RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.CultureInvariant);

        public static readonly Regex LocationIndexRegex =
            new Regex(@"\x24Location\x28\s*(?<Increment>\d+)\s*\x29",
                RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.CultureInvariant);

		public static string Parse(string path, IEnumerable<string> defines, Dictionary<string, int> loops)
		{
			List<string> defineList = new List<string>(defines); 

			StringBuilder sb = new StringBuilder();

			List<string> lines = new List<string>(FileHelper.ReadAllLines(path)); 

			int lineIndex = 0;
            int locationIndex = 0; // $Location(4)

			while (lineIndex < lines.Count)
			{
				string trimmed = lines[lineIndex].Trim();

				if (trimmed.StartsWith("#include ", StringComparison.InvariantCultureIgnoreCase) == true)
				{
					string includePath = trimmed.Substring("#include ".Length).Trim();

					lines.RemoveAt(lineIndex);

					lines.InsertRange(lineIndex, FileHelper.ReadAllLines(includePath));
				}
				else
				{
					lineIndex++; 
				}
			}

			Stack<string> stack = new Stack<string>();

			bool allow = true;

			List<string> finalLines = new List<string>();

            foreach (string line in lines)
			{
				string trimmed = line.Trim();				

				if (trimmed.StartsWith("#section ", StringComparison.InvariantCultureIgnoreCase) == true)
				{
					stack.Push(trimmed.Substring("#section ".Length).Trim());

					allow = ScanStack(stack, defineList);

					//sb.AppendLine(); 
					//finalLines.Add(string.Empty);
				}
				else if (trimmed.StartsWith("#end", StringComparison.InvariantCultureIgnoreCase) == true)
				{
					stack.Pop();

					allow = ScanStack(stack, defineList);

					//finalLines.Add(string.Empty);
				}
				else if (allow == true)
				{
					finalLines.Add(line);
				}
				else
				{
					//finalLines.Add(string.Empty);
				}
			}

			for (int i = 0; i < finalLines.Count; )
			{
				string line = finalLines[i++]; 
				string trimmed = line.Trim();

                int indent = 0; 

				if (trimmed.StartsWith("#foreach ", StringComparison.InvariantCultureIgnoreCase) == true)
				{
					List<string> forLines = new List<string>();

					string forCommand = trimmed;

					for (; i < finalLines.Count; )
					{
						line = finalLines[i++]; 

						trimmed = line.Trim();

                        if (trimmed.StartsWith("#foreach ", StringComparison.InvariantCultureIgnoreCase) == true)
                        {
                            indent++;
                            forLines.Add(line); 
                        }
						else if (trimmed.StartsWith("#forend", StringComparison.InvariantCultureIgnoreCase) == true)
						{
                            if (indent == 0)
                            {
                                break;
                            }

                            indent--;
                            forLines.Add(line); 
						}
						else
						{
							forLines.Add(line); 
						}
					}

					ExecuteLoop(sb, forCommand, forLines, loops, ref locationIndex); 
				}
				else
				{
                    while (LocationIndexRegex.IsMatch(line) == true)
                    {
                        Match match = LocationIndexRegex.Match(line);
                        int increment = int.Parse(match.Groups["Increment"].Value);

                        line = line.Replace(line.Substring(match.Index, match.Length), locationIndex.ToString());

                        locationIndex += increment;
                    }

					sb.AppendLine(line);
				}
			}

			string finalString = sb.ToString();

			foreach (string key in loops.Keys)
			{
                finalString = finalString.Replace("$" + key, loops[key].ToString()); 
			}

			return finalString; 
		}

		private static void ExecuteLoop(StringBuilder sb, string forCommand, List<string> lines, Dictionary<string, int> loops, ref int locationIndex)
		{
			string varParser = forCommand.Substring("#foreach ".Length);

			if (ForeachRegex.IsMatch(forCommand) == false)
			{
				throw new Exception("Invalid loop string '" + forCommand + "'"); 
			}

			Match match = ForeachRegex.Match(forCommand);

			if (match.Groups["IndexKey"].Success == false)
			{
				throw new Exception("Invalid loop string '" + forCommand + "' missing index key"); 
			}

			if (match.Groups["LoopName"].Success == false)
			{
				throw new Exception("Invalid loop string '" + forCommand + "' missing loop name");
			}

			string indexKey = "$" + match.Groups["IndexKey"].Value;
            string ifIndexKey = "$ifelse_" + match.Groups["IndexKey"].Value;
			string loopName = match.Groups["LoopName"].Value;

			if (loops.ContainsKey(loopName) == false)
			{
				throw new Exception("Unknown loop name '" + loopName + "'"); 
			}

			int end = loops[loopName];

			for (int i = 0; i < end; i++)
			{
				for (int j = 0; j < lines.Count; )
			    {
				    string line = lines[j++];

                    string formattedLine = line;
                    string trimmed = formattedLine.Trim();

                    int indent = 0; 

                    if (trimmed.StartsWith("#foreach ", StringComparison.InvariantCultureIgnoreCase) == true)
                    {
                        List<string> forLines = new List<string>();

                        string subForCommand = trimmed;

                        for (; j < lines.Count; )
                        {
                            line = lines[j++];

                            trimmed = line.Trim();

                            if (trimmed.StartsWith("#foreach ", StringComparison.InvariantCultureIgnoreCase) == true)
                            {
                                indent++;
                                forLines.Add(line);
                            }
                            else if (trimmed.StartsWith("#forend", StringComparison.InvariantCultureIgnoreCase) == true)
                            {
                                if (indent == 0)
                                {
                                    break;
                                }

                                indent--;
                                forLines.Add(line); 
                            }
                            else
                            {
                                forLines.Add(line);
                            }
                        }

                        StringBuilder subSb = new StringBuilder(); 

                        ExecuteLoop(subSb, subForCommand, forLines, loops, ref locationIndex);

                        string result = subSb.ToString();

                        while (LocationIndexRegex.IsMatch(result) == true)
                        {
                            Match locationIndexMatch = LocationIndexRegex.Match(result);
                            int increment = int.Parse(locationIndexMatch.Groups["Increment"].Value);

                            result = result.Replace(line.Substring(locationIndexMatch.Index, locationIndexMatch.Length), locationIndex.ToString());

                            locationIndex += increment;
                        }

                        sb.Append(result.Replace(ifIndexKey, i == 0 ? "if" : "else if").Replace(indexKey, i.ToString()));
                    }
                    else
                    {
                        while (LocationIndexRegex.IsMatch(formattedLine) == true)
                        {
                            Match locationIndexMatch = LocationIndexRegex.Match(formattedLine);
                            int increment = int.Parse(locationIndexMatch.Groups["Increment"].Value);

                            formattedLine = formattedLine.Replace(line.Substring(locationIndexMatch.Index, locationIndexMatch.Length), locationIndex.ToString());

                            locationIndex += increment;
                        }

                        sb.AppendLine(formattedLine.Replace(ifIndexKey, i == 0 ? "if" : "else if").Replace(indexKey, i.ToString()));
                    }

					//sb.AppendLine(line.Replace(indexKey, i.ToString()));
				}
			}
		}

		private static bool ScanStack(Stack<string> stack, List<string> defines)
		{
			foreach (string str in stack)
			{
				if (str.StartsWith("!") == true)
				{
					if (defines.Contains(str.Substring(1)) == true)
					{
						return false;
					}
				}				
				else 
				{
					if (defines.Contains(str) == false)
					{
						return false;
					}
				}
			}

			return true; 
		}
	}
}
