﻿using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;

namespace Rug.Game.Core.Effect
{
	public static class SharedEffects
	{
		public static readonly Dictionary<string, IResource> Effects = new Dictionary<string, IResource>();

		private static bool m_Disposed = true;

		static SharedEffects()
		{
			
		}

		public static void LoadResources()
		{
			if (m_Disposed == true)
			{
				foreach (IResource manager in Effects.Values)
				{
					manager.LoadResources();
				}

				m_Disposed = false;
			}
		}

		public static void UnloadResources()
		{
			if (m_Disposed == false)
			{
				foreach (IResource manager in Effects.Values)
				{
					manager.UnloadResources();
				}
				m_Disposed = true;
			}
		}

		public static bool Disposed { get { return m_Disposed; } }

		public static void Dispose()
		{
			foreach (IResource manager in Effects.Values)
			{
				manager.UnloadResources();

				if (manager is IDisposable)
				{
					(manager as IDisposable).Dispose(); 
				}
			}

			Effects.Clear();

			m_Disposed = true;
		}

		public static void ReloadIfNeeded()
		{
			RC.WriteLine(ConsoleVerbosity.Verbose, ConsoleThemeColor.TextGood, "Reload If Needed"); 

			foreach (IResource manager in Effects.Values)
			{
                if (manager is IEffect && (manager as IEffect).HasChanged == true)
				{
					RC.WriteLine(ConsoleVerbosity.Verbose, ConsoleThemeColor.SubTextGood, manager.Name); 

					manager.UnloadResources();
					
					manager.LoadResources();
				}

				if (manager is ComputeEffectBase && (manager as ComputeEffectBase).HasChanged == true)
				{
					RC.WriteLine(ConsoleVerbosity.Verbose, ConsoleThemeColor.SubTextGood, manager.Name); 

					manager.UnloadResources();

					manager.LoadResources();
				}
			}			
		}
	}
}
