﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Rug.Game.Core.Effect
{
    public abstract class GeometryEffect : IEffect
    {
        private int m_ProgramHandle;
		private ProgramState m_State = ProgramState.NotLoaded;

		public ProgramState State { get { return m_State; } }

        public abstract string ShaderLocation_Vert { get; }
        public abstract string ShaderLocation_Frag { get; }
        public abstract string ShaderLocation_Geom { get; }

		public DateTime VertexTimeStamp { get; set; }
		public DateTime FragmentTimeStamp { get; set; }
        public DateTime GeometryTimeStamp { get; set; }

        public string ShaderSource_Vert { get; private set; }
        public string ShaderSource_Frag { get; private set; }
        public string ShaderSource_Geom { get; private set; }

		public bool HasChanged
		{
			get
			{
                DateTime vertexTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation_Vert + ".vert.glsl");
                DateTime fragmentTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation_Frag + ".frag.glsl");
                DateTime geometryTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation_Geom + ".geom.glsl");

				if (VertexTimeStamp != vertexTimeStamp)
				{
					return true; 
				}

				if (FragmentTimeStamp != fragmentTimeStamp)
				{
					return true;
				}

                if (GeometryTimeStamp != geometryTimeStamp)
                {
                    return true;
                }

				return false; 
			}
		}

		public string[] Defines { get; set; }

		public readonly Dictionary<string, int> Loops = new Dictionary<string, int>();

		public int ProgramHandle
		{
			get
			{
				return m_ProgramHandle;
			}
		}

        public GeometryEffect()
		{
			Defines = new string[0]; 
		}

		public void Load()
		{
			if (m_State != ProgramState.NotLoaded)
			{
				throw new Exception("Program '" + Name + "' is not is a state that will allow loading (" + m_State + ")"); 
			}

			m_ProgramHandle = GL.CreateProgram();

            string[] defines = new string[Defines.Length + 1];

            Defines.CopyTo(defines, 0);
            defines[Defines.Length] = "Pass_Vertex";

            ShaderSource_Vert = ShaderParser.Parse(ShaderLocation_Vert + ".vert.glsl", defines, Loops);

            VertexTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation_Vert + ".vert.glsl"); 

			int vertexShader = GL.CreateShader(ShaderType.VertexShader);
			GL.ShaderSource(vertexShader, ShaderSource_Vert);
			GL.CompileShader(vertexShader);

			string log = GL.GetShaderInfoLog(vertexShader);

			if (String.IsNullOrEmpty(log) == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor1, "Error Loading Vertex Shader: " + Name);
                RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, ShaderLocation_Vert + ".vert.glsl");
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, log);

				m_State = ProgramState.Error; 
			}




            Defines.CopyTo(defines, 0);
            defines[Defines.Length] = "Pass_Fragment";

            ShaderSource_Frag = ShaderParser.Parse(ShaderLocation_Frag + ".frag.glsl", defines, Loops);

            FragmentTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation_Frag + ".frag.glsl"); 

			int fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
			GL.ShaderSource(fragmentShader, ShaderSource_Frag);
			GL.CompileShader(fragmentShader);

            log = GL.GetShaderInfoLog(fragmentShader);

			if (String.IsNullOrEmpty(log) == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor1, "Error Loading Fragment Shader: " + Name);
                RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, ShaderLocation_Frag + ".frag.glsl");
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, log);

				m_State = ProgramState.Error;
            }




            Defines.CopyTo(defines, 0);
            defines[Defines.Length] = "Pass_Geometry";

            ShaderSource_Geom = ShaderParser.Parse(ShaderLocation_Geom + ".geom.glsl", defines, Loops);

            GeometryTimeStamp = FileHelper.GetLastWriteTime(ShaderLocation_Geom + ".geom.glsl");

            int geometryShader = GL.CreateShader(ShaderType.GeometryShader);
            GL.ShaderSource(geometryShader, ShaderSource_Geom);
            GL.CompileShader(geometryShader);

            log = GL.GetShaderInfoLog(geometryShader);

            if (String.IsNullOrEmpty(log) == false)
            {
                RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor1, "Error Loading Geometry Shader: " + Name);
                RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, ShaderLocation_Geom + ".geom.glsl");
                RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, log);

                m_State = ProgramState.Error;
            }


            WriteResolvedShaders();

            if (m_State == ProgramState.Error)
            {
                return; 
            }

			GL.AttachShader(m_ProgramHandle, vertexShader);
			GL.AttachShader(m_ProgramHandle, fragmentShader);
            GL.AttachShader(m_ProgramHandle, geometryShader);

			GL.DeleteShader(vertexShader);
			GL.DeleteShader(fragmentShader);
            GL.DeleteShader(geometryShader);

			m_State = ProgramState.Loaded; 
		}

        [Conditional("DEBUG")] 
        private void WriteResolvedShaders()
        {
            Helper.EnsurePathExists(Helper.ResolvePath("~/ShaderOutput/"));

            System.IO.File.WriteAllText(Helper.ResolvePath("~/ShaderOutput/" + Name.Replace(":", "_") + ".vert.glsl"), ShaderSource_Vert);
            System.IO.File.WriteAllText(Helper.ResolvePath("~/ShaderOutput/" + Name.Replace(":", "_") + ".frag.glsl"), ShaderSource_Frag);
            System.IO.File.WriteAllText(Helper.ResolvePath("~/ShaderOutput/" + Name.Replace(":", "_") + ".geom.glsl"), ShaderSource_Geom);
        }


		public void Link()
		{
			if (m_State != ProgramState.Loaded)
			{
				throw new Exception("Program '" + Name + "' is not is a state that will allow linking (" + m_State + ")");
			}

			GL.LinkProgram(m_ProgramHandle);

			string log = GL.GetProgramInfoLog(m_ProgramHandle);

			if (String.IsNullOrEmpty(log) == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor1, "Errors Linking Shader: " + Name);
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, log);

				m_State = ProgramState.Error;
				return;
			}
			else
			{
				m_State = ProgramState.Linked;
			}

            /* 
            // Set the input type of the primitives we are going to feed the geometry shader, this should be the same as
            // the primitive type given to GL.Begin. If the types do not match a GL error will occur (todo: verify GL_INVALID_ENUM, on glBegin)
            GL.Ext.ProgramParameter(m_ProgramHandle, ExtGeometryShader4.GeometryInputTypeExt, (int)All.Lines);

            // Set the output type of the geometry shader. Becasue we input Lines we will output LineStrip(s).
            GL.Ext.ProgramParameter(m_ProgramHandle, ExtGeometryShader4.GeometryOutputTypeExt, (int)All.LineStrip);

            // We must tell the shader program how much vertices the geometry shader will output (at most).
            // The simple way is to query the maximum and use that.
            int tmp;
            // Get the maximum amount of vertices into tmp.
            GL.GetInteger((GetPName)ExtGeometryShader4.MaxGeometryOutputVerticesExt, out tmp);
            // And feed amount that to the shader program. (0x0400 on a HD3850, with catalyst 9.8)
            GL.Ext.ProgramParameter(m_ProgramHandle, ExtGeometryShader4.GeometryVerticesOutExt, tmp);
            */ 
		}

		#region IResourceManager Members

		public virtual void LoadResources()
		{
			Load();

			OnLoadResources();

			Link(); 
		}

		protected abstract void OnLoadResources();

		public virtual void UnloadResources()
		{
			OnUnloadResources();

			if (m_ProgramHandle != 0)
			{
				GL.DeleteProgram(m_ProgramHandle);

				m_ProgramHandle = 0;

				m_State = ProgramState.NotLoaded;
			}
		}

		protected abstract void OnUnloadResources(); 

		#endregion

		#region IResource Members

		public abstract string Name { get; }

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.Program; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle { get { return 0; } }

		public bool IsLoaded
		{
			get { return State == ProgramState.Linked; }
		}

		#endregion
    }
}
