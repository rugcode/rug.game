﻿
using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Effect
{
    public interface IEffect : IResource
    {
        //DateTime VertexTimeStamp { get; set; }

        bool HasChanged { get; }

        bool IsLoaded { get; }

        ProgramState State { get; }

        int ProgramHandle { get; } 
    }
}
