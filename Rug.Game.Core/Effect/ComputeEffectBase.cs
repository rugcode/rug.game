﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.IO;

namespace Rug.Game.Core.Effect
{
    public abstract class ComputeEffectBase : IEffect
	{
		private int m_ProgramHandle;
		private ProgramState m_State = ProgramState.NotLoaded;

		public ProgramState State { get { return m_State; } } 

		public abstract string ShaderLocation { get; }

		public DateTime TimeStamp { get; set; }

		public bool HasChanged
		{
			get
			{
				string basePath = Helper.ResolvePath(ShaderLocation);

				DateTime timeStamp = File.GetLastWriteTime(basePath + ".cs.glsl");

				if (TimeStamp != timeStamp)
				{
					return true;
				}

				return false; 
			}
		}

		public string[] Defines { get; set; }

		public readonly Dictionary<string, int> Loops = new Dictionary<string, int>(); 

		public int ProgramHandle
		{
			get
			{
				return m_ProgramHandle;
			}
		}

		public ComputeEffectBase()
		{
			Defines = new string[0]; 
		}

		public void Load()
		{
			if (m_State != ProgramState.NotLoaded)
			{
				throw new Exception("Program '" + Name + "' is not is a state that will allow loading (" + m_State + ")"); 
			}

			string basePath = Helper.ResolvePath(ShaderLocation);
			
			m_ProgramHandle = GL.CreateProgram();

			string computeShaderSource = ShaderParser.Parse(basePath + ".cs.glsl", Defines, Loops);

			TimeStamp = File.GetLastWriteTime(basePath + ".cs.glsl");

			int computeShader = GL.CreateShader(ShaderType.ComputeShader);
			GL.ShaderSource(computeShader, computeShaderSource);
			GL.CompileShader(computeShader);

			string log = GL.GetShaderInfoLog(computeShader);

			if (String.IsNullOrEmpty(log) == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor1, "Error Loading Compute Shader: " + Name);
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, ShaderLocation + ".cs.glsl");
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, log);

				m_State = ProgramState.Error; 
				return; 
			}

			GL.AttachShader(m_ProgramHandle, computeShader);

			GL.DeleteShader(computeShader);			

			m_State = ProgramState.Loaded; 
		}

		public void Link()
		{
			if (m_State != ProgramState.Loaded)
			{
				throw new Exception("Program '" + Name + "' is not is a state that will allow linking (" + m_State + ")");
			}

			GL.LinkProgram(m_ProgramHandle);

			string log = GL.GetProgramInfoLog(m_ProgramHandle);

			if (String.IsNullOrEmpty(log) == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor1, "Errors Linking Shader: " + Name);
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.ErrorColor2, log);

				m_State = ProgramState.Error;
				return;
			}
			else
			{
				m_State = ProgramState.Linked;
			}
		}

		public void Bind()
		{
			GL.UseProgram(m_ProgramHandle);			
		}

		public void Unbind()
		{
			GL.UseProgram(0); 
		}

		public void Dispatch(uint threadsX, uint threadsY, uint threadsZ)
		{
			GL.DispatchCompute(threadsX, threadsY, threadsZ); 
		}

		#region IResourceManager Members

		public virtual void LoadResources()
		{
			Load();

			OnLoadResources();

			Link(); 
		}

		protected abstract void OnLoadResources();

		public virtual void UnloadResources()
		{
			OnUnloadResources();

			if (m_ProgramHandle != 0)
			{
				GL.DeleteProgram(m_ProgramHandle);

				m_ProgramHandle = 0;

				m_State = ProgramState.NotLoaded;
			}
		}

		protected abstract void OnUnloadResources(); 

		#endregion

		#region IResource Members

		public abstract string Name { get; }

		public ResourceType ResourceType
		{
			get { return Resources.ResourceType.Program; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle { get { return 0; } }

		public bool IsLoaded
		{
			get { return State == ProgramState.Linked; }
		}

		#endregion
	}
}
