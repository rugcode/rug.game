﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Rug.Game
{
	public class GLSettings
	{
		#region Viewport

		public Viewport Viewport;

		#endregion

		#region Clearing

		public OpenTK.Graphics.Color4 ClearColor;

		public float ClearDepth;

		#endregion

		#region Blend

		public bool EnableBlend = false;

		public BlendEquationMode BlendEquationMode = BlendEquationMode.FuncAdd;
		public BlendingFactorSrc BlendingFactorSrc = BlendingFactorSrc.SrcAlpha;
		public BlendingFactorDest BlendingFactorDest = BlendingFactorDest.OneMinusSrcAlpha;

		public bool EnableAlphaTest = false;

		public AlphaFunction AlphaFunction = AlphaFunction.Always;

		public float AlphaFunctionValue = 0;

		#endregion

		#region Depth

		public bool EnableDepthTest = false;
		public bool EnableDepthMask = false;
		public DepthFunction DepthFunction = DepthFunction.Lequal;

		#endregion

		#region Cull Face

		public bool EnableCullFace = false;
		public CullFaceMode CullFaceMode = CullFaceMode.Back;
		public FrontFaceDirection FrontFaceDirection = FrontFaceDirection.Cw;
	
		#endregion
	}
	
	public static class GLState
	{
		#region Viewport

		private static Viewport m_Viewport;
		private static Viewport a_Viewport;

		public static Viewport Viewport
		{
			get
			{
				return m_Viewport; 
			}
			set
			{
				m_Viewport = value; 
			}
		}

		#endregion 
		
		#region Clearing

		private static OpenTK.Graphics.Color4 m_ClearColor;
		private static OpenTK.Graphics.Color4 a_ClearColor;

		public static void ClearColor(OpenTK.Graphics.Color4 clearColor)
		{
			m_ClearColor = clearColor; 
		}
		
		private static float m_ClearDepth;
		private static float a_ClearDepth;

		public static void ClearDepth(float clearDepth)
		{
			m_ClearDepth = clearDepth; 
		}

		#endregion

		#region Blend

		private static bool m_EnableBlend = false;
		private static bool a_EnableBlend = false;

		public static bool EnableBlend { get { return m_EnableBlend; } set { m_EnableBlend = value; } }

		private static BlendEquationMode m_BlendEquationMode = BlendEquationMode.FuncAdd;
		private static BlendEquationMode a_BlendEquationMode = BlendEquationMode.FuncAdd;

		public static void BlendEquation(BlendEquationMode blendEquationMode)
		{
			m_BlendEquationMode = blendEquationMode; 
		}

		private static BlendingFactorSrc m_BlendingFactorSrc = BlendingFactorSrc.SrcAlpha;
		private static BlendingFactorSrc a_BlendingFactorSrc = BlendingFactorSrc.SrcAlpha;

		private static BlendingFactorDest m_BlendingFactorDest = BlendingFactorDest.OneMinusSrcAlpha;
		private static BlendingFactorDest a_BlendingFactorDest = BlendingFactorDest.OneMinusSrcAlpha;

		public static void BlendFunc(BlendingFactorSrc blendingFactorSrc, BlendingFactorDest blendingFactorDest)
		{
			m_BlendingFactorSrc = blendingFactorSrc;
			m_BlendingFactorDest = blendingFactorDest;
		}

		private static bool m_EnableAlphaTest = false;
		private static bool a_EnableAlphaTest = false;

		public static bool EnableAlphaTest { get { return m_EnableAlphaTest; } set { m_EnableAlphaTest = value; } }

		private static AlphaFunction m_AlphaFunction = AlphaFunction.Always;
		private static AlphaFunction a_AlphaFunction = AlphaFunction.Always;
		private static float m_AlphaFunctionValue = 0;
		private static float a_AlphaFunctionValue = 0; 

		public static void AlphaFunc(AlphaFunction alphaFunction, float value)
		{
			m_AlphaFunction = alphaFunction;
			m_AlphaFunctionValue = value; 
		}

		#endregion

		#region Depth

		private static bool m_EnableDepthTest = false;
		private static bool a_EnableDepthTest = false;

		public static bool EnableDepthTest { get { return m_EnableDepthTest; } set { m_EnableDepthTest = value; } }

		private static bool m_EnableDepthMask = false;
		private static bool a_EnableDepthMask = false;

		public static bool EnableDepthMask { get { return m_EnableDepthMask; } set { m_EnableDepthMask = value; } }

		private static DepthFunction m_DepthFunction = DepthFunction.Lequal;
		private static DepthFunction a_DepthFunction = DepthFunction.Lequal;

		public static void DepthFunc(DepthFunction depthFunction)
		{
			m_DepthFunction = depthFunction; 
		}

		#endregion

		#region Cull Face

		private static bool m_EnableCullFace = false;
		private static bool a_EnableCullFace = false;

		public static bool EnableCullFace { get { return m_EnableCullFace; } set { m_EnableCullFace = value; } }

		private static CullFaceMode m_CullFaceMode = CullFaceMode.Back;
		private static CullFaceMode a_CullFaceMode = CullFaceMode.Back;

		public static void CullFace(CullFaceMode cullFaceMode)
		{
			m_CullFaceMode = cullFaceMode;
		}

		private static FrontFaceDirection m_FrontFaceDirection = FrontFaceDirection.Cw;
		private static FrontFaceDirection a_FrontFaceDirection = FrontFaceDirection.Cw;

		public static void FrontFace(FrontFaceDirection frontFaceDirection)
		{
			m_FrontFaceDirection = frontFaceDirection;
		}		 

		#endregion

		private static TextureUnit m_TextureUnit = TextureUnit.Texture0;

		public static void ActiveTexture(TextureUnit textureUnit)
		{
			if (m_TextureUnit != textureUnit)
			{
				m_TextureUnit = textureUnit;
				GL.ActiveTexture(m_TextureUnit);
			}
		}

        public static void BindTexture(TextureUnit textureUnit, Core.Textures.Texture2D texture)
        {
            ActiveTexture(textureUnit);

            if (texture == null) 
            {
                GL.BindTexture(TextureTarget.Texture2D, 0); 
            }
            else 
            {
                GL.BindTexture(texture.TextureTarget, texture.ResourceHandle);
            }

            GLState.CheckError(); 
        }

        public static void BindTexture(TextureUnit textureUnit, TextureTarget target, Core.Textures.Texture2D texture)
        {
            ActiveTexture(textureUnit);

            if (texture == null)
            {
                GL.BindTexture(target, 0);
            }
            else if (texture.TextureTarget != target)
            {
                throw new Exception("Incorrect texture target, wanted '" + target + "' got '" + texture.TextureTarget + "'"); 
            }
            else
            {
                GL.BindTexture(texture.TextureTarget, texture.ResourceHandle);
            }

            GLState.CheckError(); 
        }

        public static void BindTexture(TextureUnit textureUnit, TextureDDS cubemapMap)
        {
            ActiveTexture(textureUnit);

            if (cubemapMap == null)
            {
                GL.BindTexture(TextureTarget.TextureCubeMap, 0);
            }            
            else
            {
                GL.BindTexture(cubemapMap.TextureTarget, cubemapMap.ResourceHandle);
            }

            GLState.CheckError(); 
        }

        public static void BindTexture(TextureUnit textureUnit, TextureCube cubemapMap)
        {
            ActiveTexture(textureUnit);

            if (cubemapMap == null)
            {
                GL.BindTexture(TextureTarget.TextureCubeMap, 0);
            }            
            else
            {
                GL.BindTexture(cubemapMap.TextureTarget, cubemapMap.ResourceHandle);
            }

            GLState.CheckError(); 
        }
        

        public static void ClearTexture(TextureUnit textureUnit, TextureTarget textureTarget)
        {
            ActiveTexture(textureUnit);
            GL.BindTexture(textureTarget, 0);

            GLState.CheckError(); 
        }

		public static void CopyTo(GLSettings settings)
		{
			settings.Viewport = m_Viewport;
			settings.ClearColor = m_ClearColor;
			settings.ClearDepth = m_ClearDepth;
			settings.EnableBlend = m_EnableBlend;
			settings.BlendEquationMode = m_BlendEquationMode;
			settings.BlendingFactorSrc = m_BlendingFactorSrc;
			settings.BlendingFactorDest = m_BlendingFactorDest;
			settings.EnableAlphaTest = m_EnableAlphaTest;
			settings.AlphaFunction = m_AlphaFunction;
			settings.AlphaFunctionValue = m_AlphaFunctionValue;
			settings.EnableDepthTest = m_EnableDepthTest;
			settings.EnableDepthMask = m_EnableDepthMask;
			settings.DepthFunction = m_DepthFunction;
			settings.EnableCullFace = m_EnableCullFace;
			settings.CullFaceMode = m_CullFaceMode;
			settings.FrontFaceDirection = m_FrontFaceDirection;
		}

		public static void CopyFrom(GLSettings settings)
		{
			m_Viewport = settings.Viewport;
			m_ClearColor = settings.ClearColor;
			m_ClearDepth = settings.ClearDepth;
			m_EnableBlend = settings.EnableBlend;
			m_BlendEquationMode = settings.BlendEquationMode;
			m_BlendingFactorSrc = settings.BlendingFactorSrc;
			m_BlendingFactorDest = settings.BlendingFactorDest;
			m_EnableAlphaTest = settings.EnableAlphaTest;
			m_AlphaFunction = settings.AlphaFunction;
			m_AlphaFunctionValue = settings.AlphaFunctionValue;
			m_EnableDepthTest = settings.EnableDepthTest;
			m_EnableDepthMask = settings.EnableDepthMask;
			m_DepthFunction = settings.DepthFunction;
			m_EnableCullFace = settings.EnableCullFace;
			m_CullFaceMode = settings.CullFaceMode;
			m_FrontFaceDirection = settings.FrontFaceDirection;
		}

		public static void Apply(View3D view)
		{
			Apply(view.WindowSize); 
		}

		public static void Apply(int width, int height)
		{
			Apply(new Vector2(width, height));
		}
		
		public static void Apply(Vector2 windowSize)
		{
			if (a_ClearColor != m_ClearColor) 
			{
				GL.ClearColor(m_ClearColor);
				a_ClearColor = m_ClearColor; 
				
			}

			if (a_ClearDepth != m_ClearDepth)
			{
				GL.ClearDepth(m_ClearDepth);
				a_ClearDepth = m_ClearDepth;

			}

			if (a_EnableBlend != m_EnableBlend)
			{
				if (m_EnableBlend == true)
				{
					GL.Enable(EnableCap.Blend); 
				}
				else
				{
					GL.Disable(EnableCap.Blend); 
				}

				a_EnableBlend = m_EnableBlend;
			}

			if (a_BlendEquationMode != m_BlendEquationMode)
			{
				GL.BlendEquation(m_BlendEquationMode);
				a_BlendEquationMode = m_BlendEquationMode; 
			}

			if (a_BlendingFactorSrc != m_BlendingFactorSrc ||
				a_BlendingFactorDest != m_BlendingFactorDest) 
			{
				GL.BlendFunc(m_BlendingFactorSrc, m_BlendingFactorDest);
				
				a_BlendingFactorSrc = m_BlendingFactorSrc;
				a_BlendingFactorDest = m_BlendingFactorDest;
			}

			if (a_EnableDepthTest != m_EnableDepthTest)
			{
				if (m_EnableDepthTest == true)
				{
					GL.Enable(EnableCap.DepthTest);
				}
				else
				{
					GL.Disable(EnableCap.DepthTest);
				}

				a_EnableDepthTest = m_EnableDepthTest;
			}

			if (a_EnableDepthMask != m_EnableDepthMask)
			{
				GL.DepthMask(m_EnableDepthMask); 
				a_EnableDepthMask = m_EnableDepthMask;
			}			

			if (a_DepthFunction != m_DepthFunction)
			{
				GL.DepthFunc(m_DepthFunction);
				a_DepthFunction = m_DepthFunction;
			}

			if (a_EnableCullFace != m_EnableCullFace)
			{
				if (m_EnableCullFace == true)
				{
					GL.Enable(EnableCap.CullFace);
				}
				else
				{
					GL.Disable(EnableCap.CullFace);
				}

				a_EnableCullFace = m_EnableCullFace;
			}

			if (a_CullFaceMode != m_CullFaceMode)
			{
				GL.CullFace(m_CullFaceMode);
				a_CullFaceMode = m_CullFaceMode;
			}

			if (a_FrontFaceDirection != m_FrontFaceDirection)
			{
				GL.FrontFace(m_FrontFaceDirection);
				a_FrontFaceDirection = m_FrontFaceDirection;
			}

			if (a_Viewport.Equals(m_Viewport) == false) 
			{				
				GL.Viewport(m_Viewport.TopLeftX, (int)windowSize.Y - (m_Viewport.TopLeftY + m_Viewport.Height), m_Viewport.Width, m_Viewport.Height);
				a_Viewport = m_Viewport;
			}
		}

		public static void ApplyAll(View3D view)
		{
			ApplyAll(view.WindowSize); 
		}
		
		public static void ApplyAll(Vector2 windowSize)
		{
			GL.ActiveTexture(m_TextureUnit); 

			GL.ClearColor(m_ClearColor);
			a_ClearColor = m_ClearColor;

			GL.ClearDepth(m_ClearDepth);
			a_ClearDepth = m_ClearDepth;

			if (m_EnableBlend == true)
			{
				GL.Enable(EnableCap.Blend);
			}
			else
			{
				GL.Disable(EnableCap.Blend);
			}

			a_EnableBlend = m_EnableBlend;

			GL.BlendEquation(m_BlendEquationMode);
			a_BlendEquationMode = m_BlendEquationMode;

			GL.BlendFunc(m_BlendingFactorSrc, m_BlendingFactorDest);

			a_BlendingFactorSrc = m_BlendingFactorSrc;
			a_BlendingFactorDest = m_BlendingFactorDest;

			if (m_EnableDepthTest == true)
			{
				GL.Enable(EnableCap.DepthTest);
			}
			else
			{
				GL.Disable(EnableCap.DepthTest);
			}

			a_EnableDepthTest = m_EnableDepthTest;

			GL.DepthMask(m_EnableDepthMask);
			a_EnableDepthMask = m_EnableDepthMask;

			GL.DepthFunc(m_DepthFunction);
			a_DepthFunction = m_DepthFunction;

			if (m_EnableCullFace == true)
			{
				GL.Enable(EnableCap.CullFace);
			}
			else
			{
				GL.Disable(EnableCap.CullFace);
			}

			a_EnableCullFace = m_EnableCullFace;

			GL.CullFace(m_CullFaceMode);
			a_CullFaceMode = m_CullFaceMode;

			GL.FrontFace(m_FrontFaceDirection);
			a_FrontFaceDirection = m_FrontFaceDirection;

			GL.Viewport(m_Viewport.TopLeftX, (int)windowSize.Y - (m_Viewport.TopLeftY + m_Viewport.Height), m_Viewport.Width, m_Viewport.Height);			
			a_Viewport = m_Viewport;
		}

        //[Conditional("DEBUG")]
        public static void CheckError()
        {
            ErrorCode error = GL.GetError();

            if (error != ErrorCode.NoError)
            {
                RC.WriteLine("Check error: " + error.ToString()); 
            }
        }

        private static List<string> m_Extentions = null; 

        public static bool ContainsExtention(string extention)
        {
            if (m_Extentions == null) 
            {
                m_Extentions = new List<string>(GL.GetString(StringName.Extensions).Split(' '));
            }

            return m_Extentions.Contains(extention);
        }
    }
}
