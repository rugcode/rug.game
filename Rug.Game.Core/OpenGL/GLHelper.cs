﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using System;
using System.Diagnostics;

namespace Rug.Game
{
	public static class GLHelper
	{
		#region CreateFromQuaternion

		/// <summary>
		/// Build a rotation matrix from the specified quaternion.
		/// </summary>
		/// <param name="q">Quaternion to translate.</param>
		/// <param name="m">Matrix result.</param>
		public static void CreateFromQuaternion(ref Quaternion q, ref Matrix4 m)
		{
			m = Matrix4.Identity;

			float X = q.X;
			float Y = q.Y;
			float Z = q.Z;
			float W = q.W;

			float xx = X * X;
			float xy = X * Y;
			float xz = X * Z;
			float xw = X * W;
			float yy = Y * Y;
			float yz = Y * Z;
			float yw = Y * W;
			float zz = Z * Z;
			float zw = Z * W;

			m.M11 = 1 - 2 * (yy + zz);
			m.M21 = 2 * (xy - zw);
			m.M31 = 2 * (xz + yw);
			m.M12 = 2 * (xy + zw);
			m.M22 = 1 - 2 * (xx + zz);
			m.M32 = 2 * (yz - xw);
			m.M13 = 2 * (xz - yw);
			m.M23 = 2 * (yz + xw);
			m.M33 = 1 - 2 * (xx + yy);
		}

		/// <summary>
		/// Build a rotation matrix from the specified quaternion.
		/// </summary>
		/// <param name="q">Quaternion to translate.</param>
		/// <returns>A matrix instance.</returns>
		public static Matrix4 CreateFromQuaternion(ref Quaternion q)
		{
			Matrix4 result = Matrix4.Identity;

			float X = q.X;
			float Y = q.Y;
			float Z = q.Z;
			float W = q.W;

			float xx = X * X;
			float xy = X * Y;
			float xz = X * Z;
			float xw = X * W;
			float yy = Y * Y;
			float yz = Y * Z;
			float yw = Y * W;
			float zz = Z * Z;
			float zw = Z * W;

			result.M11 = 1 - 2 * (yy + zz);
			result.M21 = 2 * (xy - zw);
			result.M31 = 2 * (xz + yw);
			result.M12 = 2 * (xy + zw);
			result.M22 = 1 - 2 * (xx + zz);
			result.M32 = 2 * (yz - xw);
			result.M13 = 2 * (xz - yw);
			result.M23 = 2 * (yz + xw);
			result.M33 = 1 - 2 * (xx + yy);

			return result;
		}

		/// <summary>
		/// Build a rotation matrix from the specified quaternion.
		/// </summary>
		/// <param name="q">Quaternion to translate.</param>
		/// <param name="m">Matrix result.</param>
		public static void CreateFromQuaternion(ref Quaterniond q, ref Matrix4d m)
		{
			m = Matrix4d.Identity;

			double X = q.X;
			double Y = q.Y;
			double Z = q.Z;
			double W = q.W;

			double xx = X * X;
			double xy = X * Y;
			double xz = X * Z;
			double xw = X * W;
			double yy = Y * Y;
			double yz = Y * Z;
			double yw = Y * W;
			double zz = Z * Z;
			double zw = Z * W;

			m.M11 = 1 - 2 * (yy + zz);
			m.M21 = 2 * (xy - zw);
			m.M31 = 2 * (xz + yw);
			m.M12 = 2 * (xy + zw);
			m.M22 = 1 - 2 * (xx + zz);
			m.M32 = 2 * (yz - xw);
			m.M13 = 2 * (xz - yw);
			m.M23 = 2 * (yz + xw);
			m.M33 = 1 - 2 * (xx + yy);
		}

		/// <summary>
		/// Build a rotation matrix from the specified quaternion.
		/// </summary>
		/// <param name="q">Quaternion to translate.</param>
		/// <returns>A matrix instance.</returns>
		public static Matrix4d CreateFromQuaternion(ref Quaterniond q)
		{
			Matrix4d result = Matrix4d.Identity;

			double X = q.X;
			double Y = q.Y;
			double Z = q.Z;
			double W = q.W;

			double xx = X * X;
			double xy = X * Y;
			double xz = X * Z;
			double xw = X * W;
			double yy = Y * Y;
			double yz = Y * Z;
			double yw = Y * W;
			double zz = Z * Z;
			double zw = Z * W;

			result.M11 = 1 - 2 * (yy + zz);
			result.M21 = 2 * (xy - zw);
			result.M31 = 2 * (xz + yw);
			result.M12 = 2 * (xy + zw);
			result.M22 = 1 - 2 * (xx + zz);
			result.M32 = 2 * (yz - xw);
			result.M13 = 2 * (xz - yw);
			result.M23 = 2 * (yz + xw);
			result.M33 = 1 - 2 * (xx + yy);
			return result;
		}

		#endregion

		#region FromMatrix

		/// <summary>
		/// Build a quaternion from the specified rotation matrix.
		/// </summary>
		/// <param name="m">Matrix to translate</param>
		/// <param name="q">Quaternion result</param>
		public static void CreateFromMatrix(ref Matrix4 m, ref Quaternion q)
		{
			float trace = 1 + m.M11 + m.M22 + m.M33;
			float S = 0;
			float X = 0;
			float Y = 0;
			float Z = 0;
			float W = 0;

			if (trace > 0.0000001)
			{
				S = (float)Math.Sqrt(trace) * 2;
				X = (m.M23 - m.M32) / S;
				Y = (m.M31 - m.M13) / S;
				Z = (m.M12 - m.M21) / S;
				W = 0.25f * S;
			}
			else
			{
				if (m.M11 > m.M22 && m.M11 > m.M33)
				{
					// Column 0: 
					S = (float)Math.Sqrt(1.0 + m.M11 - m.M22 - m.M33) * 2;
					X = 0.25f * S;
					Y = (m.M12 + m.M21) / S;
					Z = (m.M31 + m.M13) / S;
					W = (m.M23 - m.M32) / S;
				}
				else if (m.M22 > m.M33)
				{
					// Column 1: 
					S = (float)Math.Sqrt(1.0 + m.M22 - m.M11 - m.M33) * 2;
					X = (m.M12 + m.M21) / S;
					Y = 0.25f * S;
					Z = (m.M23 + m.M32) / S;
					W = (m.M31 - m.M13) / S;
				}
				else
				{
					// Column 2:
					S = (float)Math.Sqrt(1.0 + m.M33 - m.M11 - m.M22) * 2;
					X = (m.M31 + m.M13) / S;
					Y = (m.M23 + m.M32) / S;
					Z = 0.25f * S;
					W = (m.M12 - m.M21) / S;
				}
			}

			q = new Quaternion(X, Y, Z, W);
		}

		/// <summary>
		/// Build a quaternion from the specified rotation matrix.
		/// </summary>
		/// <param name="m">Matrix to translate.</param>
		/// <returns>A quaternion</returns>
		public static Quaternion CreateFromMatrix(ref Matrix4 m)
		{
			Quaternion q;

			float trace = 1 + m.M11 + m.M22 + m.M33;
			float S = 0;
			float X = 0;
			float Y = 0;
			float Z = 0;
			float W = 0;

			if (trace > 0.0000001)
			{
				S = (float)Math.Sqrt(trace) * 2;
				X = (m.M23 - m.M32) / S;
				Y = (m.M31 - m.M13) / S;
				Z = (m.M12 - m.M21) / S;
				W = 0.25f * S;
			}
			else
			{
				if (m.M11 > m.M22 && m.M11 > m.M33)
				{
					// Column 0: 
					S = (float)Math.Sqrt(1.0 + m.M11 - m.M22 - m.M33) * 2;
					X = 0.25f * S;
					Y = (m.M12 + m.M21) / S;
					Z = (m.M31 + m.M13) / S;
					W = (m.M23 - m.M32) / S;
				}
				else if (m.M22 > m.M33)
				{
					// Column 1: 
					S = (float)Math.Sqrt(1.0 + m.M22 - m.M11 - m.M33) * 2;
					X = (m.M12 + m.M21) / S;
					Y = 0.25f * S;
					Z = (m.M23 + m.M32) / S;
					W = (m.M31 - m.M13) / S;
				}
				else
				{
					// Column 2:
					S = (float)Math.Sqrt(1.0 + m.M33 - m.M11 - m.M22) * 2;
					X = (m.M31 + m.M13) / S;
					Y = (m.M23 + m.M32) / S;
					Z = 0.25f * S;
					W = (m.M12 - m.M21) / S;
				}
			}
			q = new Quaternion(X, Y, Z, W);
			return q;
		}

		/// <summary>
		/// Build a quaternion from the specified rotation matrix.
		/// </summary>
		/// <param name="m">Matrix to translate</param>
		/// <param name="q">Quaternion result</param>
		public static void CreateFromMatrix(ref Matrix4d m, ref Quaterniond q)
		{
			double trace = 1 + m.M11 + m.M22 + m.M33;
			double S = 0;
			double X = 0;
			double Y = 0;
			double Z = 0;
			double W = 0;

			if (trace > 0.0000001)
			{
				S = Math.Sqrt(trace) * 2;
				X = (m.M23 - m.M32) / S;
				Y = (m.M31 - m.M13) / S;
				Z = (m.M12 - m.M21) / S;
				W = 0.25 * S;
			}
			else
			{
				if (m.M11 > m.M22 && m.M11 > m.M33)
				{
					// Column 0: 
					S = Math.Sqrt(1.0 + m.M11 - m.M22 - m.M33) * 2;
					X = 0.25 * S;
					Y = (m.M12 + m.M21) / S;
					Z = (m.M31 + m.M13) / S;
					W = (m.M23 - m.M32) / S;
				}
				else if (m.M22 > m.M33)
				{
					// Column 1: 
					S = Math.Sqrt(1.0 + m.M22 - m.M11 - m.M33) * 2;
					X = (m.M12 + m.M21) / S;
					Y = 0.25 * S;
					Z = (m.M23 + m.M32) / S;
					W = (m.M31 - m.M13) / S;
				}
				else
				{
					// Column 2:
					S = Math.Sqrt(1.0 + m.M33 - m.M11 - m.M22) * 2;
					X = (m.M31 + m.M13) / S;
					Y = (m.M23 + m.M32) / S;
					Z = 0.25 * S;
					W = (m.M12 - m.M21) / S;
				}
			}
			q = new Quaterniond(X, Y, Z, W);
		}

		/// <summary>
		/// Build a quaternion from the specified rotation matrix.
		/// </summary>
		/// <param name="m">Matrix to translate.</param>
		/// <returns>A quaternion</returns>
		public static Quaterniond CreateFromMatrix(ref Matrix4d m)
		{
			Quaterniond q;

			double trace = 1 + m.M11 + m.M22 + m.M33;
			double S = 0;
			double X = 0;
			double Y = 0;
			double Z = 0;
			double W = 0;

			if (trace > 0.0000001)
			{
				S = Math.Sqrt(trace) * 2;
				X = (m.M23 - m.M32) / S;
				Y = (m.M31 - m.M13) / S;
				Z = (m.M12 - m.M21) / S;
				W = 0.25 * S;
			}
			else
			{
				if (m.M11 > m.M22 && m.M11 > m.M33)
				{
					// Column 0: 
					S = Math.Sqrt(1.0 + m.M11 - m.M22 - m.M33) * 2;
					X = 0.25 * S;
					Y = (m.M12 + m.M21) / S;
					Z = (m.M31 + m.M13) / S;
					W = (m.M23 - m.M32) / S;
				}
				else if (m.M22 > m.M33)
				{
					// Column 1: 
					S = Math.Sqrt(1.0 + m.M22 - m.M11 - m.M33) * 2;
					X = (m.M12 + m.M21) / S;
					Y = 0.25 * S;
					Z = (m.M23 + m.M32) / S;
					W = (m.M31 - m.M13) / S;
				}
				else
				{
					// Column 2:
					S = Math.Sqrt(1.0 + m.M33 - m.M11 - m.M22) * 2;
					X = (m.M31 + m.M13) / S;
					Y = (m.M23 + m.M32) / S;
					Z = 0.25 * S;
					W = (m.M12 - m.M21) / S;
				}
			}
			q = new Quaterniond(X, Y, Z, W);

			return q;
		}

		#endregion

        //[Conditional("DEBUG")]
		public static void CheckFramebufferExt()
		{
			switch (GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
			{
				case FramebufferErrorCode.FramebufferCompleteExt:
					{
						Console.WriteLine("FBO: The framebuffer is complete and valid for rendering.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
					{
						Console.WriteLine("FBO: One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
					{
						Console.WriteLine("FBO: There are no attachments.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
					{
						Console.WriteLine("FBO: Attachments are of different size. All attachments must have the same width and height.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
					{
						Console.WriteLine("FBO: The color attachments have different format. All color attachments must have the same format.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
					{
						Console.WriteLine("FBO: An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
					{
						Console.WriteLine("FBO: The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
						break;
					}
				case FramebufferErrorCode.FramebufferUnsupportedExt:
					{
						Console.WriteLine("FBO: This particular FBO configuration is not supported by the implementation.");
						break;
					}
				default:
					{
						Console.WriteLine("FBO: Status unknown. (yes, this is really bad.)");
						break;
					}

			}
		}

        [Conditional("DEBUG")]
		internal static void CheckTexture()
		{			
			switch (GL.GetError())
			{
				case ErrorCode.NoError:
					return;
				case ErrorCode.InvalidEnum:
					RC.WriteError((int)ErrorCode.InvalidEnum, "Invalid Enum"); 
					break;			
				case ErrorCode.InvalidOperation:
					RC.WriteError((int)ErrorCode.InvalidOperation, "Invalid Operation");
					break;
				case ErrorCode.InvalidValue:
					RC.WriteError((int)ErrorCode.InvalidValue, "Invalid Value");
					break;
				case ErrorCode.OutOfMemory:
					RC.WriteError((int)ErrorCode.OutOfMemory, "Out Of Memory");
					break;
				case ErrorCode.StackOverflow:
					RC.WriteError((int)ErrorCode.StackOverflow, "Stack Overflow");
					break;
				case ErrorCode.StackUnderflow:
					RC.WriteError((int)ErrorCode.StackUnderflow, "Stack Underflow");
					break;
				case ErrorCode.TableTooLarge:
					RC.WriteError((int)ErrorCode.TableTooLarge, "Table Too Large");
					break;
				case ErrorCode.TextureTooLargeExt:
					RC.WriteError((int)ErrorCode.TextureTooLargeExt, "Texture Too Large");
					break;
				default:
					break;
			}
		}


        public static int MultiSampleToSampleCount(MultiSamples multisamples)
        {
            switch (multisamples)
            {
                case MultiSamples.X1:
                    return 1;
                case MultiSamples.X2:
                    return 2;
                case MultiSamples.X4:
                    return 4;
                case MultiSamples.X8:
                    return 8;
                case MultiSamples.X16:
                    return 16;
                default:
                    return 1;
            }
        }

        public static MultiSamples SampleCountToMultiSample(int value)
        {
            switch (value)
            {
                case 1:
                    return MultiSamples.X1;
                case 2:
                    return MultiSamples.X2;
                case 4:
                    return MultiSamples.X4;
                case 8:
                    return MultiSamples.X8;
                case 16:
                    return MultiSamples.X16;
                default:
                    return MultiSamples.X1;
            }
        }

	}
}
