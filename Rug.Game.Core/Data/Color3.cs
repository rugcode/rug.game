﻿using OpenTK.Graphics;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Data
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct Color3
	{
		public float R;
		public float G;
		public float B;

		public Color3(float r, float g, float b)
		{
			R = r;
			G = g;
			B = b;
		}

        public Color3(int rgb)
        {
            float r = (float)((rgb & 0xFF0000) >> 16) / 255f;
            float g = (float)((rgb & 0x00FF00) >> 8) / 255f;
            float b = (float)((rgb & 0x0000FF)) / 255f;

            R = r;
            G = g;
            B = b;
        }

		public static implicit operator Color3(Color4 color)
		{
			return new Color3(color.R, color.G, color.B);
		}

		public static Color4 ToColor4(Color3 color, float alpha)
		{
			return new Color4(color.R, color.G, color.B, alpha); 
		}

        public static Color3 Lerp(Color3 a, Color3 b, float blend)
        {
            a.R = (blend * (b.R - a.R)) + a.R;
            a.G = (blend * (b.G - a.G)) + a.G;
            a.B = (blend * (b.B - a.B)) + a.B;

            return a;
        }

        public static Color3 operator +(Color3 left, Color3 right)
        {
            left.R += right.R;
            left.G += right.G;
            left.B += right.B;
            return left;
        }

        public static Color3 operator -(Color3 left, Color3 right)
        {
            left.R -= right.R;
            left.G -= right.G;
            left.B -= right.B;
            return left;
        }

        public static Color3 operator -(Color3 vec)
        {
            vec.R = -vec.R;
            vec.G = -vec.G;
            vec.B = -vec.B;
            return vec;
        }

        public static Color3 operator *(Color3 vec, float scale)
        {
            vec.R *= scale;
            vec.G *= scale;
            vec.B *= scale;
            return vec;
        }

        public static Color3 operator *(float scale, Color3 vec)
        {
            vec.R *= scale;
            vec.G *= scale;
            vec.B *= scale;
            return vec;
        }

        public static Color3 operator /(Color3 vec, float scale)
        {
            float num = 1f / scale;
            vec.R *= num;
            vec.G *= num;
            vec.B *= num;
            return vec;
        }

        public static bool operator ==(Color3 left, Color3 right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Color3 left, Color3 right)
        {
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return ((this.R.GetHashCode() ^ this.G.GetHashCode()) ^ this.B.GetHashCode());
        }

        public override bool Equals(object obj)
        {
            return ((obj is Color3) && this.Equals((Color3)obj));
        }

        public bool Equals(Color3 other)
        {
            return (((this.R == other.R) && (this.G == other.G)) && (this.B == other.B));
        }
    }
}
