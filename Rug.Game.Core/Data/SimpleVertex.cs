﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct SimpleVertex
	{		
		public Vector3 Position;
		public Vector2 TextureCoords;
		public Color4 Color;

		public enum Elements : int { Position = 0, TextureCoords = 1, Color = 2 };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int TextureCoordsOffset;
		public static readonly int ColorOffset;

		static SimpleVertex()
		{
			Stride = BlittableValueType<SimpleVertex>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(SimpleVertex), "Position");
			TextureCoordsOffset = (int)Marshal.OffsetOf(typeof(SimpleVertex), "TextureCoords");
			ColorOffset = (int)Marshal.OffsetOf(typeof(SimpleVertex), "Color");

			if (ColorOffset + BlittableValueType<Color4>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.TextureCoords, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.TextureCoords);
			GL.EnableVertexAttribArray((int)Elements.Color);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.TextureCoords);
			GL.DisableVertexAttribArray((int)Elements.Color);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return SimpleVertex.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				baseLocation += 3;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.TextureCoords + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, TextureCoordsOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.TextureCoords + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.TextureCoords + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);

				baseLocation += 3;
			}

			#endregion
		}

		#endregion
	}
}
