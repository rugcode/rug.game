﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Data
{
	[StructLayout(LayoutKind.Sequential)]
	public struct PointLight
	{
		public Vector3 Position;
		public float Radius;

		public Color3 Color;
		public float SpecularAmount;		

		public enum Elements : int { Position = 0, Radius = 1, Color = 2, SpecularAmount };
		
		public static readonly int Stride;

		public static readonly int PositionOffset;
		public static readonly int RadiusOffset;
		public static readonly int ColorOffset;
		public static readonly int SpecularAmountOffset;

		static PointLight()
		{
			Stride = BlittableValueType<PointLight>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(PointLight), "Position");
			RadiusOffset = (int)Marshal.OffsetOf(typeof(PointLight), "Radius");
			ColorOffset = (int)Marshal.OffsetOf(typeof(PointLight), "Color");
			SpecularAmountOffset = (int)Marshal.OffsetOf(typeof(PointLight), "SpecularAmount");

			if (SpecularAmountOffset + BlittableValueType<float>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.Radius, 1, VertexAttribPointerType.Float, false, Stride, RadiusOffset);			
			GL.VertexAttribPointer((int)Elements.Color, 3, VertexAttribPointerType.Float, false, Stride, ColorOffset);
			GL.VertexAttribPointer((int)Elements.SpecularAmount, 1, VertexAttribPointerType.Float, false, Stride, SpecularAmountOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.Radius);
			GL.EnableVertexAttribArray((int)Elements.Color);
			GL.EnableVertexAttribArray((int)Elements.SpecularAmount);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.Radius);
			GL.DisableVertexAttribArray((int)Elements.Color);
			GL.DisableVertexAttribArray((int)Elements.SpecularAmount);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return PointLight.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Radius + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, RadiusOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.SpecularAmount + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, SpecularAmountOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Radius + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.SpecularAmount + baseLocation);

				baseLocation += 4;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Radius + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, RadiusOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.SpecularAmount + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, SpecularAmountOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Radius + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.SpecularAmount + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Radius + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.SpecularAmount + baseLocation, devisor);

				baseLocation += 4;
			}

			#endregion
		}

		#endregion
	}
}
