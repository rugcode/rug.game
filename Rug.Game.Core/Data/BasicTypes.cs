﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System.Runtime.InteropServices;

namespace Rug.Game.Core.Data
{
	public static class BasicTypes
	{
		public static class Vector2
		{
			public enum Elements : int { Position = 0 };
			public static readonly int Stride;
			public static readonly int PositionOffset;

			static Vector2()
			{
				Stride = BlittableValueType<OpenTK.Vector2>.Stride;

				PositionOffset = 0;
			}

			public static void Bind()
			{
				GL.VertexAttribPointer((int)Elements.Position, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);

				GL.EnableVertexAttribArray((int)Elements.Position);
			}

			public static void Unbind()
			{
				GL.DisableVertexAttribArray((int)Elements.Position);
			}

			public readonly static IVertexFormat Format = new FormatInfo();

			#region Format Class

			private class FormatInfo : IVertexFormat
			{
				#region IVertexFormat Members

				public int Stride
				{
					get { return Vector2.Stride; }
				}

				public void CreateLayout(ref int baseLocation)
				{
					GL.VertexAttribPointer((int)Elements.Position + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);

					GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

					baseLocation++; 
				}

				public void CreateLayout(ref int baseLocation, int devisor)
				{
					GL.VertexAttribPointer((int)Elements.Position + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, PositionOffset);

					GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

					GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);

					baseLocation++;
				}

				#endregion
			}

			#endregion
		}

		public static class Vector3
		{
			public enum Elements : int { Position = 0 };
			public static readonly int Stride;
			public static readonly int PositionOffset;

			static Vector3()
			{
				Stride = BlittableValueType<OpenTK.Vector3>.Stride;

				PositionOffset = 0;
			}

			public static void Bind()
			{
				GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);

				GL.EnableVertexAttribArray((int)Elements.Position);
			}

			public static void Unbind()
			{
				GL.DisableVertexAttribArray((int)Elements.Position);
			}

			public readonly static IVertexFormat Format = new FormatInfo();

			#region Format Class

			private class FormatInfo : IVertexFormat
			{
				#region IVertexFormat Members

				public int Stride
				{
					get { return Vector3.Stride; }
				}

				public void CreateLayout(ref int baseLocation)
				{
					GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);

					GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

					baseLocation++;
				}

				public void CreateLayout(ref int baseLocation, int devisor)
				{
					GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);

					GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

					GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);

					baseLocation++;
				}

				#endregion
			}

			#endregion
		}

		public static class Vector4
		{
			public enum Elements : int { Position = 0 };
			public static readonly int Stride;
			public static readonly int PositionOffset;

			static Vector4()
			{
				Stride = BlittableValueType<OpenTK.Vector4>.Stride;

				PositionOffset = 0;
			}

			public static void Bind()
			{
				GL.VertexAttribPointer((int)Elements.Position, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);

				GL.EnableVertexAttribArray((int)Elements.Position);
			}

			public static void Unbind()
			{
				GL.DisableVertexAttribArray((int)Elements.Position);
			}

			public readonly static IVertexFormat Format = new FormatInfo();

			#region Format Class

			private class FormatInfo : IVertexFormat
			{
				#region IVertexFormat Members

				public int Stride
				{
					get { return Vector4.Stride; }
				}

				public void CreateLayout(ref int baseLocation)
				{
					GL.VertexAttribPointer((int)Elements.Position + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);

					GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

					baseLocation++;
				}

				public void CreateLayout(ref int baseLocation, int devisor)
				{
					GL.VertexAttribPointer((int)Elements.Position + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);

					GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);

					GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);

					baseLocation++;
				}

				#endregion
			}

			#endregion
		}

		public static class Matrix4
		{			
			public enum Elements : int { Row0 = 0, Row1 = 1, Row2 = 2, Row3 = 3, };
			public static readonly int Stride;

			public static readonly int Row0Offset;
			public static readonly int Row1Offset;
			public static readonly int Row2Offset;
			public static readonly int Row3Offset;


			static Matrix4()
			{
				Stride = BlittableValueType<OpenTK.Matrix4>.Stride;

				Row0Offset = (int)Marshal.OffsetOf(typeof(Matrix4), "Row0");
				Row1Offset = (int)Marshal.OffsetOf(typeof(Matrix4), "Row1");
				Row2Offset = (int)Marshal.OffsetOf(typeof(Matrix4), "Row2");
				Row3Offset = (int)Marshal.OffsetOf(typeof(Matrix4), "Row3");
			}

			public static void Bind()
			{
				GL.VertexAttribPointer((int)Elements.Row0, 4, VertexAttribPointerType.Float, false, Stride, Row0Offset);
				GL.VertexAttribPointer((int)Elements.Row1, 4, VertexAttribPointerType.Float, false, Stride, Row1Offset);
				GL.VertexAttribPointer((int)Elements.Row2, 4, VertexAttribPointerType.Float, false, Stride, Row2Offset);
				GL.VertexAttribPointer((int)Elements.Row3, 4, VertexAttribPointerType.Float, false, Stride, Row3Offset);

				GL.EnableVertexAttribArray((int)Elements.Row0);
				GL.EnableVertexAttribArray((int)Elements.Row1);
				GL.EnableVertexAttribArray((int)Elements.Row2);
				GL.EnableVertexAttribArray((int)Elements.Row3);
			}

			public static void Unbind()
			{
				GL.DisableVertexAttribArray((int)Elements.Row0);
				GL.DisableVertexAttribArray((int)Elements.Row1);
				GL.DisableVertexAttribArray((int)Elements.Row2);
				GL.DisableVertexAttribArray((int)Elements.Row3);
			}

			public readonly static IVertexFormat Format = new FormatInfo();

			#region Format Class

			private class FormatInfo : IVertexFormat
			{
				#region IVertexFormat Members

				public int Stride
				{
					get { return Matrix4.Stride; }
				}

				public void CreateLayout(ref int baseLocation)
				{
					GL.VertexAttribPointer((int)Elements.Row0 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row0Offset);
					GL.VertexAttribPointer((int)Elements.Row1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row1Offset);
					GL.VertexAttribPointer((int)Elements.Row2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row2Offset);
					GL.VertexAttribPointer((int)Elements.Row3 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row3Offset);

					GL.EnableVertexAttribArray((int)Elements.Row0 + baseLocation);
					GL.EnableVertexAttribArray((int)Elements.Row1 + baseLocation);
					GL.EnableVertexAttribArray((int)Elements.Row2 + baseLocation);
					GL.EnableVertexAttribArray((int)Elements.Row3 + baseLocation);

					baseLocation += 4;
				}

				public void CreateLayout(ref int baseLocation, int devisor)
				{
					GL.VertexAttribPointer((int)Elements.Row0 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row0Offset);
					GL.VertexAttribPointer((int)Elements.Row1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row1Offset);
					GL.VertexAttribPointer((int)Elements.Row2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row2Offset);
					GL.VertexAttribPointer((int)Elements.Row3 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Row3Offset);

					GL.EnableVertexAttribArray((int)Elements.Row0 + baseLocation);
					GL.EnableVertexAttribArray((int)Elements.Row1 + baseLocation);
					GL.EnableVertexAttribArray((int)Elements.Row2 + baseLocation);
					GL.EnableVertexAttribArray((int)Elements.Row3 + baseLocation);

					GL.Arb.VertexAttribDivisor((int)Elements.Row0 + baseLocation, devisor);
					GL.Arb.VertexAttribDivisor((int)Elements.Row1 + baseLocation, devisor);
					GL.Arb.VertexAttribDivisor((int)Elements.Row2 + baseLocation, devisor);
					GL.Arb.VertexAttribDivisor((int)Elements.Row3 + baseLocation, devisor);

					baseLocation += 4;
				}

				#endregion
			}

			#endregion
		}
	}
}
