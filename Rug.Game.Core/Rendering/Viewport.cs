﻿using OpenTK;
using System;

namespace Rug.Game.Core.Rendering
{
	public struct Viewport : IEquatable<Viewport>
	{
		public int Height;
		public float MaxDepth;
		public float MinDepth;
		public int TopLeftX;
		public int TopLeftY;
		public int Width;

		public Vector2 Size { get { return new Vector2((float)Width, (float)Height); } }

		public Viewport(int x, int y, int width, int height)
		{
			TopLeftX = x;
			TopLeftY = y;
			Width = width;
			Height = height;

			MaxDepth = 1;
			MinDepth = 0; 
		}

		public Viewport(int x, int y, int width, int height, float minZ, float maxZ)
		{
			TopLeftX = x;
			TopLeftY = y;
			Width = width;
			Height = height;

			MaxDepth = maxZ;
			MinDepth = minZ; 
		}

		public override bool Equals(object obj)
		{
			if (obj is Viewport)
			{
				return Equals((Viewport)obj);
			}

			return base.Equals(obj);
		}

		#region IEquatable<Viewport> Members

		public bool Equals(Viewport other)
		{
			return other.TopLeftX == TopLeftX &&
				other.TopLeftY == TopLeftY &&
				other.Width == Width &&
				other.Height == Height; 
		}

		#endregion
	}
}
