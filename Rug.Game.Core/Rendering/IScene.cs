﻿
namespace Rug.Game.Core.Rendering
{
	public interface IScene
	{
		void Render(View3D view);
	}
}
