﻿using OpenTK;
using System;

namespace Rug.Game.Core.Rendering
{
	public class Camera
	{
		#region Fields
		
		public float Scale;
		public float AspectRatio;
		public float FOV;

		public Vector3 Forward;
		public Vector3 Up;
		public Vector3 Right;

		public float Offset = -4f;
		public float ViewRotation = 0f;
		public float ViewElevation = 0f;

		public Vector3 Center;
		public Vector3 LookAtPoint = new Vector3(0, 0, -0.5f);
		public Quaternion Rotation = Quaternion.Identity;
		public Quaternion DestinationRotation = Quaternion.Identity;

		public bool SmoothCamera = true;

		public float SmoothSpeed = 0.12f;

		public bool IsFreeCamera = true;

		public bool HasChanged;

		#endregion

		public Camera(float FOV, float AspectRatio, float scale)
		{
			Center = new Vector3(0, 0, 0);
			Rotation = Quaternion.Identity;
			DestinationRotation = Quaternion.Identity; 

			this.Scale = scale; 
			this.FOV = FOV;
			this.AspectRatio = AspectRatio;			

			Rotate(0f, 0f, 0f);
		}

		public void Setup(float FOV, float AspectRatio, float Scale)
		{
			this.Scale = Scale; 
			this.FOV = FOV;
			this.AspectRatio = AspectRatio;						
		}

		#region State

		public CameraState State
		{
			get 
			{
				return new CameraState(this); 
			}

			set
			{
				SetFromState(value); 
			}
		}

		public void SetFromState(CameraState value)
		{
			Scale = value.Scale;
			AspectRatio = value.AspectRatio;
			FOV = value.FOV;
			Forward = value.Forward;
			Up = value.Up;
			Right = value.Right;
			Center = value.Center;
			Rotation = value.Rotation;

			HasChanged = true; 
		}

		public void CopyToState(ref CameraState value)
		{
			value.Scale = Scale;
			value.AspectRatio = AspectRatio;
			value.FOV = FOV;
			value.Forward = Forward;
			value.Up = Up;
			value.Right = Right;
			value.Center = Center;
			value.Rotation = Rotation;
		}

		#endregion

		#region	Move

		public virtual void Translate(Vector3 translate)
		{			
			Center += Vector3.TransformVector(translate, Matrix4.Rotate(Rotation));
		}

		public virtual void Rotate(float yaw, float pitch, float roll)
		{
			if (SmoothCamera == true)
			{
				if (yaw != 0) DestinationRotation = Quaternion.Multiply(DestinationRotation, Quaternion.FromAxisAngle(Vector3.UnitY, yaw));
				if (pitch != 0) DestinationRotation = Quaternion.Multiply(DestinationRotation, Quaternion.FromAxisAngle(Vector3.UnitX, pitch));
				if (roll != 0) DestinationRotation = Quaternion.Multiply(DestinationRotation, Quaternion.FromAxisAngle(Vector3.UnitZ, roll));

				DestinationRotation.Normalize();
			}
			else
			{
				if (yaw != 0) Rotation = Quaternion.Multiply(Rotation, Quaternion.FromAxisAngle(Vector3.UnitY, yaw));
				if (pitch != 0) Rotation = Quaternion.Multiply(Rotation, Quaternion.FromAxisAngle(Vector3.UnitX, pitch));
				if (roll != 0) Rotation = Quaternion.Multiply(Rotation, Quaternion.FromAxisAngle(Vector3.UnitZ, roll));
			
				Rotation.Normalize();
			}
			
			SetRightUpForwardVectors(); 			
		}

		public virtual void SetRotation(Quaternion newRotation)
		{
			if (SmoothCamera == true)
			{
				DestinationRotation = newRotation;
				DestinationRotation.Normalize();
			}
			else
			{
				Rotation = newRotation;
				Rotation.Normalize();
			}

			SetRightUpForwardVectors(); 			
		}

		public void SetRightUpForwardVectors()
		{
			SetVector3(ref Right, Vector3.Transform(Vector3.UnitX, Rotation));
			SetVector3(ref Up, Vector3.Transform(Vector3.UnitY, Rotation));
			SetVector3(ref Forward, Vector3.Transform(Vector3.UnitZ, Rotation));
		}

		private void SetVector3(ref Vector3 vector3, Vector4 vector4)
		{
			vector3.X = vector4.X;
			vector3.Y = vector4.Y;
			vector3.Z = vector4.Z; 
		}

		private void SetVector3(ref Vector3 vector3, Vector3 vector4)
		{
			vector3.X = vector4.X;
			vector3.Y = vector4.Y;
			vector3.Z = vector4.Z;
		}

		#endregion

		public virtual void Update()
		{
			if (SmoothCamera == true)
			{
				Rotation = Quaternion.Slerp(Rotation, DestinationRotation, SmoothSpeed * Environment.FrameDelta);
				Rotation.Normalize();
				SetRightUpForwardVectors(); 
			}
		} 
	}

	public struct CameraState : IEquatable<CameraState>, IEquatable<Camera>
	{
		public float Scale;
		public float AspectRatio;
		public float FOV;

		public Vector3 Forward;
		public Vector3 Up;
		public Vector3 Right;

		public Vector3 Center;
		public Quaternion Rotation;

		public CameraState(Camera camera)
		{
			Scale = camera.Scale;
			AspectRatio = camera.AspectRatio;
			FOV = camera.FOV;
			Forward = camera.Forward;
			Up = camera.Up;
			Right = camera.Right;
			Center = camera.Center;
			Rotation = camera.Rotation; 			
		}

		public override bool Equals(object obj)
		{
			if (obj is CameraState)
			{
				return Equals((CameraState)obj); 
			}
			else if (obj is Camera)
			{
				return Equals(obj as Camera);
			}
			else
			{
				return false;
			}
		}

		public bool Equals(CameraState state)
		{
			if (Scale == state.Scale &&
				AspectRatio == state.AspectRatio &&
				FOV == state.FOV &&
				Forward == state.Forward &&
				Up == state.Up &&
				Right == state.Right &&
				Center == state.Center &&
				Rotation == state.Rotation)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool Equals(Camera camera)
		{
			if (Scale == camera.Scale &&
				AspectRatio == camera.AspectRatio &&
				FOV == camera.FOV &&
				Forward == camera.Forward &&
				Up == camera.Up &&
				Right == camera.Right &&
				Center == camera.Center &&
				Rotation == camera.Rotation)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
