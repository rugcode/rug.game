﻿using OpenTK;

namespace Rug.Game.Core.Rendering
{
    public enum ProjectionKind
    {
        Perspective,
        Orthographic
    }

	public class View3D : View2D
	{
		public Camera Camera;
		public ViewFrustum Frustum;

		public Matrix4 View = Matrix4.Identity;
		public Matrix4 ViewCenter = Matrix4.Identity;
		public Matrix4 Projection = Matrix4.Identity;
		public Matrix4 ProjectionInverse = Matrix4.Identity;
		public Matrix4 World = Matrix4.Identity;
		public Matrix4 NormalWorld = Matrix4.Identity;
		public Matrix4 WorldInverse = Matrix4.Identity;

		public Vector3 LightDirection;
		public Vector2 RangeZ = new Vector2(0.1f, 6000f);
        //public Vector2 RangeZ = new Vector2(60f, 100f); 
        public Vector3 OrthographicBoxSize = new Vector3(100, 100, 200);

		public float DayTime;
		public bool UpdateFrustum = true;

        public ProjectionKind ProjectionKind { get; set; } 

		public View3D(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight, float FOV, float Scale)
			: base(activeRegion, windowWidth, windowHeight)
		{
            ProjectionKind = Rendering.ProjectionKind.Perspective; 

			Camera = new Camera(FOV, (float)activeRegion.Width / (float)activeRegion.Height, Scale);
			Frustum = new ViewFrustum();

			UpdateProjection();

			SetGlobalLight(new Vector3(0, 0, 0), new Vector3(0, 1f, 1f)); 
		}

		public override void Resize(System.Drawing.Rectangle activeRegion, int windowWidth, int windowHeight)
		{
			base.Resize(activeRegion, windowWidth, windowHeight);

			Camera.Setup(Camera.FOV, (float)activeRegion.Width / (float)activeRegion.Height, Camera.Scale);

			UpdateProjection();
		}

		public void SetGlobalLight(Vector3 pos, Vector3 lookAt)
		{
			LightDirection = lookAt - pos;
			LightDirection.Normalize();
		}	

		public void LookAt(Vector3 target)
		{
			Vector3 dir = Camera.Center - target;
			dir.Normalize();
			Vector3 right = new Vector3(dir.Z, 0, -dir.X);
			right.Normalize();
			Vector3 up = Vector3.Cross(dir, right);
		}

		public virtual void UpdateProjection()
		{
            if (ProjectionKind == Rendering.ProjectionKind.Perspective)
            {
                Projection = Matrix4.CreatePerspectiveFieldOfView(Camera.FOV, Camera.AspectRatio, RangeZ.X, RangeZ.Y);
            }
            else
            {
                Projection = Matrix4.CreateOrthographicOffCenter(
                    -OrthographicBoxSize.X, OrthographicBoxSize.X,
                    -OrthographicBoxSize.Y, OrthographicBoxSize.Y,
                    -OrthographicBoxSize.Z, OrthographicBoxSize.Z); 
            }

			ProjectionInverse = Matrix4.Invert(Projection);

			if (Camera.IsFreeCamera)
			{
				Matrix4 matRot = Matrix4.CreateFromQuaternion(Quaternion.Conjugate(Camera.Rotation));

				ViewCenter = (Matrix4.CreateTranslation(Camera.Center) * matRot);

				Vector3 fwd = Vector3.TransformVector(Vector3.UnitZ, matRot);

				float offset = Camera.Offset;
				if (Camera.ViewRotation != 0)
				{
					offset *= -1f;
				}

				View = 
					Matrix4.CreateTranslation(Camera.Forward * offset) * 
					(Matrix4.CreateTranslation(Camera.Center) * 
					(Matrix4.CreateFromQuaternion(Quaternion.Conjugate(Camera.Rotation)) * 
					 Matrix4.CreateRotationY(Camera.ViewRotation) * 
					 Matrix4.CreateRotationX(Camera.ViewElevation)));

				World = View;
				//WorldInverse = Matrix4.Invert(View);

                Quaternion normalQuaternion = Camera.Rotation;
                normalQuaternion.Invert();

                NormalWorld = Matrix4.CreateFromQuaternion(normalQuaternion);
				//NormalWorld = WorldInverse;
				//NormalWorld.Transpose();
			}
			else
			{
				
			}

			if (UpdateFrustum == true)
			{
				Frustum.Setup(this, Camera);
			}
		}

		public Vector2 TransformMouseCoords(System.Windows.Forms.Form form, System.Windows.Forms.MouseEventArgs e)
		{
			if ((form == null) ||
				(form.ClientSize.Width == Viewport.Width && form.ClientSize.Height == Viewport.Height))
			{
				return new Vector2(e.X, e.Y);
			}

			return new Vector2(e.X, e.Y);
		}

        public Vector2 TransformMouseCoords(System.Windows.Forms.Form form, System.Drawing.Point point)
        {
            if ((form == null) ||
                (form.ClientSize.Width == Viewport.Width && form.ClientSize.Height == Viewport.Height))
            {
                return new Vector2(point.X, point.Y);
            }

            return new Vector2(point.X, point.Y);
        }

		public Vector2 TransformMouseRay(System.Windows.Forms.Form form, System.Windows.Forms.MouseEventArgs e)
		{
			Vector2 coords = TransformMouseCoords(form, e);

			return new Vector2((coords.X / (float)Viewport.Width) * 2.0f - 1.0f, ((coords.Y / (float)Viewport.Height) * 2.0f - 1.0f) * -1.0f);
		}

        public Vector2 TransformMouseRay(Vector2 coords)
        {
            return new Vector2((coords.X / (float)Viewport.Width) * 2.0f - 1.0f, ((coords.Y / (float)Viewport.Height) * 2.0f - 1.0f) * -1.0f);
        }

		public bool NeedsResize(System.Drawing.Size size)
		{
			return Viewport.Width != size.Width || Viewport.Height != size.Height; 
		}
	}
}
