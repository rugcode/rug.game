﻿using System;

namespace Rug.Game.Ui.Composite
{
	public class ControlGroupAttribute : Attribute, IComparable<ControlGroupAttribute>, IEquatable<ControlGroupAttribute>
	{
		private int m_Order;

		private string m_Name;

		public int Order { get { return m_Order; } } 

		public string Name { get { return m_Name; } } 
			
		public ControlGroupAttribute(int order, string name)
		{
			m_Order = order; 
			m_Name = name; 
		}

		public override bool Equals(object obj)
		{
			return ToString().Equals(obj);
		}

		public override int GetHashCode()
		{
			return ToString().GetHashCode();
		}

		public override string ToString()
		{
			return string.Format("{0}, {1}", Order, Name);
		}

		#region IEquatable<ControlGroupAttribute> Members

		public bool Equals(ControlGroupAttribute other)
		{
			return m_Order == other.Order && m_Name == other.Name; 
		}

		#endregion

		#region IComparable<ControlGroupAttribute> Members

		public int CompareTo(ControlGroupAttribute other)
		{
			return Order - other.Order; 
		}

		#endregion
	}
}
