﻿
using Rug.Game.Ui.Dynamic;
using System;
using System.Reflection;

namespace Rug.Game.Ui.Composite
{
	public class ControlIntergrationAttribute : Attribute
	{
		public readonly int Index; 

		public ControlIntergrationAttribute(int index)
		{
			Index = index;
		}
	}

	public struct ControlOptionsStub : IComparable, IComparable<ControlOptionsStub>
	{
		public ControlOptionsAttribute Attribute;
		public FieldInfo Field;

		public ControlOptionsStub(ControlOptionsAttribute attribute, FieldInfo field)
		{
			Attribute = attribute;
			Field = field; 
		}

		#region IComparable Members

		public int CompareTo(ControlOptionsStub other)
		{
			return Attribute.CompareTo(other.Attribute); 
		}

		#endregion

		#region IComparable Members

		public int CompareTo(object obj)
		{
			if (obj is ControlOptionsStub == false) throw new ArgumentException();

			return CompareTo((ControlOptionsStub)obj);
		}

		#endregion
	}

	public class ControlOptionsAttribute : Attribute, IComparable, IComparable<ControlOptionsAttribute>
	{
		#region Private Members

		private int m_Order;
		private string m_Name;

		#endregion

		#region Public Properties

		public int Order { get { return m_Order; } }
		public string Name { get { return m_Name; } }

		#endregion

		public ControlOptionsAttribute(int order, string name)
		{
			m_Order = order;
			m_Name = name;
		}

		#region IComparable<SliderOptionsAttribute> Members

		public int CompareTo(ControlOptionsAttribute other)
		{
			return this.Order - other.Order; 
		}

		#endregion

		#region IComparable Members

		public int CompareTo(object obj)
		{
			if (obj is ControlOptionsAttribute == false) throw new ArgumentException();

			return CompareTo(obj as ControlOptionsAttribute);
		}

		#endregion
	}

	public abstract class SliderOptionsAttribute : ControlOptionsAttribute
	{
		#region Private Members

		private SliderScale m_SliderScale;

		#endregion

		#region Public Properties

		public abstract string Format { get; }

		public SliderScale SliderScale { get { return m_SliderScale; } }

		#endregion

		public SliderOptionsAttribute(int order, string name, SliderScale sliderScale)
			: base(order, name)
		{
			m_SliderScale = sliderScale;
		}
	}

	public class SliderOptions_IntAttribute : SliderOptionsAttribute
	{
		#region Private Members

		private int m_MinValue;
		private int m_MaxValue;
		private int m_DefaultValue;

		#endregion

		#region Public Properties
		
		public override string Format { get { return "N0"; } }

		public int MinValue { get { return m_MinValue; } }
		public int MaxValue { get { return m_MaxValue; } }
		public int DefaultValue { get { return m_DefaultValue; } }

		#endregion

		public SliderOptions_IntAttribute(int order, string name, int minValue, int maxValue, int defaultValue, SliderScale sliderScale)
			: base(order, name, sliderScale) 
		{
			m_MinValue = minValue;
			m_MaxValue = maxValue;
			m_DefaultValue = defaultValue;
		}
	}

	public class SliderOptions_FloatAttribute : SliderOptionsAttribute
	{
		#region Private Members
		
		private string m_Format;

		private float m_MinValue;
		private float m_MaxValue;
		private float m_DefaultValue;

		#endregion

		#region Public Properties

		public override string Format { get { return m_Format; } }

		public float MinValue { get { return m_MinValue; } }
		public float MaxValue { get { return m_MaxValue; } }
		public float DefaultValue { get { return m_DefaultValue; } }

		#endregion

		public SliderOptions_FloatAttribute(int order, string name, float minValue, float maxValue, float defaultValue, SliderScale sliderScale)
			: base(order, name, sliderScale)
		{
			m_Format = "N3";

			m_MinValue = minValue;
			m_MaxValue = maxValue;
			m_DefaultValue = defaultValue;
		}

		public SliderOptions_FloatAttribute(int order, string name, float minValue, float maxValue, float defaultValue, SliderScale sliderScale, string format)
			: base(order, name, sliderScale) 
		{
			m_Format = format;

			m_MinValue = minValue;
			m_MaxValue = maxValue;
			m_DefaultValue = defaultValue;
		}
	}
}
