﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditor
{
    public partial class MapEditorForm : Form
    {
        public MapEditorForm()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mapEditorPanel1.New(); 
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                mapEditorPanel1.Import(openFileDialog1.FileName);
            }     
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                mapEditorPanel1.Export(saveFileDialog1.FileName); 
            }            
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mapEditorPanel1.Copy(); 
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mapEditorPanel1.Paste(); 
        }

        private void generateModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mapEditorPanel1.Generate(); 
        }

        private void autoGenerateToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            mapEditorPanel1.AutoGenerate = autoGenerateToolStripMenuItem.Checked;
        }

        private void MapEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true; 

                this.Hide(); 
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            mapEditorPanel1.SnowLevel = (1f / (float)trackBar1.Maximum) * (float)trackBar1.Value;
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            mapEditorPanel1.SnowAngle = (float)trackBar2.Value * 0.2f;
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            mapEditorPanel1.BrushSize = (float)trackBar3.Value * 0.1f; 
        }

        private void undersideToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            mapEditorPanel1.GenerateUnderSide = undersideToolStripMenuItem.Checked; 
        }
    }
}
