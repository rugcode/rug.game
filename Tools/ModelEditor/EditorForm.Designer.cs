﻿namespace ModelEditor
{
	partial class EditorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.modelEditMode = new System.Windows.Forms.ToolStripButton();
            this.hullEditMode = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.colorSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.extraKey = new System.Windows.Forms.ToolStripTextBox();
            this.snapScale = new System.Windows.Forms.ToolStripComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importOBJToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.isoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.topToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bottomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.leftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.frontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.verticesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wireframeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decompositionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subdivideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertNormalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupByNormalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_GroupByNormalFaceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapYAndZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapXAndZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeCircularYAndZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.makeCircularUnitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.move05YToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mapEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decomposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.flipZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(692, 38);
            this.panel1.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelEditMode,
            this.hullEditMode,
            this.toolStripButton1,
            this.colorSelect,
            this.toolStripButton2,
            this.toolStripComboBox1,
            this.extraKey,
            this.snapScale});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(692, 39);
            this.toolStrip1.TabIndex = 0;
            // 
            // modelEditMode
            // 
            this.modelEditMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.modelEditMode.Image = ((System.Drawing.Image)(resources.GetObject("modelEditMode.Image")));
            this.modelEditMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.modelEditMode.Name = "modelEditMode";
            this.modelEditMode.Size = new System.Drawing.Size(36, 36);
            this.modelEditMode.Text = "Verts & Faces";
            this.modelEditMode.Click += new System.EventHandler(this.modelEditMode_Click);
            // 
            // hullEditMode
            // 
            this.hullEditMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.hullEditMode.Image = ((System.Drawing.Image)(resources.GetObject("hullEditMode.Image")));
            this.hullEditMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.hullEditMode.Name = "hullEditMode";
            this.hullEditMode.Size = new System.Drawing.Size(36, 36);
            this.hullEditMode.Text = "Hull Verts";
            this.hullEditMode.Click += new System.EventHandler(this.hullEditMode_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "Extra Verts";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // colorSelect
            // 
            this.colorSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.colorSelect.Image = ((System.Drawing.Image)(resources.GetObject("colorSelect.Image")));
            this.colorSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colorSelect.Name = "colorSelect";
            this.colorSelect.Size = new System.Drawing.Size(36, 36);
            this.colorSelect.Text = "Face Color";
            this.colorSelect.Click += new System.EventHandler(this.colorSelect_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 36);
            this.toolStripButton2.ToolTipText = "Select Color";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 39);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // extraKey
            // 
            this.extraKey.Name = "extraKey";
            this.extraKey.Size = new System.Drawing.Size(100, 39);
            this.extraKey.TextChanged += new System.EventHandler(this.extraKey_TextChanged);
            // 
            // snapScale
            // 
            this.snapScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.snapScale.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "8",
            "16",
            "32",
            "64",
            "128"});
            this.snapScale.Name = "snapScale";
            this.snapScale.Size = new System.Drawing.Size(121, 39);
            this.snapScale.SelectedIndexChanged += new System.EventHandler(this.snapScale_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(692, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.importOBJToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // importOBJToolStripMenuItem
            // 
            this.importOBJToolStripMenuItem.Name = "importOBJToolStripMenuItem";
            this.importOBJToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.importOBJToolStripMenuItem.Text = "Import OBJ";
            this.importOBJToolStripMenuItem.Click += new System.EventHandler(this.importOBJToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.defaultToolStripMenuItem,
            this.isoToolStripMenuItem,
            this.toolStripSeparator1,
            this.topToolStripMenuItem,
            this.bottomToolStripMenuItem,
            this.toolStripSeparator2,
            this.leftToolStripMenuItem,
            this.rightToolStripMenuItem,
            this.toolStripSeparator3,
            this.frontToolStripMenuItem,
            this.backToolStripMenuItem,
            this.toolStripSeparator4,
            this.verticesToolStripMenuItem,
            this.wireframeToolStripMenuItem,
            this.facesToolStripMenuItem,
            this.normalsToolStripMenuItem,
            this.hullToolStripMenuItem,
            this.extrasToolStripMenuItem,
            this.decompositionToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // defaultToolStripMenuItem
            // 
            this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
            this.defaultToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.defaultToolStripMenuItem.Text = "Default";
            this.defaultToolStripMenuItem.Click += new System.EventHandler(this.defaultToolStripMenuItem_Click);
            // 
            // isoToolStripMenuItem
            // 
            this.isoToolStripMenuItem.Name = "isoToolStripMenuItem";
            this.isoToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.isoToolStripMenuItem.Text = "Iso";
            this.isoToolStripMenuItem.Click += new System.EventHandler(this.isoToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // topToolStripMenuItem
            // 
            this.topToolStripMenuItem.Name = "topToolStripMenuItem";
            this.topToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.topToolStripMenuItem.Text = "Top";
            this.topToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // bottomToolStripMenuItem
            // 
            this.bottomToolStripMenuItem.Name = "bottomToolStripMenuItem";
            this.bottomToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.bottomToolStripMenuItem.Text = "Bottom";
            this.bottomToolStripMenuItem.Click += new System.EventHandler(this.bottomToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(152, 6);
            // 
            // leftToolStripMenuItem
            // 
            this.leftToolStripMenuItem.Name = "leftToolStripMenuItem";
            this.leftToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.leftToolStripMenuItem.Text = "Left";
            this.leftToolStripMenuItem.Click += new System.EventHandler(this.leftToolStripMenuItem_Click);
            // 
            // rightToolStripMenuItem
            // 
            this.rightToolStripMenuItem.Name = "rightToolStripMenuItem";
            this.rightToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.rightToolStripMenuItem.Text = "Right";
            this.rightToolStripMenuItem.Click += new System.EventHandler(this.rightToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(152, 6);
            // 
            // frontToolStripMenuItem
            // 
            this.frontToolStripMenuItem.Name = "frontToolStripMenuItem";
            this.frontToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.frontToolStripMenuItem.Text = "Front";
            this.frontToolStripMenuItem.Click += new System.EventHandler(this.frontToolStripMenuItem_Click);
            // 
            // backToolStripMenuItem
            // 
            this.backToolStripMenuItem.Name = "backToolStripMenuItem";
            this.backToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.backToolStripMenuItem.Text = "Back";
            this.backToolStripMenuItem.Click += new System.EventHandler(this.backToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(152, 6);
            // 
            // verticesToolStripMenuItem
            // 
            this.verticesToolStripMenuItem.Name = "verticesToolStripMenuItem";
            this.verticesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.verticesToolStripMenuItem.Text = "Vertices";
            this.verticesToolStripMenuItem.Click += new System.EventHandler(this.verticesToolStripMenuItem_Click);
            // 
            // wireframeToolStripMenuItem
            // 
            this.wireframeToolStripMenuItem.Name = "wireframeToolStripMenuItem";
            this.wireframeToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.wireframeToolStripMenuItem.Text = "Wireframe";
            this.wireframeToolStripMenuItem.Click += new System.EventHandler(this.wireframeToolStripMenuItem_Click);
            // 
            // facesToolStripMenuItem
            // 
            this.facesToolStripMenuItem.Name = "facesToolStripMenuItem";
            this.facesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.facesToolStripMenuItem.Text = "Faces";
            this.facesToolStripMenuItem.Click += new System.EventHandler(this.facesToolStripMenuItem_Click);
            // 
            // normalsToolStripMenuItem
            // 
            this.normalsToolStripMenuItem.Name = "normalsToolStripMenuItem";
            this.normalsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.normalsToolStripMenuItem.Text = "Normals";
            this.normalsToolStripMenuItem.Click += new System.EventHandler(this.normalsToolStripMenuItem_Click);
            // 
            // hullToolStripMenuItem
            // 
            this.hullToolStripMenuItem.Name = "hullToolStripMenuItem";
            this.hullToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.hullToolStripMenuItem.Text = "Hull";
            this.hullToolStripMenuItem.Click += new System.EventHandler(this.hullToolStripMenuItem_Click);
            // 
            // extrasToolStripMenuItem
            // 
            this.extrasToolStripMenuItem.Name = "extrasToolStripMenuItem";
            this.extrasToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.extrasToolStripMenuItem.Text = "Extras";
            this.extrasToolStripMenuItem.Click += new System.EventHandler(this.extrasToolStripMenuItem_Click);
            // 
            // decompositionToolStripMenuItem
            // 
            this.decompositionToolStripMenuItem.Name = "decompositionToolStripMenuItem";
            this.decompositionToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.decompositionToolStripMenuItem.Text = "Decomposition";
            this.decompositionToolStripMenuItem.Click += new System.EventHandler(this.decompositionToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subdivideToolStripMenuItem,
            this.invertNormalsToolStripMenuItem,
            this.groupByNormalToolStripMenuItem,
            this.m_GroupByNormalFaceMenuItem,
            this.swapYAndZToolStripMenuItem,
            this.swapXAndZToolStripMenuItem,
            this.makeCircularYAndZToolStripMenuItem,
            this.toolStripMenuItem1,
            this.makeCircularUnitToolStripMenuItem,
            this.flipZToolStripMenuItem,
            this.move05YToolStripMenuItem,
            this.scaleToolStripMenuItem,
            this.mapEditorToolStripMenuItem,
            this.decomposeToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // subdivideToolStripMenuItem
            // 
            this.subdivideToolStripMenuItem.Name = "subdivideToolStripMenuItem";
            this.subdivideToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.subdivideToolStripMenuItem.Text = "Subdivide";
            this.subdivideToolStripMenuItem.Click += new System.EventHandler(this.subdivideToolStripMenuItem_Click);
            // 
            // invertNormalsToolStripMenuItem
            // 
            this.invertNormalsToolStripMenuItem.Name = "invertNormalsToolStripMenuItem";
            this.invertNormalsToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.invertNormalsToolStripMenuItem.Text = "Invert Normals";
            this.invertNormalsToolStripMenuItem.Click += new System.EventHandler(this.invertNormalsToolStripMenuItem_Click);
            // 
            // groupByNormalToolStripMenuItem
            // 
            this.groupByNormalToolStripMenuItem.Name = "groupByNormalToolStripMenuItem";
            this.groupByNormalToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.groupByNormalToolStripMenuItem.Text = "Group By Normal";
            this.groupByNormalToolStripMenuItem.Click += new System.EventHandler(this.groupByNormalToolStripMenuItem_Click);
            // 
            // m_GroupByNormalFaceMenuItem
            // 
            this.m_GroupByNormalFaceMenuItem.Name = "m_GroupByNormalFaceMenuItem";
            this.m_GroupByNormalFaceMenuItem.Size = new System.Drawing.Size(240, 22);
            this.m_GroupByNormalFaceMenuItem.Text = "Group By Normal (Face Islands)";
            this.m_GroupByNormalFaceMenuItem.Click += new System.EventHandler(this.m_GroupByNormalFaceMenuItem_Click);
            // 
            // swapYAndZToolStripMenuItem
            // 
            this.swapYAndZToolStripMenuItem.Name = "swapYAndZToolStripMenuItem";
            this.swapYAndZToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.swapYAndZToolStripMenuItem.Text = "Swap Y and Z";
            this.swapYAndZToolStripMenuItem.Click += new System.EventHandler(this.swapYAndZToolStripMenuItem_Click);
            // 
            // swapXAndZToolStripMenuItem
            // 
            this.swapXAndZToolStripMenuItem.Name = "swapXAndZToolStripMenuItem";
            this.swapXAndZToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.swapXAndZToolStripMenuItem.Text = "Swap X and Z";
            this.swapXAndZToolStripMenuItem.Click += new System.EventHandler(this.swapXAndZToolStripMenuItem_Click);
            // 
            // makeCircularYAndZToolStripMenuItem
            // 
            this.makeCircularYAndZToolStripMenuItem.Name = "makeCircularYAndZToolStripMenuItem";
            this.makeCircularYAndZToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.makeCircularYAndZToolStripMenuItem.Text = "Make Circular Y and Z";
            this.makeCircularYAndZToolStripMenuItem.Click += new System.EventHandler(this.makeUnitCircularYAndZToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(240, 22);
            this.toolStripMenuItem1.Text = "Make Circular Y and Z (Unit)";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.makeCircularYAndZToolStripMenuItem_Click);
            // 
            // makeCircularUnitToolStripMenuItem
            // 
            this.makeCircularUnitToolStripMenuItem.Name = "makeCircularUnitToolStripMenuItem";
            this.makeCircularUnitToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.makeCircularUnitToolStripMenuItem.Text = "Make Circular (Unit)";
            this.makeCircularUnitToolStripMenuItem.Click += new System.EventHandler(this.makeCircularUnitToolStripMenuItem_Click);
            // 
            // move05YToolStripMenuItem
            // 
            this.move05YToolStripMenuItem.Name = "move05YToolStripMenuItem";
            this.move05YToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.move05YToolStripMenuItem.Text = "Move -0.5 Y";
            this.move05YToolStripMenuItem.Click += new System.EventHandler(this.move05YToolStripMenuItem_Click);
            // 
            // scaleToolStripMenuItem
            // 
            this.scaleToolStripMenuItem.Name = "scaleToolStripMenuItem";
            this.scaleToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.scaleToolStripMenuItem.Text = "Scale All";
            this.scaleToolStripMenuItem.Click += new System.EventHandler(this.scaleToolStripMenuItem_Click);
            // 
            // mapEditorToolStripMenuItem
            // 
            this.mapEditorToolStripMenuItem.CheckOnClick = true;
            this.mapEditorToolStripMenuItem.Name = "mapEditorToolStripMenuItem";
            this.mapEditorToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.mapEditorToolStripMenuItem.Text = "Map Editor";
            this.mapEditorToolStripMenuItem.CheckedChanged += new System.EventHandler(this.mapEditorToolStripMenuItem_CheckedChanged);
            // 
            // decomposeToolStripMenuItem
            // 
            this.decomposeToolStripMenuItem.Name = "decomposeToolStripMenuItem";
            this.decomposeToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.decomposeToolStripMenuItem.Text = "Decompose";
            this.decomposeToolStripMenuItem.Click += new System.EventHandler(this.decomposeToolStripMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Xml files|*.xml|All files|*.*";
            this.saveFileDialog1.Title = "Save model file";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Xml files|*.xml|All files|*.*";
            this.openFileDialog1.Title = "Load model file";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Location = new System.Drawing.Point(0, 69);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(692, 564);
            this.panel3.TabIndex = 2;
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.Filter = "OBJ Files|*.obj|All files|*.*";
            this.openFileDialog2.Title = "Load model file";
            // 
            // flipZToolStripMenuItem
            // 
            this.flipZToolStripMenuItem.Name = "flipZToolStripMenuItem";
            this.flipZToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.flipZToolStripMenuItem.Text = "Flip Z";
            this.flipZToolStripMenuItem.Click += new System.EventHandler(this.flipZToolStripMenuItem_Click);
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 633);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EditorForm";
            this.Text = "Model Editor";
            this.Load += new System.EventHandler(this.EditorForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EditorForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EditorForm_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.EditorForm_KeyUp);
            this.Resize += new System.EventHandler(this.EditorForm_Resize);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem topToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem bottomToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem leftToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem rightToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem frontToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem verticesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem facesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem hullToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem extrasToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem isoToolStripMenuItem;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton modelEditMode;
		private System.Windows.Forms.ToolStripButton hullEditMode;
		private System.Windows.Forms.ToolStripButton toolStripButton1;
		private System.Windows.Forms.ToolStripButton colorSelect;
		private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.ToolStripComboBox snapScale;
		private System.Windows.Forms.ToolStripTextBox extraKey;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ToolStripMenuItem normalsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem wireframeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapYAndZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertNormalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subdivideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeCircularYAndZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupByNormalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem makeCircularUnitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mapEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapXAndZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem move05YToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripMenuItem importOBJToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.ToolStripMenuItem m_GroupByNormalFaceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decomposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decompositionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipZToolStripMenuItem;
	}
}

