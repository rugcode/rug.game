﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game;
using Rug.Game.Core.MeshData;
using Rug.Game.Flat.Models;
using Rug.Game.Physics.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ModelEditor
{
	public partial class EditorForm : Form
	{
		private ModelEditor.EditorPanel panel2;
        private MapEditorForm m_MapEditor;

		public EditorForm()
		{
			InitializeComponent();
		}

		private void EditorForm_Load(object sender, EventArgs e)
		{
			panel2 = new EditorPanel(); 
			panel2.SelectionChanged +=new EventHandler(panel2_SelectionChanged);

			panel3.Controls.Add(panel2);

			panel2.Dock = DockStyle.Fill; 

			toolStripComboBox1.Items.AddRange(
				new object[] 
				{ 
                    ExtraTypes.Volume, 	
					ExtraTypes.MainThruster, 
					ExtraTypes.Thruster, 
					ExtraTypes.HardPoint,
					ExtraTypes.Turret,
					ExtraTypes.Pylon, 	
				});

			verticesToolStripMenuItem.Checked = false;
			wireframeToolStripMenuItem.Checked = true;
            facesToolStripMenuItem.Checked = false;
            normalsToolStripMenuItem.Checked = false;
            hullToolStripMenuItem.Checked = false;
			extrasToolStripMenuItem.Checked = true;
            decompositionToolStripMenuItem.Checked = true; 

            SetVisibleItems();

			ModelData model = new ModelData();

			model.Verts.Add(new Vector3(0f, 0.5f, 0f));
			model.Verts.Add(new Vector3(1f, -0.5f, 0f));
			model.Verts.Add(new Vector3(0f, -0.5f, 1f));
			model.Verts.Add(new Vector3(-1f, -0.5f, 0f));

			model.Faces.Add(new FaceData()
			{
				V0 = 0,
				V1 = 1,
				V2 = 2,
				Color = Color.White,
				Group = -1,
			});

			model.Faces.Add(new FaceData()
			{
				V0 = 0,
				V1 = 2,
				V2 = 3,
				Color = Color.White,
				Group = -1,
			});

			model.Extras.Add(new ExtraData()
			{
				Type = 0,
				Position = new Vector3(0, 0, 0),
				Direction = new Vector3(1, 0, 0),				
			});
			
			panel2.ModelData = model;
			panel2.GridScale = 8f;
			panel2.Scale = 128f;
			panel2.VertSize = 7f;
			panel2.ExtraLength = 1f;
			panel2.SnapToGrid = true;

			DoubleBuffered = true;

			panel2.Invalidate();

			Invalidate();

            m_MapEditor = new MapEditorForm();
            m_MapEditor.VisibleChanged += m_MapEditor_VisibleChanged;
		}

        void m_MapEditor_VisibleChanged(object sender, EventArgs e)
        {
            mapEditorToolStripMenuItem.Checked = m_MapEditor.Visible; 
        }

		private void EditorForm_Resize(object sender, EventArgs e)
		{
			panel2.Invalidate(); 

			Invalidate();
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ModelData model = new ModelData();

			panel2.ModelData = model;
			panel2.GridScale = 8f;
			panel2.Scale = 128f;
			panel2.VertSize = 7f;
			panel2.ExtraLength = 1f;
			panel2.SnapToGrid = true;
			panel2.Invalidate();
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (String.IsNullOrEmpty(panel2.ModelData.FilePath) == false)
			{
				string resolvedPath = Helper.ResolvePath(panel2.ModelData.FilePath); 

				FileInfo fileInfo = new FileInfo(resolvedPath);

				openFileDialog1.InitialDirectory = fileInfo.DirectoryName;
				openFileDialog1.FileName = fileInfo.Name; 
			}

			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				newToolStripMenuItem_Click(sender, e);

				panel2.ModelData.Load(openFileDialog1.FileName);

				panel2.Invalidate();
			}
		}

        private void importOBJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                newToolStripMenuItem_Click(sender, e);

			    Vector3[] verts;
			    ushort[] indices;

			    ObjLoader.LoadObject_Indexed(openFileDialog2.FileName, Vector4.Zero, out verts, out indices);

                foreach (Vector3 vert in verts)
                {
                    panel2.ModelData.Verts.Add(vert); 
                }

                for (int i = 0; i < indices.Length; i += 3)
                {
                    panel2.ModelData.Faces.Add(new FaceData()
                    {
                        Group = -1, 
                        Color = Color4.White, 
                        V0 = indices[i],
                        V1 = indices[i + 1],
                        V2 = indices[i + 2],
                    });
                }

                panel2.ModelData.CalcuateFaceNormals(); 

                panel2.Invalidate();
            }
        }

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (String.IsNullOrEmpty(panel2.ModelData.FilePath) == true)
			{
				saveAsToolStripMenuItem_Click(sender, e);
			}
			else
			{
				panel2.ModelData.Save(panel2.ModelData.FilePath); 
			}
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (String.IsNullOrEmpty(panel2.ModelData.FilePath) == false)
			{
				string resolvedPath = Helper.ResolvePath(panel2.ModelData.FilePath);

				FileInfo fileInfo = new FileInfo(resolvedPath);

				saveFileDialog1.InitialDirectory = fileInfo.DirectoryName;
				saveFileDialog1.FileName = fileInfo.Name;
			}

			if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				panel2.ModelData.Save(saveFileDialog1.FileName);
			}
		}

		private void closeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			newToolStripMenuItem_Click(sender, e);
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Close(); 
		}

		private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Default;

			panel2.Invalidate();
		}

		private void isoToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Iso;

			panel2.Invalidate();
		}

		private void topToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Top;
			
			panel2.Invalidate();
		}

		private void bottomToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Bottom;

			panel2.Invalidate();
		}

		private void leftToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Left;

			panel2.Invalidate();
		}

		private void rightToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Right;

			panel2.Invalidate();
		}

		private void frontToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Front;

			panel2.Invalidate();
		}

		private void backToolStripMenuItem_Click(object sender, EventArgs e)
		{
			panel2.ViewMode = EditorPanel.ViewModes.Back;

			panel2.Invalidate();
		}

		private void verticesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			verticesToolStripMenuItem.Checked = !verticesToolStripMenuItem.Checked;

			SetVisibleItems(); 			
		}

		private void wireframeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			wireframeToolStripMenuItem.Checked = !wireframeToolStripMenuItem.Checked;

			SetVisibleItems(); 
		}

		private void facesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			facesToolStripMenuItem.Checked = !facesToolStripMenuItem.Checked;

			SetVisibleItems(); 
		}


		private void normalsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			normalsToolStripMenuItem.Checked = !normalsToolStripMenuItem.Checked;

			SetVisibleItems(); 
		}

		private void hullToolStripMenuItem_Click(object sender, EventArgs e)
		{
			hullToolStripMenuItem.Checked = !hullToolStripMenuItem.Checked;

			SetVisibleItems(); 
		}

		private void extrasToolStripMenuItem_Click(object sender, EventArgs e)
		{
			extrasToolStripMenuItem.Checked = !extrasToolStripMenuItem.Checked;

			SetVisibleItems(); 
		}

        private void decompositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            decompositionToolStripMenuItem.Checked = !decompositionToolStripMenuItem.Checked;

            SetVisibleItems(); 
        }

		private void SetVisibleItems()
		{
			EditorPanel.VisibleItemFlags flags = EditorPanel.VisibleItemFlags.None;

			if (verticesToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.Vertices;
			}

			if (wireframeToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.WireFrame;
			}

			if (facesToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.Faces;
			}

			if (normalsToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.Normals;
			}

			if (hullToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.Hull;
			}

			if (extrasToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.Extras;
			}

            if (decompositionToolStripMenuItem.Checked == true)
			{
				flags |= EditorPanel.VisibleItemFlags.Decomposed;
			}
            		
			panel2.VisibleItems = flags;

			panel2.Invalidate();
		}

		private void EditorForm_KeyDown(object sender, KeyEventArgs e)
		{

		}

		private void EditorForm_KeyUp(object sender, KeyEventArgs e)
		{

		}

		private void EditorForm_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{

		}

		private void hullEditMode_Click(object sender, EventArgs e)
		{
			panel2.EditorMode = EditorPanel.EditorModes.Hull;
            panel2.PaintMode = false; 
		}

		private void modelEditMode_Click(object sender, EventArgs e)
		{
			panel2.EditorMode = EditorPanel.EditorModes.Model;
            panel2.PaintMode = false; 
		}

		private void toolStripButton1_Click(object sender, EventArgs e)
		{
			panel2.EditorMode = EditorPanel.EditorModes.Extras;
            panel2.PaintMode = false; 
		}

		private void colorSelect_Click(object sender, EventArgs e)
		{
            panel2.PaintMode = true; 
		}

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (panel2.HasValidSelection == true &&
                panel2.Selection.Array == ModelArrays.Faces)
            {
                //FaceData data = panel2.ModelData.Faces[panel2.Selection.Index];

                //colorDialog1.Color = (Color)data.Color;

                if (colorDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //data.Color = colorDialog1.Color;

                    /* 
                    foreach (ModelSelectionInfo info in panel2.SelectedItems)
                    {
                        FaceData face = panel2.ModelData.Faces[info.Index];

                        face.Color = colorDialog1.Color;

                        panel2.ModelData.Faces[info.Index] = face;
                    }
                    */ 

                    toolStripButton2.BackColor = colorDialog1.Color; 
                }                
            }
        }

		private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (toolStripComboBox1.SelectedIndex == -1) 
			{
				return; 
			}

			if (panel2.HasValidSelection == true &&
				panel2.Selection.Array == ModelArrays.Extras)
			{
				int index = (panel2.Selection.Index - (panel2.Selection.Index % 2)) / 2;

				ExtraData data = panel2.ModelData.Extras[index];

				data.Type = (ExtraTypes)toolStripComboBox1.SelectedIndex;

				panel2.ModelData.Extras[index] = data; 
			}
		}

		private void extraKey_TextChanged(object sender, EventArgs e)
		{
			if (panel2.HasValidSelection == true &&
				panel2.Selection.Array == ModelArrays.Extras)
			{
				int index = (panel2.Selection.Index - (panel2.Selection.Index % 2)) / 2;

				ExtraData data = panel2.ModelData.Extras[index];

				data.Key = extraKey.Text;

				panel2.ModelData.Extras[index] = data;
			}
		}

		private void panel2_SelectionChanged(object sender, EventArgs e)
		{
			if (panel2.HasValidSelection == true &&
				panel2.Selection.Array == ModelArrays.Extras)
			{
				int index = (panel2.Selection.Index - (panel2.Selection.Index % 2)) / 2;

				toolStripComboBox1.SelectedIndex = (int)panel2.ModelData.Extras[index].Type;
				toolStripComboBox1.Enabled = true;

				extraKey.Text = panel2.ModelData.Extras[index].Key;
				extraKey.Enabled = true; 
			}
			else 
			{
				toolStripComboBox1.SelectedIndex = -1; 
				toolStripComboBox1.Enabled = false;

				extraKey.Text = "";
				extraKey.Enabled = false; 
			}

			if (panel2.HasValidSelection == true &&
				panel2.Selection.Array == ModelArrays.Faces)
			{
                if (panel2.PaintMode == true)
                {
                    foreach (ModelSelectionInfo info in panel2.SelectedItems)
                    {
                        FaceData face = panel2.ModelData.Faces[info.Index];

                        face.Color = toolStripButton2.BackColor;

                        panel2.ModelData.Faces[info.Index] = face;
                    }
                }

				colorSelect.Enabled = true;
                toolStripButton2.Enabled = true;
                toolStripButton2.BackColor = (Color)panel2.ModelData.Faces[panel2.Selection.Index].Color; 
			}
			else
			{
				colorSelect.Enabled = false;
                toolStripButton2.Enabled = false; 
			}
		}

		private void snapScale_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (snapScale.SelectedIndex == -1) 
			{
				return; 
			}

			float value = float.Parse(snapScale.SelectedItem.ToString());

			panel2.GridScale = value;
		}

        private void swapYAndZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                panel2.ModelData.Verts[i] = panel2.ModelData.Verts[i].Xzy; 
            }

            for (int i = 0; i < panel2.ModelData.Extras.Count; i++)
            {
                ExtraData data = panel2.ModelData.Extras[i];

                data.Position = data.Position.Xzy;

                panel2.ModelData.Extras[i] = data; 
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();            
        }

        private void swapXAndZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                panel2.ModelData.Verts[i] = panel2.ModelData.Verts[i].Zyx;
            }

            for (int i = 0; i < panel2.ModelData.Extras.Count; i++)
            {
                ExtraData data = panel2.ModelData.Extras[i];

                data.Position = data.Position.Zyx;

                panel2.ModelData.Extras[i] = data;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();  
        }

        private void invertNormalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Faces.Count; i++)
            {
                FaceData data = panel2.ModelData.Faces[i];

                data.FlipNormal();

                panel2.ModelData.Faces[i] = data; 
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();   
        }

        private void subdivideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<FaceData> originalFaces = new List<FaceData>(panel2.ModelData.Faces);

            panel2.ModelData.Faces.Clear(); 

            for (int i = 0; i < originalFaces.Count; i++)
            {
                FaceData data = originalFaces[i];

                int v0i = data.V0;
                int v1i = data.V1;
                int v2i = data.V2;

                Vector3 v0 = panel2.ModelData.Verts[v0i];
                Vector3 v1 = panel2.ModelData.Verts[v1i];
                Vector3 v2 = panel2.ModelData.Verts[v2i];

                Vector3 v0_v1 = (v0 + v1) / 2f;
                Vector3 v1_v2 = (v1 + v2) / 2f;
                Vector3 v2_v0 = (v2 + v0) / 2f;

                int v0_v1i = panel2.ModelData.Verts.Count;
                panel2.ModelData.Verts.Add(v0_v1);

                int v1_v2i = panel2.ModelData.Verts.Count;
                panel2.ModelData.Verts.Add(v1_v2);

                int v2_v0i = panel2.ModelData.Verts.Count;
                panel2.ModelData.Verts.Add(v2_v0);

                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color,
                    Normal = data.Normal,
                    Group = data.Group,
                    V0 = v0i,
                    V1 = v0_v1i,
                    V2 = v2_v0i,
                });


                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color,
                    Normal = data.Normal,
                    Group = data.Group,
                    V0 = v1i,
                    V1 = v1_v2i,
                    V2 = v0_v1i,
                });

                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color,
                    Normal = data.Normal,
                    Group = data.Group,
                    V0 = v2i,
                    V1 = v2_v0i,
                    V2 = v1_v2i,
                });

                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color,
                    Normal = data.Normal,
                    Group = data.Group,
                    V0 = v0_v1i,
                    V1 = v1_v2i,
                    V2 = v2_v0i,
                });

                /*
                Vector3 v3 = (v0 + v1 + v2) / 3f;

                int v3i =  panel2.ModelData.Verts.Count; 
                 
                panel2.ModelData.Verts.Add(v3); 
                
                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color, 
                    Normal = data.Normal, 
                    Group = data.Group, 
                    V0 = v0i, 
                    V1 = v1i, 
                    V2 = v3i,
                });

                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color,
                    Normal = data.Normal,
                    Group = data.Group,
                    V0 = v1i,
                    V1 = v2i,
                    V2 = v3i,
                });

                panel2.ModelData.Faces.Add(new FaceData()
                {
                    Color = data.Color,
                    Normal = data.Normal,
                    Group = data.Group,
                    V0 = v2i,
                    V1 = v0i,
                    V2 = v3i,
                });
                */
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();   
        }

        private void makeUnitCircularYAndZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            float maxDist = 0;

            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                maxDist = Math.Max(panel2.ModelData.Verts[i].Yz.Length, maxDist);
            }

            MakeCircularYZ(maxDist);
        }

        private void makeCircularYAndZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MakeCircularYZ(1f);
        }

        private void MakeCircularYZ(float maxDist)
        {
            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                Vector3 vert = panel2.ModelData.Verts[i];

                Vector2 yz = vert.Yz;

                yz.Normalize();

                vert.Yz = yz * maxDist;

                panel2.ModelData.Verts[i] = vert;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();
        }

        private void makeCircularUnitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MakeCircularXyz(1f);
        }

        private void MakeCircularXyz(float maxDist)
        {
            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                Vector3 vert = panel2.ModelData.Verts[i];

                Vector3 xyz = vert;

                xyz.Normalize();

                vert = xyz * maxDist;

                panel2.ModelData.Verts[i] = vert;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();
        }

        private void scaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            float max = 0; 

            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                Vector3 vert = panel2.ModelData.Verts[i];

                max = Math.Max(max, vert.Length); 
            }

            float scale = 2f / max;

            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                Vector3 vert = panel2.ModelData.Verts[i];

                panel2.ModelData.Verts[i] = vert * scale;
            }

            for (int i = 0; i < panel2.ModelData.Extras.Count; i++)
            {
                ExtraData data = panel2.ModelData.Extras[i];
                Vector3 vert = data.Position;

                data.Position = vert * scale;

                panel2.ModelData.Extras[i] = data;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();
        }

        private void flipZToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                Vector3 vert = panel2.ModelData.Verts[i];

                vert.Z *= -1;

                panel2.ModelData.Verts[i] = vert;
            }

            for (int i = 0; i < panel2.ModelData.Extras.Count; i++)
            {
                ExtraData data = panel2.ModelData.Extras[i];
                Vector3 vert = data.Position;

                vert.Z *= -1;

                data.Position = vert;

                panel2.ModelData.Extras[i] = data;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.FlipFace(); 

            panel2.Invalidate();
        }

        private void move05YToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Verts.Count; i++)
            {
                Vector3 vert = panel2.ModelData.Verts[i];

                vert.Y -= 0.5f;

                panel2.ModelData.Verts[i] = vert;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();
        }

        private void groupByNormalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Faces.Count; i++)
            {
                FaceData data = panel2.ModelData.Faces[i];

                data.Group = -1; 

                panel2.ModelData.Faces[i] = data; 
            }

            panel2.ModelData.CalcuateFaceNormals();

            int groupID = 0; 

            for (int i = 0; i < panel2.ModelData.Faces.Count; i++)
            {
                FaceData data = panel2.ModelData.Faces[i];

                Vector3 normal = data.Normal;

                for (int j = i + 1; j < panel2.ModelData.Faces.Count; j++)
                {
                    FaceData data2 = panel2.ModelData.Faces[j];

                    if (data.Normal == data2.Normal)
                    {
                        if (data.Group == -1)
                        {
                            data.Group = groupID++; 
                        }

                        data2.Group = data.Group;
                    }

                    panel2.ModelData.Faces[j] = data2; 
                }

                panel2.ModelData.Faces[i] = data;
            }

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();  
        }

        private void m_GroupByNormalFaceMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panel2.ModelData.Faces.Count; i++)
            {
                FaceData data = panel2.ModelData.Faces[i];

                data.Group = -1;

                panel2.ModelData.Faces[i] = data;
            }

            panel2.ModelData.CalcuateFaceNormals();

            Dictionary<int, List<int>> joinedGroups = new Dictionary<int, List<int>>(); 

            int groupID = 0;

            for (int i = 0; i < panel2.ModelData.Faces.Count; i++)
            {
                FaceData data = panel2.ModelData.Faces[i];

                Vector3 normal = data.Normal;

                for (int j = i + 1; j < panel2.ModelData.Faces.Count; j++)
                {
                    FaceData data2 = panel2.ModelData.Faces[j];

                    if (data.Contains(data2.V0) == false &&
                        data.Contains(data2.V1) == false &&
                        data.Contains(data2.V2) == false)
                    {
                        continue;
                    }

                    if (Vector3.Dot(data.Normal, data2.Normal) > 0.999f)
                    {                        
                        if (data.Group == -1)
                        {
                            data.Group = groupID++;
                        }

                        if (data2.Group != -1 && data2.Group != data.Group)
                        {     
                            int min = Math.Min(data2.Group, data.Group);
                            int max = Math.Max(data2.Group, data.Group);

                            if (joinedGroups.ContainsKey(min) == true) 
                            {
                                joinedGroups[min].Add(max); 
                            }
                            else 
                            {
                                joinedGroups.Add(min, new List<int>(new int[] { max }));
                            }
                        }

                        data2.Group = data.Group;
                    }

                    panel2.ModelData.Faces[j] = data2;
                }

                panel2.ModelData.Faces[i] = data;
            }

            int changedCount = 0;

            do
            {
                changedCount = 0;

                foreach (int key in joinedGroups.Keys)
                {
                    List<int> joined = joinedGroups[key];

                    for (int i = 0; i < panel2.ModelData.Faces.Count; i++)
                    {
                        FaceData data = panel2.ModelData.Faces[i];

                        if (joined.Contains(data.Group) == true)
                        {
                            data.Group = key;
                            changedCount++;
                        }

                        panel2.ModelData.Faces[i] = data;
                    }
                }
            }
            while (changedCount > 0);

            panel2.ModelData.CalcuateFaceNormals();

            panel2.Invalidate();  
        }

        private void mapEditorToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (mapEditorToolStripMenuItem.Checked == true)
            {
                m_MapEditor.Show();
            }
            else
            {
                m_MapEditor.Hide(); 
            }
        }

        private void decomposeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConvexHullDecomposer decomp = new ConvexHullDecomposer(panel2.ModelData);

            decomp.Decompose();

            panel2.Decomposed.Clear();

            panel2.Decomposed.AddRange(decomp.Results);

            panel2.Invalidate();  
        }

	}
}
