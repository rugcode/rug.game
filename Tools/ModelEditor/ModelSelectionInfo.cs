﻿using OpenTK;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ModelEditor
{
    public struct ModelSelectionInfo
    {
        public int Index;
        public ModelArrays Array;

        public Vector3[] InitialValue;

        public PointF MouseStart;
    }
}
