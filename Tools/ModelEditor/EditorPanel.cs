﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ModelEditor
{
	internal class EditorPanel : Panel
	{
		public enum ViewModes
		{
			Default, 
			Top, 
			Bottom, 
			Left, 
			Right, 
			Front, 
			Back,
			Iso,
		}

		public enum VisibleItemFlags: int
		{
			None = 0,
			Vertices = 1,
			WireFrame = 2, 
			Faces = 4, 
			Normals = 8, 
			Hull = 16, 
			Extras = 32, 
            Decomposed = 64, 
		}

		public enum EditorModes
		{
			Model,
			Hull,
			Extras, 
		}


		#region Private Members

		private bool m_EditEnabled = false;

		private Point m_DragStart;

		private bool m_Dragging;
		
		private bool m_Panning;
		private bool m_ForcePan;
		private Vector3 m_StartCenter = Vector3.Zero;
		private Vector3 m_Center = Vector3.Zero; 

		private bool m_Rotating = false;
		private Quaternion m_Rotation = Quaternion.Identity;
		private Quaternion m_DragStartRotation;

		private bool m_HasValidSelection = false;

		private ModelSelectionInfo m_Selection;
		private List<ModelSelectionInfo> m_SelectedItems = new List<ModelSelectionInfo>();

		private ViewModes m_SelectionView;
		private Rectangle m_SelectionViewRectangle;
        private Rectangle m_SelectionRectangle;
		private Pen m_GridPen;
		//private ModelData m_ModelData;

		private List<int> m_FaceBuilder = new List<int>();

		#endregion

		public event EventHandler SelectionChanged;
		private bool m_AddSelect;
        private bool m_RemoveSelect;

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ModelSelectionInfo Selection { get { return m_Selection; } }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public List<ModelSelectionInfo> SelectedItems { get { return m_SelectedItems; } }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool HasValidSelection { get { return m_HasValidSelection; } }

		[Browsable(false)]
		public ViewModes ViewMode { get; set; }

		[Browsable(false)]
		public VisibleItemFlags VisibleItems { get; set; }

		[Browsable(false)]
		public EditorModes EditorMode { get; set; }

		[Browsable(false)]
		public ModelData ModelData 
		{
			get
			{
				return StaticObjects.ModelData; 
			}
			set
			{
				m_Center = Vector3.Zero;
				m_StartCenter = Vector3.Zero;

				m_Rotation = Quaternion.Identity;

                StaticObjects.ModelData = value;
                StaticObjects.ModelData.CalcuateFaceNormals();
				
				InvalidateSelection();
			}
		}

        [Browsable(false)]
        public List<ModelData> Decomposed { get; private set; }  

		public float GridScale { get; set; }

		public new float Scale { get; set; }

		public float ExtraLength { get; set; }

		public float VertSize { get; set; }

		public bool SnapToGrid { get; set; }

		public EditorPanel()
		{
            Decomposed = new List<ModelData>(); 

			this.SetStyle(ControlStyles.Selectable, true);
			this.TabStop = true;

			ModelData = new ModelData();
			GridScale = 1f / 64f;
			Scale = 128f;
			VertSize = 5f; 
			ExtraLength = 64f;
			SnapToGrid = true;
			EditorMode = EditorModes.Model; 

			DoubleBuffered = true;

			this.MouseClick += new MouseEventHandler(EditorPanel_MouseClick);
			this.MouseDoubleClick += new MouseEventHandler(EditorPanel_MouseDoubleClick);
			this.MouseDown += new MouseEventHandler(EditorPanel_MouseDown);
			this.MouseMove += new MouseEventHandler(EditorPanel_MouseMove);
			this.MouseUp += new MouseEventHandler(EditorPanel_MouseUp);
			this.MouseWheel += new MouseEventHandler(EditorPanel_MouseWheel);

			m_GridPen = new Pen(Color.FromArgb(32, 32, 32), 1f);

            StaticObjects.OnInvalidate += StaticObjects_OnInvalidate;
		}

        void StaticObjects_OnInvalidate(object sender, EventArgs e)
        {
            Invalidate(); 
        }

		private void OnSelectionChanged()
		{
			if (SelectionChanged != null)
			{
				SelectionChanged(this, EventArgs.Empty); 
			}
		}

		private void ValidateSelection()
		{
			m_HasValidSelection = true;

			OnSelectionChanged();
		}

		public void InvalidateSelection()
		{
			m_HasValidSelection = false;

			SelectedItems.Clear();

			OnSelectionChanged();
		}

		private void MaintainSelection(ModelSelectionInfo selection, bool onlyAdd)
		{
			bool shouldAdd = true; 

			if (m_AddSelect == false ||
				HasValidSelection == false ||
				m_Selection.Array != selection.Array ||
				selection.Array == ModelArrays.Extras)
			{
				SelectedItems.Clear();
			}
			else
			{
				for (int i = SelectedItems.Count - 1; i >= 0; i--)
				{
					if (SelectedItems[i].Index == selection.Index) 
					{
                        if (onlyAdd == false)
                        {
                            SelectedItems.RemoveAt(i);
                        }

						shouldAdd = false;
					}
				}
			}

			if (shouldAdd == true)
			{
				SelectedItems.Add(selection);
				m_Selection = selection;
			}

			ValidateSelection();			
		}


		#region Input Events

		void EditorPanel_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				ViewModes mode;
				Rectangle rect;

				GetViewModeAndRect(e.Location, out mode, out rect);

				Matrix4 transform;
				GetMatrix(mode, rect, out transform);

				ModelSelectionInfo selection;

				if (FindObject(e.Location, mode, rect, transform, out selection) == false)
				{
					m_FaceBuilder.Clear();

					return;
				}				

				m_SelectionView = mode;
				m_SelectionViewRectangle = rect;

                MaintainSelection(selection, m_RemoveSelect);

				//m_Selection = selection;

				//ValidateSelection();

				if (m_Selection.Array != ModelArrays.Verts)
				{
					m_FaceBuilder.Clear();

					Invalidate();

					return;
				}

				if (EditorMode == EditorModes.Model)
				{					
					m_FaceBuilder.Add(m_Selection.Index);

					if (m_FaceBuilder.Count == 3)
					{
						if (m_FaceBuilder[0] == m_FaceBuilder[1] ||
							m_FaceBuilder[0] == m_FaceBuilder[1] ||
							m_FaceBuilder[1] == m_FaceBuilder[2])
						{
							m_FaceBuilder.Clear();

							Invalidate();

							return;
						}

						FaceData face = new FaceData()
						{
							Color = Color.White,
							V0 = m_FaceBuilder[0],
							V1 = m_FaceBuilder[1],
							V2 = m_FaceBuilder[2],
							Group = -1,
						};

						ModelData.Faces.Add(face);

						ModelData.CalcuateFaceNormals();

						m_FaceBuilder.Clear();

						Invalidate();
					}
				}
                else if (EditorMode == EditorModes.Hull &&
                        ModelData.HullVerts.Contains(m_Selection.Index) == false)
                {
                    ModelData.HullVerts.Add(m_Selection.Index);
                }
                /* 
                else
                {
                    m_SelectionRectangle = new Rectangle(e.Location, new System.Drawing.Size(1, 1));
                }
                */ 
			}
		}

		void EditorPanel_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Left)
			{
				if (m_EditEnabled == false)
				{
					return; 
				}

				ViewModes mode; 
				Rectangle rect;

				GetViewModeAndRect(e.Location, out mode, out rect);

				Matrix4 transform;
				GetMatrix(mode, rect, out transform);
				
				transform.Invert(); 

				Vector3 newVector = Vector3.Transform(new Vector3(e.Location.X, e.Location.Y, 0), transform);

				if (EditorMode == EditorModes.Extras)
				{
					ModelData.Extras.Add(new ExtraData()
					{
						Position = newVector, 
						Direction = Vector3.UnitY, 
						Type = 0
					}); 
				}
				else
				{
					ModelData.Verts.Add(newVector);
				}
			}
		}

		void EditorPanel_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Left && 
				m_Dragging == false && 
				m_Rotating == false && 
				m_Panning == false)
			{				
				ViewModes mode; 
				Rectangle rect;

				GetViewModeAndRect(e.Location, out mode, out rect);

				Matrix4 transform;
				GetMatrix(mode, rect, out transform);
				
				ModelSelectionInfo selection;
				
				m_SelectionView = mode;
				m_SelectionViewRectangle = rect;

				if (FindObject(e.Location, mode, rect, transform, out selection) == false)
				{
					m_DragStart = e.Location;

					if (mode == ViewModes.Iso && 
						m_ForcePan == false)
					{
						m_Rotating = true;						
						m_DragStartRotation = m_Rotation;						
					}
					else
					{
						m_Panning = true;						
						m_StartCenter = m_Center;						
					}

					//InvalidateSelection(); 

					Invalidate();

					return; 
				}


                MaintainSelection(selection, m_AddSelect && ((Control.ModifierKeys & Keys.Alt) != Keys.Alt));

				m_Dragging = true;

				//m_Selection = selection;

				//ValidateSelection();

				Invalidate();
			}
			else if (m_AddSelect == false)
			{
				InvalidateSelection(); 
			}

			/* 
			else if (e.Button == System.Windows.Forms.MouseButtons.Right && 
				m_Panning == false)
			{
			
			}
			*/
		}

		void EditorPanel_MouseUp(object sender, MouseEventArgs e)
		{
			m_Dragging = false;
			m_Panning = false; 
			m_Rotating = false; 

			Invalidate();
		}

		void EditorPanel_MouseMove(object sender, MouseEventArgs e)
		{
			if (m_Dragging == true)
			{
				ViewModes mode = m_SelectionView;
				Rectangle rect = m_SelectionViewRectangle;

				Matrix4 transform;
				GetMatrix(mode, rect, out transform);

				transform.Invert();

				Vector3 mouseStart = new Vector3(m_Selection.MouseStart.X, m_Selection.MouseStart.Y, 0);

				mouseStart = Vector3.Transform(mouseStart, transform);

				Vector3 mouseEnd = new Vector3(e.Location.X, e.Location.Y, 0);

				mouseEnd = Vector3.Transform(mouseEnd, transform);

				if (m_EditEnabled == true)
				{
                    bool snapOverride = true;

                    if (SelectedItems.Count == 1)
                    {
                        snapOverride = false; 
                    }

					foreach (ModelSelectionInfo info in SelectedItems)
					{
                        AdjustSelection(info, mode, mouseEnd - mouseStart, snapOverride);
					}
				}

				ModelData.CalcuateFaceNormals();

				Invalidate();
			}
			else if (m_Rotating == true)
			{
				m_Rotation = m_DragStartRotation;

				m_Rotation = Quaternion.Multiply(m_Rotation, Quaternion.FromAxisAngle(Vector3.UnitY, ((float)m_DragStart.X - (float)e.Location.X) * 0.01f));
				m_Rotation = Quaternion.Multiply(m_Rotation, Quaternion.FromAxisAngle(Vector3.UnitX, ((float)m_DragStart.Y - (float)e.Location.Y) * 0.01f));

				m_Rotation.Normalize(); 

				Invalidate();
			}
			else if (m_Panning == true)
			{
				ViewModes mode = m_SelectionView;
				Rectangle rect = m_SelectionViewRectangle;

				Matrix4 transform;
				GetMatrix(mode, rect, out transform);

				transform.Invert();

				Vector3 mouseStart = new Vector3(m_DragStart.X, m_DragStart.Y, 0);

				mouseStart = Vector3.Transform(mouseStart, transform);

				Vector3 mouseEnd = new Vector3(e.Location.X, e.Location.Y, 0);

				mouseEnd = Vector3.Transform(mouseEnd, transform);

				m_Center = m_StartCenter + (mouseEnd - mouseStart); 

				Invalidate();				
			}
		}

		void EditorPanel_MouseWheel(object sender, MouseEventArgs e)
		{
			if (e.Delta > 0)
			{
				Scale *= 2f;				
			}
			else
			{
				Scale *= 0.5f;
			}

			Invalidate(); 
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			this.Focus();
			base.OnMouseDown(e);
		}

		protected override bool IsInputKey(Keys keyData)
		{
			if (keyData == Keys.Up || keyData == Keys.Down) return true;
			if (keyData == Keys.Left || keyData == Keys.Right) return true;
			return base.IsInputKey(keyData);
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			if (e.KeyCode == Keys.Space)
			{
				SnapToGrid = false;
			}

			if (e.KeyCode == Keys.ShiftKey)
			{
				m_AddSelect = true;
			}

            if (e.KeyCode == Keys.Alt)
            {
                m_RemoveSelect = true;
            }

			if (e.KeyCode == Keys.ControlKey)
			{
				m_ForcePan = true;
			}

			if (e.KeyCode == Keys.Escape)
			{
				m_EditEnabled = !m_EditEnabled;

				Invalidate(); 
			}

			if (e.KeyCode == Keys.F)
			{
				FlipFace();
			}

			if (e.KeyCode == Keys.G)
			{
				GroupFaces();
			}

			if (e.KeyCode == Keys.H)
			{
				UngroupFaces();
			}

			if (e.KeyCode == Keys.P)
			{
				ConformToPlane(); 
			}

			if (e.KeyCode == Keys.C)
			{
				m_Center = Vector3.Zero;
				m_StartCenter = Vector3.Zero;
				m_Rotation = Quaternion.Identity; 

				Invalidate(); 
			}

			if (e.KeyCode == Keys.Back || 
				e.KeyCode == Keys.Delete)
			{
				switch (EditorMode)
				{
					case EditorModes.Model:
						DeleteFace();
						DeleteVertex(); 
						break;
					case EditorModes.Hull:
						DeleteHullVertex();
						break;
					case EditorModes.Extras:
						DeleteExtra(); 
						break;
					default:
						break;
				}
			}
		}

		protected override void OnKeyUp(KeyEventArgs e)
		{
			base.OnKeyUp(e);

			if (e.KeyCode == Keys.Space)
			{
				SnapToGrid = true;
			}
			
			if (e.KeyCode == Keys.ShiftKey)
			{
				m_AddSelect = false;
			}			

			if (e.KeyCode == Keys.ControlKey)
			{
				m_ForcePan = false;
			}

            if (e.KeyCode == Keys.Alt)
            {
                m_RemoveSelect = false;
            }
		}

		protected override void OnKeyPress(System.Windows.Forms.KeyPressEventArgs e)
		{
			base.OnKeyPress(e);
		}

		protected override void OnEnter(EventArgs e)
		{
			this.Invalidate();
			base.OnEnter(e);
		}

		protected override void OnLeave(EventArgs e)
		{
			this.Invalidate();
			base.OnLeave(e);
		}

		#endregion

		#region Paint Event

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			
			e.Graphics.Clear(Color.Black);

			try
			{
				switch (ViewMode)
				{
					case ViewModes.Default:
						int sizeX = this.ClientRectangle.Width / 2;
						int sizeY = this.ClientRectangle.Height / 2;

						RenderView(e.Graphics, ViewModes.Top, new Rectangle(0, 0, sizeX, sizeY));
						RenderView(e.Graphics, ViewModes.Front, new Rectangle(0, sizeY, sizeX, sizeY));
						RenderView(e.Graphics, ViewModes.Left, new Rectangle(sizeX, sizeY, sizeX, sizeY));
						RenderView(e.Graphics, ViewModes.Iso, new Rectangle(sizeX, 0, sizeX, sizeY));
						break;
					default:
						RenderView(e.Graphics, ViewMode, new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));
						break;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());	
			}
		}

		#endregion

		#region Views And Rendering

		private void GetViewModeAndRect(Point location, out ViewModes mode, out Rectangle rect)
		{
			switch (ViewMode)
			{
				case ViewModes.Default:
					int sizeX = this.ClientRectangle.Width / 2;
					int sizeY = this.ClientRectangle.Height / 2;

					rect = new Rectangle(0, 0, sizeX, sizeY);

					if (rect.Contains(location) == true)
					{
						mode = ViewModes.Top;
						return; 
					}

					rect = new Rectangle(0, sizeY, sizeX, sizeY); 

					if (rect.Contains(location) == true)
					{
						mode = ViewModes.Front;
						return; 
					}

					rect = new Rectangle(sizeX, sizeY, sizeX, sizeY); 

					if (rect.Contains(location) == true)
					{
						mode = ViewModes.Left;
						return; 
					}

					rect = new Rectangle(sizeX, 0, sizeX, sizeY); 
					if (rect.Contains(location) == true)
					{
						mode = ViewModes.Iso;
						return; 
					}

					throw new Exception("Invalid point"); 
				default:
					mode = ViewMode;
					rect = new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height);
					break;
			}
		}

		private void GetMatrix(ViewModes mode, Rectangle rect, out Matrix4 transform)
		{
            /* 
			switch (mode)
			{
				case ViewModes.Top:
					transform = Matrix4.Identity;
					break;
				case ViewModes.Bottom:
					transform = Matrix4.CreateRotationY(MathHelper.Pi);
					break;
				case ViewModes.Left:					
					transform = Matrix4.CreateRotationX(MathHelper.Pi * 0.5f) * Matrix4.CreateRotationY(MathHelper.Pi * 0.5f);
					break;
				case ViewModes.Right:
					transform = Matrix4.CreateRotationX(MathHelper.Pi * 0.5f) * Matrix4.CreateRotationY(-MathHelper.Pi * 0.5f);
					break;
				case ViewModes.Front:
					transform = Matrix4.CreateRotationX(MathHelper.Pi * 0.5f);
					break;
				case ViewModes.Back:
					transform = Matrix4.CreateRotationX(-MathHelper.Pi * 0.5f);
					break;
				case ViewModes.Iso:
					transform = Matrix4.CreateFromQuaternion(m_Rotation);
					break;
				default:
					transform = Matrix4.Identity;
					break;
			}
            */

            switch (mode)
            {
                case ViewModes.Top:
                    transform = Matrix4.CreateRotationX(-MathHelper.PiOver2) * Matrix4.CreateRotationY(MathHelper.Pi);                    
                    break;
                case ViewModes.Bottom:
                    transform = Matrix4.CreateRotationX(MathHelper.PiOver2) * Matrix4.CreateRotationY(MathHelper.Pi); 
                    break;
                case ViewModes.Left:
                    //transform = Matrix4.CreateRotationX(MathHelper.PiOver2) * Matrix4.CreateRotationY(MathHelper.PiOver2);
                    transform = Matrix4.CreateRotationY(MathHelper.PiOver2) * Matrix4.CreateRotationZ(MathHelper.Pi);   
                    break;
                case ViewModes.Right:
                    transform = Matrix4.CreateRotationY(-MathHelper.PiOver2) * Matrix4.CreateRotationZ(MathHelper.Pi);   
                    break;
                case ViewModes.Front:
                    transform = Matrix4.CreateRotationZ(MathHelper.Pi);
                    break;
                case ViewModes.Back:
                    transform = Matrix4.CreateRotationY(MathHelper.Pi) * Matrix4.CreateRotationZ(MathHelper.Pi);  
                    break;
                case ViewModes.Iso:
                    transform = Matrix4.CreateFromQuaternion(m_Rotation) * Matrix4.CreateRotationZ(MathHelper.Pi);
                    break;
                default:
                    transform = Matrix4.Identity;
                    break;
            }


			transform = Matrix4.CreateTranslation(m_Center) * transform; 
			
			transform *= Matrix4.CreateScale(Scale); 

			transform *= Matrix4.CreateTranslation(new Vector3((float)rect.Width * 0.5f + rect.X, (float)rect.Height * 0.5f + rect.Y, 0f));
		}

		private bool FindObject(PointF mouse, ViewModes mode, Rectangle rect, Matrix4 transform, out ModelSelectionInfo selection)
		{
			float hVertSize = VertSize * 0.5f; 

			selection = new ModelSelectionInfo(); 

			if ((VisibleItems & VisibleItemFlags.Vertices) == VisibleItemFlags.Vertices)
			{
				for (int i = 0; i < ModelData.Verts.Count; i++) 
				{
					Vector3 point = ModelData.Verts[i];

					Vector3 pt3 = Vector3.Transform(point, transform);

					PointF pt2 = new PointF(pt3.X, pt3.Y);

					if (new RectangleF(pt2.X - hVertSize, pt2.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
					{
						selection.Array = ModelArrays.Verts;
						selection.Index = i;

						selection.InitialValue = new Vector3[] { point };
						selection.MouseStart = mouse;

						return true; 
					}
				}
			}

			if ((VisibleItems & VisibleItemFlags.Faces) == VisibleItemFlags.Faces)
			{
				for (int i = 0; i < ModelData.Faces.Count; i++)
				{
					FaceData face = ModelData.Faces[i];

					Vector3 pt0 = Vector3.Transform(ModelData.Verts[face.V0], transform);
					Vector3 pt1 = Vector3.Transform(ModelData.Verts[face.V1], transform);
					Vector3 pt2 = Vector3.Transform(ModelData.Verts[face.V2], transform);

					Vector3 center = (pt0 + pt1 + pt2) / 3f;

					if (new RectangleF(center.X - hVertSize, center.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
					{
						selection.Array = ModelArrays.Faces;
						selection.Index = i;

						pt0 = ModelData.Verts[face.V0];
						pt1 = ModelData.Verts[face.V1];
						pt2 = ModelData.Verts[face.V2];

						center = (pt0 + pt1 + pt2) / 3f;

						selection.InitialValue = new Vector3[] { pt0, pt1, pt2, center };
						selection.MouseStart = mouse;

						return true;
					}
				}
			}
	
			/* 
			if ((VisibleItems & VisibleItemFlags.Hull) == VisibleItemFlags.Hull)
			{
				List<PointF> linePoints = new List<PointF>();

				for (int i = 0; i < ModelData.HullVerts.Count; i++) 
				{
					Vector3 point = ModelData.Verts[ModelData.HullVerts[i]];

					Vector3 pt3 = Vector3.Transform(point, transform);

					PointF pt = new PointF(pt3.X, pt3.Y);

					if (new RectangleF(pt.X - hVertSize, pt.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
					{
						selection.Array = ModelArrays.HullVerts;
						selection.Index = i;

						selection.InitialValue = new Vector3[] { point } ;
						selection.MouseStart = mouse;

						return true;
					}
				}
			}
			*/

			if ((VisibleItems & VisibleItemFlags.Extras) == VisibleItemFlags.Extras)
			{
				for (int i = 0; i < ModelData.Extras.Count; i++) 				
				{
					ExtraData extra = ModelData.Extras[i]; 

					Vector3 pos = Vector3.Transform(extra.Position, transform);

					Vector3 dir = Vector3.Transform(extra.Position + (extra.Direction * ExtraLength), transform);

					if (new RectangleF(pos.X - hVertSize, pos.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
					{
						selection.Array = ModelArrays.Extras;
						selection.Index = i * 2;

						selection.InitialValue = new Vector3[] { extra.Position };
						selection.MouseStart = mouse;

						return true;
					}

					if (new RectangleF(dir.X - hVertSize, dir.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
					{
						selection.Array = ModelArrays.Extras;
						selection.Index = i * 2 + 1;

						selection.InitialValue = new Vector3[] { extra.Direction };
						selection.MouseStart = mouse;

						return true;
					}
				}
			}

			return false; 
		}

        private void AdjustSelection(ModelSelectionInfo selection, ViewModes mode, Vector3 mouseOff, bool @override)
		{
            Vector3 mouseResolve = DoSnap(mouseOff, !@override); 

			switch (selection.Array)
			{
				case ModelArrays.Verts:
                    ModelData.Verts[selection.Index] = DoSnap(selection.InitialValue[0] + mouseResolve, @override);
					break;
				case ModelArrays.Faces:
					FaceData face = ModelData.Faces[selection.Index];

                    ModelData.Verts[face.V0] = DoSnap(selection.InitialValue[0] + mouseResolve, @override);
                    ModelData.Verts[face.V1] = DoSnap(selection.InitialValue[1] + mouseResolve, @override);
                    ModelData.Verts[face.V2] = DoSnap(selection.InitialValue[2] + mouseResolve, @override);

					break;
				case ModelArrays.Extras:				
					int index = (selection.Index - (selection.Index % 2)) / 2;

					ExtraData data = ModelData.Extras[index];

					if (selection.Index % 2 == 0)
					{
                        data.Position = DoSnap(selection.InitialValue[0] + mouseResolve, @override);
					}
					else
					{
                        data.Direction = DoSnap(selection.InitialValue[0] + mouseResolve, @override);

						if (data.Direction == Vector3.Zero)
						{
							data.Direction = Vector3.UnitY;
						}

                        if (data.Type != ExtraTypes.Volume)
                        {
                            data.Direction.Normalize();
                        }
					}

					ModelData.Extras[index] = data; 
					break;
				default:
					break;
			}
		}

		private Vector3 DoSnap(Vector3 value, bool @override)
		{
            if (SnapToGrid == true && @override == false)
			{
				value *= GridScale;

				value.X = (float)Math.Round(value.X);
				value.Y = (float)Math.Round(value.Y);
				value.Z = (float)Math.Round(value.Z);

				value *= 1f / GridScale; 				
			}

			return value; 
		}

		private FaceGroup GetTempFaceGroup()
		{
			FaceGroup group = new FaceGroup();

			foreach (ModelSelectionInfo info in SelectedItems)
			{
				group.Faces.Add(ModelData.Faces[info.Index]);
			}

			Vector3 center = Vector3.Zero;
			Vector3 normal = Vector3.Zero;

			foreach (FaceData face in group.Faces)
			{
				Vector3 v0 = ModelData.Verts[face.V0];
				Vector3 v1 = ModelData.Verts[face.V1];
				Vector3 v2 = ModelData.Verts[face.V2];

				Vector3 faceCenter = (v0 + v1 + v2) / 3f;

				Vector3 faceNormal = face.Normal;

				faceNormal.Normalize();

				center += faceCenter;
				normal += faceNormal;
			}

			center /= group.Faces.Count;
			normal /= group.Faces.Count;

			normal.Normalize();

			group.Center = center;
			group.Normal = normal;

			return group;
		}

		private void RenderView(Graphics g, ViewModes mode, Rectangle rect)
		{
			List<int> selectionIndices = new List<int>();
			ModelArrays selectionArray = Selection.Array;
			List<int> selectedGroups = new List<int>();

			foreach (ModelSelectionInfo info in SelectedItems)
			{
				selectionIndices.Add(info.Index);

				if (info.Array == ModelArrays.Faces)
				{
					int group = ModelData.Faces[info.Index].Group;

					if (group != -1 &&
						selectedGroups.Contains(group) == false)
					{
						selectedGroups.Add(group); 
					}
				}
			}

			float hVertSize = VertSize * 0.5f;
			float VertSize3 = VertSize * 3f; 

			g.SetClip(rect);

			Matrix4 transform;
			GetMatrix(mode, rect, out transform);

			{
				Vector3 xMin = Vector3.Transform(Vector3.UnitX * -1000f, transform);
				Vector3 xMax = Vector3.Transform(Vector3.UnitX * 1000f, transform);

				g.DrawLine(m_GridPen, new PointF(xMin.X, xMin.Y), new PointF(xMax.X, xMax.Y));

				Vector3 yMin = Vector3.Transform(Vector3.UnitY * -1000f, transform);
				Vector3 yMax = Vector3.Transform(Vector3.UnitY * 1000f, transform);

				g.DrawLine(m_GridPen, new PointF(yMin.X, yMin.Y), new PointF(yMax.X, yMax.Y));

				Vector3 zMin = Vector3.Transform(Vector3.UnitZ * -1000f, transform);
				Vector3 zMax = Vector3.Transform(Vector3.UnitZ * 1000f, transform);
			
				g.DrawLine(m_GridPen, new PointF(zMin.X, zMin.Y), new PointF(zMax.X, zMax.Y));
			}

			for (int i = 1; i < 8; i++)
			{
				Vector3 TopLeft = Vector3.Transform(new Vector3( -i, 0, -i), transform);
				Vector3 TopRight = Vector3.Transform(new Vector3( i, 0, -i), transform);

				Vector3 BottomLeft = Vector3.Transform(new Vector3( -i, 0, i), transform);
				Vector3 BottomRight = Vector3.Transform(new Vector3( i, 0, i), transform);

				g.DrawLines(m_GridPen, new PointF[] 
				{					
					new PointF(TopLeft.X, TopLeft.Y), 
					new PointF(TopRight.X, TopRight.Y), 
					new PointF(BottomRight.X, BottomRight.Y), 
					new PointF(BottomLeft.X, BottomLeft.Y), 
					new PointF(TopLeft.X, TopLeft.Y), 
				});				
			}

            for (int i = 1; i < 8; i++)
            {
                Vector3 TopLeft = Vector3.Transform(new Vector3(0, -i, -i), transform);
                Vector3 TopRight = Vector3.Transform(new Vector3(0, i, - i), transform);

                Vector3 BottomLeft = Vector3.Transform(new Vector3(0, -i, i), transform);
                Vector3 BottomRight = Vector3.Transform(new Vector3(0, i, i), transform);

                g.DrawLines(m_GridPen, new PointF[] 
				{					
					new PointF(TopLeft.X, TopLeft.Y), 
					new PointF(TopRight.X, TopRight.Y), 
					new PointF(BottomRight.X, BottomRight.Y), 
					new PointF(BottomLeft.X, BottomLeft.Y), 
					new PointF(TopLeft.X, TopLeft.Y), 
				});
            }


            for (int i = 1; i < 8; i++)
            {
                Vector3 TopLeft = Vector3.Transform(new Vector3(-i, -i, 0), transform);
                Vector3 TopRight = Vector3.Transform(new Vector3(i, -i, 0), transform);

                Vector3 BottomLeft = Vector3.Transform(new Vector3(-i, i, 0), transform);
                Vector3 BottomRight = Vector3.Transform(new Vector3(i, i, 0), transform);

                g.DrawLines(m_GridPen, new PointF[] 
				{					
					new PointF(TopLeft.X, TopLeft.Y), 
					new PointF(TopRight.X, TopRight.Y), 
					new PointF(BottomRight.X, BottomRight.Y), 
					new PointF(BottomLeft.X, BottomLeft.Y), 
					new PointF(TopLeft.X, TopLeft.Y), 
				});
            }

			bool drawAxis = false;
			Vector3 axisCenter = Vector3.Zero; 

			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Verts)
			{
				axisCenter = ModelData.Verts[Selection.Index];
				drawAxis = true; 
			}
			else if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Extras)
			{
				int index = (Selection.Index - (Selection.Index % 2)) / 2;

				axisCenter = ModelData.Extras[index].Position;
				drawAxis = true;
			}			

			if (drawAxis == true) 
			{
				Vector3 yAxis0 = Vector3.Transform(new Vector3(-10f, axisCenter.Y, axisCenter.Z), transform);
				Vector3 yAxis1 = Vector3.Transform(new Vector3(10f, axisCenter.Y, axisCenter.Z), transform);

				Vector3 zAxis0 = Vector3.Transform(new Vector3(axisCenter.X, axisCenter.Y, -10f), transform);
				Vector3 zAxis1 = Vector3.Transform(new Vector3(axisCenter.X, axisCenter.Y,  10f), transform);

				Vector3 zAxis2 = Vector3.Transform(new Vector3(-axisCenter.X, axisCenter.Y, -10f), transform);
				Vector3 zAxis3 = Vector3.Transform(new Vector3(-axisCenter.X, axisCenter.Y, 10f), transform);

				Vector3 xAxis0 = Vector3.Transform(new Vector3(axisCenter.X, -10f, axisCenter.Z), transform);
				Vector3 xAxis1 = Vector3.Transform(new Vector3(axisCenter.X, 10f, axisCenter.Z), transform);

				Vector3 xAxis2 = Vector3.Transform(new Vector3(-axisCenter.X, -10f, axisCenter.Z), transform);
				Vector3 xAxis3 = Vector3.Transform(new Vector3(-axisCenter.X, 10f, axisCenter.Z), transform);				

				g.DrawLine(m_GridPen, new PointF(yAxis0.X, yAxis0.Y), new PointF(yAxis1.X, yAxis1.Y));

				g.DrawLine(m_GridPen, new PointF(zAxis0.X, zAxis0.Y), new PointF(zAxis1.X, zAxis1.Y));
				g.DrawLine(m_GridPen, new PointF(zAxis2.X, zAxis2.Y), new PointF(zAxis3.X, zAxis3.Y));
				
				g.DrawLine(m_GridPen, new PointF(xAxis0.X, xAxis0.Y), new PointF(xAxis1.X, xAxis1.Y));				
				g.DrawLine(m_GridPen, new PointF(xAxis2.X, xAxis2.Y), new PointF(xAxis3.X, xAxis3.Y));
			}

			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Faces)
			{	
				using (Brush groupBrush = new SolidBrush(Color.FromArgb(128, Color.Orange)))
				using (Pen groupPen = new Pen(groupBrush, 2f))
				{
					FaceGroup tempGroup = GetTempFaceGroup();

					Vector3 groupCenter = Vector3.Transform(tempGroup.Center, transform);
					Vector3 groupNormal = Vector3.TransformNormal(tempGroup.Normal, transform);

					groupNormal.Normalize();

					groupNormal *= 0.125f * Scale;

					g.FillRectangle(groupBrush, new RectangleF(groupCenter.X - VertSize, groupCenter.Y - VertSize, VertSize * 2f, VertSize * 2f));
					g.DrawLine(groupPen, new PointF(groupCenter.X, groupCenter.Y), new PointF(groupCenter.X + groupNormal.X, groupCenter.Y + groupNormal.Y));	
				}
			}

			if (m_EditEnabled == true)
			{
				g.DrawRectangle(Pens.White, rect);
			}
			else
			{
				using (Pen disablePen = new Pen(Brushes.Red, 2))
				{
					g.DrawRectangle(disablePen, rect);
				}
			}

			g.DrawString(mode.ToString(), this.Parent.Font, Brushes.White, new Point(rect.X + 10, rect.Y + 10));

			if ((VisibleItems & VisibleItemFlags.Vertices) == VisibleItemFlags.Vertices)
			{
				for (int i = 0; i < ModelData.Verts.Count; i++)
				{
					Vector3 point = ModelData.Verts[i];

					Brush brush;

					if (HasValidSelection == true &&
						selectionArray == ModelArrays.Verts &&
						selectionIndices.Contains(i) == true)
					{
						if (Selection.Index == i)
						{
							brush = Brushes.Magenta;
						}
						else
						{
							brush = Brushes.DarkMagenta;
						}
					}
					else
					{
						brush = Brushes.LightGray;
					}

					Vector3 pt3 = Vector3.Transform(point, transform);

					PointF pt2 = new PointF(pt3.X, pt3.Y);

					g.FillRectangle(brush, new RectangleF(pt2.X - hVertSize, pt2.Y - hVertSize, VertSize, VertSize));
				}
			}

            if ((VisibleItems & VisibleItemFlags.Decomposed) == VisibleItemFlags.Decomposed)
            {
                //Pen outlinePen = Pens.DarkCyan;

                int penIndex = 0;

                foreach (ModelData decomp in Decomposed)
                {
                    Pen outlinePen = m_ClusterPens[penIndex++ % m_ClusterPens.Length];

                    for (int i = 0; i < decomp.Faces.Count; i++)
                    {
                        FaceData face = decomp.Faces[i];

                        Vector3 pt0 = Vector3.Transform(decomp.Verts[face.V0], transform);
                        Vector3 pt1 = Vector3.Transform(decomp.Verts[face.V1], transform);
                        Vector3 pt2 = Vector3.Transform(decomp.Verts[face.V2], transform);

                        g.DrawLines(outlinePen, new PointF[] 
						{ 
							new PointF(pt0.X, pt0.Y), 
							new PointF(pt1.X, pt1.Y), 
							new PointF(pt2.X, pt2.Y), 
							new PointF(pt0.X, pt0.Y), 
						});
                    }
                }
            }

			if ((VisibleItems & VisibleItemFlags.Faces) == VisibleItemFlags.Faces ||
				(VisibleItems & VisibleItemFlags.WireFrame) == VisibleItemFlags.WireFrame)
			{
				using (Brush groupBrush = new SolidBrush(Color.FromArgb(128, Color.DarkGreen)))
				{
					bool showFaces = (VisibleItems & VisibleItemFlags.Faces) == VisibleItemFlags.Faces;
					bool showWireFrame = (VisibleItems & VisibleItemFlags.WireFrame) == VisibleItemFlags.WireFrame; 
					bool showNormals = (VisibleItems & VisibleItemFlags.Normals) == VisibleItemFlags.Normals; 

					for (int i = 0; i < ModelData.Faces.Count; i++)
					{
						Pen outlinePen = Pens.White;
						FaceData face = ModelData.Faces[i];

						using (Brush brush = new SolidBrush((Color)face.Color))
						using (Pen pen = new Pen((Color)face.Color))
						{
							if (face.Color == Color4.Black)
							{
								pen.Color = Color.White;
								pen.DashPattern = new float[] { 3f, 8f }; 
							}						

							Vector3 pt0 = Vector3.Transform(ModelData.Verts[face.V0], transform);
							Vector3 pt1 = Vector3.Transform(ModelData.Verts[face.V1], transform);
							Vector3 pt2 = Vector3.Transform(ModelData.Verts[face.V2], transform);

							if (showWireFrame == true)
							{
								g.DrawLines(pen, new PointF[] 
								{ 
									new PointF(pt0.X, pt0.Y), 
									new PointF(pt1.X, pt1.Y), 
									new PointF(pt2.X, pt2.Y), 
									new PointF(pt0.X, pt0.Y), 
								});
							}
						
							Vector3 center = (pt0 + pt1 + pt2) / 3f;

							if (showFaces == true)
							{
								RectangleF bounds = new RectangleF(center.X - hVertSize, center.Y - hVertSize, VertSize, VertSize);

								g.FillRectangle(brush, bounds);
								g.DrawRectangle(outlinePen, bounds.X, bounds.Y, bounds.Width, bounds.Height);

								if (selectedGroups.Contains(face.Group) == true)
								{
									g.FillPolygon(groupBrush, new PointF[] 
									{ 
										new PointF(pt0.X, pt0.Y), 
										new PointF(pt1.X, pt1.Y), 
										new PointF(pt2.X, pt2.Y), 
										//new PointF(pt0.X, pt0.Y), 
									});
								}
							}

							if (showNormals == true)
							{
								Vector3 normal = Vector3.TransformNormal(face.Normal, transform);

								normal.Normalize();

								normal = center + (normal * 0.125f * Scale);

								g.DrawLine(outlinePen, new PointF(center.X, center.Y), new PointF(normal.X, normal.Y));
							}
						}				
					}

					if (HasValidSelection == true &&
						Selection.Array == ModelArrays.Faces)
					{
						foreach (int index in selectionIndices)
						{
							Pen outlinePen;
							if (Selection.Index == index)
							{
								outlinePen = Pens.Magenta;
							}
							else
							{
								outlinePen = Pens.DarkMagenta;
							}

							FaceData face = ModelData.Faces[index];

							using (Brush brush = new SolidBrush((Color)face.Color))
							{
								Vector3 pt0 = Vector3.Transform(ModelData.Verts[face.V0], transform);
								Vector3 pt1 = Vector3.Transform(ModelData.Verts[face.V1], transform);
								Vector3 pt2 = Vector3.Transform(ModelData.Verts[face.V2], transform);

								if (showWireFrame == true)
								{
									g.DrawLines(outlinePen, new PointF[] 
								{ 
									new PointF(pt0.X, pt0.Y), 
									new PointF(pt1.X, pt1.Y), 
									new PointF(pt2.X, pt2.Y), 
									new PointF(pt0.X, pt0.Y), 
								});
								}

								Vector3 center = (pt0 + pt1 + pt2) / 3f;

								if (showFaces == true)
								{
									RectangleF bounds = new RectangleF(center.X - hVertSize, center.Y - hVertSize, VertSize, VertSize);

									g.FillRectangle(brush, bounds);
									g.DrawRectangle(outlinePen, bounds.X, bounds.Y, bounds.Width, bounds.Height);
								}

								if (showNormals == true)
								{
									Vector3 normal = Vector3.TransformNormal(face.Normal, transform);

									normal.Normalize();

									normal = center + (normal * 0.125f * Scale);

									g.DrawLine(outlinePen, new PointF(center.X, center.Y), new PointF(normal.X, normal.Y));
								}
							}
						}
					}
				}
			}

			if ((VisibleItems & VisibleItemFlags.Hull) == VisibleItemFlags.Hull)
			{
				List<PointF> linePoints = new List<PointF>();

				for (int i = 0; i < ModelData.HullVerts.Count; i++)
				{
					Vector3 vert = ModelData.Verts[ModelData.HullVerts[i]];

					Brush brush;

					if (HasValidSelection == true &&
						selectionArray == ModelArrays.HullVerts &&
						selectionIndices.Contains(i) == true)
					{
						if (Selection.Index == i)
						{
							brush = Brushes.Magenta;
						}
						else
						{
							brush = Brushes.DarkMagenta;
						}
					}
					else
					{
						brush = Brushes.DarkCyan;
					}

					Vector3 pt3 = Vector3.Transform(vert, transform);

					PointF pt = new PointF(pt3.X, pt3.Y);

					linePoints.Add(pt);

					g.FillRectangle(brush, new RectangleF(pt.X - hVertSize, pt.Y - hVertSize, VertSize, VertSize));
				}

				if (linePoints.Count > 0)
				{
					linePoints.Add(linePoints[0]);

					g.DrawLines(Pens.DarkCyan, linePoints.ToArray());
				}
			}

			if ((VisibleItems & VisibleItemFlags.Extras) == VisibleItemFlags.Extras)
			{
				for (int i = 0; i < ModelData.Extras.Count; i++)
				{
					ExtraData extra = ModelData.Extras[i];

					Brush brush;
					Pen pen;

					/* 
					if (HasValidSelection == true &&
						selectionArray == ModelArrays.HullVerts &&
						selectionIndices.Contains(i) == true)
					{
						if (Selection.Index == i)
						{
							brush = Brushes.Magenta;
						}
						else
						{
							brush = Brushes.DarkMagenta;
						}
					}
					else
					{
						brush = Brushes.DarkCyan;
					}
					*/ 

					if (HasValidSelection == true &&
						Selection.Array == ModelArrays.Extras &&
						((Selection.Index - (Selection.Index % 2)) / 2) == i)
					{
						brush = Brushes.Magenta;
						pen = Pens.Magenta;
					}
					else
					{
						brush = Brushes.LightGreen;
						pen = Pens.LightGreen;
					}

                    if (extra.Type != ExtraTypes.Volume)
                    {
                        Vector3 pos = Vector3.Transform(extra.Position, transform);

                        Vector3 dir = Vector3.Transform(extra.Position + (extra.Direction * ExtraLength), transform);

                        g.FillRectangle(brush, new RectangleF(pos.X - hVertSize, pos.Y - hVertSize, VertSize, VertSize));

                        g.FillRectangle(brush, new RectangleF(dir.X - hVertSize, dir.Y - hVertSize, VertSize, VertSize));

                        g.DrawLine(pen, new PointF(pos.X, pos.Y), new PointF(dir.X, dir.Y));

                        g.DrawLine(pen, new PointF(pos.X - VertSize3, pos.Y), new PointF(pos.X + VertSize3, pos.Y));
                        g.DrawLine(pen, new PointF(pos.X, pos.Y - VertSize3), new PointF(pos.X, pos.Y + VertSize3));
                    }
                    else
                    {
                        Vector3 pos = Vector3.Transform(extra.Position, transform);

                        Vector3 dir = Vector3.Transform(extra.Position + (extra.Direction), transform);

                        g.FillRectangle(brush, new RectangleF(pos.X - hVertSize, pos.Y - hVertSize, VertSize, VertSize));

                        g.FillRectangle(brush, new RectangleF(dir.X - hVertSize, dir.Y - hVertSize, VertSize, VertSize));

                        Vector3 v0 = Vector3.Transform(extra.Position + new Vector3(extra.Direction.X, -extra.Direction.Y, -extra.Direction.Z), transform);
                        Vector3 v1 = Vector3.Transform(extra.Position + new Vector3(extra.Direction.X, -extra.Direction.Y, extra.Direction.Z), transform);
                        Vector3 v2 = Vector3.Transform(extra.Position + new Vector3(extra.Direction.X, extra.Direction.Y, extra.Direction.Z), transform);
                        Vector3 v3 = Vector3.Transform(extra.Position + new Vector3(extra.Direction.X, extra.Direction.Y, -extra.Direction.Z), transform);
                        
                        Vector3 v4 = Vector3.Transform(extra.Position + new Vector3(-extra.Direction.X, -extra.Direction.Y, -extra.Direction.Z), transform);
                        Vector3 v5 = Vector3.Transform(extra.Position + new Vector3(-extra.Direction.X, -extra.Direction.Y, extra.Direction.Z), transform);                        
                        Vector3 v6 = Vector3.Transform(extra.Position + new Vector3(-extra.Direction.X, extra.Direction.Y, extra.Direction.Z), transform);
                        Vector3 v7 = Vector3.Transform(extra.Position + new Vector3(-extra.Direction.X, extra.Direction.Y, -extra.Direction.Z), transform);

                        g.DrawLine(pen, new PointF(v0.X, v0.Y), new PointF(v1.X, v1.Y));
                        g.DrawLine(pen, new PointF(v1.X, v1.Y), new PointF(v2.X, v2.Y));
                        g.DrawLine(pen, new PointF(v2.X, v2.Y), new PointF(v3.X, v3.Y));
                        g.DrawLine(pen, new PointF(v3.X, v3.Y), new PointF(v0.X, v0.Y));

                        g.DrawLine(pen, new PointF(v4.X, v4.Y), new PointF(v5.X, v5.Y));
                        g.DrawLine(pen, new PointF(v5.X, v5.Y), new PointF(v6.X, v6.Y));
                        g.DrawLine(pen, new PointF(v6.X, v6.Y), new PointF(v7.X, v7.Y));
                        g.DrawLine(pen, new PointF(v7.X, v7.Y), new PointF(v4.X, v4.Y));

                        g.DrawLine(pen, new PointF(v0.X, v0.Y), new PointF(v4.X, v4.Y));
                        g.DrawLine(pen, new PointF(v1.X, v1.Y), new PointF(v5.X, v5.Y));
                        g.DrawLine(pen, new PointF(v2.X, v2.Y), new PointF(v6.X, v6.Y));
                        g.DrawLine(pen, new PointF(v3.X, v3.Y), new PointF(v7.X, v7.Y));
                    }
				}
			}

			if ((VisibleItems & VisibleItemFlags.Vertices) == VisibleItemFlags.Vertices)
			{
				for (int i = 0; i < m_FaceBuilder.Count; i++)
				{
					Vector3 point = ModelData.Verts[m_FaceBuilder[i]];

					Brush brush;					
					
					brush = Brushes.Yellow;

					Vector3 pt3 = Vector3.Transform(point, transform);

					PointF pt2 = new PointF(pt3.X, pt3.Y);

					g.FillRectangle(brush, new RectangleF(pt2.X - hVertSize, pt2.Y - hVertSize, VertSize, VertSize));
				}
			}
		}

        private Pen[] m_ClusterPens = new Pen[]
        {
            Pens.DarkCyan, 
            Pens.Maroon, 
            Pens.LawnGreen,
            Pens.Honeydew, 
            Pens.HotPink, 
            Pens.Orange,
            Pens.Aquamarine, 
            Pens.Indigo,
        };

		#endregion

		private void ConformToPlane(int vertIndex, Vector3 dir, ref Vector3 center)
		{
			Vector3 point = ModelData.Verts[vertIndex];

			Vector3 result;

			float dist = Vector3.Dot(dir, point - center);

			result = point - (dir * dist);
			
			ModelData.Verts[vertIndex] = result;
		}

		public void ConformToPlane()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Faces)
			{		
				FaceGroup group = GetTempFaceGroup();

				foreach (FaceData face in group.Faces) 
				{
					ConformToPlane(face.V0, group.Normal, ref group.Center);
					ConformToPlane(face.V1, group.Normal, ref group.Center);
					ConformToPlane(face.V2, group.Normal, ref group.Center);
				}

				ModelData.CalcuateFaceNormals();

				Invalidate();
			}	
		}

		public void GroupFaces()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Faces)
			{
				int maxGroup = -1;

				foreach (FaceData face in ModelData.Faces)
				{
					maxGroup = Math.Max(maxGroup, face.Group); 
				}

				maxGroup += 1; 

				foreach (ModelSelectionInfo info in SelectedItems)
				{
					FaceData face = ModelData.Faces[info.Index];

					face.Group = maxGroup;

					ModelData.Faces[info.Index] = face;
				}

				ModelData.CalcuateFaceNormals();

				Invalidate();
			}	
		}

		public void UngroupFaces()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Faces)
			{
				foreach (ModelSelectionInfo info in SelectedItems)
				{
					FaceData face = ModelData.Faces[info.Index];

					face.Group = -1;

					ModelData.Faces[info.Index] = face;
				}

				ModelData.CalcuateFaceNormals();

				Invalidate();
			}	
		}

		public void FlipFace()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Faces)
			{
				foreach (ModelSelectionInfo info in SelectedItems)
				{
					FaceData face = ModelData.Faces[info.Index];

					face.FlipNormal();

					ModelData.Faces[info.Index] = face;
				}

				ModelData.CalcuateFaceNormals();

				Invalidate();
			}						
		}

		public void DeleteFace()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Faces)
			{
				List<int> toRemove = new List<int>(); 

				foreach (ModelSelectionInfo info in SelectedItems)
				{
					toRemove.Add(info.Index);					
				}

				toRemove.Sort();

				toRemove.Reverse();

				foreach (int index in toRemove)
				{
					ModelData.Faces.RemoveAt(index);
				}

				InvalidateSelection();

				Invalidate();
			}	
		}

		public void DeleteVertex()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Verts)
			{
				int index = Selection.Index;

				for (int i = ModelData.Faces.Count - 1; i >= 0; i--)
				{
					if (ModelData.Faces[i].Contains(index) == true)
					{
						ModelData.Faces.RemoveAt(i);
					}
					else if (ModelData.Faces[i].HasGreater(index) == true)
					{
						FaceData data = ModelData.Faces[i];

						data.AdjustIndices(index);
						
						ModelData.Faces[i] = data; 
					}
				}

				ModelData.Verts.RemoveAt(Selection.Index);

				for (int i = ModelData.HullVerts.Count - 1; i >= 0; i--)
				{
					if (ModelData.HullVerts[i] == index)
					{
						ModelData.HullVerts.RemoveAt(i);
					}
					else if (ModelData.HullVerts[i] > index)
					{
						ModelData.HullVerts[i]--;
					}
				}

				InvalidateSelection();

				Invalidate();
			}
		}

		private void DeleteHullVertex()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Verts)
			{
				if (ModelData.HullVerts.Contains(Selection.Index) == true)
				{
					ModelData.HullVerts.Remove(Selection.Index);

					InvalidateSelection();

					Invalidate();
				}
			}
		}

		private void DeleteExtra()
		{
			if (HasValidSelection == true &&
				Selection.Array == ModelArrays.Extras)
			{
				int index = (Selection.Index - (Selection.Index % 2)) / 2;

				ModelData.Extras.RemoveAt(index);

				InvalidateSelection();

				Invalidate();
			}
		}

        public bool PaintMode { get; set; }
    }
}
