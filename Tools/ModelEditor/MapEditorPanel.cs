﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ModelEditor
{
    internal class MapEditorPanel : Panel
    {
        float[,] m_Data = null;

        Vector3[,] m_TopGridPoints = null;
        Vector3[,] m_BottomGridPoints = null;
        
        int[,] m_TopGridIndicies = null;
        int[,] m_BottomGridIndicies = null;

        int[] m_GridRows = null; 

        int m_Width, m_Height;
        int m_SpreadX = 16, m_SpreadY = 16;
        //int m_SpreadX = 32, m_SpreadY = 32;

        float m_HeightMax = 10f; //64f;

        float m_Scale = 0.1f;

        float m_SnowLevel = 0f;
        float m_SnowAngle = 0.5f;

        Bitmap m_Bitmap = null;
        byte[] m_BitmapBytes = null;
        int m_Stride = 0; 

        private float m_BrushSize = 1f;

        private Point m_MouseLocation;
        private bool m_MouseDown = false;
        public bool GenerateUnderSide = true; 

        public bool AutoGenerate { get; set; }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public float BrushSize
        {
            get
            {
                return m_BrushSize; 
            }
            set
            {
                m_BrushSize = value;

                Invalidate(); 
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public float SnowLevel
        {
            get
            {
                return m_SnowLevel; 
            }
            set
            {
                m_SnowLevel = value;

                DoGenerate(); 
            }
        }

        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public float SnowAngle
        {
            get
            {
                return m_SnowAngle;
            }
            set
            {
                m_SnowAngle = value;

                DoGenerate();
            }
        }

        public MapEditorPanel()
        {
            this.SetStyle(ControlStyles.Selectable, true);
            this.TabStop = true;

            DoubleBuffered = true;

            this.MouseClick += MapEditorPanel_MouseClick;
            this.MouseDoubleClick += MapEditorPanel_MouseDoubleClick;
            this.MouseDown += MapEditorPanel_MouseDown;
            this.MouseMove += MapEditorPanel_MouseMove;
            this.MouseUp += MapEditorPanel_MouseUp;
            this.MouseWheel += MapEditorPanel_MouseWheel;
        }

        public void New()
        {
            m_Width = 512; 
            m_Height = 512; 

            m_Data = new float[m_Width, m_Height];
            
            m_TopGridPoints = new Vector3[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];
            m_BottomGridPoints = new Vector3[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];

            m_TopGridIndicies = new int[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];
            m_BottomGridIndicies = new int[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];

            m_GridRows = new int[m_TopGridPoints.GetLength(1)];

            if (m_Bitmap != null)
            {
                m_Bitmap.Dispose();
                m_Bitmap = null;
                m_BitmapBytes = null;
            }

            m_Bitmap = new Bitmap(m_Width, m_Height, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

            DoGenerate();

            Invalidate();
        }

        public void Import(string path)
        {
            using (Bitmap bmp = (Bitmap)Bitmap.FromFile(path))
            {
                LoadFromImage(bmp);
            }
        }

        private void LoadFromImage(Bitmap bmp)
        {
            m_Width = bmp.Width;
            m_Height = bmp.Height;

            m_Data = new float[m_Width, m_Height];
            
            m_TopGridPoints = new Vector3[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];
            m_BottomGridPoints = new Vector3[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];

            m_TopGridIndicies = new int[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];
            m_BottomGridIndicies = new int[(m_Width / m_SpreadX), (m_Height / m_SpreadY)];

            m_GridRows = new int[m_TopGridPoints.GetLength(1)];

            for (int y = 0; y < m_Height; y++)
            {
                for (int x = 0; x < m_Width; x++)
                {
                    Color color = bmp.GetPixel(x, y);

                    float pixel = (float)color.R / 255f;

                    m_Data[x, y] = pixel;
                }
            }

            if (m_Bitmap != null)
            {
                m_Bitmap.Dispose();
                m_Bitmap = null;
                m_BitmapBytes = null;
            }

            m_Bitmap = new Bitmap(m_Width, m_Height, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);

            DoGenerate();

            Invalidate();
        }

        private void DoGenerate()
        {
            GenerateBitmap();

            if (AutoGenerate == false)
            {
                return;
            }

            Generate(); 
        }

        private void GenerateBitmap()
        {
            if (m_Bitmap == null)
            {
                return; 
            }

            ColorPalette ncp = m_Bitmap.Palette;
            
            for (int i = 0; i < 256; i++)
            {
                float data = (float)i / 255f;

                if ((data * data) > m_SnowLevel)
                {
                    ncp.Entries[i] = Color.FromArgb(255, i, i, i);
                }
                else
                {
                    ncp.Entries[i] = Color.FromArgb(255, 0, i, 0);
                }
            }

            m_Bitmap.Palette = ncp;

            var BoundsRect = new Rectangle(0, 0, m_Width, m_Height);

            BitmapData bmpData = m_Bitmap.LockBits(BoundsRect, ImageLockMode.WriteOnly, m_Bitmap.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            m_Stride = bmpData.Stride;

            int bytes = m_Stride * m_Bitmap.Height;

            if (m_BitmapBytes == null || m_BitmapBytes.Length != bytes)
            {
                m_BitmapBytes = new byte[bytes];
            }

            for (int y = 0; y < m_Height; y++)
            {
                int index = y * m_Stride; 
                for (int x = 0; x < m_Width; x++)
                {
                    m_BitmapBytes[index++] = (byte)(MathHelper.Clamp(m_Data[x, y], 0f, 1f) * 255f);
                }
            }

            Marshal.Copy(m_BitmapBytes, 0, ptr, bytes);
            m_Bitmap.UnlockBits(bmpData);

            Invalidate(); 
        }


        private void FlushBitmap(Rectangle bounds)
        {
            if (m_Bitmap == null)
            {
                return;
            }

            if (bounds.Height < 0)
            {
                return; 
            }

            var BoundsRect = new Rectangle(0, bounds.Y, m_Width, bounds.Height);

            BitmapData bmpData = m_Bitmap.LockBits(BoundsRect, ImageLockMode.WriteOnly, m_Bitmap.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            int bytes = bmpData.Stride * BoundsRect.Height;

            /* 
            if (m_BitmapBytes == null || m_BitmapBytes.Length != bytes)
            {
                m_BitmapBytes = new byte[bytes];
            }

            for (int y = 0; y < m_Height; y++)
            {
                int index = y * bmpData.Stride;

                for (int x = 0; x < m_Width; x++)
                {
                    m_BitmapBytes[index++] = (byte)(MathHelper.Clamp(m_Data[x, y], 0f, 1f) * 255f);
                }
            }
            */

            Marshal.Copy(m_BitmapBytes, BoundsRect.Y * bmpData.Stride, ptr, bytes);

            m_Bitmap.UnlockBits(bmpData);
        }

        private Bitmap CreateBitmap()
        {
            Bitmap bitmap = new Bitmap(m_Width, m_Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb); 

            var BoundsRect = new Rectangle(0, 0, m_Width, m_Height);

            BitmapData bmpData = bitmap.LockBits(BoundsRect, ImageLockMode.WriteOnly, bitmap.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            int bytes = bmpData.Stride * bitmap.Height;

            byte[] bytesData = new byte[bytes];

            for (int y = 0; y < m_Height; y++)
            {
                int index = y * bmpData.Stride;
                for (int x = 0; x < m_Width; x++)
                {
                    byte data = (byte)(MathHelper.Clamp(m_Data[x, y], 0f, 1f) * 255f); 

                    bytesData[index++] = data;
                    bytesData[index++] = data;
                    bytesData[index++] = data;
                    bytesData[index++] = 255;
                }
            }

            Marshal.Copy(bytesData, 0, ptr, bytes);
            
            bitmap.UnlockBits(bmpData);

            return bitmap; 
        }

        public void Export(string path)
        {
            if (m_Data == null)
            {
                return; 
            }

            using (Bitmap bitmap = CreateBitmap())
            {
                bitmap.Save(path); 
            }
        }

        public void Copy()
        {
            Clipboard.SetImage(CreateBitmap());
        }

        public void Paste()
        {
            if (Clipboard.ContainsImage())
            {
                LoadFromImage((Bitmap)Clipboard.GetImage());                
            }
        }

        public void Generate()
        {
            if (m_Data == null)
            {
                return;
            }

            StaticObjects.ModelData.Faces.Clear();
            StaticObjects.ModelData.Verts.Clear();
            //StaticObjects.ModelData.Extras.Clear();
            StaticObjects.ModelData.HullVerts.Clear();

            int xMax = (m_Width / m_SpreadX); // -1;
            int yMax = (m_Height / m_SpreadY); // - 1; 

            int xIndex = 0;
            int yIndex = 0;

            for (int y = 0; y < yMax; y++)
            {
                xIndex = 0;

                for (int x = 0; x < xMax; x++)
                {
                    int dataX, dataY;

                    dataX = x * m_SpreadX;
                    if (y % 2 == 0)
                    {
                        dataX += m_SpreadX / 2;

                        if (x == xMax - 1)
                        {
                            m_TopGridPoints[xIndex++, yIndex] = -Vector3.UnitZ;
                            continue;
                        }
                    }

                    if (y % 2 == 1)
                    {
                        if (x == xMax - 1)
                        {
                            m_TopGridPoints[xIndex++, yIndex] = -Vector3.UnitZ;
                            continue;
                        }
                    }
                    else
                    {
                        if (x == xMax - 2)
                        {
                            m_TopGridPoints[xIndex++, yIndex] = -Vector3.UnitZ;
                            continue;
                        }
                    }

                    if (y == 0 || y == yMax - 1)
                    {
                        if (x % 3 == 2)
                        {
                            m_TopGridPoints[xIndex++, yIndex] = -Vector3.UnitZ;
                            continue;
                        }
                    }

                    dataY = y * m_SpreadY;

                    float data = m_Data[dataX, dataY];

                    data = data * data;

                    if (data <= 0)
                    {
                        m_TopGridPoints[xIndex++, yIndex] = -Vector3.UnitZ;
                        continue;
                    }

                    m_TopGridPoints[xIndex++, yIndex] = new OpenTK.Vector3(
                        (((float)m_Width * -0.5f) + (float)dataX) * m_Scale,
                        data * m_HeightMax,
                        (((float)m_Height * -0.5f) + (float)dataY) * m_Scale);
                }

                m_GridRows[yIndex] = xIndex;
                yIndex++;
            }

            {
                for (int y = 0; y < yMax; y++)
                {
                    int max = m_GridRows[y];

                    for (int x = 0; x < max; x++)
                    {
                        Vector3 vert = m_TopGridPoints[x, y];

                        if (vert == -Vector3.UnitZ)
                        {
                            m_TopGridIndicies[x, y] = -1;
                        }
                        else
                        {
                            m_TopGridIndicies[x, y] = StaticObjects.ModelData.Verts.Count;

                            StaticObjects.ModelData.Verts.Add(vert);
                        }
                    }
                }

                for (int y = 0; y < yMax - 1; y++)
                {
                    int max = m_GridRows[y];

                    for (int x = 0; x < max - 1; x++)
                    {
                        int i0 = m_TopGridIndicies[x, y];
                        int i1 = m_TopGridIndicies[x + 1, y];
                        int i2 = m_TopGridIndicies[x, y + 1];
                        int i3 = m_TopGridIndicies[x + 1, y + 1];

                        if (y % 2 == 0)
                        {
                            if (i0 != -1 && i2 != -1 && i3 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.DarkGreen,
                                    Group = -1,
                                    V0 = i0,
                                    V1 = i2,
                                    V2 = i3,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }

                            if (i0 != -1 && i1 != -1 && i3 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.DarkGreen,
                                    Group = -1,
                                    V0 = i0,
                                    V1 = i3,
                                    V2 = i1,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }
                        }
                        else
                        {
                            if (i0 != -1 && i1 != -1 && i2 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.DarkGreen,
                                    Group = -1,
                                    V0 = i0,
                                    V1 = i2,
                                    V2 = i1,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }

                            if (i1 != -1 && i2 != -1 && i3 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.DarkGreen,
                                    Group = -1,
                                    V0 = i1,
                                    V1 = i2,
                                    V2 = i3,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }
                        }
                    }
                }

                StaticObjects.ModelData.CalcuateFaceNormals();

                for (int i = 0; i < StaticObjects.ModelData.Faces.Count; i++)
                {
                    FaceData face = StaticObjects.ModelData.Faces[i];

                    face.Color = GetFaceColor(face);

                    StaticObjects.ModelData.Faces[i] = face;
                }
            }

            if (GenerateUnderSide == true) 
            {
                List<Vector3> tempVerts = new List<Vector3>(); 

                for (int y = 0; y < yMax; y++)
                {
                    int max = m_GridRows[y];

                    for (int x = 0; x < max; x++)
                    {
                        Vector3 vert = m_TopGridPoints[x, y];

                        if (vert == -Vector3.UnitZ)
                        {
                            m_BottomGridIndicies[x, y] = -1;
                        }
                        else
                        {
                            vert.Y = -0.25f + -vert.Y;

                            m_BottomGridIndicies[x, y] = tempVerts.Count;
                            tempVerts.Add(vert);
                        }
                    }
                }

                for (int j = 0; j < 3; j++)
                {
                    Vector3[] tempVerts_Copy = tempVerts.ToArray();

                    for (int y = 0; y < yMax - 1; y++)
                    {
                        int max = m_GridRows[y];

                        for (int x = 0; x < max - 1; x++)
                        {
                            int i0 = m_BottomGridIndicies[x, y];
                            int i1 = m_BottomGridIndicies[x + 1, y];
                            int i2 = m_BottomGridIndicies[x, y + 1];
                            int i3 = m_BottomGridIndicies[x + 1, y + 1];

                            if (y % 2 == 0)
                            {
                                if (i0 != -1 && i2 != -1 && i3 != -1)
                                {
                                    float average = (tempVerts[i0].Y + tempVerts[i2].Y + tempVerts[i3].Y) / 3f;

                                    tempVerts_Copy[i0].Y = (tempVerts_Copy[i0].Y + average) * 0.5f;
                                    tempVerts_Copy[i2].Y = (tempVerts_Copy[i2].Y + average) * 0.5f;
                                    tempVerts_Copy[i3].Y = (tempVerts_Copy[i3].Y + average) * 0.5f;
                                }

                                if (i0 != -1 && i1 != -1 && i3 != -1)
                                {
                                    float average = (tempVerts[i0].Y + tempVerts[i3].Y + tempVerts[i1].Y) / 3f;

                                    tempVerts_Copy[i0].Y = (tempVerts_Copy[i0].Y + average) * 0.5f;
                                    tempVerts_Copy[i1].Y = (tempVerts_Copy[i1].Y + average) * 0.5f;
                                    tempVerts_Copy[i3].Y = (tempVerts_Copy[i3].Y + average) * 0.5f;
                                }
                            }
                            else
                            {
                                if (i0 != -1 && i1 != -1 && i2 != -1)
                                {
                                    float average = (tempVerts[i0].Y + tempVerts[i2].Y + tempVerts[i1].Y) / 3f;

                                    tempVerts_Copy[i0].Y = (tempVerts_Copy[i0].Y + average) * 0.5f;
                                    tempVerts_Copy[i1].Y = (tempVerts_Copy[i1].Y + average) * 0.5f;
                                    tempVerts_Copy[i2].Y = (tempVerts_Copy[i2].Y + average) * 0.5f;
                                }

                                if (i1 != -1 && i2 != -1 && i3 != -1)
                                {
                                    float average = (tempVerts[i2].Y + tempVerts[i1].Y + tempVerts[i3].Y) / 3f;

                                    tempVerts_Copy[i1].Y = (tempVerts_Copy[i1].Y + average) * 0.5f;
                                    tempVerts_Copy[i2].Y = (tempVerts_Copy[i2].Y + average) * 0.5f;
                                    tempVerts_Copy[i3].Y = (tempVerts_Copy[i3].Y + average) * 0.5f;
                                }
                            }
                        }
                    }

                    tempVerts.Clear();
                    tempVerts.AddRange(tempVerts_Copy); 
                }

                for (int y = 0; y < yMax; y++)
                {
                    int max = m_GridRows[y];

                    for (int x = 0; x < max; x++)
                    {
                        Vector3 vert = m_TopGridPoints[x, y];

                        if (vert == -Vector3.UnitZ)
                        {
                            m_BottomGridIndicies[x, y] = -1;
                        }
                        else
                        {       
                            int index = m_BottomGridIndicies[x, y];

                            vert = tempVerts[index];

                            vert.Y *= 2.5f; 

                            m_BottomGridIndicies[x, y] = StaticObjects.ModelData.Verts.Count;
                            StaticObjects.ModelData.Verts.Add(vert);
                        }
                    }
                }

                int faceIndex = StaticObjects.ModelData.Faces.Count; 

                for (int y = 0; y < yMax - 1; y++)
                {
                    int max = m_GridRows[y];

                    for (int x = 0; x < max - 1; x++)
                    {
                        int i0 = m_BottomGridIndicies[x, y];
                        int i1 = m_BottomGridIndicies[x + 1, y];
                        int i2 = m_BottomGridIndicies[x, y + 1];
                        int i3 = m_BottomGridIndicies[x + 1, y + 1];

                        if (y % 2 == 0)
                        {
                            if (i0 != -1 && i2 != -1 && i3 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.SaddleBrown,
                                    Group = -1,
                                    V0 = i2,
                                    V1 = i0,
                                    V2 = i3,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }

                            if (i0 != -1 && i1 != -1 && i3 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.SaddleBrown,
                                    Group = -1,
                                    V0 = i3,
                                    V1 = i0,
                                    V2 = i1,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }
                        }
                        else
                        {
                            if (i0 != -1 && i1 != -1 && i2 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.SaddleBrown,
                                    Group = -1,
                                    V0 = i2,
                                    V1 = i0,
                                    V2 = i1,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }

                            if (i1 != -1 && i2 != -1 && i3 != -1)
                            {
                                FaceData face = new FaceData()
                                {
                                    Color = Color4.SaddleBrown,
                                    Group = -1,
                                    V0 = i2,
                                    V1 = i1,
                                    V2 = i3,
                                };

                                StaticObjects.ModelData.Faces.Add(face);
                            }
                        }
                    }
                }


                // Find Outside Edges 
                faceIndex = StaticObjects.ModelData.Faces.Count;
                int groups = 0; 

                for (int y = 0; y < yMax - 1; y++)
                {
                    int max = m_GridRows[y];

                    for (int x = 0; x < max - 1; x++)
                    {
                        int i0_T = m_TopGridIndicies[x, y];
                        int i1_T = m_TopGridIndicies[x + 1, y];
                        int i2_T = m_TopGridIndicies[x, y + 1];
                        int i3_T = m_TopGridIndicies[x + 1, y + 1];

                        int i0_B = m_BottomGridIndicies[x, y];
                        int i1_B = m_BottomGridIndicies[x + 1, y];
                        int i2_B = m_BottomGridIndicies[x, y + 1];
                        int i3_B = m_BottomGridIndicies[x + 1, y + 1];

                        if (y % 2 == 0)
                        {                        
                            //AddFacesForEdges(ref groups, i0_T, i2_T, i3_T, i0_B, i2_B, i3_B);
                            AddFacesForEdges(ref groups, i0_T, i2_T, i3_T, i0_B, i2_B, i3_B);

                            AddFacesForEdges(ref groups, i3_T, i1_T, i0_T, i3_B, i1_B, i0_B);
                        }
                        else
                        {
                            AddFacesForEdges(ref groups, i2_T, i1_T, i0_T, i2_B, i1_B, i0_B);
                            
                            AddFacesForEdges(ref groups, i1_T, i2_T, i3_T, i1_B, i2_B, i3_B);
                        }
                    }
                }

                StaticObjects.ModelData.CalcuateFaceNormals();

                /* 
                for (int i = faceIndex; i < StaticObjects.ModelData.Faces.Count; i++)
                {
                    FaceData face = StaticObjects.ModelData.Faces[i];

                    face.Color = GetFaceColor(face);

                    StaticObjects.ModelData.Faces[i] = face;
                }
                */
            }


            StaticObjects.Invalidate();
        }

        private static void AddFacesForEdges(ref int groups, int i0_T, int i1_T, int i2_T, int i0_B, int i1_B, int i2_B)
        {
            if (i0_T == -1 && (i1_T != -1 && i2_T != -1))
            {
                AddFaces(ref groups, i2_T, i1_T, i2_B, i1_B);
            }

            if (i1_T == -1 && (i0_T != -1 && i2_T != -1))
            {
                AddFaces(ref groups, i0_T, i2_T, i0_B, i2_B);
            }

            if (i2_T == -1 && (i0_T != -1 && i1_T != -1))
            {
                AddFaces(ref groups, i1_T, i0_T, i1_B, i0_B);
            }
        }

        private static void AddFaces(ref int groups, int i0_T, int i1_T, int i0_B, int i1_B)
        {
            StaticObjects.ModelData.Faces.Add(new FaceData()
            {
                Color = Color4.SaddleBrown,
                Group = groups,
                V0 = i0_T,
                V1 = i0_B,
                V2 = i1_T,
            });

            StaticObjects.ModelData.Faces.Add(new FaceData()
            {
                Color = Color4.SaddleBrown,
                Group = groups,
                V0 = i0_B,
                V1 = i1_B,
                V2 = i1_T,
            });

            groups++;
        }

        private Color4 GetFaceColor(FaceData face)
        {            
            float max =
                Math.Max(Math.Max(StaticObjects.ModelData.Verts[face.V0].Y, StaticObjects.ModelData.Verts[face.V1].Y), StaticObjects.ModelData.Verts[face.V2].Y);

            float dot = Vector3.Dot(face.Normal, Vector3.UnitY); 

            if (max > m_HeightMax * m_SnowLevel)
            {
                if (dot > SnowAngle)
                {
                    return Color4.White;
                }
                else
                {
                    return Color4.DarkGray;
                }
            }
            else
            {
                return Color4.DarkGreen; 
            }
        }

        void MapEditorPanel_MouseClick(object sender, MouseEventArgs e)
        {

        }

        void MapEditorPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        void MapEditorPanel_MouseDown(object sender, MouseEventArgs e)
        {
            m_MouseLocation = e.Location;
            m_MouseDown = true;

            WriteBrush(); 

            Invalidate(); 
        }

        private void WriteBrush()
        {
            if (m_Bitmap == null) 
            {
                return; 
            }

            Vector2 center = new Vector2((float)m_MouseLocation.X, (float)m_MouseLocation.Y); 
            Vector2 pixel = new Vector2(0, 0); 

            float brushSize = (m_BrushSize * 0.5f); 

            RectangleF rect = new RectangleF(m_MouseLocation.X - brushSize, m_MouseLocation.Y - brushSize, m_BrushSize, m_BrushSize);

            Point location = new Point(Math.Max((int)rect.Left, 0), Math.Max((int)rect.Top, 0));

            Rectangle bounds = new Rectangle(
                location.X,
                location.Y,
                Math.Min((int)rect.Right, m_Width) - location.X,
                Math.Min((int)rect.Bottom, m_Height) - location.Y);

            //for (int y = Math.Max((int)rect.Top, 0), ye = Math.Min((int)rect.Bottom, m_Height); y < ye; y++)
            for (int y = bounds.Top, ye = bounds.Bottom; y < ye; y++)
            {
                int index = y * m_Stride;

                //for (int x = Math.Max((int)rect.Left, 0), xe = Math.Min((int)rect.Right, m_Width); x < xe; x++)
                for (int x = bounds.Left, xe = bounds.Right; x < xe; x++)
                {
                    if (rect.Contains(x, y) == false) 
                    {
                        continue; 
                    }

                    pixel.X = (float)x;
                    pixel.Y = (float)y;

                    float dist = (center - pixel).Length; 

                    if (dist > brushSize) 
                    {
                        continue; 
                    }

                    dist /= brushSize; 

                    dist = 1f - dist;

                    m_Data[x, y] += dist * 0.01f;

                    m_BitmapBytes[index + x] = (byte)(MathHelper.Clamp(m_Data[x, y], 0f, 1f) * 255f);                    
                }
            }

            FlushBitmap(bounds);

            if (AutoGenerate == false)
            {
                return;
            }

            Generate(); 
        }

        void MapEditorPanel_MouseMove(object sender, MouseEventArgs e)
        {
            m_MouseLocation = e.Location;

            if (m_MouseDown == true)
            {
                WriteBrush(); 
            }

            Invalidate(); 
        }

        void MapEditorPanel_MouseUp(object sender, MouseEventArgs e)
        {
            m_MouseLocation = e.Location;
            m_MouseDown = false;
            Invalidate(); 
        }

        void MapEditorPanel_MouseWheel(object sender, MouseEventArgs e)
        {

        }

        #region Paint Event

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.Clear(Color.Black);

            try
            {
                if (m_Bitmap != null)
                {
                    Pen pen;

                    if (m_MouseDown == true)
                    {
                        pen = Pens.SkyBlue; 
                    }
                    else
                    {
                        pen = Pens.Cyan; 
                    }

                    e.Graphics.DrawImageUnscaled(m_Bitmap, 0, 0);

                    RectangleF rect = new RectangleF(m_MouseLocation.X - (m_BrushSize * 0.5f), m_MouseLocation.Y - (m_BrushSize * 0.5f), m_BrushSize, m_BrushSize);

                    e.Graphics.DrawEllipse(pen, rect); 
                }

                e.Graphics.DrawRectangle(Pens.White, new Rectangle(0, 0, m_Width, m_Height)); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        #endregion
    }
}
