﻿using System;
using System.Windows.Forms;

namespace ModelEditor
{
	static class Program
	{
		/// <summary>
		/// The main entry mouse for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new EditorForm());
		}
	}
}
