﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditor
{
    static class StaticObjects
    {
        public static Rug.Game.Flat.Models.ModelData ModelData;

        public static event EventHandler OnInvalidate; 

        internal static void Invalidate()
        {
            if (OnInvalidate == null)
            {
                return; 
            }

            OnInvalidate(null, EventArgs.Empty); 
        }
    }
}
