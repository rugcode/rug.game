﻿using ModelEditorGL.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL
{
    static class StaticObjects
    {
        public static EditorData Data = new EditorData();

        public static event EventHandler Invalidated; 

        public static void Invalidate()
        {
            if (Invalidated != null)
            {
                Invalidated(null, EventArgs.Empty); 
            }
        }
    }
}
