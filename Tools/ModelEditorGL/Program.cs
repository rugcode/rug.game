﻿using Rug.Cmd;
using Rug.Cmd.Colors;
using Rug.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ModelEditorGL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ConsoleColorState state = RC.ColorState;

            Preferences.Load();

            if (FileHelper.FileExists("~/data.rpa") == true)
            {
                PackageHelper.GetPackage("~/data.rpa", false, System.IO.FileAccess.Read, true);
            }

            try
            {
                RC.Verbosity = ConsoleVerbosity.Debug;

                RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new EditorForm());
            }
            catch (Exception ex)
            {
                RC.WriteException(04, ex);
                RC.PromptForKey("Press any key to exit.", true, false);
            }
            finally
            {
                Preferences.Save(); 

                RC.ColorState = state;
            }
        }
    }
}
