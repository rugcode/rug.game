﻿using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    class ModelSelectionInfo
    {
        //public EditableModelData ModelData;

        public int ModelID; 

        public ModelArrays Array;

        public readonly List<int> Indices = new List<int>();

        public void Add(ModelSelectionInfo data)
        {
            foreach (int index in data.Indices)
            {
                if (Indices.Contains(index) == false)
                {
                    Indices.Add(index);

                    StaticObjects.Data.ActionMadeChange = true; 
                }
            }
        }

        public void Subtract(ModelSelectionInfo data)
        {
            foreach (int index in data.Indices)
            {
                Indices.Remove(index);

                StaticObjects.Data.ActionMadeChange = true; 
            }
        }

        public void Sort()
        {
            Indices.Sort(); 
        }

        public ModelSelectionInfo Clone()
        {
            ModelSelectionInfo info = new ModelSelectionInfo();

            info.ModelID = ModelID;
            info.Array = Array;
            info.Indices.AddRange(Indices); 

            return info; 
        }
    }
}
