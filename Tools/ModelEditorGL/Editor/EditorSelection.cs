﻿using OpenTK;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    class EditorSelection
    {
        public readonly List<ModelSelectionInfo> Selection = new List<ModelSelectionInfo>();

        public int PointsTotal { get; private set; }

        public int FacesTotal { get; private set; }

        public Vector3 Center { get; private set; }

        public Vector3 Bounds { get; private set; }

        public EditorSelection Clone()
        {
            EditorSelection selection = new EditorSelection();

            selection.Center = Center;
            selection.Bounds = Bounds;

            selection.PointsTotal = PointsTotal;
            selection.FacesTotal = FacesTotal; 

            foreach (ModelSelectionInfo info in Selection)
            {
                selection.Selection.Add(info.Clone()); 
            }

            return selection; 
        }

        public ModelSelectionInfo FindSelection(int id, ModelArrays array)
        {
            foreach (ModelSelectionInfo info in Selection) 
            {
                if (info.ModelID == id && info.Array == array)
                {
                    return info; 
                }
            }

            return null; 
        }

        public void Add(ModelSelectionInfo[] infos)
        {
            foreach (ModelSelectionInfo info in infos)
            {
                Add(info);
            }

            CalculateTotal();
        }

        public void Add(ModelSelectionInfo data)
        {
            if (data == null)
            {
                return; 
            }

            ModelSelectionInfo existing = FindSelection(data.ModelID, data.Array);

            if (existing != null)
            {
                existing.Add(data);
            }
            else 
            {
                Selection.Add(data);
                
                StaticObjects.Data.ActionMadeChange = true; 
            }

            CalculateTotal();
        }

        public void Subtract(ModelSelectionInfo[] infos)
        {
            foreach (ModelSelectionInfo info in infos)
            {
                Subtract(info);
            }

            CalculateTotal();
        }

        public void Subtract(ModelSelectionInfo data)
        {
            if (data == null)
            {
                return;
            }

            ModelSelectionInfo existing = FindSelection(data.ModelID, data.Array);

            if (existing != null)
            {
                existing.Subtract(data);
            }

            CalculateTotal();
        }

        public void CalculateTotal()
        {
            PointsTotal = 0;
            FacesTotal = 0; 

            foreach (ModelSelectionInfo info in Selection)
            {
                switch (info.Array)
                {
                    case ModelArrays.Verts:
                        PointsTotal += info.Indices.Count; 
                        break;
                    case ModelArrays.Faces:
                        FacesTotal += info.Indices.Count;
                        break; 
                    case ModelArrays.HullVerts:
                    case ModelArrays.Extras: 
                    case ModelArrays.Objects:
                        break;
                    default:
                        break;
                }
            }
        }

        public int CalculateCenter()
        {
            int modelID = -1; 

            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            foreach (ModelSelectionInfo info in Selection)
            {
                EditableModelData model = StaticObjects.Data.Models[info.ModelID];

                if (modelID == -1)
                {
                    modelID = info.ModelID; 
                }

                switch (info.Array)
                {
                    case ModelArrays.Verts:
                        for (int i = 0; i < info.Indices.Count; i++)
                        {
                            Vector3 vert = model.ModelData.Verts[info.Indices[i]];

                            Vector3.ComponentMin(ref min, ref vert, out min);
                            Vector3.ComponentMax(ref max, ref vert, out max);
                        }
                        break;
                    case ModelArrays.Faces:
                        for (int i = 0; i < info.Indices.Count; i++)
                        {
                            FaceData face = model.ModelData.Faces[info.Indices[i]];

                            Vector3 v0 = model.ModelData.Verts[face.V0];
                            Vector3 v1 = model.ModelData.Verts[face.V1];
                            Vector3 v2 = model.ModelData.Verts[face.V2];

                            Vector3.ComponentMin(ref min, ref v0, out min);
                            Vector3.ComponentMax(ref max, ref v0, out max);

                            Vector3.ComponentMin(ref min, ref v1, out min);
                            Vector3.ComponentMax(ref max, ref v1, out max);

                            Vector3.ComponentMin(ref min, ref v2, out min);
                            Vector3.ComponentMax(ref max, ref v2, out max);
                        }
                        break;
                    case ModelArrays.Objects:
                        for (int i = 0; i < info.Indices.Count; i++)
                        {
                            if (info.Indices[i] == -1)
                            {
                                Vector3 center = model.ActivePosition;
                                Vector3 bounds = model.Bounds * model.ActiveScale;

                                Vector3 c0 = center + bounds;
                                Vector3 c1 = center - bounds;

                                Vector3.ComponentMin(ref min, ref c0, out min);
                                Vector3.ComponentMax(ref max, ref c0, out max);

                                Vector3.ComponentMin(ref min, ref c1, out min);
                                Vector3.ComponentMax(ref max, ref c1, out max);
                            }
                            else
                            {
                                Vector3 center = model.ModelData.HullPrimitives[info.Indices[i]].Center;

                                Vector3.ComponentMin(ref min, ref center, out min);
                                Vector3.ComponentMax(ref max, ref center, out max);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            Bounds = (max - min) * 0.5f;
            Center = min + Bounds;

            return modelID; 
        }

        public void Clear()
        {
            Selection.Clear();

            CalculateTotal();
        }
    }
}
