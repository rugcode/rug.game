﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game;
using Rug.Game.Core.MeshData;
using Rug.Game.Core.Resources;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Editor
{
    partial class EditorData : IResourceManager
    {        
        public readonly ResourceManager ResourceManagers = new ResourceManager();

        public readonly Dictionary<int, EditableModelData> Models = new Dictionary<int, EditableModelData>();

        public EditorSelection Selection = new EditorSelection();

        public EditorSelectionCache SelectionCache = new EditorSelectionCache(); 

        public EditorInputState InputState = new EditorInputState();

        public EditorUndoHistory UndoHistory = new EditorUndoHistory(); 

        public int ModelID = 0; 

        public bool Disposed { get { return ResourceManagers.Disposed; } }

        public bool ActionMadeChange = false;

        public bool NeedsSave { get; set; }        

        private int m_CurrentModelID;

        public EditableModelData CurrentModel { get { return Models[m_CurrentModelID]; } }

        public event EventHandler EditorObjectsNeedUpdate;

        public event EventHandler EditorControlsNeedUpdate;

        public event EventHandler EditorSelectionChanged;

        public EditorData()
        {
            NeedsSave = false;
            ActionMadeChange = false; 
        }
        
        public virtual void LoadResources()
        {
            ResourceManagers.LoadResources();            
        }
        
        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1 + ResourceManagers.Total; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= (this as IResourceManager).Count)
            {
                process.Pop();
            }
            else
            {
                process.Increment();
                process.Push(ResourceManagers);
            }
        }

        public virtual void UnloadResources()
        {
            ResourceManagers.UnloadResources();
        }

        public void Dispose()
        {
            ResourceManagers.Dispose();
        }

        public void CacheSelection()
        {
            int modelID = Selection.CalculateCenter();

            if (modelID != -1)
            {
                m_CurrentModelID = modelID;
            }

            SelectionCache.Cache(Selection);             
        }

        public void OnEditorControlsNeedUpdate() 
        {
            if (EditorControlsNeedUpdate != null) 
            {
                EditorControlsNeedUpdate(this, EventArgs.Empty); 
            }
        }

        public void OnEditorObjectsNeedUpdate()
        {
            if (EditorObjectsNeedUpdate != null) 
            {
                EditorObjectsNeedUpdate(this, EventArgs.Empty); 
            }            
        }

        public void SetSelectionMode(ModelSelectionMode mode)
        {
            if (InputState.SelectionMode == mode)
            {
                return; 
            }

            Selection.Clear();
            SelectionCache.Clear(); 

            if (InputState.SelectionMode == ModelSelectionMode.Objects)
            {
                foreach (EditableModelData data in Models.Values)
                {
                    data.ApplyTransform(); 
                }
            }

            InputState.SelectionMode = mode;

            if (InputState.SelectionMode == ModelSelectionMode.Objects)
            {
                foreach (EditableModelData data in Models.Values)
                {
                    data.NormaliseForObjectEdit(); 
                }
            }

            UpdateModels_Full();         
        }

        public void ResetForObjectEdit()
        {
            if (InputState.SelectionMode != ModelSelectionMode.Objects)
            {
                return; 
            }

            CheckForObjectChanges();

            foreach (EditableModelData data in Models.Values)
            {
                data.ApplyTransform();
                data.NormaliseForObjectEdit();
            }

            UpdateModels_Full();   
        }

        public void CheckForObjectChanges()
        {
            if (InputState.SelectionMode != ModelSelectionMode.Objects)
            {
                return; 
            }

            foreach (EditableModelData data in Models.Values)
            {
                EditorModelSelectionCache cache;

                if (SelectionCache.Caches.TryGetValue(data.ModelID, out cache) == false)
                {
                    continue;
                }

                ActionMadeChange |= cache.Rotation != data.ActiveRotation;
                ActionMadeChange |= cache.Scale != data.ActiveScale;
                ActionMadeChange |= cache.Location != data.ActivePosition; 
            }
        }

        public void UpdateModels()
        {
            foreach (EditableModelData data in Models.Values)
            {
                data.UpdateModel(Selection, InputState.ToolMode);
            }
        }
      
        public void UpdateModels_Full()
        {
            foreach (EditableModelData data in Models.Values)
            {
                data.ModelData.CalcuateFaceNormals();
                data.UpdateModel(Selection, InputState.ToolMode);
            }            
        }

        public EditableModelData AddModel(int modelID, ModelData data)
        {
            EditableModelData editableData = new EditableModelData(data);

            editableData.ModelID = modelID;
            editableData.Name = Helper.ValueOrDefault(data.FilePath, "Unnamed");

            if (Disposed == false)
            {
                editableData.LoadResources();
            }

            Models.Add(editableData.ModelID, editableData);
            ResourceManagers.Add(editableData);

            m_CurrentModelID = modelID;

            OnEditorObjectsNeedUpdate(); 

            return editableData; 
        }

        public void RemoveModel(int modelID)
        {
            EditableModelData editableData = Models[modelID];

            if (Disposed == false)
            {
                editableData.UnloadResources();
            }

            Models.Remove(editableData.ModelID);
            ResourceManagers.Remove(editableData);

            if (m_CurrentModelID == modelID)
            {
                if (Models.Count == 0)
                {
                    // Add default model
                    AddModel(ModelID++, new ModelData()); 
                }
                else
                {
                    m_CurrentModelID = Models.Values.First().ModelID;
                }
            }

            OnEditorObjectsNeedUpdate(); 
        }

        public void PasteSelected()
        {
            
        }

        public void CopySelected()
        {
            
        }

        public void CutSelected()
        {
            
        }

        public void SplitSelected()
        {
            
        }

        public void MergeSelected()
        {
            EditableModelData dest = CurrentModel;
            List<int> modelsToRemove = new List<int>(); 

            foreach (EditableModelData editableModelData in Models.Values)
            {
                if (editableModelData == dest)
                {
                    continue; 
                }

                dest.ModelData.Merge(editableModelData.ModelData, ref editableModelData.Transform);

                modelsToRemove.Add(editableModelData.ModelID); 
            }

            foreach (int id in modelsToRemove) 
            {
                RemoveModel(id); 
            }

            UpdateModels_Full(); 
        }

        public void Subdivide()
        {
            foreach (EditableModelData editableModelData in Models.Values)
            {
                ModelData modelData = editableModelData.ModelData; 

                List<FaceData> originalFaces = new List<FaceData>(modelData.Faces);

                modelData.Faces.Clear();

                for (int i = 0; i < originalFaces.Count; i++)
                {
                    FaceData data = originalFaces[i];

                    int v0i = data.V0;
                    int v1i = data.V1;
                    int v2i = data.V2;

                    Vector3 v0 = modelData.Verts[v0i];
                    Vector3 v1 = modelData.Verts[v1i];
                    Vector3 v2 = modelData.Verts[v2i];

                    Vector3 v0_v1 = (v0 + v1) / 2f;
                    Vector3 v1_v2 = (v1 + v2) / 2f;
                    Vector3 v2_v0 = (v2 + v0) / 2f;

                    int v0_v1i = modelData.Verts.Count;
                    modelData.Verts.Add(v0_v1);

                    int v1_v2i = modelData.Verts.Count;
                    modelData.Verts.Add(v1_v2);

                    int v2_v0i = modelData.Verts.Count;
                    modelData.Verts.Add(v2_v0);

                    modelData.Faces.Add(new FaceData()
                    {
                        Color = data.Color,
                        Normal = data.Normal,
                        Group = data.Group,
                        V0 = v0i,
                        V1 = v0_v1i,
                        V2 = v2_v0i,
                    });


                    modelData.Faces.Add(new FaceData()
                    {
                        Color = data.Color,
                        Normal = data.Normal,
                        Group = data.Group,
                        V0 = v1i,
                        V1 = v1_v2i,
                        V2 = v0_v1i,
                    });

                    modelData.Faces.Add(new FaceData()
                    {
                        Color = data.Color,
                        Normal = data.Normal,
                        Group = data.Group,
                        V0 = v2i,
                        V1 = v2_v0i,
                        V2 = v1_v2i,
                    });

                    modelData.Faces.Add(new FaceData()
                    {
                        Color = data.Color,
                        Normal = data.Normal,
                        Group = data.Group,
                        V0 = v0_v1i,
                        V1 = v1_v2i,
                        V2 = v2_v0i,
                    });
                }

                modelData.CalcuateFaceNormals();
            }
        }
    }
}
