﻿using OpenTK;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    enum UndoHistoryStepType 
    {
        SelectionModeChanged,
        SelectionChange, 
        ModelAdded, 
        ModelRemoved, 
        ModelModified, 
    };

    class EditorUndoHistoryStepItem
    {
        public int ModelID; 

        public ModelData Data; 

        public Quaternion Rotation;

        public Vector3 Scale;

        public Vector3 Position;

        public EditorUndoHistoryStepItem(EditableModelData data)
        {
            ModelID = data.ModelID; 

            Data = new ModelData();

            data.ModelData.CopyTo(Data);

            Rotation = data.ActiveRotation;
            Scale = data.ActiveScale;
            Position = data.ActivePosition; 
        }

        public void Apply()
        {
            EditableModelData data = StaticObjects.Data.Models[ModelID];

            Data.CopyTo(data.ModelData);

            data.ActiveRotation = Rotation;
            data.ActiveScale = Scale;
            data.ActivePosition = Position; 
        }

        public void AddModel()
        {
            StaticObjects.Data.AddModel(ModelID, new ModelData());

            Apply(); 
        }

        public void RemoveModel()
        {
            StaticObjects.Data.RemoveModel(ModelID);
        }
    }

    class EditorUndoHistoryStep 
    {
        private static int StepID = 0;

        public readonly int ID; 

        public readonly UndoHistoryStepType StepType; 

        public readonly List<EditorUndoHistoryStepItem> Items = new List<EditorUndoHistoryStepItem>();

        public ModelSelectionMode SelectionMode;

        public EditorSelection Selection;

        public EditorUndoHistoryStep(UndoHistoryStepType stepType)
        {
            ID = StepID++; 

            StepType = stepType;

            switch (StepType)
            {
                case UndoHistoryStepType.SelectionModeChanged:
                    SelectionMode = StaticObjects.Data.InputState.SelectionMode;
                    break;
                case UndoHistoryStepType.SelectionChange:
                    Selection = StaticObjects.Data.Selection.Clone();
                    break;
                case UndoHistoryStepType.ModelModified:
                    break;
                default:
                    throw new Exception("Unknown step type: " + StepType); 
            }

            foreach (EditableModelData data in StaticObjects.Data.Models.Values)
            {
                Items.Add(new EditorUndoHistoryStepItem(data));
            }
        }

        public EditorUndoHistoryStep(UndoHistoryStepType stepType, int modelID)
        {
            ID = StepID++;

            StepType = stepType;

            switch (StepType)
            {
                case UndoHistoryStepType.ModelAdded:                    
                case UndoHistoryStepType.ModelRemoved:
                    Items.Add(new EditorUndoHistoryStepItem(StaticObjects.Data.Models[modelID]));
                    break; 
                default:
                    throw new Exception("Unknown step type: " + StepType);
            }
        }

        public void Apply(bool reverse)
        {
            switch (StepType)
            {
                case UndoHistoryStepType.SelectionModeChanged:
                    foreach (EditorUndoHistoryStepItem item in Items)
                    {
                        item.Apply();
                    }
                    StaticObjects.Data.InputState.SelectionMode = SelectionMode; 
                    break;
                case UndoHistoryStepType.SelectionChange:
                    foreach (EditorUndoHistoryStepItem item in Items)
                    {
                        item.Apply();
                    }
                    StaticObjects.Data.Selection = Selection.Clone();
                    StaticObjects.Data.CacheSelection(); 
                    break;
                case UndoHistoryStepType.ModelAdded:
                    if (reverse == false)
                    {
                        foreach (EditorUndoHistoryStepItem item in Items)
                        {
                            item.AddModel();
                        }
                    }
                    else
                    {
                        foreach (EditorUndoHistoryStepItem item in Items)
                        {
                            item.RemoveModel();
                        }
                    }
                    break;
                case UndoHistoryStepType.ModelRemoved:
                    if (reverse == false)
                    {
                        foreach (EditorUndoHistoryStepItem item in Items)
                        {
                            item.RemoveModel();                            
                        }
                    }
                    else
                    {
                        foreach (EditorUndoHistoryStepItem item in Items)
                        {
                            item.AddModel();
                        }
                    }
                    break;
                case UndoHistoryStepType.ModelModified:
                    foreach (EditorUndoHistoryStepItem item in Items)
                    {
                        item.Apply(); 
                    }
                    break;
                default:
                    break;
            }
        }
    }

    class EditorUndoHistory
    {
        public readonly int HistoryMax = 256;

        public readonly List<EditorUndoHistoryStep> History = new List<EditorUndoHistoryStep>();

        public int Index = 0;        

        public event EventHandler Changed;

        private bool SuspendChanged = false; 

        private void OnChanged()
        {
            if (SuspendChanged == true)
            {
                return; 
            }

            if (Changed != null)
            {
                Changed(this, EventArgs.Empty);
            }
        }

        public void Add(UndoHistoryStepType stepType)
        {
            Add(stepType, -1);
        }

        public void Add(UndoHistoryStepType stepType, int modelID)
        {
            try
            {
                if (stepType == UndoHistoryStepType.SelectionChange)
                {
                    if (History.Count >= 2 && Index >= 2)
                    {
                        if (History[Index - 1].StepType == UndoHistoryStepType.SelectionChange && 
                            History[Index - 2].StepType == UndoHistoryStepType.SelectionChange) 
                        {
                            History.RemoveAt(Index - 1);
                            Index--;
                        }
                    }
                }

                if (History.Count > Index)
                {
                    History.RemoveRange(Index, History.Count - Index);
                }
                
                if (History.Count >= HistoryMax)
                {
                    History.RemoveAt(0);
                    Index--;
                }

                EditorUndoHistoryStep step = null;

                switch (stepType)
                {
                    case UndoHistoryStepType.SelectionModeChanged:
                    case UndoHistoryStepType.SelectionChange:
                    case UndoHistoryStepType.ModelModified:
                        if (modelID != -1)
                        {
                            throw new Exception("Invalid step"); 
                        }

                        step = new EditorUndoHistoryStep(stepType);
                        break;
                    case UndoHistoryStepType.ModelAdded:
                    case UndoHistoryStepType.ModelRemoved:
                        if (modelID == -1)
                        {
                            throw new Exception("Invalid step");
                        }

                        step = new EditorUndoHistoryStep(stepType, modelID);
                        break;
                    default:
                        break;
                }

                if (step == null)
                {
                    throw new Exception("Invalid step"); 
                }

                History.Add(step);

                Index++;
            }             
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            OnChanged(); 
        }

        public void SetUndoIndex(int index)
        {
            try
            {
                SuspendChanged = true;

                while (index < Index)
                {
                    Undo();
                }

                while (index > Index)
                {
                    Redo();
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
            finally
            {
                SuspendChanged = false;
            }

            OnChanged();
        }

        public void Undo()
        {
            try
            {
                if (Index == 0)
                {
                    return;
                }

                Index = Math.Max(0, Index - 1);

                ApplyHistory(History[Index], true);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            OnChanged(); 
        }

        public void Redo()
        {
            try
            {
                if (Index == History.Count)
                {
                    return;
                }

                ApplyHistory(History[Index], false);

                Index = Math.Min(History.Count, Index + 1);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            OnChanged(); 
        }

        private void ApplyHistory(EditorUndoHistoryStep step, bool reverse)
        {
            step.Apply(reverse); 
        }

        public void Clear()
        {
            History.Clear();
            Index = 0;

            OnChanged(); 
        }
    }
}
