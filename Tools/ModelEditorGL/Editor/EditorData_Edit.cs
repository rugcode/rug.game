﻿using OpenTK;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Editor
{
    partial class EditorData
    {
        public void FlipNormals()
        {
            ActionMadeChange = false; 

            foreach (ModelSelectionInfo selection in Selection.Selection)
            {
                if (selection.Array == ModelArrays.Objects) 
                {
                    ModelData data = Models[selection.ModelID].ModelData;

                    for (int i = 0; i < data.Faces.Count; i++)
                    {
                        FaceData face = data.Faces[i];

                        face.FlipNormal();

                        data.Faces[i] = face;

                        ActionMadeChange = true;
                    }

                    data.CalcuateFaceNormals(); 
                }
                else if (selection.Array == ModelArrays.Faces)
                {
                    ModelData data = Models[selection.ModelID].ModelData;

                    for (int i = 0, ie = selection.Indices.Count; i < ie; i++)
                    {
                        int index = selection.Indices[i];

                        FaceData face = data.Faces[index];

                        face.FlipNormal();

                        data.Faces[index] = face;

                        ActionMadeChange = true;
                    }

                    data.CalcuateFaceNormals(); 
                }
            }

            if (ActionMadeChange == true)
            {
                StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.ModelModified);

                UpdateModels();

                NeedsSave = true; 
            }
        }

        public void AddFaces(Vector3 cameraNormal)
        {
            ActionMadeChange = false; 

            foreach (ModelSelectionInfo selection in Selection.Selection)
            {
                if (selection.Array != ModelArrays.Verts)
                {
                    continue; 
                }

                ModelData data = Models[selection.ModelID].ModelData;

                List<int> addedFaces = new List<int>(); 

                for (int i = 0, ie = selection.Indices.Count; i < ie; i += 3)
                {
                    if (i + 2 >= ie)
                    {
                        break; 
                    }

                    int v0 = selection.Indices[i + 0];
                    int v1 = selection.Indices[i + 1];
                    int v2 = selection.Indices[i + 2];

                    FaceData face = new FaceData()
                    {
                        V0= v0, 
                        V1 = v1, 
                        V2 = v2, 
                        Color = InputState.PaintColor, 
                        Group = -1,                          
                    };

                    addedFaces.Add(data.Faces.Count);
                    data.Faces.Add(face); 

                    ActionMadeChange = true; 
                }

                data.CalcuateFaceNormals();

                foreach (int i in addedFaces)
                {
                    FaceData face = data.Faces[i];

                    if (Vector3.Dot(face.Normal, cameraNormal) < 0)
                    {
                        face.FlipNormal(); 
                    }

                    data.Faces[i] = face; 
                }                                  
            }

            if (ActionMadeChange == true)
            {
                StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.ModelModified);

                UpdateModels();

                NeedsSave = true;
            }
        }

        public void RemoveSelectedModels()
        {
            if (InputState.SelectionMode != ModelSelectionMode.Objects)
            {
                return;
            }

            List<int> idsToRemove = new List<int>();

            foreach (ModelSelectionInfo selection in Selection.Selection)
            {
                if (idsToRemove.Contains(selection.ModelID) == false)
                {
                    idsToRemove.Add(selection.ModelID);
                }
            }

            foreach (int id in idsToRemove)
            {
                UndoHistory.Add(UndoHistoryStepType.ModelRemoved, id);
            }

            foreach (int id in idsToRemove)
            {
                RemoveModel(id);
            }

            NeedsSave = true;
        }

        public void DeleteSelected()
        {
            if (InputState.SelectionMode == ModelSelectionMode.Objects)
            {
                RemoveSelectedModels();

                return;
            }

            try
            {
                foreach (EditableModelData data in Models.Values)
                {
                    List<int> facesToRemove = new List<int>();
                    List<int> vertsToRemove = new List<int>();

                    ModelSelectionInfo selection;

                    selection = Selection.FindSelection(data.ModelID, ModelArrays.Faces);

                    if (selection != null)
                    {
                        selection.Sort();
                        facesToRemove.AddRange(selection.Indices);
                    }

                    selection = Selection.FindSelection(data.ModelID, ModelArrays.Verts);

                    if (selection != null)
                    {
                        selection.Sort();
                        vertsToRemove.AddRange(selection.Indices);

                        for (int i = 0; i < data.ModelData.Faces.Count; i++)
                        {
                            FaceData face = data.ModelData.Faces[i];

                            for (int j = vertsToRemove.Count - 1; j >= 0; j--)
                            {
                                int vertIndex = selection.Indices[j];

                                if (face.Contains(vertIndex) == true)
                                {
                                    if (facesToRemove.Contains(i) == false)
                                    {
                                        facesToRemove.Add(i);
                                        //break;
                                    }
                                }
                                else if (face.HasGreater(vertIndex) == true)
                                {
                                    face.AdjustIndices(vertIndex);

                                    data.ModelData.Faces[i] = face;
                                }
                            }
                        }
                    }

                    vertsToRemove.Sort();
                    facesToRemove.Sort();

                    for (int i = vertsToRemove.Count - 1; i >= 0; i--)
                    {
                        data.ModelData.Verts.RemoveAt(vertsToRemove[i]);
                    }

                    for (int i = facesToRemove.Count - 1; i >= 0; i--)
                    {
                        data.ModelData.Faces.RemoveAt(facesToRemove[i]);
                    }

                    NeedsSave |= vertsToRemove.Count > 0 || facesToRemove.Count > 0;
                }

                Selection.Clear();
                SelectionCache.Clear();

                NeedsSave = true;

                UpdateModels();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }     

        public void AdjustSelection_Move(Vector3 move, bool @override)
        {
            foreach (ModelSelectionInfo selection in Selection.Selection)
            {
                EditorModelSelectionCache cache = SelectionCache.Caches[selection.ModelID];

                AdjustSelection_Move(selection, cache, move, @override);
            }

            //UpdateModels();
        }

        private void AdjustSelection_Move(ModelSelectionInfo selection, EditorModelSelectionCache cache, Vector3 move, bool @override)
        {
            EditableModelData modelData = Models[selection.ModelID];

            Vector3 mouseResolve = DoSnap(move, !@override);

            switch (selection.Array)
            {
                case ModelArrays.Verts:
                    foreach (int index in selection.Indices)
                    {
                        modelData.ModelData.Verts[index] = DoSnap_Changing(cache.Verts[index], cache.Verts[index] + mouseResolve, @override);
                    }
                    break;
                case ModelArrays.Faces:
                    foreach (int index in selection.Indices)
                    {
                        FaceData face = modelData.ModelData.Faces[index];

                        modelData.ModelData.Verts[face.V0] = DoSnap_Changing(cache.Verts[face.V0], cache.Verts[face.V0] + mouseResolve, @override);
                        modelData.ModelData.Verts[face.V1] = DoSnap_Changing(cache.Verts[face.V1], cache.Verts[face.V1] + mouseResolve, @override);
                        modelData.ModelData.Verts[face.V2] = DoSnap_Changing(cache.Verts[face.V2], cache.Verts[face.V2] + mouseResolve, @override);
                    }
                    break;
                case ModelArrays.Objects:
                    foreach (int index in selection.Indices)
                    {
                        if (index == -1)
                        {
                            modelData.ActivePosition = DoSnap_Changing(cache.Location, cache.Location + mouseResolve, @override);
                        }
                        else
                        {
                            ModelPrimitive primitive = modelData.ModelData.HullPrimitives[index];

                            primitive.Center = DoSnap_Changing(cache.HullPrimitives[index].Center, cache.HullPrimitives[index].Center + mouseResolve, @override); 

                            modelData.ModelData.HullPrimitives[index] = primitive; 
                        }
                    }
                    break;
                /* 
            case ModelArrays.Extras:
                int index = (selection.Index - (selection.Index % 2)) / 2;

                ExtraData data = ModelData.Extras[index];

                if (selection.Index % 2 == 0)
                {
                    data.Position = DoSnap(selection.InitialValue[0] + mouseResolve, @override);
                }
                else
                {
                    data.Direction = DoSnap(selection.InitialValue[0] + mouseResolve, @override);

                    if (data.Direction == Vector3.Zero)
                    {
                        data.Direction = Vector3.UnitY;
                    }

                    if (data.Type != ExtraTypes.Volume)
                    {
                        data.Direction.Normalize();
                    }
                }

                ModelData.Extras[index] = data;
                break;
                */
                default:
                    break;
            }
        }

        public void AdjustSelection_Rotate(Vector3 axis, float angleChange, bool snap)
        {
            if (snap == true)
            {
                float PiOver8 = MathHelper.PiOver4 * 0.25f;

                float count = angleChange / PiOver8;

                angleChange = PiOver8 * (float)(int)(count - 0.5f);
            }

            foreach (ModelSelectionInfo selection in Selection.Selection)
            {
                EditorModelSelectionCache cache = SelectionCache.Caches[selection.ModelID];

                AdjustSelection_Rotate(selection, cache, axis, angleChange);
            }
        }

        private void AdjustSelection_Rotate(ModelSelectionInfo selection, EditorModelSelectionCache cache, Vector3 axis, float angle)
        {
            EditableModelData modelData = Models[selection.ModelID];

            switch (selection.Array)
            {
                case ModelArrays.Objects:
                    foreach (int index in selection.Indices)
                    {
                        if (index == -1)
                        {
                            modelData.ActiveRotation = Quaternion.Multiply(Quaternion.FromAxisAngle(axis, angle), cache.Rotation);
                            modelData.ActiveRotation.Normalize();
                            modelData.ActivePosition = Vector3.Transform(cache.Location - Selection.Center, modelData.ActiveRotation) + Selection.Center;
                        }
                        else
                        {
                            ModelPrimitive primitive = modelData.ModelData.HullPrimitives[index];

                            Quaternion orientation = Quaternion.Multiply(Quaternion.FromAxisAngle(axis, angle), cache.HullPrimitives[index].Orientation); 
                            
                            orientation.Normalize();
                            
                            primitive.Orientation = orientation;

                            primitive.Center = Vector3.Transform(cache.HullPrimitives[index].Center - Selection.Center, orientation) + Selection.Center;

                            modelData.ModelData.HullPrimitives[index] = primitive; 
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        public void AdjustSelection_Scale(Vector3 scale, Vector3 end, bool isUniform)
        {
            //Vector3 startScale = Selection.Center;

            //Vector3 move = (end - start) - Selection.Center;

            //scale = DoSnap(scale, false);

            /* 
            if (isUniform == true)
            {
                float max = Math.Max(Math.Max(Math.Abs(move.X), Math.Abs(move.Y)), Math.Abs(move.Z));

                if (Math.Sign(move.X) == -1 ||
                    Math.Sign(move.Y) == -1 ||
                    Math.Sign(move.Z) == -1)
                {
                    max *= -1f;
                }

                move = new Vector3(max, max, max);
            }
            */ 

            foreach (ModelSelectionInfo selection in Selection.Selection)
            {
                EditorModelSelectionCache cache = SelectionCache.Caches[selection.ModelID];

                AdjustSelection_Scale(selection, cache, scale);
            }
        }

        private void AdjustSelection_Scale(ModelSelectionInfo selection, EditorModelSelectionCache cache, Vector3 scale)
        {
            EditableModelData modelData = Models[selection.ModelID];

            switch (selection.Array)
            {
                case ModelArrays.Objects:
                    foreach (int index in selection.Indices)
                    {
                        if (index == -1)
                        {
                            modelData.ActiveScale = cache.Scale * scale;
                            modelData.ActivePosition = ((cache.Location - Selection.Center) * modelData.ActiveScale) + Selection.Center;
                        }
                        else
                        {
                            ModelPrimitive primitive = modelData.ModelData.HullPrimitives[index];

                            Vector3 localScale = Vector3.Transform(scale, primitive.Orientation);

                            primitive.Scale = cache.HullPrimitives[index].Scale * (cache.Scale * scale);

                            primitive.Center = ((cache.HullPrimitives[index].Center - Selection.Center) * primitive.Scale) + Selection.Center;

                            modelData.ModelData.HullPrimitives[index] = primitive;
                        }
                    }

                    break;
                default:
                    break;
            }
        }

        public Vector3 DoSnap(Vector3 value, bool @override)
        {
            if ((InputState.CurrentAction == EditorInputAction.Move_Snap ||
                InputState.CurrentAction == EditorInputAction.Scale_Snap ||
                InputState.CurrentAction == EditorInputAction.Scale_Uniform_Snap ||
                InputState.CurrentAction == EditorInputAction.Rotate_Snap) &&
                @override == false)
            {
                value *= InputState.GridScale;

                value.X = (float)Math.Round(value.X);
                value.Y = (float)Math.Round(value.Y);
                value.Z = (float)Math.Round(value.Z);

                value *= 1f / InputState.GridScale;
            }

            return value;
        }

        private Vector3 DoSnap_Changing(Vector3 initial, Vector3 value, bool @override)
        {
            if (InputState.SnapToGrid == true && @override == false)
            {
                value *= InputState.GridScale;

                value.X = (float)Math.Round(value.X);
                value.Y = (float)Math.Round(value.Y);
                value.Z = (float)Math.Round(value.Z);

                value *= 1f / InputState.GridScale;
            }

            ActionMadeChange |= initial != value;
            NeedsSave |= ActionMadeChange;

            return value;
        }

        public void Paint(ModelSelectionInfo selection)
        {
            if (selection == null)
            {
                return;
            }

            if (selection.Array != ModelArrays.Faces)
            {
                return;
            }

            ModelSelectionInfo existingSelection = Selection.FindSelection(selection.ModelID, ModelArrays.Faces);

            if ((Selection.FacesTotal != 0 || Selection.PointsTotal != 0) && existingSelection == null)
            {
                return;
            }

            EditableModelData modelData = Models[selection.ModelID];

            foreach (int index in selection.Indices)
            {
                if (existingSelection != null && existingSelection.Indices.Contains(index) == false)
                {
                    continue;
                }

                SetFaceColor(modelData.ModelData, index, InputState.PaintColor);

                ActionMadeChange = true;
                NeedsSave = true;
            }
        }

        public void Paint_Flood(ModelSelectionInfo selection)
        {
            if (selection == null)
            {
                return;
            }

            if (selection.Array != ModelArrays.Faces)
            {
                return;
            }

            ModelSelectionInfo existingSelection = Selection.FindSelection(selection.ModelID, ModelArrays.Faces);

            if ((Selection.FacesTotal != 0 || Selection.PointsTotal != 0) && existingSelection == null)
            {
                return;
            }

            EditableModelData modelData = Models[selection.ModelID];

            if (existingSelection == null)
            {
                for (int index = 0; index < modelData.ModelData.Faces.Count; index++)
                {
                    SetFaceColor(modelData.ModelData, index, InputState.PaintColor);

                    ActionMadeChange = true;
                    NeedsSave = true;
                }
            }
            else
            {
                foreach (int index in existingSelection.Indices)
                {
                    SetFaceColor(modelData.ModelData, index, InputState.PaintColor);

                    ActionMadeChange = true;
                    NeedsSave = true;
                }
            }
        }

        private void SetFaceColor(ModelData modelData, int index, System.Drawing.Color color)
        {
            FaceData data = modelData.Faces[index];

            data.Color = color;

            modelData.Faces[index] = data;
        }

        public void Pipette(ModelSelectionInfo selection)
        {
            if (selection == null)
            {
                return;
            }

            if (selection.Array != ModelArrays.Faces)
            {
                return;
            }

            ModelSelectionInfo existingSelection = Selection.FindSelection(selection.ModelID, ModelArrays.Faces);

            if ((Selection.FacesTotal != 0 || Selection.PointsTotal != 0) && existingSelection == null)
            {
                return;
            }

            EditableModelData modelData = Models[selection.ModelID];

            foreach (int index in selection.Indices)
            {
                if (existingSelection != null && existingSelection.Indices.Contains(index) == false)
                {
                    continue;
                }

                InputState.PaintColor = (Color)modelData.ModelData.Faces[index].Color;                
            }

            OnEditorControlsNeedUpdate();
        }
    }
}
