﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    enum ViewModes
    {
        Default,
        Top,
        Bottom,
        Left,
        Right,
        Front,
        Back,
        Iso,
    }

    enum VisibleItemFlags : int
    {
        None = 0,
        Vertices = 1,
        WireFrame = 2,
        Faces = 4,
        Normals = 8,
        Hull = 16,
        Extras = 32,
        Decomposed = 64,
        Solid = 128,
        Objects = 256, 
    }

    enum ModelSelectionMode : int
    {
        //All = 0, 
        Vertices = 0,
        Edges = 1,
        Faces = 2,
        Objects = 3,
        //FacesCenter = 2,
    }

    enum EditorInputAction
    {
        None,

        ViewPan,
        ViewRotate,

        Select_Single,
        AddSelect_Single,
        SubtractSelect_Single,

        Select_Rect,
        AddSelect_Rect,
        SubtractSelect_Rect,

        Select_Rect_Through,
        AddSelect_Rect_Through,
        SubtractSelect_Rect_Through,

        Select_Marque,
        AddSelect_Marque,
        SubtractSelect_Marque,

        Move,
        Move_Snap,

        Paint,
        Paint_Flood,

        Rotate,
        Rotate_Snap,

        Scale,
        Scale_Uniform,
        Scale_Snap,
        Scale_Uniform_Snap,
        Pipette,
    }

    enum EditorToolMode : int
    {
        Select = 0,
        Move = 1,
        Paint = 2,
        Scale = 3,
        Rotate = 4,
        Pipette = 5,
    }

    enum EditorLightingMode : int 
    {
        None = 0, 
        StrongWhite = 1, 
        OverAndUnder = 2,
        AllAround = 3, 
    }
}
