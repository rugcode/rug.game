﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Maths;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Effect;
using Rug.Game.Flat;
using Rug.Game.Flat.Models;
using Rug.Game.Flat.Shaders.Flat;
using Rug.Game.Physics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Environment = Rug.Game.Environment; 

namespace ModelEditorGL.Editor
{
    class EditorPanel
    {
        private ResourceManager ResourceManagers = new ResourceManager();
        private ResourceSet Resources = new ResourceSet("Resources", ResourceMode.Static);

        private ModelSceneLight[] Lights = new ModelSceneLight[4];

        private View3D m_View;
        private ModelLighting Lighting;
        private float m_Time;
        private float VertSize = 3f; 

        private PhysicsDebugLines m_GridLines;
        private PhysicsDebugLines m_OverlayLines;
        private PhysicsDebugLines m_SelectionLines;

        [Browsable(false)]
        public EditorLightingMode ModelLighting = EditorLightingMode.OverAndUnder;
        
        [Browsable(false)]
        public Rectangle Bounds { get; set; }

        [Browsable(false)]
        public ViewModes ViewMode { get; set; }

        [Browsable(false)]
        public VisibleItemFlags VisibleItems { get; set; }

        [Browsable(false)]
        public Control Control { get; set; }

        public Vector3 CameraNormal { get; private set; } 

        public EditorPanel()
        {
            m_View = new View3D(Bounds, 1, 1, (float)Math.PI / 4, 1f);
        }

        internal void Load()
        {
            VisibleItems =
                VisibleItemFlags.Solid | 
                VisibleItemFlags.Vertices | 
                VisibleItemFlags.Normals | 
                VisibleItemFlags.WireFrame | 
                VisibleItemFlags.Faces;

            m_View.Resize(Bounds, Bounds.Width, Bounds.Height);
            m_View.ProjectionKind = ProjectionKind.Orthographic;
            m_View.Camera.SmoothCamera = false;

            ModelEffect.RegisterAllShaders(ModelLightingMode.DiffuseSpecular | ModelLightingMode.Color, 0, Lights.Length, ModelTextureMode.Untextured);
            //ModelEffect.RegisterShaders(ModelLightingMode.DiffuseSpecular, 0, 2, ModelTextureMode.Textured);
            SharedEffects.Effects.Add("Simple_Particles", new Simple_Particles());
            SharedEffects.Effects.Add("Debug_Lines", new Debug_Lines());

            ModelHelper.DefaultSpecAmount = 0.25f;
            ModelHelper.DefaultSpecFactor = 0.2f;

            SharedEffects.LoadResources();

            ResourceManagers.Add(StaticObjects.Data);

            Lighting = new ModelLighting(new ModelLightingMode[] { ModelLightingMode.DiffuseSpecular | ModelLightingMode.Color, ModelLightingMode.None }, 0, Lights.Length);
            Lights[0] = new ModelSceneLight()
            {
                Center = new Vector3(30, 80, 60) * 8000f,
                Color = Color4.White,
                SpecularPower = 1.2f,
                Radius = float.PositiveInfinity,
                AttenuatuionTermA = 0f,
                Intensity = 0.8f,
                Directional = false,
            };

            Lights[1] = new ModelSceneLight()
            {
                Center = new Vector3(60, -80, 30) * 8000f,
                Color = Color4.SkyBlue,
                SpecularPower = 1.2f,
                Radius = float.PositiveInfinity,
                AttenuatuionTermA = 0f,
                Intensity = 0.5f,
                Directional = false,
            };

            Lights[2] = new ModelSceneLight()
            {
                Center = new Vector3(-60, -80, 30) * 8000f,
                Color = Color4.SkyBlue,
                SpecularPower = 1.2f,
                Radius = float.PositiveInfinity,
                AttenuatuionTermA = 0f,
                Intensity = 0.5f,
                Directional = false,
            };

            Lights[3] = new ModelSceneLight()
            {
                Center = new Vector3(0, -30, -60) * 8000f,
                Color = Color4.SkyBlue,
                SpecularPower = 1.2f,
                Radius = float.PositiveInfinity,
                AttenuatuionTermA = 0f,
                Intensity = 0.5f,
                Directional = false,
            };

            ResourceManagers.Add(Lighting);

            m_GridLines = new PhysicsDebugLines(512);
            ResourceManagers.Add(m_GridLines);

            m_OverlayLines = new PhysicsDebugLines(16);
            ResourceManagers.Add(m_OverlayLines);
           
            m_SelectionLines = new PhysicsDebugLines(2048);
            ResourceManagers.Add(m_SelectionLines);

            ResourceManagers.LoadResources();

            CreateGridLines();

            Lighting.LightingArguments.FogParams.Factor = 0f;
            Lighting.LightingArguments.RedParams.Factor = -1f; 

            StaticObjects.Data.UpdateModels();       

            Control.Invalidate();
        }

        #region CreateGridLines

        private void CreateGridLines()
        {
            int index = 0;

            Color4 gridColor = new Color4(255, 255, 255, 32);

            {
                Vector3 xMin = Vector3.UnitX * -1000f;
                Vector3 xMax = Vector3.UnitX * 1000f;

                Vector3 yMin = Vector3.UnitY * -1000f;
                Vector3 yMax = Vector3.UnitY * 1000f;

                Vector3 zMin = Vector3.UnitZ * -1000f;
                Vector3 zMax = Vector3.UnitZ * 1000f;

                AddLines(m_GridLines, ref index, gridColor,
                    new Vector3[] 
                        { 
                            xMin, xMax, 
                            yMin, yMax, 
                            zMin, zMax, 
                        });
            }

            for (int i = 1; i < 8; i++)
            {
                Vector3 TopLeft = new Vector3(-i, 0, -i);
                Vector3 TopRight = new Vector3(i, 0, -i);

                Vector3 BottomLeft = new Vector3(-i, 0, i);
                Vector3 BottomRight = new Vector3(i, 0, i);

                AddLines(m_GridLines, ref index, gridColor,
                    new Vector3[] 
				        {					
					        TopLeft, TopRight,
					        TopRight, BottomRight,
					        BottomRight, BottomLeft,
					        BottomLeft, TopLeft,
				        });
            }

            for (int i = 1; i < 8; i++)
            {
                Vector3 TopLeft = new Vector3(0, -i, -i);
                Vector3 TopRight = new Vector3(0, i, -i);

                Vector3 BottomLeft = new Vector3(0, -i, i);
                Vector3 BottomRight = new Vector3(0, i, i);

                AddLines(m_GridLines, ref index, gridColor,
                    new Vector3[] 
				        {					
					        TopLeft, TopRight,
					        TopRight, BottomRight,
					        BottomRight, BottomLeft,
					        BottomLeft, TopLeft,
				        });
            }


            for (int i = 1; i < 8; i++)
            {
                Vector3 TopLeft = new Vector3(-i, -i, 0);
                Vector3 TopRight = new Vector3(i, -i, 0);

                Vector3 BottomLeft = new Vector3(-i, i, 0);
                Vector3 BottomRight = new Vector3(i, i, 0);

                AddLines(m_GridLines, ref index, gridColor,
                    new Vector3[] 
				        {					
					        TopLeft, TopRight,
					        TopRight, BottomRight,
					        BottomRight, BottomLeft,
					        BottomLeft, TopLeft,
				        });
            }

            /* 
            bool drawAxis = false;
            Vector3 axisCenter = Vector3.Zero;

            if (HasValidSelection == true &&
                Selection.Array == ModelArrays.Verts)
            {
                axisCenter = ModelData.Verts[Selection.Index];
                drawAxis = true;
            }
            else if (HasValidSelection == true &&
                Selection.Array == ModelArrays.Extras)
            {
                int index = (Selection.Index - (Selection.Index % 2)) / 2;

                axisCenter = ModelData.Extras[index].Position;
                drawAxis = true;
            }

            if (drawAxis == true)
            {
                Vector3 yAxis0 = Vector3.Transform(new Vector3(-10f, axisCenter.Y, axisCenter.Z), transform);
                Vector3 yAxis1 = Vector3.Transform(new Vector3(10f, axisCenter.Y, axisCenter.Z), transform);

                Vector3 zAxis0 = Vector3.Transform(new Vector3(axisCenter.X, axisCenter.Y, -10f), transform);
                Vector3 zAxis1 = Vector3.Transform(new Vector3(axisCenter.X, axisCenter.Y, 10f), transform);

                Vector3 zAxis2 = Vector3.Transform(new Vector3(-axisCenter.X, axisCenter.Y, -10f), transform);
                Vector3 zAxis3 = Vector3.Transform(new Vector3(-axisCenter.X, axisCenter.Y, 10f), transform);

                Vector3 xAxis0 = Vector3.Transform(new Vector3(axisCenter.X, -10f, axisCenter.Z), transform);
                Vector3 xAxis1 = Vector3.Transform(new Vector3(axisCenter.X, 10f, axisCenter.Z), transform);

                Vector3 xAxis2 = Vector3.Transform(new Vector3(-axisCenter.X, -10f, axisCenter.Z), transform);
                Vector3 xAxis3 = Vector3.Transform(new Vector3(-axisCenter.X, 10f, axisCenter.Z), transform);

                g.DrawLine(m_GridPen, new PointF(yAxis0.X, yAxis0.Y), new PointF(yAxis1.X, yAxis1.Y));

                g.DrawLine(m_GridPen, new PointF(zAxis0.X, zAxis0.Y), new PointF(zAxis1.X, zAxis1.Y));
                g.DrawLine(m_GridPen, new PointF(zAxis2.X, zAxis2.Y), new PointF(zAxis3.X, zAxis3.Y));

                g.DrawLine(m_GridPen, new PointF(xAxis0.X, xAxis0.Y), new PointF(xAxis1.X, xAxis1.Y));
                g.DrawLine(m_GridPen, new PointF(xAxis2.X, xAxis2.Y), new PointF(xAxis3.X, xAxis3.Y));
            }
             * */

            m_GridLines.InstanceCount = index;
            m_GridLines.Update();
        }

        #endregion

        private void AddLines(PhysicsDebugLines lines, ref int index, Color4 color, Vector3[] verts)
        {
            for (int i = 0; i < verts.Length; i += 2) 
            {
                lines.Starts[index] = verts[i];
                lines.Ends[index] = verts[i + 1];
                lines.Colors[index++] = color; 
            }
        }

        public void Dispose()
        {
            ResourceManagers.Dispose();

            SharedEffects.UnloadResources();
        }

        public void Resize()
        {
            if (m_View.NeedsResize(Bounds.Size) == true)
            {
                m_View.Resize(Bounds, Bounds.Width, Bounds.Height);
            }

            GLState.Viewport = m_View.Viewport;
            GLState.Apply(m_View);
        }

        #region Render

        public void Render(float delta)
        {
            ModelLightingMode renderMode = ModelLightingMode.DiffuseSpecular;

            switch (ModelLighting)
            {
                case EditorLightingMode.None:
                    renderMode = ModelLightingMode.Color; 
                    Lights[0].Intensity = 1;
                    Lights[1].Intensity = 0;
                    Lights[2].Intensity = 0;
                    Lights[3].Intensity = 0; 
                    break;
                case EditorLightingMode.StrongWhite:
                    Lights[0].Intensity = 1f;
                    Lights[1].Intensity = 0;
                    Lights[2].Intensity = 0;
                    Lights[3].Intensity = 0; 
                    break;
                case EditorLightingMode.OverAndUnder:
                    Lights[0].Intensity = 0.8f;
                    Lights[1].Intensity = 0.8f;
                    Lights[2].Intensity = 0;
                    Lights[3].Intensity = 0; 
                    break;
                case EditorLightingMode.AllAround:
                    Lights[0].Intensity = 0.8f;
                    Lights[1].Intensity = 0.5f;
                    Lights[2].Intensity = 0.5f;
                    Lights[3].Intensity = 0.5f; 
                    break;
                default:
                    break;
            }

            Viewport backup = m_View.Viewport; 

            GLState.CullFace(OpenTK.Graphics.OpenGL.CullFaceMode.Back);
            GLState.EnableCullFace = true;
            GLState.EnableDepthMask = true;
            GLState.EnableDepthTest = true;
            GLState.EnableBlend = true;

            GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GLState.BlendEquation(BlendEquationMode.FuncAdd);
            GLState.ClearDepth(1.0f);
            GLState.Viewport = backup;

            GLState.ApplyAll(m_View);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            m_View.Camera.Update();
            m_View.UpdateProjection();

            foreach (ModelSceneLight light in Lights)
            {
                light.Update();
            }

            foreach (EditableModelData data in StaticObjects.Data.Models.Values)
            {
                data.UpdateTransform(); 
            }
             
            try
            {
                switch (ViewMode)
                {
                    case ViewModes.Default:
                        int sizeX = m_View.Viewport.Width / 2;
                        int sizeY = m_View.Viewport.Height / 2;

                        RenderView(ViewModes.Top, new Rectangle(0, 0, sizeX, sizeY), renderMode);
                        RenderView(ViewModes.Front, new Rectangle(0, sizeY, sizeX, sizeY), renderMode);
                        RenderView(ViewModes.Left, new Rectangle(sizeX, sizeY, sizeX, sizeY), renderMode);
                        RenderView(ViewModes.Iso, new Rectangle(sizeX, 0, sizeX, sizeY), renderMode);
                        break;
                    default:
                        RenderView(ViewMode, new Rectangle(0, 0, m_View.Viewport.Width, m_View.Viewport.Height), renderMode);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            GLState.Viewport = backup;

            GLState.Apply(m_View);

            GL.Clear(ClearBufferMask.DepthBufferBit);

            try
            {
                int index = 0;

                float pixelX = m_View.PixelSize.X;
                float pixelY = m_View.PixelSize.Y;

                Vector3 topLeft = new Vector3(-1 + pixelX, -1 + pixelY, 0.5f);
                Vector3 topRight = new Vector3(1 - pixelX, -1 + pixelY, 0.5f);

                Vector3 botLeft = new Vector3(-1 + pixelX, 1 - pixelY, 0.5f);
                Vector3 botRight = new Vector3(1 - pixelX, 1 - pixelY, 0.5f);

                switch (ViewMode)
                {
                    case ViewModes.Default:
                        AddLines(m_OverlayLines, ref index, Color4.Black, new Vector3[] 
                        {
                            new Vector3(0, -1, 0.5f), new Vector3(0, 1, 0.5f), 
                            new Vector3(-1, 0, 0.5f), new Vector3(1, 0, 0.5f), 

                            topLeft, topRight, 
                            topRight, botRight,
                            botRight, botLeft,
                            botLeft, topLeft,
                        });
                        break;
                    default:
                        AddLines(m_OverlayLines, ref index, Color4.Black, new Vector3[] 
                        {
                            topLeft, topRight, 
                            topRight, botRight,
                            botRight, botLeft,
                            botLeft, topLeft,
                        });
                        break;
                }

                m_OverlayLines.InstanceCount = index;
                m_OverlayLines.Update();

                m_OverlayLines.Render_Identity(m_View); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void RenderView(ViewModes viewMode, Rectangle rectangle, ModelLightingMode mode)
        {
            Matrix4 transform;
            Quaternion rotation;
            StaticObjects.Data.InputState.GetMatrix(viewMode, rectangle, out transform, out rotation);

            float ratio = (float)rectangle.Width / (float)rectangle.Height;

            m_View.OrthographicBoxSize = new Vector3(StaticObjects.Data.InputState.Zoom * ratio, StaticObjects.Data.InputState.Zoom, 1000f);
            m_View.Camera.Offset = -500f;
            m_View.Camera.Center = -StaticObjects.Data.InputState.Center; 
            m_View.Camera.SetRotation(rotation);            
            m_View.UpdateProjection();

            for (int i = 0; i < Lights.Length; i++)
            {
                Lighting.Lights[i] = Lights[i].ToLightInstance(ref m_View.World, ref m_View.NormalWorld);
            }

            Lighting.UpdateLightingArguments(m_View);
            Lighting.Update();

            GLState.EnableDepthTest = true;
            GLState.Viewport = new Viewport(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
            GLState.Apply(m_View);

            Color4 color = new Color4(1f, 1f, 1f, BitConverter.ToSingle(Vector2h.GetBytes(new Vector2h(0.9f, 1f)), 0));

            if ((VisibleItems & VisibleItemFlags.Solid) == VisibleItemFlags.Solid)
            {
                Matrix4 objectMatrix = transform;
                Matrix4 normalMatrix = Matrix4.Identity;

                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.Model.Render(m_View, data.Transform, data.NormalTransform, color, null, Lighting, mode);
                }                
            }

            if ((VisibleItems & VisibleItemFlags.WireFrame) == VisibleItemFlags.WireFrame)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.Wireframe.Render(m_View, ref data.Transform); 
                }                                
            }

            if ((VisibleItems & VisibleItemFlags.Hull) == VisibleItemFlags.Hull)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.HullFrames.Render(m_View, ref data.Transform); 
                }                                
            }            

            if ((VisibleItems & VisibleItemFlags.Normals) == VisibleItemFlags.Normals)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.FaceNormals.Render(m_View, ref data.Transform); 
                }                
            }

            GLState.EnableDepthTest = false;
            GLState.Apply(m_View);

            //GL.Clear(ClearBufferMask.DepthBufferBit); 

            if ((VisibleItems & VisibleItemFlags.Vertices) == VisibleItemFlags.Vertices)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.Vertices.ParticleScale = ((StaticObjects.Data.InputState.Zoom * ratio) / (float)rectangle.Width) * VertSize;
                    data.Vertices.Render(m_View, ref data.Transform);
                }                 
            }
           
            if ((VisibleItems & VisibleItemFlags.Faces) == VisibleItemFlags.Faces)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.FaceVertices.ParticleScale = ((StaticObjects.Data.InputState.Zoom * ratio) / (float)rectangle.Width) * VertSize;
                    data.FaceVertices.Render(m_View, ref data.Transform);
                }
            }

            //GL.Clear(ClearBufferMask.DepthBufferBit);
            if ((VisibleItems & VisibleItemFlags.Objects) == VisibleItemFlags.Objects)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    data.ObjectCenters.ParticleScale = ((StaticObjects.Data.InputState.Zoom * ratio) / (float)rectangle.Width) * VertSize * 2;
                    //data.ObjectCenters.Render(m_View, ref data.Transform);
                    data.ObjectCenters.Render(m_View);

                    //data.ObjectLines.Render(m_View, ref data.Transform);
                    data.ObjectLines.Render(m_View);
                }
            }

            m_GridLines.Render(m_View);

            if (StaticObjects.Data.InputState.SelectionView == viewMode && StaticObjects.Data.InputState.SelectionRectangle != Rectangle.Empty)
            {
                Rectangle selection = StaticObjects.Data.InputState.SelectionRectangle;

                PointF start = new PointF(selection.Left - rectangle.X, selection.Top - rectangle.Y);
                PointF end = new PointF(selection.Right - rectangle.X, selection.Bottom - rectangle.Y);

                ConvertMouseToView(ref rectangle, ref start);
                ConvertMouseToView(ref rectangle, ref end);

                Vector3 topLeft = new Vector3(start.X, start.Y, 0.5f);
                Vector3 topRight = new Vector3(end.X, start.Y, 0.5f);

                Vector3 botLeft = new Vector3(start.X, end.Y, 0.5f);
                Vector3 botRight = new Vector3(end.X, end.Y, 0.5f);

                int index = 0; 

                AddLines(m_SelectionLines, ref index, Color4.Orange, new Vector3[] 
                        {
                            topLeft, topRight, 
                            topRight, botRight,
                            botRight, botLeft,
                            botLeft, topLeft,
                        });

                m_SelectionLines.InstanceCount = index;
                m_SelectionLines.Update();

                m_SelectionLines.Render_Identity(m_View); 
            }
        }

        #endregion 

        #region View Methods

        private static void ConvertMouseToView(ref Rectangle rectangle, ref PointF point)
        {
            point.X /= (float)rectangle.Width;
            point.Y /= (float)rectangle.Height;

            point.X = point.X * 2f - 1f;
            point.Y = point.Y * 2f - 1f;

            point.Y *= -1f;
        }

        private static void ConvertMouseToView(ref Rectangle rectangle, ref Vector2 point)
        {
            point.X /= (float)rectangle.Width;
            point.Y /= (float)rectangle.Height;

            point.X = point.X * 2f - 1f;
            point.Y = point.Y * 2f - 1f;

            point.Y *= -1f;
        }

        private void GetViewModeAndRect(Point location, out ViewModes mode, out Rectangle rect)
        {
            switch (ViewMode)
            {
                case ViewModes.Default:
                    int sizeX = Bounds.Width / 2;
                    int sizeY = Bounds.Height / 2;

                    rect = new Rectangle(0, 0, sizeX, sizeY);

                    if (rect.Contains(location) == true)
                    {
                        mode = ViewModes.Top;
                        return;
                    }

                    rect = new Rectangle(0, sizeY, sizeX, sizeY);

                    if (rect.Contains(location) == true)
                    {
                        mode = ViewModes.Front;
                        return;
                    }

                    rect = new Rectangle(sizeX, sizeY, sizeX, sizeY);

                    if (rect.Contains(location) == true)
                    {
                        mode = ViewModes.Left;
                        return;
                    }

                    rect = new Rectangle(sizeX, 0, sizeX, sizeY);
                    if (rect.Contains(location) == true)
                    {
                        mode = ViewModes.Iso;
                        return;
                    }

                    throw new Exception("Invalid point");
                default:
                    mode = ViewMode;
                    rect = new Rectangle(0, 0, Bounds.Width, Bounds.Height);
                    break;
            }
        }

        #region Setup View And Transform For Mouse

        private Matrix4 SetupViewAndTransformForMouse(ViewModes mode, ref Rectangle rectangle)
        {
            Matrix4 transform;
            Quaternion rotation;
            StaticObjects.Data.InputState.GetMatrix(mode, rectangle, out transform, out rotation);

            float ratio = (float)rectangle.Width / (float)rectangle.Height;

            m_View.OrthographicBoxSize = new Vector3(StaticObjects.Data.InputState.Zoom * ratio, StaticObjects.Data.InputState.Zoom, 1000f);
            m_View.Camera.Offset = -500f;
            m_View.Camera.Center = -StaticObjects.Data.InputState.Center;
            m_View.Camera.SetRotation(rotation);
            m_View.UpdateProjection();

            transform = m_View.View * m_View.Projection;

            return transform;
        }

        #endregion

        #endregion
                
        #region Input Events

        public void KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete ||
                e.KeyCode == Keys.Back)
            {
                StaticObjects.Data.DeleteSelected();

                Control.Invalidate();
            }

            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                StaticObjects.Data.AddFaces(CameraNormal);

                Control.Invalidate();
            }

            if (e.KeyCode == Keys.F)
            {
                StaticObjects.Data.FlipNormals();

                Control.Invalidate();
            }

            if (e.KeyCode == Keys.Escape)
            {
                StaticObjects.Data.Selection.Clear();
                StaticObjects.Data.CacheSelection();
                StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);
                StaticObjects.Data.UpdateModels();
                Control.Invalidate(); 
            }
        }

        public void KeyUp(object sender, KeyEventArgs e)
        {
            
        }

        public void KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            
        }

        public void MouseWheel(object sender, MouseEventArgs e)
        {
            StaticObjects.Data.InputState.DoZoom(e.Delta); 

            Control.Invalidate(); 
        }

        public void MouseUp(object sender, MouseEventArgs e)
        {           
            switch (StaticObjects.Data.InputState.CurrentAction)
            {
                case EditorInputAction.None:
                    break;
                case EditorInputAction.ViewPan:
                case EditorInputAction.ViewRotate:
                    break;
                case EditorInputAction.Select_Single:
                case EditorInputAction.AddSelect_Single:
                case EditorInputAction.SubtractSelect_Single:
                    if (StaticObjects.Data.ActionMadeChange == true)
                    {
                        StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);
                    }
                    break;
                case EditorInputAction.Select_Rect:
                case EditorInputAction.AddSelect_Rect:
                case EditorInputAction.SubtractSelect_Rect:
                    StaticObjects.Data.InputState.EndSelect_Rect(e);
                    DoSelection_Rect(StaticObjects.Data.InputState.CurrentAction, false); 
                    StaticObjects.Data.InputState.ClearSelect_Rect();
                    if (StaticObjects.Data.ActionMadeChange == true)
                    {
                        StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);
                    }
                    break;
                case EditorInputAction.Select_Rect_Through:
                case EditorInputAction.AddSelect_Rect_Through:
                case EditorInputAction.SubtractSelect_Rect_Through:
                    StaticObjects.Data.InputState.EndSelect_Rect(e);
                    DoSelection_Rect(StaticObjects.Data.InputState.CurrentAction, true); 
                    StaticObjects.Data.InputState.ClearSelect_Rect();                    
                    if (StaticObjects.Data.ActionMadeChange == true)
                    {
                        StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);
                    }
                    break;
                case EditorInputAction.Select_Marque:
                case EditorInputAction.AddSelect_Marque:
                case EditorInputAction.SubtractSelect_Marque:
                    break;
                case EditorInputAction.Move:
                case EditorInputAction.Move_Snap:
                case EditorInputAction.Paint:
                case EditorInputAction.Paint_Flood:
                    if (StaticObjects.Data.ActionMadeChange == true)
                    {
                        StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.ModelModified);
                    }
                    break;
                case EditorInputAction.Rotate:
                case EditorInputAction.Rotate_Snap:
                case EditorInputAction.Scale:                
                case EditorInputAction.Scale_Snap:
                case EditorInputAction.Scale_Uniform:
                case EditorInputAction.Scale_Uniform_Snap:
                    StaticObjects.Data.ResetForObjectEdit();
                    if (StaticObjects.Data.ActionMadeChange == true)
                    {
                        StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.ModelModified);
                    }
                    break;
                default:
                    break;
            }

            StaticObjects.Data.InputState.ClearAction();

            Control.Cursor = Cursors.Default;

            Control.Invalidate(); 
        }

        public void MouseMove(object sender, MouseEventArgs e)
        {
            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);

            PointF mouse = new PointF(e.X - rectangle.X, e.Y - rectangle.Y);

            ConvertMouseToView(ref rectangle, ref mouse);      

            switch (StaticObjects.Data.InputState.CurrentAction)
            {
                case EditorInputAction.None:
                    return;
                case EditorInputAction.ViewPan:
                    StaticObjects.Data.InputState.DoViewPan(e);
                    break;
                case EditorInputAction.ViewRotate:
                    StaticObjects.Data.InputState.DoViewRotation(e);
                    break;
                case EditorInputAction.Select_Single:
                case EditorInputAction.AddSelect_Single:
                case EditorInputAction.SubtractSelect_Single:
                    ModelSelectionInfo selection;
                    bool found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);

                    if (StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Select_Single)
                    {
                        StaticObjects.Data.Selection.Clear();
                        StaticObjects.Data.Selection.Add(selection);
                    }
                    else if (StaticObjects.Data.InputState.CurrentAction == EditorInputAction.AddSelect_Single)
                    {
                        StaticObjects.Data.Selection.Add(selection);
                    }
                    else if (StaticObjects.Data.InputState.CurrentAction == EditorInputAction.SubtractSelect_Single)
                    {
                        StaticObjects.Data.Selection.Subtract(selection);
                    }

                    StaticObjects.Data.CacheSelection();
                    StaticObjects.Data.UpdateModels();
                    break;
                case EditorInputAction.Select_Rect:
                case EditorInputAction.AddSelect_Rect:
                case EditorInputAction.SubtractSelect_Rect:
                case EditorInputAction.Select_Rect_Through:
                case EditorInputAction.AddSelect_Rect_Through:
                case EditorInputAction.SubtractSelect_Rect_Through:
                    StaticObjects.Data.InputState.UpdateSelect_Rect(e);
                    break;
                case EditorInputAction.Move:
                case EditorInputAction.Move_Snap:
                    DoMove(e); 
                    break;
                case EditorInputAction.Rotate:
                case EditorInputAction.Rotate_Snap:
                    DoRotate(e); 
                    break;
                case EditorInputAction.Scale:
                case EditorInputAction.Scale_Snap:
                case EditorInputAction.Scale_Uniform:
                case EditorInputAction.Scale_Uniform_Snap:
                    DoScale(e); 
                    break;
                case EditorInputAction.Paint:
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);
                    StaticObjects.Data.Paint(selection);
                    StaticObjects.Data.UpdateModels();
                    break;
                case EditorInputAction.Paint_Flood:
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);
                    StaticObjects.Data.Paint_Flood(selection);
                    StaticObjects.Data.UpdateModels();
                    break;    
                case EditorInputAction.Pipette: 
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);
                    StaticObjects.Data.Pipette(selection);
                    break;
                default:
                    break;
            }

            Control.Invalidate(); 
        }

        public void MouseDown(object sender, MouseEventArgs e)
        {
            ViewModes mode;
            Rectangle rectangle;

            GetViewModeAndRect(e.Location, out mode, out rectangle);

            StaticObjects.Data.InputState.SetCurrentAction(mode, e.Button);

            StaticObjects.Data.InputState.SelectionView = mode;
            StaticObjects.Data.InputState.SelectionViewRectangle = rectangle;

            StaticObjects.Data.InputState.DragStart = e.Location;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);

            PointF mouse = new PointF(e.X - rectangle.X, e.Y - rectangle.Y);

            ConvertMouseToView(ref rectangle, ref mouse);
            
            ModelSelectionInfo selection;
            bool found;

            StaticObjects.Data.ActionMadeChange = false; 

            switch (StaticObjects.Data.InputState.CurrentAction)
            {
                case EditorInputAction.None:                    
                    break;
                case EditorInputAction.Select_Single:
                case EditorInputAction.AddSelect_Single:
                case EditorInputAction.SubtractSelect_Single:    
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);

                    if (StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Select_Single)
                    {
                        StaticObjects.Data.Selection.Clear();
                        StaticObjects.Data.Selection.Add(selection); 
                    }
                    else if (StaticObjects.Data.InputState.CurrentAction == EditorInputAction.AddSelect_Single)
                    {
                        StaticObjects.Data.Selection.Add(selection);                        
                    }
                    else if (StaticObjects.Data.InputState.CurrentAction == EditorInputAction.SubtractSelect_Single)
                    {
                        StaticObjects.Data.Selection.Subtract(selection);
                    }

                    StaticObjects.Data.CacheSelection(); 
                    StaticObjects.Data.UpdateModels();
                    break;
                case EditorInputAction.Select_Rect:
                case EditorInputAction.AddSelect_Rect:
                case EditorInputAction.SubtractSelect_Rect:
                case EditorInputAction.Select_Rect_Through:
                case EditorInputAction.AddSelect_Rect_Through:
                case EditorInputAction.SubtractSelect_Rect_Through:
                    StaticObjects.Data.InputState.BeginSelect_Rect(e);
                    break;
                case EditorInputAction.Move:
                case EditorInputAction.Move_Snap:
                case EditorInputAction.Scale_Uniform:
                case EditorInputAction.Scale_Uniform_Snap:
                case EditorInputAction.Rotate:
                case EditorInputAction.Rotate_Snap:
                case EditorInputAction.Scale:
                case EditorInputAction.Scale_Snap:
                    StaticObjects.Data.InputState.StartMove(e);
                    StaticObjects.Data.CacheSelection(); 
                    break;
                case EditorInputAction.Paint:
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);
                    StaticObjects.Data.Paint(selection);
                    StaticObjects.Data.UpdateModels();
                    break;
                case EditorInputAction.Paint_Flood:
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);
                    StaticObjects.Data.Paint_Flood(selection);
                    StaticObjects.Data.UpdateModels();
                    break;
                case EditorInputAction.Pipette:
                    found = FindObject_Ray(mouse, mode, rectangle, transform, out selection);
                    StaticObjects.Data.Pipette(selection);                    
                    break;
                case EditorInputAction.ViewPan:
                    StaticObjects.Data.InputState.StartPan();                     
                    break;
                case EditorInputAction.ViewRotate:
                    StaticObjects.Data.InputState.StartRotation();                                         
                    break;
                default:
                    break;
            }

            switch (StaticObjects.Data.InputState.CurrentAction)
            {
                case EditorInputAction.None:
                case EditorInputAction.Select_Single:
                case EditorInputAction.AddSelect_Single:
                case EditorInputAction.SubtractSelect_Single:
                case EditorInputAction.Select_Rect:
                case EditorInputAction.AddSelect_Rect:
                case EditorInputAction.SubtractSelect_Rect:
                case EditorInputAction.Select_Rect_Through:
                case EditorInputAction.AddSelect_Rect_Through:
                case EditorInputAction.SubtractSelect_Rect_Through:
                case EditorInputAction.Select_Marque:
                case EditorInputAction.AddSelect_Marque:
                case EditorInputAction.SubtractSelect_Marque:
                    Control.Cursor = Cursors.Default;
                    break;
                case EditorInputAction.Move:
                case EditorInputAction.Move_Snap:
                case EditorInputAction.Rotate:
                case EditorInputAction.Rotate_Snap:
                case EditorInputAction.Scale:
                case EditorInputAction.Scale_Snap:
                case EditorInputAction.Scale_Uniform:
                case EditorInputAction.Scale_Uniform_Snap:
                    Control.Cursor = Cursors.Hand;
                    break;
                case EditorInputAction.ViewPan:
                case EditorInputAction.ViewRotate:
                    Control.Cursor = Cursors.Hand;                    
                    break;
                default:
                    break;
            }

            CameraNormal = Vector3.Normalize(Vector3.Transform(Vector3.UnitZ, m_View.Camera.Rotation)); 

            Control.Invalidate();  
        }

        public void MouseDoubleClick(object sender, MouseEventArgs e)
        {
            switch (StaticObjects.Data.InputState.CurrentAction)
            {
                case EditorInputAction.Select_Single:
                    DoAddNewVertex(e); 
                    break;
                default:
                    break;
            }

            Control.Invalidate(); 
        }

        public void MouseClick(object sender, MouseEventArgs e)
        {
            Control.Invalidate();
        }

        #endregion

        #region Do Add

        private void DoAddNewVertex(MouseEventArgs e)
        {
            if (e.Button != System.Windows.Forms.MouseButtons.Left)
            {
                return; 
            }

            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);

            transform.Invert();

            PointF start = new PointF(e.Location.X - rectangle.X, e.Location.Y - rectangle.Y);

            ConvertMouseToView(ref rectangle, ref start);

            Vector3 newVector = Vector3.Transform(new Vector3(start.X, start.Y, 0.5f), transform);

            StaticObjects.Data.CurrentModel.ModelData.Verts.Add(newVector);
            
            StaticObjects.Data.NeedsSave = true; 

            StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.ModelModified);

            StaticObjects.Data.UpdateModels();
        }

        #endregion

        #region Do Selection Rect

        private void DoSelection_Rect(EditorInputAction editorInputAction, bool through)
        {
            if (StaticObjects.Data.InputState.SelectionRectangle == Rectangle.Empty)
            {
                return;
            }

            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);

            Rectangle selection = StaticObjects.Data.InputState.SelectionRectangle;

            PointF start = new PointF(selection.Left - rectangle.X, selection.Bottom - rectangle.Y);
            PointF end = new PointF(selection.Right - rectangle.X, selection.Top - rectangle.Y);

            ConvertMouseToView(ref rectangle, ref start);
            ConvertMouseToView(ref rectangle, ref end);

            RectangleF selectionRect = new RectangleF(start.X, start.Y, end.X - start.X, end.Y - start.Y);

            ModelSelectionInfo[] infos;

            FindObject_Rectangle(selectionRect, StaticObjects.Data.InputState.SelectionView, rectangle, transform, through, out infos);

            switch (editorInputAction)
            {
                case EditorInputAction.Select_Rect:
                case EditorInputAction.Select_Rect_Through:
                    StaticObjects.Data.Selection.Clear();
                    StaticObjects.Data.Selection.Add(infos);
                    break;
                case EditorInputAction.AddSelect_Rect:
                case EditorInputAction.AddSelect_Rect_Through:
                    StaticObjects.Data.Selection.Add(infos);
                    break;
                case EditorInputAction.SubtractSelect_Rect:
                case EditorInputAction.SubtractSelect_Rect_Through:
                    StaticObjects.Data.Selection.Subtract(infos);
                    break;
                default:
                    break;
            }

            StaticObjects.Data.CacheSelection();

            StaticObjects.Data.UpdateModels();
        }

        #endregion

        #region Do Edit

        private void DoMove(MouseEventArgs e)
        {
            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);

            transform.Invert();

            PointF start = new PointF(StaticObjects.Data.InputState.DragStart.X, StaticObjects.Data.InputState.DragStart.Y);
            PointF end = new PointF(e.Location.X, e.Location.Y);

            ConvertMouseToView(ref rectangle, ref start);
            ConvertMouseToView(ref rectangle, ref end);

            Vector3 mouseStart = new Vector3(start.X, start.Y, 0);

            mouseStart = Vector3.Transform(mouseStart, transform);

            Vector3 mouseEnd = new Vector3(end.X, end.Y, 0);

            mouseEnd = Vector3.Transform(mouseEnd, transform);

            bool snapOverride = true;

            if (StaticObjects.Data.Selection.PointsTotal == 1 &&
                StaticObjects.Data.Selection.FacesTotal == 0)
            {
                snapOverride = false;
            }

            StaticObjects.Data.AdjustSelection_Move(mouseEnd - mouseStart, snapOverride);

            StaticObjects.Data.UpdateModels_Full();
        }

        private void DoRotate(MouseEventArgs e)
        {
            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);
            Matrix4 transformInvert = transform;
            transformInvert.Invert(); 

            Rectangle selection = StaticObjects.Data.InputState.SelectionRectangle;

            Vector2 start = new Vector2(StaticObjects.Data.InputState.DragStart.X - rectangle.X, StaticObjects.Data.InputState.DragStart.Y - rectangle.Y);
            Vector2 end = new Vector2(e.Location.X - rectangle.X, e.Location.Y - rectangle.Y);

            ConvertMouseToView(ref rectangle, ref start);
            ConvertMouseToView(ref rectangle, ref end);

            Vector2 selectionCenterInScreenSpace = Vector3.Transform(StaticObjects.Data.Selection.Center, transform).Xy;

            Vector3 axis = Vector3.TransformVector(Vector3.UnitZ, transformInvert); 

            float angle1 = GetAngle(selectionCenterInScreenSpace, start);
            float angle2 = GetAngle(selectionCenterInScreenSpace, end);

            float angleChange = GetChangedAngle(angle1, angle2);

            StaticObjects.Data.AdjustSelection_Rotate(axis, angleChange, StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Rotate_Snap);

            //selectionCenterInScreenSpace

            /* 
            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);

            transform.Invert();

            PointF start = new PointF(StaticObjects.Data.InputState.DragStart.X, StaticObjects.Data.InputState.DragStart.Y);
            PointF end = new PointF(e.Location.X, e.Location.Y);

            ConvertMouseToView(ref rectangle, ref start);
            ConvertMouseToView(ref rectangle, ref end);

            Vector3 mouseStart = new Vector3(start.X, start.Y, 0);

            mouseStart = Vector3.Transform(mouseStart, transform);

            Vector3 mouseEnd = new Vector3(end.X, end.Y, 0);

            mouseEnd = Vector3.Transform(mouseEnd, transform);

            bool snapOverride = true;

            if (StaticObjects.Data.Selection.PointsTotal == 1 &&
                StaticObjects.Data.Selection.FacesTotal == 0)
            {
                snapOverride = false;
            }

            StaticObjects.Data.AdjustSelection_Rotate(mouseEnd - mouseStart, snapOverride);

            StaticObjects.Data.UpdateModels_Full();
            */

            StaticObjects.Data.UpdateModels();
        }

        /* 
        public void SetCurrentVector(Vector2 vector)
        {
            SetCurrentAngle((float)Math.Atan2(vector.Y - Center.Y, vector.X - Center.X) * (Reverse ? -1f : 1f));
        }

        public void SetCurrentAngle(float currentAngle)
        {
            currentAngle = ClampAngle(currentAngle);

            CurrentAngleMove = currentAngle - CurrentAngle;

            CurrentAngle = currentAngle;
        }       
        
         */

        private static float GetChangedAngle(float idealAngle, float currentAngle)
        {
            float a = idealAngle - currentAngle;

            a = ClampAngle(a);

            return a;
        }

        private float GetAngle(Vector2 center, Vector2 vector)
        {
            return ClampAngle((float)Math.Atan2(vector.Y - center.Y, vector.X - center.X));
        }

        private static float ClampAngle(float a)
        {
            while (a > MathHelper.Pi)
            {
                a -= MathHelper.TwoPi;
            }

            while (a < -MathHelper.Pi)
            {
                a += MathHelper.TwoPi;
            }

            return a;
        }

        private void DoScale(MouseEventArgs e)
        {
            /* 
            ViewModes mode = StaticObjects.Data.InputState.SelectionView;
            Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

            Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);
            Matrix4 transformInvert = transform;
            transformInvert.Invert();

            Rectangle selection = StaticObjects.Data.InputState.SelectionRectangle;

            Vector2 start = new Vector2(StaticObjects.Data.InputState.DragStart.X, StaticObjects.Data.InputState.DragStart.Y);
            Vector2 end = new Vector2(e.Location.X, e.Location.Y);

            ConvertMouseToView(ref rectangle, ref start);
            ConvertMouseToView(ref rectangle, ref end);

            Vector2 selectionCenterInScreenSpace = Vector3.Transform(StaticObjects.Data.InputState.Center, transform).Xy; 
            */
            /* 
            {
                ViewModes mode = StaticObjects.Data.InputState.SelectionView;
                Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

                Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);
                Matrix4 transformInvert = transform;
                transformInvert.Invert();

                Rectangle selection = StaticObjects.Data.InputState.SelectionRectangle;

                Vector2 start = new Vector2(StaticObjects.Data.InputState.DragStart.X - rectangle.X, StaticObjects.Data.InputState.DragStart.Y - rectangle.Y);
                Vector2 end = new Vector2(e.Location.X - rectangle.X, e.Location.Y - rectangle.Y);

                ConvertMouseToView(ref rectangle, ref start);
                ConvertMouseToView(ref rectangle, ref end);

                Vector2 selectionCenterInScreenSpace = Vector3.Transform(StaticObjects.Data.InputState.Center, transform).Xy;

                Vector2 scale = 
            }
            */ 
            {
                ViewModes mode = StaticObjects.Data.InputState.SelectionView;
                Rectangle rectangle = StaticObjects.Data.InputState.SelectionViewRectangle;

                Matrix4 transform = SetupViewAndTransformForMouse(mode, ref rectangle);
                Matrix4 transformInvert = transform;
                transformInvert.Invert();

                //PointF start = new PointF(StaticObjects.Data.InputState.DragStart.X, StaticObjects.Data.InputState.DragStart.Y);
                //PointF end = new PointF(e.Location.X, e.Location.Y);

                PointF start = new PointF(StaticObjects.Data.InputState.DragStart.X - rectangle.X, StaticObjects.Data.InputState.DragStart.Y - rectangle.Y);
                PointF end = new PointF(e.Location.X - rectangle.X, e.Location.Y - rectangle.Y);

                ConvertMouseToView(ref rectangle, ref start);
                ConvertMouseToView(ref rectangle, ref end);

                Vector3 mouseStart = new Vector3(start.X, start.Y, 0);

                mouseStart = Vector3.Transform(mouseStart, transformInvert);

                Vector2 selectionCenterInScreenSpace = Vector3.Transform(StaticObjects.Data.Selection.Center, transform).Xy;

                Vector3 scale = new Vector3(1f / (selectionCenterInScreenSpace.X - start.X), 1f / (selectionCenterInScreenSpace.Y - start.Y), 1f);

                scale.X *= (selectionCenterInScreenSpace.X - end.X);
                scale.Y *= (selectionCenterInScreenSpace.Y - end.Y); 

                Vector3 mouseEnd = new Vector3(end.X, end.Y, 0);

                mouseEnd = Vector3.Transform(mouseEnd, transformInvert);

                bool isUniform = StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Scale_Uniform ||
                                 StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Scale_Uniform_Snap;

                bool snap = StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Scale_Snap ||
                            StaticObjects.Data.InputState.CurrentAction == EditorInputAction.Scale_Uniform_Snap;
            
                scale = StaticObjects.Data.DoSnap(scale, false);

                if (isUniform == true)
                {
                    float max = Math.Max(Math.Abs(scale.X), Math.Abs(scale.Y));

                    if (Math.Sign(scale.X) == -1 ||
                        Math.Sign(scale.Y) == -1)
                    {
                        max *= -1f;
                    }

                    scale = new Vector3(max, max, max);
                }       

                transformInvert = transformInvert.ClearScale();
                transformInvert = transformInvert.ClearTranslation();
                scale = Vector3.Transform(scale, transformInvert);

                /* 
                if (StaticObjects.Data.Selection.PointsTotal == 1 &&
                    StaticObjects.Data.Selection.FacesTotal == 0)
                {
                    snapOverride = false;
                }
                */

                StaticObjects.Data.AdjustSelection_Scale(scale, mouseEnd, isUniform);
                //StaticObjects.Data.AdjustSelection_Scale(mouseEnd, isUniform);

                StaticObjects.Data.UpdateModels();
            }
        }

        #endregion

        #region Find Objects

        private bool FindObject_Ray(PointF mouse, ViewModes mode, Rectangle rect, Matrix4 transform, out ModelSelectionInfo selection)
        {
            float hVertSize = (1f / (float)rect.Width) * VertSize;
            float vVertSize = (1f / (float)rect.Height) * VertSize;

            selection = new ModelSelectionInfo();

            float bestZ = float.PositiveInfinity;
            bool found = false;

            ModelSelectionMode selectionMode = StaticObjects.Data.InputState.SelectionMode; 

            if (selectionMode == ModelSelectionMode.Vertices)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    for (int i = 0; i < data.ModelData.Verts.Count; i++)
                    {
                        Vector3 point = data.ModelData.Verts[i];

                        Vector3 pt3 = Vector3.Transform(point, transform);

                        if (pt3.Z > bestZ)
                        {
                            continue;
                        }

                        PointF pt2 = new PointF(pt3.X, pt3.Y);

                        if (new RectangleF(pt2.X - hVertSize, pt2.Y - vVertSize, hVertSize * 2f, vVertSize * 2f).Contains(mouse) == true)
                        {
                            selection.ModelID = data.ModelID;

                            selection.Array = ModelArrays.Verts;

                            selection.Indices.Clear();
                            selection.Indices.Add(i);

                            bestZ = pt3.Z;

                            found = true;
                        }
                    }
                }
            }
            else if (selectionMode == ModelSelectionMode.Faces)
            {
                Matrix4 rayTrasform = transform;
                rayTrasform.Invert();

                Vector3 rayStart = Vector3.Transform(new Vector3(mouse.X, mouse.Y, 0), rayTrasform);
                Vector3 rayEnd = Vector3.Transform(new Vector3(mouse.X, mouse.Y, 1), rayTrasform);

                Ray ray = new Ray(rayStart, Vector3.Normalize(rayEnd - rayStart));

                bool selectFaceCenter =
                    ((VisibleItems & VisibleItemFlags.Solid) == VisibleItemFlags.None) &&
                    ((VisibleItems & VisibleItemFlags.Faces) == VisibleItemFlags.Faces); 

                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    if (selectFaceCenter)
                    {
                        for (int i = 0; i < data.ModelData.Faces.Count; i++)
                        {
                            FaceData face = data.ModelData.Faces[i];

                            Vector3 pt0 = Vector3.Transform(data.ModelData.Verts[face.V0], transform);
                            Vector3 pt1 = Vector3.Transform(data.ModelData.Verts[face.V1], transform);
                            Vector3 pt2 = Vector3.Transform(data.ModelData.Verts[face.V2], transform);

                            Vector3 center = (pt0 + pt1 + pt2) / 3f;

                            if (center.Z > bestZ)
                            {
                                continue;
                            }

                            if (new RectangleF(center.X - hVertSize, center.Y - vVertSize, hVertSize * 2f, vVertSize * 2f).Contains(mouse) == true)
                            {
                                selection.ModelID = data.ModelID;

                                selection.Array = ModelArrays.Faces;

                                selection.Indices.Clear();
                                selection.Indices.Add(i);

                                bestZ = center.Z;

                                found = true;
                            }
                        }
                    }
                    else if (selectionMode == ModelSelectionMode.Faces)
                    {
                        int[] faces;
                        bool hitFace = data.ModelData.FindIntersectionFaces_Faceing(ref ray, out faces);

                        if (hitFace == true)
                        {
                            selection.ModelID = data.ModelID;

                            selection.Array = ModelArrays.Faces;

                            selection.Indices.Clear();
                            selection.Indices.AddRange(faces);

                            found = true;
                        }
                    }
                    else
                    {
                        int[] faces;
                        bool hitFace = data.ModelData.FindIntersectionFaces(ref ray, out faces);

                        if (hitFace == true)
                        {
                            selection.ModelID = data.ModelID;

                            selection.Array = ModelArrays.Faces;

                            selection.Indices.Clear();
                            selection.Indices.AddRange(faces);

                            found = true;
                        }
                    }
                }
            }
            else if (selectionMode == ModelSelectionMode.Objects)
            {
                hVertSize *= 2f;
                vVertSize *= 2f;

                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    {
                        Vector3 point = Vector3.Transform(data.Center, data.Transform);

                        Vector3 pt3 = Vector3.Transform(point, transform);

                        if (pt3.Z > bestZ)
                        {
                            continue;
                        }

                        PointF pt2 = new PointF(pt3.X, pt3.Y);

                        if (new RectangleF(pt2.X - hVertSize, pt2.Y - vVertSize, hVertSize * 2f, vVertSize * 2f).Contains(mouse) == true)
                        {
                            selection.ModelID = data.ModelID;

                            selection.Array = ModelArrays.Objects;

                            selection.Indices.Clear();
                            selection.Indices.Add(-1);

                            bestZ = pt3.Z;

                            found = true;
                        }
                    }

                    for (int i = 0; i < data.ModelData.HullPrimitives.Count; i++)
                    {
                        ModelPrimitive primitive = data.ModelData.HullPrimitives[i]; 

                        //Vector3 point = Vector3.Transform(data.Center, data.Transform);

                        Vector3 pt3 = Vector3.Transform(primitive.Center, transform);

                        if (pt3.Z > bestZ)
                        {
                            continue;
                        }

                        PointF pt2 = new PointF(pt3.X, pt3.Y);

                        if (new RectangleF(pt2.X - hVertSize, pt2.Y - vVertSize, hVertSize * 2f, vVertSize * 2f).Contains(mouse) == true)
                        {
                            selection.ModelID = data.ModelID;

                            selection.Array = ModelArrays.Objects;

                            selection.Indices.Clear();
                            selection.Indices.Add(i);

                            bestZ = pt3.Z;

                            found = true;
                        }
                    }
                }
            }

            /* 
            if ((VisibleItems & VisibleItemFlags.Hull) == VisibleItemFlags.Hull)
            {
                List<PointF> linePoints = new List<PointF>();

                for (int i = 0; i < ModelData.HullVerts.Count; i++) 
                {
                    Vector3 point = ModelData.Verts[ModelData.HullVerts[i]];

                    Vector3 pt3 = Vector3.Transform(point, transform);

                    PointF pt = new PointF(pt3.X, pt3.Y);

                    if (new RectangleF(pt.X - hVertSize, pt.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
                    {
                        selection.Array = ModelArrays.HullVerts;
                        selection.Index = i;

                        selection.InitialValue = new Vector3[] { point } ;
                        selection.MouseStart = mouse;

                        return true;
                    }
                }
            }
            */

            /*
            if ((VisibleItems & VisibleItemFlags.Extras) == VisibleItemFlags.Extras)
            {
                for (int i = 0; i < ModelData.Extras.Count; i++)
                {
                    ExtraData extra = ModelData.Extras[i];

                    Vector3 pos = Vector3.Transform(extra.Position, transform);

                    Vector3 dir = Vector3.Transform(extra.Position + (extra.Direction * ExtraLength), transform);

                    if (new RectangleF(pos.X - hVertSize, pos.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
                    {
                        selection.Array = ModelArrays.Extras;
                        selection.Index = i * 2;

                        selection.InitialValue = new Vector3[] { extra.Position };
                        selection.MouseStart = mouse;

                        return true;
                    }

                    if (new RectangleF(dir.X - hVertSize, dir.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
                    {
                        selection.Array = ModelArrays.Extras;
                        selection.Index = i * 2 + 1;

                        selection.InitialValue = new Vector3[] { extra.Direction };
                        selection.MouseStart = mouse;

                        return true;
                    }
                }
            }
            */

            if (found == true)
            {
                return true;
            }
            else
            {
                selection = null;

                return false;
            }
        }

        private bool FindObject_Rectangle(RectangleF mouseRect, ViewModes mode, Rectangle rect, Matrix4 transform, bool through, out ModelSelectionInfo[] selections)
        {
            float hVertSize = (1f / (float)rect.Width) * VertSize;
            float vVertSize = (1f / (float)rect.Height) * VertSize;

            List<ModelSelectionInfo> selectionList = new List<ModelSelectionInfo>();

            ModelSelectionMode selectionMode = StaticObjects.Data.InputState.SelectionMode;

            if (selectionMode == ModelSelectionMode.Vertices)
            {
                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    bool found = false;

                    ModelSelectionInfo selection = new ModelSelectionInfo();

                    selection.ModelID = data.ModelID;
                    selection.Array = ModelArrays.Verts;

                    for (int i = 0; i < data.ModelData.Verts.Count; i++)
                    {
                        Vector3 point = data.ModelData.Verts[i];

                        Vector3 pt3 = Vector3.Transform(point, transform);

                        PointF pt2 = new PointF(pt3.X, pt3.Y); 

                        if (mouseRect.Contains(pt2) == true)
                        {
                            selection.Indices.Add(i);
                            found = true;
                        }
                    }

                    if (found == true)
                    {
                        selectionList.Add(selection);
                    }
                }
            }
            else if (selectionMode == ModelSelectionMode.Faces)
            {
                Matrix4 rayTrasform = transform;
                rayTrasform.Invert();
                
                Vector3 rayStart = Vector3.Transform(new Vector3(0, 0, 0), rayTrasform);
                Vector3 rayEnd = Vector3.Transform(new Vector3(0, 0, 1), rayTrasform);

                Vector3 rayNormal = Vector3.Normalize(rayEnd - rayStart);
                        
                Vector3 boxStart = Vector3.Transform(new Vector3(mouseRect.Left, mouseRect.Bottom, 0), rayTrasform);
                Vector3 boxEnd = Vector3.Transform(new Vector3(mouseRect.Right, mouseRect.Top, 0), rayTrasform);

                rayTrasform = rayTrasform.ClearProjection();
                rayTrasform = rayTrasform.ClearScale();
                rayTrasform = rayTrasform.ClearTranslation();

                Vector3 boxUp = Vector3.Transform(-Vector3.UnitY, rayTrasform);
                Vector3 boxDown = Vector3.Transform(Vector3.UnitY, rayTrasform);
                Vector3 boxRight = Vector3.Transform(-Vector3.UnitX, rayTrasform);
                Vector3 boxLeft = Vector3.Transform(Vector3.UnitX, rayTrasform);

                Plane top = new Plane(boxEnd, boxDown);
                Plane bottom = new Plane(boxStart, boxUp);
                Plane left = new Plane(boxStart, boxLeft);
                Plane right = new Plane(boxEnd, boxRight);

                Plane[] planes = new Plane[] { top, bottom, left, right }; 

                bool selectFaceCenter = 
                    ((VisibleItems & VisibleItemFlags.Solid) == VisibleItemFlags.None) && 
                    ((VisibleItems & VisibleItemFlags.Faces) == VisibleItemFlags.Faces); 

                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    bool found = false;

                    ModelSelectionInfo selection = new ModelSelectionInfo();

                    selection.ModelID = data.ModelID;
                    selection.Array = ModelArrays.Faces;

                    if (selectFaceCenter == true)
                    {
                        for (int i = 0; i < data.ModelData.Faces.Count; i++)
                        {
                            FaceData face = data.ModelData.Faces[i];

                            Vector3 pt0 = Vector3.Transform(data.ModelData.Verts[face.V0], transform);
                            Vector3 pt1 = Vector3.Transform(data.ModelData.Verts[face.V1], transform);
                            Vector3 pt2 = Vector3.Transform(data.ModelData.Verts[face.V2], transform);

                            Vector3 center = (pt0 + pt1 + pt2) / 3f;

                            PointF centerF = new PointF(center.X, center.Y);

                            if (mouseRect.Contains(centerF) == true)
                            {
                                selection.Indices.Add(i);
                                found = true;
                            }
                        }
                    }
                    else if (through == false)
                    {
                        int[] faces;
                        bool hitFace = data.ModelData.FindIntersectionFaces_Faceing(planes, ref rayNormal, out faces);

                        if (hitFace == true)
                        {
                            selection.Indices.AddRange(faces);
                            found = true;
                        }
                    }
                    else
                    {
                        int[] faces;
                        bool hitFace = data.ModelData.FindIntersectionFaces(planes, out faces);

                        if (hitFace == true)
                        {
                            selection.Indices.AddRange(faces);
                            found = true;
                        }
                    }

                    if (found == true)
                    {
                        selectionList.Add(selection);
                    }
                }
            }
            else if (selectionMode == ModelSelectionMode.Objects)
            {
                hVertSize *= 2f;
                vVertSize *= 2f;

                foreach (EditableModelData data in StaticObjects.Data.Models.Values)
                {
                    bool found = false;

                    ModelSelectionInfo selection = new ModelSelectionInfo();

                    selection.ModelID = data.ModelID;
                    selection.Array = ModelArrays.Objects;

                    {
                        Vector3 point = Vector3.Transform(data.Center, data.Transform);

                        Vector3 pt3 = Vector3.Transform(point, transform);

                        PointF pt2 = new PointF(pt3.X, pt3.Y);

                        if (mouseRect.Contains(pt2) == true)
                        {
                            selection.Indices.Add(-1);
                            found = true;
                        }
                    }

                    for (int i = 0; i < data.ModelData.HullPrimitives.Count; i++)
                    {
                        ModelPrimitive primitive = data.ModelData.HullPrimitives[i];

                        //Vector3 point = Vector3.Transform(data.Center, data.Transform);

                        Vector3 pt3 = Vector3.Transform(primitive.Center, transform);

                        PointF pt2 = new PointF(pt3.X, pt3.Y);

                        if (mouseRect.Contains(pt2) == true)                        
                        {                            
                            selection.Indices.Add(i);
                            found = true;
                        }
                    }

                    if (found == true)
                    {
                        selectionList.Add(selection);
                    }
                }
            }

            /* 
            if ((VisibleItems & VisibleItemFlags.Hull) == VisibleItemFlags.Hull)
            {
                List<PointF> linePoints = new List<PointF>();

                for (int i = 0; i < ModelData.HullVerts.Count; i++) 
                {
                    Vector3 point = ModelData.Verts[ModelData.HullVerts[i]];

                    Vector3 pt3 = Vector3.Transform(point, transform);

                    PointF pt = new PointF(pt3.X, pt3.Y);

                    if (new RectangleF(pt.X - hVertSize, pt.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
                    {
                        selection.Array = ModelArrays.HullVerts;
                        selection.Index = i;

                        selection.InitialValue = new Vector3[] { point } ;
                        selection.MouseStart = mouse;

                        return true;
                    }
                }
            }
            */

            /*
            if ((VisibleItems & VisibleItemFlags.Extras) == VisibleItemFlags.Extras)
            {
                for (int i = 0; i < ModelData.Extras.Count; i++)
                {
                    ExtraData extra = ModelData.Extras[i];

                    Vector3 pos = Vector3.Transform(extra.Position, transform);

                    Vector3 dir = Vector3.Transform(extra.Position + (extra.Direction * ExtraLength), transform);

                    if (new RectangleF(pos.X - hVertSize, pos.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
                    {
                        selection.Array = ModelArrays.Extras;
                        selection.Index = i * 2;

                        selection.InitialValue = new Vector3[] { extra.Position };
                        selection.MouseStart = mouse;

                        return true;
                    }

                    if (new RectangleF(dir.X - hVertSize, dir.Y - hVertSize, VertSize, VertSize).Contains(mouse) == true)
                    {
                        selection.Array = ModelArrays.Extras;
                        selection.Index = i * 2 + 1;

                        selection.InitialValue = new Vector3[] { extra.Direction };
                        selection.MouseStart = mouse;

                        return true;
                    }
                }
            }
            */

            selections = selectionList.ToArray();

            return selectionList.Count > 0;
        }

        #endregion
    }
}
