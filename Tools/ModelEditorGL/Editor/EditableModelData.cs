﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Flat.Models;
using Rug.Game.Physics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    class EditableModelData : IResourceManager
    {
        private static Color4[] HullColors = new Color4[]
        {
            Color4.DarkCyan, 
            Color4.Maroon, 
            Color4.LawnGreen,
            Color4.Honeydew, 
            Color4.HotPink, 
            //Color4.Orange,
            Color4.Aquamarine, 
            Color4.Indigo,
        };


        public static Vector3[] UnitCircleX;
        public static Vector3[] UnitCircleY;
        public static Vector3[] UnitCircleZ;

        static EditableModelData()
        {
            int count = 16; 

            UnitCircleX = new Vector3[count];
            UnitCircleY = new Vector3[count];
            UnitCircleZ = new Vector3[count];

            float angleInc = MathHelper.TwoPi / (float)count; 

            for (int i = 0; i < count; i++)
            {
                UnitCircleX[i] = Vector3.Transform(Vector3.UnitY, Matrix4.CreateFromAxisAngle(Vector3.UnitX, MathHelper.PiOver2 - (angleInc * (float)i)));
                UnitCircleY[i] = Vector3.Transform(Vector3.UnitZ, Matrix4.CreateFromAxisAngle(Vector3.UnitY, angleInc * (float)i));
                UnitCircleZ[i] = Vector3.Transform(Vector3.UnitX, Matrix4.CreateFromAxisAngle(Vector3.UnitZ, angleInc * (float)i));
            }
        }

        private ResourceManager ResourceManagers = new ResourceManager();

        public int ModelID = 0;

        public string Name = String.Empty; 

        public ModelData ModelData;

        public DynamicModel Model;        
        public PhysicsDebugLines Wireframe;
        public PhysicsDebugLines FaceNormals;
        public PhysicsDebugLines HullFrames;
        public Particles Vertices;
        public Particles FaceVertices;

        public Particles ObjectCenters;
        public PhysicsDebugLines ObjectLines;

        public Quaternion ActiveRotation = Quaternion.Identity;
        public Vector3 ActiveScale = new Vector3(1f, 1f, 1f);
        public Vector3 ActivePosition = new Vector3(0f, 0f, 0f);

        public Matrix4 Transform = Matrix4.Identity;
        public Matrix4 NormalTransform = Matrix4.Identity; 

        public Vector3 Center = new Vector3(0f, 0f, 0f);
        public Vector3 Bounds = new Vector3(0f, 0f, 0f); 

        public bool Disposed { get; private set; }

        public EditableModelData(ModelData data)
        {
            Disposed = true; 

            ModelData = data;

            Model = new DynamicModel("Model");
            Model.Data = ModelData;
            Model.CopyData(); 

            ResourceManagers.Add(Model);

            Wireframe = new PhysicsDebugLines(ModelData.Faces.Count * 3);
            ResourceManagers.Add(Wireframe);

            HullFrames = new PhysicsDebugLines(32);
            ResourceManagers.Add(HullFrames);

            FaceNormals = new PhysicsDebugLines(ModelData.Faces.Count);
            ResourceManagers.Add(FaceNormals);

            Vertices = new Particles("~/Data/White.png", ModelData.Verts.Count);
            ResourceManagers.Add(Vertices);

            FaceVertices = new Particles("~/Data/White.png", ModelData.Faces.Count);
            ResourceManagers.Add(FaceVertices);

            ObjectCenters = new Particles("~/Data/White.png", 32);
            ResourceManagers.Add(ObjectCenters);

            ObjectLines = new PhysicsDebugLines(12 * 32);
            ResourceManagers.Add(ObjectLines);
            
        }

        public void NormaliseForObjectEdit()
        {
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            for (int i = 0; i < ModelData.Verts.Count; i++)
            {
                Vector3 vert = ModelData.Verts[i];

                Vector3.ComponentMin(ref min, ref vert, out min);
                Vector3.ComponentMax(ref max, ref vert, out max);
            }

            Vector3 bounds = (max - min) * 0.5f;
            Vector3 center = min + bounds;

            ActivePosition = center;

            for (int i = 0; i < ModelData.Verts.Count; i++)
            {
                Vector3 vert = ModelData.Verts[i];

                ModelData.Verts[i] = vert - center; 
            }
        }

        public void ApplyTransform()
        {
            UpdateTransform();

            for (int i = 0; i < ModelData.Verts.Count; i++)
            {
                Vector3 vert = ModelData.Verts[i]; 

                Vector3.Transform(ref vert, ref Transform, out vert);

                ModelData.Verts[i] = vert; 
            }

            ModelData.CalcuateFaceNormals(); 

            ActiveRotation = Quaternion.Identity;
            ActiveScale = new Vector3(1f, 1f, 1f);
            ActivePosition = new Vector3(0f, 0f, 0f);

            UpdateTransform();
        }

        public void UpdateTransform()
        {
            NormalTransform = Matrix4.CreateFromQuaternion(ActiveRotation);
            Transform = Matrix4.CreateScale(ActiveScale) * NormalTransform * Matrix4.CreateTranslation(ActivePosition); 
        }        

        public void UpdateModel(EditorSelection selection, EditorToolMode tool)
        {        
            int wireframeIndex = 0;
            int normalsIndex = 0;

            int hullVertCount = 0;

            if (ModelData.ConvexHullData.Count < 512)
            {
                foreach (HullData hull in ModelData.ConvexHullData)
                {
                    int connections = hull.Faces.Count * 3;

                    hullVertCount += connections * connections;
                }
            }

            HullFrames.DesiredMaxCount = hullVertCount;

            Wireframe.DesiredMaxCount = ModelData.Faces.Count * 3;
            FaceNormals.DesiredMaxCount = ModelData.Faces.Count;
            Vertices.DesiredMaxCount = ModelData.Verts.Count;
            FaceVertices.DesiredMaxCount = ModelData.Faces.Count;

            Wireframe.CheckResize();
            FaceNormals.CheckResize();
            Vertices.CheckResize();
            FaceVertices.CheckResize();
            HullFrames.CheckResize(); 

            Color4 groupBrush = new Color4(0, 64, 0, 32);
            //Color4 wireframeColor = new Color4(0, 0, 0, 12);
            Color4 wireframeColor = new Color4(0, 0, 0, 48);

            Color4 vertColor = Color4.Black;
            vertColor.A = 0.25f;

            Color4 objectsColor = Color4.LightGreen;
            Color4 selectedObjectsColor = Color4.Magenta;

            Color4 selectedVertColor = Color4.Orange;
            Color4 selectedFaceColor = Color4.Orange;
            selectedFaceColor.A = 1f;
            Color4 selectedEdgeColor = Color4.White;

            Color4 faceColor = Color4.SkyBlue;
            faceColor.A = 0.25f;

            float normalScale = 0.5f;

            ModelSelectionInfo vertSelection = selection.FindSelection(this.ModelID, ModelArrays.Verts);
            ModelSelectionInfo faceSelection = selection.FindSelection(this.ModelID, ModelArrays.Faces);
            ModelSelectionInfo objectsSelection = selection.FindSelection(this.ModelID, ModelArrays.Objects);

            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            for (int i = 0; i < ModelData.Verts.Count; i++)
            {
                Vector3 vert = ModelData.Verts[i];

                Vector3.ComponentMin(ref min, ref vert, out min);
                Vector3.ComponentMax(ref max, ref vert, out max);

                Vertices.Instances[i] = new Rug.Game.Core.Data.BasicVertex()
                {
                    Color = vertColor,
                    Position = new Vector4(vert, 1f),
                };
            }

            if (vertSelection != null)
            {
                foreach (int index in vertSelection.Indices)
                {
                    Vertices.Instances[index].Color = selectedVertColor;
                }
            }

            for (int i = 0; i < ModelData.Faces.Count; i++)
            {
                Color4 outlinePen = Color4.White;

                FaceData face = ModelData.Faces[i];

                Color4 brush = face.Color;
                Color4 pen = face.Color;

                Vector3 pt0 = ModelData.Verts[face.V0];
                Vector3 pt1 = ModelData.Verts[face.V1];
                Vector3 pt2 = ModelData.Verts[face.V2];

                AddLines(Wireframe, ref wireframeIndex, wireframeColor, new Vector3[] 
                    {
                        pt0, pt1, 
                        pt1, pt2, 
                        pt2, pt0, 
                    });

                Vector3 center = (pt0 + pt1 + pt2) / 3f;

                FaceVertices.Instances[i] = new Rug.Game.Core.Data.BasicVertex()
                    {
                        Color = faceColor,
                        Position = new Vector4(center, 1f),
                    };

                Vector3 normal = face.Normal;

                normal.Normalize();

                normal = center + (normal * 0.125f * normalScale);

                AddLines(FaceNormals, ref normalsIndex, wireframeColor, new Vector3[] 
                    {
                        center, normal, 
                    });

            }

            hullVertCount = 0;
            int hullIndex = 0;

            if (ModelData.ConvexHullData.Count < 512)
            {
                foreach (HullData hull in ModelData.ConvexHullData)
                {
                    Color4 hullColor = HullColors[hullIndex++];

                    hullIndex %= HullColors.Length;

                    List<int> hullVertIndex = new List<int>();

                    foreach (FaceData face in hull.Faces)
                    {
                        if (hullVertIndex.Contains(face.V0) == false)
                        {
                            hullVertIndex.Add(face.V0);
                        }

                        if (hullVertIndex.Contains(face.V1) == false)
                        {
                            hullVertIndex.Add(face.V1);
                        }

                        if (hullVertIndex.Contains(face.V2) == false)
                        {
                            hullVertIndex.Add(face.V2);
                        }
                    }

                    for (int i = 0; i < hullVertIndex.Count; i++)
                    {
                        for (int j = i + 1; j < hullVertIndex.Count; j++)
                        {
                            Vector3 pt0 = ModelData.Verts[hullVertIndex[i]];
                            Vector3 pt1 = ModelData.Verts[hullVertIndex[j]];

                            AddLines(HullFrames, ref hullVertCount, hullColor, new Vector3[] 
                            {
                                pt0, pt1, 
                            });
                        }
                    }

                    /* 
                    foreach (FaceData face in hull.Faces)
                    {
                        Vector3 pt0 = ModelData.Verts[face.V0];
                        Vector3 pt1 = ModelData.Verts[face.V1];
                        Vector3 pt2 = ModelData.Verts[face.V2];

                        AddLines(HullFrames, ref hullVertCount, hullColor, new Vector3[] 
                        {
                            pt0, pt1, 
                            pt1, pt2, 
                            pt2, pt0, 
                        });
                    }
                    */
                }
            }

            Model.CopyData();

            if (faceSelection != null)
            {
                foreach (int index in faceSelection.Indices)
                {
                    FaceVertices.Instances[index].Color = selectedVertColor;

                    if (tool != EditorToolMode.Paint &&
                        tool != EditorToolMode.Pipette)
                    {
                        SetFaceColor_Active(index, selectedFaceColor);
                    }

                    Wireframe.Colors[(index * 3) + 0] = selectedEdgeColor;
                    Wireframe.Colors[(index * 3) + 1] = selectedEdgeColor;
                    Wireframe.Colors[(index * 3) + 2] = selectedEdgeColor;
                }
            }

            Model.FlushModelData();

            ObjectCenters.DesiredMaxCount = 1 + ModelData.HullPrimitives.Count;
            
            ObjectLines.DesiredMaxCount = (1 + ModelData.HullPrimitives.Count) * UnitCircleY.Length * 6;

            ObjectCenters.CheckResize();
            ObjectLines.CheckResize();

            {
                int centersIndex = 0;
                int linesIndex = 0;

                if (ModelData.Verts.Count > 0)
                {
                    Color4 color = objectsColor; 

                    if (objectsSelection != null && objectsSelection.Indices.Contains(-1))
                    {
                        color = selectedObjectsColor; 
                    }

                    Bounds = (max - min) * 0.5f;
                    Center = min + Bounds;

                    Vector3 center = Center + ActivePosition; 

                    ObjectCenters.Instances[centersIndex++] = new Rug.Game.Core.Data.BasicVertex()
                    {
                        Color = color,
                        Position = new Vector4(center, 1f),
                    };

                    AddCuboid(ObjectLines, ref linesIndex, center, Bounds * ActiveScale, ActiveRotation, color);                    
                }

                for (int i = 0; i < ModelData.HullPrimitives.Count; i++) 
                {
                    ModelPrimitive primitive = ModelData.HullPrimitives[i]; 

                    Color4 color = objectsColor;

                    if (objectsSelection != null && objectsSelection.Indices.Contains(i))
                    {
                        color = selectedObjectsColor;
                    }

                    Vector3 center = primitive.Center; // -ActivePosition; 

                    ObjectCenters.Instances[centersIndex++] = new Rug.Game.Core.Data.BasicVertex()
                    {
                        Color = color,
                        Position = new Vector4(center, 1f),
                    };

                    switch (primitive.PrimitiveType)
                    {
                        case PrimitiveType.Sphere:
                            AddSphere(ObjectLines, ref linesIndex, center, new Vector3(primitive.Scale.X), primitive.Orientation, color);  
                            break;
                        case PrimitiveType.Capsule:
                            AddCapsule(ObjectLines, ref linesIndex, center, new Vector3(primitive.Scale.X, primitive.Scale.Y, primitive.Scale.X), primitive.Orientation, color);
                            break;
                        case PrimitiveType.Cylinder:
                            AddCylinder(ObjectLines, ref linesIndex, center, new Vector3(primitive.Scale.X, primitive.Scale.Y, primitive.Scale.Z), primitive.Orientation, color);
                            break;
                        case PrimitiveType.Cone:
                            AddCone(ObjectLines, ref linesIndex, center, new Vector3(primitive.Scale.X, primitive.Scale.Y, primitive.Scale.X), primitive.Orientation, color);
                            break;
                        case PrimitiveType.Box:
                            AddCuboid(ObjectLines, ref linesIndex, center, primitive.Scale, primitive.Orientation, color);  
                            break;
                        default:
                            break;
                    }                    
                }

                ObjectCenters.InstanceCount = centersIndex;

                ObjectLines.InstanceCount = linesIndex;
            }
            
            ObjectCenters.Update();
            ObjectLines.Update();

            FaceNormals.InstanceCount = normalsIndex;
            FaceNormals.Update();

            Wireframe.InstanceCount = wireframeIndex;
            Wireframe.Update();

            Vertices.InstanceCount = ModelData.Verts.Count;
            Vertices.Update();

            FaceVertices.InstanceCount = ModelData.Faces.Count;
            FaceVertices.Update();

            HullFrames.InstanceCount = hullVertCount;
            HullFrames.Update();
        }

        private void AddCuboid(PhysicsDebugLines lines, ref int index, Vector3 center, Vector3 size, Color4 color)
        {
            Vector3 topLeft0 = new Vector3(-1, 1, 1);
            Vector3 topRight0 = new Vector3(1, 1, 1);
            Vector3 botLeft0 = new Vector3(-1, -1, 1);
            Vector3 botRight0 = new Vector3(1, -1, 1);

            Vector3 topLeft1 = new Vector3(-1, 1, -1);
            Vector3 topRight1 = new Vector3(1, 1, -1);
            Vector3 botLeft1 = new Vector3(-1, -1, -1);
            Vector3 botRight1 = new Vector3(1, -1, -1);

            topLeft0 = size * topLeft0 + center;
            topRight0 = size * topRight0 + center;
            botLeft0 = size * botLeft0 + center;
            botRight0 = size * botRight0 + center;

            topLeft1 = size * topLeft1 + center;
            topRight1 = size * topRight1 + center;
            botLeft1 = size * botLeft1 + center;
            botRight1 = size * botRight1 + center;

            AddLines(lines, ref index, color, 
                new Vector3[]
                { 
                    topLeft0, topRight0, 
                    topRight0, botRight0,
                    botRight0, botLeft0,
                    botLeft0, topLeft0,

                    topLeft1, topRight1, 
                    topRight1, botRight1,
                    botRight1, botLeft1,
                    botLeft1, topLeft1,

                    topLeft0, topLeft1,
                    topRight0, topRight1,
                    botLeft0, botLeft1,
                    botRight0, botRight1,
                }
            );
        }

        private void AddCylinder(PhysicsDebugLines lines, ref int index, Vector3 center, Vector3 size, Quaternion orentation, Color4 color)
        {            
            Vector3[] vertsY_Top = new Vector3[UnitCircleY.Length * 2];
            Vector3[] vertsY_Bottom = new Vector3[UnitCircleY.Length * 2];
            Vector3[] connections = new Vector3[UnitCircleY.Length * 2];

            for (int i = 0; i < UnitCircleY.Length; i++)
            {
                vertsY_Top[i * 2] = Vector3.Transform(size * (UnitCircleY[i] + (Vector3.UnitY)), orentation) + center;
                vertsY_Bottom[i * 2] = Vector3.Transform(size * (UnitCircleY[i] + (-Vector3.UnitY)), orentation) + center;
            }

            for (int i = 0; i < UnitCircleY.Length - 1; i++)
            {
                if (i < UnitCircleY.Length - 1)
                {
                    vertsY_Top[i * 2 + 1] = vertsY_Top[i * 2 + 2];
                    vertsY_Bottom[i * 2 + 1] = vertsY_Bottom[i * 2 + 2];
                }

                connections[i * 2] = vertsY_Top[i * 2];
                connections[i * 2 + 1] = vertsY_Bottom[i * 2];
            }

            vertsY_Top[UnitCircleX.Length * 2 - 1] = vertsY_Top[0];
            vertsY_Bottom[UnitCircleX.Length * 2 - 1] = vertsY_Bottom[0];

            AddLines(lines, ref index, color,
                vertsY_Top
            );

            AddLines(lines, ref index, color,
                vertsY_Bottom
            );

            AddLines(lines, ref index, color,
                connections
            );
        }

        private void AddCone(PhysicsDebugLines lines, ref int index, Vector3 center, Vector3 size, Quaternion orentation, Color4 color)
        {
            Vector3 vertsY_Top = Vector3.Transform(size * Vector3.UnitY, orentation) + center;
            Vector3[] vertsY_Bottom = new Vector3[UnitCircleY.Length * 2];
            Vector3[] connections = new Vector3[UnitCircleY.Length * 2];

            for (int i = 0; i < UnitCircleY.Length; i++)
            {                
                vertsY_Bottom[i * 2] = Vector3.Transform(size * (UnitCircleY[i] + (-Vector3.UnitY)), orentation) + center;
            }

            for (int i = 0; i < UnitCircleY.Length; i++)
            {
                if (i < UnitCircleY.Length - 1)
                {
                    vertsY_Bottom[i * 2 + 1] = vertsY_Bottom[i * 2 + 2];
                }
           
                connections[i * 2] = vertsY_Top;
                connections[i * 2 + 1] = vertsY_Bottom[i * 2];
            }

            vertsY_Bottom[UnitCircleX.Length * 2 - 1] = vertsY_Bottom[0];

            AddLines(lines, ref index, color,
                vertsY_Bottom
            );

            AddLines(lines, ref index, color,
                connections
            );
        }

        private void AddSphere(PhysicsDebugLines lines, ref int index, Vector3 center, Vector3 size, Quaternion orentation, Color4 color)
        {
            Vector3[] vertsX = new Vector3[UnitCircleX.Length * 2];
            Vector3[] vertsY = new Vector3[UnitCircleY.Length * 2];
            Vector3[] vertsZ = new Vector3[UnitCircleZ.Length * 2];

            for (int i = 0; i < UnitCircleX.Length; i++)
            {
                vertsX[i * 2] = Vector3.Transform(size * UnitCircleX[i], orentation) + center;
                vertsY[i * 2] = Vector3.Transform(size * UnitCircleY[i], orentation) + center;
                vertsZ[i * 2] = Vector3.Transform(size * UnitCircleZ[i], orentation) + center;
            }

            for (int i = 0; i < UnitCircleX.Length - 1; i++)
            {
                vertsX[i * 2 + 1] = vertsX[i * 2 + 2];
                vertsY[i * 2 + 1] = vertsY[i * 2 + 2];
                vertsZ[i * 2 + 1] = vertsZ[i * 2 + 2];
            }

            vertsX[UnitCircleX.Length * 2 - 1] = vertsX[0];
            vertsY[UnitCircleX.Length * 2 - 1] = vertsY[0];
            vertsZ[UnitCircleX.Length * 2 - 1] = vertsZ[0];

            AddLines(lines, ref index, color,
                vertsX
            );

            AddLines(lines, ref index, color,
                vertsY
            );

            AddLines(lines, ref index, color,
                vertsZ
            );
        }

        private void AddCapsule(PhysicsDebugLines lines, ref int index, Vector3 center, Vector3 size, Quaternion orentation, Color4 color)
        {
            {
                Vector3[] vertsY_Top = new Vector3[UnitCircleY.Length * 2];
                Vector3[] vertsY_Bottom = new Vector3[UnitCircleY.Length * 2];
                Vector3[] connections = new Vector3[UnitCircleY.Length * 2];

                for (int i = 0; i < UnitCircleY.Length; i++)
                {
                    vertsY_Top[i * 2] = Vector3.Transform(size * (UnitCircleY[i] + (Vector3.UnitY)), orentation) + center;
                    vertsY_Bottom[i * 2] = Vector3.Transform(size * (UnitCircleY[i] + (-Vector3.UnitY)), orentation) + center;
                }

                for (int i = 0; i < UnitCircleY.Length - 1; i++)
                {
                    if (i < UnitCircleY.Length - 1)
                    {
                        vertsY_Top[i * 2 + 1] = vertsY_Top[i * 2 + 2];
                        vertsY_Bottom[i * 2 + 1] = vertsY_Bottom[i * 2 + 2];
                    }

                    connections[i * 2] = vertsY_Top[i * 2];
                    connections[i * 2 + 1] = vertsY_Bottom[i * 2];
                }

                vertsY_Top[UnitCircleX.Length * 2 - 1] = vertsY_Top[0];
                vertsY_Bottom[UnitCircleX.Length * 2 - 1] = vertsY_Bottom[0];

                AddLines(lines, ref index, color,
                    vertsY_Top
                );

                AddLines(lines, ref index, color,
                    vertsY_Bottom
                );

                AddLines(lines, ref index, color,
                    connections
                );
            }

            int startIndex = 0;
            int endIndex = (UnitCircleX.Length / 2) + 1; 
            Vector3 offsetVec = Vector3.UnitY * size.Y;

            Vector3 radius = new Vector3(size.X); 

            {
                Vector3[] vertsX = new Vector3[endIndex * 2];
                Vector3[] vertsZ = new Vector3[endIndex * 2];

                for (int i = 0; i < endIndex; i++)
                {
                    vertsX[i * 2] = Vector3.Transform((radius * (UnitCircleX[(startIndex + i) % UnitCircleX.Length])) + offsetVec, orentation) + center;
                    vertsZ[i * 2] = Vector3.Transform((radius * (UnitCircleZ[(startIndex + i) % UnitCircleX.Length])) + offsetVec, orentation) + center;
                }

                for (int i = 0; i < endIndex - 1; i++)
                {
                    vertsX[i * 2 + 1] = vertsX[i * 2 + 2];
                    vertsZ[i * 2 + 1] = vertsZ[i * 2 + 2];
                }

                vertsX[endIndex * 2 - 1] = vertsX[0];
                vertsZ[endIndex * 2 - 1] = vertsZ[0];

                AddLines(lines, ref index, color,
                    vertsX
                );

                AddLines(lines, ref index, color,
                    vertsZ
                );
            }
 
            startIndex = (UnitCircleX.Length / 2);
            offsetVec = -Vector3.UnitY * size.Y;

            {
                Vector3[] vertsX = new Vector3[endIndex * 2];
                Vector3[] vertsZ = new Vector3[endIndex * 2];

                for (int i = 0; i < endIndex; i++)
                {
                    vertsX[i * 2] = Vector3.Transform((radius * (UnitCircleX[(startIndex + i) % UnitCircleX.Length])) + offsetVec, orentation) + center;
                    vertsZ[i * 2] = Vector3.Transform((radius * (UnitCircleZ[(startIndex + i) % UnitCircleX.Length])) + offsetVec, orentation) + center;
                }

                for (int i = 0; i < endIndex - 1; i++)
                {
                    vertsX[i * 2 + 1] = vertsX[i * 2 + 2];
                    vertsZ[i * 2 + 1] = vertsZ[i * 2 + 2];
                }

                vertsX[endIndex * 2 - 1] = vertsX[0];
                vertsZ[endIndex * 2 - 1] = vertsZ[0];

                AddLines(lines, ref index, color,
                    vertsX
                );

                AddLines(lines, ref index, color,
                    vertsZ
                );
            }
        }

        private void AddCuboid(PhysicsDebugLines lines, ref int index, Vector3 center, Vector3 size, Quaternion orentation, Color4 color)
        {
            Vector3 topLeft0 = new Vector3(-1, 1, 1);
            Vector3 topRight0 = new Vector3(1, 1, 1);
            Vector3 botLeft0 = new Vector3(-1, -1, 1);
            Vector3 botRight0 = new Vector3(1, -1, 1);

            Vector3 topLeft1 = new Vector3(-1, 1, -1);
            Vector3 topRight1 = new Vector3(1, 1, -1);
            Vector3 botLeft1 = new Vector3(-1, -1, -1);
            Vector3 botRight1 = new Vector3(1, -1, -1);

            topLeft0 = Vector3.Transform(size * topLeft0, orentation) + center;
            topRight0 = Vector3.Transform(size * topRight0, orentation) + center;
            botLeft0 = Vector3.Transform(size * botLeft0, orentation) + center;
            botRight0 = Vector3.Transform(size * botRight0, orentation) + center;

            topLeft1 = Vector3.Transform(size * topLeft1, orentation) + center;
            topRight1 = Vector3.Transform(size * topRight1, orentation) + center;
            botLeft1 = Vector3.Transform(size * botLeft1, orentation) + center;
            botRight1 = Vector3.Transform(size * botRight1, orentation) + center;

            AddLines(lines, ref index, color,
                new Vector3[]
                { 
                    topLeft0, topRight0, 
                    topRight0, botRight0,
                    botRight0, botLeft0,
                    botLeft0, topLeft0,

                    topLeft1, topRight1, 
                    topRight1, botRight1,
                    botRight1, botLeft1,
                    botLeft1, topLeft1,

                    topLeft0, topLeft1,
                    topRight0, topRight1,
                    botLeft0, botLeft1,
                    botRight0, botRight1,
                }
            );
        }

        private void SetFaceColor_Active(int index, Color4 color)
        {
            FaceData data = Model.ActiveData.Faces[index];

            data.Color = color;

            Model.ActiveData.Faces[index] = data; 
        }

        private void AddLines(PhysicsDebugLines lines, ref int index, Color4 color, Vector3[] verts)
        {            
            for (int i = 0; i < verts.Length; i += 2)
            {
                if (index >= lines.MaxCount)
                {
                    return; 
                }

                lines.Starts[index] = verts[i];
                lines.Ends[index] = verts[i + 1];
                lines.Colors[index++] = color;
            }
        }

        public virtual void LoadResources()
        {
            ResourceManagers.LoadResources();            
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1 + ResourceManagers.Total; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= (this as IResourceManager).Count)
            {
                process.Pop();

                Disposed = false;
            }
            else
            {
                process.Increment();
                process.Push(ResourceManagers);
            }
        }

        public virtual void UnloadResources()
        {
            ResourceManagers.UnloadResources();
        }

        public void Dispose()
        {
            ResourceManagers.Dispose();
        }
    }
}
