﻿using Rug.Game.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    class EditorInputHandler : IInputHandler
    {
        public bool Enabled
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool HasFocus
        {
            get { throw new NotImplementedException(); }
        }

        public void OnKeyDown(System.Windows.Forms.KeyEventArgs args, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnKeyUp(System.Windows.Forms.KeyEventArgs args, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnKeyPress(char @char, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnMouseDown(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnMouseUp(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnMouseClick(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnMouseMoved(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void OnMouseWheel(System.Windows.Forms.MouseEventArgs e, out bool handled)
        {
            throw new NotImplementedException();
        }

        public void Invalidate()
        {
            throw new NotImplementedException();
        }
    }
}
