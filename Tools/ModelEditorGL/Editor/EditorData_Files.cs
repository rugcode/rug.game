﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.MeshData;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Editor
{
    partial class EditorData
    {
        public string FilePath { get; set; }

        public bool CanSave { get { return String.IsNullOrWhiteSpace(FilePath) == false; } }

        public void ClearEditor()
        {
            ModelID = 0;
            FilePath = null;
            NeedsSave = false;

            InputState = new EditorInputState();

            Selection.Clear();
            SelectionCache.Clear();

            UndoHistory.Clear();
            UndoHistory.Add(UndoHistoryStepType.SelectionChange);

            foreach (EditableModelData data in Models.Values)
            {
                data.Dispose();
            }

            Models.Clear();
            ResourceManagers.Clear();

            UpdateModels();
        }

        private void AddDefaultModel()
        {
            EditableModelData editableData = AddModel(ModelID++, new ModelData());
            
            UndoHistory.Clear();

            UndoHistory.Add(UndoHistoryStepType.SelectionChange);
        }

        public void New()
        {
            ClearEditor();

            AddDefaultModel(); 
        }

        public void Open(string filepath)
        {
            ClearEditor();

            FilePath = filepath;

            ModelData data = new ModelData();

            data.Load(filepath);

            EditableModelData editableData = AddModel(ModelID++, data);   

            //UndoHistory.Add(UndoHistoryStepType.ModelAdded, editableData.ModelID);

            UndoHistory.Clear();
            UndoHistory.Add(UndoHistoryStepType.SelectionChange);

            UpdateModels();
        }

        public void ImportObj(string filepath)
        {
            ClearEditor();

            Vector3[] verts;
            ushort[] indices;

            ObjLoader.LoadObject_Indexed(filepath, Vector4.Zero, out verts, out indices);

            ModelData data = new ModelData();

            foreach (Vector3 vert in verts)
            {
                data.Verts.Add(vert);
            }

            for (int i = 0; i < indices.Length; i += 3)
            {
                data.Faces.Add(new FaceData()
                {
                    Group = -1,
                    Color = Color4.White,
                    V0 = indices[i],
                    V1 = indices[i + 1],
                    V2 = indices[i + 2],
                });
            }

            data.CalcuateFaceNormals();

            EditableModelData editableData = AddModel(ModelID++, data);             

            //UndoHistory.Add(UndoHistoryStepType.ModelAdded, editableData.ModelID);

            UndoHistory.Clear();
            UndoHistory.Add(UndoHistoryStepType.SelectionChange);
            
            NeedsSave = true;

            UpdateModels();
        }
        
        public void Save()
        {
            if (CanSave == false)
            {
                return;
            }

            int index = 0;

            foreach (EditableModelData data in Models.Values)
            {
                if (index == 0)
                {
                    data.ModelData.Save(FilePath);
                }
                else
                {
                    FileInfo fileInfo = new FileInfo(FilePath);

                    data.ModelData.Save(fileInfo.FullName.Substring(0, fileInfo.FullName.Length - fileInfo.Extension.Length) + "-" + index + fileInfo.Extension);
                }
            }

            NeedsSave = false;
        }

        public void SaveAs(string filepath)
        {
            FilePath = filepath;

            Save();
        }

        public void Close()
        {
            New();
        }

        public void Add(string filepath)
        {
            ModelData data = new ModelData();

            data.Load(filepath);

            EditableModelData editableData = AddModel(ModelID++, data); 

            if (InputState.SelectionMode == ModelSelectionMode.Objects)
            {
                editableData.NormaliseForObjectEdit();
            }

            UndoHistory.Add(UndoHistoryStepType.ModelAdded, editableData.ModelID);

            UpdateModels();
        }
    }
}
