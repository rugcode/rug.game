﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Editor
{   
    class EditorInputState
    {
        public EditorInputAction CurrentAction;

        public ViewModes SelectionView;
        public Rectangle SelectionViewRectangle;

        public Vector3 Center = Vector3.Zero;
        public Vector3 StartCenter = Vector3.Zero;
        public Point DragStart;

        public Point SelectionStart = Point.Empty;
        public Point SelectionEnd = Point.Empty; 
        public Rectangle SelectionRectangle = Rectangle.Empty; 

        public Quaternion Rotation = Quaternion.Identity;
        public Quaternion DragStartRotation;

        public bool SnapToGrid = true;
        public float GridScale = 16f;
        
        public float Zoom = 2f;
        public EditorToolMode ToolMode;
        public ModelSelectionMode SelectionMode;
        public Color PaintColor;

        public EditorInputState()
        {
            Rotation = Quaternion.FromAxisAngle(Vector3.UnitY, -MathHelper.PiOver4) * 
                        Quaternion.FromAxisAngle(Vector3.UnitX, -MathHelper.PiOver4); 
        }

        public void GetMatrix(ViewModes mode, Rectangle rect, out Matrix4 transform, out Quaternion rotation)
        {
            switch (mode)
            {
                case ViewModes.Top:
                    rotation = Quaternion.FromAxisAngle(Vector3.UnitX, -MathHelper.PiOver2);
                    break;
                case ViewModes.Bottom:
                    rotation = Quaternion.FromAxisAngle(Vector3.UnitX, -MathHelper.PiOver2) * Quaternion.FromAxisAngle(Vector3.UnitY, MathHelper.Pi);
                    break;
                case ViewModes.Left:
                    rotation = Quaternion.FromAxisAngle(Vector3.UnitY, MathHelper.PiOver2);
                    break;
                case ViewModes.Right:
                    rotation = Quaternion.FromAxisAngle(Vector3.UnitY, -MathHelper.PiOver2);
                    break;
                case ViewModes.Front:
                    rotation = Quaternion.Identity;
                    break;
                case ViewModes.Back:
                    rotation = Quaternion.FromAxisAngle(Vector3.UnitY, MathHelper.Pi);
                    break;
                case ViewModes.Iso:
                    rotation = Rotation;
                    break;
                default:
                    rotation = Quaternion.Identity;
                    break;
            }

            transform = Matrix4.Identity;
        }

        public void SetCurrentAction(ViewModes mode, System.Windows.Forms.MouseButtons buttons)
        {
            bool isControl = (Control.ModifierKeys & Keys.Control) == Keys.Control;
            bool isAlt = (Control.ModifierKeys & Keys.Alt) == Keys.Alt;
            bool isShift = (Control.ModifierKeys & Keys.Shift) == Keys.Shift;

            if (isAlt == true && buttons == MouseButtons.Left)
            {
                if (mode == ViewModes.Iso && isShift == false)
                {
                    CurrentAction = EditorInputAction.ViewRotate;
                }
                else
                {
                    CurrentAction = EditorInputAction.ViewPan;
                }

                return; 
            }

            if (ToolMode == EditorToolMode.Move)
            {
                if (buttons == MouseButtons.Left && isShift == false)
                {
                    CurrentAction = EditorInputAction.Move_Snap; 
                }
                else
                {
                    CurrentAction = EditorInputAction.Move;
                }

                return; 
            }

            if (ToolMode == EditorToolMode.Rotate)
            {
                if (buttons == MouseButtons.Left && isShift == false)
                {
                    CurrentAction = EditorInputAction.Rotate_Snap;
                }
                else
                {
                    CurrentAction = EditorInputAction.Rotate;
                }

                return;
            }

            if (ToolMode == EditorToolMode.Scale)
            {
                if (buttons == MouseButtons.Left)
                {
                    if (isShift == false)
                    {
                        CurrentAction = EditorInputAction.Scale_Snap;
                    }
                    else
                    {
                        CurrentAction = EditorInputAction.Scale_Uniform_Snap;
                    }
                }
                else
                {
                    if (isShift == false)
                    {
                        CurrentAction = EditorInputAction.Scale;
                    }
                    else
                    {
                        CurrentAction = EditorInputAction.Scale_Uniform;
                    }
                }

                return;
            }

            if (ToolMode == EditorToolMode.Paint)
            {
                if (buttons == MouseButtons.Left && isShift == false)
                {
                    CurrentAction = EditorInputAction.Paint;
                }
                else
                {
                    CurrentAction = EditorInputAction.Paint_Flood;
                }

                return;
            }

            if (ToolMode == EditorToolMode.Pipette)
            {
                if (buttons == MouseButtons.Left)
                {
                    CurrentAction = EditorInputAction.Pipette;
                }                

                return;
            }

            if (ToolMode == EditorToolMode.Select)
            {
                if (buttons == MouseButtons.Left)
                {
                    if (isControl == true)
                    {
                        CurrentAction = EditorInputAction.SubtractSelect_Single;
                    }
                    else if (isShift == true)
                    {
                        CurrentAction = EditorInputAction.AddSelect_Single;
                    }
                    else
                    {
                        CurrentAction = EditorInputAction.Select_Single;
                    }
                }
                else if (buttons == MouseButtons.Right)
                {
                    if (isControl == true)
                    {
                        CurrentAction = EditorInputAction.SubtractSelect_Rect;
                    }
                    else if (isShift == true)
                    {
                        CurrentAction = EditorInputAction.AddSelect_Rect;
                    }
                    else
                    {
                        CurrentAction = EditorInputAction.Select_Rect;
                    }
                }
                else if (buttons == MouseButtons.Middle)
                {
                    if (isControl == true)
                    {
                        CurrentAction = EditorInputAction.SubtractSelect_Rect_Through;
                    }
                    else if (isShift == true)
                    {
                        CurrentAction = EditorInputAction.AddSelect_Rect_Through;
                    }
                    else
                    {
                        CurrentAction = EditorInputAction.Select_Rect_Through;
                    }
                }
            }

            /* 
            if (buttons == MouseButtons.Right) 
            {
                if (mode == ViewModes.Iso && isShift == false)
                {
                    CurrentAction = EditorInputAction.Rotation;
                    return; 
                }

                CurrentAction = EditorInputAction.Pan;
                return;                 
            }

            if (ToolMode == EditorToolMode.Move)
            {
                CurrentAction = EditorInputAction.Move;
            }
            else if (ToolMode == EditorToolMode.Paint)
            {

            }
            else if (ToolMode == EditorToolMode.Select)
            {
                if (isShift == true && isControl == true)
                {
                    CurrentAction = EditorInputAction.SubtractSelect_Single;
                }
                else if (isShift == true)
                {
                    CurrentAction = EditorInputAction.AddSelect_Single;
                }
                else
                {
                    CurrentAction = EditorInputAction.Select_Single;
                }

                if (isAlt == true)
                {
                    switch (CurrentAction)
                    {
                        case EditorInputAction.Select_Single:
                            CurrentAction = EditorInputAction.Select_Rect;
                            break;
                        case EditorInputAction.AddSelect_Single:
                            CurrentAction = EditorInputAction.AddSelect_Rect;
                            break;
                        case EditorInputAction.SubtractSelect_Single:
                            CurrentAction = EditorInputAction.SubtractSelect_Rect;
                            break;
                    }
                }
            }
             * */ 
        }

        public void ClearAction()
        {
            CurrentAction = EditorInputAction.None; 
        }

        public void DoViewRotation(MouseEventArgs e)
        {
            Rotation = DragStartRotation;

            Rotation = Quaternion.Multiply(Rotation, Quaternion.FromAxisAngle(Vector3.UnitY, ((float)DragStart.X - (float)e.Location.X) * 0.01f));
            Rotation = Quaternion.Multiply(Rotation, Quaternion.FromAxisAngle(Vector3.UnitX, ((float)DragStart.Y - (float)e.Location.Y) * 0.01f));

            Rotation.Normalize();
        }

        public void DoViewPan(MouseEventArgs e)
        {         
            ViewModes mode = SelectionView;
            Rectangle rect = SelectionViewRectangle;

            Quaternion rotation;
            Matrix4 transform;
            GetMatrix(mode, rect, out transform, out rotation);

            float ratio = (float)rect.Width / (float)rect.Height;

            transform = Matrix4.CreateFromQuaternion(rotation);

            Vector3 mouseStart = new Vector3(DragStart.X, DragStart.Y, 0);

            mouseStart.X *= ((Zoom * ratio) / (float)rect.Width);
            mouseStart.Y *= -((Zoom) / (float)rect.Height);

            mouseStart *= 2f;

            mouseStart = Vector3.Transform(mouseStart, transform);

            Vector3 mouseEnd = new Vector3(e.Location.X, e.Location.Y, 0);

            mouseEnd.X *= ((Zoom * ratio) / (float)rect.Width);
            mouseEnd.Y *= -((Zoom) / (float)rect.Height);

            mouseEnd *= 2f;

            mouseEnd = Vector3.Transform(mouseEnd, transform);

            Center = StartCenter - (mouseEnd - mouseStart);
        }

        public void DoZoom(int delta)
        {
            if (delta < 0)
            {
                Zoom *= 1.25f;
            }
            else
            {
                Zoom *= 0.8f;
            }
        }

        public void StartRotation()
        {
            DragStartRotation = Rotation;
        }

        public void StartPan()
        {
            StartCenter = Center;
        }

        public void StartMove(MouseEventArgs e)
        {
            DragStart = e.Location; 
        }

        public void BeginSelect_Rect(MouseEventArgs e)
        {
            SelectionStart = e.Location;
            SelectionEnd = e.Location; 
            SelectionRectangle = Rectangle.Empty; 
        }

        public void UpdateSelect_Rect(MouseEventArgs e)
        {
            SelectionEnd = e.Location;

            UpdateSelectionRectangle();             
        }

        public void EndSelect_Rect(MouseEventArgs e)
        {
            SelectionEnd = e.Location;

            UpdateSelectionRectangle();      
        }

        public void ClearSelect_Rect()
        {
            SelectionStart = Point.Empty;
            SelectionEnd = Point.Empty;
            SelectionRectangle = Rectangle.Empty; 
        }

        private void UpdateSelectionRectangle()
        {
            int minX, maxX, minY, maxY;

            minX = Math.Min(SelectionStart.X, SelectionEnd.X);
            minY = Math.Min(SelectionStart.Y, SelectionEnd.Y);

            maxX = Math.Max(SelectionStart.X, SelectionEnd.X);
            maxY = Math.Max(SelectionStart.Y, SelectionEnd.Y); 

            SelectionRectangle = new Rectangle(minX, minY, maxX - minX, maxY - minY);
        }
    }
}
