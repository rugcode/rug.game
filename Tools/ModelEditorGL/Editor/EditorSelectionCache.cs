﻿using OpenTK;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelEditorGL.Editor
{
    class EditorModelSelectionCache
    {
        public int ModelID;

        public readonly List<Vector3> Verts = new List<Vector3>();
        public readonly List<ModelPrimitive> HullPrimitives = new List<ModelPrimitive>();

        public Quaternion Rotation;        
        public Vector3 Scale;
        public Vector3 Location; 

        public void Cache()
        {
            Verts.Clear(); 
            Verts.AddRange(StaticObjects.Data.Models[ModelID].ModelData.Verts);

            HullPrimitives.Clear();
            HullPrimitives.AddRange(StaticObjects.Data.Models[ModelID].ModelData.HullPrimitives);

            Cache_ObjectInfo();
        }

        public void Cache_ObjectInfo()
        {
            Rotation = StaticObjects.Data.Models[ModelID].ActiveRotation;
            Scale = StaticObjects.Data.Models[ModelID].ActiveScale;
            Location = StaticObjects.Data.Models[ModelID].ActivePosition; 
        }
    }

    class EditorSelectionCache
    {
        public readonly Dictionary<int, EditorModelSelectionCache> Caches = new Dictionary<int, EditorModelSelectionCache>();

        public void Cache(EditorSelection selection)
        {
            Clear();

            foreach (ModelSelectionInfo info in selection.Selection)
            {
                //if (info.Array != Rug.Game.Flat.Models.ModelArrays.Verts)
                if (Caches.ContainsKey(info.ModelID) == true) 
                {
                    //Caches[info.ModelData].Cache_ObjectInfo(); 
                    continue; 
                }

                EditorModelSelectionCache cache = new EditorModelSelectionCache(); 

                cache.ModelID = info.ModelID;

                cache.Cache();

                Caches.Add(cache.ModelID, cache); 
            }
        }

        public void Clear()
        {
            Caches.Clear();
        }
    }
}
