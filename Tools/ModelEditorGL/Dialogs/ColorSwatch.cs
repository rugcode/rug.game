﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ModelEditorGL.Dialogs.ColorPicker;

namespace ModelEditorGL.Dialogs
{
    internal partial class ColorSwatch : UserControl
    {
        private List<Color> m_Colors = new List<Color>();
        private int m_SelectedIndex = -1; 

        [Browsable(false)]
        public Size BoxSize { get; set; }

        [Browsable(false)]
        public Color SelectedColor
        {
            get
            {
                if (m_SelectedIndex == -1)
                {
                    return Color.Black; 
                }

                return m_Colors[m_SelectedIndex]; 
            }
            set
            {
                SelectColor(value); 
            }
        }

        [Browsable(false)]
        public event EventHandler SelectedColorChanged; 

        public ColorSwatch()
        {
            BoxSize = new System.Drawing.Size(18, 18); 

            InitializeComponent();
            
            this.DoubleBuffered = true;
        }

        public bool SelectColor(Color color)
        {
            if (m_Colors.Contains(color) == false)
            {
                m_SelectedIndex = -1;
            }
            else
            {
                m_SelectedIndex = m_Colors.IndexOf(color);

                if (SelectedColorChanged != null)
                {
                    SelectedColorChanged(this, EventArgs.Empty);
                }
            }

            Invalidate();

            return m_SelectedIndex != -1; 
        }

        public void Add(Color color)
        {
            if (m_Colors.Contains(color) == true)
            {
                return; 
            }

            m_Colors.Add(color);

            Invalidate();
        }

        public void Remove(Color color)
        {
            int index = m_Colors.IndexOf(color);

            if (index == -1)
            {
                return; 
            }

            m_Colors.RemoveAt(index); 

            if (m_SelectedIndex == index) 
            {
                m_SelectedIndex = -1; 
            }
            else if (m_SelectedIndex > index) 
            {
                m_SelectedIndex--; 
            }

            Invalidate(); 
        }

        public void Clear()
        {
            m_Colors.Clear();

            m_SelectedIndex = -1;

            Invalidate();
        }

        private void ColorSwatch_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            int hCount = Size.Width / BoxSize.Width;

            int vCount = (m_Colors.Count / hCount) + 1;

            if (Size.Height != vCount * BoxSize.Height)
            {
                Size = new Size(Size.Width, vCount * BoxSize.Height);                
            }

            Invalidate(); 
        }

        private void ColorSwatch_Click(object sender, EventArgs e)
        {
            if (!(e is MouseEventArgs))
            {
                return; 
            }

            MouseEventArgs mouseArgs = e as MouseEventArgs;

            Point location = mouseArgs.Location;

            int hCount = Size.Width / BoxSize.Width;

            int index = 0;

            Rectangle rectangle = new Rectangle(1, 1, BoxSize.Width - 2, BoxSize.Height - 2);

            while (index <= m_Colors.Count)
            {
                for (int x = 0; x < hCount; x++)
                {
                    if (index == m_Colors.Count)
                    {
                        using (ColorPickerDialog picker = new ColorPickerDialog())
                        {
                            picker.Color = StaticObjects.Data.InputState.PaintColor;

                            if (picker.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                            {
                                Invalidate(); 

                                return;
                            }

                            Add(picker.Color);
                            SelectColor(picker.Color); 
                        }

                        Invalidate(); 

                        return;
                    }

                    if (index >= m_Colors.Count)
                    {
                        return;
                    }

                    if (rectangle.Contains(location) == true) 
                    {
                        SelectColor(m_Colors[index]); 

                        Invalidate(); 

                        return; 
                    }

                    index++; 

                    rectangle.Offset(BoxSize.Width, 0);
                }

                rectangle.X = 1;
                rectangle.Offset(0, BoxSize.Height);
            }

            Invalidate(); 
        }

        private void ColorSwatch_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias; 

            int hCount = Size.Width / BoxSize.Width;

            int index = 0;
    
            Rectangle rectangle = new Rectangle(1, 1, BoxSize.Width - 2, BoxSize.Height - 2);

            using (Pen borderPen = new Pen(SystemColors.ControlDarkDark))
            using (Brush ellipseBrush = new SolidBrush(SystemColors.ControlDark))
            using (Pen selectedPen = new Pen(SystemColors.ControlLightLight))
            using (Pen crossPen = new Pen(SystemColors.ControlLightLight))
            {
                selectedPen.DashPattern = new float[] { 2, 2 };
                //crossPen.Width = 3; 

                while (index <= m_Colors.Count)
                {
                    for (int x = 0; x < hCount; x++)
                    {
                        if (index == m_Colors.Count)
                        {
                            e.Graphics.FillEllipse(ellipseBrush, rectangle);
                            e.Graphics.DrawLine(crossPen, new PointF(rectangle.Width * 0.5f + rectangle.X, rectangle.Top + 3), new PointF(rectangle.Width * 0.5f + rectangle.X, rectangle.Bottom - 3));
                            e.Graphics.DrawLine(crossPen, new PointF(rectangle.Left + 3, rectangle.Height * 0.5f + rectangle.Y), new PointF(rectangle.Right - 3, rectangle.Height * 0.5f + rectangle.Y));
                        }

                        if (index >= m_Colors.Count)
                        {
                            return;
                        }

                        bool selected = m_SelectedIndex == index; 

                        using (SolidBrush brush = new SolidBrush(m_Colors[index++]))
                        {
                            e.Graphics.FillRectangle(brush, rectangle);
                        }

                        e.Graphics.DrawRectangle(borderPen, rectangle);

                        if (selected == true)
                        {
                            e.Graphics.DrawRectangle(selectedPen, rectangle);
                        }

                        rectangle.Offset(BoxSize.Width, 0);
                    }

                    rectangle.X = 1;
                    rectangle.Offset(0, BoxSize.Height);
                }
            }
        }
    }
}
