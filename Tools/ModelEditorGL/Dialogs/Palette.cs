﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs
{
    internal partial class Palette : Form
    {
        [Browsable(false)]
        public Color SelectedColor
        {
            get { return colorSwatch1.SelectedColor; }
            set { colorSwatch1.SelectedColor = value; }
        }

        public event EventHandler SelectedColorChanged; 

        public Palette()
        {
            InitializeComponent();

            colorSwatch1.SelectedColorChanged += colorSwatch1_SelectedColorChanged;
        }

        void colorSwatch1_SelectedColorChanged(object sender, EventArgs e)
        {
            if (SelectedColorChanged != null)
            {
                SelectedColorChanged(this, e); 
            }
        }

        public bool SelectColor(Color color)
        {
            return colorSwatch1.SelectColor(color); 
        }

        public void Add(Color color)
        {
            colorSwatch1.Add(color); 
        }

        public void Remove(Color color)
        {
            colorSwatch1.Remove(color); 
        }

        public void Clear()
        {
            colorSwatch1.Clear();
        }

        private void Palette_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.None || e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;

                Hide(); 
            }
        }

        private void Palette_Load(object sender, EventArgs e)
        {
            if (Preferences.Windows["Palette"].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Preferences.Windows["Palette"].Bounds;
            }

            //WindowState = Preferences.Windows["Palette"].WindowState;
		}

        #region Window Resize / Move Events

        private void Form_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Preferences.Windows["Palette"].Bounds = this.DesktopBounds;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                //Preferences.Windows["Logger"].WindowState = WindowState;
            }
        }

        #endregion     
    }
}
