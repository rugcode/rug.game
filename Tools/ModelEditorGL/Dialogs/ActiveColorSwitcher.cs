﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ModelEditorGL.Dialogs.ColorPicker;

namespace ModelEditorGL.Dialogs
{
    internal partial class ActiveColorSwitcher : UserControl
    {
        private Color[] m_Colors = new Color[2];        

        [Browsable(false)]
        public Color this[int index]
        {
            get { return m_Colors[index]; }
            set 
            {
                m_Colors[index] = value;

                Invalidate(); 
            }
        }

        [Browsable(false)]
        public Color SelectedColor
        {
            get { return m_Colors[0]; }
            set
            {
                m_Colors[0] = value;

                Invalidate(); 
            }
        }
        
        public event EventHandler SelectedColorChanged;

        public ActiveColorSwitcher()
        {
            InitializeComponent();
            
            this.DoubleBuffered = true;

            m_Colors[0] = Color.White;
            m_Colors[1] = Color.Black; 
        }

        private void OnSelectedColorChanged()
        {
            if (SelectedColorChanged != null)
            {
                SelectedColorChanged(this, EventArgs.Empty); 
            }
        }

        private void ActiveColorSwitcher_Load(object sender, EventArgs e)
        {
            
        }

        private void ActiveColorSwitcher_Click(object sender, EventArgs e)
        {
            if (!(e is MouseEventArgs))
            {
                return; 
            }

            MouseEventArgs mouseArgs = e as MouseEventArgs;

            Point location = mouseArgs.Location;

            int hWidth = (int)(((float)Size.Width / (float)m_Colors.Length) * 0.5f);
            int hHeight = (int)(((float)Size.Height / (float)m_Colors.Length) * 0.5f);

            Rectangle rectangle = new Rectangle(0, 0, hWidth * 3, hHeight * 3);

            rectangle.Inflate(-3, -3); 

            for (int i = 0; i < m_Colors.Length; i++)
            {               
                if (rectangle.Contains(location) == true) 
                {
                    if (i == 0)
                    {
                        using (ColorPickerDialog picker = new ColorPickerDialog())
                        {
                            picker.Color = this[i];

                            if (picker.ShowDialog(this) != System.Windows.Forms.DialogResult.OK)
                            {
                                Invalidate();

                                return;
                            }

                            this[i] = picker.Color;
                        }
                    }
                    else
                    {
                        Color backup = this[i];
                        Color next;

                        for (int j = 0; j <= i; j++)
                        {
                            next = this[j];

                            this[j] = backup;

                            backup = next; 
                        }
                    }

                    OnSelectedColorChanged(); 

                    Invalidate(); 

                    return; 
                }

                rectangle.Offset(hWidth, hHeight);
            }

            Invalidate(); 
        }

        private void ActiveColorSwitcher_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            int hWidth = (int)(((float)Size.Width / (float)m_Colors.Length) * 0.5f);
            int hHeight = (int)(((float)Size.Height / (float)m_Colors.Length) * 0.5f);

            Rectangle rectangle = new Rectangle(0, 0, hWidth * 3, hHeight * 3);

            rectangle.Inflate(-3, -3);

            rectangle.Offset(hWidth * (m_Colors.Length - 1), hHeight * (m_Colors.Length - 1));

            using (Pen borderPen = new Pen(SystemColors.ControlDarkDark))
            {
                for (int i = m_Colors.Length - 1; i >= 0; i--)
                {
                    using (SolidBrush brush = new SolidBrush(m_Colors[i]))
                    {
                        e.Graphics.FillRectangle(brush, rectangle);
                    }

                    e.Graphics.DrawRectangle(borderPen, rectangle);

                    rectangle.Offset(-hWidth, -hHeight);
                }
            }
        }
    }
}
