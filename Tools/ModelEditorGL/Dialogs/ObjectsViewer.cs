﻿using ModelEditorGL.Editor;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs
{
    public partial class ObjectsViewer : Form
    {
        bool SuspendIndexEvent = false;

        public event EventHandler ObjectSelected; 

        public ObjectsViewer()
        {
            InitializeComponent();
        }

        public void UpdateObjects()
        {
            SuspendIndexEvent = true;

            treeView1.SuspendLayout();
            treeView1.Nodes.Clear();
            treeView1.CheckBoxes = false; 

            foreach (EditableModelData data in StaticObjects.Data.Models.Values)
            {
                TreeNode node = treeView1.Nodes.Add(data.Name);
                
                node.Tag = data;
                //node.Checked = true; 

                CreateSubNode(node, ModelArrays.Verts);
                CreateSubNode(node, ModelArrays.Faces);            
            }

            treeView1.ExpandAll(); 

            treeView1.ResumeLayout();

            SuspendIndexEvent = false; 
        }

        private static void CreateSubNode(TreeNode node, ModelArrays array)
        {
            TreeNode subNode = null; 

            switch (array)
            {
                case ModelArrays.Verts:
                    subNode = node.Nodes.Add("Vertices"); 
                    break;
                case ModelArrays.Faces:
                    subNode = node.Nodes.Add("Faces"); 
                    break;
                case ModelArrays.HullVerts:
                case ModelArrays.Extras:
                case ModelArrays.Objects:
                default:
                    return; 
            }

            subNode.Tag = array;
            //subNode.Checked = true; 
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag is ModelArrays)
            {
                ModelArrays array = (ModelArrays)e.Node.Tag;

                EditableModelData data = e.Node.Parent.Tag as EditableModelData;

                ModelSelectionInfo info = new ModelSelectionInfo()
                {
                    Array = array,
                    ModelID = data.ModelID,
                };

                switch (array)
                {
                    case ModelArrays.Verts:
                        StaticObjects.Data.SetSelectionMode(ModelSelectionMode.Vertices); 
                        for (int i = 0; i < data.ModelData.Verts.Count; i++)
                        {
                            info.Indices.Add(i);
                        }
                        break;
                    case ModelArrays.Faces:
                        StaticObjects.Data.SetSelectionMode(ModelSelectionMode.Faces); 
                        for (int i = 0; i < data.ModelData.Faces.Count; i++)
                        {
                            info.Indices.Add(i);
                        }
                        break;
                    case ModelArrays.HullVerts:
                        break;
                    case ModelArrays.Extras:
                        break;
                    case ModelArrays.Objects:
                        break;
                    default:
                        break;
                }

                //if (e.Node.Checked == true)
                {
                    StaticObjects.Data.Selection.Clear();
                    StaticObjects.Data.Selection.Add(info);
                }
                /* 
                else
                {
                    StaticObjects.Data.Selection.Subtract(info);
                }
                */                
            }
            else if (e.Node.Tag is EditableModelData)
            {
                StaticObjects.Data.SetSelectionMode(ModelSelectionMode.Objects); 

                EditableModelData data = e.Node.Tag as EditableModelData;

                ModelSelectionInfo info = new ModelSelectionInfo()
                {
                    Array = ModelArrays.Objects,
                    ModelID = data.ModelID,
                };

                info.Indices.Add(-1);

                //if (e.Node.Checked == true)
                {
                    StaticObjects.Data.Selection.Clear();
                    StaticObjects.Data.Selection.Add(info);
                }
                //else
                //{
                //    StaticObjects.Data.Selection.Subtract(info);
                //}
            }

            StaticObjects.Data.CacheSelection();
            StaticObjects.Data.UpdateModels();
            StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);
            StaticObjects.Invalidate();
        }

        private void treeView1_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            EditableModelData data = e.Node.Tag as EditableModelData;
            
            data.Name = e.Label; 

            //e.Node.Text = data.ModelID + " " + e.Label;
        }

        private void treeView1_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Node.Tag is ModelArrays)
            {
                e.CancelEdit = true;
                return; 
            }

            //e.Node.Text = (e.Node.Tag as EditableModelData).Name; 
        }

        private void treeView1_BeforeCheck(object sender, TreeViewCancelEventArgs e)
        {
            ModelSelectionMode selectionMode = StaticObjects.Data.InputState.SelectionMode; 

            if (e.Node.Tag is ModelArrays)
            {
                ModelArrays array = (ModelArrays)e.Node.Tag;
                
                switch (array)
                {
                    case ModelArrays.Verts:
                        e.Cancel = selectionMode != ModelSelectionMode.Vertices;
                        break;
                    case ModelArrays.Faces:
                        e.Cancel = selectionMode != ModelSelectionMode.Faces;
                        break;
                    case ModelArrays.HullVerts:                        
                    case ModelArrays.Extras:
                        return; 
                    case ModelArrays.Objects:
                        e.Cancel = selectionMode != ModelSelectionMode.Objects;
                        break;
                    default:
                        break;
                }

                return;
            }
            else if (selectionMode != ModelSelectionMode.Objects)
            {
                e.Cancel = true; 
            }
        }

        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag is ModelArrays)
            {
                ModelArrays array = (ModelArrays)e.Node.Tag;

                EditableModelData data = e.Node.Parent.Tag as EditableModelData;

                ModelSelectionInfo info = new ModelSelectionInfo()
                {
                    Array = array,
                    ModelID = data.ModelID,
                };

                switch (array)
                {
                    case ModelArrays.Verts:
                        for (int i = 0; i < data.ModelData.Verts.Count; i++)
                        {
                            info.Indices.Add(i);
                        }
                        break;
                    case ModelArrays.Faces:
                        for (int i = 0; i < data.ModelData.Faces.Count; i++)
                        {
                            info.Indices.Add(i);
                        }
                        break;
                    case ModelArrays.HullVerts:
                        break;
                    case ModelArrays.Extras:
                        break;
                    case ModelArrays.Objects:
                        break;
                    default:
                        break;
                }

                if (e.Node.Checked == true)
                {
                    StaticObjects.Data.Selection.Add(info);
                }
                else
                {
                    StaticObjects.Data.Selection.Subtract(info);
                }
            }
            else if (e.Node.Tag is EditableModelData)
            {
                EditableModelData data = e.Node.Tag as EditableModelData;

                ModelSelectionInfo info = new ModelSelectionInfo()
                {
                    Array = ModelArrays.Objects,
                    ModelID = data.ModelID,
                };

                info.Indices.Add(-1);

                if (e.Node.Checked == true)
                {
                    StaticObjects.Data.Selection.Add(info);
                }
                else
                {
                    StaticObjects.Data.Selection.Subtract(info);
                }
            }

            StaticObjects.Data.CacheSelection();
            StaticObjects.Data.UpdateModels();
            StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);
            StaticObjects.Invalidate();
            //e.Action = TreeViewAction.
        }

        /* 
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SuspendIndexEvent == true)
            {
                return;
            }

            StaticObjects.Data.UndoHistory.SetUndoIndex(listBox1.SelectedIndex);

            if (ObjectSelected != null)
            {
                ObjectSelected(this, EventArgs.Empty);
            }
        }
        */ 

        private void ObjectsViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.None || e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;

                Hide();
            }
        }

        private void ObjectsViewer_Load(object sender, EventArgs e)
        {
            if (Preferences.Windows["ObjectsViewer"].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Preferences.Windows["ObjectsViewer"].Bounds;
            }

            //WindowState = Preferences.Windows["ObjectsViewer"].WindowState;
        }

        #region Window Resize / Move Events

        private void Form_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Preferences.Windows["ObjectsViewer"].Bounds = this.DesktopBounds;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                //Preferences.Windows["ObjectsViewer"].WindowState = WindowState;
            }
        }

        #endregion 
    }
}
