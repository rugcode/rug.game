﻿namespace ModelEditorGL.Dialogs
{
    partial class Toolbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_SelectButton = new System.Windows.Forms.Button();
            this.m_MoveButton = new System.Windows.Forms.Button();
            this.m_PaintButton = new System.Windows.Forms.Button();
            this.m_Vertices = new System.Windows.Forms.Button();
            this.m_Faces = new System.Windows.Forms.Button();
            this.m_Edges = new System.Windows.Forms.Button();
            this.m_Objects = new System.Windows.Forms.Button();
            this.m_ScaleButton = new System.Windows.Forms.Button();
            this.m_RotateButton = new System.Windows.Forms.Button();
            this.m_PipetteButton = new System.Windows.Forms.Button();
            this.activeColorSwitcher1 = new ModelEditorGL.Dialogs.ActiveColorSwitcher();
            this.SuspendLayout();
            // 
            // m_SelectButton
            // 
            this.m_SelectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_SelectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_SelectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_SelectButton.Location = new System.Drawing.Point(1, 57);
            this.m_SelectButton.Name = "m_SelectButton";
            this.m_SelectButton.Size = new System.Drawing.Size(24, 24);
            this.m_SelectButton.TabIndex = 0;
            this.m_SelectButton.Text = "Select";
            this.m_SelectButton.UseVisualStyleBackColor = true;
            this.m_SelectButton.Click += new System.EventHandler(this.m_SelectButton_Click);
            // 
            // m_MoveButton
            // 
            this.m_MoveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_MoveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_MoveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_MoveButton.Location = new System.Drawing.Point(27, 57);
            this.m_MoveButton.Name = "m_MoveButton";
            this.m_MoveButton.Size = new System.Drawing.Size(24, 24);
            this.m_MoveButton.TabIndex = 1;
            this.m_MoveButton.Text = "Move";
            this.m_MoveButton.UseVisualStyleBackColor = true;
            this.m_MoveButton.Click += new System.EventHandler(this.m_MoveButton_Click);
            // 
            // m_PaintButton
            // 
            this.m_PaintButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_PaintButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_PaintButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_PaintButton.Location = new System.Drawing.Point(1, 109);
            this.m_PaintButton.Name = "m_PaintButton";
            this.m_PaintButton.Size = new System.Drawing.Size(24, 24);
            this.m_PaintButton.TabIndex = 2;
            this.m_PaintButton.Text = "Brush";
            this.m_PaintButton.UseVisualStyleBackColor = true;
            this.m_PaintButton.Click += new System.EventHandler(this.m_PaintButton_Click);
            // 
            // m_Vertices
            // 
            this.m_Vertices.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_Vertices.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_Vertices.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_Vertices.Location = new System.Drawing.Point(1, 1);
            this.m_Vertices.Name = "m_Vertices";
            this.m_Vertices.Size = new System.Drawing.Size(24, 24);
            this.m_Vertices.TabIndex = 5;
            this.m_Vertices.Text = "Vertices";
            this.m_Vertices.UseVisualStyleBackColor = true;
            this.m_Vertices.Click += new System.EventHandler(this.m_Vertices_Click);
            // 
            // m_Faces
            // 
            this.m_Faces.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_Faces.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_Faces.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_Faces.Location = new System.Drawing.Point(1, 27);
            this.m_Faces.Name = "m_Faces";
            this.m_Faces.Size = new System.Drawing.Size(24, 24);
            this.m_Faces.TabIndex = 6;
            this.m_Faces.Text = "Faces";
            this.m_Faces.UseVisualStyleBackColor = true;
            this.m_Faces.Click += new System.EventHandler(this.m_Faces_Click);
            // 
            // m_Edges
            // 
            this.m_Edges.Enabled = false;
            this.m_Edges.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_Edges.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_Edges.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_Edges.Location = new System.Drawing.Point(27, 1);
            this.m_Edges.Name = "m_Edges";
            this.m_Edges.Size = new System.Drawing.Size(24, 24);
            this.m_Edges.TabIndex = 7;
            this.m_Edges.Text = "Edges";
            this.m_Edges.UseVisualStyleBackColor = true;
            this.m_Edges.Click += new System.EventHandler(this.m_Edges_Click);
            // 
            // m_Objects
            // 
            this.m_Objects.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_Objects.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_Objects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_Objects.Location = new System.Drawing.Point(27, 27);
            this.m_Objects.Name = "m_Objects";
            this.m_Objects.Size = new System.Drawing.Size(24, 24);
            this.m_Objects.TabIndex = 8;
            this.m_Objects.Text = "Objects";
            this.m_Objects.UseVisualStyleBackColor = true;
            this.m_Objects.Click += new System.EventHandler(this.m_Objects_Click);
            // 
            // m_ScaleButton
            // 
            this.m_ScaleButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_ScaleButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_ScaleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_ScaleButton.Location = new System.Drawing.Point(1, 83);
            this.m_ScaleButton.Name = "m_ScaleButton";
            this.m_ScaleButton.Size = new System.Drawing.Size(24, 24);
            this.m_ScaleButton.TabIndex = 9;
            this.m_ScaleButton.Text = "Scale";
            this.m_ScaleButton.UseVisualStyleBackColor = true;
            this.m_ScaleButton.Click += new System.EventHandler(this.m_ScaleButton_Click);
            // 
            // m_RotateButton
            // 
            this.m_RotateButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_RotateButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_RotateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_RotateButton.Location = new System.Drawing.Point(27, 83);
            this.m_RotateButton.Name = "m_RotateButton";
            this.m_RotateButton.Size = new System.Drawing.Size(24, 24);
            this.m_RotateButton.TabIndex = 10;
            this.m_RotateButton.Text = "Rotate";
            this.m_RotateButton.UseVisualStyleBackColor = true;
            this.m_RotateButton.Click += new System.EventHandler(this.m_RotateButton_Click);
            // 
            // m_PipetteButton
            // 
            this.m_PipetteButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.m_PipetteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.m_PipetteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_PipetteButton.Location = new System.Drawing.Point(27, 109);
            this.m_PipetteButton.Name = "m_PipetteButton";
            this.m_PipetteButton.Size = new System.Drawing.Size(24, 24);
            this.m_PipetteButton.TabIndex = 11;
            this.m_PipetteButton.Text = "Pipette";
            this.m_PipetteButton.UseVisualStyleBackColor = true;
            this.m_PipetteButton.Click += new System.EventHandler(this.m_PipetteButton_Click);
            // 
            // activeColorSwitcher1
            // 
            this.activeColorSwitcher1.Location = new System.Drawing.Point(1, 163);
            this.activeColorSwitcher1.Name = "activeColorSwitcher1";
            this.activeColorSwitcher1.SelectedColor = System.Drawing.Color.White;
            this.activeColorSwitcher1.Size = new System.Drawing.Size(50, 50);
            this.activeColorSwitcher1.TabIndex = 4;
            this.activeColorSwitcher1.SelectedColorChanged += new System.EventHandler(this.activeColorSwitcher1_SelectedColorChanged);
            // 
            // Toolbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(52, 217);
            this.Controls.Add(this.m_PipetteButton);
            this.Controls.Add(this.m_RotateButton);
            this.Controls.Add(this.m_ScaleButton);
            this.Controls.Add(this.m_Objects);
            this.Controls.Add(this.m_Edges);
            this.Controls.Add(this.m_Faces);
            this.Controls.Add(this.m_Vertices);
            this.Controls.Add(this.activeColorSwitcher1);
            this.Controls.Add(this.m_PaintButton);
            this.Controls.Add(this.m_MoveButton);
            this.Controls.Add(this.m_SelectButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Toolbox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Tools";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Toolbox_FormClosing);
            this.Load += new System.EventHandler(this.Toolbox_Load);
            this.ResizeBegin += new System.EventHandler(this.Form_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.Form_ResizeEnd);
            this.Resize += new System.EventHandler(this.Form_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button m_SelectButton;
        private System.Windows.Forms.Button m_MoveButton;
        private System.Windows.Forms.Button m_PaintButton;
        private ActiveColorSwitcher activeColorSwitcher1;
        private System.Windows.Forms.Button m_Vertices;
        private System.Windows.Forms.Button m_Faces;
        private System.Windows.Forms.Button m_Edges;
        private System.Windows.Forms.Button m_Objects;
        private System.Windows.Forms.Button m_ScaleButton;
        private System.Windows.Forms.Button m_RotateButton;
        private System.Windows.Forms.Button m_PipetteButton;
    }
}