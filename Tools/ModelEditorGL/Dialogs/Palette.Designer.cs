﻿namespace ModelEditorGL.Dialogs
{
    partial class Palette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.colorSwatch1 = new ModelEditorGL.Dialogs.ColorSwatch();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.colorSwatch1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(108, 125);
            this.panel1.TabIndex = 0;
            // 
            // colorSwatch1
            // 
            this.colorSwatch1.BoxSize = new System.Drawing.Size(16, 16);
            this.colorSwatch1.Dock = System.Windows.Forms.DockStyle.Top;
            this.colorSwatch1.Location = new System.Drawing.Point(0, 0);
            this.colorSwatch1.Name = "colorSwatch1";
            this.colorSwatch1.SelectedColor = System.Drawing.Color.Black;
            this.colorSwatch1.Size = new System.Drawing.Size(108, 16);
            this.colorSwatch1.TabIndex = 0;
            // 
            // Palette
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(108, 125);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Palette";
            this.Text = "Palette";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Palette_FormClosing);
            this.Load += new System.EventHandler(this.Palette_Load);
            this.ResizeBegin += new System.EventHandler(this.Form_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.Form_ResizeEnd);
            this.Resize += new System.EventHandler(this.Form_Resize);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private ColorSwatch colorSwatch1;
    }
}