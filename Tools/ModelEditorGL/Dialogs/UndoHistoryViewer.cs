﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs
{
    public partial class UndoHistoryViewer : Form
    {
        bool SuspendIndexEvent = false;

        public event EventHandler HistorySelected; 

        public UndoHistoryViewer()
        {
            InitializeComponent();
        }

        public void UpdateHistory()
        {
            SuspendIndexEvent = true; 

            listBox1.SuspendLayout(); 
            listBox1.Items.Clear(); 

            foreach (Editor.EditorUndoHistoryStep step in StaticObjects.Data.UndoHistory.History)
            {
                listBox1.Items.Add(step.ID + ": " + step.StepType); 
            }

            listBox1.Items.Add("Current State"); 

            if (listBox1.Items.Count > StaticObjects.Data.UndoHistory.Index)
            {
                listBox1.SelectedIndex = StaticObjects.Data.UndoHistory.Index;
            }

            listBox1.ResumeLayout();

            SuspendIndexEvent = false; 
        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SuspendIndexEvent == true)
            {
                return;
            }

            StaticObjects.Data.UndoHistory.SetUndoIndex(listBox1.SelectedIndex);

            if (HistorySelected != null)
            {
                HistorySelected(this, EventArgs.Empty);
            }
        }

        private void ObjectsViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.None || e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;

                Hide();
            }
        }

        private void ObjectsViewer_Load(object sender, EventArgs e)
        {
            if (Preferences.Windows["UndoHistoryViewer"].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Preferences.Windows["UndoHistoryViewer"].Bounds;
            }

            //WindowState = Preferences.Windows["ObjectsViewer"].WindowState;

            //StaticObjects.Data.UndoHistory.Changed += UndoHistory_Changed;
        }

        #region Window Resize / Move Events

        private void Form_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Preferences.Windows["UndoHistoryViewer"].Bounds = this.DesktopBounds;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                //Preferences.Windows["ObjectsViewer"].WindowState = WindowState;
            }
        }

        #endregion 
    }
}
