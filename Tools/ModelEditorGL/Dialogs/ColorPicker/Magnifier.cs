using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal partial class Magnifier : UserControl
	{
		public Magnifier()
		{
			InitializeComponent();						
		}

		private Bitmap m_Image;

		public Bitmap Image
		{
			get { return m_Image; }
			set 
			{ 
				m_Image = value;
				this.Invalidate(); 
			}
		}
	

		private void OnPaint(object sender, PaintEventArgs e)
		{
			if (m_Image != null)
			{			
				e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;

				e.Graphics.DrawImage(m_Image,
										this.ClientRectangle,
										new Rectangle(0, 0, m_Image.Size.Width, m_Image.Height),
										GraphicsUnit.Pixel);

			}
			else
				e.Graphics.Clear(Color.Black); 
		}
	}
}
