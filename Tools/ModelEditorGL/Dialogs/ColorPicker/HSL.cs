using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace ModelEditorGL.Dialogs.ColorPicker
{
	// Hue Saturation and lightness 
    internal class HSL
	{
		#region Static Functions

		/// <summary>
		/// Custom rounding function.
		/// </summary>
		/// <param name="val">Value to round</param>
		/// <returns>Rounded value</returns>
		public static int Round(double val)
		{
			int ret_val = (int)val;

			int temp = (int)(val * 100);

			if ((temp % 100) >= 50)
				ret_val += 1;

			return ret_val;
		}

		public static int ClipColor(double val)
		{
			return ClipColor(HSL.Round(val));
		}

		public static int ClipColor(int val)
		{
			return val > 255 ? 255 : val < 0 ? 0 : val;
		}

		#endregion

		#region Class Variables

		public HSL(Color c)
		{
			int Max, Min, Diff, Sum;

			//	Of our RGB values, assign the highest value to Max, and the Smallest to Min
			if (c.R > c.G)
			{
				Max = c.R;
				Min = c.G;
			}
			else
			{
				Max = c.G;
				Min = c.R;
			}
			if (c.B > Max)
				Max = c.B;
			else if (c.B < Min)
				Min = c.B;

			Diff = Max - Min;
			Sum = Max + Min;

			//	Luminance - a.k.a. Brightness - Adobe photoshop uses the logic that the
			//	site VBspeed regards (regarded) as too primitive = superior decides the 
			//	level of brightness.
			m_Lightness = (double)Max / 255;

			//	Saturation
			if (Max == 0)
				m_Saturation = 0;	//	Protecting from the impossible operation of division by zero.
			else
				m_Saturation = (double)Diff / Max;	//	The logic of Adobe Photoshops is this simple.

			//	Hue		R is situated at the angel of 360 eller noll degrees; 
			//			G vid 120 degrees
			//			B vid 240 degrees
			double q;
			if (Diff == 0)
				q = 0; // Protecting from the impossible operation of division by zero.
			else
				q = (double)60 / Diff;

			if (Max == c.R)
			{
				if (c.G < c.B)
					m_Hue = (double)(360 + q * (c.G - c.B)) / 360;
				else
					m_Hue = (double)(q * (c.G - c.B)) / 360;
			}
			else if (Max == c.G)
				m_Hue = (double)(120 + q * (c.B - c.R)) / 360;
			else if (Max == c.B)
				m_Hue = (double)(240 + q * (c.R - c.G)) / 360;
			else
				m_Hue = 0.0;
		}

		public HSL(double h, double s, double l)
		{
			m_Hue = h;
			m_Saturation = s;
			m_Lightness = l;
		}

		public HSL()
		{
			m_Hue = 0;
			m_Saturation = 0;
			m_Lightness = 0;
		}

		double m_Hue;
		double m_Saturation;
		double m_Lightness;

		#endregion

		#region Public Methods

		public double H
		{
			get { return m_Hue; }
			set
			{
				m_Hue = value;
				m_Hue = m_Hue > 1 ? 1 : m_Hue < 0 ? 0 : m_Hue;
			}
		}


		public double S
		{
			get { return m_Saturation; }
			set
			{
				m_Saturation = value;
				m_Saturation = m_Saturation > 1 ? 1 : m_Saturation < 0 ? 0 : m_Saturation;
			}
		}


		public double L
		{
			get { return m_Lightness; }
			set
			{
				m_Lightness = value;
				m_Lightness = m_Lightness > 1 ? 1 : m_Lightness < 0 ? 0 : m_Lightness;
			}
		}

		/// <summary> 
		/// Converts a colour from HSL to RGB 
		/// </summary> 
		/// <remarks>Adapted from the algoritm in Foley and Van-Dam</remarks> 
		/// <param name="hsl">The HSL value</param> 
		/// <returns>A Color structure containing the equivalent RGB values</returns> 
		public Color Color
		{
			get
			{
				int Max, Mid, Min;
				double q;

				Max = Round(L * 255);
				Min = Round((1.0 - S) * (L / 1.0) * 255);
				q = (double)(Max - Min) / 255;

				if (H >= 0 && H <= (double)1 / 6)
				{
					Mid = Round(((H - 0) * q) * 1530 + Min);
					return Color.FromArgb(Max, Mid, Min);
				}
				else if (H <= (double)1 / 3)
				{
					Mid = Round(-((H - (double)1 / 6) * q) * 1530 + Max);
					return Color.FromArgb(Mid, Max, Min);
				}
				else if (H <= 0.5)
				{
					Mid = Round(((H - (double)1 / 3) * q) * 1530 + Min);
					return Color.FromArgb(Min, Max, Mid);
				}
				else if (H <= (double)2 / 3)
				{
					Mid = Round(-((H - 0.5) * q) * 1530 + Max);
					return Color.FromArgb(Min, Mid, Max);
				}
				else if (H <= (double)5 / 6)
				{
					Mid = Round(((H - (double)2 / 3) * q) * 1530 + Min);
					return Color.FromArgb(Mid, Min, Max);
				}
				else if (H <= 1.0)
				{
					Mid = Round(-((H - (double)5 / 6) * q) * 1530 + Max);
					return Color.FromArgb(Max, Min, Mid);
				}
				else
					return Color.FromArgb(0, 0, 0);
			}
		}

		#endregion
	} 
}
