using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using Rug.Core;
using System.Text.RegularExpressions;

namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal partial class ColorPicker : UserControl
	{
		#region Members 

		private HSL m_HSL;
		private Color m_RGB; 

		#endregion 

		#region Constructor
		
		public ColorPicker()
		{
			InitializeComponent();
		}

		#endregion

		#region Properties

		public Color OriginalColor
		{
			set 
			{
				colorMatcher1.OriginalColor = value;
			}
		}

		public Color RGB
		{
			get
			{
				return m_RGB;
			}
			set
			{
				m_RGB = value;
				m_HSL = new HSL(value);

				SetColorText(); 

				colorMatcher1.ActiveColor = m_RGB;
				this.colorGradBox1.RGB = m_RGB;
				this.colorSlider1.RGB = m_RGB;


			}
		}

		#endregion

		#region Set the text in all the boxes 

		private void SetColorText()
		{
			m_Red.Text = m_RGB.R.ToString();
			m_Green.Text = m_RGB.G.ToString();
			m_Blue.Text = m_RGB.B.ToString();

			m_Hue.Text = HSL.Round(m_HSL.H * 360).ToString();
			m_Sat.Text = HSL.Round(m_HSL.S * 100).ToString();
			m_Luminance.Text = HSL.Round(m_HSL.L * 100).ToString();

            m_Hex.Text = Rug.Game.Helper.Color.ToHtmlString(m_RGB).Substring(1);
		}

		#endregion
		
		#region Internal Events

		#region Color Selector Control Events

		private void ColorGradBoxChanged(object sender, System.EventArgs e)
		{
			m_HSL = this.colorGradBox1.HSL;
			m_RGB = m_HSL.Color;

			SetColorText(); 

			colorSlider1.HSL = m_HSL;
			colorMatcher1.ActiveColor = m_RGB;
		}

		private void ColorSliderScroll(object sender, System.EventArgs e)
		{
			if (colorSlider1.DrawStyle == ColorRange.Red)
			{
				m_RGB = Color.FromArgb(colorSlider1.RGB.R, m_RGB.G, m_RGB.B);
				m_HSL = new HSL(m_RGB);

				colorGradBox1.RGB = m_RGB;
			}
			else if (colorSlider1.DrawStyle == ColorRange.Green)
			{
				m_RGB = Color.FromArgb(m_RGB.R, colorSlider1.RGB.G, m_RGB.B);
				m_HSL = new HSL(m_RGB);

				colorGradBox1.RGB = m_RGB;
			}
			else if (colorSlider1.DrawStyle == ColorRange.Blue)
			{
				m_RGB = Color.FromArgb(m_RGB.R, m_RGB.G, colorSlider1.RGB.B);				
				m_HSL = new HSL(m_RGB);

				colorGradBox1.RGB = m_RGB;
			}
			else
			{
				m_HSL = colorSlider1.HSL;
				m_RGB = m_HSL.Color;

				colorGradBox1.HSL = m_HSL;
			}
			
			SetColorText();		
	
			colorMatcher1.ActiveColor = m_RGB;
		}

		#endregion 

		#region Draw Style Checkboxes

		private void select_Hue(object sender, EventArgs e)
		{
			this.colorGradBox1.DrawStyle = ColorRange.Hue;
			this.colorSlider1.DrawStyle = ColorRange.Hue;
		}

		private void select_Sat(object sender, EventArgs e)
		{
			this.colorGradBox1.DrawStyle = ColorRange.Saturation;
			this.colorSlider1.DrawStyle = ColorRange.Saturation;
		}

		private void select_Luminance(object sender, EventArgs e)
		{
			this.colorGradBox1.DrawStyle = ColorRange.Luminance;
			this.colorSlider1.DrawStyle = ColorRange.Luminance;
		}

		private void select_Red(object sender, EventArgs e)
		{
			this.colorGradBox1.DrawStyle = ColorRange.Red;
			this.colorSlider1.DrawStyle = ColorRange.Red;
		}

		private void select_Green(object sender, EventArgs e)
		{
			this.colorGradBox1.DrawStyle = ColorRange.Green;
			this.colorSlider1.DrawStyle = ColorRange.Green;
		}

		private void select_Blue(object sender, EventArgs e)
		{
			this.colorGradBox1.DrawStyle = ColorRange.Blue;
			this.colorSlider1.DrawStyle = ColorRange.Blue;
		}

		#endregion

		#region Text input boxes 

		private void OnLeave_Hex(object sender, EventArgs e)
		{
            Color col = Rug.Game.Helper.Color.FromHtmlString(this.m_Hex.Text, false);
			
			if (col != Color.Transparent)
			{
				m_RGB = col;
				m_HSL = new HSL(m_RGB);
			}

			RGB = m_RGB; 
		}

		private void OnLeave_Hue(object sender, EventArgs e)
		{
			string text = m_Hue.Text;
			int hue;
			
			if (!int.TryParse(text, out hue))
			{
				MessageBox.Show("Hue must be a number value between 0 and 360");
				m_Hue.Text = HSL.Round(m_HSL.H * 360).ToString();
				return;
			}

			if (hue < 0)
			{
				//MessageBox.Show("An integer between 0 and 360 is required.\nClosest value inserted.");
				m_Hue.Text = "0";
				this.m_HSL.H = 0.0;
			}
			else if (hue > 360)
			{
				//MessageBox.Show("An integer between 0 and 360 is required.\nClosest value inserted.");
				m_Hue.Text = "360";
				this.m_HSL.H = 1.0;
			}
			else
			{
				this.m_HSL.H = (double)hue / 360;
			}

			m_RGB = m_HSL.Color;
			RGB = m_RGB; 
		}

		private void OnLeave_Sat(object sender, EventArgs e)
		{
			string text = m_Sat.Text;
			int sat;

			if (!int.TryParse(text, out sat))
			{
				MessageBox.Show("Saturation must be a number value between 0 and 100");
				m_Sat.Text = HSL.Round(m_HSL.S * 100).ToString();
				return;
			}

			if (sat < 0)
			{
				//MessageBox.Show("An integer between 0 and 100 is required.\nClosest value inserted.");
				m_Sat.Text = "0";
				this.m_HSL.S = 0.0;
			}
			else if (sat > 100)
			{
				//MessageBox.Show("An integer between 0 and 100 is required.\nClosest value inserted.");
				m_Sat.Text = "100";
				this.m_HSL.S = 1.0;
			}
			else
			{
				this.m_HSL.S = (double)sat / 100;
			}

			m_RGB = m_HSL.Color;

			RGB = m_RGB; 
		}

		private void OnLeave_Luminance(object sender, EventArgs e)
		{
			string text = m_Luminance.Text;
			int sat;

			if (!int.TryParse(text, out sat))
			{
				MessageBox.Show("Brightness must be a number value between 0 and 100");
				m_Luminance.Text = HSL.Round(m_HSL.L * 100).ToString();
				return;
			}

			if (sat < 0)
			{
				//MessageBox.Show("An integer between 0 and 100 is required.\nClosest value inserted.");
				m_Luminance.Text = "0";
				this.m_HSL.L = 0.0;
			}
			else if (sat > 100)
			{
				//MessageBox.Show("An integer between 0 and 100 is required.\nClosest value inserted.");
				m_Luminance.Text = "100";
				this.m_HSL.L = 1.0;
			}
			else
			{
				this.m_HSL.L = (double)sat / 100;
			}

			m_RGB = m_HSL.Color;

			RGB = m_RGB; 
		}

		private void OnLeave_Red(object sender, EventArgs e)
		{
			string text = m_Red.Text;
			int col;

			if (!int.TryParse(text, out col))
			{
				MessageBox.Show("Red must be a number value between 0 and 255");
				m_Red.Text = m_RGB.R.ToString();
				return;
			}

			if (col < 0)
			{
				//MessageBox.Show("An integer between 0 and 255 is required.\nClosest value inserted.");
				m_Red.Text = "0";
				m_RGB = Color.FromArgb(0, m_RGB.G, m_RGB.B);
			}
			else if (col > 255)
			{
				//MessageBox.Show("An integer between 0 and 255 is required.\nClosest value inserted.");
				m_Red.Text = "255";
				m_RGB = Color.FromArgb(255, m_RGB.G, m_RGB.B);
			}
			else
			{
				m_RGB = Color.FromArgb(col, m_RGB.G, m_RGB.B);
			}
			
			m_HSL = new HSL(m_RGB);

			RGB = m_RGB; 
		}

		private void OnLeave_Green(object sender, EventArgs e)
		{
			string text = m_Green.Text;
			int col;

			if (!int.TryParse(text, out col))
			{
				MessageBox.Show("Greeb must be a number value between 0 and 255");
				m_Red.Text = m_RGB.G.ToString();
				return;
			}

			if (col < 0)
			{
				//MessageBox.Show("An integer between 0 and 255 is required.\nClosest value inserted.");
				m_Green.Text = "0";
				m_RGB = Color.FromArgb(m_RGB.R, 0, m_RGB.B);
			}
			else if (col > 255)
			{
				//MessageBox.Show("An integer between 0 and 255 is required.\nClosest value inserted.");
				m_Green.Text = "255";
				m_RGB = Color.FromArgb(m_RGB.R, 255, m_RGB.B);
			}
			else
			{
				m_RGB = Color.FromArgb(m_RGB.R, col, m_RGB.B);
			}

			m_HSL = new HSL(m_RGB);

			RGB = m_RGB; 
		}

		private void OnLeave_Blue(object sender, EventArgs e)
		{
			string text = m_Blue.Text;
			int col;

			if (!int.TryParse(text, out col))
			{
				MessageBox.Show("Blue must be a number value between 0 and 255");
				m_Blue.Text = m_RGB.B.ToString();
				return;
			}

			if (col < 0)
			{
				//MessageBox.Show("An integer between 0 and 255 is required.\nClosest value inserted.");
				m_Blue.Text = "0";
				m_RGB = Color.FromArgb(m_RGB.R, m_RGB.G, 0);
			}
			else if (col > 255)
			{
				//MessageBox.Show("An integer between 0 and 255 is required.\nClosest value inserted.");
				m_Blue.Text = "255";
				m_RGB = Color.FromArgb(m_RGB.R, m_RGB.G, 255);
			}
			else
			{
				m_RGB = Color.FromArgb(m_RGB.R, m_RGB.G, col);
			}

			m_HSL = new HSL(m_RGB);

			RGB = m_RGB; 
		}

		#endregion

		private void RevertToPrimary(object sender, EventArgs e)
		{
			m_RGB = colorMatcher1.OriginalColor;
			m_HSL = new HSL(m_RGB);

			RGB = m_RGB; 
		}

		#endregion 
	}
}
