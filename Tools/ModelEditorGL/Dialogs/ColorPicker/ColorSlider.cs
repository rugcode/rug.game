using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs.ColorPicker
{
	#region Color Range 
	
    internal enum ColorRange
	{
		Hue,
		Saturation,
		Luminance,
		Red,
		Green,
		Blue
	}
	
	#endregion 

    internal partial class ColorSlider : UserControl
	{
		#region Members

		//	Slider properties
		private int m_Marker = 0;
		private bool m_Dragging = false;

		//	These variables keep track of how to fill in the content inside the box;
		private ColorRange m_DrawStyle = ColorRange.Hue;

		private HSL m_HSL;
		private Color m_RGB;

		#endregion 

		#region Properties

		public ColorRange DrawStyle
		{
			get { return m_DrawStyle; }
			set 
			{
				m_DrawStyle = value;
				// Reset_Slider(false);
				Reset_Slider();
				Invalidate();
			}
		}

		public HSL HSL
		{
			get { return m_HSL; }
			set 
			{ 
				m_HSL = value;
				m_RGB = value.Color;
				Reset_Slider();
				Invalidate();
			}
		}

		public Color RGB
		{
			get { return m_RGB; }
			set 
			{
				m_RGB = value;
				m_HSL = new HSL(value);
				Reset_Slider();
				Invalidate();			
			}
		}

		#endregion 
		
		#region Event 
		
		public new event EventHandler Scroll;
		
		#endregion 

		#region Constructors

		public ColorSlider()
		{
			InitializeComponent();

			//	Initialize Colors
			m_HSL = new HSL();
			m_HSL.H = 1.0;
			m_HSL.S = 1.0;
			m_HSL.L = 1.0;
			m_RGB = m_HSL.Color;
			m_DrawStyle = ColorRange.Hue;
		}

		#endregion

		#region Control Events

		private void ctrl1DColorBar_Load(object sender, System.EventArgs e)
		{
			Invalidate();
			//Redraw_Control();
		}

		private void ctrl1DColorBar_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Left)	//	Only respond to left mouse button events
				return;

			m_Dragging = true;		//	Begin dragging which notifies MouseMove function that it needs to update the marker

			int y;
			y = e.Y;
			y -= 4;											//	Calculate slider position
			if (y < 0) y = 0;
			if (y > this.Height - 9) y = this.Height - 9;

			if (y == m_Marker)					//	If the slider hasn't moved, no need to redraw it.
				return;										//	or send a scroll notification

			Graphics g = this.CreateGraphics();
			DrawSlider(g, y, false);	//	Redraw the slider
			g.Dispose(); 

			ResetHSLRGB();			//	Reset the color

			if (Scroll != null)	//	Notify anyone who cares that the controls slider(color) has changed
				Scroll(this, e);
		}

		private void ctrl1DColorBar_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (!m_Dragging)		//	Only respond when the mouse is dragging the marker.
				return;

			int y;
			y = e.Y;
			y -= 4; 										//	Calculate slider position
			if (y < 0) 
				y = 0;
			if (y > this.Height - 9) 
				y = this.Height - 9;

			if (y == m_Marker)					//	If the slider hasn't moved, no need to redraw it.
				return;										//	or send a scroll notification

			Graphics g = this.CreateGraphics();
			DrawSlider(g, y, false);	//	Redraw the slider
			g.Dispose();
			ResetHSLRGB();			//	Reset the color

			if (Scroll != null)	//	Notify anyone who cares that the controls slider(color) has changed
				Scroll(this, e);
		}

		private void ctrl1DColorBar_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (e.Button != MouseButtons.Left)	//	Only respond to left mouse button events
				return;

			m_Dragging = false;

			int y;
			y = e.Y;
			y -= 4; 										//	Calculate slider position
			if (y < 0) 
				y = 0;
			if (y > this.Height - 9) 
				y = this.Height - 9;

			if (y == m_Marker)					//	If the slider hasn't moved, no need to redraw it.
				return;										//	or send a scroll notification
			
			Graphics g = this.CreateGraphics();
			DrawSlider(g, y, false);	//	Redraw the slider
			g.Dispose(); 

			ResetHSLRGB();			//	Reset the color

			if (Scroll != null)	//	Notify anyone who cares that the controls slider(color) has changed
				Scroll(this, e);
		}

		private void ctrl1DColorBar_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Redraw_Control(e.Graphics);
		}

		private void ctrl1DColorBar_Resize(object sender, System.EventArgs e)
		{
			Invalidate(); //Redraw_Control();
		}

		#endregion

		#region Inner Render Functions

		/// <summary>
		/// Redraws the background over the slider area on both sides of the control
		/// </summary>
		private void ClearSlider(Graphics g)
		{
			//Graphics g = this.CreateGraphics();
			Brush brush = System.Drawing.SystemBrushes.Control;
			g.FillRectangle(brush, 0, 0, 8, this.Height);				//	clear left hand slider
			g.FillRectangle(brush, this.Width - 8, 0, 8, this.Height);	//	clear right hand slider
		}

		/// <summary>
		/// Draws the slider arrows on both sides of the control.
		/// </summary>
		/// <param name="position">position value of the slider, lowest being at the bottom.  The range
		/// is between 0 and the controls height-9.  The values will be adjusted if too large/small</param>
		/// <param name="Unconditional">If Unconditional is true, the slider is drawn, otherwise some logic 
		/// is performed to determine is drawing is really neccessary.</param>
		private void DrawSlider(Graphics g, int position, bool Unconditional)
		{
			if (position < 0) position = 0;
			if (position > this.Height - 9) position = this.Height - 9;

			if (m_Marker == position && !Unconditional)	//	If the marker position hasn't changed
				return;												//	since the last time it was drawn and we don't HAVE to redraw
			//	then exit procedure

			m_Marker = position;	//	Update the controls marker position

			this.ClearSlider(g);		//	Remove old slider

			//Graphics g = this.CreateGraphics();

			Pen pencil = ResourceManager.Pens[System.Drawing.SystemColors.ControlDarkDark, 1f];//  Color.FromArgb(116, 114, 106));	//	Same gray color Photoshop uses
			Brush brush = Brushes.White;

			Point[] arrow = new Point[7];						//	 GGG
			arrow[0] = new Point(1, position);					//	G   G
			arrow[1] = new Point(3, position);					//	G    G
			arrow[2] = new Point(7, position + 4);				//	G     G
			arrow[3] = new Point(3, position + 8);				//	G      G
			arrow[4] = new Point(1, position + 8);				//	G     G
			arrow[5] = new Point(0, position + 7);				//	G    G
			arrow[6] = new Point(0, position + 1);				//	G   G
			//	 GGG

			g.FillPolygon(brush, arrow);	//	Fill left arrow with white
			g.DrawPolygon(pencil, arrow);	//	Draw left arrow border with gray

			//	    GGG
			arrow[0] = new Point(this.Width - 2, position);		//	   G   G
			arrow[1] = new Point(this.Width - 4, position);		//	  G    G
			arrow[2] = new Point(this.Width - 8, position + 4);	//	 G     G
			arrow[3] = new Point(this.Width - 4, position + 8);	//	G      G
			arrow[4] = new Point(this.Width - 2, position + 8);	//	 G     G
			arrow[5] = new Point(this.Width - 1, position + 7);	//	  G    G
			arrow[6] = new Point(this.Width - 1, position + 1);	//	   G   G
			//	    GGG

			g.FillPolygon(brush, arrow);	//	Fill right arrow with white
			g.DrawPolygon(pencil, arrow);	//	Draw right arrow border with gray

		}


		/// <summary>
		/// Draws the border around the control, in this case the border around the content area between
		/// the slider arrows.
		/// </summary>
		private void DrawBorder(Graphics g)
		{
			Pen pencil;

			// pencil = ResourceManager.Pens[SystemColors.Control, 1f];
			pencil = ResourceManager.Pens[SystemColors.ControlDark, 1f];
			//	Draw top line
			g.DrawLine(pencil, this.Width - 10, 2, 9, 2);
			//	Draw left hand line
			g.DrawLine(pencil, 9, 2, 9, this.Height - 4);

			pencil = ResourceManager.Pens[SystemColors.ControlLightLight, 1f];
			//	Draw right hand line
			g.DrawLine(pencil, this.Width - 9, 2, this.Width - 9, this.Height - 3);
			//	Draw bottome line
			g.DrawLine(pencil, this.Width - 9, this.Height - 3, 9, this.Height - 3);

			//	Draw inner black rectangle
			// pencil = ResourceManager.Pens[SystemColors.ControlDarkDark, 1f];
			pencil = ResourceManager.Pens[Color.Black, 1f];
			g.DrawRectangle(pencil, 10, 3, this.Width - 20, this.Height - 7);
		}


		/// <summary>
		/// Evaluates the DrawStyle of the control and calls the appropriate
		/// drawing function for content
		/// </summary>
		private void DrawContent(Graphics g)
		{
			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					Draw_Style_Hue(g);
					break;
				case ColorRange.Saturation:
					Draw_Style_Saturation(g);
					break;
				case ColorRange.Luminance:
					Draw_Style_Luminance(g);
					break;
				case ColorRange.Red:
					Draw_Style_Red(g);
					break;
				case ColorRange.Green:
					Draw_Style_Green(g);
					break;
				case ColorRange.Blue:
					Draw_Style_Blue(g);
					break;
			}
		}

		#endregion

		#region Render Gradiant
		/// <summary>
		/// Fills in the content of the control showing all values of Hue (from 0 to 360)
		/// </summary>
		private void Draw_Style_Hue(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			HSL _hsl = new HSL();
			_hsl.S = 1.0;	//	S and L will both be at 100% for this DrawStyle
			_hsl.L = 1.0;

			for (int i = 0; i < this.Height - 8; i++)	//	i represents the current line of pixels we want to draw horizontally
			{
				_hsl.H = 1.0 - (double)i / (this.Height - 8);			//	H (hue) is based on the current vertical position
				Pen pen = ResourceManager.Pens[_hsl.Color, 1f];	//	Get the Color for this line

				g.DrawLine(pen, 11, i + 4, this.Width - 11, i + 4);	//	Draw the line and loop back for next line
			}
		}


		/// <summary>
		/// Fills in the content of the control showing all values of Saturation (0 to 100%) for the given
		/// Hue and Luminance.
		/// </summary>
		private void Draw_Style_Saturation(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			HSL _hsl = new HSL();
			_hsl.H = m_HSL.H;	//	Use the H and L values of the current color (m_hsl)
			_hsl.L = m_HSL.L;

			for (int i = 0; i < this.Height - 8; i++) //	i represents the current line of pixels we want to draw horizontally
			{
				_hsl.S = 1.0 - (double)i / (this.Height - 8);			//	S (Saturation) is based on the current vertical position
				Pen pen = ResourceManager.Pens[_hsl.Color, 1f];	//	Get the Color for this line

				g.DrawLine(pen, 11, i + 4, this.Width - 11, i + 4);	//	Draw the line and loop back for next line
			}
		}


		/// <summary>
		/// Fills in the content of the control showing all values of Luminance (0 to 100%) for the given
		/// Hue and Saturation.
		/// </summary>
		private void Draw_Style_Luminance(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			HSL _hsl = new HSL();
			_hsl.H = m_HSL.H;	//	Use the H and S values of the current color (m_hsl)
			_hsl.S = m_HSL.S;

			for (int i = 0; i < this.Height - 8; i++) //	i represents the current line of pixels we want to draw horizontally
			{
				_hsl.L = 1.0 - (double)i / (this.Height - 8);			//	L (Luminance) is based on the current vertical position
				Pen pen = ResourceManager.Pens[_hsl.Color, 1f];	//	Get the Color for this line

				g.DrawLine(pen, 11, i + 4, this.Width - 11, i + 4);	//	Draw the line and loop back for next line
			}
		}


		/// <summary>
		/// Fills in the content of the control showing all values of Red (0 to 255) for the given
		/// Green and Blue.
		/// </summary>
		private void Draw_Style_Red(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			for (int i = 0; i < this.Height - 8; i++) //	i represents the current line of pixels we want to draw horizontally
			{
				int red = 255 - HSL.ClipColor(255 * (double)i / (this.Height - 8));	//	red is based on the current vertical position
				Pen pen = ResourceManager.Pens[Color.FromArgb(red, m_RGB.G, m_RGB.B), 1f];	//	Get the Color for this line

				g.DrawLine(pen, 11, i + 4, this.Width - 11, i + 4);			//	Draw the line and loop back for next line
			}
		}


		/// <summary>
		/// Fills in the content of the control showing all values of Green (0 to 255) for the given
		/// Red and Blue.
		/// </summary>
		private void Draw_Style_Green(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			for (int i = 0; i < this.Height - 8; i++) //	i represents the current line of pixels we want to draw horizontally
			{
				int green = 255 - HSL.ClipColor(255 * (double)i / (this.Height - 8));	//	green is based on the current vertical position
				Pen pen = ResourceManager.Pens[Color.FromArgb(m_RGB.R, green, m_RGB.B), 1f];	//	Get the Color for this line

				g.DrawLine(pen, 11, i + 4, this.Width - 11, i + 4);			//	Draw the line and loop back for next line
			}
		}


		/// <summary>
		/// Fills in the content of the control showing all values of Blue (0 to 255) for the given
		/// Red and Green.
		/// </summary>
		private void Draw_Style_Blue(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			for (int i = 0; i < this.Height - 8; i++) //	i represents the current line of pixels we want to draw horizontally
			{
				int blue = 255 - HSL.ClipColor(255 * (double)i / (this.Height - 8));	//	green is based on the current vertical position
				Pen pen = ResourceManager.Pens[Color.FromArgb(m_RGB.R, m_RGB.G, blue), 1f];	//	Get the Color for this line

				g.DrawLine(pen, 11, i + 4, this.Width - 11, i + 4);			//	Draw the line and loop back for next line
			}
		}


		#endregion

		#region Render Control

		/// <summary>
		/// Calls all the functions neccessary to redraw the entire control.
		/// </summary>
		private void Redraw_Control(Graphics g)
		{
			DrawSlider(g, m_Marker, true);			
			
			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					Draw_Style_Hue(g);
					break;
				case ColorRange.Saturation:
					Draw_Style_Saturation(g);
					break;
				case ColorRange.Luminance:
					Draw_Style_Luminance(g);
					break;
				case ColorRange.Red:
					Draw_Style_Red(g);
					break;
				case ColorRange.Green:
					Draw_Style_Green(g);
					break;
				case ColorRange.Blue:
					Draw_Style_Blue(g);
					break;
			}
			
			DrawBorder(g);
		}


		/// <summary>
		/// Resets the vertical position of the slider to match the controls color.  Gives the option of redrawing the slider.
		/// </summary>
		/// <param name="Redraw">Set to true if you want the function to redraw the slider after determining the best position</param>
		private void Reset_Slider() // Graphics g, bool Redraw)
		{
			//	The position of the marker (slider) changes based on the current drawstyle:
			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					m_Marker = (this.Height - 8) - HSL.Round((this.Height - 8) * m_HSL.H);
					break;
				case ColorRange.Saturation:
					m_Marker = (this.Height - 8) - HSL.Round((this.Height - 8) * m_HSL.S);
					break;
				case ColorRange.Luminance:
					m_Marker = (this.Height - 8) - HSL.Round((this.Height - 8) * m_HSL.L);
					break;
				case ColorRange.Red:
					m_Marker = (this.Height - 8) - HSL.Round((this.Height - 8) * (double)m_RGB.R / 255);
					break;
				case ColorRange.Green:
					m_Marker = (this.Height - 8) - HSL.Round((this.Height - 8) * (double)m_RGB.G / 255);
					break;
				case ColorRange.Blue:
					m_Marker = (this.Height - 8) - HSL.Round((this.Height - 8) * (double)m_RGB.B / 255);
					break;
			}
		}


		/// <summary>
		/// Resets the controls color (both HSL and RGB variables) based on the current slider position
		/// </summary>
		private void ResetHSLRGB()
		{
			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					m_HSL.H = 1.0 - (double)m_Marker / (this.Height - 9);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Saturation:
					m_HSL.S = 1.0 - (double)m_Marker / (this.Height - 9);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Luminance:
					m_HSL.L = 1.0 - (double)m_Marker / (this.Height - 9);
					m_RGB = m_HSL.Color;
					break;
				case ColorRange.Red:
					m_RGB = Color.FromArgb(255 - HSL.ClipColor(255 * (double)m_Marker / (this.Height - 9)), m_RGB.G, m_RGB.B);
					m_HSL = new HSL(m_RGB);
					break;
				case ColorRange.Green:
					m_RGB = Color.FromArgb(m_RGB.R, 255 - HSL.ClipColor(255 * (double)m_Marker / (this.Height - 9)), m_RGB.B);
					m_HSL = new HSL(m_RGB);
					break;
				case ColorRange.Blue:
					m_RGB = Color.FromArgb(m_RGB.R, m_RGB.G, 255 - HSL.ClipColor(255 * (double)m_Marker / (this.Height - 9)));
					m_HSL = new HSL(m_RGB);
					break;
			}
		}

		#endregion
	}
}
