using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal partial class ColorPickerDialog : Form
	{
        public enum ColorPickerDialogMode { Mix, Eye }

        private ColorPickerDialogMode m_PickerMode = ColorPickerDialogMode.Mix; 
        public ColorPickerDialogMode PickerMode 
        {
            get { return m_PickerMode; }
            set
            {
                if (value == ColorPickerDialogMode.Eye && m_PickerMode != ColorPickerDialogMode.Eye)
                    Eye_Click(this, EventArgs.Empty);
                else if (value == ColorPickerDialogMode.Mix && m_PickerMode != ColorPickerDialogMode.Mix)
                    Mix_Click(this, EventArgs.Empty);                
            }            
        }

		#region Members

		private Color original = Color.Transparent;

		#endregion

		public ColorPickerDialog()
		{
			InitializeComponent();
		}
		
		#region Properties
		
		public Color Color
		{
			get 
			{
				//if (this.button3.Enabled)
                if (PickerMode == ColorPickerDialogMode.Mix)
					return this.colorPicker1.RGB;
				else //if (this.eyeDropper1.Enabled)
					return this.eyeDropper1.RGB;

				//return this.colorPicker1.RGB; 
			}
			set 
			{
				original = value; 
				
				this.colorPicker1.OriginalColor = original; 
				this.eyeDropper1.OriginalColor = original;

				// if (this.button3.Enabled) // this.colorPicker1.Enabled)
                if (PickerMode == ColorPickerDialogMode.Mix)
					this.colorPicker1.RGB = value;
				else //if (this.button3.Enabledthis.eyeDropper1.Enabled)
					this.eyeDropper1.RGB = value;
			}
		}

		#endregion

		#region Internal Events

		private void OnClose(object sender, FormClosingEventArgs e)
		{
			this.eyeDropper1.Stop();
		}
		
		#endregion 

		#region Mode Switching (Color mixer / Eyedropper) 

		private void Eye_Click(object sender, EventArgs e)
		{
            if (this.Visible)
			    this.eyeDropper1.Start();

			this.eyeDropper1.Enabled = true;
			this.eyeDropper1.Visible = true;
			this.colorPicker1.Visible = false;

			this.button3.FlatStyle = FlatStyle.Popup;
			this.button3.Enabled = false; 
			this.button4.FlatStyle = FlatStyle.Standard;
			this.button4.Enabled = true;

			this.eyeDropper1.RGB = this.colorPicker1.RGB;

            m_PickerMode = ColorPickerDialogMode.Eye;  
		}

		private void Mix_Click(object sender, EventArgs e)
		{
			this.eyeDropper1.Stop();
			
			this.eyeDropper1.Visible = false;
			this.colorPicker1.Visible = true;

			this.button3.FlatStyle = FlatStyle.Standard;
			this.button3.Enabled = true; 
			this.button4.FlatStyle = FlatStyle.Popup;
			this.button4.Enabled = false;

			this.colorPicker1.RGB = this.eyeDropper1.RGB;

            m_PickerMode = ColorPickerDialogMode.Mix;  
		}
		
		#endregion 

        private void OnVisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
                this.eyeDropper1.Start();
            else
                this.eyeDropper1.Stop();
        }


        void ColorPickerDialog_Load(object sender, System.EventArgs e)
        {
            if (Preferences.Windows["ColorPicker"].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Preferences.Windows["ColorPicker"].Bounds;
            }

            //WindowState = Preferences.Windows["ColorPicker"].WindowState;
        }

        #region Window Resize / Move Events

        private void Form_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Preferences.Windows["ColorPicker"].Bounds = this.DesktopBounds;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                //Preferences.Windows["ColorPicker"].WindowState = WindowState;
            }
        }

        #endregion 
	}
}