namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal partial class ColorPicker
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            ModelEditorGL.Dialogs.ColorPicker.HSL hsl3 = new ModelEditorGL.Dialogs.ColorPicker.HSL();
            ModelEditorGL.Dialogs.ColorPicker.HSL hsl4 = new ModelEditorGL.Dialogs.ColorPicker.HSL();
			this.m_lbl_Black_Symbol = new System.Windows.Forms.Label();
			this.m_lbl_Saturation_Symbol = new System.Windows.Forms.Label();
			this.m_lbl_Hue_Symbol = new System.Windows.Forms.Label();
			this.m_lbl_HexPound = new System.Windows.Forms.Label();
			this.m_rbtn_Blue = new System.Windows.Forms.RadioButton();
			this.m_rbtn_Green = new System.Windows.Forms.RadioButton();
			this.m_rbtn_Red = new System.Windows.Forms.RadioButton();
			this.m_rbtn_Black = new System.Windows.Forms.RadioButton();
			this.m_rbtn_Sat = new System.Windows.Forms.RadioButton();
			this.m_rbtn_Hue = new System.Windows.Forms.RadioButton();
			this.m_Hex = new System.Windows.Forms.TextBox();
			this.m_Blue = new System.Windows.Forms.TextBox();
			this.m_Green = new System.Windows.Forms.TextBox();
			this.m_Red = new System.Windows.Forms.TextBox();
			this.m_Luminance = new System.Windows.Forms.TextBox();
			this.m_Sat = new System.Windows.Forms.TextBox();
			this.m_Hue = new System.Windows.Forms.TextBox();
            this.colorMatcher1 = new ModelEditorGL.Dialogs.ColorPicker.ColorMatcher();
            this.colorSlider1 = new ModelEditorGL.Dialogs.ColorPicker.ColorSlider();
            this.colorGradBox1 = new ModelEditorGL.Dialogs.ColorPicker.ColorGradBox();
			this.SuspendLayout();
			// 
			// m_lbl_Black_Symbol
			// 
			this.m_lbl_Black_Symbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_lbl_Black_Symbol.Location = new System.Drawing.Point(385, 122);
			this.m_lbl_Black_Symbol.Name = "m_lbl_Black_Symbol";
			this.m_lbl_Black_Symbol.Size = new System.Drawing.Size(16, 21);
			this.m_lbl_Black_Symbol.TabIndex = 77;
			this.m_lbl_Black_Symbol.Text = "%";
			// 
			// m_lbl_Saturation_Symbol
			// 
			this.m_lbl_Saturation_Symbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_lbl_Saturation_Symbol.Location = new System.Drawing.Point(385, 97);
			this.m_lbl_Saturation_Symbol.Name = "m_lbl_Saturation_Symbol";
			this.m_lbl_Saturation_Symbol.Size = new System.Drawing.Size(16, 21);
			this.m_lbl_Saturation_Symbol.TabIndex = 76;
			this.m_lbl_Saturation_Symbol.Text = "%";
			// 
			// m_lbl_Hue_Symbol
			// 
			this.m_lbl_Hue_Symbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_lbl_Hue_Symbol.Location = new System.Drawing.Point(385, 72);
			this.m_lbl_Hue_Symbol.Name = "m_lbl_Hue_Symbol";
			this.m_lbl_Hue_Symbol.Size = new System.Drawing.Size(16, 21);
			this.m_lbl_Hue_Symbol.TabIndex = 75;
			this.m_lbl_Hue_Symbol.Text = "�";
			// 
			// m_lbl_HexPound
			// 
			this.m_lbl_HexPound.Location = new System.Drawing.Point(316, 239);
			this.m_lbl_HexPound.Name = "m_lbl_HexPound";
			this.m_lbl_HexPound.Size = new System.Drawing.Size(16, 14);
			this.m_lbl_HexPound.TabIndex = 67;
			this.m_lbl_HexPound.Text = "#";
			// 
			// m_rbtn_Blue
			// 
			this.m_rbtn_Blue.Location = new System.Drawing.Point(312, 202);
			this.m_rbtn_Blue.Name = "m_rbtn_Blue";
			this.m_rbtn_Blue.Size = new System.Drawing.Size(35, 24);
			this.m_rbtn_Blue.TabIndex = 66;
			this.m_rbtn_Blue.Text = "B:";
			this.m_rbtn_Blue.CheckedChanged += new System.EventHandler(this.select_Blue);
			// 
			// m_rbtn_Green
			// 
			this.m_rbtn_Green.Location = new System.Drawing.Point(312, 177);
			this.m_rbtn_Green.Name = "m_rbtn_Green";
			this.m_rbtn_Green.Size = new System.Drawing.Size(35, 24);
			this.m_rbtn_Green.TabIndex = 65;
			this.m_rbtn_Green.Text = "G:";
			this.m_rbtn_Green.CheckedChanged += new System.EventHandler(this.select_Green);
			// 
			// m_rbtn_Red
			// 
			this.m_rbtn_Red.Location = new System.Drawing.Point(312, 152);
			this.m_rbtn_Red.Name = "m_rbtn_Red";
			this.m_rbtn_Red.Size = new System.Drawing.Size(35, 24);
			this.m_rbtn_Red.TabIndex = 64;
			this.m_rbtn_Red.Text = "R:";
			this.m_rbtn_Red.CheckedChanged += new System.EventHandler(this.select_Red);
			// 
			// m_rbtn_Black
			// 
			this.m_rbtn_Black.Location = new System.Drawing.Point(312, 122);
			this.m_rbtn_Black.Name = "m_rbtn_Black";
			this.m_rbtn_Black.Size = new System.Drawing.Size(35, 24);
			this.m_rbtn_Black.TabIndex = 63;
			this.m_rbtn_Black.Text = "B:";
			this.m_rbtn_Black.CheckedChanged += new System.EventHandler(this.select_Luminance);
			// 
			// m_rbtn_Sat
			// 
			this.m_rbtn_Sat.Location = new System.Drawing.Point(312, 97);
			this.m_rbtn_Sat.Name = "m_rbtn_Sat";
			this.m_rbtn_Sat.Size = new System.Drawing.Size(35, 24);
			this.m_rbtn_Sat.TabIndex = 62;
			this.m_rbtn_Sat.Text = "S:";
			this.m_rbtn_Sat.CheckedChanged += new System.EventHandler(this.select_Sat);
			// 
			// m_rbtn_Hue
			// 
			this.m_rbtn_Hue.Checked = true;
			this.m_rbtn_Hue.Location = new System.Drawing.Point(312, 72);
			this.m_rbtn_Hue.Name = "m_rbtn_Hue";
			this.m_rbtn_Hue.Size = new System.Drawing.Size(35, 24);
			this.m_rbtn_Hue.TabIndex = 61;
			this.m_rbtn_Hue.TabStop = true;
			this.m_rbtn_Hue.Text = "H:";
			this.m_rbtn_Hue.CheckedChanged += new System.EventHandler(this.select_Hue);
			// 
			// m_Hex
			// 
			this.m_Hex.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Hex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Hex.Location = new System.Drawing.Point(332, 235);
			this.m_Hex.Name = "m_Hex";
			this.m_Hex.Size = new System.Drawing.Size(56, 21);
			this.m_Hex.TabIndex = 60;
			this.m_Hex.Leave += new System.EventHandler(this.OnLeave_Hex);
			// 
			// m_Blue
			// 
			this.m_Blue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Blue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Blue.Location = new System.Drawing.Point(349, 202);
			this.m_Blue.Name = "m_Blue";
			this.m_Blue.Size = new System.Drawing.Size(35, 21);
			this.m_Blue.TabIndex = 52;
			this.m_Blue.Leave += new System.EventHandler(this.OnLeave_Blue);
			// 
			// m_Green
			// 
			this.m_Green.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Green.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Green.Location = new System.Drawing.Point(349, 177);
			this.m_Green.Name = "m_Green";
			this.m_Green.Size = new System.Drawing.Size(35, 21);
			this.m_Green.TabIndex = 51;
			this.m_Green.Leave += new System.EventHandler(this.OnLeave_Green);
			// 
			// m_Red
			// 
			this.m_Red.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Red.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Red.Location = new System.Drawing.Point(349, 152);
			this.m_Red.Name = "m_Red";
			this.m_Red.Size = new System.Drawing.Size(35, 21);
			this.m_Red.TabIndex = 50;
			this.m_Red.Leave += new System.EventHandler(this.OnLeave_Red);
			// 
			// m_Brightness
			// 
			this.m_Luminance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Luminance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Luminance.Location = new System.Drawing.Point(349, 122);
			this.m_Luminance.Name = "m_Brightness";
			this.m_Luminance.Size = new System.Drawing.Size(35, 21);
			this.m_Luminance.TabIndex = 49;
			this.m_Luminance.Leave += new System.EventHandler(this.OnLeave_Luminance);
			// 
			// m_Sat
			// 
			this.m_Sat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Sat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Sat.Location = new System.Drawing.Point(349, 97);
			this.m_Sat.Name = "m_Sat";
			this.m_Sat.Size = new System.Drawing.Size(35, 21);
			this.m_Sat.TabIndex = 48;
			this.m_Sat.Leave += new System.EventHandler(this.OnLeave_Sat);
			// 
			// m_Hue
			// 
			this.m_Hue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.m_Hue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.m_Hue.Location = new System.Drawing.Point(349, 72);
			this.m_Hue.Name = "m_Hue";
			this.m_Hue.Size = new System.Drawing.Size(35, 21);
			this.m_Hue.TabIndex = 47;
			this.m_Hue.Leave += new System.EventHandler(this.OnLeave_Hue);
			// 
			// colorMatcher1
			// 
			this.colorMatcher1.ActiveColor = System.Drawing.Color.White;
			this.colorMatcher1.Location = new System.Drawing.Point(312, 3);
			this.colorMatcher1.Name = "colorMatcher1";
			this.colorMatcher1.OriginalColor = System.Drawing.Color.Black;
			this.colorMatcher1.Size = new System.Drawing.Size(96, 48);
			this.colorMatcher1.TabIndex = 83;
			this.colorMatcher1.ColorChanged += new System.EventHandler(this.RevertToPrimary);
			// 
			// colorSlider1
			// 
            this.colorSlider1.DrawStyle = ModelEditorGL.Dialogs.ColorPicker.ColorRange.Hue;
			hsl3.H = 0;
			hsl3.L = 1;
			hsl3.S = 1;
			this.colorSlider1.HSL = hsl3;
			this.colorSlider1.Location = new System.Drawing.Point(265, 3);
			this.colorSlider1.Name = "colorSlider1";
			this.colorSlider1.RGB = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.colorSlider1.Size = new System.Drawing.Size(41, 260);
			this.colorSlider1.TabIndex = 1;
			this.colorSlider1.Scroll += new System.EventHandler(this.ColorSliderScroll);
			// 
			// colorGradBox1
			// 
            this.colorGradBox1.DrawStyle = ModelEditorGL.Dialogs.ColorPicker.ColorRange.Hue;
			hsl4.H = 0;
			hsl4.L = 1;
			hsl4.S = 0;
			this.colorGradBox1.HSL = hsl4;
			this.colorGradBox1.Location = new System.Drawing.Point(3, 3);
			this.colorGradBox1.Name = "colorGradBox1";
			this.colorGradBox1.RGB = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.colorGradBox1.Size = new System.Drawing.Size(260, 260);
			this.colorGradBox1.TabIndex = 0;
			this.colorGradBox1.Scroll += new System.EventHandler(this.ColorGradBoxChanged);
			// 
			// ColorPicker
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.m_lbl_Black_Symbol);
			this.Controls.Add(this.m_lbl_Saturation_Symbol);
			this.Controls.Add(this.m_lbl_Hue_Symbol);
			this.Controls.Add(this.m_lbl_HexPound);
			this.Controls.Add(this.m_rbtn_Blue);
			this.Controls.Add(this.m_rbtn_Green);
			this.Controls.Add(this.m_rbtn_Red);
			this.Controls.Add(this.m_rbtn_Black);
			this.Controls.Add(this.m_rbtn_Sat);
			this.Controls.Add(this.m_rbtn_Hue);
			this.Controls.Add(this.m_Hex);
			this.Controls.Add(this.m_Blue);
			this.Controls.Add(this.m_Green);
			this.Controls.Add(this.m_Red);
			this.Controls.Add(this.m_Luminance);
			this.Controls.Add(this.m_Sat);
			this.Controls.Add(this.m_Hue);
			this.Controls.Add(this.colorSlider1);
			this.Controls.Add(this.colorGradBox1);
			this.Controls.Add(this.colorMatcher1);
			this.Name = "ColorPicker";
			this.Size = new System.Drawing.Size(416, 271);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private ColorGradBox colorGradBox1;
		private ColorSlider colorSlider1;
		private System.Windows.Forms.Label m_lbl_Black_Symbol;
		private System.Windows.Forms.Label m_lbl_Saturation_Symbol;
		private System.Windows.Forms.Label m_lbl_Hue_Symbol;
		private System.Windows.Forms.Label m_lbl_HexPound;
		private System.Windows.Forms.RadioButton m_rbtn_Blue;
		private System.Windows.Forms.RadioButton m_rbtn_Green;
		private System.Windows.Forms.RadioButton m_rbtn_Red;
		private System.Windows.Forms.RadioButton m_rbtn_Black;
		private System.Windows.Forms.RadioButton m_rbtn_Sat;
		private System.Windows.Forms.RadioButton m_rbtn_Hue;
		private System.Windows.Forms.TextBox m_Hex;
		private System.Windows.Forms.TextBox m_Blue;
		private System.Windows.Forms.TextBox m_Green;
		private System.Windows.Forms.TextBox m_Red;
		private System.Windows.Forms.TextBox m_Luminance;
		private System.Windows.Forms.TextBox m_Sat;
		private System.Windows.Forms.TextBox m_Hue;
		private ColorMatcher colorMatcher1;
	}
}
