using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal partial class ColorMatcher : UserControl
	{
		public ColorMatcher()
		{
			InitializeComponent();
		}

		public event EventHandler ColorChanged; 

		private Color m_OriginalColor = Color.Black;

		public Color OriginalColor
		{
			get { return m_OriginalColor; }
			set 
			{ 
				m_OriginalColor = value;
				int h = (this.Width - 4) / 2;
				Graphics g = this.CreateGraphics();
				g.FillRectangle(ResourceManager.Brushes[m_OriginalColor], 2 + h, 2, h, this.Height - 4);
				g.Dispose(); 

			}
		}

		private Color m_ActiveColor = Color.White;

		public Color ActiveColor
		{
			get { return m_ActiveColor; }
			set 
			{ 
				m_ActiveColor = value;
				//Invalidate();
				int h = (this.Width - 4) / 2;
				Graphics g = this.CreateGraphics();
				g.FillRectangle(ResourceManager.Brushes[m_ActiveColor], 2, 2, h, this.Height - 4);
				g.Dispose(); 
			}
		}

		private void DrawBorder(Graphics g)
		{
			Pen pencil;

			// pencil = ResourceManager.Pens[SystemColors.Control, 1f];
			pencil = ResourceManager.Pens[SystemColors.ControlDark, 1f];
			//	Draw top line
			g.DrawLine(pencil, this.Width - 2, 0, 0, 0);
			//	Draw left hand line
			g.DrawLine(pencil, 0, 0, 0, this.Height - 2);

			pencil = ResourceManager.Pens[SystemColors.ControlLightLight, 1f];
			//	Draw right hand line
			g.DrawLine(pencil, this.Width - 1, 0, this.Width - 1, this.Height - 1);
			//	Draw bottome line
			g.DrawLine(pencil, this.Width - 1, this.Height - 1, 0, this.Height - 1);

			//	Draw inner black rectangle
			// pencil = ResourceManager.Pens[SystemColors.ControlDarkDark, 1f];
			pencil = ResourceManager.Pens[Color.Black, 1f];
			g.DrawRectangle(pencil, 1, 1, this.Width - 3, this.Height - 3);
		}

		private void OnPaint(object sender, PaintEventArgs e)
		{
			int h = (this.Width - 4) / 2;
			e.Graphics.FillRectangle(ResourceManager.Brushes[ActiveColor], 2, 2, h, this.Height - 4);
			e.Graphics.FillRectangle(ResourceManager.Brushes[OriginalColor], 2 + h, 2, h, this.Height - 4);
			DrawBorder(e.Graphics); 
		}

		private void OnClick(object sender, MouseEventArgs e)
		{
			if (e.Location.X > this.Width / 2)
			{
				if (ColorChanged != null)
					ColorChanged(this, null); 
			}
		}
	}
}
