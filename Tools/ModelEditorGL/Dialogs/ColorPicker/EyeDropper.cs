using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal partial class EyeDropper : UserControl
	{
		#region Members
		private HatchBrush m_Brush = null;
			
		private bool m_CaptureActive = true;
		private bool m_ShowFollow = false;
		
		/// <summary>
		/// Follows the cursor while it is not over the control 
		/// is 1x1 pixel big and should be hidden 
		/// </summary>
		private CursorFollow m_Follow;
		/// <summary>
		/// 1x1 image for grabbing the stealth pixel 
		/// </summary>
		private Bitmap m_Stealth;
        private Graphics m_StelthGraphics; 

		private Color m_RGB = Color.Black;

		private Bitmap m_Capture;
		private Bitmap m_LastCapture;

		#endregion 

		#region Constructor 

		public EyeDropper()
		{
			InitializeComponent();

			this.comboBox1.SelectedIndex = 0;
			this.comboBox1.Enabled = false; 

			m_Brush = new HatchBrush(HatchStyle.LargeCheckerBoard, Color.White, Color.LightGray);

			this.numericUpDown3.Value = (decimal)20;
			
			this.timer1.Interval = (int)this.numericUpDown3.Value;

			m_Stealth = new Bitmap(1, 1, PixelFormat.Format32bppArgb);
            m_StelthGraphics = Graphics.FromImage(m_Stealth);            

		}

		#endregion 

		#region Properties

		public bool CaptureActive
		{
			get { return m_CaptureActive; }
			set { m_CaptureActive = value; }
		}
	
		public Color OriginalColor
		{
			set
			{
				colorMatcher1.OriginalColor = value;
			}
			get
			{
				return colorMatcher1.OriginalColor;
			}
		}

		public Color RGB
		{
			get 
			{
				return m_RGB;
			}
			set 
			{
				//this.colorBox2.BackColor = value;
				colorMatcher1.ActiveColor = value; 
				m_RGB = value; 
			}
		}

		public bool ShowFollow
		{
			get { return m_ShowFollow; }
			set
			{ 
				m_ShowFollow = value;

                if (this.Visible)
                {
                    if (m_Follow != null)
                    {
                        if (m_ShowFollow)
                            m_Follow.Show();
                        else
                            m_Follow.Hide();
                    }
                }
                else if (m_Follow != null)
                    m_Follow.Hide();
			}
		}

		public new bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;

                if (this.Visible || !value)
				    this.timer1.Enabled = value;
			}
		}

		#endregion 

		#region Starting / Stopping 

		public void Start()
		{
			this.timer1.Start();

			m_Follow = new CursorFollow();
			m_Follow.Click += new EventHandler(m_Follow_Click);
			m_Follow.MouseWheel += new MouseEventHandler(m_Follow_MouseWheel);

			if (m_ShowFollow)
				m_Follow.Show();
			else
				m_Follow.Hide();
		}

		public void Stop()
		{
            this.timer1.Enabled = false; 

			this.timer1.Stop();
			if (m_Follow != null) 
			{
				m_Follow.Dispose();
				m_Follow = null;
			}
		}

		#endregion 

		#region Capture Timer Tick

		private void timer1_Tick(object sender, EventArgs e)
		{
			if (CaptureActive)
			{
				// m_Follow.Show();
				bool inside = this.ParentForm.Bounds.Contains(Cursor.Position);

				if (m_ShowFollow && inside)
					ShowFollow = false;
				else if (!m_ShowFollow && !inside)
					ShowFollow = true;

				if (m_ShowFollow)
					if (m_Follow != null)
						m_Follow.Location = new Point(Cursor.Position.X, Cursor.Position.Y);

				GrabImage();
			}
		}

		#endregion 

		#region Color Sampleing

		private void CaptureColor()
		{						
			GrabImage();

			if (m_LastCapture != null)
				m_LastCapture.Dispose();
			
			m_LastCapture = m_Capture;

			this.panel1.Image = m_LastCapture;

			m_Capture = null;

			Point off = new Point(m_LastCapture.Width / 2, m_LastCapture.Height / 2);

			if (numericUpDown1.Value == 1)
				m_RGB = m_LastCapture.GetPixel(off.X, off.Y);
			else
			{
				// this is not fast 

				int a = (int)numericUpDown1.Value;
				
				List<Color> colors = new List<Color>();

				for (int y = off.Y - (a / 2), ey = off.Y + (a); y < ey; y++)
				{
					for (int x = off.X - (a / 2), ex = off.X + (a); x < ex; x++)
					{
						colors.Add(m_LastCapture.GetPixel(x, y));
					}
				}
				
				m_RGB = BlendColors(colors); 
			}

			colorMatcher1.ActiveColor = m_RGB; 
		}

		/// <summary>
		/// Calculate the average color of a list of colors 
		/// </summary>
		/// <param name="colors"></param>
		/// <returns></returns>
		private Color BlendColors(List<Color> colors)
		{			
			double r = 0, g = 0, b = 0;

			foreach (Color col in colors)
			{
				r += col.R;
				g += col.G;
				b += col.B;
			}

			r /= colors.Count;
			g /= colors.Count;
			b /= colors.Count;

			return Color.FromArgb(
				(int)r > 255 ? 255 : (int)r < 0 ? 0 : (int)r,
				(int)g > 255 ? 255 : (int)g < 0 ? 0 : (int)g,
				(int)b > 255 ? 255 : (int)b < 0 ? 0 : (int)b);
		}

		#endregion 

		/// <summary>
		/// Preform the actual screen grab 
		/// </summary>
		private void GrabImage()
		{
			#region Create / Check Image Buffers 
			
			// is the capture buffer invalid? 
			if ((m_Capture != null) &&
				((this.panel1.ClientSize.Width / (int)this.numericUpDown2.Value != m_Capture.Width) ||
				(this.panel1.ClientSize.Height / (int)this.numericUpDown2.Value != m_Capture.Height)))
			{
				// dispose of it 
				m_Capture.Dispose();
				m_Capture = null; 
			}

			// create a new capture buffer if needed 
			if (m_Capture == null)
				m_Capture = new Bitmap(this.panel1.ClientSize.Width / (int)this.numericUpDown2.Value,
										this.panel1.ClientSize.Height / (int)this.numericUpDown2.Value, PixelFormat.Format32bppArgb);
			
			#endregion

			#region Floating stealth window

			// if the floating (the mouse is out of the colour picker bounds) 
			if (m_ShowFollow)
			{
				// if the floating form is active 
				if (m_Follow != null)
				{
					// make the floating form transparent without explicitly hiding it 
					// this avoids ugly changes of focus 
					m_Follow.TransparencyKey = Color.White;
					m_Follow.BackColor = Color.White; 

					// get the color for the stelth pixel 					
					m_StelthGraphics.CopyFromScreen(new Point(Cursor.Position.X + 1, Cursor.Position.Y), new Point(0, 0), m_Stealth.Size);

					// Hide the window by making it exactly the same color as the pixel 
					// under it. 
					// This means that it is both invisible and able to recive the mouse 
					// events we want 
					m_Follow.TransparencyKey = Color.Transparent; 
					m_Follow.BackColor = m_Stealth.GetPixel(0, 0);
				}
			}
			
			#endregion
			
			#region Grab the magnifier image

			Graphics g = Graphics.FromImage(m_Capture);

			// fill in the checkered background 
			g.FillRectangle(m_Brush,0, 0, m_Capture.Width, m_Capture.Height);					

			// offset the grab so that the mouse is at the center of the image 
			Point off = new Point(m_Capture.Width / 2, m_Capture.Height / 2);
			
			// calculate the new position to capture at 
			Point pt = new Point(Cursor.Position.X - off.X, Cursor.Position.Y - off.Y);

			// grab the image
			g.CopyFromScreen(pt, new Point(0, 0), m_Capture.Size);
			g.Dispose();

			// set the magnifier image
			this.panel2.Image = m_Capture; 

			#endregion 
		}

		#region Internal Events

		#region Floating form 
		/// <summary>
		/// click event from the floating form 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void m_Follow_Click(object sender, EventArgs e)
		{
			CaptureColor();
		}

		/// <summary>
		/// Adjust the zoom 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void m_Follow_MouseWheel(object sender, MouseEventArgs e)
		{
			if (e.Delta > 0)
			{
				if (this.numericUpDown2.Value + 1 < this.numericUpDown2.Maximum)
					this.numericUpDown2.Value += 1;
				else
					this.numericUpDown2.Value = this.numericUpDown2.Maximum;
			}
			else if (e.Delta < 0)
			{
				if (this.numericUpDown2.Value - 1 > this.numericUpDown2.Minimum)
					this.numericUpDown2.Value -= 1;
				else
					this.numericUpDown2.Value = this.numericUpDown2.Minimum;
			}

		}
		
		#endregion
		
		/// <summary>
		/// Click from inside this control 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClick(object sender, EventArgs e)
		{
			CaptureColor();
		}

		/// <summary>
		/// Revert to the original color 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void RevertToPrimary(object sender, EventArgs e)
		{
			colorMatcher1.ActiveColor = colorMatcher1.OriginalColor;
			//colorBox1.BackColor = OriginalColor;
			m_RGB = colorMatcher1.OriginalColor;
		}

		/// <summary>
		/// Capture check box checked state changed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCaptureChange(object sender, EventArgs e)
		{
			CaptureActive = this.checkBox1.Checked;

			this.timer1.Enabled = CaptureActive;

			if (!CaptureActive)
				ShowFollow = false; 			
		}

		private void RateChanged(object sender, EventArgs e)
		{			
			this.timer1.Interval = (int)this.numericUpDown3.Value;
		}

		#endregion 

        private void OnVisibleChanged(object sender, EventArgs e)
        {
            ShowFollow = ShowFollow;
            Enabled = Enabled; 
        }
	}
}
