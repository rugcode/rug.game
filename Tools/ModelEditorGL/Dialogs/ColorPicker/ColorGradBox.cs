using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace ModelEditorGL.Dialogs.ColorPicker
{
	internal partial class ColorGradBox : UserControl
	{
		#region Members 

		private Point m_Marker = new Point(0, 0);
		private bool m_Dragging = false;
		private Bitmap m_Gradiant = null;
		private bool m_RedrawGradiant = true; 

		private ColorRange m_DrawStyle = ColorRange.Hue;

		private HSL m_hsl;
		private Color m_rgb;

		#endregion 

		#region Properties

		public ColorRange DrawStyle
		{
			get { return m_DrawStyle; }
			set 
			{
				m_DrawStyle = value;
				m_RedrawGradiant = true;
				ResetMarker();
				Invalidate();
			}
		}

		public HSL HSL
		{
			get { return m_hsl; }
			set 
			{ 
				m_hsl = value;
				m_rgb = value.Color; 
				m_RedrawGradiant = true;
				ResetMarker();
				Invalidate();
			}
		}

		public Color RGB
		{
			get { return m_rgb; }
			set 
			{ 
				m_rgb = value;
				m_hsl = new HSL(value); 
				m_RedrawGradiant = true;
				ResetMarker();
				Invalidate();
			}
		}

		#endregion 

		#region Events

		public new event EventHandler Scroll;

		#endregion

		#region Constructor 

		public ColorGradBox()
		{
			InitializeComponent();
			
			m_hsl = new HSL();
			m_hsl.H = 1.0;
			m_hsl.S = 1.0;
			m_hsl.L = 1.0;
			m_rgb = m_hsl.Color;
			m_DrawStyle = ColorRange.Hue;
		}

		#endregion 

		#region Draw Gradiant 
		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Hue value.
		/// </summary>
		private void Draw_Style_Hue(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			HSL hsl_start = new HSL();
			HSL hsl_end = new HSL();
			hsl_start.H = m_hsl.H;
			hsl_end.H = m_hsl.H;
			hsl_start.S = 0.0;
			hsl_end.S = 1.0;

			for (int i = 0; i < this.Height - 4; i++)				//	For each horizontal line in the control:
			{
				hsl_start.L = 1.0 - (double)i / (this.Height - 4);	//	Calculate luminance at this line (Hue and Saturation are constant)
				hsl_end.L = hsl_start.L;

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), hsl_start.Color, hsl_end.Color, 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose(); 
			}
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Saturation value.
		/// </summary>
		private void Draw_Style_Saturation(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			HSL hsl_start = new HSL();
			HSL hsl_end = new HSL();
			hsl_start.S = m_hsl.S;
			hsl_end.S = m_hsl.S;
			hsl_start.L = 1.0;
			hsl_end.L = 0.0;

			for (int i = 0; i < this.Width - 4; i++)		//	For each vertical line in the control:
			{
				hsl_start.H = (double)i / (this.Width - 4);	//	Calculate Hue at this line (Saturation and Luminance are constant)
				hsl_end.H = hsl_start.H;

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, 1, this.Height - 4), hsl_start.Color, hsl_end.Color, 90, false);
				g.FillRectangle(br, new Rectangle(i + 2, 2, 1, this.Height - 4));
				br.Dispose(); 
			}
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Luminance or Brightness value.
		/// </summary>
		private void Draw_Style_Luminance(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			HSL hsl_start = new HSL();
			HSL hsl_end = new HSL();
			hsl_start.L = m_hsl.L;
			hsl_end.L = m_hsl.L;
			hsl_start.S = 1.0;
			hsl_end.S = 0.0;

			for (int i = 0; i < this.Width - 4; i++)		//	For each vertical line in the control:
			{
				hsl_start.H = (double)i / (this.Width - 4);	//	Calculate Hue at this line (Saturation and Luminance are constant)
				hsl_end.H = hsl_start.H;

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, 1, this.Height - 4), hsl_start.Color, hsl_end.Color, 90, false);
				g.FillRectangle(br, new Rectangle(i + 2, 2, 1, this.Height - 4));
				br.Dispose(); 
			}
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Red value.
		/// </summary>
		private void Draw_Style_Red(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			int red = m_rgb.R;

			for (int i = 0; i < this.Height - 4; i++)				//	For each horizontal line in the control:
			{
				//	Calculate Green at this line (Red and Blue are constant)
				int green = HSL.ClipColor(255 - (255 * (double)i / (this.Height - 4)));

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), Color.FromArgb(red, green, 0), Color.FromArgb(red, green, 255), 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose(); 
			}
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Green value.
		/// </summary>
		private void Draw_Style_Green(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			int green = m_rgb.G;

			for (int i = 0; i < this.Height - 4; i++)	//	For each horizontal line in the control:
			{
				//	Calculate Red at this line (Green and Blue are constant)
				int red = HSL.ClipColor(255 - (255 * (double)i / (this.Height - 4)));

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), Color.FromArgb(red, green, 0), Color.FromArgb(red, green, 255), 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose(); 
			}
		}


		/// <summary>
		/// Draws the content of the control filling in all color values with the provided Blue value.
		/// </summary>
		private void Draw_Style_Blue(Graphics g)
		{
			//Graphics g = this.CreateGraphics();

			int blue = m_rgb.B;

			for (int i = 0; i < this.Height - 4; i++)	//	For each horizontal line in the control:
			{
				//	Calculate Green at this line (Red and Blue are constant)
				int green = HSL.ClipColor(255 - (255 * (double)i / (this.Height - 4)));

				LinearGradientBrush br = new LinearGradientBrush(new Rectangle(2, 2, this.Width - 4, 1), Color.FromArgb(0, green, blue), Color.FromArgb(255, green, blue), 0, false);
				g.FillRectangle(br, new Rectangle(2, i + 2, this.Width - 4, 1));
				br.Dispose(); 
			}
		}

		#endregion 

		#region Render 

		/// <summary>
		/// Calls all the functions neccessary to redraw the entire control.
		/// </summary>
		private void Redraw_Control(Graphics g)
		{		
			// do we need to redraw the gradiant 
			if (m_RedrawGradiant || m_Gradiant == null)
			{
				// is the gradiant image valid? 
				if ((m_Gradiant == null) ||
					(m_Gradiant.Width != this.Width) ||
					(m_Gradiant.Height != this.Height))
				{
					// dispose of it if needed 
					if (m_Gradiant != null) 
					{
						m_Gradiant.Dispose(); 
						m_Gradiant = null; 
					}

					// create the gradiant image 
					m_Gradiant = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb);

				}

				// get a graphics object to draw into the gradiant 
				Graphics g2 = Graphics.FromImage(m_Gradiant);
			
				// fill the gradiant 
				switch (m_DrawStyle)
				{
					case ColorRange.Hue:
						Draw_Style_Hue(g2);
						break;
					case ColorRange.Saturation:
						Draw_Style_Saturation(g2);
						break;
					case ColorRange.Luminance:
						Draw_Style_Luminance(g2);
						break;
					case ColorRange.Red:
						Draw_Style_Red(g2);
						break;
					case ColorRange.Green:
						Draw_Style_Green(g2);
						break;
					case ColorRange.Blue:
						Draw_Style_Blue(g2);
						break;
				}

				// draw the border 
				DrawBorder(g2);				

				// we do not need to redraw it
				m_RedrawGradiant = false; 

				// dispose of the temp graphics 
				g2.Dispose(); 
			}

			// draw the gradiant to the control 
			g.DrawImage(m_Gradiant, new Point(0, 0)); 

			// draw the marker 
			DrawMarker(g, m_Marker.X, m_Marker.Y, true);
		}

		/// <summary>
		/// Draws the marker (circle) inside the box
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="Unconditional"></param>
		private void DrawMarker(Graphics g, int x, int y, bool Unconditional)	
		{
			// clip the x and y 
			x = x < 0 ? 0 : x > this.Width - 4 ? this.Width - 4 : x;
			y = y < 0 ? 0 : y > this.Height - 4 ? this.Height - 4 : y;

			// set the clip region to within the border 
			g.SetClip(new Rectangle(2, 2, this.Width - 4, this.Height - 4)); 			

			//	Get the color for the current marker location 
			HSL _hsl = GetColor(x, y);

			Pen pen;

			// make sure the marker will stand out 
			if (_hsl.L > 0.5)
				pen = ResourceManager.Pens[Color.Black, 1f];
			else
				pen = ResourceManager.Pens[Color.White, 1f];

			//	Draw the marker : 11 x 11 circle
			g.DrawEllipse(pen, x - 3, y - 3, 10, 10);						

			// reset the clipping area 
			g.SetClip(this.ClientRectangle); 			
		}


		/// <summary>
		/// Draws the border around the control.
		/// </summary>
		private void DrawBorder(Graphics g)
		{		
			Pen pencil;

			// pencil = ResourceManager.Pens[SystemColors.Control, 1f];
			pencil = ResourceManager.Pens[SystemColors.ControlDark, 1f];
			//	Draw top line
			g.DrawLine(pencil, this.Width - 2, 0, 0, 0);
			//	Draw left hand line
			g.DrawLine(pencil, 0, 0, 0, this.Height - 2);

			pencil = ResourceManager.Pens[SystemColors.ControlLightLight, 1f];
			//	Draw right hand line
			g.DrawLine(pencil, this.Width - 1, 0, this.Width - 1, this.Height - 1);
			//	Draw bottome line
			g.DrawLine(pencil, this.Width - 1, this.Height - 1, 0, this.Height - 1);

			//	Draw inner black rectangle
			// pencil = ResourceManager.Pens[SystemColors.ControlDarkDark, 1f];
			pencil = ResourceManager.Pens[Color.Black, 1f];
			g.DrawRectangle(pencil, 1, 1, this.Width - 3, this.Height - 3);
		}

		#endregion 

		#region  Marker to color / Color to Marker
		/// <summary>
		/// Returns the graphed color at the x,y position on the control
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		private HSL GetColor(int x, int y)
		{

			HSL _hsl = new HSL();

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					_hsl.H = m_hsl.H;
					_hsl.S = (double)x / (this.Width - 4);
					_hsl.L = 1.0 - (double)y / (this.Height - 4);
					break;
				case ColorRange.Saturation:
					_hsl.S = m_hsl.S;
					_hsl.H = (double)x / (this.Width - 4);
					_hsl.L = 1.0 - (double)y / (this.Height - 4);
					break;
				case ColorRange.Luminance:
					_hsl.L = m_hsl.L;
					_hsl.H = (double)x / (this.Width - 4);
					_hsl.S = 1.0 - (double)y / (this.Height - 4);
					break;
				case ColorRange.Red:
					_hsl = new HSL(Color.FromArgb(m_rgb.R, HSL.ClipColor(255 * (1.0 - (double)y / (this.Height - 4))), HSL.ClipColor(255 * (double)x / (this.Width - 4))));
					break;
				case ColorRange.Green:
					_hsl = new HSL(Color.FromArgb(HSL.ClipColor(255 * (1.0 - (double)y / (this.Height - 4))), m_rgb.G, HSL.ClipColor(255 * (double)x / (this.Width - 4))));
					break;
				case ColorRange.Blue:
					_hsl = new HSL(Color.FromArgb(HSL.ClipColor(255 * (double)x / (this.Width - 4)), HSL.ClipColor(255 * (1.0 - (double)y / (this.Height - 4))), m_rgb.B));
					break;
			}

			return _hsl;
		}

		/// <summary>
		/// Sets the marker position to the active color
		/// </summary>
		private void ResetMarker()
		{
			double x = 0, y = 0;

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					x = m_hsl.S *(this.Width - 4); 
					y = (1.0 - m_hsl.L) * (this.Height - 4);
					break;
				case ColorRange.Saturation:
					x = (m_hsl.H * (this.Width - 4));
					y = ((1.0 - m_hsl.L) * (this.Height - 4));
					break;
				case ColorRange.Luminance:
					x = (m_hsl.H * (this.Width - 4));
					y = ((1.0 - m_hsl.S) * (this.Height - 4));
					break;
				case ColorRange.Red:
					x = (((double)m_rgb.B / 255) * (this.Width - 4));
					y = ((1.0 - ((double)m_rgb.G / 255)) * (this.Height - 4));
					break;
				case ColorRange.Green:
					x = (((double)m_rgb.B / 255) * (this.Width - 4));
					y = ((1.0 - ((double)m_rgb.R / 255)) * (this.Height - 4));
					break;
				case ColorRange.Blue:
					x = ((double)m_rgb.R / 255) * (this.Width - 4f);
					y = ((1.0 - ((double)m_rgb.G / 255)) * (this.Height - 4));
					break;
			}

			m_Marker.X = (int)(x);
			m_Marker.Y = (int)(y);

		}

		/// <summary>
		/// Resets the controls color (both HSL and RGB variables) based on the current marker position
		/// </summary>
		private void ResetHSLRGB()
		{
			int red, green, blue;

			switch (m_DrawStyle)
			{
				case ColorRange.Hue:
					m_hsl.S = (double)m_Marker.X / (this.Width - 4);
					m_hsl.L = 1.0 - (double)m_Marker.Y / (this.Height - 4);
					m_rgb = m_hsl.Color;
					break;
				case ColorRange.Saturation:
					m_hsl.H = (double)m_Marker.X / (this.Width - 4);
					m_hsl.L = 1.0 - (double)m_Marker.Y / (this.Height - 4);
					m_rgb = m_hsl.Color;
					break;
				case ColorRange.Luminance:
					m_hsl.H = (double)m_Marker.X / (this.Width - 4);
					m_hsl.S = 1.0 - (double)m_Marker.Y / (this.Height - 4);
					m_rgb = m_hsl.Color;
					break;
				case ColorRange.Red:
					blue = HSL.ClipColor(255 * (double)m_Marker.X / (this.Width - 4));
					green = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (this.Height - 4)));
					m_rgb = Color.FromArgb(m_rgb.R, green, blue);
					m_hsl = new HSL(m_rgb);
					break;
				case ColorRange.Green:
					blue = HSL.ClipColor(255 * (double)m_Marker.X / (this.Width - 4));
					red = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (this.Height - 4)));
					m_rgb = Color.FromArgb(red, m_rgb.G, blue);
					m_hsl = new HSL(m_rgb);
					break;
				case ColorRange.Blue:
					red = HSL.ClipColor(255 * (double)m_Marker.X / (this.Width - 4));
					green = HSL.ClipColor(255 * (1.0 - (double)m_Marker.Y / (this.Height - 4)));
					m_rgb = Color.FromArgb(red, green, m_rgb.B);
					m_hsl = new HSL(m_rgb);
					break;
			}
		}

		private void SetMarkerPostion(Point p)
		{
			Point last = m_Marker;
			m_Marker = p;

			ResetHSLRGB();

			if (Scroll != null)	//	Notify anyone who cares that the controls marker (selected color) has changed
				Scroll(this, null);

			if (m_Gradiant != null)
			{
				Graphics g = this.CreateGraphics();

				int x = last.X;
				int y = last.Y;

				if (x < 0)
					x = 0;
				if (x > this.Width - 4)
					x = this.Width - 4;
				if (y < 0)
					y = 0;
				if (y > this.Height - 4)
					y = this.Height - 4;

				Rectangle rect = new Rectangle(x - 4, y - 4, 12, 12);

				g.DrawImage(m_Gradiant, rect, rect, GraphicsUnit.Pixel);

				DrawMarker(g, p.X, p.Y, true);

				g.Dispose();

			}
			else
				Invalidate();
		}

		#endregion 

		#region Internal Events 

		private void OnPaint(object sender, PaintEventArgs e)
		{
			Redraw_Control(e.Graphics);
		}	

		private void onMouseDown(object sender, MouseEventArgs e)
		{
			m_Dragging = true;
			SetMarkerPostion(e.Location);
		}

		private void onMouseMove(object sender, MouseEventArgs e)
		{
			if (m_Dragging)
			{
				SetMarkerPostion(e.Location);				
			}
		}

		private void onMouseUp(object sender, MouseEventArgs e)
		{
			m_Dragging = false;
		}

		#endregion 
	}
}
