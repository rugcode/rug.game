using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;

namespace ModelEditorGL.Dialogs.ColorPicker
{
    internal sealed class ResourceMarkers
    {
        internal List<string> Keys = new List<string>();

        public void AddKey(string key)
        {
            if (!Keys.Contains(key))
                Keys.Add(key);
        }

        public bool Contains(string key)
        {
            return Keys.Contains(key); 
        }
    }

    internal class ResourceManager
	{
        public enum ClenseMode { Full, Panels, Internal, Extras, Minimal }

        public delegate void EnsureSkinElementsEvent();
        public delegate void ClearedEvent();
        public delegate void MarkInUseEvent(ClenseMode mode, ResourceMarkers markers);

        public static event EnsureSkinElementsEvent Loading;
        public static event ClearedEvent Cleared;
        public static event MarkInUseEvent Clensing; 

		public static readonly PenCollection Pens = new PenCollection();

		public static readonly BrushCollection Brushes = new BrushCollection();

		//public static readonly BitmapCollection Images = new BitmapCollection();

		//public static readonly ColorisedBitmapCollection ColorisedImages = new ColorisedBitmapCollection();

        //public static readonly AlphaMappedBitmapCollection AlphaMappedImages = new AlphaMappedBitmapCollection();

		//public static readonly ScaledBitmapCollection ScaledImages = new ScaledBitmapCollection();

        public static bool SuspendClense = false;

        public static void PreLoad()
        {
            if (Loading != null)
            {
                Loading();
            }
        }

        public static void Clense(ClenseMode mode)
        {
            if (SuspendClense)
            {
                return;
            }

            ResourceMarkers markers = new ResourceMarkers();

            if (Clensing != null)
            {
                Clensing(mode, markers);
            }

            Pens.Clense(markers);
            Brushes.Clense(markers);
            //Images.Clense(markers);
            //ColorisedImages.Clense(markers);
            //AlphaMappedImages.Clense(markers);
            //ScaledImages.Clense(markers);
        }

		public static void Clear()
		{
			Pens.Clear();
			Brushes.Clear();
			//Images.Clear();
			//ColorisedImages.Clear();
            //AlphaMappedImages.Clear(); 
			//ScaledImages.Clear();

            if (Cleared != null)
            {
                Cleared();
            }
		}

        public static int Total
        {
            get
            {
                return Pens.Count + Brushes.Count; // + Images.Count + ColorisedImages.Count + AlphaMappedImages.Count + ScaledImages.Count; 
            }
        }

        public static int TotalImages
        {
            get
            {
                return 0; //  Images.Count + ColorisedImages.Count + AlphaMappedImages.Count + ScaledImages.Count;
            }
        }
	}

	#region Resource Collections

	#region ResourceCollection

    internal class ResourceCollection<T> where T : IDisposable
	{
		private Dictionary<string, T> lookup = new Dictionary<string, T>();

        public int Count { get { return lookup.Count; } } 

		protected void InnerAdd(string key, T data)
		{
			if (lookup.ContainsKey(key))
				throw new Exception("Resource with the key '" + key + "' allready present in collection");

			lookup.Add(key, data);
		}

		protected void InnerRemove(string key)
		{
			if (!lookup.ContainsKey(key))
				throw new Exception("Resource with the key '" + key + "' is not present in collection");

			T value = lookup[key];
			lookup.Remove(key);
			value.Dispose();
		}

		protected bool Contains(string key)
		{
			return lookup.ContainsKey(key);
		}

        public string[] AllKeys
        {
            get
            {
                string[] keys = new string[lookup.Count];

                lookup.Keys.CopyTo(keys, 0);

                return keys; 
            }
        }

		protected T this[string key]
		{
			get { return lookup[key]; }
		}

		public void Clear()
		{
			foreach (T t in lookup.Values)
				t.Dispose();

			lookup.Clear();
		}

        internal void Clense(ResourceMarkers markers)
        {
            //List<T> removed = new List<T>(this.lookup.Count); 
            List<string> removed = new List<string>(this.lookup.Count); 

            foreach (string key in lookup.Keys)
            {
                if (!markers.Contains(key))
                    removed.Add(key);
            }

            foreach (string key in removed)
            {
                T t = lookup[key];
                
                lookup.Remove(key);                 

                t.Dispose();
            }
        }
	}

	#endregion

	#region PenCollection

    internal class PenCollection : ResourceCollection<Pen>
	{
		public Pen Add(Color color, float width)
		{
			Pen pen = new Pen(color, width);

			InnerAdd(ToString(color, width), pen);

			return pen;
		}

		public bool Contains(Color color, float width)
		{
			return base.Contains(ToString(color, width));
		}

		public Pen this[Color color, float width]
		{
			get
			{
				string key = ToString(color, width);

				if (base.Contains(key))
					return base[key];
				else
					return Add(color, width);
			}
		}

        public string GetKey(Color color, float width)
        {
            return string.Format("{0},{1}", Rug.Game.Helper.Color.ToARGBHtmlString(color), width.ToString("N2"));
        }

		private string ToString(Color color, float width)
		{
            return GetKey(color, width);
		}

		private string ToString(Pen pen)
		{
            return GetKey(pen.Color, pen.Width);
		}
    }

	#endregion

	#region BrushCollection

    internal class BrushCollection : ResourceCollection<Brush>
	{
		public Brush Add(Color color)
		{
			Brush brush = new SolidBrush(color);

			InnerAdd(ToString(color), brush);

			return brush;
		}

		public bool Contains(Color color)
		{
			return base.Contains(ToString(color));
		}

		public Brush this[Color color]
		{
			get
			{
				string key = ToString(color);

				if (base.Contains(key))
					return base[key];
				else
					return Add(color);
			}
		}

        public string GetKey(Color color)
        {
            return string.Format("{0}", Rug.Game.Helper.Color.ToARGBHtmlString(color));
        }

		private string ToString(Color color)
		{
            return string.Format("{0}", Rug.Game.Helper.Color.ToARGBHtmlString(color));
		}
	}

	#endregion

    /* 
	#region BitmapCollection

	public class BitmapCollection : ResourceCollection<Bitmap>
	{
		public Bitmap Add(string path)
		{
			Bitmap bitmap = null; 

			if ((path.StartsWith("~/")) || (path.StartsWith("~\\")))
				bitmap = LoadEmbededImage(path.Substring(2));
			else
				bitmap = LoadFileSytemImage(path);

			if (bitmap == null)
				throw new Exception("Could not load image with the path '" + path + "'");

			InnerAdd(path, bitmap);

			return bitmap;
		}

		private Bitmap LoadFileSytemImage(string path)
		{
			return (Bitmap)Bitmap.FromFile(path); 
		}

		private Bitmap LoadEmbededImage(string path)
		{
            return ImageHelper.GetImage(path);
		}

		public new bool Contains(string key)
		{
			return base.Contains(key);
		}

        public bool Exists(string path)
        {
            if ((path.StartsWith("~/")) || (path.StartsWith("~\\")))
                return ImageHelper.ImageExists(path.Substring(2));
            else
                return File.Exists(path); 
        }

		public new Bitmap this[string key]
		{
			get
			{
				if (base.Contains(key))
					return base[key];
				else
					return Add(key);
			}
		}

        public string GetKey(string path)
        {
            return path;
        }
	}

	#endregion

	#region BitmapCollection

	public class ColorisedBitmapCollection : ResourceCollection<Bitmap>
	{
		public Bitmap Add(string path, Color color)
		{
			Bitmap bitmap = ResourceManager.Images[path];

			//Bitmap colorised = ImageHelper.Colorise(bitmap, color);
            Bitmap colorised = ImageHelper.GetAlphaMap2(bitmap, color);
            
			string key = ToString(path, color);

			InnerAdd(key, colorised);

			return colorised;
		}

		public bool Contains(string path, Color color)
		{
			string key = ToString(path, color);
			return base.Contains(key);
		}

        public Bitmap Add(string path, Color fore, Color back)
        {
            Bitmap bitmap = ResourceManager.Images[path];

            //Bitmap colorised = ImageHelper.Colorise(bitmap, color);
            Bitmap colorised = ImageHelper.GetAlphaMap3(bitmap, fore, back);

            string key = ToString(path, fore, back);

            InnerAdd(key, colorised);

            return colorised;
        }

        public bool Contains(string path, Color fore, Color back)
        {
            string key = ToString(path, fore, back);
            return base.Contains(key);
        }

		public new bool Contains(string key)
		{
			return base.Contains(key);
		}

		public Bitmap this[string path, Color color]
		{
			get
			{
				string key = ToString(path, color);

				if (base.Contains(key))
					return base[key];
				else
					return Add(path, color);
			}
		}

        public Bitmap this[string path, Color fore, Color back]
        {
            get
            {
                string key = ToString(path, fore, back);

                if (base.Contains(key))
                    return base[key];
                else
                    return Add(path, fore, back);
            }
        }

		private string ToString(string path, Color color)
		{
            return string.Format("C_{0},{1}", path, Rug.Game.Helper.Color.ToARGBHtmlString(color));
		}

        private string ToString(string path, Color fore, Color back)
        {
            return string.Format("C_{0},{1}", path, Rug.Game.Helper.Color.ToARGBHtmlString(fore) + Rug.Game.Helper.Color.ToARGBHtmlString(back));
        }

        public string GetKey(string path, Color color)
        {
            return ToString(path, color); // string.Format("C_{0},{1}", path, Helper.Color.ToARGBHtmlString(color));
        }

        public string GetKey(string path, Color fore, Color back)
        {
            return ToString(path, fore, back); // string.Format("C_{0},{1}", path, Helper.Color.ToARGBHtmlString(color));
        }
	}

	#endregion

    #region BitmapCollection

    public class AlphaMappedBitmapCollection : ResourceCollection<Bitmap>
    {
        public Bitmap Add(string path, Color color)
        {
            Bitmap bitmap = ResourceManager.Images[path];

            //Bitmap colorised = ImageHelper.GetAlphaMap(bitmap, color);
            Bitmap colorised = ImageHelper.GetLumToAlphaMap(bitmap, color);

            string key = ToString(path, color);

            InnerAdd(key, colorised);

            return colorised;
        }

        public bool Contains(string path, Color color)
        {
            string key = ToString(path, color);
            return base.Contains(key);
        }

        public new bool Contains(string key)
        {
            return base.Contains(key);
        }

        public Bitmap this[string path, Color color]
        {
            get
            {
                string key = ToString(path, color);

                if (base.Contains(key))
                    return base[key];
                else
                    return Add(path, color);
            }
        }

        private string ToString(string path, Color color)
        {
            return string.Format("A_{0},{1}", path, Rug.Game.Helper.Color.ToARGBHtmlString(color));
        }

        public string GetKey(string path, Color color)
        {
            return string.Format("A_{0},{1}", path, Rug.Game.Helper.Color.ToARGBHtmlString(color));
        }
    }

    #endregion

	#region BitmapCollection

	public class ScaledBitmapCollection : ResourceCollection<Bitmap>
	{
		public Bitmap Add(int width, int height, string path)
		{
			Bitmap bitmap = ResourceManager.Images[path];

			bitmap = ImageHelper.ScaleBitmap(bitmap, width, height);

			string key = ToString(width, height, path);

			InnerAdd(key, bitmap);

			return bitmap;
		}

		public Bitmap Add(int width, int height, string path, Color color)
		{
			Bitmap bitmap = ResourceManager.ColorisedImages[path, color];

			bitmap = ImageHelper.ScaleBitmap(bitmap, width, height);

			string key = ToString(width, height, path, color);

			InnerAdd(key, bitmap);

			return bitmap;
		}

		public bool Contains(int width, int height, string path, Color color)
		{
			string key = ToString(width, height, path, color);
			return base.Contains(key);
		}

		public bool Contains(int width, int height, string path)
		{
			string key = ToString(width, height, path);
			return base.Contains(key);
		}

		public new bool Contains(string key)
		{
			return base.Contains(key);
		}

		public Bitmap this[int width, int height, string path]
		{
			get
			{
				string key = ToString(width, height, path);

				if (base.Contains(key))
					return base[key];
				else
					return Add(width, height, path);
			}
		}

		public Bitmap this[int width, int height, string path, Color color]
		{
			get
			{
				string key = ToString(width, height, path, color);

				if (base.Contains(key))
					return base[key];
				else
					return Add(width, height, path, color);
			}
		}

		private string ToString(int width, int height, string path, Color color)
		{
			return string.Format("S_{0},{1},{2},{3}", width, height, path, Rug.Game.Helper.Color.ToARGBHtmlString(color));
		}


		private string ToString(int width, int height, string path)
		{
            return string.Format("S_{0},{1},{2}", width, height, path);
		}

        public string GetKey(int width, int height, string path, Color color)
        {
            return ToString(width, height, path, color);
        }

        public string GetKey(int width, int height, string path)
        {
            return ToString(width, height, path);
        }
    }

	#endregion
    */ 

	#endregion 
}
