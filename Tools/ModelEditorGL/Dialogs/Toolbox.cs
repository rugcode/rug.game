﻿using ModelEditorGL.Editor;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ModelEditorGL.Dialogs
{
    internal partial class Toolbox : Form
    {
        private EditorToolMode m_ToolMode;
        private ModelSelectionMode m_SelectionMode;

        private Bitmap[,] m_Icons = new Bitmap[10, 2]; 

        public event EventHandler SelectedToolChanged;

        public event EventHandler SelectedSelectionModeChanged;

        public event EventHandler SelectedColorChanged;

        [Browsable(false)]
        public Color SelectedColor
        {
            get { return activeColorSwitcher1.SelectedColor; }
            set { activeColorSwitcher1.SelectedColor = value; } 
        }

        [Browsable(false)]
        public ModelSelectionMode SelectionMode 
        {
            get { return m_SelectionMode; }
            set
            {
                m_SelectionMode = value;

                SetSelectedSelectionMode(); 
            }
        }

        [Browsable(false)]
        public EditorToolMode ToolMode
        {
            get { return m_ToolMode; }
            set
            {
                m_ToolMode = value;

                SetSelectedTool();
            }
        }

        public Toolbox()
        {
            InitializeComponent();
        }

        private void SetButtonStyle(Button button, int iconIndex, bool selected)
        {
            button.Text = ""; 
            button.Image = m_Icons[iconIndex, selected ? 0 : 1]; 
            button.BackColor = selected ? SystemColors.ButtonHighlight : SystemColors.ControlDark;
            button.ForeColor = selected ? SystemColors.ControlText : SystemColors.ButtonHighlight;

            button.FlatAppearance.MouseDownBackColor = selected ? SystemColors.ControlLightLight : SystemColors.ControlDarkDark;
            button.FlatAppearance.MouseOverBackColor = selected ? SystemColors.ControlLightLight : SystemColors.ControlDarkDark;
        }

        private void m_SelectButton_Click(object sender, EventArgs e)
        {
            ToolMode = EditorToolMode.Select;

            OnSelectedToolChanged();
        }

        private void m_MoveButton_Click(object sender, EventArgs e)
        {
            ToolMode = EditorToolMode.Move;

            OnSelectedToolChanged();
        }

        private void m_PaintButton_Click(object sender, EventArgs e)
        {
            ToolMode = EditorToolMode.Paint;
            
            OnSelectedToolChanged();
        }

        private void m_ScaleButton_Click(object sender, EventArgs e)
        {
            ToolMode = EditorToolMode.Scale;

            OnSelectedToolChanged();
        }

        private void m_RotateButton_Click(object sender, EventArgs e)
        {
            ToolMode = EditorToolMode.Rotate;

            OnSelectedToolChanged();
        }
        
        private void m_PipetteButton_Click(object sender, EventArgs e)
        {
            ToolMode = EditorToolMode.Pipette;

            OnSelectedToolChanged();
        }

        private void OnSelectedToolChanged()
        {
            if (SelectedToolChanged != null)
            {
                SelectedToolChanged(this, EventArgs.Empty); 
            }
        }

        private void SetSelectedTool()
        {
            //LoadIcons("Select", 4, SystemColors.ControlText, SystemColors.ButtonHighlight);
            //LoadIcons("Move", 5, SystemColors.ControlText, SystemColors.ButtonHighlight);
            //LoadIcons("Paint", 6, SystemColors.ControlText, SystemColors.ButtonHighlight);
            //LoadIcons("Scale", 7, SystemColors.ControlText, SystemColors.ButtonHighlight); 

            SetButtonStyle(m_SelectButton, 4, ToolMode == EditorToolMode.Select);
            SetButtonStyle(m_MoveButton, 5, ToolMode == EditorToolMode.Move);
            SetButtonStyle(m_PaintButton, 6, ToolMode == EditorToolMode.Paint);
            SetButtonStyle(m_ScaleButton, 7, ToolMode == EditorToolMode.Scale);
            SetButtonStyle(m_RotateButton, 8, ToolMode == EditorToolMode.Rotate);
            SetButtonStyle(m_PipetteButton, 9, ToolMode == EditorToolMode.Pipette);
        }

        private void m_Vertices_Click(object sender, EventArgs e)
        {
            SelectionMode = ModelSelectionMode.Vertices;

            OnSelectedSelectionModeChanged();
        }

        private void m_Edges_Click(object sender, EventArgs e)
        {
            SelectionMode = ModelSelectionMode.Edges;

            OnSelectedSelectionModeChanged();
        }

        private void m_Faces_Click(object sender, EventArgs e)
        {
            SelectionMode = ModelSelectionMode.Faces;

            OnSelectedSelectionModeChanged();
        }

        private void m_Objects_Click(object sender, EventArgs e)
        {
            SelectionMode = ModelSelectionMode.Objects;

            OnSelectedSelectionModeChanged();
        }

        private void OnSelectedSelectionModeChanged()
        {
            if (SelectedSelectionModeChanged != null)
            {
                SelectedSelectionModeChanged(this, EventArgs.Empty);
            }
        }

        private void SetSelectedSelectionMode()
        {
            /* 
            LoadIcons("Vert", 0, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Edge", 1, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Face", 2, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Object", 3, SystemColors.ControlText, SystemColors.ButtonHighlight);
            */ 
            SetButtonStyle(m_Vertices, 0, SelectionMode == ModelSelectionMode.Vertices);
            SetButtonStyle(m_Edges, 1, SelectionMode == ModelSelectionMode.Edges);
            SetButtonStyle(m_Faces, 2, SelectionMode == ModelSelectionMode.Faces);
            SetButtonStyle(m_Objects, 3, SelectionMode == ModelSelectionMode.Objects);
        }

        private void activeColorSwitcher1_SelectedColorChanged(object sender, EventArgs e)
        {
            if (SelectedColorChanged != null)
            {
                SelectedColorChanged(this, EventArgs.Empty); 
            }
        }

        private void Toolbox_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.None || e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;

                Hide();
            }
        }

        private void Toolbox_Load(object sender, EventArgs e)
        {
            if (Preferences.Windows["Toolbox"].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Preferences.Windows["Toolbox"].Bounds;
            }

            LoadIcons("Vert", 0, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Edge", 1, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Face", 2, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Object", 3, SystemColors.ControlText, SystemColors.ButtonHighlight);

            LoadIcons("Select", 4, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Move", 5, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Paint", 6, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Scale", 7, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Rotate", 8, SystemColors.ControlText, SystemColors.ButtonHighlight);
            LoadIcons("Pipette", 9, SystemColors.ControlText, SystemColors.ButtonHighlight);   

            SetSelectedTool();
            SetSelectedSelectionMode(); 
        }

        private void LoadIcons(string name, int index, Color color1, Color color2)
        {
            using (Bitmap icon = (Bitmap)ModelEditorGL.Properties.Resources.ResourceManager.GetObject(name))
            {
                m_Icons[index, 0] = ColoriseIcon(icon, color1);
                m_Icons[index, 1] = ColoriseIcon(icon, color2); 
            }
        }

        private Bitmap ColoriseIcon(Bitmap icon, Color color1)
        {
            Bitmap newIcon = new Bitmap(icon.Width, icon.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (Graphics g = Graphics.FromImage(newIcon))
            {
                g.Clear(Color.Transparent);

                Color4 colorFloat = color1; 

                // Initialize the color matrix. 
                // Note the value 0.8 in row 4, column 4. 
                float[][] matrixItems = { 
                   new float[] {colorFloat.R, 0, 0, 0, 0},
                   new float[] {0, colorFloat.G, 0, 0, 0},
                   new float[] {0, 0, colorFloat.B, 0, 0},
                   new float[] {0, 0, 0, 1f, 0}, 
                   new float[] {0, 0, 0, 0, 1f}};
                ColorMatrix colorMatrix = new ColorMatrix(matrixItems);

                // Create an ImageAttributes object and set its color matrix.
                ImageAttributes imageAtt = new ImageAttributes();
                imageAtt.SetColorMatrix(
                   colorMatrix,
                   ColorMatrixFlag.Default,
                   ColorAdjustType.Bitmap);

                // Now draw the semitransparent bitmap image. 
                int iWidth = newIcon.Width;
                int iHeight = newIcon.Height;
                g.DrawImage(
                   icon,
                   new Rectangle(-1, -1, iWidth, iHeight),  // destination rectangle
                   0,                          // source rectangle x 
                   0,                          // source rectangle y
                   iWidth,                        // source rectangle width
                   iHeight,                       // source rectangle height
                   GraphicsUnit.Pixel,
                   imageAtt);
            }

            return newIcon; 
        }

        #region Window Resize / Move Events

        private void Form_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Preferences.Windows["Toolbox"].Bounds = this.DesktopBounds;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                //Preferences.Windows["Logger"].WindowState = WindowState;
            }
        }

        #endregion    
    }
}
