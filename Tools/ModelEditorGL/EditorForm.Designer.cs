﻿
namespace ModelEditorGL
{
    partial class EditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importOBJToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportOBJToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.addFacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flipFacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.isoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.topToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bottomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.leftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.frontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.solidToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wireframeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decompositionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.objectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.mergeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seperateSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.addSphereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sphereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capsuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cylinderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.boxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lightingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strongWhiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overUnderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allAroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subdivideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertNormalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupByNormalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_GroupByNormalFaceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeCircularToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centeriseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.createHullFromFacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decomposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.generateMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reSeedGenerationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weldVerticiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolboxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.objectsViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.m_SelectionType = new System.Windows.Forms.ToolStripComboBox();
            this.m_ToolSelection = new System.Windows.Forms.ToolStripComboBox();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.scale2xToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scale05xToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 570);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(769, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(26, 17);
            this.toolStripStatusLabel1.Text = "Idle";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolStripMenuItem2,
            this.lightingToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.windowsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(769, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.importOBJToolStripMenuItem,
            this.exportOBJToolStripMenuItem,
            this.toolStripSeparator5,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator6,
            this.closeToolStripMenuItem,
            this.toolStripSeparator7,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // importOBJToolStripMenuItem
            // 
            this.importOBJToolStripMenuItem.Name = "importOBJToolStripMenuItem";
            this.importOBJToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.importOBJToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.importOBJToolStripMenuItem.Text = "Import OBJ";
            this.importOBJToolStripMenuItem.Click += new System.EventHandler(this.importOBJToolStripMenuItem_Click);
            // 
            // exportOBJToolStripMenuItem
            // 
            this.exportOBJToolStripMenuItem.Name = "exportOBJToolStripMenuItem";
            this.exportOBJToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.exportOBJToolStripMenuItem.Text = "Export OBJ";
            this.exportOBJToolStripMenuItem.Click += new System.EventHandler(this.exportOBJToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(181, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(181, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(181, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator8,
            this.selectAllToolStripMenuItem,
            this.invertSelectionToolStripMenuItem,
            this.toolStripSeparator14,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator9,
            this.addFacesToolStripMenuItem,
            this.flipFacesToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(221, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // invertSelectionToolStripMenuItem
            // 
            this.invertSelectionToolStripMenuItem.Name = "invertSelectionToolStripMenuItem";
            this.invertSelectionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.I)));
            this.invertSelectionToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.invertSelectionToolStripMenuItem.Text = "Invert Selection";
            this.invertSelectionToolStripMenuItem.Click += new System.EventHandler(this.invertSelectionToolStripMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(221, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(221, 6);
            // 
            // addFacesToolStripMenuItem
            // 
            this.addFacesToolStripMenuItem.Name = "addFacesToolStripMenuItem";
            this.addFacesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Space)));
            this.addFacesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.addFacesToolStripMenuItem.Text = "Add Faces";
            this.addFacesToolStripMenuItem.Click += new System.EventHandler(this.addFacesToolStripMenuItem_Click);
            // 
            // flipFacesToolStripMenuItem
            // 
            this.flipFacesToolStripMenuItem.Name = "flipFacesToolStripMenuItem";
            this.flipFacesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.flipFacesToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.flipFacesToolStripMenuItem.Text = "Flip Faces";
            this.flipFacesToolStripMenuItem.Click += new System.EventHandler(this.flipFacesToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.defaultToolStripMenuItem,
            this.isoToolStripMenuItem,
            this.toolStripSeparator1,
            this.topToolStripMenuItem,
            this.bottomToolStripMenuItem,
            this.toolStripSeparator2,
            this.leftToolStripMenuItem,
            this.rightToolStripMenuItem,
            this.toolStripSeparator3,
            this.frontToolStripMenuItem,
            this.backToolStripMenuItem,
            this.toolStripSeparator4,
            this.solidToolStripMenuItem,
            this.verticesToolStripMenuItem,
            this.wireframeToolStripMenuItem,
            this.facesToolStripMenuItem,
            this.normalsToolStripMenuItem,
            this.hullToolStripMenuItem,
            this.extrasToolStripMenuItem,
            this.decompositionToolStripMenuItem,
            this.objectsToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // defaultToolStripMenuItem
            // 
            this.defaultToolStripMenuItem.Name = "defaultToolStripMenuItem";
            this.defaultToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.defaultToolStripMenuItem.Text = "Default";
            this.defaultToolStripMenuItem.Click += new System.EventHandler(this.defaultToolStripMenuItem_Click);
            // 
            // isoToolStripMenuItem
            // 
            this.isoToolStripMenuItem.Name = "isoToolStripMenuItem";
            this.isoToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.isoToolStripMenuItem.Text = "Iso";
            this.isoToolStripMenuItem.Click += new System.EventHandler(this.isoToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // topToolStripMenuItem
            // 
            this.topToolStripMenuItem.Name = "topToolStripMenuItem";
            this.topToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.topToolStripMenuItem.Text = "Top";
            this.topToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // bottomToolStripMenuItem
            // 
            this.bottomToolStripMenuItem.Name = "bottomToolStripMenuItem";
            this.bottomToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.bottomToolStripMenuItem.Text = "Bottom";
            this.bottomToolStripMenuItem.Click += new System.EventHandler(this.bottomToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(152, 6);
            // 
            // leftToolStripMenuItem
            // 
            this.leftToolStripMenuItem.Name = "leftToolStripMenuItem";
            this.leftToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.leftToolStripMenuItem.Text = "Left";
            this.leftToolStripMenuItem.Click += new System.EventHandler(this.leftToolStripMenuItem_Click);
            // 
            // rightToolStripMenuItem
            // 
            this.rightToolStripMenuItem.Name = "rightToolStripMenuItem";
            this.rightToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.rightToolStripMenuItem.Text = "Right";
            this.rightToolStripMenuItem.Click += new System.EventHandler(this.rightToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(152, 6);
            // 
            // frontToolStripMenuItem
            // 
            this.frontToolStripMenuItem.Name = "frontToolStripMenuItem";
            this.frontToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.frontToolStripMenuItem.Text = "Front";
            this.frontToolStripMenuItem.Click += new System.EventHandler(this.frontToolStripMenuItem_Click);
            // 
            // backToolStripMenuItem
            // 
            this.backToolStripMenuItem.Name = "backToolStripMenuItem";
            this.backToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.backToolStripMenuItem.Text = "Back";
            this.backToolStripMenuItem.Click += new System.EventHandler(this.backToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(152, 6);
            // 
            // solidToolStripMenuItem
            // 
            this.solidToolStripMenuItem.CheckOnClick = true;
            this.solidToolStripMenuItem.Name = "solidToolStripMenuItem";
            this.solidToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.solidToolStripMenuItem.Text = "Solid";
            this.solidToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // verticesToolStripMenuItem
            // 
            this.verticesToolStripMenuItem.CheckOnClick = true;
            this.verticesToolStripMenuItem.Name = "verticesToolStripMenuItem";
            this.verticesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.verticesToolStripMenuItem.Text = "Vertices";
            this.verticesToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // wireframeToolStripMenuItem
            // 
            this.wireframeToolStripMenuItem.CheckOnClick = true;
            this.wireframeToolStripMenuItem.Name = "wireframeToolStripMenuItem";
            this.wireframeToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.wireframeToolStripMenuItem.Text = "Wireframe";
            this.wireframeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // facesToolStripMenuItem
            // 
            this.facesToolStripMenuItem.CheckOnClick = true;
            this.facesToolStripMenuItem.Name = "facesToolStripMenuItem";
            this.facesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.facesToolStripMenuItem.Text = "Faces";
            this.facesToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // normalsToolStripMenuItem
            // 
            this.normalsToolStripMenuItem.CheckOnClick = true;
            this.normalsToolStripMenuItem.Name = "normalsToolStripMenuItem";
            this.normalsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.normalsToolStripMenuItem.Text = "Normals";
            this.normalsToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // hullToolStripMenuItem
            // 
            this.hullToolStripMenuItem.CheckOnClick = true;
            this.hullToolStripMenuItem.Name = "hullToolStripMenuItem";
            this.hullToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.hullToolStripMenuItem.Text = "Hull";
            this.hullToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // extrasToolStripMenuItem
            // 
            this.extrasToolStripMenuItem.CheckOnClick = true;
            this.extrasToolStripMenuItem.Name = "extrasToolStripMenuItem";
            this.extrasToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.extrasToolStripMenuItem.Text = "Extras";
            this.extrasToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // decompositionToolStripMenuItem
            // 
            this.decompositionToolStripMenuItem.CheckOnClick = true;
            this.decompositionToolStripMenuItem.Name = "decompositionToolStripMenuItem";
            this.decompositionToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.decompositionToolStripMenuItem.Text = "Decomposition";
            this.decompositionToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // objectsToolStripMenuItem
            // 
            this.objectsToolStripMenuItem.CheckOnClick = true;
            this.objectsToolStripMenuItem.Name = "objectsToolStripMenuItem";
            this.objectsToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.objectsToolStripMenuItem.Text = "Objects";
            this.objectsToolStripMenuItem.CheckedChanged += new System.EventHandler(this.VisibleItems_Checked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.toolStripSeparator10,
            this.mergeToolStripMenuItem,
            this.seperateSelectedToolStripMenuItem,
            this.toolStripSeparator11,
            this.addSphereToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(53, 20);
            this.toolStripMenuItem2.Text = "Model";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.addToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(171, 6);
            // 
            // mergeToolStripMenuItem
            // 
            this.mergeToolStripMenuItem.Name = "mergeToolStripMenuItem";
            this.mergeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.mergeToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.mergeToolStripMenuItem.Text = "Merge";
            this.mergeToolStripMenuItem.Click += new System.EventHandler(this.mergeToolStripMenuItem_Click);
            // 
            // seperateSelectedToolStripMenuItem
            // 
            this.seperateSelectedToolStripMenuItem.Name = "seperateSelectedToolStripMenuItem";
            this.seperateSelectedToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.M)));
            this.seperateSelectedToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.seperateSelectedToolStripMenuItem.Text = "Split";
            this.seperateSelectedToolStripMenuItem.Click += new System.EventHandler(this.seperateSelectedToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(171, 6);
            // 
            // addSphereToolStripMenuItem
            // 
            this.addSphereToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sphereToolStripMenuItem,
            this.capsuleToolStripMenuItem,
            this.cylinderToolStripMenuItem,
            this.coneToolStripMenuItem,
            this.boxToolStripMenuItem});
            this.addSphereToolStripMenuItem.Name = "addSphereToolStripMenuItem";
            this.addSphereToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.addSphereToolStripMenuItem.Text = "Add Hull Primitive";
            // 
            // sphereToolStripMenuItem
            // 
            this.sphereToolStripMenuItem.Name = "sphereToolStripMenuItem";
            this.sphereToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.sphereToolStripMenuItem.Text = "Sphere";
            this.sphereToolStripMenuItem.Click += new System.EventHandler(this.sphereToolStripMenuItem_Click);
            // 
            // capsuleToolStripMenuItem
            // 
            this.capsuleToolStripMenuItem.Name = "capsuleToolStripMenuItem";
            this.capsuleToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.capsuleToolStripMenuItem.Text = "Capsule";
            this.capsuleToolStripMenuItem.Click += new System.EventHandler(this.capsuleToolStripMenuItem_Click);
            // 
            // cylinderToolStripMenuItem
            // 
            this.cylinderToolStripMenuItem.Name = "cylinderToolStripMenuItem";
            this.cylinderToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.cylinderToolStripMenuItem.Text = "Cylinder";
            this.cylinderToolStripMenuItem.Click += new System.EventHandler(this.cylinderToolStripMenuItem_Click);
            // 
            // coneToolStripMenuItem
            // 
            this.coneToolStripMenuItem.Name = "coneToolStripMenuItem";
            this.coneToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.coneToolStripMenuItem.Text = "Cone";
            this.coneToolStripMenuItem.Click += new System.EventHandler(this.coneToolStripMenuItem_Click);
            // 
            // boxToolStripMenuItem
            // 
            this.boxToolStripMenuItem.Name = "boxToolStripMenuItem";
            this.boxToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.boxToolStripMenuItem.Text = "Box";
            this.boxToolStripMenuItem.Click += new System.EventHandler(this.boxToolStripMenuItem_Click);
            // 
            // lightingToolStripMenuItem
            // 
            this.lightingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noneToolStripMenuItem,
            this.strongWhiteToolStripMenuItem,
            this.overUnderToolStripMenuItem,
            this.allAroundToolStripMenuItem});
            this.lightingToolStripMenuItem.Name = "lightingToolStripMenuItem";
            this.lightingToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.lightingToolStripMenuItem.Text = "Lighting";
            // 
            // noneToolStripMenuItem
            // 
            this.noneToolStripMenuItem.Name = "noneToolStripMenuItem";
            this.noneToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.noneToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.noneToolStripMenuItem.Text = "None";
            this.noneToolStripMenuItem.Click += new System.EventHandler(this.noneToolStripMenuItem_Click);
            // 
            // strongWhiteToolStripMenuItem
            // 
            this.strongWhiteToolStripMenuItem.Name = "strongWhiteToolStripMenuItem";
            this.strongWhiteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.strongWhiteToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.strongWhiteToolStripMenuItem.Text = "Strong White";
            this.strongWhiteToolStripMenuItem.Click += new System.EventHandler(this.strongWhiteToolStripMenuItem_Click);
            // 
            // overUnderToolStripMenuItem
            // 
            this.overUnderToolStripMenuItem.Name = "overUnderToolStripMenuItem";
            this.overUnderToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.overUnderToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.overUnderToolStripMenuItem.Text = "Over & Under";
            this.overUnderToolStripMenuItem.Click += new System.EventHandler(this.overUnderToolStripMenuItem_Click);
            // 
            // allAroundToolStripMenuItem
            // 
            this.allAroundToolStripMenuItem.Name = "allAroundToolStripMenuItem";
            this.allAroundToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.allAroundToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.allAroundToolStripMenuItem.Text = "All Around";
            this.allAroundToolStripMenuItem.Click += new System.EventHandler(this.allAroundToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subdivideToolStripMenuItem,
            this.invertNormalsToolStripMenuItem,
            this.groupByNormalToolStripMenuItem,
            this.m_GroupByNormalFaceMenuItem,
            this.makeCircularToolStripMenuItem,
            this.centeriseToolStripMenuItem,
            this.toolStripSeparator13,
            this.createHullFromFacesToolStripMenuItem,
            this.decomposeToolStripMenuItem,
            this.toolStripSeparator12,
            this.generateMapToolStripMenuItem,
            this.reSeedGenerationToolStripMenuItem,
            this.weldVerticiesToolStripMenuItem,
            this.toolStripSeparator15,
            this.scale2xToolStripMenuItem,
            this.scale05xToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // subdivideToolStripMenuItem
            // 
            this.subdivideToolStripMenuItem.Name = "subdivideToolStripMenuItem";
            this.subdivideToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.subdivideToolStripMenuItem.Text = "Subdivide";
            this.subdivideToolStripMenuItem.Click += new System.EventHandler(this.subdivideToolStripMenuItem_Click);
            // 
            // invertNormalsToolStripMenuItem
            // 
            this.invertNormalsToolStripMenuItem.Name = "invertNormalsToolStripMenuItem";
            this.invertNormalsToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.invertNormalsToolStripMenuItem.Text = "Invert Normals";
            this.invertNormalsToolStripMenuItem.Click += new System.EventHandler(this.invertNormalsToolStripMenuItem_Click);
            // 
            // groupByNormalToolStripMenuItem
            // 
            this.groupByNormalToolStripMenuItem.Name = "groupByNormalToolStripMenuItem";
            this.groupByNormalToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.groupByNormalToolStripMenuItem.Text = "Group By Normal";
            this.groupByNormalToolStripMenuItem.Click += new System.EventHandler(this.groupByNormalToolStripMenuItem_Click);
            // 
            // m_GroupByNormalFaceMenuItem
            // 
            this.m_GroupByNormalFaceMenuItem.Name = "m_GroupByNormalFaceMenuItem";
            this.m_GroupByNormalFaceMenuItem.Size = new System.Drawing.Size(240, 22);
            this.m_GroupByNormalFaceMenuItem.Text = "Group By Normal (Face Islands)";
            this.m_GroupByNormalFaceMenuItem.Click += new System.EventHandler(this.m_GroupByNormalFaceMenuItem_Click);
            // 
            // makeCircularToolStripMenuItem
            // 
            this.makeCircularToolStripMenuItem.Name = "makeCircularToolStripMenuItem";
            this.makeCircularToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.makeCircularToolStripMenuItem.Text = "Make Circular";
            this.makeCircularToolStripMenuItem.Click += new System.EventHandler(this.makeCircularUnitToolStripMenuItem_Click);
            // 
            // centeriseToolStripMenuItem
            // 
            this.centeriseToolStripMenuItem.Name = "centeriseToolStripMenuItem";
            this.centeriseToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.centeriseToolStripMenuItem.Text = "Centerise";
            this.centeriseToolStripMenuItem.Click += new System.EventHandler(this.centeriseToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(237, 6);
            // 
            // createHullFromFacesToolStripMenuItem
            // 
            this.createHullFromFacesToolStripMenuItem.Name = "createHullFromFacesToolStripMenuItem";
            this.createHullFromFacesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.createHullFromFacesToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.createHullFromFacesToolStripMenuItem.Text = "Create Hull From Faces";
            this.createHullFromFacesToolStripMenuItem.Click += new System.EventHandler(this.createHullFromFacesToolStripMenuItem_Click);
            // 
            // decomposeToolStripMenuItem
            // 
            this.decomposeToolStripMenuItem.Name = "decomposeToolStripMenuItem";
            this.decomposeToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.decomposeToolStripMenuItem.Text = "Decompose";
            this.decomposeToolStripMenuItem.Click += new System.EventHandler(this.decomposeToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(237, 6);
            // 
            // generateMapToolStripMenuItem
            // 
            this.generateMapToolStripMenuItem.Name = "generateMapToolStripMenuItem";
            this.generateMapToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.generateMapToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.generateMapToolStripMenuItem.Text = "Generate Map";
            this.generateMapToolStripMenuItem.Click += new System.EventHandler(this.generateMapToolStripMenuItem_Click);
            // 
            // reSeedGenerationToolStripMenuItem
            // 
            this.reSeedGenerationToolStripMenuItem.Name = "reSeedGenerationToolStripMenuItem";
            this.reSeedGenerationToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.reSeedGenerationToolStripMenuItem.Text = "Re-Seed Generation";
            this.reSeedGenerationToolStripMenuItem.Click += new System.EventHandler(this.reSeedGenerationToolStripMenuItem_Click);
            // 
            // weldVerticiesToolStripMenuItem
            // 
            this.weldVerticiesToolStripMenuItem.Name = "weldVerticiesToolStripMenuItem";
            this.weldVerticiesToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.weldVerticiesToolStripMenuItem.Text = "Weld Verticies";
            this.weldVerticiesToolStripMenuItem.Click += new System.EventHandler(this.weldVerticiesToolStripMenuItem_Click);
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolboxToolStripMenuItem,
            this.paletteToolStripMenuItem,
            this.undoHistoryToolStripMenuItem,
            this.objectsViewToolStripMenuItem});
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowsToolStripMenuItem.Text = "Window";
            // 
            // toolboxToolStripMenuItem
            // 
            this.toolboxToolStripMenuItem.CheckOnClick = true;
            this.toolboxToolStripMenuItem.Name = "toolboxToolStripMenuItem";
            this.toolboxToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.toolboxToolStripMenuItem.Text = "Toolbox";
            // 
            // paletteToolStripMenuItem
            // 
            this.paletteToolStripMenuItem.CheckOnClick = true;
            this.paletteToolStripMenuItem.Name = "paletteToolStripMenuItem";
            this.paletteToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.paletteToolStripMenuItem.Text = "Palette";
            this.paletteToolStripMenuItem.CheckedChanged += new System.EventHandler(this.paletteToolStripMenuItem_CheckedChanged);
            // 
            // undoHistoryToolStripMenuItem
            // 
            this.undoHistoryToolStripMenuItem.CheckOnClick = true;
            this.undoHistoryToolStripMenuItem.Name = "undoHistoryToolStripMenuItem";
            this.undoHistoryToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoHistoryToolStripMenuItem.Text = "Undo History";
            this.undoHistoryToolStripMenuItem.CheckedChanged += new System.EventHandler(this.undoHistoryToolStripMenuItem_CheckedChanged);
            // 
            // objectsViewToolStripMenuItem
            // 
            this.objectsViewToolStripMenuItem.CheckOnClick = true;
            this.objectsViewToolStripMenuItem.Name = "objectsViewToolStripMenuItem";
            this.objectsViewToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.objectsViewToolStripMenuItem.Text = "Objects View";
            this.objectsViewToolStripMenuItem.CheckedChanged += new System.EventHandler(this.objectsViewToolStripMenuItem_CheckedChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Xml files|*.xml|All files|*.*";
            this.saveFileDialog1.Title = "Save model file";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Xml files|*.xml|All files|*.*";
            this.openFileDialog1.Title = "Load model file";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.Filter = "OBJ Files|*.obj|All files|*.*";
            this.openFileDialog2.Title = "Load model file";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Location = new System.Drawing.Point(0, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(769, 515);
            this.panel1.TabIndex = 3;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_SelectionType,
            this.m_ToolSelection});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(769, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // m_SelectionType
            // 
            this.m_SelectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_SelectionType.Name = "m_SelectionType";
            this.m_SelectionType.Size = new System.Drawing.Size(121, 25);
            this.m_SelectionType.SelectedIndexChanged += new System.EventHandler(this.m_SelectionType_SelectedIndexChanged);
            // 
            // m_ToolSelection
            // 
            this.m_ToolSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ToolSelection.Name = "m_ToolSelection";
            this.m_ToolSelection.Size = new System.Drawing.Size(121, 25);
            this.m_ToolSelection.SelectedIndexChanged += new System.EventHandler(this.m_ToolSelection_SelectedIndexChanged);
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.Filter = "Obj files|*.obj|All files|*.*";
            this.saveFileDialog2.Title = "Save Obj file";
            // 
            // scale2xToolStripMenuItem
            // 
            this.scale2xToolStripMenuItem.Name = "scale2xToolStripMenuItem";
            this.scale2xToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.scale2xToolStripMenuItem.Text = "Scale 2x";
            this.scale2xToolStripMenuItem.Click += new System.EventHandler(this.scale2xToolStripMenuItem_Click);
            // 
            // scale05xToolStripMenuItem
            // 
            this.scale05xToolStripMenuItem.Name = "scale05xToolStripMenuItem";
            this.scale05xToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.scale05xToolStripMenuItem.Text = "Scale 0.5x";
            this.scale05xToolStripMenuItem.Click += new System.EventHandler(this.scale05xToolStripMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(237, 6);
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 592);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "EditorForm";
            this.Text = "Model Editor (GL)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditorForm_FormClosing);
            this.ResizeBegin += new System.EventHandler(this.Form_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.Form_ResizeEnd);
            this.Resize += new System.EventHandler(this.Form_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importOBJToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem defaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem isoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem topToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bottomToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem leftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem frontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem verticesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wireframeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hullToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extrasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decompositionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subdivideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertNormalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupByNormalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_GroupByNormalFaceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decomposeToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.ToolStripMenuItem solidToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox m_SelectionType;
        private System.Windows.Forms.ToolStripComboBox m_ToolSelection;
        private System.Windows.Forms.ToolStripMenuItem lightingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem strongWhiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overUnderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allAroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paletteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem objectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolboxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem objectsViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem addFacesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flipFacesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seperateSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem addSphereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sphereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem capsuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cylinderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boxToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem generateMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reSeedGenerationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centeriseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem makeCircularToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportOBJToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.ToolStripMenuItem createHullFromFacesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weldVerticiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem scale2xToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scale05xToolStripMenuItem;
    }
}

