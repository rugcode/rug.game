﻿using AfterLotusPointMap.Map;
using ModelEditorGL.Editor;
using OpenTK;
using Rug.Game;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AfterLotusPointMap;

namespace ModelEditorGL
{
    partial class EditorForm
    {

        void StaticObjects_Invalidated(object sender, EventArgs e)
        {
            m_SelectionType.SelectedIndex = (int)StaticObjects.Data.InputState.SelectionMode;
            // m_Toolbox.SelectionMode = StaticObjects.Data.InputState.SelectionMode; 
            glControl1.Invalidate();
        }

        void Data_EditorObjectsNeedUpdate(object sender, EventArgs e)
        {
            m_ObjectsView.UpdateObjects();
        }

        void Data_EditorControlsNeedUpdate(object sender, EventArgs e)
        {
            m_Toolbox.SelectedColor = StaticObjects.Data.InputState.PaintColor;

            m_Palette.SelectColor(m_Toolbox.SelectedColor);
        }

        void UndoHistory_Changed(object sender, EventArgs e)
        {
            m_UndoHistory.UpdateHistory();
        }

        void m_Toolbox_SelectedSelectionModeChanged(object sender, EventArgs e)
        {
            m_SelectionType.SelectedIndex = (int)m_Toolbox.SelectionMode;
        }

        void m_Toolbox_SelectedToolChanged(object sender, EventArgs e)
        {
            m_ToolSelection.SelectedIndex = (int)m_Toolbox.ToolMode;
        }

        void m_Toolbox_SelectedColorChanged(object sender, EventArgs e)
        {
            StaticObjects.Data.InputState.PaintColor = m_Toolbox.SelectedColor;

            m_Palette.SelectColor(m_Toolbox.SelectedColor);
        }

        void m_Palette_SelectedColorChanged(object sender, EventArgs e)
        {
            m_Toolbox.SelectedColor = m_Palette.SelectedColor;

            StaticObjects.Data.InputState.PaintColor = m_Toolbox.SelectedColor;
        }

        void m_Palette_VisibleChanged(object sender, EventArgs e)
        {
            paletteToolStripMenuItem.Checked = m_Palette.Visible;
        }

        void m_Toolbox_VisibleChanged(object sender, EventArgs e)
        {
            toolboxToolStripMenuItem.Checked = m_Toolbox.Visible;
        }

        void m_ObjectsView_VisibleChanged(object sender, EventArgs e)
        {
            objectsViewToolStripMenuItem.Checked = m_ObjectsView.Visible;
        }

        private void SetDefaults()
        {
            m_ToolSelection.SelectedIndex = 0;
            m_SelectionType.SelectedIndex = 0;

            SetVisibleItems();

            SetLightingChecked();
        }

        #region File Menu

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.New();

            SetDefaults();

            glControl1.Invalidate();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StaticObjects.Data.Open(openFileDialog1.FileName);

                SetDefaults();
            }

            glControl1.Invalidate();
        }

        private void importOBJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StaticObjects.Data.ImportObj(openFileDialog2.FileName);

                SetDefaults();
            }

            glControl1.Invalidate();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (StaticObjects.Data.CanSave == true)
            {
                StaticObjects.Data.Save();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }

            glControl1.Invalidate();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StaticObjects.Data.FilePath) == false)
            {
                string resolvedPath = Helper.ResolvePath(StaticObjects.Data.FilePath);

                FileInfo fileInfo = new FileInfo(resolvedPath);

                saveFileDialog1.InitialDirectory = fileInfo.DirectoryName;
                saveFileDialog1.FileName = fileInfo.Name;
            }

            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StaticObjects.Data.SaveAs(saveFileDialog1.FileName);
            }

            glControl1.Invalidate();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.Close();

            SetDefaults();

            glControl1.Invalidate();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StaticObjects.Data.Add(openFileDialog1.FileName);
            }

            glControl1.Invalidate();
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.RemoveSelectedModels();

            glControl1.Invalidate();
        }

        #endregion

        #region View Modes

        private void defaultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Default;
            glControl1.Invalidate();
        }

        private void isoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Iso;
            glControl1.Invalidate();
        }

        private void topToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Top;
            glControl1.Invalidate();
        }

        private void bottomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Bottom;
            glControl1.Invalidate();
        }

        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Left;
            glControl1.Invalidate();
        }

        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Right;
            glControl1.Invalidate();
        }

        private void frontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Front;
            glControl1.Invalidate();
        }

        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ViewMode = ViewModes.Back;
            glControl1.Invalidate();
        }

        #endregion

        #region Visible Items

        private void VisibleItems_Checked(object sender, EventArgs e)
        {
            SetVisibleItems();
        }

        private void SetVisibleItems()
        {
            VisibleItemFlags flags = VisibleItemFlags.None;

            if (solidToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Solid;
            }

            if (verticesToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Vertices;
            }

            if (wireframeToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.WireFrame;
            }

            if (facesToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Faces;
            }

            if (normalsToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Normals;
            }

            if (hullToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Hull;
            }

            if (extrasToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Extras;
            }

            if (decompositionToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Decomposed;
            }

            if (objectsToolStripMenuItem.Checked == true)
            {
                flags |= VisibleItemFlags.Objects;
            }

            m_Editor.VisibleItems = flags;

            glControl1.Invalidate();
        }

        #endregion

        private void m_SelectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_Toolbox.SelectionMode = (ModelSelectionMode)m_SelectionType.SelectedIndex;
            StaticObjects.Data.SetSelectionMode(m_Toolbox.SelectionMode);

            switch (StaticObjects.Data.InputState.SelectionMode)
            {
                /* 
                case ModelSelectionMode.All:
                    verticesToolStripMenuItem.Checked = true;
                    facesToolStripMenuItem.Checked = !solidToolStripMenuItem.Checked;
                    normalsToolStripMenuItem.Checked = !solidToolStripMenuItem.Checked;

                    //wireframeToolStripMenuItem.Checked = true;
                                        
                    //hullToolStripMenuItem.Checked = true;
                    //extrasToolStripMenuItem.Checked = true;
                    //decompositionToolStripMenuItem.Checked = false;
                    break;
                */
                case ModelSelectionMode.Vertices:
                    verticesToolStripMenuItem.Checked = true;
                    facesToolStripMenuItem.Checked = !solidToolStripMenuItem.Checked;
                    normalsToolStripMenuItem.Checked = !solidToolStripMenuItem.Checked;
                    objectsToolStripMenuItem.Checked = false;

                    //wireframeToolStripMenuItem.Checked = true;
                    break;
                case ModelSelectionMode.Faces:
                    verticesToolStripMenuItem.Checked = false;
                    facesToolStripMenuItem.Checked = !solidToolStripMenuItem.Checked;
                    normalsToolStripMenuItem.Checked = !solidToolStripMenuItem.Checked;
                    objectsToolStripMenuItem.Checked = false;
                    //wireframeToolStripMenuItem.Checked = true;
                    break;
                case ModelSelectionMode.Objects:
                    verticesToolStripMenuItem.Checked = false;
                    facesToolStripMenuItem.Checked = false;
                    normalsToolStripMenuItem.Checked = false;
                    objectsToolStripMenuItem.Checked = true;
                    break;
                default:
                    break;
            }
        }

        private void m_ToolSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_Toolbox.ToolMode = (EditorToolMode)m_ToolSelection.SelectedIndex;
            StaticObjects.Data.InputState.ToolMode = m_Toolbox.ToolMode;
            StaticObjects.Data.UpdateModels();
            glControl1.Invalidate();
        }

        private void noneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ModelLighting = EditorLightingMode.None;

            SetLightingChecked();

            glControl1.Invalidate();
        }

        private void strongWhiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ModelLighting = EditorLightingMode.StrongWhite;

            SetLightingChecked();

            glControl1.Invalidate();
        }

        private void overUnderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ModelLighting = EditorLightingMode.OverAndUnder;

            SetLightingChecked();

            glControl1.Invalidate();
        }

        private void allAroundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_Editor.ModelLighting = EditorLightingMode.AllAround;

            SetLightingChecked();

            glControl1.Invalidate();
        }

        private void SetLightingChecked()
        {
            noneToolStripMenuItem.Checked = m_Editor.ModelLighting == EditorLightingMode.None;
            strongWhiteToolStripMenuItem.Checked = m_Editor.ModelLighting == EditorLightingMode.StrongWhite;
            overUnderToolStripMenuItem.Checked = m_Editor.ModelLighting == EditorLightingMode.OverAndUnder;
            allAroundToolStripMenuItem.Checked = m_Editor.ModelLighting == EditorLightingMode.AllAround;
        }


        private void paletteToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (paletteToolStripMenuItem.Checked == true)
            {
                m_Palette.Show();
            }
            else
            {
                m_Palette.Hide();
            }
        }

        private void toolboxToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (toolboxToolStripMenuItem.Checked == true)
            {
                m_Toolbox.Show();
            }
            else
            {
                m_Toolbox.Hide();
            }
        }

        private void objectsViewToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (objectsViewToolStripMenuItem.Checked == true)
            {
                m_ObjectsView.Show();
            }
            else
            {
                m_ObjectsView.Hide();
            }
        }

        private void undoHistoryToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (undoHistoryToolStripMenuItem.Checked == true)
            {
                m_UndoHistory.Show();
            }
            else
            {
                m_UndoHistory.Hide();
            }
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.UndoHistory.Undo();
            m_SelectionType.SelectedIndex = (int)StaticObjects.Data.InputState.SelectionMode;
            StaticObjects.Data.UpdateModels_Full();
            glControl1.Invalidate();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.UndoHistory.Redo();
            m_SelectionType.SelectedIndex = (int)StaticObjects.Data.InputState.SelectionMode;
            StaticObjects.Data.UpdateModels_Full();
            glControl1.Invalidate();
        }

        void m_UndoHistory_HistorySelected(object sender, EventArgs e)
        {
            m_SelectionType.SelectedIndex = (int)StaticObjects.Data.InputState.SelectionMode;
            StaticObjects.Data.UpdateModels_Full();
            glControl1.Invalidate();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CutSelected();
            glControl1.Invalidate();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CopySelected();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.PasteSelected();
            glControl1.Invalidate();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.DeleteSelected();
            glControl1.Invalidate();
        }

        private void addFacesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.AddFaces(m_Editor.CameraNormal);
            glControl1.Invalidate();
        }

        private void flipFacesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.FlipNormals();
            glControl1.Invalidate();
        }

        private void mergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.MergeSelected();
            glControl1.Invalidate();
        }

        private void seperateSelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.SplitSelected();
            glControl1.Invalidate();
        }

        private void sphereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CurrentModel.ModelData.HullPrimitives.Add(new ModelPrimitive()
            {
                Center = Vector3.Zero,
                Orientation = Quaternion.Identity,
                Scale = new Vector3(0.25f, 0.25f, 0.25f),
                Margin = 0,
                PrimitiveType = Rug.Game.Flat.Models.PrimitiveType.Sphere
            });
        }

        private void capsuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CurrentModel.ModelData.HullPrimitives.Add(new ModelPrimitive()
            {
                Center = Vector3.Zero,
                Orientation = Quaternion.Identity,
                Scale = new Vector3(0.25f, 0.25f, 0.25f),
                Margin = 0,
                PrimitiveType = Rug.Game.Flat.Models.PrimitiveType.Capsule
            });
        }

        private void cylinderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CurrentModel.ModelData.HullPrimitives.Add(new ModelPrimitive()
            {
                Center = Vector3.Zero,
                Orientation = Quaternion.Identity,
                Scale = new Vector3(0.25f, 0.25f, 0.25f),
                Margin = 0,
                PrimitiveType = Rug.Game.Flat.Models.PrimitiveType.Cylinder
            });
        }

        private void coneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CurrentModel.ModelData.HullPrimitives.Add(new ModelPrimitive()
            {
                Center = Vector3.Zero,
                Orientation = Quaternion.Identity,
                Scale = new Vector3(0.25f, 0.25f, 0.25f),
                Margin = 0,
                PrimitiveType = Rug.Game.Flat.Models.PrimitiveType.Cone
            });
        }

        private void boxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.CurrentModel.ModelData.HullPrimitives.Add(new ModelPrimitive()
            {
                Center = Vector3.Zero,
                Orientation = Quaternion.Identity,
                Scale = new Vector3(0.25f, 0.25f, 0.25f),
                Margin = 0,
                PrimitiveType = Rug.Game.Flat.Models.PrimitiveType.Box
            });
        }

        private void subdivideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.Selection.Clear();
            StaticObjects.Data.SelectionCache.Clear();

            StaticObjects.Data.Subdivide();

            glControl1.Invalidate();
        }

        private void invertNormalsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void groupByNormalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void weldVerticiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (EditableModelData model in StaticObjects.Data.Models.Values)
            {
                model.ModelData.WeldVerts(0.0001f);
            }

            StaticObjects.Data.UpdateModels_Full();

            glControl1.Invalidate();
        }

        private void m_GroupByNormalFaceMenuItem_Click(object sender, EventArgs e)
        {
            foreach (EditableModelData model in StaticObjects.Data.Models.Values)
            {
                for (int i = 0; i < model.ModelData.Faces.Count; i++)
                {
                    FaceData data = model.ModelData.Faces[i];

                    data.Group = -1;

                    model.ModelData.Faces[i] = data;
                }

                model.ModelData.CalcuateFaceNormals();

                Dictionary<int, List<int>> joinedGroups = new Dictionary<int, List<int>>();

                int groupID = 0;

                for (int i = 0; i < model.ModelData.Faces.Count; i++)
                {
                    FaceData data = model.ModelData.Faces[i];

                    Vector3 normal = data.Normal;

                    for (int j = i + 1; j < model.ModelData.Faces.Count; j++)
                    {
                        FaceData data2 = model.ModelData.Faces[j];

                        if (data.Contains(data2.V0) == false &&
                            data.Contains(data2.V1) == false &&
                            data.Contains(data2.V2) == false)
                        {
                            continue;
                        }

                        if (Vector3.Dot(data.Normal, data2.Normal) > 0.999f)
                        {
                            if (data.Group == -1)
                            {
                                data.Group = groupID++;
                            }

                            if (data2.Group != -1 && data2.Group != data.Group)
                            {
                                int min = Math.Min(data2.Group, data.Group);
                                int max = Math.Max(data2.Group, data.Group);

                                if (joinedGroups.ContainsKey(min) == true)
                                {
                                    joinedGroups[min].Add(max);
                                }
                                else
                                {
                                    joinedGroups.Add(min, new List<int>(new int[] { max }));
                                }
                            }

                            data2.Group = data.Group;
                        }

                        model.ModelData.Faces[j] = data2;
                    }

                    model.ModelData.Faces[i] = data;
                }

                int changedCount = 0;

                do
                {
                    changedCount = 0;

                    foreach (int key in joinedGroups.Keys)
                    {
                        List<int> joined = joinedGroups[key];

                        for (int i = 0; i < model.ModelData.Faces.Count; i++)
                        {
                            FaceData data = model.ModelData.Faces[i];

                            if (joined.Contains(data.Group) == true)
                            {
                                data.Group = key;
                                changedCount++;
                            }

                            model.ModelData.Faces[i] = data;
                        }
                    }
                }
                while (changedCount > 0);

                model.ModelData.CalcuateFaceNormals();
            }

            StaticObjects.Data.UpdateModels_Full();

            glControl1.Invalidate();
        }


        private void makeCircularUnitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MakeCircularXyz(1f);
        }

        private void MakeCircularXyz(float maxDist)
        {
            foreach (EditableModelData model in StaticObjects.Data.Models.Values)
            {
                for (int i = 0; i < model.ModelData.Verts.Count; i++)
                {
                    Vector3 vert = model.ModelData.Verts[i];

                    Vector3 xyz = vert;

                    xyz.Normalize();

                    vert = xyz * maxDist;

                    model.ModelData.Verts[i] = vert;
                }

                model.ModelData.CalcuateFaceNormals();
            }

            StaticObjects.Data.UpdateModels_Full();

            glControl1.Invalidate();
        }

        private void decomposeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void generateMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                StaticObjects.Data.ClearEditor();

                for (int i = 0; i < 14; i++)
                {
                    StaticObjects.Data.InputState.DoZoom(-3);
                }

                //StaticObjects.Data.Selection.Clear();
                //StaticObjects.Data.SelectionCache.Clear();

                Biome biome;

                BiomeGroup group = (BiomeGroup)m_MasterRandom.Next(0, (int)BiomeGroup.MAX);

                StaticObjects.Data.AddModel(StaticObjects.Data.ModelID++, m_MapGenerator.Generate(m_MasterRandom.Next(), group, out biome));

                wireframeToolStripMenuItem.Checked = false;
                m_SelectionType.SelectedIndex = (int)ModelSelectionMode.Faces;
                //m_Editor.ModelLighting = EditorLightingMode.AllAround;
                //SetLightingChecked();                

                StaticObjects.Data.UpdateModels_Full();

                glControl1.Invalidate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void reSeedGenerationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_MasterRandom = new Random();
        }

        private void scale2xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScaleModels(2f);
        }

        private void scale05xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScaleModels(0.5f);
        }

        private void ScaleModels(float scale)
        {
            foreach (EditableModelData model in StaticObjects.Data.Models.Values)
            {
                model.ActiveScale = new Vector3(scale, scale, scale);
                model.ApplyTransform();

                for (int i = 0; i < model.ModelData.HullPrimitives.Count; i++)
                {
                    ModelPrimitive prim = model.ModelData.HullPrimitives[i];

                    prim.Center *= scale;
                    prim.Scale *= scale;

                    model.ModelData.HullPrimitives[i] = prim;
                }
            }

            StaticObjects.Data.UpdateModels_Full();

            glControl1.Invalidate();
        }

        private void centeriseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Vector3 center = Vector3.Zero;

            foreach (EditableModelData model in StaticObjects.Data.Models.Values)
            {
                center += model.ModelData.Center;
            }

            center /= (float)StaticObjects.Data.Models.Count;

            foreach (EditableModelData model in StaticObjects.Data.Models.Values)
            {
                for (int i = 0; i < model.ModelData.Verts.Count; i++)
                {
                    model.ModelData.Verts[i] -= center;
                }

                for (int i = 0; i < model.ModelData.HullPrimitives.Count; i++)
                {
                    ModelPrimitive prim = model.ModelData.HullPrimitives[i];

                    prim.Center -= center;

                    model.ModelData.HullPrimitives[i] = prim;
                }
            }

            StaticObjects.Data.UpdateModels_Full();

            glControl1.Invalidate();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StaticObjects.Data.Selection.Clear();

            switch (StaticObjects.Data.InputState.SelectionMode)
            {
                case ModelSelectionMode.Vertices:
                    foreach (EditableModelData model in StaticObjects.Data.Models.Values)
                    {
                        ModelSelectionInfo info = new ModelSelectionInfo()
                        {
                            ModelID = model.ModelID,
                            Array = ModelArrays.Verts,
                        };

                        for (int i = 0; i < model.ModelData.Verts.Count; i++)
                        {
                            info.Indices.Add(i);
                        }

                        StaticObjects.Data.Selection.Add(info);
                    }
                    break;
                case ModelSelectionMode.Edges:
                    break;
                case ModelSelectionMode.Faces:
                    foreach (EditableModelData model in StaticObjects.Data.Models.Values)
                    {
                        ModelSelectionInfo info = new ModelSelectionInfo()
                        {
                            ModelID = model.ModelID,
                            Array = ModelArrays.Faces,
                        };

                        for (int i = 0; i < model.ModelData.Faces.Count; i++)
                        {
                            info.Indices.Add(i);
                        }

                        StaticObjects.Data.Selection.Add(info);
                    }
                    break;
                case ModelSelectionMode.Objects:
                    break;
                default:
                    break;
            }

            StaticObjects.Data.CacheSelection();

            StaticObjects.Data.UpdateModels_Full();

            StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);

            glControl1.Invalidate();
        }

        private void invertSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ModelSelectionInfo> newSelections = new List<ModelSelectionInfo>();

            switch (StaticObjects.Data.InputState.SelectionMode)
            {
                case ModelSelectionMode.Vertices:
                    foreach (EditableModelData model in StaticObjects.Data.Models.Values)
                    {
                        ModelSelectionInfo oldSelection = StaticObjects.Data.Selection.FindSelection(model.ModelID, ModelArrays.Verts);

                        ModelSelectionInfo info = new ModelSelectionInfo()
                        {
                            ModelID = model.ModelID,
                            Array = ModelArrays.Verts,
                        };

                        for (int i = 0; i < model.ModelData.Verts.Count; i++)
                        {
                            if (oldSelection.Indices.Contains(i) == false)
                            {
                                info.Indices.Add(i);
                            }
                        }

                        newSelections.Add(info);
                    }
                    break;
                case ModelSelectionMode.Edges:
                    break;
                case ModelSelectionMode.Faces:
                    foreach (EditableModelData model in StaticObjects.Data.Models.Values)
                    {
                        ModelSelectionInfo oldSelection = StaticObjects.Data.Selection.FindSelection(model.ModelID, ModelArrays.Faces);

                        ModelSelectionInfo info = new ModelSelectionInfo()
                        {
                            ModelID = model.ModelID,
                            Array = ModelArrays.Faces,
                        };

                        for (int i = 0; i < model.ModelData.Faces.Count; i++)
                        {
                            if (oldSelection.Indices.Contains(i) == false)
                            {
                                info.Indices.Add(i);
                            }
                        }

                        newSelections.Add(info);
                    }
                    break;
                case ModelSelectionMode.Objects:
                    break;
                default:
                    break;
            }

            StaticObjects.Data.Selection.Clear();

            StaticObjects.Data.Selection.Add(newSelections.ToArray());

            StaticObjects.Data.CacheSelection();

            StaticObjects.Data.UpdateModels_Full();

            StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.SelectionChange);

            glControl1.Invalidate();
        }


        private void createHullFromFacesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switch (StaticObjects.Data.InputState.SelectionMode)
            {
                case ModelSelectionMode.Vertices:
                case ModelSelectionMode.Edges:
                case ModelSelectionMode.Objects:
                    return;
                case ModelSelectionMode.Faces:
                    foreach (EditableModelData model in StaticObjects.Data.Models.Values)
                    {
                        ModelSelectionInfo oldSelection = StaticObjects.Data.Selection.FindSelection(model.ModelID, ModelArrays.Faces);

                        HullData hull = new HullData();

                        foreach (int index in oldSelection.Indices)
                        {
                            FaceData face = model.ModelData.Faces[index];

                            hull.Faces.Add(new FaceData()
                            {
                                V0 = face.V0,
                                V1 = face.V1,
                                V2 = face.V2,
                                Color = face.Color,
                                Group = -1, // group,
                            });
                        }

                        foreach (FaceData face in hull.Faces)
                        {
                            hull.Center += model.ModelData.CalculateFaceCenter(face);
                        }

                        hull.Center /= (float)hull.Faces.Count;

                        model.ModelData.ConvexHullData.Add(hull);
                    }
                    break;
                default:
                    break;
            }

            //StaticObjects.Data.Selection.Clear();

            //StaticObjects.Data.Selection.Add(newSelections.ToArray());

            StaticObjects.Data.CacheSelection();

            StaticObjects.Data.UpdateModels_Full();

            StaticObjects.Data.UndoHistory.Add(UndoHistoryStepType.ModelModified);

            glControl1.Invalidate();
        }

        public static void GetUV(Vector3 vectorToCenter, out float u, out float v)
        {
            //Debug.Log("Initial Vector: " + vectorToCenter.ToString());

            // Noticing poles aren't working. Not sure if that's intentional. 	
            float angle = (float)Math.Atan2(vectorToCenter.Z, vectorToCenter.X);
            u = (angle / (float)(Math.PI * 2f));
            v = 0.5f - (float)(Math.Asin(vectorToCenter.Y) / Math.PI);

            //Debug.Log("Calculated U: " + u + ", V: " + v);

            //Debug.Log("Resolved Vector: " + GetDirectionFromUV(u, v));

        }


        private void exportOBJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            ModelData data = StaticObjects.Data.Models.Values.First().ModelData;

            sb.AppendLine("mtllib sphere.mtl");
            sb.AppendLine("o ModelData");

            for (int i = 0; i < data.Verts.Count; i++)
            {
                Vector3 vert = data.Verts[i];

                //vert *= 0.5f;

                sb.AppendLine(string.Format("v {0} {1} {2}", vert.X, vert.Y, vert.Z));
            }

            for (int i = 0; i < data.Verts.Count; i++)
            {
                /* 
                Vector3 nomalisedVert = -Vector3.Normalize(data.Verts[i]); 
                
                float u, v;

                GetUV(nomalisedVert, out u, out v);

                sb.AppendLine(string.Format("vt {0} {1}", u, 1f - v));
                */

                Vector3 nomalisedVert = data.Verts[i];

                if (nomalisedVert.Z > 0)
                {
                    float u = nomalisedVert.X * 0.5f + 0.5f, v = nomalisedVert.Y * 0.5f + 0.5f;

                    //GetUV(nomalisedVert, out u, out v);

                    sb.AppendLine(string.Format("vt {0} {1}", u, v));
                }
                else
                {
                    float u = (-nomalisedVert.X) * 0.5f, v = nomalisedVert.Y * 0.5f + 0.5f;

                    //GetUV(nomalisedVert, out u, out v);

                    sb.AppendLine(string.Format("vt {0} {1}", u, v));
                }
            }

            Vector3 center = new Vector3(0f, 0f, 0f);

            for (int i = 0; i < data.Faces.Count; i++)
            {
                Vector3 normal = data.Faces[i].Normal;

                sb.AppendLine(string.Format("vn {0} {1} {2}", normal.X, normal.Y, normal.Z));
            }

            sb.AppendLine("usemtl Default_Smoothing");
            sb.AppendLine("s off");

            for (int i = 0; i < data.Faces.Count; i++)
            {
                FaceData face = data.Faces[i];

                //f 181/292/176 182/250/182 209/210/208

                if (face.V0 < 0 || face.V0 >= data.Verts.Count ||
                    face.V1 < 0 || face.V1 >= data.Verts.Count ||
                    face.V2 < 0 || face.V2 >= data.Verts.Count)
                {
                    throw new Exception("This is a problem");
                }

                sb.AppendLine(string.Format("f {0}/{0}/{3} {1}/{1}/{3} {2}/{2}/{3}", face.V0 + 1, face.V1 + 1, face.V2 + 1, i + 1));
            }

            SaveObj(sb);
        }

        /* 
        private void exportOBJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            ModelData data = StaticObjects.Data.Models.Values.First().ModelData;

            sb.AppendLine("mtllib sphere.mtl");

            for (int i = 0; i < data.Verts.Count; i++)
            {
                Vector3 vert = data.Verts[i];

                //vert *= 0.5f;

                sb.AppendLine(string.Format("v {0} {1} {2}", vert.X, vert.Y, vert.Z));
            }

            for (int i = 0; i < data.Verts.Count; i++)
            {
                /* 
                Vector3 nomalisedVert = -Vector3.Normalize(data.Verts[i]); 
                
                float u, v;

                GetUV(nomalisedVert, out u, out v);

                sb.AppendLine(string.Format("vt {0} {1}", u, 1f - v));
                * /

                Vector3 nomalisedVert = data.Verts[i];

                if (nomalisedVert.Z > 0)
                {
                    float u = nomalisedVert.X * 0.5f + 0.5f, v = nomalisedVert.Y * 0.5f + 0.5f;

                    //GetUV(nomalisedVert, out u, out v);

                    sb.AppendLine(string.Format("vt {0} {1}", u, v));
                }
                else
                {
                    float u = (-nomalisedVert.X) * 0.5f, v = nomalisedVert.Y * 0.5f + 0.5f;

                    //GetUV(nomalisedVert, out u, out v);

                    sb.AppendLine(string.Format("vt {0} {1}", u, v));
                }
            }

            Vector3 center = new Vector3(0f, 0f, 0f);

            for (int i = 0; i < data.Verts.Count; i++)
            {
                Vector3 normal = Vector3.Normalize(data.Verts[i]);
               
                sb.AppendLine(string.Format("vn {0} {1} {2}", normal.X, normal.Y, normal.Z));
            }

            sb.AppendLine("usemtl Default_Smoothing");
            sb.AppendLine("s 1");

            for (int i = 0; i < data.Faces.Count; i++)
            {
                FaceData face = data.Faces[i];

                //f 181/292/176 182/250/182 209/210/208

                if (face.V0 < 0 || face.V0 >= data.Verts.Count ||
                    face.V1 < 0 || face.V1 >= data.Verts.Count ||
                    face.V2 < 0 || face.V2 >= data.Verts.Count)
                {
                    throw new Exception("This is a problem");
                }

                sb.AppendLine(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}", face.V0 + 1, face.V1 + 1, face.V2 + 1));
            }

            SaveObj(sb);
        }
        */

        /* 
        private void exportOBJToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            ModelData data = StaticObjects.Data.Models.Values.First().ModelData;

            sb.AppendLine("mtllib sphere.mtl");

            for (int i = 0; i < data.Verts.Count; i++)
            {
                Vector3 vert = data.Verts[i];

                sb.AppendLine(string.Format("v {0} {1} {2}", vert.X, vert.Y, vert.Z));
            }

            for (int i = 0; i < data.Verts.Count; i++)
            {
                sb.AppendLine(string.Format("vt 0 0"));
            }

            Vector3 center = new Vector3(0f, 0.25f, 0f);

            for (int i = 0; i < data.Verts.Count; i++)
            {
                Vector3 vert = data.Verts[i];

                //vert -= center;

                Vector2 distFromCenter = vert.Xz;

                float radius = distFromCenter.Length * ((float)Math.PI * 0.54f); // 0.58f 

                float angle = (float)(Math.Atan2(distFromCenter.Y, distFromCenter.X));

                Vector3 normal = Vector3.Transform(Vector3.Transform(Vector3.UnitY, Matrix4.CreateRotationZ(radius)), Matrix4.CreateRotationY(angle));
                //Vector3 normal = Vector3.Transform(Vector3.Transform(Vector3.UnitZ, Matrix4.CreateRotationX(radius)), Matrix4.CreateRotationZ(angle));

                normal.Normalize();

                // Tilt for the dome
                //normal = Vector3.Transform(normal, Matrix4.CreateRotationX((MathHelper.TwoPi / 360f) * 10f));

                sb.AppendLine(string.Format("vn {0} {1} {2}", normal.X, normal.Y, normal.Z));
            }

            sb.AppendLine("usemtl Default_Smoothing");
            sb.AppendLine("s 1");

            for (int i = 0; i < data.Faces.Count; i++)
            {
                FaceData face = data.Faces[i];

                //f 181/292/176 182/250/182 209/210/208

                if (face.V0 < 0 || face.V0 >= data.Verts.Count ||
                    face.V1 < 0 || face.V1 >= data.Verts.Count ||
                    face.V2 < 0 || face.V2 >= data.Verts.Count)
                {
                    throw new Exception("This is a problem");
                }

                sb.AppendLine(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}", face.V0 + 1, face.V1 + 1, face.V2 + 1));
            }

            SaveObj(sb);
        }
        */

        private void SaveObj(StringBuilder sb)
        {
            if (String.IsNullOrEmpty(StaticObjects.Data.FilePath) == false)
            {
                string resolvedPath = Helper.ResolvePath(StaticObjects.Data.FilePath);

                FileInfo fileInfo = new FileInfo(resolvedPath);

                saveFileDialog2.InitialDirectory = fileInfo.DirectoryName;
                saveFileDialog2.FileName = fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length) + ".obj";
            }

            if (saveFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog2.FileName, sb.ToString());
            }
        }
    }
}
