﻿using AfterLotusPointMap;
using ModelEditorGL.Dialogs;
using ModelEditorGL.Editor;
using OpenTK;
using OpenTK.Graphics;
using Rug.Game;
using System;
using System.Drawing;
using System.Windows.Forms;
using Environment = Rug.Game.Environment;

namespace ModelEditorGL
{
    partial class EditorForm : Form
    {
		private GLControl glControl1;
        private EditorPanel m_Editor;
        private readonly Clock Clock = new Clock();
        private float m_FrameAccumulator;
        private int m_FrameCount;

        private Toolbox m_Toolbox;
        private Palette m_Palette;
        private ObjectsViewer m_ObjectsView;
        private UndoHistoryViewer m_UndoHistory;

        private MapModelGenerator m_MapGenerator = new MapModelGenerator();
        private Random m_MasterRandom = new Random(24232349 ^ 23084); 

        public EditorForm()
		{
			InitializeComponent();
             
			glControl1 = new GLControl(new OpenTK.Graphics.GraphicsMode(32, 24, 0, 8))
			{
				Dock = DockStyle.Fill,
			};

			panel1.Controls.Add(glControl1);

			glControl1.Paint += glControl1_Paint;
			glControl1.Resize += glControl1_Resize;
			glControl1.KeyDown += glControl1_KeyDown;
            glControl1.KeyPress += glControl1_KeyPress;
            glControl1.KeyUp += glControl1_KeyUp;
            glControl1.MouseClick += glControl1_MouseClick;
            glControl1.MouseDoubleClick += glControl1_MouseDoubleClick;
            glControl1.MouseDown += glControl1_MouseDown;
            glControl1.MouseMove += glControl1_MouseMove;
            glControl1.MouseUp += glControl1_MouseUp;
            glControl1.MouseWheel += glControl1_MouseWheel;

			glControl1.Load += new EventHandler(glControl1_Load);
			glControl1.Disposed += new EventHandler(glControl1_Disposed);

            m_Editor = new EditorPanel();
            m_Editor.Control = glControl1;
            
            m_Toolbox = new Toolbox();
            m_Toolbox.VisibleChanged += m_Toolbox_VisibleChanged;
            m_Toolbox.SelectedColorChanged += m_Toolbox_SelectedColorChanged;
            m_Toolbox.SelectedToolChanged += m_Toolbox_SelectedToolChanged;
            m_Toolbox.SelectedSelectionModeChanged += m_Toolbox_SelectedSelectionModeChanged;

            m_Palette = new Palette();            
            m_Palette.VisibleChanged += m_Palette_VisibleChanged;
            m_Palette.SelectedColorChanged += m_Palette_SelectedColorChanged;

            m_ObjectsView = new ObjectsViewer();
            m_ObjectsView.VisibleChanged += m_ObjectsView_VisibleChanged;

            m_UndoHistory = new UndoHistoryViewer();
            m_UndoHistory.HistorySelected += m_UndoHistory_HistorySelected;

            solidToolStripMenuItem.Checked = true;
            verticesToolStripMenuItem.Checked = true;
            wireframeToolStripMenuItem.Checked = true;
            facesToolStripMenuItem.Checked = true;
            normalsToolStripMenuItem.Checked = true;
            hullToolStripMenuItem.Checked = true;
            extrasToolStripMenuItem.Checked = true;
            decompositionToolStripMenuItem.Checked = true;
            objectsToolStripMenuItem.Checked = true;

            for (int i = 0; i <= (int)EditorToolMode.Pipette; i++)
            {
                m_ToolSelection.Items.Add((EditorToolMode)i);
            }

            for (int i = 0; i <= (int)ModelSelectionMode.Objects; i++)
            {
                m_SelectionType.Items.Add((ModelSelectionMode)i);
            }

            SetDefaults();
            
            m_Toolbox.Show(this);
            m_Palette.Show(this);
            m_ObjectsView.Show(this);
            m_UndoHistory.Show(this);

            StaticObjects.Data.UndoHistory.Changed += UndoHistory_Changed;
            StaticObjects.Data.EditorControlsNeedUpdate += Data_EditorControlsNeedUpdate;
            StaticObjects.Data.EditorObjectsNeedUpdate += Data_EditorObjectsNeedUpdate;
            StaticObjects.Invalidated += StaticObjects_Invalidated;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			glControl1_Resize(this, EventArgs.Empty); 

            GLState.ClearColor(new Color4(48, 48, 48, 255));

            if (Preferences.Windows["Editor"].Bounds != Rectangle.Empty)
            {
                this.DesktopBounds = Preferences.Windows["Editor"].Bounds;
            }

            WindowState = Preferences.Windows["Editor"].WindowState;
		}

        #region Window Resize / Move Events

        private void Form_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form_ResizeEnd(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                Preferences.Windows["Editor"].Bounds = this.DesktopBounds;
            }
        }

        private void Form_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Minimized)
            {
                Preferences.Windows["Editor"].WindowState = WindowState;
            }
        }

        #endregion

        #region GL Control Events

        void glControl1_Load(object sender, EventArgs e)
        {
            m_Editor.Bounds = glControl1.ClientRectangle;
            m_Editor.Load();

            StaticObjects.Data.Open("~/Data/Cube.xml");             

            Clock.Start();
        }

        void glControl1_Disposed(object sender, EventArgs e)
        {
            m_Editor.Dispose();
        }

		private void glControl1_Paint(object sender, PaintEventArgs e)
		{
			Environment.DrawCalls = Environment.DrawCallsAccumulator;
			Environment.DrawCallsAccumulator = 0;

			Environment.BufferUpdates = Environment.BufferUpdatesAccumulator;
			Environment.BufferUpdatesAccumulator = 0;

			Environment.FrameDelta = Clock.Update();

			m_FrameAccumulator += Environment.FrameDelta;

			++m_FrameCount;

			if (m_FrameAccumulator >= 1.0f)
			{
				Environment.FramesPerSecond = m_FrameCount / m_FrameAccumulator;
				Environment.FramesClick = true;
				m_FrameAccumulator = 0.0f;
				m_FrameCount = 0;

			}
			else
			{
				Environment.FramesClick = false;
			}

			glControl1.MakeCurrent();

            m_Editor.Render(Environment.FrameDelta); 

			glControl1.SwapBuffers();
		}

		private void glControl1_Resize(object sender, EventArgs e)
		{
			if (glControl1.ClientSize.Height == 0)
			{
				glControl1.ClientSize = new System.Drawing.Size(glControl1.ClientSize.Width, 1);
			}

            m_Editor.Bounds = glControl1.ClientRectangle;

            m_Editor.Resize(); 
		}

		private void glControl1_KeyDown(object sender, KeyEventArgs e)
		{
            if (e.KeyCode == Keys.S)
            {
                m_ToolSelection.SelectedIndex = (int)EditorToolMode.Select;
            }

            if (e.KeyCode == Keys.M)
            {
                m_ToolSelection.SelectedIndex = (int)EditorToolMode.Move;
            }

            if (e.KeyCode == Keys.P)
            {
                m_ToolSelection.SelectedIndex = (int)EditorToolMode.Paint;
            }

            if (e.KeyCode == Keys.D)
            {
                m_ToolSelection.SelectedIndex = (int)EditorToolMode.Pipette;
            }

            if (e.KeyCode == Keys.E)
            {
                m_ToolSelection.SelectedIndex = (int)EditorToolMode.Scale;
            }

            if (e.KeyCode == Keys.R)
            {
                m_ToolSelection.SelectedIndex = (int)EditorToolMode.Rotate;
            }

            if (e.KeyCode == Keys.D1)
            {
                m_SelectionType.SelectedIndex = 0;
            }

            if (e.KeyCode == Keys.D2)
            {
                m_SelectionType.SelectedIndex = 1;
            }

            if (e.KeyCode == Keys.D3)
            {
                m_SelectionType.SelectedIndex = 2;
            }

            if (e.KeyCode == Keys.D4)
            {
                m_SelectionType.SelectedIndex = 3;
            }

            /* 
            if (e.KeyCode == Keys.D3)
            {
                m_SelectionType.SelectedIndex = 4;
            }
            */ 

            m_Editor.KeyDown(sender, e); 
		}

        void glControl1_KeyUp(object sender, KeyEventArgs e)
        {
            m_Editor.KeyUp(sender, e); 
        }

        void glControl1_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            m_Editor.KeyPress(sender, e); 
        }

        void glControl1_MouseWheel(object sender, MouseEventArgs e)
        {
            m_Editor.MouseWheel(sender, e); 
        }

        void glControl1_MouseUp(object sender, MouseEventArgs e)
        {
            m_Editor.MouseUp(sender, e); 
        }

        void glControl1_MouseMove(object sender, MouseEventArgs e)
        {
            m_Editor.MouseMove(sender, e); 
        }

        void glControl1_MouseDown(object sender, MouseEventArgs e)
        {
            m_Editor.MouseDown(sender, e); 
        }

        void glControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            m_Editor.MouseDoubleClick(sender, e); 
        }

        void glControl1_MouseClick(object sender, MouseEventArgs e)
        {
            m_Editor.MouseClick(sender, e);
        }

        #endregion 

        private void EditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing || e.CloseReason == CloseReason.None)
            {
                if (StaticObjects.Data.NeedsSave == true)
                {
                    switch (MessageBox.Show(this, "Save unsaved changes?", "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.Cancel:
                            e.Cancel = true;
                            break;
                        case DialogResult.No:
                            break;
                        case DialogResult.Yes:
                            saveToolStripMenuItem_Click(sender, e);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
