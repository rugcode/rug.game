﻿using Rug.Game;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace ModelEditorGL
{
    class WindowOptions
    {
        /// <summary>
        /// Is the window currently open.
        /// </summary>
        public bool IsOpen { get; set; }

        /// <summary>
        /// The current state of the window (maximised or normal).
        /// </summary>
        public FormWindowState WindowState { get; set; }

        /// <summary>
        /// The bounds of the window on the desktop.
        /// </summary>
        public Rectangle Bounds { get; set; }
    }

    class WindowManager
    {
        private readonly Dictionary<string, WindowOptions> m_Windows = new Dictionary<string, WindowOptions>();

        public ICollection<string> Keys { get { return m_Windows.Keys; } }

        public void Clear()
        {
            m_Windows.Clear();
        }

        public bool Contains(string key)
        {
            return m_Windows.ContainsKey(key);
        }

        public WindowOptions this[string key]
        {
            get
            {
                if (Contains(key) == false)
                {
                    m_Windows.Add(key, new WindowOptions());
                }

                return m_Windows[key];
            }
        }
    }

    static class Preferences
    {
        /// <summary>
        /// The location of the options file 
        /// </summary>
        private static string m_PreferencesFileName = "~/Preferences.xml";

        /// <summary>
        /// The current state of the window (maximised or normal) 
        /// </summary>
        public static FormWindowState WindowState { get; set; }

        /// <summary>
        /// The bounds of the window on the desktop
        /// </summary>
        public static Rectangle Bounds { get; set; }

        public static readonly WindowManager Windows = new WindowManager();

        /* 
        public static bool AutoConnectAtStartup { get; set; }

        public static bool ConnectToFirstDevice { get; set; }

        public static bool RememberWindowLayout { get; set; }

        public static bool AutoDisconnect { get; set; }

        public static bool DeviceErrorsInMessageBox { get; set; }

        public static AutoConnectorSearchTypes AutoConnectSearchType { get; set; }

        public static readonly List<ConnectionManagerInfo> SerialConnections = new List<ConnectionManagerInfo>();

        public static readonly List<ConnectionManagerInfo> WifiConnections = new List<ConnectionManagerInfo>();

        public static int AsyncRetryCount { get; set; }

        public static int AsyncProcessTimeout { get; set; }
        */

        /// <summary>
        /// Load the options 
        /// </summary>
        public static void Load()
        {
            // set all options to their defaults 
            SetDefaults();

            // check to see if there is a options file to load 
            if (File.Exists(Helper.ResolvePath(m_PreferencesFileName)) == false)
            {
                return;
            }

            // try to load the options
            Inner_Load();
        }

        public static void SetDefaults()
        {
            // set the bound to empty
            Bounds = Rectangle.Empty;

            // normal window state
            WindowState = FormWindowState.Normal;

            /* 
            AutoConnectSearchType = AutoConnectorSearchTypes.Any;

            AutoConnectAtStartup = true;

            ConnectToFirstDevice = false;

            RememberWindowLayout = true;

            AutoDisconnect = true;

            DeviceErrorsInMessageBox = true;

            AsyncRetryCount = 3;

            AsyncProcessTimeout = 100;
            */ 

            Windows.Clear();
        }

        private static void Inner_Load()
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                // load the options from the resolved path
                doc.Load(Helper.ResolvePath(m_PreferencesFileName));

                XmlNode node = doc.DocumentElement;

                // if the node is not null 
                if (node != null)
                {
                    /* 
                    AutoConnectSearchType = Helper.GetAttributeValue(node, "AutoConnectSearchType", AutoConnectSearchType);

                    AutoConnectAtStartup = Helper.GetAttributeValue(node, "AutoConnectAtStartup", AutoConnectAtStartup);

                    ConnectToFirstDevice = Helper.GetAttributeValue(node, "ConnectToFirstDevice", ConnectToFirstDevice);

                    RememberWindowLayout = Helper.GetAttributeValue(node, "RememberWindowLayout", RememberWindowLayout);

                    AutoDisconnect = Helper.GetAttributeValue(node, "AutoDisconnect", AutoDisconnect);

                    DeviceErrorsInMessageBox = Helper.GetAttributeValue(node, "DeviceErrorsInMessageBox", DeviceErrorsInMessageBox);

                    AsyncRetryCount = Helper.GetAttributeValue(node, "AsyncRetryCount", AsyncRetryCount);

                    AsyncProcessTimeout = Helper.GetAttributeValue(node, "AsyncProcessTimeout", AsyncProcessTimeout);
                    */ 

                    // get rhe string for the bounding rectangle
                    Bounds = CheckWindowBounds(Helper.GetAttributeValue(node, "Bounds", Bounds));

                    // get the window state
                    WindowState = Helper.GetAttributeValue(node, "WindowState", WindowState);

                    foreach (XmlNode windowNode in node.SelectNodes("Windows/Window"))
                    {
                        string name = Helper.GetAttributeValue(windowNode, "Name", null);

                        if (String.IsNullOrEmpty(name) == true)
                        {
                            continue;
                        }

                        if (Windows.Contains(name) == true)
                        {
                            continue;
                        }

                        WindowOptions options = Windows[name];

                        // get the open state 
                        options.IsOpen = Helper.GetAttributeValue(windowNode, "IsOpen", options.IsOpen);

                        // get the window state
                        options.WindowState = Helper.GetAttributeValue(windowNode, "WindowState", options.WindowState);

                        // get the bounding rectangle
                        options.Bounds = CheckWindowBounds(Helper.GetAttributeValue(windowNode, "Bounds", options.Bounds));
                    }
                }
            }
            catch (Exception ex)
            {
                // somthing went wrong, tell the user
                MessageBox.Show(ex.Message, "Could not load options");
            }
        }

        public static void Save()
        {
            try
            {
                XmlDocument doc = new XmlDocument();

                XmlElement node = Helper.CreateElement(doc, "Options");
                doc.AppendChild(node);

                /* 
                Helper.AppendAttributeAndValue(node, "AutoConnectSearchType", AutoConnectSearchType);
                Helper.AppendAttributeAndValue(node, "AutoConnectAtStartup", AutoConnectAtStartup);
                Helper.AppendAttributeAndValue(node, "ConnectToFirstDevice", ConnectToFirstDevice);
                Helper.AppendAttributeAndValue(node, "RememberWindowLayout", RememberWindowLayout);
                Helper.AppendAttributeAndValue(node, "AutoDisconnect", AutoDisconnect);
                Helper.AppendAttributeAndValue(node, "DeviceErrorsInMessageBox", DeviceErrorsInMessageBox);

                Helper.AppendAttributeAndValue(node, "AsyncRetryCount", AsyncRetryCount);
                Helper.AppendAttributeAndValue(node, "AsyncProcessTimeout", AsyncProcessTimeout);
                */ 

                //if (RememberWindowLayout == true)
                {
                    Helper.AppendAttributeAndValue(node, "Bounds", Bounds);
                    Helper.AppendAttributeAndValue(node, "WindowState", WindowState);

                    XmlElement windows = Helper.CreateElement(doc, "Windows");

                    foreach (string name in Windows.Keys)
                    {
                        WindowOptions options = Windows[name];

                        XmlElement window = Helper.CreateElement(doc, "Window");

                        Helper.AppendAttributeAndValue(window, "Name", name);

                        Helper.AppendAttributeAndValue(window, "IsOpen", options.IsOpen);

                        Helper.AppendAttributeAndValue(window, "WindowState", options.WindowState);

                        Helper.AppendAttributeAndValue(window, "Bounds", options.Bounds);


                        windows.AppendChild(window);
                    }

                    node.AppendChild(windows);
                }

                Helper.EnsurePathExists(Helper.ResolvePath(m_PreferencesFileName));
                doc.Save(Helper.ResolvePath(m_PreferencesFileName));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Could not save options");
            }
        }

        private static Rectangle CheckWindowBounds(Rectangle bounds)
        {
            // if the bounds is not empty
            if (bounds != Rectangle.Empty)
            {
                // check that the bounds is on the screen
                if (IsOnScreen(bounds) == false)
                {
                    // if the bounds is off the screen set it to empty 
                    bounds = Rectangle.Empty;
                }
            }

            return bounds;
        }

        /// <summary>
        /// Check that a rectangle is fully on the screen
        /// </summary>
        /// <param name="rectangle">the rectangle to check</param>
        /// <returns>true if the rectangle is fully on a screen</returns>
        private static bool IsOnScreen(Rectangle rectangle)
        {
            Screen[] screens = Screen.AllScreens;
            foreach (Screen screen in screens)
            {
                if (screen.WorkingArea.Contains(rectangle))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
