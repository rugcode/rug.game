﻿using Rug.Game.Core.Text;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace FontMatrixEditor
{
	public partial class FontEditorWindow : Form
	{
		FontMatrix m_FontMatix;
		string m_LoadedFileName;
		Bitmap m_Bitmap; 

		public FontEditorWindow()
		{
			InitializeComponent();

			NewMatrix(); 
		}

		private void DisposeOfMatrix()
		{
			if (m_FontMatix != null)
			{
				m_FontMatix.Dispose();
			}

			m_LoadedFileName = null;

			if (this.IsDisposed == false && this.Disposing == false)
			{
				propertyGrid1.SelectedObject = null;
			}
		}

		private void NewMatrix()
		{
			DisposeOfMatrix();
			
			m_FontMatix = new FontMatrix(); 

			m_LoadedFileName = null;

			propertyGrid1.SelectedObject = m_FontMatix;

			RenderToBitmap(); 
		}

		private void LoadMatrix(string location)
		{
			DisposeOfMatrix();

			m_FontMatix = FontMatrix.Read(location);

			m_LoadedFileName = location;

			propertyGrid1.SelectedObject = m_FontMatix;

			RenderToBitmap(); 
		}

		private void SaveMatrix(string location)
		{
			if (m_FontMatix != null)
			{
				m_FontMatix.Write(location);
				m_LoadedFileName = location; 
			}
		}

		private void RenderToBitmap()
		{
			if (m_Bitmap != null)
			{
				pictureBox1.Image = null;

				m_Bitmap.Dispose();

				m_Bitmap = null; 
			}

			if (m_FontMatix != null)
			{
				m_FontMatix.XLarge.Dispose();
				m_FontMatix.Large.Dispose();
				m_FontMatix.Heading.Dispose();
				m_FontMatix.Regular.Dispose();
				m_FontMatix.Small.Dispose();				
				m_FontMatix.Monospaced.Dispose();

				using (Bitmap tempBmp = m_FontMatix.RenderToBitmap())
				{
					m_Bitmap = new Bitmap(tempBmp.Width, tempBmp.Height, tempBmp.PixelFormat);

					using (Graphics graphics = Graphics.FromImage(m_Bitmap))
					{						
						graphics.Clear(Color.Transparent);
						graphics.DrawImageUnscaled(tempBmp, 0, 0);

						if (showBoxesToolStripMenuItem.Checked == true)
						{
							foreach (FontType type in new FontType[] { FontType.XLarge, FontType.Large, FontType.Heading, FontType.Regular, FontType.Small, FontType.Monospaced })
							{
								for (int i = 0; i < FontMatrix.AllChars.Length; i++)
								{
									RectangleF rect = m_FontMatix[type, FontMatrix.AllChars[i]];

									graphics.DrawRectangle(Pens.Red, rect.X * tempBmp.Width, rect.Y * tempBmp.Height, rect.Width * tempBmp.Width, rect.Height * tempBmp.Height);
								}
							}
						}
					}
				}

				pictureBox1.Image = m_Bitmap; 
			}
		}

		private void newToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NewMatrix(); 
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				LoadMatrix(openFileDialog1.FileName);
			}
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (m_LoadedFileName != null)
			{
				SaveMatrix(m_LoadedFileName);
			}
			else
			{
				saveAsToolStripMenuItem_Click(sender, e);
			}
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				SaveMatrix(saveFileDialog1.FileName); 
			}
		}

		private void closeToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DisposeOfMatrix(); 
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DisposeOfMatrix();

			Close(); 
		}

		private void FontEditorWindow_FormClosed(object sender, FormClosedEventArgs e)
		{
			DisposeOfMatrix(); 
		}

		private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			RenderToBitmap();
		}

		private void renderToolStripMenuItem_Click(object sender, EventArgs e)
		{
			RenderToBitmap();
		}

		private void showBoxesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			showBoxesToolStripMenuItem.Checked = !showBoxesToolStripMenuItem.Checked; 
		}
	}
}
