﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Audio
{
	public class AudioBankInfo : IResourceInfo
	{
		public ResourceType ResourceType
		{
			get { return ResourceType.AudioBank; }
		}

		public void OnLoad()
		{

		}
	}
}
