﻿using FMOD.Studio;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Audio
{
	public class AudioBank : IResource
	{
		private AudioBankInfo m_ResourceInfo;
		
		#region Members

		public string Name
		{
			get;
			private set; 
		}

		public ResourceType ResourceType
		{
			get { return ResourceInfo.ResourceType; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public AudioBankInfo ResourceInfo
		{
			get { return m_ResourceInfo; } 
		}

		IResourceInfo IResource.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public int ResourceHandle
		{
			get { return 0; }
		}

		uint IResource.ResourceHandle
		{
			get { return unchecked((uint)0); }
		}

		public bool IsLoaded { get; protected set; }

		public string FileName { get; private set; }

		public Bank Bank { get; private set; } 

		public event EventHandler<ResourceEventArgs> LoadStateChanged;

		public AudioBank(string name, string fileName, AudioBankInfo info)
		{
			Name = name; 

			FileName = fileName; 

			m_ResourceInfo = info;
		}

		public void LoadResources()
		{
			if (IsLoaded == true)
			{
				return; 
			}

			Bank bank;

            if (FileHelper.AliasExists(FileName) == true)
            {
                AudioHelper.Check(AudioMaster.Instance.Context.loadBankMemory(PackageHelper.ReadAllBytes(FileName), LOAD_BANK_FLAGS.NORMAL, out bank));
            }
            else if (FileHelper.FileExists(FileName) == true)
            {
                AudioHelper.Check(AudioMaster.Instance.Context.loadBankFile(Helper.ResolvePath(FileName), LOAD_BANK_FLAGS.NORMAL, out bank));
            }
            else
            {
                throw new Exception(string.Format("Could not find Audio Bank file at path '{0}'", FileName));
            }

			Bank = bank;

			IsLoaded = true; 
		}

		public void UnloadResources()
		{
			if (IsLoaded == false)
			{
				return;
			}

			Bank.unload();

			IsLoaded = false; 
		}

		#endregion
	}
}
