﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Audio
{
	public class AudioBufferInfo : IResourceInfo
	{
		public ResourceType ResourceType
		{
			get { return ResourceType.AudioBuffer; }
		}

		public void OnLoad()
		{

		}
	}
}
