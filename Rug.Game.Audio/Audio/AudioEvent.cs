﻿using FMOD.Studio;
using System;
using System.Collections.Generic;

namespace Rug.Game.Audio
{
	public abstract class AudioEvent
	{
        private _3D_ATTRIBUTES m_SpacialInfo; 
 
		public readonly Dictionary<string, AudioEventArgument> Arguments = new Dictionary<string, AudioEventArgument>();
		public readonly List<AudioEventCue> Cues = new List<AudioEventCue>(); 

		public bool IsAttached { get; private set; }

		public AudioBuffer Buffer { get; private set; }

		public EventInstance Instance { get; protected set; }

        public _3D_ATTRIBUTES SpacialInfo 
        {
            set 
            {
                if (IsAttached && Instance != null)
                {
                    Instance.set3DAttributes(value);
                }

                m_SpacialInfo = value; 
            }
            get
            {
                return m_SpacialInfo; 
            }
        }

		public AudioEvent(AudioBuffer buffer)
		{
			Buffer = buffer; 
		}

		public virtual void Attach()
		{
			if (IsAttached == true)
			{
				return; 
			}

			EventInstance instance;

			AudioHelper.Check(Buffer.Description.createInstance(out instance));

			Instance = instance;

			foreach (AudioEventArgument arg in Arguments.Values)
			{
				arg.Attach(); 
			}

			int ie;

			AudioHelper.Check(instance.getCueCount(out ie));

			for (int i = Cues.Count; i < ie; i++)
			{
				Cues.Add(new AudioEventCue(this, i)); 
			}

			foreach (AudioEventCue cues in Cues)
			{
				cues.Attach(); 
			}

			IsAttached = true; 
		}

		public virtual void Detach()
		{
			if (IsAttached == false)
			{
				return; 
			}

			foreach (AudioEventArgument arg in Arguments.Values)
			{
				arg.Detach();
			}

			foreach (AudioEventCue cues in Cues)
			{
				cues.Detach();
			}

			IsAttached = false;
		}
	}
}
