﻿using FMOD.Studio;
using Rug.Cmd;

namespace Rug.Game.Audio
{
	public class AudioEventArgument
	{
		private ParameterInstance m_Instance;
		private float m_Value; 

		public string Name { get; private set; }
		public AudioEvent Event { get; private set; }

		public float Value
		{
			get
			{
				return m_Value; 
			}
			set
			{
				m_Value = value;

				if (m_Instance != null && m_Instance.isValid())
				{
					try
					{
						AudioHelper.Check(m_Instance.setValue(m_Value));
					}
					catch 
					{
						RC.WriteLine("Moop"); 
					} 
				}
			}
		}

		public AudioEventArgument(string name, AudioEvent @event)
		{
			Name = name;
			Event = @event; 
		}

		public void Attach()
		{
			AudioHelper.Check(Event.Instance.getParameter(Name, out m_Instance));

			Value = Value;
			//AudioHelper.Check(m_Instance.getValue(out m_Value)); 		
		}

		public void Detach()
		{
			m_Instance = null; 
		}
	}
}
