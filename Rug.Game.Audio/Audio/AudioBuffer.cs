﻿using FMOD;
using FMOD.Studio;
using Rug.Game.Core.Resources;
using System;

namespace Rug.Game.Audio
{
	public class AudioBuffer : IResource
	{
		private AudioBufferInfo m_ResourceInfo;

		#region IResource Members

		public string Name
		{
			get;
			private set; 
		}

		public ResourceType ResourceType
		{
			get { return ResourceInfo.ResourceType; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public AudioBufferInfo ResourceInfo
		{
			get { return m_ResourceInfo; } 
		}

		IResourceInfo IResource.ResourceInfo
		{
			get { return m_ResourceInfo; }
		}

		public int ResourceHandle
		{
			get { return 0; }
		}

		uint IResource.ResourceHandle
		{
			get { return unchecked((uint)0); }
		}

		public bool IsLoaded
		{
			get;
			private set; 
		}

		public string Path { get; private set; }

        public Guid Buffer { get; private set; }
		public EventDescription Description { get; private set; } 

		public event EventHandler<ResourceEventArgs> LoadStateChanged;

		public AudioBuffer(string name, string path, AudioBufferInfo info)
		{
			Name = name;

			Path = path; 

			m_ResourceInfo = info;
		}

		public void LoadResources()
		{
			if (IsLoaded == true)
			{
				return; 
			}

			Guid id; 

			AudioHelper.Check(AudioMaster.Instance.Context.lookupID(Path, out id));

			EventDescription description;

			//AudioHelper.Check(AudioMaster.Instance.Context.getEvent(id, LOADING_MODE.BEGIN_NOW, out description));
            AudioHelper.Check(AudioMaster.Instance.Context.getEvent(Path, out description));

			Buffer = id;
			Description = description;

			AudioHelper.Check(Description.loadSampleData()); 

			IsLoaded = true; 
		}

		public void UnloadResources()
		{
			if (IsLoaded == false)
			{
				return;
			}

			//AudioHelper.Check(Description.releaseAllInstances()); 
			AudioHelper.Check(Description.unloadSampleData());
			
			IsLoaded = false; 
		}

		#endregion

	}
}
