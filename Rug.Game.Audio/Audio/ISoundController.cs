﻿
namespace Rug.Game.Audio
{
	public interface ISoundController
	{
		string Name { get; } 

		bool AquireSource();

		void ReleaseSource();

		bool HasSource { get; }
	}
}
