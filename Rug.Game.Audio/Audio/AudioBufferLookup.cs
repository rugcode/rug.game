﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Audio
{
	public class AudioBufferLookup : NamedResourceSet<AudioBuffer>
	{
		public AudioBufferLookup(string name, ResourceMode mode)
			: base(name, mode)
		{

		}
	}
}
