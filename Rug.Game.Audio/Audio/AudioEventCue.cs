﻿using FMOD.Studio;

namespace Rug.Game.Audio
{
	public class AudioEventCue
	{
		private CueInstance m_Instance;

		public AudioEvent Event { get; private set; }
		public int Index { get; private set; }

		public AudioEventCue(AudioEvent @event, int index)
		{
			Event = @event;

			Index = index; 
		}

		public void Attach()
		{
			AudioHelper.Check(Event.Instance.getCueByIndex(Index, out m_Instance));
		}

		public void Detach()
		{
			m_Instance = null; 
		}

		public void Trigger()
		{
			AudioHelper.Check(m_Instance.trigger());
		}
	}
}
