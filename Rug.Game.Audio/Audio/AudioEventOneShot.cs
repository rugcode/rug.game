﻿
namespace Rug.Game.Audio
{
	public class AudioEventOneShot : AudioEvent
	{
		public AudioEventOneShot(AudioBuffer buffer) 
			: base(buffer)
		{
			
		}

		public void Play()
		{
			Instance.start(); 
		}

		public void Stop()
		{
			Instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT); 
		}

		public override void Detach()
		{
			base.Detach();

 			AudioHelper.Check(Instance.release()); 
		}
	}
}
