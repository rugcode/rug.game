﻿
namespace Rug.Game.Audio
{
	public class AudioEventLoop : AudioEvent
	{
		public AudioEventLoop(AudioBuffer buffer) 
			: base(buffer)
		{
			
		}

		public void Play()
		{
            if (Instance.isValid() == false)
            {
                return; 
            }

			Instance.start(); 
		}

		public void Stop()
		{
            if (Instance.isValid() == false)
            {
                return;
            }

			Instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT); 
		}

		public override void Detach()
		{
			if (IsAttached == false)
			{
				return; 
			}

			base.Detach();

 			AudioHelper.Check(Instance.release()); 
		}
		/* 
		ERRCHECK(system.getEvent(loopingAmbienceID, LOADING_MODE.BEGIN_NOW, out loopingAmbienceDescription));

		ERRCHECK(loopingAmbienceDescription.createInstance(out loopingAmbienceInstance));
		*/

	}
}
