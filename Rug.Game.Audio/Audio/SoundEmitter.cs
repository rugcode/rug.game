﻿using OpenTK;
using Rug.Cmd;
using System;

namespace Rug.Game.Audio
{
	public enum SoundPriority : int
	{
		Critical = 0,
		High = 1,
		Medium = 2,
		Low = 3, 
	}

	public interface ISoundLocationProvider
	{
		Vector3 GetSoundCenter(); 
	}

	public abstract class SoundEmitter : ISoundController
	{
		public SoundPriority Priority;

		public ISoundLocationProvider LocationProvider;

		//public Matrix4 Offset;

		public AudioEventLoop Source;

		//public AudioBuffer Buffer;

		public bool HasSource { get; protected set; }

		public string Name { get; private set; } 

        public Vector3 Position;

        public Vector3 Velocity; 
	
		public SoundEmitter(string name, string buffer)
		{
			Name = name;

			try
			{
				Source = new AudioEventLoop(AudioMaster.Instance.Buffers[buffer]);
			}
			catch (Exception ex)
			{
				throw new Exception("Could not load SoundEmitter '" + name + "' from buffer '" + buffer + "'.", ex); 
			}
		}

		public bool AquireSource()
		{
			if (Source.IsAttached == false)
			{
				Source.Attach();
			}

			/* 
			if (Source == null)
			{
				//Source = AudioMaster.Instance.FindSource(Priority);

				if (Source != null)
				{
					RC.WriteLine(ConsoleThemeColor.TextGood, "Aquired: " + Name + ", " + Priority);
				}
				else
				{
					RC.WriteLine(ConsoleThemeColor.TextBad, "Failed to aquire: " + Name + ", " + Priority);
				}
			}
			*/ 

			if (Source != null)
			{
				HasSource = true;
				//Source.Controller = this;

				UpdatePosition(); 
			}
			else
			{
				HasSource = false; 
			}

			return Source != null;
		}

		public virtual void ReleaseSource()
		{
			if (HasSource == true)
			{
				RC.WriteLine(ConsoleThemeColor.TextNutral, "Release: " + Name + ", " + Priority);

				//Source.Controller = null;
			}

			try
			{
				Source.Detach();
			}
			catch { } 

			HasSource = false;					
		}

		protected void UpdatePosition()
		{
			if (HasSource) // && LocationProvider != null)
			{
                Source.SpacialInfo = new FMOD.Studio._3D_ATTRIBUTES()
                {
                    up = new FMOD.VECTOR() { x = 0, y = 1, z = 0 },
                    forward = new FMOD.VECTOR() { x = 0, y = 0, z = 1 },
                    velocity = new FMOD.VECTOR() { x = Velocity.X, y = Velocity.Y, z = Velocity.Z },
                    position = new FMOD.VECTOR() { x = Position.X, y = Position.Y, z = Position.Z }, 
                };

                //Source.
				//Source.SetPosition(new Vector3(LocationProvider.GetSoundCenter()));
				/* 
				Source.SetMaxDistance(0.5f);
				Source.SetReferenceDistance(2f);
				Source.SetRolloffFactor(3f);
				*/

				//Source.SetMaxDistance(0.5f);
				//Source.SetReferenceDistance(0.5f);
				//Source.SetRolloffFactor(4f);
			}
		}

		public virtual void Update(float delta)
		{
			UpdatePosition();
		}
	}
}
