﻿
namespace FMOD.Studio
{
    /*
	class FmodDemo : IDisposable
	{
		System system = null;

		Bank masterBank;
		Bank stringsBank;
		//Bank ambienceBank;
		//Bank menuBank;
		//Bank weaponsBank;

		GUID loopingAmbienceID;
		EventDescription loopingAmbienceDescription;
		EventInstance loopingAmbienceInstance;

		//GUID cancelID;
		//EventDescription cancelDescription;
		//EventInstance cancelInstance;

		//GUID explosionID;
		//EventDescription explosionDescription;

		public FmodDemo()
		{
			ERRCHECK(System.create(out system)); 
			ERRCHECK(system.initialize(32, INITFLAGS.NORMAL | INITFLAGS.LIVEUPDATE, FMOD.INITFLAGS.NORMAL | FMOD.INITFLAGS.PROFILE_ENABLE, IntPtr.Zero)); 

			
			ERRCHECK(system.loadBankFile(Helper.ResolvePath("~/Data/Media/Master Bank2.bank"), LOAD_BANK_FLAGS.NORMAL, out masterBank));
			
			ERRCHECK(system.loadBankFile(Helper.ResolvePath("~/Data/Media/Master Bank2.strings.bank"), LOAD_BANK_FLAGS.NORMAL, out stringsBank)); 
				
			//ERRCHECK(system.loadBankFile(Helper.ResolvePath("~/Data/Media/Surround_Ambience.bank"), LOAD_BANK_FLAGS.NORMAL, out ambienceBank));
    			
			//ERRCHECK(system.loadBankFile(Helper.ResolvePath("~/Data/Media/UI_Menu.bank"), LOAD_BANK_FLAGS.NORMAL, out menuBank));

			//ERRCHECK(system.loadBankFile(Helper.ResolvePath("~/Data/Media/Weapons.bank"), LOAD_BANK_FLAGS.NORMAL, out weaponsBank));

			
			// Get the Looping Ambience event
			//ERRCHECK(system.lookupID("event:/Ambience/Country", out loopingAmbienceID));

			ERRCHECK(system.lookupID("event:/Thrust/Main-Thrust", out loopingAmbienceID));

			ERRCHECK(system.getEvent(loopingAmbienceID, LOADING_MODE.BEGIN_NOW, out loopingAmbienceDescription));

			ERRCHECK(loopingAmbienceDescription.createInstance(out loopingAmbienceInstance));

			// Get the 4 Second Surge event			
			//ERRCHECK(system.lookupID("event:/UI/Cancel", out cancelID) );
			
			//ERRCHECK(system.getEvent(cancelID, LOADING_MODE.BEGIN_NOW, out cancelDescription));
    
			//ERRCHECK(cancelDescription.createInstance(out cancelInstance));
    
			// Get the Single Explosion event
			//ERRCHECK(system.lookupID("event:/Explosions/Single Explosion", out explosionID));

			//ERRCHECK(system.getEvent(explosionID, LOADING_MODE.BEGIN_NOW, out explosionDescription));

			// Start loading explosion sample data and keep it in memory
			//ERRCHECK(explosionDescription.loadSampleData());
		}

		private void ERRCHECK(RESULT result)
		{
			if (result != RESULT.OK)
			{
				throw new Exception("FMOD Exception: " + result.ToString()); 
			}
		}

		public void Update()
		{
			ERRCHECK(system.update());
		}

		public void Play_Cancel()
		{
			// Calling start on an instance will cause it to restart if it's already playing
			//ERRCHECK(cancelInstance.start());
		}

		public void Play_LoopingAmbience()
		{
			ERRCHECK(loopingAmbienceInstance.start()); 
		}

		public void Stop_LoopingAmbience()
		{
			ERRCHECK(loopingAmbienceInstance.stop(STOP_MODE.IMMEDIATE));
		}

		public void Play_Explosion()
		{
			/* 
			// One-shot event
			EventInstance eventInstance;
			ERRCHECK(explosionDescription.createInstance(out eventInstance));

			ERRCHECK(eventInstance.start());

			// Release will clean up the instance when it completes
			ERRCHECK(eventInstance.release());
			* /
		}

		public void Dispose()
		{
			//ERRCHECK(weaponsBank.unload());
			//ERRCHECK(menuBank.unload());
			//ERRCHECK(ambienceBank.unload());
			//ERRCHECK(stringsBank.unload());
			ERRCHECK(masterBank.unload());

			ERRCHECK(system.release());
		}
	}
     * */
}
/* 

	void *extraDriverData = NULL;
    Common_Init(&extraDriverData);

    FMOD::Studio::System* system = NULL;
    ERRCHECK( FMOD::Studio::System::create(&system) );
    ERRCHECK( system->initialize(32, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, extraDriverData) );
    
    FMOD::Studio::Bank* masterBank = NULL;
    ERRCHECK( system->loadBankFile(Common_MediaPath("Master Bank.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &masterBank) );
    
    FMOD::Studio::Bank* stringsBank = NULL;
    ERRCHECK( system->loadBankFile(Common_MediaPath("Master Bank.strings.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &stringsBank) );
    
    FMOD::Studio::Bank* ambienceBank = NULL;
    ERRCHECK( system->loadBankFile(Common_MediaPath("Surround_Ambience.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &ambienceBank) );
    
    FMOD::Studio::Bank* menuBank = NULL;
    ERRCHECK( system->loadBankFile(Common_MediaPath("UI_Menu.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &menuBank) );
    
    FMOD::Studio::Bank* weaponsBank = NULL;
    ERRCHECK( system->loadBankFile(Common_MediaPath("Weapons.bank"), FMOD_STUDIO_LOAD_BANK_NORMAL, &weaponsBank) );

    // Get the Looping Ambience event
    FMOD::Studio::ID loopingAmbienceID = {0};
    ERRCHECK( system->lookupID("event:/Ambience/Country", &loopingAmbienceID) );

    FMOD::Studio::EventDescription* loopingAmbienceDescription = NULL;
    ERRCHECK( system->getEvent(&loopingAmbienceID, FMOD_STUDIO_LOAD_BEGIN_NOW, &loopingAmbienceDescription) );
    
    FMOD::Studio::EventInstance* loopingAmbienceInstance = NULL;
    ERRCHECK( loopingAmbienceDescription->createInstance(&loopingAmbienceInstance) );
    
    // Get the 4 Second Surge event
    FMOD::Studio::ID cancelID = {0};
    ERRCHECK( system->lookupID("event:/UI/Cancel", &cancelID) );

    FMOD::Studio::EventDescription* cancelDescription = NULL;
    ERRCHECK( system->getEvent(&cancelID, FMOD_STUDIO_LOAD_BEGIN_NOW, &cancelDescription) );
    
    FMOD::Studio::EventInstance* cancelInstance = NULL;
    ERRCHECK( cancelDescription->createInstance(&cancelInstance) );
    
    // Get the Single Explosion event
    FMOD::Studio::ID explosionID = {0};
    ERRCHECK( system->lookupID("event:/Explosions/Single Explosion", &explosionID) );

    FMOD::Studio::EventDescription* explosionDescription = NULL;
    ERRCHECK( system->getEvent(&explosionID, FMOD_STUDIO_LOAD_BEGIN_NOW, &explosionDescription) );

    // Start loading explosion sample data and keep it in memory
    ERRCHECK( explosionDescription->loadSampleData() );

    do
    {
        Common_Update();
        
        if (Common_BtnPress(BTN_ACTION1))
        {
            // One-shot event
            FMOD::Studio::EventInstance* eventInstance = NULL;
            ERRCHECK( explosionDescription->createInstance(&eventInstance) );

            ERRCHECK( eventInstance->start() );

            // Release will clean up the instance when it completes
            ERRCHECK( eventInstance->release() );
        }
    
        if (Common_BtnPress(BTN_ACTION2))
        {
            ERRCHECK( loopingAmbienceInstance->start() );
        }

        if (Common_BtnPress(BTN_ACTION3))
        {
            ERRCHECK( loopingAmbienceInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE) );
        }

        if (Common_BtnPress(BTN_ACTION4))
        {
            // Calling start on an instance will cause it to restart if it's already playing
            ERRCHECK( cancelInstance->start() );
        }

        ERRCHECK( system->update() );

        Common_Draw("==================================================");
        Common_Draw("Simple Event Example.");
        Common_Draw("Copyright (c) Firelight Technologies 2014-2014.");
        Common_Draw("==================================================");
        Common_Draw("");
        Common_Draw("Press %s to fire and forget the explosion", Common_BtnStr(BTN_ACTION1));
        Common_Draw("Press %s to start the looping ambience", Common_BtnStr(BTN_ACTION2));
        Common_Draw("Press %s to stop the looping ambience", Common_BtnStr(BTN_ACTION3));
        Common_Draw("Press %s to start/restart the cancel sound", Common_BtnStr(BTN_ACTION4));
        Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));

        Common_Sleep(50);
    } while (!Common_BtnPress(BTN_QUIT));
    
    ERRCHECK( weaponsBank->unload() );
    ERRCHECK( menuBank->unload() );
    ERRCHECK( ambienceBank->unload() );
    ERRCHECK( stringsBank->unload() );
    ERRCHECK( masterBank->unload() );

    ERRCHECK( system->release() );

    Common_Close();
*/ 