﻿using FMOD.Studio;
using OpenTK;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;

namespace Rug.Game.Audio
{
    /* 
    public class ChannelGroupDsp : IResourceManager
    {
        FMOD.DSP dsp; 

        public readonly int Index;
        
    }
    */ 
    public class AudioChannelGroup : IResourceManager
    {
        AudioMaster master;
        string path;
        FMOD.ChannelGroup group; 

        public bool Disposed { get; private set; }

        public int Count { get { return 1; } }

        public int Total { get { return Count; } }

        public AudioChannelGroup(AudioMaster master, string path)
        {
            Disposed = true; 
            this.master = master;
            this.path = path;
        }
        
		public void LoadResources()
		{
			if (Disposed == true)
			{
                group = master.GetBusChannelGroup(path);

                //group.getDSP(0, out dsp

                Disposed = false; 
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment(); 
            process.Pop(); 
        }

		public void UnloadResources()
		{
			if (Disposed == false)
			{
                group.release(); 
				Disposed = true; 
			}
		}

        public void Dispose()
        {
            UnloadResources();
        }
    }

	public class AudioMaster : IResourceManager 
	{
		public static AudioMaster Instance; 

		FMOD.Studio.System m_Context;

		Dictionary<string, AudioBank> m_Banks = new Dictionary<string, AudioBank>(); 

		AudioBufferLookup m_Buffers = new AudioBufferLookup("Audio Buffers", ResourceMode.Static);

		private bool m_Disposed;

		internal AudioBufferLookup Buffers { get { return m_Buffers; } }

		public FMOD.Studio.System Context { get { return m_Context; } }

		public float GlobalGain { get; set; }

		public Vector3 Center { get; set; }

        public Vector3 Velocity { get; set; }

        public Vector3 Up { get; set; }

        public Vector3 Forward { get; set; }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

		public AudioMaster()
		{
			GlobalGain = 50f; 

			m_Disposed = true;
		}

		public void RegisterBank(string name, string path)
		{
			m_Banks.Add(name, new AudioBank(name, path, new AudioBankInfo()));
		}

		public void RegisterEvent(string name, string path)
		{
			m_Buffers.Add(new AudioBuffer(name, path, new AudioBufferInfo()));
		}

		public void UpdateListener(Vector3 center)
		{
			Center = center;

            if (m_Disposed == true)
            {
                return; 
            }

            if (m_Context.isValid() == false)
            {
                return; 
            }

            m_Context.setListenerAttributes(0, new _3D_ATTRIBUTES()
            {
                position = new FMOD.VECTOR() { x = Center.X, y = Center.Y, z = Center.Z },
                velocity = new FMOD.VECTOR() { x = Velocity.X, y = Velocity.Y, z = Velocity.Z },
                up = new FMOD.VECTOR() { x = Up.X, y = Up.Y, z = Up.Z },
                forward = new FMOD.VECTOR() { x = Forward.X, y = Forward.Y, z = Forward.Z },
            }); 

			m_Context.update(); 
		}

        public FMOD.ChannelGroup GetBusChannelGroup(string path)
        {
            Bus bus;
            AudioHelper.Check(m_Context.getBus(path, out bus)); // "bus:/All"

            FMOD.ChannelGroup group;
            AudioHelper.Check(bus.getChannelGroup(out group)); // "bus:/All"

            return group; 
            /* 
                ChannelGroup group;
                bus.getChannelGroup(out group);

                DSP dsp; 
                group.getDSP(0, out dsp);

                dsp.
            */ 
        }

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				AudioHelper.Check(FMOD.Studio.System.create(out m_Context));
#if DEBUG
				AudioHelper.Check(m_Context.initialize(32, INITFLAGS.NORMAL | INITFLAGS.LIVEUPDATE, FMOD.INITFLAGS.NORMAL | FMOD.INITFLAGS.PROFILE_ENABLE, IntPtr.Zero));
#else
                AudioHelper.Check(m_Context.initialize(32, INITFLAGS.NORMAL, FMOD.INITFLAGS.NORMAL, IntPtr.Zero));
#endif

				foreach (AudioBank bank in m_Banks.Values)
				{
					bank.LoadResources(); 
				}

				m_Buffers.LoadResources();

				m_Disposed = false; 
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment(); 
            process.Pop(); 
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Buffers.UnloadResources();

				foreach (AudioBank bank in m_Banks.Values)
				{
					bank.UnloadResources();
				}

				m_Context.unloadAll();
				m_Context.release();
			
				m_Disposed = true; 
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources();

			
		}

		#endregion
    }
}
