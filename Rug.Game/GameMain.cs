﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Effect;
using Rug.Game.Displays;
using System;
using System.Windows.Forms;

namespace Rug.Game
{
	public class GameMain : IDisposable
	{
		public readonly DisplayController DisplayController;

		private readonly Clock Clock = new Clock();
		private bool m_CloseNow;
		private float m_FrameAccumulator;
		private int m_FrameCount; 

		public GameMain(string title)
		{
			DisplayController = new DisplayController(title);
			
			LoadConfiguration(); 
		}

		public void Run()
		{
			DisplayController.AssignDisplays(); 

			DisplayController.CreateDefaultForm();

			DisplayController.CreateAdditionalForms();

			DisplayController.ApplySettings();

			InitializeDevice();
			
			//Environment.Form = DisplayController[0];

			foreach (Rug.Game.Displays.GameWindow form in DisplayController)
			{				
				
				form.GLControl.MouseDown += new MouseEventHandler(HandleMouseDown);
				form.GLControl.MouseUp += new MouseEventHandler(HandleMouseUp);
				form.GLControl.MouseClick += new MouseEventHandler(HandleMouseClick);
				form.GLControl.MouseMove += new MouseEventHandler(HandleMouseMove);
				form.GLControl.MouseWheel += new MouseEventHandler(HandleMouseWheel);

				form.GLControl.KeyDown += new KeyEventHandler(HandleKeyDown);
				form.GLControl.KeyUp += new KeyEventHandler(HandleKeyUp);
				form.GLControl.KeyPress += new KeyPressEventHandler(HandleKeyPress);
			}

			DisplayController[0].GLControl.MakeCurrent();

			GLState.ClearColor(Color4.Black);
			GLState.ClearDepth(1.0f);
			GLState.ApplyAll(new Vector2(DisplayController.MainForm.ClientSize.Width, DisplayController.MainForm.ClientSize.Height));

			foreach (Rug.Game.Displays.GameWindow window in DisplayController)
			{
				window.GLControl.MakeCurrent();
				GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);
				window.GLControl.SwapBuffers();
			}			
			
			OnInitialize();

			LoadResources();

			Clock.Start();

			DisplayController[0].GLControl.MakeCurrent();

			MultiSamples multiSamples = DisplaySettings.SampleLevel; 

			RenderLoop.Run(DisplayController.MainForm, () =>
			{
				if (DisplayController.WillApplySettings == true) 
				{
                    // LOAD OPTO HERE
					//UnloadResources();

					DisplayController.TryApplySettings();

					DisplayController[0].GLControl.MakeCurrent();

                    // LOAD OPTO HERE
					//LoadResources(); 

				}

				DisplayController.TryAlignWindows();

				if (DisplayController.WasResized == true || 
					multiSamples != DisplaySettings.SampleLevel)
				{
					multiSamples = DisplaySettings.SampleLevel; 

					DisplayController.ResetWasResized(); 

					OnResize();					
				}

				if (m_CloseNow == true || DisplayController.ShouldClose == true)
				{
					DisplaySettings.AllowClosing = true;

					foreach (Rug.Game.Displays.GameWindow context in DisplayController)
					{
						context.Close();
					}

					return;
				}

				Update();

				Render();
			});

			try
			{
				UnloadResources();
			}
			catch
			{

			}
		}

		private void Update()
		{
			Environment.DrawCalls = Environment.DrawCallsAccumulator;
			Environment.DrawCallsAccumulator = 0;

			Environment.BufferUpdates = Environment.BufferUpdatesAccumulator;
			Environment.BufferUpdatesAccumulator = 0;

            Environment.FrameDelta = Clock.Update();
            Environment.Time += Environment.FrameDelta; 

			m_FrameAccumulator += Environment.FrameDelta;

			++m_FrameCount;

			if (m_FrameAccumulator >= 1.0f)
			{
				Environment.FramesPerSecond = m_FrameCount / m_FrameAccumulator;
				Environment.FramesClick = true;
                m_FrameAccumulator %= 1f;
				m_FrameCount = 0;

			}
			else
			{
				Environment.FramesClick = false;
			}

			OnUpdate();
		}

		private void Render()
		{		
			OnRenderBegin();

			OnRender();

			OnRenderEnd();

            if (Environment.LimitFrameRate == true)
            {
                Clock.SpinTill(0, 1f / 60f);
            }
		}		

		private void InitializeDevice()
		{

		}

		private void LoadResources()
		{
			RC.WriteLine(ConsoleThemeColor.PromptColor2, "LoadResources()");

			SharedEffects.LoadResources();

			DisplayController.LoadResources();

			Environment.Resources.LoadResources();

            if (DisplayController.Count == 1)
            {
                DisplayController.TryAlignWindows();

                if (DisplayController.WasResized == true)
                {
                    DisplayController.ResetWasResized();

                    OnResize();
                }
                else
                {
                    LoadResolutionDependentResources();
                }
            }
            else
            {
                LoadResolutionDependentResources();
            }

			OnResourceLoad();
		}

		protected virtual void OnResourceLoad() { }

		private void UnloadResources()
		{
			RC.WriteLine(ConsoleThemeColor.PromptColor2, "UnloadResources()");

			OnResourceUnload();

			Environment.Resources.UnloadResources();

			SharedEffects.UnloadResources();
			UnloadResolutionDependentResources();

			DisplayController.UnloadResources();
		}

		protected virtual void OnResourceUnload() { }

		private void LoadResolutionDependentResources()
		{
			RC.WriteLine(ConsoleThemeColor.PromptColor2, "LoadResolutionDependentResources()");

			OnLoadResolutionDependentResources();

			Environment.ResolutionDependentResources.LoadResources();
		}

		protected virtual void OnLoadResolutionDependentResources() { }

		private void UnloadResolutionDependentResources()
		{
			RC.WriteLine(ConsoleThemeColor.PromptColor2, "UnloadResolutionDependentResources()");

            if (Environment.ResolutionDependentResources.IsLoaded == true)
            {
                Environment.ResolutionDependentResources.UnloadResources();
            }

			OnUnloadResolutionDependentResources();
		}

		protected virtual void OnUnloadResolutionDependentResources() { }

		protected virtual void LoadConfiguration()
		{
			
		}

		protected virtual void OnInitialize()
		{
			
		}

		protected virtual void OnResize()
		{
			UnloadResolutionDependentResources();

			LoadResolutionDependentResources(); 
		}

		protected virtual void OnDispose()
		{
			
		}

		protected virtual void OnUpdate()
		{
			
		}

		protected virtual void OnRenderBegin()
		{
			
		}

		protected virtual void OnRender()
		{
			
		}

		protected virtual void OnRenderEnd()
		{
			
		}

		protected virtual void HandleKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{

		}

		protected virtual void HandleKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{

		}

		protected virtual void HandleKeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{

		}

		protected virtual void HandleMouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
		{

		}

		protected virtual void HandleMouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
		{

		}

		protected virtual void HandleMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{

		}

		protected virtual void HandleMouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{

		}

		protected virtual void HandleMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{

		}

		#region IDisposable Members


		public void Quit()
		{
			if (OnQuit() == true)
			{
				m_CloseNow = true;
			}
		}

		protected virtual bool OnQuit()
		{
			return true; 
		}

		/// <summary>
		/// Disposes of object resources.
		/// </summary>
		public void Dispose()
		{
			OnDispose();

			Dispose(true);

			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Disposes of object resources.
		/// </summary>
		/// <param name="disposeManagedResources">If true, managed resources should be
		/// disposed of in addition to unmanaged resources.</param>
		protected virtual void Dispose(bool disposeManagedResources)
		{
			if (disposeManagedResources)
			{
				SharedEffects.Dispose();

				if (DisplayController != null)
				{
					DisplayController.Dispose();
				}
			}
		}

		#endregion
	}
}
