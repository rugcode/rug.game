﻿using Rug.Game.Core.Text;
using Rug.Game.Displays;
using Rug.Game.Ui.Dialogs;
using Rug.Game.Ui.Dialogs.ColorPicker;
using Rug.Game.Ui.Menus;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game.Ui.Simple
{
	public class SplashScreenBase : UiScene
	{
        public DebugOverlay DebugOverlay { get; private set; } 
		private ResolutionTester m_ResolutionTester;
		private DisplayController m_DisplayController; 

		public readonly VisiblePanelController Controller = new VisiblePanelController();
		public readonly List<PanelBase> Panels = new List<PanelBase>();
        
        public MenuBar MenuBar { get; private set; } 

		public SplashScreenBase(DisplayController displayController)
		{
			int index = 1;

			m_DisplayController = displayController; 

			Controller.Dialogs.ColorPickerDialog = new ColorPickerDialog();

			Controller.Dialogs.ColorPickerDialog.Size = new System.Drawing.Size(400, 400);
			Controller.Dialogs.ColorPickerDialog.RelitiveZIndex = 700;
			Controller.Dialogs.ColorPickerDialog.Title = "Select Color";

			this.Controls.Add(Controller.Dialogs.ColorPickerDialog);

			Controller.Dialogs.ColorPickerDialog.InitializeControls();
			Controller.Dialogs.ColorPickerDialog.Opening += new EventHandler(Dialog_Opening);
			Controller.Dialogs.ColorPickerDialog.Closing += new DialogResultEventHandler(Dialog_Closing);

			CreatePanels(ref index);

			CreateMenu();

			DebugOverlay = CreateDebugOverlay();
			DebugOverlay.Docking = System.Windows.Forms.DockStyle.Bottom;
			DebugOverlay.RelitiveZIndex = 800;
			DebugOverlay.IsVisible = true;
			this.Controls.Add(DebugOverlay);
			DebugOverlay.InitializeControls();

			foreach (PanelBase panel in Panels)
			{
				this.Controls.Add(panel);

				panel.Initiate();

				panel.IsVisible = false;
			}

			Resize(m_DisplayController[0].ClientSize);
			Initialize();
			Invalidate();
			Align();
			IsVisible = true;
		}

		private void CreateMenu()
		{
			#region Menu Bar

			int index = 100;

            MenuBar = new MenuBar();

            MenuBar.IsVisible = true;
            MenuBar.Docking = System.Windows.Forms.DockStyle.Top;
            MenuBar.Size = new System.Drawing.Size(30, 30);
            MenuBar.RelitiveZIndex = index;
            Controls.Add(MenuBar);

            MenuBarItem file = MenuBar.AddItem("File");
			file.IsVisible = true;
			file.FontType = FontType.Regular;
			file.Hover += new EventHandler(OnMenuHover);
			file.Open += new EventHandler(OnMenuOpen);

			CreateFileMenu(file);

			foreach (PanelBase panel in Panels)
			{
                panel.AddMenuItems(MenuBar);
			}

            CreateAdditionalMenus(MenuBar);

			{
				MenuItem exit = file.AddItem("Exit");
				exit.IsVisible = true;
				exit.FontType = FontType.Regular;

				exit.Click += new System.EventHandler(exit_Click);
				exit.Hover += new EventHandler(OnMenuHover);
			}

			{
                MenuBarItem setup = MenuBar.AddItem("Setup");
				setup.IsVisible = true;
				setup.FontType = FontType.Regular;
				setup.Open += new EventHandler(OnMenuOpen);
				setup.Hover += new EventHandler(OnMenuHover);

				MenuItem display = setup.AddItem("Display");
				display.IsVisible = true;
				display.FontType = FontType.Regular;

				display.Click += new System.EventHandler(display_Click);
				display.Hover += new EventHandler(OnMenuHover);
			}

			#endregion
		}

		private void display_Click(object sender, System.EventArgs e)
		{
			PlayClick(); 

			if (m_ResolutionTester != null)
			{
				m_ResolutionTester.Dispose();
				m_ResolutionTester = null; 
			}

			m_ResolutionTester = new ResolutionTester(m_DisplayController);

			m_ResolutionTester.Show(); 
		}

		private void exit_Click(object sender, System.EventArgs e)
		{
			OnExitClicked();
		}

		public override void Resize(Size size)
		{
			base.Resize(size);

			Align();
		}

		public void Align()
		{
			foreach (PanelBase panel in Panels)
			{
				panel.Align();
			}
		}

		public override void Update()
		{
			base.Update();

			DebugOverlay.Update();
		}

		public override void Dispose()
		{
			 base.Dispose();

			 if (m_ResolutionTester != null)
			 {
				 m_ResolutionTester.Dispose();
				 m_ResolutionTester = null;
			 }
		}


		void Dialog_Closing(object sender, DialogResultEventArgs args)
		{
			PlayClose();

			UiDialog dialog = (sender as UiDialog);

			if (dialog.IsVisible == false)
			{
				if (OpenDialogs.Peek() != dialog)
				{
					throw new Exception("Dialog '" + dialog.Title + "' is not ontop of the dialog stack");
				}

				OpenDialogs.Pop();
			}
		}

		void Dialog_Opening(object sender, EventArgs e)
		{
			PlayOpen();

			UiDialog dialog = (sender as UiDialog);

			if (dialog.IsVisible == true)
			{
				if (OpenDialogs.Contains(dialog))
				{
					throw new Exception("Dialog '" + dialog.Title + "' is already in the dialog stack");
				}

				OpenDialogs.Push(sender as UiDialog);
			}
		}



		protected virtual void CreatePanels(ref int index) { }

		protected virtual DebugOverlay CreateDebugOverlay()
		{
			return new BasicDebugOverlay();
		}

		protected virtual void CreateFileMenu(MenuBarItem menu) { }

		protected virtual void CreateAdditionalMenus(MenuBar menu) { }

		protected virtual void OnMenuOpen(object sender, EventArgs e) { }

		protected virtual void OnMenuHover(object sender, EventArgs e) { }

		protected virtual void PlayClick() { }

		protected virtual void PlayOpen() { }

		protected virtual void PlayClose() { }

		protected virtual void OnExitClicked() { }
	}
}
