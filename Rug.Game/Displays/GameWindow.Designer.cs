﻿namespace Rug.Game.Displays
{
	partial class GameWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_GLControl = new Rug.Game.Displays.GameGLControl();
			this.SuspendLayout();
			// 
			// m_GLControl
			// 
			this.m_GLControl.BackColor = System.Drawing.Color.Black;
			this.m_GLControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_GLControl.Location = new System.Drawing.Point(0, 0);
			this.m_GLControl.Name = "m_GLControl";
			this.m_GLControl.Size = new System.Drawing.Size(600, 503);
			this.m_GLControl.TabIndex = 0;
			this.m_GLControl.VSync = true;
			// 
			// GameWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(600, 503);
			this.Controls.Add(this.m_GLControl);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GameWindow";
			this.Text = "Game Window";
			this.Load += new System.EventHandler(this.GameWindow_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private GameGLControl m_GLControl;
	}
}