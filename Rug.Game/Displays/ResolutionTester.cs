﻿using OpenTK;
using Rug.Cmd;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace Rug.Game.Displays
{
	public partial class ResolutionTester : Form
	{
		private static string m_OptionsFileName = "~/DisplaySettings.xml";

		private DisplayController m_Controller;

		public ResolutionTester(DisplayController controller)
		{
			m_Controller = controller; 

			InitializeComponent();
		}

		private void ResolutionTester_Load(object sender, EventArgs e)
		{
			m_DisplaySettingsGroup.Enabled = false; 

			m_WindowMode.Items.Add(GameWindowMode.Fullscreen); 
			m_WindowMode.Items.Add(GameWindowMode.Windowed); 
			m_WindowMode.Items.Add(GameWindowMode.SizableWindow); 
			m_WindowMode.Items.Add(GameWindowMode.BorderlessWindow);

			m_MultiSamples.Items.Add(MultiSamples.X1);
			m_MultiSamples.Items.Add(MultiSamples.X2);
			m_MultiSamples.Items.Add(MultiSamples.X4);
			m_MultiSamples.Items.Add(MultiSamples.X8);
			m_MultiSamples.Items.Add(MultiSamples.X16);

			LoadGlobalSettings();

			LoadWindowList(); 
		}

		private void LoadGlobalSettings()
		{
			m_WindowMode.SelectedIndex = (int)DisplaySettings.WindowMode - 1;
			m_DesiredDisplays.Value = DisplaySettings.DesiredWindowCount;
			m_AllwaysOnTop.Checked = DisplaySettings.AllwaysOnTop;
			m_LowestCommonDenominator.Checked = DisplaySettings.LowestCommonDenominator;
			m_StickToDisplayResolutions.Checked = DisplaySettings.StickToDisplayResolutions;
			m_AllowClosing.Checked = DisplaySettings.AllowClosing;

			m_MultiSamples.SelectedIndex = (int)DisplaySettings.SampleLevel;
		}
		
		private void StoreGlobalSettings()
		{
			/* 
			DisplaySettings.WindowMode = (GameWindowMode)m_WindowMode.SelectedIndex + 1;
			DisplaySettings.DesiredWindowCount = (int)m_DesiredDisplays.Value;
			DisplaySettings.AllwaysOnTop = m_AllwaysOnTop.Checked;
			DisplaySettings.LowestCommonDenominator = m_LowestCommonDenominator.Checked;
			DisplaySettings.StickToDisplayResolutions = m_StickToDisplayResolutions.Checked; 	
			*/ 
		}

		private void LoadWindowList()
		{
			m_DisplayList.SuspendLayout(); 

			int index = m_DisplayList.SelectedIndex; 
			m_DisplayList.Items.Clear(); 

			foreach (GameDisplay display in DisplaySettings.Windows)
			{
				m_DisplayList.Items.Add(display); 
			}

			if (index >= 0 && index < m_DisplayList.Items.Count)
			{
				m_DisplayList.SelectedIndex = index;
			}

			m_DisplayList.ResumeLayout(true); 
		}

		private void StoreWindowList()
		{
			m_Controller.SaveWindowStates();

			LoadWindowList();
		}

		private void DisplayList_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = m_DisplayList.SelectedIndex;

			m_FixedResolutionSelector.Items.Clear();

			if (index >= 0 && index < DisplaySettings.Windows.Count)
			{
				m_DisplaySettingsGroup.Enabled = true; 

				Rectangle bounds = DisplaySettings.Windows[index].Bounds;
				int displayIndex = DisplaySettings.Windows[index].DisplayIndex;

				m_DisplayIndex.Value = displayIndex;
				m_ResolutionX.Value = bounds.Width;
				m_ResolutionY.Value = bounds.Height;

				bool found = false;
				int foundIndex = 0;

                if (displayIndex >= 0 && displayIndex < DisplaySettings.DisplayDevices.Count)
				{
                    DisplayDeviceProxy device = DisplaySettings.DisplayDevices[displayIndex];

					int resIndex = 0;

                    foreach (DisplayResolution resolution in DisplayController.GetPossibleResolutions(new DisplayDeviceProxy[] { device }))
					{
						m_FixedResolutionSelector.Items.Add(resolution);

						if (resolution.Width == bounds.Width &&
							resolution.Height == bounds.Height)
						{
							found = true;

							foundIndex = resIndex;
						}

						resIndex++;
					}

					m_FixedResolutionSelector.SelectedIndex = foundIndex;
				}

				m_FixedResolution.Checked = found;
				m_FixedResolutionSelector.Enabled = found; 

				m_ResolutionX.Enabled = !found;
				m_ResolutionY.Enabled = !found; 
			}
			else
			{
				m_DisplaySettingsGroup.Enabled = false; 
			}
		}

		private void MultiSamples_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void FixedResolution_CheckedChanged(object sender, EventArgs e)
		{
			m_FixedResolutionSelector.Enabled = m_FixedResolution.Checked;
		}

		private void Arbitrary_CheckedChanged(object sender, EventArgs e)
		{
			m_ResolutionX.Enabled = m_Arbitrary.Checked;
			m_ResolutionY.Enabled = m_Arbitrary.Checked;
		}

		private void Apply_Click(object sender, EventArgs e)
		{
			DisplaySettings.WindowMode = (GameWindowMode)m_WindowMode.SelectedIndex + 1; 
			DisplaySettings.DesiredWindowCount = (int)m_DesiredDisplays.Value;
			DisplaySettings.AllwaysOnTop = m_AllwaysOnTop.Checked;
			DisplaySettings.LowestCommonDenominator = m_LowestCommonDenominator.Checked;
			DisplaySettings.StickToDisplayResolutions = m_StickToDisplayResolutions.Checked;
			DisplaySettings.AllowClosing = m_AllowClosing.Checked;

			DisplaySettings.SampleLevel = (MultiSamples)m_MultiSamples.SelectedIndex; 

			m_Controller.ShouldApplySettings(); 
		}

		private void Save_Click(object sender, EventArgs e)
		{
			SaveSettings();
		}

		private void Load_Click(object sender, EventArgs e)
		{
			LoadSettings(); 
		}

		private void Add_Click(object sender, EventArgs e)
		{
			GameDisplay display = new GameDisplay() { Bounds = new Rectangle(Point.Empty, DisplaySettings.DefaultWindowSize), DisplayIndex = -1 };

			DisplaySettings.Windows.Add(display);

			m_DisplayList.Items.Add(display); 
		}

		private void Remove_Click(object sender, EventArgs e)
		{
			int index = m_DisplayList.SelectedIndex;
			
			if (index >= 0 && index < DisplaySettings.Windows.Count)
			{
				DisplaySettings.Windows.RemoveAt(index);

				m_DisplayList.Items.RemoveAt(index); 
			}
		}

		private void Scan_Click(object sender, EventArgs e)
		{
			LoadGlobalSettings();

			LoadWindowList(); 
		}

		private void Store_Click(object sender, EventArgs e)
		{
			StoreGlobalSettings(); 

			StoreWindowList(); 
		}

		private void DisplayIndex_ValueChanged(object sender, EventArgs e)
		{
			GameDisplay display = DisplaySettings.Windows[m_DisplayList.SelectedIndex];

			display.DisplayIndex = (int)m_DisplayIndex.Value;

			m_DisplayList.Refresh(); 
		}

		private void FixedResolutionSelector_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (m_FixedResolution.Checked == true)
			{
				GameDisplay display = DisplaySettings.Windows[m_DisplayList.SelectedIndex];

				Rectangle bounds = display.Bounds; 

				DisplayResolution desiredRes = m_FixedResolutionSelector.SelectedItem as DisplayResolution;

				display.Bounds = new Rectangle(bounds.Location, new Size(desiredRes.Width, desiredRes.Height));

				m_DisplayList.Refresh(); 
			}
		}

		private void ResolutionX_ValueChanged(object sender, EventArgs e)
		{
			if (m_FixedResolution.Checked == false)
			{
				GameDisplay display = DisplaySettings.Windows[m_DisplayList.SelectedIndex];

				Rectangle bounds = display.Bounds;

				display.Bounds = new Rectangle(bounds.Location, new Size((int)m_ResolutionX.Value, bounds.Height));

				m_DisplayList.Refresh(); 
			}
		}

		private void ResolutionY_ValueChanged(object sender, EventArgs e)
		{
			if (m_FixedResolution.Checked == false)
			{
				GameDisplay display = DisplaySettings.Windows[m_DisplayList.SelectedIndex];

				Rectangle bounds = display.Bounds;

				display.Bounds = new Rectangle(bounds.Location, new Size(bounds.Width, (int)m_ResolutionY.Value));

				m_DisplayList.Refresh(); 
			}
		}

		private void LoadSettings()
		{
			RC.Clear(); 

			try
			{
				XmlDocument doc = new XmlDocument();

				RC.WriteLine(ConsoleThemeColor.TextGood, "Loading display settings");
				RC.WriteLine(ConsoleThemeColor.SubTextGood,Helper.ResolvePath(m_OptionsFileName));

				// load the options from the resolved path
				doc.Load(Helper.ResolvePath(m_OptionsFileName));

				XmlNode node = doc.DocumentElement;

				// if the node is not null 
				if (node != null)
				{
					DisplaySettings.Windows.Clear(); 

					DisplaySettings.Load(node);
				}
			}
			catch (Exception ex)
			{
				// somthing went wrong, tell the user
				MessageBox.Show(ex.Message, "Could not load options");
			}

			LoadGlobalSettings();

			LoadWindowList();

			Apply_Click(this, EventArgs.Empty);
		}

		public void SaveSettings()
		{
			StoreGlobalSettings();

			StoreWindowList(); 

			try
			{
				XmlDocument doc = new XmlDocument();

				XmlElement node = Helper.CreateElement(doc, "Options");

				DisplaySettings.Save(node);

				doc.AppendChild(node);

				Helper.EnsurePathExists(Helper.ResolvePath(m_OptionsFileName));

				doc.Save(Helper.ResolvePath(m_OptionsFileName));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Could not save options");
			}
		}
	}
}
