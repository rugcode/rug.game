﻿﻿// Copyright (c) 2010-2011 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#if !WIN8
using System;
using System.Threading;
using System.Windows.Forms;


namespace Rug.Game.Displays
{
	/// <summary>
	/// RenderLoop provides a rendering loop infrastructure.
	/// </summary>
	internal class RenderLoop
	{
		private RenderLoop()
		{
		}

		/// <summary>
		/// Delegate for the rendering loop.
		/// </summary>
		public delegate void RenderCallback();

		/// <summary>
		/// Runs the specified main loop in the specified context.
		/// </summary>
		public static void Run(ApplicationContext context, RenderCallback renderCallback)
		{
            RandomiseFrameRate = false; 

			Run(context.MainForm, renderCallback);
		}

		/// <summary>
		/// Runs the specified main loop for the specified windows form.
		/// </summary>
		/// <param name="form">The form.</param>
		/// <param name="renderCallback">The rendering callback.</param>
		public static void Run(Control form, RenderCallback renderCallback)
		{
            RandomiseFrameRate = false; 

			var proxyWindow = new ProxyNativeWindow(form);
			proxyWindow.Run(renderCallback);
		}

		/// <summary>
		/// Gets or sets a value indicating whether the render loop should use a custom windows event handler (default false).
		/// </summary>
		/// <value>
		///   <c>true</c> if the render loop should use a custom windows event handler (default false); otherwise, <c>false</c>.
		/// </value>
		/// <remarks>
		/// By default, RenderLoop is using <see cref="Application.DoEvents"/> to process windows event message. Set this parameter to true to use a custom event handler that could
		/// lead to better performance. Note that using a custom windows event message handler is not compatible with <see cref="Application.AddMessageFilter"/> or any other features
		/// that are part of <see cref="Application"/>.
		/// </remarks>
		public static bool UseCustomDoEvents { get; set; }

        public static bool LimitFrameRate { get; set; }

        public static bool RandomiseFrameRate { get; set; }

        private static Random m_Random = new Random(); 

		/// <summary>
		/// ProxyNativeWindow, used only to detect if the original window is destroyed
		/// </summary>
		private class ProxyNativeWindow //: IMessageFilter
		{
			private readonly Control _form;
			private readonly IntPtr _windowHandle;
			private bool _isAlive;

			/// <summary>
			/// Initializes a new instance of the <see cref="ProxyNativeWindow"/> class.
			/// </summary>
			public ProxyNativeWindow(Control form)
			{
				_form = form;
				_windowHandle = form.Handle;
				_form.Disposed += _form_Disposed;
				_isAlive = true;
			}

			void _form_Disposed(object sender, EventArgs e)
			{
				_isAlive = false;
			}

			/// <summary>
			/// Private rendering loop
			/// </summary>
			public void Run(RenderCallback renderCallback)
			{
				// Show the form
				_form.Show();

				// Main rendering loop);
				while (_isAlive)
				{
					// Revert back to Application.DoEvents in order to support Application.AddMessageFilter
					// Seems that DoEvents is compatible with Mono unlike Application.Run that was not running
					// correctly.
					Application.DoEvents();

					if (_isAlive)
					{
						renderCallback();

                        if (RandomiseFrameRate == true)
                        {
                            Thread.CurrentThread.Join(m_Random.Next(100));
                        }
                        else if (Environment.LimitFrameRate == true)
                        {
                            // this is temp (BUT FIXES ALMOST EVERYTHING!)
                            //Thread.CurrentThread.Join(1);
                        }
					}
				}

				_form.Disposed -= _form_Disposed;
			}
		}
	}
}
#endif