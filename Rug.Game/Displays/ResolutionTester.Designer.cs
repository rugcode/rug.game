﻿namespace Rug.Game.Displays
{
	partial class ResolutionTester
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_DisplaySettingsGroup = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.m_Arbitrary = new System.Windows.Forms.RadioButton();
			this.m_FixedResolution = new System.Windows.Forms.RadioButton();
			this.m_ResolutionX = new System.Windows.Forms.NumericUpDown();
			this.m_ResolutionY = new System.Windows.Forms.NumericUpDown();
			this.m_FixedResolutionSelector = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.m_DisplayIndex = new System.Windows.Forms.NumericUpDown();
			this.m_Apply = new System.Windows.Forms.Button();
			this.m_Save = new System.Windows.Forms.Button();
			this.m_Load = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.m_AllowClosing = new System.Windows.Forms.CheckBox();
			this.m_WindowMode = new System.Windows.Forms.ComboBox();
			this.m_AllwaysOnTop = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.m_LowestCommonDenominator = new System.Windows.Forms.CheckBox();
			this.m_StickToDisplayResolutions = new System.Windows.Forms.CheckBox();
			this.m_DesiredDisplays = new System.Windows.Forms.NumericUpDown();
			this.m_Add = new System.Windows.Forms.Button();
			this.m_Remove = new System.Windows.Forms.Button();
			this.m_Scan = new System.Windows.Forms.Button();
			this.m_Store = new System.Windows.Forms.Button();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.m_DisplayList = new Rug.Game.Controls.RefreshingListBox();
			this.label4 = new System.Windows.Forms.Label();
			this.m_MultiSamples = new System.Windows.Forms.ComboBox();
			this.m_DisplaySettingsGroup.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_ResolutionX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.m_ResolutionY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.m_DisplayIndex)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_DesiredDisplays)).BeginInit();
			this.SuspendLayout();
			// 
			// m_DisplaySettingsGroup
			// 
			this.m_DisplaySettingsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_DisplaySettingsGroup.Controls.Add(this.label3);
			this.m_DisplaySettingsGroup.Controls.Add(this.m_Arbitrary);
			this.m_DisplaySettingsGroup.Controls.Add(this.m_FixedResolution);
			this.m_DisplaySettingsGroup.Controls.Add(this.m_ResolutionX);
			this.m_DisplaySettingsGroup.Controls.Add(this.m_ResolutionY);
			this.m_DisplaySettingsGroup.Controls.Add(this.m_FixedResolutionSelector);
			this.m_DisplaySettingsGroup.Controls.Add(this.label2);
			this.m_DisplaySettingsGroup.Controls.Add(this.m_DisplayIndex);
			this.m_DisplaySettingsGroup.Location = new System.Drawing.Point(336, 211);
			this.m_DisplaySettingsGroup.Name = "m_DisplaySettingsGroup";
			this.m_DisplaySettingsGroup.Size = new System.Drawing.Size(356, 100);
			this.m_DisplaySettingsGroup.TabIndex = 1;
			this.m_DisplaySettingsGroup.TabStop = false;
			this.m_DisplaySettingsGroup.Text = "Display Settings";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(217, 77);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(12, 13);
			this.label3.TabIndex = 12;
			this.label3.Text = "x";
			// 
			// m_Arbitrary
			// 
			this.m_Arbitrary.AutoSize = true;
			this.m_Arbitrary.Location = new System.Drawing.Point(7, 75);
			this.m_Arbitrary.Name = "m_Arbitrary";
			this.m_Arbitrary.Size = new System.Drawing.Size(63, 17);
			this.m_Arbitrary.TabIndex = 11;
			this.m_Arbitrary.TabStop = true;
			this.m_Arbitrary.Text = "Arbitrary";
			this.m_Arbitrary.UseVisualStyleBackColor = true;
			this.m_Arbitrary.CheckedChanged += new System.EventHandler(this.Arbitrary_CheckedChanged);
			// 
			// m_FixedResolution
			// 
			this.m_FixedResolution.AutoSize = true;
			this.m_FixedResolution.Location = new System.Drawing.Point(7, 49);
			this.m_FixedResolution.Name = "m_FixedResolution";
			this.m_FixedResolution.Size = new System.Drawing.Size(50, 17);
			this.m_FixedResolution.TabIndex = 10;
			this.m_FixedResolution.TabStop = true;
			this.m_FixedResolution.Text = "Fixed";
			this.m_FixedResolution.UseVisualStyleBackColor = true;
			this.m_FixedResolution.CheckedChanged += new System.EventHandler(this.FixedResolution_CheckedChanged);
			// 
			// m_ResolutionX
			// 
			this.m_ResolutionX.Location = new System.Drawing.Point(100, 72);
			this.m_ResolutionX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.m_ResolutionX.Name = "m_ResolutionX";
			this.m_ResolutionX.Size = new System.Drawing.Size(111, 20);
			this.m_ResolutionX.TabIndex = 9;
			this.m_ResolutionX.ValueChanged += new System.EventHandler(this.ResolutionX_ValueChanged);
			// 
			// m_ResolutionY
			// 
			this.m_ResolutionY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_ResolutionY.Location = new System.Drawing.Point(239, 72);
			this.m_ResolutionY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.m_ResolutionY.Name = "m_ResolutionY";
			this.m_ResolutionY.Size = new System.Drawing.Size(111, 20);
			this.m_ResolutionY.TabIndex = 8;
			this.m_ResolutionY.ValueChanged += new System.EventHandler(this.ResolutionY_ValueChanged);
			// 
			// m_FixedResolutionSelector
			// 
			this.m_FixedResolutionSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_FixedResolutionSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_FixedResolutionSelector.FormattingEnabled = true;
			this.m_FixedResolutionSelector.Location = new System.Drawing.Point(101, 45);
			this.m_FixedResolutionSelector.Name = "m_FixedResolutionSelector";
			this.m_FixedResolutionSelector.Size = new System.Drawing.Size(249, 21);
			this.m_FixedResolutionSelector.TabIndex = 6;
			this.m_FixedResolutionSelector.SelectedIndexChanged += new System.EventHandler(this.FixedResolutionSelector_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 21);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(69, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Display index";
			// 
			// m_DisplayIndex
			// 
			this.m_DisplayIndex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_DisplayIndex.Location = new System.Drawing.Point(101, 19);
			this.m_DisplayIndex.Maximum = new decimal(new int[] {
            65,
            0,
            0,
            0});
			this.m_DisplayIndex.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.m_DisplayIndex.Name = "m_DisplayIndex";
			this.m_DisplayIndex.Size = new System.Drawing.Size(249, 20);
			this.m_DisplayIndex.TabIndex = 4;
			this.m_DisplayIndex.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.m_DisplayIndex.ValueChanged += new System.EventHandler(this.DisplayIndex_ValueChanged);
			// 
			// m_Apply
			// 
			this.m_Apply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Apply.Location = new System.Drawing.Point(455, 317);
			this.m_Apply.Name = "m_Apply";
			this.m_Apply.Size = new System.Drawing.Size(75, 23);
			this.m_Apply.TabIndex = 2;
			this.m_Apply.Text = "Apply";
			this.m_Apply.UseVisualStyleBackColor = true;
			this.m_Apply.Click += new System.EventHandler(this.Apply_Click);
			// 
			// m_Save
			// 
			this.m_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Save.Location = new System.Drawing.Point(536, 317);
			this.m_Save.Name = "m_Save";
			this.m_Save.Size = new System.Drawing.Size(75, 23);
			this.m_Save.TabIndex = 3;
			this.m_Save.Text = "Save";
			this.m_Save.UseVisualStyleBackColor = true;
			this.m_Save.Click += new System.EventHandler(this.Save_Click);
			// 
			// m_Load
			// 
			this.m_Load.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Load.Location = new System.Drawing.Point(617, 317);
			this.m_Load.Name = "m_Load";
			this.m_Load.Size = new System.Drawing.Size(75, 23);
			this.m_Load.TabIndex = 4;
			this.m_Load.Text = "Load";
			this.m_Load.UseVisualStyleBackColor = true;
			this.m_Load.Click += new System.EventHandler(this.Load_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.m_MultiSamples);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.m_AllowClosing);
			this.groupBox2.Controls.Add(this.m_WindowMode);
			this.groupBox2.Controls.Add(this.m_AllwaysOnTop);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.m_LowestCommonDenominator);
			this.groupBox2.Controls.Add(this.m_StickToDisplayResolutions);
			this.groupBox2.Controls.Add(this.m_DesiredDisplays);
			this.groupBox2.Location = new System.Drawing.Point(336, 13);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(356, 192);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Global Settings";
			// 
			// m_AllowClosing
			// 
			this.m_AllowClosing.AutoSize = true;
			this.m_AllowClosing.Location = new System.Drawing.Point(6, 169);
			this.m_AllowClosing.Name = "m_AllowClosing";
			this.m_AllowClosing.Size = new System.Drawing.Size(79, 17);
			this.m_AllowClosing.TabIndex = 6;
			this.m_AllowClosing.Text = "Allow close";
			this.m_AllowClosing.UseVisualStyleBackColor = true;
			// 
			// m_WindowMode
			// 
			this.m_WindowMode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_WindowMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_WindowMode.FormattingEnabled = true;
			this.m_WindowMode.Location = new System.Drawing.Point(6, 19);
			this.m_WindowMode.Name = "m_WindowMode";
			this.m_WindowMode.Size = new System.Drawing.Size(344, 21);
			this.m_WindowMode.TabIndex = 5;
			// 
			// m_AllwaysOnTop
			// 
			this.m_AllwaysOnTop.AutoSize = true;
			this.m_AllwaysOnTop.Location = new System.Drawing.Point(6, 146);
			this.m_AllwaysOnTop.Name = "m_AllwaysOnTop";
			this.m_AllwaysOnTop.Size = new System.Drawing.Size(94, 17);
			this.m_AllwaysOnTop.TabIndex = 4;
			this.m_AllwaysOnTop.Text = "Allways on top";
			this.m_AllwaysOnTop.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 48);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(83, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Desired displays";
			// 
			// m_LowestCommonDenominator
			// 
			this.m_LowestCommonDenominator.AutoSize = true;
			this.m_LowestCommonDenominator.Location = new System.Drawing.Point(6, 123);
			this.m_LowestCommonDenominator.Name = "m_LowestCommonDenominator";
			this.m_LowestCommonDenominator.Size = new System.Drawing.Size(164, 17);
			this.m_LowestCommonDenominator.TabIndex = 2;
			this.m_LowestCommonDenominator.Text = "Lowest common denominator";
			this.m_LowestCommonDenominator.UseVisualStyleBackColor = true;
			// 
			// m_StickToDisplayResolutions
			// 
			this.m_StickToDisplayResolutions.AutoSize = true;
			this.m_StickToDisplayResolutions.Location = new System.Drawing.Point(6, 100);
			this.m_StickToDisplayResolutions.Name = "m_StickToDisplayResolutions";
			this.m_StickToDisplayResolutions.Size = new System.Drawing.Size(150, 17);
			this.m_StickToDisplayResolutions.TabIndex = 1;
			this.m_StickToDisplayResolutions.Text = "Stick to display resolutions";
			this.m_StickToDisplayResolutions.UseVisualStyleBackColor = true;
			// 
			// m_DesiredDisplays
			// 
			this.m_DesiredDisplays.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_DesiredDisplays.Location = new System.Drawing.Point(100, 46);
			this.m_DesiredDisplays.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
			this.m_DesiredDisplays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.m_DesiredDisplays.Name = "m_DesiredDisplays";
			this.m_DesiredDisplays.Size = new System.Drawing.Size(250, 20);
			this.m_DesiredDisplays.TabIndex = 0;
			this.m_DesiredDisplays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// m_Add
			// 
			this.m_Add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.m_Add.Location = new System.Drawing.Point(12, 317);
			this.m_Add.Name = "m_Add";
			this.m_Add.Size = new System.Drawing.Size(75, 23);
			this.m_Add.TabIndex = 6;
			this.m_Add.Text = "Add";
			this.m_Add.UseVisualStyleBackColor = true;
			this.m_Add.Click += new System.EventHandler(this.Add_Click);
			// 
			// m_Remove
			// 
			this.m_Remove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.m_Remove.Location = new System.Drawing.Point(93, 317);
			this.m_Remove.Name = "m_Remove";
			this.m_Remove.Size = new System.Drawing.Size(75, 23);
			this.m_Remove.TabIndex = 7;
			this.m_Remove.Text = "Remove";
			this.m_Remove.UseVisualStyleBackColor = true;
			this.m_Remove.Click += new System.EventHandler(this.Remove_Click);
			// 
			// m_Scan
			// 
			this.m_Scan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.m_Scan.Location = new System.Drawing.Point(174, 317);
			this.m_Scan.Name = "m_Scan";
			this.m_Scan.Size = new System.Drawing.Size(75, 23);
			this.m_Scan.TabIndex = 8;
			this.m_Scan.Text = "Scan";
			this.m_Scan.UseVisualStyleBackColor = true;
			this.m_Scan.Click += new System.EventHandler(this.Scan_Click);
			// 
			// m_Store
			// 
			this.m_Store.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.m_Store.Location = new System.Drawing.Point(255, 317);
			this.m_Store.Name = "m_Store";
			this.m_Store.Size = new System.Drawing.Size(75, 23);
			this.m_Store.TabIndex = 9;
			this.m_Store.Text = "Store";
			this.m_Store.UseVisualStyleBackColor = true;
			this.m_Store.Click += new System.EventHandler(this.Store_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// m_DisplayList
			// 
			this.m_DisplayList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_DisplayList.FormattingEnabled = true;
			this.m_DisplayList.IntegralHeight = false;
			this.m_DisplayList.Location = new System.Drawing.Point(12, 12);
			this.m_DisplayList.Name = "m_DisplayList";
			this.m_DisplayList.Size = new System.Drawing.Size(318, 299);
			this.m_DisplayList.TabIndex = 0;
			this.m_DisplayList.SelectedIndexChanged += new System.EventHandler(this.DisplayList_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 75);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Multi Samples";
			// 
			// m_MultiSamples
			// 
			this.m_MultiSamples.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_MultiSamples.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.m_MultiSamples.FormattingEnabled = true;
			this.m_MultiSamples.Location = new System.Drawing.Point(100, 72);
			this.m_MultiSamples.Name = "m_MultiSamples";
			this.m_MultiSamples.Size = new System.Drawing.Size(250, 21);
			this.m_MultiSamples.TabIndex = 8;
			this.m_MultiSamples.SelectedIndexChanged += new System.EventHandler(this.MultiSamples_SelectedIndexChanged);
			// 
			// ResolutionTester
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(704, 351);
			this.Controls.Add(this.m_Store);
			this.Controls.Add(this.m_Scan);
			this.Controls.Add(this.m_Remove);
			this.Controls.Add(this.m_Add);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.m_Load);
			this.Controls.Add(this.m_Save);
			this.Controls.Add(this.m_Apply);
			this.Controls.Add(this.m_DisplaySettingsGroup);
			this.Controls.Add(this.m_DisplayList);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ResolutionTester";
			this.Text = "Resolution Tester";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.ResolutionTester_Load);
			this.m_DisplaySettingsGroup.ResumeLayout(false);
			this.m_DisplaySettingsGroup.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_ResolutionX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.m_ResolutionY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.m_DisplayIndex)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_DesiredDisplays)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Rug.Game.Controls.RefreshingListBox m_DisplayList;
		private System.Windows.Forms.GroupBox m_DisplaySettingsGroup;
		private System.Windows.Forms.Button m_Apply;
		private System.Windows.Forms.Button m_Save;
		private System.Windows.Forms.Button m_Load;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox m_LowestCommonDenominator;
		private System.Windows.Forms.CheckBox m_StickToDisplayResolutions;
		private System.Windows.Forms.NumericUpDown m_DesiredDisplays;
		private System.Windows.Forms.CheckBox m_AllwaysOnTop;
		private System.Windows.Forms.ComboBox m_WindowMode;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown m_DisplayIndex;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RadioButton m_Arbitrary;
		private System.Windows.Forms.RadioButton m_FixedResolution;
		private System.Windows.Forms.NumericUpDown m_ResolutionX;
		private System.Windows.Forms.NumericUpDown m_ResolutionY;
		private System.Windows.Forms.ComboBox m_FixedResolutionSelector;
		private System.Windows.Forms.Button m_Add;
		private System.Windows.Forms.Button m_Remove;
		private System.Windows.Forms.Button m_Scan;
		private System.Windows.Forms.Button m_Store;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.CheckBox m_AllowClosing;
		private System.Windows.Forms.ComboBox m_MultiSamples;
		private System.Windows.Forms.Label label4;
	}
}