﻿using OpenTK;

namespace Rug.Game.Displays
{
	public class GameGLControl : GLControl
	{
		public GameGLControl() : base()
		{
			ResizeRedraw = true;
		}

		protected override bool IsInputKey(System.Windows.Forms.Keys keyData)
		{
			return true; 
		}
	}
}
