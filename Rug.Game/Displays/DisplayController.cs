﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Cmd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Rug.Game.Displays
{
	public class DisplayController : ApplicationContext, IEnumerable<GameWindow>, IDisposable // , IResourceManager
	{
		#region Private Members

		private List<GameWindow> m_Windows = new List<GameWindow>();
		private IGraphicsContext m_SharedContext; 

		private bool m_Disposed;
		private int m_ActiveWindowCount;
		internal bool ShouldClose = false;

		private bool m_ShouldAlignWindows = false;
		private bool m_ShouldApplySettings;

		private bool m_WasResized = false; 

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets windows by index 
		/// </summary>
		/// <param name="index">The index of the window</param>
		/// <returns>A game window</returns>
		public GameWindow this[int index] { get { return m_Windows[index]; } }

		/// <summary>
		/// Number of windows
		/// </summary>
		public int Count { get { return m_Windows.Count; } }

		/// <summary>
		/// The title of the application
		/// </summary>
		public string Title { get; set; }

		public bool WillApplySettings { get { return m_ShouldApplySettings; } }

		public bool WasResized { get { return m_WasResized; } } 

		#endregion

		/// <summary>
		/// Create a display controller 
		/// </summary>
		/// <param name="title">The title of the application</param>
		public DisplayController(string title)
		{
			Title = title;
			m_ActiveWindowCount = 0; 

			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "Display Controller");

			// Handle the ApplicationExit event to know when the application is exiting.
			Application.ApplicationExit += new EventHandler(this.OnApplicationExit);

			Microsoft.Win32.SystemEvents.DisplaySettingsChanged += new EventHandler(SystemEvents_DisplaySettingsChanged);
		}

		#region Was Resized

		internal void OnResize()
		{
			m_WasResized = true;
		}

		public void ResetWasResized()
		{
			m_WasResized = false;
		}

		#endregion

		#region Apply Settings

		public void ShouldApplySettings()
		{
			m_ShouldApplySettings = true; 
		}

		public bool TryApplySettings()
		{
			if (m_ShouldApplySettings == true)
			{
				m_ShouldApplySettings = false;

				ApplySettings();

				return true;
			}
			else
			{
				return false; 
			}
		}

		public void ApplySettings()
		{
			while (m_Windows.Count > DisplaySettings.DesiredWindowCount)
			{
				GameWindow window = m_Windows[m_Windows.Count - 1];

				window.Closed -= new EventHandler(OnFormClosed);
				window.Closing -= new CancelEventHandler(OnFormClosing);

				window.Close(); 

				m_Windows.RemoveAt(m_Windows.Count - 1);

				m_ActiveWindowCount--; 
			}

			AssignDisplays();

			CreateAdditionalForms(); 

			for (int i = 0; i < m_Windows.Count; i++)
			{
				if (DisplaySettings.Windows.Count > i)
				{
					m_Windows[i].ResizeBegin_Manual();

                    m_Windows[i].AssignDisplay(DisplaySettings.DisplayDevices[DisplaySettings.Windows[i].DisplayIndex]); 
					m_Windows[i].DesiredBounds = DisplaySettings.Windows[i].Bounds;					
				}

				m_Windows[i].DetachEvents(); 
			}

			AlignWindows();

			for (int i = 0; i < m_Windows.Count; i++)
			{
				m_Windows[i].AttachEvents();

				m_Windows[i].ResizeEnd_Manual(); 
			}
		}

		#endregion

		#region Save Window States

		public void SaveWindowStates()
		{
			for (int i = 0; i < m_Windows.Count; i++)
			{
				if (DisplaySettings.Windows.Count > i)
				{
					switch (DisplaySettings.WindowMode)
					{
						case GameWindowMode.Fullscreen:
							DisplaySettings.Windows[i].Bounds = m_Windows[i].DesktopBounds;
							break;
						case GameWindowMode.Windowed:
						case GameWindowMode.SizableWindow:
						case GameWindowMode.BorderlessWindow:
							DisplaySettings.Windows[i].Bounds = new Rectangle(m_Windows[i].DesktopBounds.Location, m_Windows[i].ClientSize);
							break;
						default:
							break;
					}
				}
			}
		}

		#endregion

		#region Assign Displays

		/// <summary>
		/// Assign windows to the available displays
		/// </summary>
		public void AssignDisplays()
		{
			int indexOfDefaultDisplay = -1;

			#region Enumerate all the displays

			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.TitleText, "Displays");

			// find all displays and print them
            for (int i = 0; i < DisplaySettings.DisplayDevices.Count; i++)
			{
                if (DisplaySettings.DisplayDevices[i].Device.IsPrimary == true)
				{
                    RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, DisplaySettings.DisplayDevices[i].Device.ToString() + " (Default)");

					// record the index of the default display
					indexOfDefaultDisplay = i;
				}
				else
				{
                    RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, DisplaySettings.DisplayDevices[i].Device.ToString());
				}
			}

			#endregion 

			// calculate the number of windows we want
            DisplaySettings.ResolvedWindowCount = Math.Min(DisplaySettings.DesiredWindowCount, DisplaySettings.DisplayDevices.Count);

			#region Eliminate any display index that is out of range (no longer present) 

			List<int> desiredIndexes = new List<int>(); 

			// iterate through all the setup displays and note their indexes 
			for (int i = 0; i < DisplaySettings.Windows.Count; i++)
			{
				int index = DisplaySettings.Windows[i].DisplayIndex;

                if (index < 0 || index >= DisplaySettings.DisplayDevices.Count)
				{
					index = -1; 
				}

				if (desiredIndexes.Contains(index) == false)
				{
					desiredIndexes.Add(index);
				}
				else
				{
					desiredIndexes.Add(-1);
				}
			}

			#endregion

			#region Add wildcards for any display has not been defined

            while (desiredIndexes.Count < DisplaySettings.DisplayDevices.Count) 
			{
				if (desiredIndexes.Count == 0)
				{
					desiredIndexes.Add(indexOfDefaultDisplay);
				}
				else
				{
					desiredIndexes.Add(-1);
				}
			}

			#endregion

			#region Eliminate any index that is already in use

			List<int> validIndexes = new List<int>();

			for (int i = 0; i < DisplaySettings.ResolvedWindowCount; i++)
			{
                for (int j = 0; j < DisplaySettings.DisplayDevices.Count; j++)
				{
					bool found = false;

                    for (int k = 0; k < DisplaySettings.DisplayDevices.Count; k++)
					{
						found |= desiredIndexes[k] == j;
					}

					if (found == false)
					{
						validIndexes.Add(j);
					}
				}
			}

			#endregion

			#region Resolve the index and assign the resolutions of all windows

			for (int i = 0; i < DisplaySettings.ResolvedWindowCount; i++)
			{
				if (desiredIndexes[i] == -1)
				{
					desiredIndexes[i] = validIndexes[0];

					validIndexes.RemoveAt(0); 
				}

				if (DisplaySettings.Windows.Count <= i)
				{
					DisplaySettings.Windows.Add(new GameDisplay()); 
				}

				DisplaySettings.Windows[i].DisplayIndex = desiredIndexes[i];

                DisplayDeviceProxy activeDevice = DisplaySettings.DisplayDevices[DisplaySettings.Windows[i].DisplayIndex]; 

				Rectangle bounds = DisplaySettings.Windows[i].Bounds;

				// if the window does not have a bounds 
				if (bounds == Rectangle.Empty || 
					bounds.Size.Width < DisplaySettings.MinimumWindowSize.Width || 
					bounds.Size.Height < DisplaySettings.MinimumWindowSize.Height) 
				{				
					switch (DisplaySettings.WindowMode)
					{
						case GameWindowMode.Fullscreen:
							bounds = activeDevice.Device.Bounds;
							break;
						case GameWindowMode.Windowed:
						case GameWindowMode.SizableWindow:
						case GameWindowMode.BorderlessWindow:
							bounds = new Rectangle(activeDevice.Bounds.Location, DisplaySettings.DefaultWindowSize);
							break;
						default:
							break;
					}	
				}
				else 
				{
					switch (DisplaySettings.WindowMode)
					{
						case GameWindowMode.Fullscreen:
							// find a suitable resolution
							DisplayResolution resolution = activeDevice.SelectResolution(bounds.Width, bounds.Height, 32, 60f); 

							// align the bounds with the display and set the size
                            bounds = new Rectangle(activeDevice.Bounds.Location, new Size(resolution.Width, resolution.Height));
							break;
						case GameWindowMode.Windowed:
						case GameWindowMode.SizableWindow:
						case GameWindowMode.BorderlessWindow:
							//if (activeDevice.Bounds.Contains(bounds) == false)
							if (IsOnScreen(bounds) == false)
							{
                                if (bounds.Size.Width > activeDevice.Bounds.Width ||
                                    bounds.Size.Height > activeDevice.Bounds.Height)
								{
                                    bounds = new Rectangle(activeDevice.Bounds.Location, activeDevice.Bounds.Size);
								}
								else
								{
                                    bounds = new Rectangle(activeDevice.Bounds.Location, bounds.Size);
								}
							}
							break;
						default:
							break;
					}	
				}

				DisplaySettings.Windows[i].Bounds = bounds;
			}

			#endregion

			#region Refine resolutions to fit rules

			if (DisplaySettings.StickToDisplayResolutions == true &&
				DisplaySettings.LowestCommonDenominator == true)
			{
                List<DisplayDeviceProxy> devices = new List<DisplayDeviceProxy>(); 

				for (int i = 0; i < DisplaySettings.ResolvedWindowCount; i++)
				{
                    devices.Add(DisplaySettings.DisplayDevices[DisplaySettings.Windows[i].DisplayIndex]); 
				}

				DisplayResolution[] resolution = FindSuitableResolution(DisplaySettings.Windows[0].Bounds.Size, devices.ToArray());

				for (int i = 0; i < DisplaySettings.ResolvedWindowCount; i++)
				{
					DisplaySettings.Windows[i].Bounds = new Rectangle(DisplaySettings.Windows[i].Bounds.Location, new Size(resolution[0].Width, resolution[0].Height));
				}
			}
			else if (DisplaySettings.StickToDisplayResolutions == true)
			{
				for (int i = 0; i < DisplaySettings.ResolvedWindowCount; i++)
				{
                    DisplayResolution resolution = FindSuitableResolution(DisplaySettings.Windows[i].Bounds.Size, DisplaySettings.DisplayDevices[DisplaySettings.Windows[i].DisplayIndex]);

					DisplaySettings.Windows[i].Bounds = new Rectangle(DisplaySettings.Windows[i].Bounds.Location, new Size(resolution.Width, resolution.Height));
                   // DisplaySettings.Windows[i].Bounds = new Rectangle(DisplaySettings.DisplayDevices[DisplaySettings.Windows[i].DisplayIndex].Screen.Bounds.Location, new Size(resolution.Width, resolution.Height));
				}
			}

			#endregion
		}

		/// <summary>
		/// Check that a rectangle is fully on the screen
		/// </summary>
		/// <param name="rectangle">the rectangle to check</param>
		/// <returns>true if the rectangle is fully on a screen</returns>
		private bool IsOnScreen(Rectangle rectangle)
		{
			Screen[] screens = Screen.AllScreens;

			foreach (Screen screen in screens)
			{
				if (screen.WorkingArea.IntersectsWith(rectangle))
				{
					return true;
				}
			}

			return false;
		}

		#endregion

		#region Create Default Form

		/// <summary>
		/// Create the default form
		/// </summary>
		public void CreateDefaultForm()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Display Controller: Create Default");

            GameWindow newContext = new GameWindow(this, DisplaySettings.DisplayDevices[DisplaySettings.Windows[0].DisplayIndex], ConsoleThemeColor.TitleText);

			newContext.DesiredBounds = DisplaySettings.Windows[0].Bounds; 

			newContext.Text = Title + " " + 1.ToString();

			m_Windows.Add(newContext);

			newContext.Closed += new EventHandler(OnFormClosed);
			newContext.Closing += new CancelEventHandler(OnFormClosing);

			m_ActiveWindowCount++;

			newContext.BackColor = Color.Red; 

			newContext.Show();			

			m_SharedContext = newContext.GraphicsContext; 

			MainForm = m_Windows[0];
		}

		#endregion 

		#region Create Additional Forms

		/// <summary>
		/// Create any additional forms
		/// </summary>
		/// <returns>An array containing all the forms created by this method</returns>
		public GameWindow[] CreateAdditionalForms()
		{
			List<GameWindow> newForms = new List<GameWindow>();

			for (int i = m_Windows.Count; i < DisplaySettings.ResolvedWindowCount; i++)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Display Controller: Create Window " + i);

                GameWindow newContext = new GameWindow(this, DisplaySettings.DisplayDevices[DisplaySettings.Windows[i].DisplayIndex], (ConsoleThemeColor)((int)ConsoleThemeColor.TitleText + i + 1));

				newContext.DesiredBounds = DisplaySettings.Windows[i].Bounds; 

				newContext.Text = Title + " " + (i + 1).ToString(); 

				m_Windows.Add(newContext);
				newForms.Add(newContext);
				
				newContext.Closed += new EventHandler(OnFormClosed);
				newContext.Closing += new CancelEventHandler(OnFormClosing);

				m_ActiveWindowCount++;

				newContext.BackColor = Color.Green; 

				newContext.Show();				
			}

			return newForms.ToArray();
		}

		#endregion

		#region Align Windows

		public void TryAlignWindows()
		{
			if (m_ShouldAlignWindows == true)
			{
				m_ShouldAlignWindows = false;

				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Display Controller: Try Align Windows");

				try
				{
					foreach (GameWindow window in m_Windows)
					{
						window.AlignToDesiredBounds();
					}
				}
				catch (Exception ex)
				{
					RC.WriteException(001, ex);
				}
				finally
				{
					RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Done");
				}
			}
		}

		/// <summary>
		/// Change resolutions and align windows
		/// </summary>
		public void AlignWindows()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Display Controller: Align Windows");

			try
			{
				m_ShouldAlignWindows = false; 

				foreach (GameWindow window in m_Windows)
				{
					window.SetWindowMode();
				}

				foreach (GameWindow window in m_Windows)
				{
					window.ChangeResolutionIfNeeded();
				}

				foreach (GameWindow window in m_Windows)
				{
					window.AlignToDesiredBounds();
				}
			}
			catch (Exception ex)
			{
				RC.WriteException(001, ex); 
			}
			finally
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Done");
			}
		}

		void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Display Controller: Display Settings Changed");

			try 
			{
				m_ShouldAlignWindows = true; 
			}
			catch (Exception ex)
			{
				RC.WriteException(001, ex); 
			}
			finally
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, "Done");
			}
		}

		#endregion

		#region Finalise Resize

		/// <summary>
		/// Finalise a resize started by a window
		/// </summary>
		/// <param name="size">The desired client size</param>
		public void FinaliseResize(Size size)
		{
			List<DisplayDeviceProxy> devices = new List<DisplayDeviceProxy>(); 

			foreach (GameWindow window in m_Windows) 
			{
				devices.Add(window.Device); 
			}

			DisplayResolution[] resolvedResolutions = FindSuitableResolution(size, devices.ToArray());

			int index = 0; 
			foreach (GameWindow window in m_Windows)
			{
				DisplayResolution resolution = resolvedResolutions[index];

				window.DesiredBounds = new Rectangle(window.Location, new Size(resolution.Width, resolution.Height));

				window.AlignToDesiredBounds();
			}
		}

		#endregion

		#region Events

		private void OnApplicationExit(object sender, EventArgs e)
		{

		}

		private void OnFormClosing(object sender, CancelEventArgs e)
		{
			if (e is FormClosingEventArgs)
			{
				FormClosingEventArgs args = e as FormClosingEventArgs;

				switch (args.CloseReason)
				{
					case CloseReason.ApplicationExitCall:
						ShouldClose = true;
						break;
					case CloseReason.FormOwnerClosing:
						ShouldClose = true;
						break;
					case CloseReason.MdiFormClosing:
						ShouldClose = true;
						break;
					case CloseReason.None:
						break;
					case CloseReason.TaskManagerClosing:
						ShouldClose = true;
						break;
					case CloseReason.UserClosing:
						if (DisplaySettings.AllowClosing == false)
						{
							e.Cancel = true;
						}
						else
						{
							ShouldClose = true;
						}
						break;
					case CloseReason.WindowsShutDown:
						ShouldClose = true;
						break;
					default:
						break;
				}

			}
			else
			{
				ShouldClose = true;
			}
		}

		private void OnFormClosed(object sender, EventArgs e)
		{
			// When a form is closed, decrement the count of open forms.			
			m_ActiveWindowCount--;

			if (m_ActiveWindowCount <= 0)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "Display Controller: Thread Exit");

				// When the count gets to 0, exit the app by calling ExitThread
				ExitThread();
			}
		}

		#endregion

		#region IEnumerable<ArtworkForm> Members

		public IEnumerator<GameWindow> GetEnumerator()
		{
			return m_Windows.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return (m_Windows as System.Collections.IEnumerable).GetEnumerator();
		}

		#endregion

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "Display Controller: Load Resources");

			if (m_Disposed == true)
			{
				foreach (GameWindow context in m_Windows)
				{
					//context.LoadResources();
				}

				m_Disposed = false;
			}
		}

		public void UnloadResources()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "Display Controller: Unload Resources");

			if (m_Disposed == false)
			{
				foreach (GameWindow context in m_Windows)
				{
					//context.UnloadResources();
				}

				m_Disposed = true;
			}
		}


		#endregion

		#region IDisposable Members

		void IDisposable.Dispose()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "Display Controller: Dispose");

			UnloadResources();

			foreach (GameWindow context in m_Windows)
			{
				context.Dispose();
			}
		}

		#endregion

		#region Get Possible Resolutions

        public static List<DisplayResolution> GetPossibleResolutions(DisplayDeviceProxy[] devices)
		{
			List<DisplayResolution> possible = new List<DisplayResolution>();

			bool first = true;

            foreach (DisplayDeviceProxy device in devices)
			{
				if (first == true)
				{
					possible.AddRange(device.AvailableResolutions);
					first = false;
				}
				else
				{
					for (int i = possible.Count - 1; i >= 0; i--) 
					{
						DisplayResolution resB = possible[i]; 

						bool found = false; 

						foreach (DisplayResolution resA in device.AvailableResolutions)
						{
							if (resA.Width == resB.Width &&
								resA.Height == resB.Height &&
								resA.RefreshRate == resB.RefreshRate &&
								resA.BitsPerPixel == resB.BitsPerPixel)
							{
								found = true; 
							}
						}

						if (found == false)
						{
							possible.RemoveAt(i);
						}
					} 
				}
			}


			for (int i = possible.Count - 1; i >= 0; i--)
			{
				if (possible[i].Width < DisplaySettings.MinimumWindowSize.Width ||
					possible[i].Height < DisplaySettings.MinimumWindowSize.Height ||
					possible[i].BitsPerPixel != 32 ||
					possible[i].RefreshRate != 60f)
				{
					possible.RemoveAt(i);
				}
			}

			return possible; 
		}

		#endregion 

		#region Find Suitable Resolution

		internal static DisplayResolution[] FindSuitableResolution(Size size, DisplayDeviceProxy[] devices)
		{
			List<DisplayResolution> possible = GetPossibleResolutions(devices);

			DisplayResolution bestMatch = FindBestMatch(size, possible);

			List<DisplayResolution> resolvedResolutions = new List<DisplayResolution>();

			if (bestMatch == null)
			{
                foreach (DisplayDeviceProxy device in devices)
				{
					resolvedResolutions.Add(device.SelectResolution(DisplaySettings.MinimumWindowSize.Width, DisplaySettings.MinimumWindowSize.Height, 32, 60f));
				}
			}
			else
			{
                foreach (DisplayDeviceProxy device in devices)
				{
					resolvedResolutions.Add(device.SelectResolution(bestMatch.Width, bestMatch.Height, 32, 60f));
				}
			}

			return resolvedResolutions.ToArray(); 
		}

        internal static DisplayResolution FindSuitableResolution(Size size, DisplayDeviceProxy device)
		{
			DisplayResolution bestMatch = FindBestMatch(size, device.AvailableResolutions);

			if (bestMatch == null)
			{
				bestMatch = device.SelectResolution(DisplaySettings.MinimumWindowSize.Width, DisplaySettings.MinimumWindowSize.Height, 32, 60f);
			}

			return bestMatch;
		}

		#endregion

		#region Find Best Match Resolution

		/// <summary>
		/// Find the best possible match resolution
		/// </summary>
		/// <param name="size">The desired client size</param>
		/// <param name="availableResolutions">All the possible resolutions</param>
		/// <returns>The best match</returns>
		private static DisplayResolution FindBestMatch(Size size, IList<DisplayResolution> availableResolutions)
		{
			List<DisplayResolution> possible = new List<DisplayResolution>(availableResolutions);

			for (int i = possible.Count - 1; i >= 0; i--)
			{
				if (possible[i].Width < DisplaySettings.MinimumWindowSize.Width ||
					possible[i].Height < DisplaySettings.MinimumWindowSize.Height || 
					possible[i].BitsPerPixel != 32 ||
					possible[i].RefreshRate != 60f)
				{
					possible.RemoveAt(i); 
				}
			}

			float desiredAspect = (float)size.Height / (float)size.Width; 

			DisplayResolution bestMatch = null;
			float bestAspect = 0; 
			
			foreach (DisplayResolution res in possible)
			{
				if (res.Width <= size.Width &&
					res.Height <= size.Height)
				{
					float newAspect = (float)res.Height / (float)res.Width; 

					if (bestMatch == null)
					{
						bestMatch = res;

						bestAspect = newAspect; 
					}
					else if (bestMatch.Width <= res.Width)
					{
						if (bestMatch.Width == res.Width)
						{
							float aspectDiffBest = Math.Abs(desiredAspect - bestAspect);
							float aspectDiffNew = Math.Abs(desiredAspect - newAspect);

							if (aspectDiffNew <= aspectDiffBest)
							{
								bestMatch = res;

								bestAspect = newAspect; 
							}
						}
						else
						{
							bestMatch = res;

							bestAspect = newAspect; 
						}
					}
				}
			}

			return bestMatch;
		}

		#endregion
	}
}
