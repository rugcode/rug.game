﻿using OpenTK;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace Rug.Game.Displays
{
	public enum GameWindowMode : int { NotSet = 0, Fullscreen = 1, Windowed = 2, SizableWindow = 3, BorderlessWindow = 4 }

	public class GameDisplay
	{
		public Rectangle Bounds { get; set; }

		public int DisplayIndex { get; set; }

		internal GameDisplay()
		{
			Bounds = Rectangle.Empty;
			DisplayIndex = -1; 
		}

		public override string ToString()
		{
			return "Display: " + DisplayIndex + ", Bounds: " + Bounds.ToString();
		}

		internal void Load(XmlNode node)
		{
			DisplayIndex = Helper.GetAttributeValue(node, "DisplayIndex", DisplayIndex);
			Bounds = Helper.GetAttributeValue(node, "Bounds", Bounds);
		}

		internal void Save(XmlElement node)
		{
			Helper.AppendAttributeAndValue(node, "DisplayIndex", DisplayIndex);
			Helper.AppendAttributeAndValue(node, "Bounds", Bounds);
		}
	}

    public class DisplayDeviceProxy
    {
        public int Index { get; private set; } 

        public DisplayIndex DispalyIndex { get; private set; }

        public DisplayDevice Device { get; private set; }

        public Screen Screen { get; private set; }

        public Rectangle Bounds { get { return Device.Bounds; } }

        public int Width { get { return Device.Width; } }

        public int Height { get { return Device.Height; } }

        public IList<DisplayResolution> AvailableResolutions { get { return Device.AvailableResolutions; } } 

        internal DisplayDeviceProxy(int index, DisplayIndex displayIndex, DisplayDevice device, Screen screen)
        {
            Index = index; 
            DispalyIndex = displayIndex;
            Device = device;
            Screen = screen;
        }

        public DisplayResolution SelectResolution(int width, int height, int bitsPerPixel, float refreshRate)
        {
            return Device.SelectResolution(width, height, bitsPerPixel, refreshRate); 
        }

        public void ChangeResolution(DisplayResolution resolution)
        {
            Device.ChangeResolution(resolution);
        }

        public void RestoreResolution()
        {
            Device.RestoreResolution();
        }

    }

	public static class DisplaySettings
	{
		/// <summary>
		/// The desired number of windows
		/// </summary>
		public static int DesiredWindowCount = 1;

		/// <summary>
		/// The resolved number of windows
		/// </summary>
		public static int ResolvedWindowCount = 0; 

		/// <summary>
		/// The settings for each game window
		/// </summary>
		public static List<GameDisplay> Windows = new List<GameDisplay>();

		/// <summary>
		/// Gmae window mode
		/// </summary>
		public static GameWindowMode WindowMode = GameWindowMode.SizableWindow;

		/// <summary>
		/// Allow the user to close the windows themselves
		/// </summary>
		public static bool AllowClosing = true;

		/// <summary>
		/// Use exclusive whem possible
		/// </summary>
		//public static bool UseExclusiveMode = true; 

		/// <summary>
		/// Should windows allways be top most? 
		/// </summary>
		public static bool AllwaysOnTop = false;

		/// <summary>
		/// If true then the client size of any one window will lock to a possible full screen 
		/// resolution of the display that it belongs to. 
		/// </summary>
		public static bool StickToDisplayResolutions = true;

		/// <summary>
		/// If true then the client size of all windows will lock to a possible full screen resolution
		/// shared by all displays currently in use
		/// </summary>
		public static bool LowestCommonDenominator = true;

		/// <summary>
		/// Default window size
		/// </summary>
		public static Size DefaultWindowSize = new Size(1024, 768);

		/// <summary>
		/// Minimum window size
		/// </summary>
		public static Size MinimumWindowSize = new Size(800, 600);

		/// <summary>
		/// Multi sample level
		/// </summary>
		public static MultiSamples SampleLevel = MultiSamples.X2;

		public static int MultiSample
		{
			get { return GLHelper.MultiSampleToSampleCount(SampleLevel); }
			set { SampleLevel = GLHelper.SampleCountToMultiSample(value); }
		}

        public static readonly List<DisplayDeviceProxy> DisplayDevices = new List<DisplayDeviceProxy>(); 

		static DisplaySettings()
		{
            DisplayDevices = new List<DisplayDeviceProxy>();

            int index = 0;
            // GetDisplay(DisplayIndex index)
			//for (int i = 0; i < DisplayDevice.AvailableDisplays.Count; i++)
            for (int i = 0; i <= (int)DisplayIndex.Sixth; i++)                
			{
				//DisplayDevice device = DisplayDevice.AvailableDisplays[i]; 
                DisplayDevice device = DisplayDevice.GetDisplay((DisplayIndex)i);

                if (device == null)
                {
                    continue;
                }

                DisplayDevices.Add(new DisplayDeviceProxy(index++, (DisplayIndex)i, device, Screen.AllScreens[i])); 
                /* 
				for (int j = 0; j < Screen.AllScreens.Length; j++)
				{
					if (device.Bounds == Screen.AllScreens[j].Bounds)
                    // if (device.ToString().Substring(0, device.ToString()
					{
                        if (index != j)
                        {
                            throw new Exception("Device screen mismatch."); 
                        }

                        DisplayDevices.Add(new DisplayDeviceProxy(index++, (DisplayIndex)i, device, Screen.AllScreens[j])); 

						break;
					}
				}
                */
			}
		}

		public static void Load(XmlNode node)
		{
			DesiredWindowCount = Helper.GetAttributeValue(node, "DesiredWindowCount", DesiredWindowCount);
			WindowMode = Helper.GetAttributeValue(node, "WindowMode", WindowMode);
			AllowClosing = Helper.GetAttributeValue(node, "AllowClosing", AllowClosing);
			AllwaysOnTop = Helper.GetAttributeValue(node, "AllwaysOnTop", AllwaysOnTop);
			StickToDisplayResolutions = Helper.GetAttributeValue(node, "StickToDisplayResolutions", StickToDisplayResolutions);
			LowestCommonDenominator = Helper.GetAttributeValue(node, "LowestCommonDenominator", LowestCommonDenominator);
			DefaultWindowSize = Helper.GetAttributeValue(node, "DefaultWindowSize", DefaultWindowSize);

			SampleLevel = Helper.GetAttributeValue(node, "SampleLevel", SampleLevel);

			foreach (XmlNode gameWindow in node.SelectNodes("Windows/GameDisplay"))
			{
				GameDisplay display = new GameDisplay();

				display.Load(gameWindow);

				Windows.Add(display); 
			}
		}

		public static void Save(XmlElement node)
		{
			Helper.AppendAttributeAndValue(node, "DesiredWindowCount", DesiredWindowCount);
			Helper.AppendAttributeAndValue(node, "WindowMode", WindowMode.ToString());
			Helper.AppendAttributeAndValue(node, "AllowClosing", AllowClosing);
			Helper.AppendAttributeAndValue(node, "AllwaysOnTop", AllwaysOnTop);
			Helper.AppendAttributeAndValue(node, "StickToDisplayResolutions", StickToDisplayResolutions);
			Helper.AppendAttributeAndValue(node, "LowestCommonDenominator", LowestCommonDenominator);
			Helper.AppendAttributeAndValue(node, "DefaultWindowSize", DefaultWindowSize);

			Helper.AppendAttributeAndValue(node, "SampleLevel", SampleLevel);

			XmlElement windows = node.AppendChild(Helper.CreateElement(node, "Windows")) as XmlElement; 

			foreach (GameDisplay display in Windows)
			{
				XmlElement window = windows.AppendChild(Helper.CreateElement(windows, "GameDisplay")) as XmlElement;

				display.Save(window);
			}
		}
	}
}
