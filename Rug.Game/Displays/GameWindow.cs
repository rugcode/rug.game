﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Cmd;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Rug.Game.Displays
{
	public partial class GameWindow : Form
	{
		private int m_ScreenIndex = -1;

		private DisplayController m_Controller;
        private DisplayDeviceProxy m_Device;
		private DisplayResolution m_Resolution; 

		private ConsoleThemeColor m_Color;

		private GameWindowMode m_CurrentMode = GameWindowMode.NotSet;
		private  int m_MouseHideCount;

		private Size m_OldClientSize; 

		public Rectangle DesiredBounds { get; set; }
		public Rectangle ResolvedBounds { get; set; }

        public DisplayDeviceProxy Device { get { return m_Device; } }
		public DisplayResolution Resolution { get { return m_Resolution; } } 

		public ConsoleThemeColor Color { get { return m_Color; } }

		public bool HideCursor { get; set; }

		public IGraphicsContext GraphicsContext { get { return m_GLControl.Context; } }
		public GLControl GLControl { get { return m_GLControl; } }

        public GameWindow(DisplayController controller, DisplayDeviceProxy displayDevice, ConsoleThemeColor consoleThemeColor)
		{
            if (Rug.Game.Environment.Icon != null)
            {
                Icon = Rug.Game.Environment.Icon;
            }
            
            m_Controller = controller; 

			m_Device = displayDevice;
			m_Color = consoleThemeColor;

            m_ScreenIndex = m_Device.Index; // DisplaySettings.DeviceToScreenLookup[m_Device.ToString()]; 

			InitializeComponent();

			ResizeRedraw = true;
			SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);	
		}

        internal void AssignDisplay(DisplayDeviceProxy displayDevice)
		{
			if (m_Device != displayDevice)
			{
				if (m_CurrentMode == GameWindowMode.Fullscreen)
				{
					RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Restoring resolution");	
					Device.RestoreResolution();

					m_CurrentMode = GameWindowMode.NotSet;
				}
                
                m_Device = displayDevice;

                /* 
				if (DisplaySettings.DeviceToScreenLookup.ContainsKey(displayDevice.ToString()) == true)
				{
					m_Device = displayDevice;
				}
				else
				{
					RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, "Device not found?! " + displayDevice);
				}

                if (DisplaySettings.DeviceToScreenLookup.ContainsKey(m_Device.ToString()) == false)
                {
                    throw new Exception("Could not find device '" + m_Device.ToString() + "'");
                }
                */

                m_ScreenIndex = m_Device.Index; //  DisplaySettings.DeviceToScreenLookup[m_Device.ToString()];
			}
		}

		private void GameWindow_Load(object sender, EventArgs e)
		{
			SetWindowMode();

			AttachEvents();	
		}

		internal void AttachEvents()
		{
			Move += new EventHandler(GameWindow_Move);

			ResizeBegin += new EventHandler(GameWindow_ResizeBegin);
			Resize += new EventHandler(GameWindow_Resize);
			ResizeEnd += new EventHandler(GameWindow_ResizeEnd);

			GLControl.MouseEnter += new EventHandler(Form_MouseEnter);
			GLControl.MouseLeave += new EventHandler(Form_MouseLeave);	
		}

		internal void DetachEvents()
		{
			Move -= new EventHandler(GameWindow_Move);

			ResizeBegin -= new EventHandler(GameWindow_ResizeBegin);
			Resize -= new EventHandler(GameWindow_Resize);
			ResizeEnd -= new EventHandler(GameWindow_ResizeEnd);

			GLControl.MouseEnter -= new EventHandler(Form_MouseEnter);
			GLControl.MouseLeave -= new EventHandler(Form_MouseLeave);	
		}

		protected override void OnPaintBackground(PaintEventArgs e)
		{
		}

		#region Mouse Events
		
		void Form_MouseLeave(object sender, EventArgs e)
		{
			if (HideCursor == true)
			{
				if (m_MouseHideCount < 0)
				{
					m_MouseHideCount++;
					Cursor.Show();
				}
			}
		}

		void Form_MouseEnter(object sender, EventArgs e)
		{
			if (HideCursor == true)
			{
				if (m_MouseHideCount >= 0)
				{
					m_MouseHideCount--;
					Cursor.Hide();
				}
			}
			else
			{
				if (m_MouseHideCount < 0)
				{
					m_MouseHideCount++;
					Cursor.Show();
				}
			}
		}

		public void DoHideCursor()
		{
			if (HideCursor == true)
			{
				if (m_MouseHideCount >= 0)
				{
					m_MouseHideCount--;
					Cursor.Hide();
				}
			}
			else
			{
				if (m_MouseHideCount < 0)
				{
					m_MouseHideCount++;
					Cursor.Show();
				}
			}
		}

		#endregion 

		#region Size and Mode

		public void SetWindowMode()
		{
			TopMost = DisplaySettings.AllwaysOnTop;

			WindowState = FormWindowState.Normal;
			
			MaximizeBox = false;
			
			MinimizeBox = false; 

			switch (DisplaySettings.WindowMode)
			{
				case GameWindowMode.Fullscreen:
					FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
					break;
				case GameWindowMode.BorderlessWindow:
					FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
					break;
				case GameWindowMode.Windowed:
					FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
					break;
				case GameWindowMode.SizableWindow:
					FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
					break;
				default:
					break;
			}

			ShowInTaskbar = true; 
		}

		public void ChangeResolutionIfNeeded()
		{
			if ((DisplaySettings.WindowMode == GameWindowMode.Fullscreen && 
				m_CurrentMode != GameWindowMode.Fullscreen) ||
				(DisplaySettings.WindowMode == GameWindowMode.Fullscreen && 
				(DesiredBounds.Width != Device.Width || DesiredBounds.Height != Device.Height))) 
			{
				DisplayResolution resolution = Device.SelectResolution(DesiredBounds.Width, DesiredBounds.Height, 32, 60f);

				m_Resolution = resolution; 

				if (resolution.Width != Device.Width || resolution.Height != Device.Height)
				{
					Device.ChangeResolution(resolution);

					RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Resolution changed");					
				}
				else
				{
					RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Resolution is correct");
				}
			}
			else if (DisplaySettings.WindowMode != GameWindowMode.Fullscreen &&
				m_CurrentMode == GameWindowMode.Fullscreen)
			{
				Device.RestoreResolution();

				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Resolution restored");

				m_Resolution = Device.SelectResolution(Device.Width, Device.Height, 32, 60f);
			}
			else
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", No change needed");

				m_Resolution = Device.SelectResolution(Device.Width, Device.Height, 32, 60f);
			}

			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText1, Resolution.ToString());
			RC.WriteLine(ConsoleVerbosity.Debug); 

			m_CurrentMode = DisplaySettings.WindowMode; 
		}

		public void AlignToDesiredBounds()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.Text, Text + ", Align to desired bounds");
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText2, "Desired bounds: " + DesiredBounds.ToString());
			
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText2, "Screen working area: " + Screen.AllScreens[m_ScreenIndex].WorkingArea.ToString());

			Rectangle screenBounds = Screen.AllScreens[m_ScreenIndex].WorkingArea; 

			switch (DisplaySettings.WindowMode)
			{
				case GameWindowMode.Fullscreen:
					DesktopBounds = new Rectangle(screenBounds.Location, DesiredBounds.Size);
					ResolvedBounds = DesktopBounds; 
					break;
				case GameWindowMode.BorderlessWindow:
				case GameWindowMode.Windowed:
				case GameWindowMode.SizableWindow:
					DesktopLocation = DesiredBounds.Location;
					ClientSize = DesiredBounds.Size;
					ResolvedBounds = new Rectangle(DesiredBounds.Location, DesiredBounds.Size); 
					break;
				default:
					break;
			}

			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText2, "Resolved bounds: " + ResolvedBounds.ToString());

			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText1, m_Resolution.ToString());
			RC.WriteLine(ConsoleVerbosity.Debug);
		}

		void GameWindow_Move(object sender, EventArgs e)
		{
			if (DisplaySettings.StickToDisplayResolutions == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Moved");

				DoNaturalResize();
			}
		}

		internal void ResizeBegin_Manual()
		{
			GameWindow_ResizeBegin(this, EventArgs.Empty);
		}

		void GameWindow_ResizeBegin(object sender, EventArgs e)
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Resize begin");

			m_OldClientSize = ClientSize; 
		}

		void GameWindow_Resize(object sender, EventArgs e)
		{
			if (DisplaySettings.StickToDisplayResolutions == false)
			{
				RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Resize");

				DoNaturalResize(); 
			}
		}

		internal void ResizeEnd_Manual()
		{
			GameWindow_ResizeEnd(this, EventArgs.Empty);
		}

		void GameWindow_ResizeEnd(object sender, EventArgs e)
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText, Text + ", Resize end");

			if (DisplaySettings.StickToDisplayResolutions == true)
			{
				if (DisplaySettings.LowestCommonDenominator == true)
				{
					m_Controller.FinaliseResize(this.ClientSize);
				}
				else
				{
					DisplayResolution resolution = DisplayController.FindSuitableResolution(this.ClientSize, m_Device);

					DesiredBounds = new Rectangle(Location, new Size(resolution.Width, resolution.Height));

					AlignToDesiredBounds();
				}
			}

			if (m_OldClientSize != ClientSize)
			{
				m_Controller.OnResize(); 
			}
		}

		private void DoNaturalResize()
		{
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText1, "Bounds: " + Bounds.ToString());
			RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText1, "Desktop bounds: " + DesktopBounds.ToString());

			switch (DisplaySettings.WindowMode)
			{
				case GameWindowMode.Fullscreen:
					break;
				case GameWindowMode.Windowed:
				case GameWindowMode.SizableWindow:
				case GameWindowMode.BorderlessWindow:
					DesiredBounds = new Rectangle(Location, ClientSize);
					AlignToDesiredBounds();
					break;
				default:
					break;
			}
		}

		#endregion 
	}
}
