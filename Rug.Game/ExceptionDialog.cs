﻿using System;
using System.Text;
using System.Windows.Forms;

namespace Rug.Game
{
    public partial class ExceptionDialog : Form
    {
        public ExceptionDialog(string title, Exception ex)
        {
            InitializeComponent();

            this.Text = title;

            textBox1.Text = ex.Message;

            textBox2.Text = ex.ToString(); 
        }

        private void m_Copy_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine(this.Text);
            sb.AppendLine();

            sb.AppendLine(textBox1.Text);
            sb.AppendLine();

            sb.AppendLine(textBox2.Text);

            Clipboard.SetText(sb.ToString()); 
        }

        public static void ShowException(string title, Exception ex)
        {
            using (ExceptionDialog dialog = new ExceptionDialog(title, ex))
            {
                dialog.ShowDialog(); 
            }
        }
    }
}
