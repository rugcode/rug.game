﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Data;
using Rug.Game.Core.Maths;
using Rug.Game.Flat.Models;
using System;

namespace Rug.Game.Flat
{
	public class ModelSceneLight
	{		
		public const float MinimumLight = 0.01f;

		public object Tag { get; set; }
		

		public Vector3 Center { get; set; }

		public Color3 Color { get; set; }

		public float SpecularPower { get; set; } 

		public float AttenuatuionTermA { get; set; }

		public float Radius { get; set; }
		
		public bool IsInfinite { get { return float.IsInfinity(Radius); } } 

		public float Intensity { get; set; }

        public bool Directional { get; set; } 


		public Color4 ColorArg { get; private set; } 

		public ModelLightAttenuation Attenuation { get; private set; }

		public float BoundsRadius { get; private set; }

		public BoundingBox Bounds { get; private set; }		

		public ModelSceneLight()
		{
			Intensity = 1f; 
			AttenuatuionTermA = 0f;
			Radius = float.PositiveInfinity;

			Bounds = new BoundingBox(); 
		}

		public void Update()
		{
			if (IsInfinite == true)
			{
				Attenuation = new ModelLightAttenuation(0f, 0f);

				BoundsRadius = float.PositiveInfinity; 

				Bounds = new BoundingBox(new Vector3(float.NegativeInfinity), new Vector3(float.PositiveInfinity)); 
			}
			else
			{				
				float b = 1.0f / (Radius * Radius * MinimumLight);

				Attenuation = new ModelLightAttenuation(AttenuatuionTermA, b);

				BoundsRadius = Math.Max(Radius, Radius * SpecularPower) * Intensity; 

				Vector3 max = new Vector3(BoundsRadius);

				Bounds = new BoundingBox(Center - max, Center + max); 
			}

			ColorArg = Color3.ToColor4(Color, SpecularPower); 
		}

        public ModelLightInstance ToLightInstance(ref Matrix4 world, ref Matrix4 normalWorld)
		{
            Vector3 center; 
            
            if (Directional == true) 
            {
                center = Vector3.Transform(Center, normalWorld); 
            }
            else 
            {
                center = Vector3.Transform(Center, world);
            }

			return new ModelLightInstance()
			{
                Position = center, // Center,
				Intensity = Intensity,
				Color = ColorArg,
				Attenuation = Attenuation,
			};
		}
	}
}
