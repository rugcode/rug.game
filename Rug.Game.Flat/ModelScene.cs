﻿using OpenTK;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Models;
using Rug.Game.Flat.Shaders.Flat;
using System.Collections.Generic;

namespace Rug.Game.Flat
{
	public class ModelScene : IScene, IResourceManager
	{
		#region Private Members

		private ResourceManager ResourceManager = new ResourceManager();

        private List<ModelBatch> m_ModelBatches_NonReflective = new List<ModelBatch>();
        private List<ModelBatch> m_ModelBatches_Reflective = new List<ModelBatch>();

        private List<ModelFixedBatch> m_ModelFixedBatches_NonReflective = new List<ModelFixedBatch>();
        private List<ModelFixedBatch> m_ModelFixedBatches_Reflective = new List<ModelFixedBatch>();

        private List<ExtrudedModelBatch> m_ExtrudedModelBatches_NonReflective = new List<ExtrudedModelBatch>();
        private List<ExtrudedModelBatch> m_ExtrudedModelBatches_Reflective = new List<ExtrudedModelBatch>();

        private List<ModelSceneObject> m_Objects_NonReflective = new List<ModelSceneObject>();
        private List<ModelSceneObject> m_Objects_Reflective = new List<ModelSceneObject>(); 

		#endregion

		#region Public Members

		public bool Disposed { get; private set; }

		public readonly ModelLighting Lighting;

        public readonly Dictionary<string, ModelBase> Models = new Dictionary<string, ModelBase>();

		public readonly Dictionary<string, ModelBatch> Batches = new Dictionary<string, ModelBatch>();

		public readonly Dictionary<string, ModelFixedBatch> FixedBatches = new Dictionary<string, ModelFixedBatch>();

        public readonly Dictionary<string, ExtrudedModelBatch> ExtrudedBatches = new Dictionary<string, ExtrudedModelBatch>();

        public readonly SFXLineBatch DefaultSFXLineBatch = new SFXLineBatch(128);

        public readonly List<SFXLineBatch> SFXLineBatches = new List<SFXLineBatch>(); 

		public readonly List<ModelSceneObject> Objects = new List<ModelSceneObject>();

		public readonly List<ModelSceneObject> FixedObjects = new List<ModelSceneObject>();

        public readonly List<ModelChain> Chains = new List<ModelChain>();

        public readonly List<SFXLineChain> SFXChains = new List<SFXLineChain>();

		public readonly List<ModelSceneLight> Lights = new List<ModelSceneLight>();

		public Shaders.Flat.ModelLightingMode LightingMode { get; set; }

		#endregion

        public ModelScene(ModelLightingMode[] lightingModFlags, int shadowLights, int lightCount)
		{
			Disposed = true;

			LightingMode = ModelLightingMode.None;

            Lighting = new ModelLighting(lightingModFlags, shadowLights, lightCount);

			ResourceManager.Add(Lighting);

            ResourceManager.Add(DefaultSFXLineBatch); 
		}

		#region Add / Remove Model

        public ModelBase AddModel(string name, string path, bool flat, bool textured)
		{
			ModelBase model = null; 
            
            if (textured == false) 
            {
                model = new Model(name, path, flat);
            }
            else
            {
                model = new TexturedModel(name, path, flat);
            }

			ResourceManager.Add(model); 

			Models.Add(name, model);

            if (Disposed == false)
            {
                model.LoadResources(); 
            }

            return model; 
		}

        public void AddModel(ModelBase model)
        {
            ResourceManager.Add(model);

            Models.Add(model.Name, model);

            if (Disposed == false)
            {
                model.LoadResources();
            }
        }

        public void RemoveModel(string name)
        {
            {
                ModelBase model;
                if (Models.TryGetValue(name, out model) == true)
                {
                    ResourceManager.Remove(model);

                    Models.Remove(name);

                    if (Disposed == false)
                    {
                        model.UnloadResources();
                    }
                }
            }

            {
                ModelBatch modelBatch;
                if (Batches.TryGetValue(name, out modelBatch) == true)
                {
                    ResourceManager.Remove(modelBatch);

                    Batches.Remove(name);

                    if (Disposed == false)
                    {
                        modelBatch.UnloadResources();
                    }
                }
            }

            {
                ModelFixedBatch modelBatch;
                if (FixedBatches.TryGetValue(name, out modelBatch) == true)
                {
                    ResourceManager.Remove(modelBatch);

                    FixedBatches.Remove(name);

                    if (Disposed == false)
                    {
                        modelBatch.UnloadResources();
                    }
                }
            }

            {
                ExtrudedModelBatch modelBatch;
                if (ExtrudedBatches.TryGetValue(name, out modelBatch) == true)
                {
                    ResourceManager.Remove(modelBatch);

                    ExtrudedBatches.Remove(name);

                    if (Disposed == false)
                    {
                        modelBatch.UnloadResources();
                    }
                }
            }
        }

        public void ClearOutAllBatches()
        {
            foreach (ModelBatch batch in Batches.Values) 
            {
                batch.UnloadResources(); 
            }
            Batches.Clear(); 

            foreach (ModelFixedBatch batch in FixedBatches.Values) 
            {
                batch.UnloadResources(); 
            }
            FixedBatches.Clear(); 

            foreach (ExtrudedModelBatch batch in ExtrudedBatches.Values) 
            {
                batch.UnloadResources(); 
            }
            ExtrudedBatches.Clear(); 
        }

		#endregion

        public void UpdateLighting()
        {
            for (int i = 0; i < Lighting.Lights.Length; i++)
            {
                ModelSceneLight light = Lights[i];

                light.Update();
            }
        }

        int frame = 0;
        int frameCutoff = 500; 

        public void Update(View3D view)
        {
            /* 
            if (frame++ > frameCutoff)
            {
                return; 
            }
            */

            /* 
            private List<ModelBatch> m_ModelBatches_NonReflective = new List<ModelBatch>();
            private List<ModelBatch> m_ModelBatches_Reflective = new List<ModelBatch>();

            private List<ExtrudedModelBatch> m_ExtrudedModelBatches_NonReflective = new List<ExtrudedModelBatch>();
            private List<ExtrudedModelBatch> m_ExtrudedModelBatches_Reflective = new List<ExtrudedModelBatch>();

            private List<ModelSceneObject> m_Objects_NonReflective = new List<ModelSceneObject>();
            private List<ModelSceneObject> m_Objects_Reflective = new List<ModelSceneObject>(); 
            */

            #region Clear Out Batches

            //m_ModelBatches_NonReflective.Clear();
            //m_ModelBatches_Reflective.Clear();

            //m_ExtrudedModelBatches_NonReflective.Clear();
            //m_ExtrudedModelBatches_Reflective.Clear();

            m_Objects_NonReflective.Clear();
            m_Objects_Reflective.Clear(); 

            foreach (ModelBatch batch in Batches.Values)
            {
                if (batch.Disposed == true)
                {
                    batch.LoadResources();
                }

                batch.CheckResize();

                batch.DesiredMaxCount = 0;
                batch.InstanceCount = 0;
            }

            foreach (ModelFixedBatch batch in FixedBatches.Values)
            {
                if (batch.Disposed == true)
                {
                    batch.LoadResources();
                }

                batch.CheckResize();

                batch.DesiredMaxCount = 0;
                batch.InstanceCount = 0;
            }

            foreach (ExtrudedModelBatch batch in ExtrudedBatches.Values)
            {
                if (batch.Disposed == true)
                {
                    batch.LoadResources();
                }

                batch.CheckResize();

                batch.DesiredMaxCount = 0;
                batch.InstanceCount = 0;
            }

            {
                DefaultSFXLineBatch.CheckResize();
                DefaultSFXLineBatch.DesiredMaxCount = 0;
                DefaultSFXLineBatch.InstanceCount = 0;
            }

            #endregion

            #region Update Scene Objects

            foreach (ModelSceneObject obj in Objects)
            {
                if (obj.IsStatic == false)
                {
                    obj.IsFixed = false;
                    obj.Update();
                }

                string model = obj.Model;

                if (obj.DoNotBatch == true)
                {
                    if (Models[model].IsReflective == false)
                    {
                        m_Objects_NonReflective.Add(obj);
                    }
                    else
                    {
                        m_Objects_Reflective.Add(obj); 
                    }

                    obj.Index = -1;
                    continue;
                }

                ModelBatch batch;

                if (Batches.TryGetValue(model, out batch) == false)
                {
                    ModelBase iModel = Models[model];

                    batch = new ModelBatch(iModel, 32, OpenTK.Graphics.OpenGL.BufferUsageHint.StreamDraw);

                    Batches.Add(model, batch);

                    ResourceManager.Add(batch);

                    if (iModel.IsReflective == false)
                    {
                        m_ModelBatches_NonReflective.Add(batch);
                    }
                    else
                    {
                        m_ModelBatches_Reflective.Add(batch);
                    }
                }

                if (batch.Disposed == true ||
                    batch.InstanceCount >= batch.MaxCount)
                {
                    if (Models[model].IsReflective == false)
                    {
                        m_Objects_NonReflective.Add(obj);
                    }
                    else
                    {
                        m_Objects_Reflective.Add(obj);
                    }

                    batch.DesiredMaxCount++;
                    obj.Index = -1;
                }
                else
                {
                    batch.DesiredMaxCount++;
                    obj.Index = batch.InstanceCount;

                    batch.Instances[batch.InstanceCount++] = obj.ToModelInstance();
                }
            }

            #endregion

            #region Update Fixed Scene Objects

            foreach (ModelSceneObject obj in FixedObjects)
            {
                if (obj.IsStatic == false)
                {
                    obj.IsFixed = true;
                    obj.Update();
                }

                string model = obj.Model;

                if (obj.DoNotBatch == true)
                {
                    if (Models[model].IsReflective == false)
                    {
                        m_Objects_NonReflective.Add(obj);
                    }
                    else
                    {
                        m_Objects_Reflective.Add(obj);
                    }

                    obj.Index = -1;
                    continue;
                }

                ModelFixedBatch batch;

                if (FixedBatches.TryGetValue(model, out batch) == false)
                {
                    ModelBase iModel = Models[model];

                    batch = new ModelFixedBatch(iModel, 32, OpenTK.Graphics.OpenGL.BufferUsageHint.StreamDraw); // DynamicDraw);

                    FixedBatches.Add(model, batch);

                    ResourceManager.Add(batch);

                    if (iModel.IsReflective == false)
                    {
                        m_ModelFixedBatches_NonReflective.Add(batch);
                    }
                    else
                    {
                        m_ModelFixedBatches_Reflective.Add(batch);
                    }
                }

                if (batch.Disposed == true ||
                    batch.InstanceCount >= batch.MaxCount)
                {
                    if (Models[model].IsReflective == false)
                    {
                        m_Objects_NonReflective.Add(obj);
                    }
                    else
                    {
                        m_Objects_Reflective.Add(obj);
                    }

                    batch.DesiredMaxCount++;
                    obj.Index = -1;
                }
                else
                {
                    batch.DesiredMaxCount++;
                    obj.Index = batch.InstanceCount;

                    batch.Instances[batch.InstanceCount++] = obj.ToModelFixedInstance();
                }
            }

            #endregion

            #region Update Model Chains

            foreach (ModelChain obj in Chains)
            {
                if (obj.IsVisible == false)
                {
                    continue;
                }

                obj.Update();

                string model = obj.Model;

                ExtrudedModelBatch batch;

                if (ExtrudedBatches.TryGetValue(model, out batch) == false)
                {
                    batch = new ExtrudedModelBatch(Models[model], 128);

                    ExtrudedBatches.Add(model, batch);

                    ResourceManager.Add(batch);
                }

                obj.AddToBatch(batch);
            }

            #endregion

            #region Update SFX Line Chains

            foreach (SFXLineChain obj in SFXChains)
            {
                if (obj.IsVisible == false)
                {
                    continue;
                }

                obj.Update();

                obj.AddToBatch(DefaultSFXLineBatch);
            }

            #endregion

            #region Update Batches

            foreach (ModelBatch batch in Batches.Values)
            {
                batch.Update();
            }

            foreach (ModelFixedBatch batch in FixedBatches.Values)
            {
                batch.Update();
            }

            foreach (ExtrudedModelBatch batch in ExtrudedBatches.Values)
            {
                batch.Update();
            }

            {
                DefaultSFXLineBatch.Update(); 
            }

            #endregion

            #region Update Lighting

            for (int i = 0; i < Lighting.Lights.Length; i++)
            {
                if (i >= Lights.Count)
                {
                    Lighting.Lights[i] = ModelLightInstance.Zero;

                    continue;
                }

                Lighting.Lights[i] = Lights[i].ToLightInstance(ref view.World, ref view.NormalWorld);
            }

            Lighting.Update();

            #endregion
        }

        public void Render(View3D view)
        {
            FogParams @default = Lighting.LightingArguments.FogParams; 

            if (LightingMode != Shaders.Flat.ModelLightingMode.Shadow)
            {
                Lighting.ConfigureTexturesForShadowCast();
            }

            Lighting.UpdateLightingArguments(view);

            bool isShadowPass = LightingMode == Shaders.Flat.ModelLightingMode.Shadow;

            foreach (ModelSceneObject obj in m_Objects_NonReflective)
            {
                ModelBase model = Models[obj.Model];

                if (model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = model.FogParams;
                }

                model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }

            #region Render Fixed Objects (Batch)

            foreach (ModelFixedBatch batch in m_ModelFixedBatches_NonReflective)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                batch.Render(view, Matrix4.Identity, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }            

            /* 
            foreach (ModelFixedBatch batch in FixedBatches.Values)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsReflective == true && isShadowPass == false)
                {
                    continue;
                }

                if (batch.Model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }                

                batch.Render(view, Matrix4.Identity, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Fixed Objects (NonBatch)



            /* 
            foreach (ModelSceneObject obj in FixedObjects)
            {
                if (obj.IsPartOfBatch == true)
                {
                    continue;
                }

                IModel model = Models[obj.Model];

                if (model.IsReflective == true && isShadowPass == false)
                {
                    continue;
                }

                if (model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = model.FogParams;
                }

                model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Objects (Batch)

            foreach (ModelBatch batch in m_ModelBatches_NonReflective)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }


            if (isShadowPass == true)
            {
                foreach (ModelFixedBatch batch in m_ModelFixedBatches_Reflective)
                {
                    if (batch.Disposed == true)
                    {
                        continue;
                    }

                    if (batch.Model.IsShadowCaster == false)
                    {
                        continue;
                    }

                    if (batch.Model.OverrideFog == true)
                    {
                        Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                    }

                    batch.Render(view, Matrix4.Identity, batch.Model.Texture, Lighting, LightingMode);

                    if (batch.Model.OverrideFog == true)
                    {
                        Lighting.LightingArguments.FogParams = @default;
                    }
                }

                foreach (ModelBatch batch in m_ModelBatches_Reflective)
                {
                    if (batch.Disposed == true)
                    {
                        continue;
                    }

                    if (batch.Model.IsShadowCaster == false)
                    {
                        continue;
                    }

                    if (batch.Model.OverrideFog == true)
                    {
                        Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                    }

                    batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                    if (batch.Model.OverrideFog == true)
                    {
                        Lighting.LightingArguments.FogParams = @default;
                    }
                }

                foreach (ModelSceneObject obj in m_Objects_Reflective)
                {
                    ModelBase model = Models[obj.Model];

                    if (model.IsShadowCaster == false)
                    {
                        continue;
                    }

                    if (model.OverrideFog == true)
                    {
                        Lighting.LightingArguments.FogParams = model.FogParams;
                    }

                    model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                    if (model.OverrideFog == true)
                    {
                        Lighting.LightingArguments.FogParams = @default;
                    }
                }
            }

            /* 
            foreach (ModelBatch batch in Batches.Values)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsReflective == true && isShadowPass == false)
                {
                    continue;
                }

                if (batch.Model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Objects (NonBatch)

            /* 
            foreach (ModelSceneObject obj in Objects)
            {
                if (obj.IsPartOfBatch == true)
                {
                    continue;
                }

                IModel model = Models[obj.Model];

                if (model.IsReflective == true && isShadowPass == false)
                {
                    continue;
                }

                if (model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = model.FogParams;
                }

                model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion
        
        }

        public void RenderChains(View3D view)
        {
            FogParams @default = Lighting.LightingArguments.FogParams; 

            bool isShadowPass = LightingMode == Shaders.Flat.ModelLightingMode.Shadow;

            bool isColorMode = LightingMode == Shaders.Flat.ModelLightingMode.Color;

            if (isShadowPass == false && isColorMode == false)
            {
                GLState.EnableDepthMask = false;
                GLState.Apply(view);

                DefaultSFXLineBatch.Render(view);

                foreach (SFXLineBatch batch in SFXLineBatches)
                {
                    batch.Render(view); 
                }

                GLState.EnableDepthMask = true;
                GLState.Apply(view);
            }   

            #region Render Chain Batches

            foreach (ExtrudedModelBatch batch in ExtrudedBatches.Values)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsReflective == true && isShadowPass == false)
                {
                    continue;
                }

                if (batch.Model.IsShadowCaster == false && isShadowPass == true)
                {
                    continue;
                }

                if (isColorMode == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }

            #endregion
        }

        public void RenderReflectiveObjects(View3D view, Texture2D sourceTexture, Texture2D depthTexture, TextureCube reflectionsCubeMap)
        {
            FogParams @default = Lighting.LightingArguments.FogParams; 

            Lighting.UpdateLightingArguments(view);

            Lighting.LightingArguments.SourceTexture = sourceTexture;
            Lighting.LightingArguments.DepthTexture = depthTexture;            
            Lighting.LightingArguments.ReflectionsCubeMap = reflectionsCubeMap; 

            /* 
            RefractionParams @params = new RefractionParams()
            {
                Distance = 0.12f,
                SpectrumSpread = 0.04f,
                Flip = false, 
            }; 

            foreach (ModelSceneObject obj in ReflectiveObjects)
            {
                //Models[obj.Model].RenderReflected(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, Lighting, sourceTexture, depthTexture);
                Models[obj.Model].RenderReflected(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, Lighting, sourceTexture, depthTexture, reflectionsCubeMap, @params);
                //Models[obj.Model].RenderReflected(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, Lighting, null, null);

                //Models[obj.Model].Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, Lighting, LightingMode);
            }
            */

            #region Render Fixed Objects (Batch)

            foreach (ModelFixedBatch batch in m_ModelFixedBatches_Reflective)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = batch.Model.RefractionParams;

                batch.Render(view, Matrix4.Identity, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }

            /* 
            foreach (ModelFixedBatch batch in FixedBatches.Values)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsReflective == false)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = batch.Model.RefractionParams;

                batch.Render(view, Matrix4.Identity, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Fixed Objects (NonBatch)

            foreach (ModelSceneObject obj in m_Objects_Reflective)
            {
                ModelBase model = Models[obj.Model];

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = model.RefractionParams;

                model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }

            /* 
            foreach (ModelSceneObject obj in FixedObjects)
            {                
                if (obj.IsPartOfBatch == true)
                {
                    continue;
                }

                IModel model = Models[obj.Model];

                if (model.IsReflective == false)
                {
                    continue;
                }

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = model.RefractionParams;

                model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Objects (Batch)

            foreach (ModelBatch batch in m_ModelBatches_Reflective)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = batch.Model.RefractionParams;

                batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }

            /* 
            foreach (ModelBatch batch in Batches.Values)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsReflective == false)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = batch.Model.RefractionParams;

                batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Objects (NonBatch)

            /* 
            foreach (ModelSceneObject obj in Objects)
            {
                if (obj.IsPartOfBatch == true)
                {
                    continue;
                }

                IModel model = Models[obj.Model];

                if (model.IsReflective == false)
                {
                    continue;
                }

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = model.RefractionParams;

                model.Render(view, obj.ObjectMatrix, obj.NormalMatrix, obj.ColorArg, model.Texture, Lighting, LightingMode);

                if (model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }
            */ 

            #endregion

            #region Render Chain Batches

            foreach (ExtrudedModelBatch batch in ExtrudedBatches.Values)
            {
                if (batch.Disposed == true)
                {
                    continue;
                }

                if (batch.Model.IsReflective == false)
                {
                    continue;
                }

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = batch.Model.FogParams;
                }

                Lighting.LightingArguments.RefractionParams = batch.Model.RefractionParams;

                batch.Render(view, batch.Model.Texture, Lighting, LightingMode);

                if (batch.Model.OverrideFog == true)
                {
                    Lighting.LightingArguments.FogParams = @default;
                }
            }

            #endregion
        }

		#region Resource Management

		public void LoadResources()
		{
			if (Disposed == false)
			{
				return; 
			}

			ResourceManager.LoadResources();

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1 + ResourceManager.Total; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= (this as IResourceManager).Count)
            {
                process.Pop();
                
                Disposed = false;
            }
            else
            {
                process.Increment();
                process.Push(ResourceManager);
            }
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

			ResourceManager.UnloadResources();

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}

		#endregion

    }
}
