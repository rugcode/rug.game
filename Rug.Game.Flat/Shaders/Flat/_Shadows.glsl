﻿
#section Shadows_Medium 
vec2 poissonDisk[4] = vec2[](
  vec2( -0.94201624, -0.39906216 ),
  vec2( 0.94558609, -0.76890725 ),
  vec2( -0.094184101, -0.92938870 ),
  vec2( 0.34495938, 0.29387760 )
);
#end

#section !Shadows_Low
#section !Shadows_Medium 
vec2 poissonDisk[9] = vec2[](
	vec2(-0.08848499, -0.1718515),
	vec2(0.2098641, 0.4700899),
	vec2(0.4483384, -0.7251894),
	vec2(-0.07633983, -0.9704938),
	vec2(-0.5603283, 0.06924847),
	vec2(0.6835836, 0.6720985),
	vec2(-0.8141588, -0.5225379),
	vec2(-0.3290946, 0.623399),
	vec2(0.8494582, 0.06793053)
); 
#end
#end

// ONE THAT HAS BEEN USED FOR A LONG TIME
//#define ZOFFSET 0.000045 
// CUT THROUGH 
//#define ZOFFSET -0.000045
// SHADOW ACEH
//#define ZOFFSET 0
// GOOD FOR THIN PLANTS BUT HAS ISSUES
//#define ZOFFSET -0.000025

//#define ZOFFSET -0.000005 
//#define ZOFFSET 0.000005 
#define ZOFFSET 0.0000000125 

//#define ZOFFSET 0.00045
//#define ZOFFSET 0.000015

#section Shadows_Preposterous 
vec4 cubic(in float x)
{
    float x2 = x * x;
    float x3 = x2 * x;
    
	vec4 w;
    
	w.x = -x3   + 3*x2 - 3*x + 1;
    w.y =  3*x3 - 6*x2       + 4;
    w.z = -3*x3 + 3*x2 + 3*x + 1;

    w.w =  x3;

    return w / 6.0;
}

float textureShadowCubic(sampler2DShadow map, vec3 lightSpacePos)
{
	ivec2 size = ivec2(textureSize(map, 0));

    //vec3 projCoords = lightSpacePos.xyz;
	vec3 projCoords_Original = lightSpacePos;    

	float result = 0.0; 
	vec3 projCoords;

	vec2 texcoord = projCoords_Original.xy * vec2(size); 
	vec2 texscale = vec2(1.0) / vec2(size); 

    float fx = fract(texcoord.x);
    float fy = fract(texcoord.y);

    texcoord.x -= fx;
    texcoord.y -= fy;

    vec4 xcubic = cubic(fx);
    vec4 ycubic = cubic(fy);

    vec4 c = vec4(texcoord.x - 0.5, texcoord.x + 1.5, texcoord.y - 0.5, texcoord.y + 1.5);
    vec4 s = vec4(xcubic.x + xcubic.y, xcubic.z + xcubic.w, ycubic.x + ycubic.y, ycubic.z + ycubic.w);
    vec4 offset = c + vec4(xcubic.y, xcubic.w, ycubic.y, ycubic.w) / s;

    float sample0 = texture(map, vec3(vec2(offset.x, offset.z) * texscale, projCoords_Original.z));
    float sample1 = texture(map, vec3(vec2(offset.y, offset.z) * texscale, projCoords_Original.z));
    float sample2 = texture(map, vec3(vec2(offset.x, offset.w) * texscale, projCoords_Original.z));
    float sample3 = texture(map, vec3(vec2(offset.y, offset.w) * texscale, projCoords_Original.z));

    float sx = s.x / (s.x + s.y);
    float sy = s.z / (s.z + s.w);

    return mix(
        mix(sample3, sample2, sx),
        mix(sample1, sample0, sx), sy);
}

vec2 SampleShadow_x9_Cubic(in sampler2DShadow map, in vec4 lightSpacePos, in float spread)
{
    //vec3 projCoords = lightSpacePos.xyz;
	vec3 projCoords_Original = lightSpacePos.xyz / lightSpacePos.w;    
    
	vec2 clamped = clamp(projCoords_Original.xy, vec2(0.0, 0.0), vec2(1.0, 1.0)); 

	if (projCoords_Original.xy != clamped) 
	{
		return vec2(0.0, 1.0);
	}

	projCoords_Original.z += ZOFFSET / spread;
	
	float result = 0.0; 
	vec3 projCoords;	
	float scale = spread; // 600.0;

	for (int i = 0; i < 9; i++)
	{
		projCoords = projCoords_Original;
		projCoords.xy += poissonDisk[i].yx / (scale * 6.0); 

		result += textureShadowCubic(map, projCoords);
	}

	result /= 9; 

	return vec2(1.0, result);
}
#end

#section Shadows_Superior
vec2 SampleShadow_x9(in sampler2DShadow map, in vec4 lightSpacePos, in float spread)
{
	vec3 projCoords_Original = lightSpacePos.xyz / lightSpacePos.w;    
	
	vec2 clamped = clamp(projCoords_Original.xy, vec2(0.0, 0.0), vec2(1.0, 1.0)); 

	if (projCoords_Original.xy != clamped) 
	{
		return vec2(0.0, 1.0);
	}
	    
	projCoords_Original.z += ZOFFSET / spread;
	
	float result = 0.0; 
	vec3 projCoords;

	float scale = spread; // 600.0; 

	for (int i = 0; i < 9; i++)
	{
		projCoords = projCoords_Original;
		projCoords.xy += poissonDisk[i].yx / (scale * 6.0); 

		result += texture(map, projCoords);
	}

	result /= 9; 

	return vec2(1.0, result);
}
#end

#section Shadows_Acceptable
vec2 SampleShadow_x4(in sampler2DShadow map, in vec4 lightSpacePos, in float spread)
{
	vec3 projCoords_Original = lightSpacePos.xyz / lightSpacePos.w;    
    
	vec2 clamped = clamp(projCoords_Original.xy, vec2(0.0, 0.0), vec2(1.0, 1.0)); 

	if (projCoords_Original.xy != clamped) 
	{
		return vec2(0.0, 1.0);
	}

	projCoords_Original.z += ZOFFSET / spread;
	
	float result = 0.0; 
	vec3 projCoords;
	float scale = spread; 

	for (int i = 0; i < 4; i++)
	{
		projCoords = projCoords_Original;
		projCoords.xy += poissonDisk[i].yx / (scale * 6); 

		result += texture(map, projCoords);
	}

	result /= 4; 

	return vec2(1.0, result);
}
#end

#section Shadows_Terrible
vec2 SampleShadow_Nearest(sampler2DShadow map, vec4 lightSpacePos)
{
	vec3 projCoords_Original = lightSpacePos.xyz / lightSpacePos.w;    
    
	vec2 clamped = clamp(projCoords_Original.xy, vec2(0.0, 0.0), vec2(1.0, 1.0)); 

	if (projCoords_Original.xy != clamped) 
	{
		return vec2(0.0, 1.0);
	}

	projCoords_Original.z += ZOFFSET;
	
	return vec2(1.0, texture(map, projCoords_Original));	 
}	
#end