﻿// GLSL GEOMETRY SHADER
#version 410

uniform mat4 objectMatrix;
uniform mat4 worldMatrix;
uniform mat4 normalWorldMatrix;
uniform mat4 perspectiveMatrix;

uniform vec3 toCameraA = vec3(0.0, 1.0, 0.0);
uniform vec3 toCameraB = vec3(1.0, 0.0, 0.0);

// line_strip_adjacency
layout(lines_adjacency) in;
layout(triangle_strip, max_vertices = 24) out;

vec4 verts[8]; // Scratch space for the eight corners of the prismoid
vec3 normals[8];

in VertexData{
	vec4 color_var;
	vec3 normal_var;
	float radius_var;
} VertexIn[];

out VertexData{	
	vec4 color_var;
	vec3 normal_var;
} VertexOut;


void emit(int a, int b, int c, int d, vec4 color1, vec4 color2)
{
    gl_Position = verts[a]; 
	VertexOut.color_var = color1; 
	VertexOut.normal_var = normals[a];
	EmitVertex();

    gl_Position = verts[b]; 
	VertexOut.color_var = color1; 
	VertexOut.normal_var = normals[b];
	EmitVertex();

    gl_Position = verts[c]; 
	VertexOut.color_var = color2; 
	VertexOut.normal_var = normals[c];
	EmitVertex();

    gl_Position = verts[d]; 
	VertexOut.color_var = color2; 
	VertexOut.normal_var = normals[d];
	EmitVertex();

    EndPrimitive();
}

void main()
{
	mat4 matrix = perspectiveMatrix * worldMatrix * objectMatrix;

    // Compute orientation vectors for the two connecting faces:
    vec3 p0, p1, p2, p3;

	p0 = gl_in[0].gl_Position.xyz; 
	p1 = gl_in[1].gl_Position.xyz;
	p2 = gl_in[2].gl_Position.xyz; 
	p3 = gl_in[3].gl_Position.xyz;

	vec3 n0 = normalize(p1-p0);
	vec3 n1 = normalize(p2-p1);
	vec3 n2 = normalize(p3-p2);
	vec3 u = normalize(n0+n1);
	vec3 v = normalize(n1+n2);

#section Include_VertexNormal
	vec3 normal1 = VertexIn[1].normal_var;
	vec3 normal2 = VertexIn[2].normal_var;
#end

#section !Include_VertexNormal
	vec3 normal1 = toCameraA;	
	float vectorDot = dot(n1, normal1); 
	if (vectorDot < -0.5 || vectorDot > 0.5) { normal1 = toCameraB; }

	vec3 normal2 = toCameraA;	
	vectorDot = dot(n2, normal2); 
	if (vectorDot < -0.5 || vectorDot > 0.5) { normal2 = toCameraB; }
#end

	// Declare scratch variables for basis vectors:
	vec3 i,j,k; 

	float r = VertexIn[1].radius_var; // Radius;

	// Compute face 1 of 2:
	j = u; i = normal1; k = cross(i, j); i *= r; k *= r;

	normals[0] = vec3(+ i + k);
	normals[1] = vec3(+ i - k);
	normals[2] = vec3(- i - k);
	normals[3] = vec3(- i + k);	
/* 
	normals[0] = (normalWorldMatrix * vec4(+ i + k, 0.0)).xyz;
	normals[1] = (normalWorldMatrix * vec4(+ i - k, 0.0)).xyz;
	normals[2] = (normalWorldMatrix * vec4(- i - k, 0.0)).xyz;
	normals[3] = (normalWorldMatrix * vec4(- i + k, 0.0)).xyz;	
*/ 

	verts[0] = matrix * vec4(p1 + i + k, 1);
	verts[1] = matrix * vec4(p1 + i - k, 1);
	verts[2] = matrix * vec4(p1 - i - k, 1);
	verts[3] = matrix * vec4(p1 - i + k, 1);

	r = VertexIn[2].radius_var; // Radius;

	// Compute face 2 of 2:
	j = v; i = normal2; k = cross(i, j); i *= r; k *= r;

	normals[4] = vec3(+ i + k);
	normals[5] = vec3(+ i - k);
	normals[6] = vec3(- i - k);
	normals[7] = vec3(- i + k);	

/* 
	normals[4] = (normalWorldMatrix * vec4(+ i + k, 0.0)).xyz;
	normals[5] = (normalWorldMatrix * vec4(+ i - k, 0.0)).xyz;
	normals[6] = (normalWorldMatrix * vec4(- i - k, 0.0)).xyz;
	normals[7] = (normalWorldMatrix * vec4(- i + k, 0.0)).xyz;	
*/ 

	verts[4] = matrix * vec4(p2 + i + k, 1);
	verts[5] = matrix * vec4(p2 + i - k, 1);
	verts[6] = matrix * vec4(p2 - i - k, 1);
	verts[7] = matrix * vec4(p2 - i + k, 1);

	vec4 color1 = VertexIn[1].color_var; 
	vec4 color2 = VertexIn[2].color_var; 

	// Emit the six faces of the prismoid:

#section Include_EndCap
	emit(0,1,3,2, color1, color1); 
	emit(5,4,6,7, color2, color2);
#end

#section !Include_EndCap
	emit(4,5,0,1, color2, color1); // away 
	emit(3,2,7,6, color1, color2); // towards
	emit(0,3,4,7, color1, color2); // left 
	emit(2,1,6,5, color1, color2); // right 
#end
}