﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Flat.Models.Data;

namespace Rug.Game.Flat.Shaders.Flat
{    
    public class ModelLineEffect : GeometryEffect
    {
		#region Private Members

		private int uObjectMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;
        private int uToCameraA;
        private int uToCameraB;
        private int uNormalWorldMatrix;   

		#endregion

		public override string Name
		{
			get { return "Model Line Effect"; }
		}

		public override string ShaderLocation_Frag { get { return @"~/Shaders/Flat/ModelLineEffect"; } }
        public override string ShaderLocation_Geom { get { return @"~/Shaders/Flat/ModelLineEffect"; } }
        public override string ShaderLocation_Vert { get { return @"~/Shaders/Flat/ModelLineEffect"; } }

        public ModelLineEffect() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, ref Vector3 normal, bool normalOverride, View3D view)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
            GL.UniformMatrix4(uNormalWorldMatrix, false, ref view.NormalWorld);

            //GL.Uniform3(uToCameraA, new Vector3(1, 0, 0)); // -view.Camera.Forward);
            //GL.Uniform3(uToCameraB, new Vector3(0, 1, 0)); // -view.Camera.Up);            

            if (normalOverride == true)
            {
                GL.Uniform3(uToCameraA, normal);
                GL.Uniform3(uToCameraB, normal);
            }
            else
            {
                GL.Uniform3(uToCameraA, new Vector3(0, 1, 0)); // -view.Camera.Up);
                GL.Uniform3(uToCameraB, new Vector3(1, 0, 0)); // -view.Camera.Forward);
            }

            //GL.Uniform3(uToCameraA, -view.Camera.Up);
            //GL.Uniform3(uToCameraB, -view.Camera.Forward);
		}

		public void Render(ref Matrix4 objectMatrix, int indexCount, DrawElementsType indexType)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.DrawElements(PrimitiveType.Lines, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void Render(ref Matrix4 objectMatrix, VertexBuffer verts)
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
            SFXLineVertex.Bind();

			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.DrawArrays(PrimitiveType.Lines, 0, 6);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

            SFXLineVertex.Unbind();

		}

		public void Render(ref Matrix4 objectMatrix, VertexBuffer verts, int count)
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
            SFXLineVertex.Bind();

			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.DrawArrays(PrimitiveType.LinesAdjacency, 0, count);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

            SFXLineVertex.Unbind();

		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
            uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");

            uToCameraA = GL.GetUniformLocation(ProgramHandle, "toCameraA");
            uToCameraB = GL.GetUniformLocation(ProgramHandle, "toCameraB");   

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
            GL.ProgramParameter(ProgramHandle, AssemblyProgramParameterArb.GeometryInputType, (int)All.LinesAdjacency);
            GL.ProgramParameter(ProgramHandle, AssemblyProgramParameterArb.GeometryOutputType, (int)All.TriangleStrip);

            int tmp;
            // Get the maximum amount of vertices into tmp.
            GL.GetInteger((GetPName)ExtGeometryShader4.MaxGeometryOutputVerticesExt, out tmp);
            GL.ProgramParameter(ProgramHandle, AssemblyProgramParameterArb.GeometryVerticesOut, tmp);

			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");			
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}