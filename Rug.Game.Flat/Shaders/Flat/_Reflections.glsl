﻿#define NEAR_EDGE_START 0.05
#define NEAR_EDGE_END 0.2

#define FAR_EDGE_START 0.95
#define FAR_EDGE_END 0.8

vec4 SampleRefraction(in vec4 surfaceColor, in vec3 surfaceNormal, in vec3 surfaceNormalInWorldFrame)
{
	vec4 finalColor = vec4(0); 

	ivec2 soureSize = ivec2(textureSize2D(sourceTexture, 0)); 

	vec2 source_UV = gl_FragCoord.xy / soureSize; 

	float spreadAmount = 1.0 - abs(dot(surfaceNormal, vec3(0, 0, 1)));
	float refractionBase = reflectionParams.x; // 0.06; 
	float spreadFactor = reflectionParams.y; // 0.02; 	
	float reflectiveness = reflectionParams.w; 

	vec3 lightAccum = vec3(0.0);

	vec3 normalInv = vec3(surfaceNormal.xy * reflectionParams.z, surfaceNormal.z); 

	float intensity = abs(dot(surfaceNormal, vec3(0, 0, 1))) * gl_FragCoord.z;

#section RefractionQuality_Preposterous
	vec2 texcoord0 = source_UV + ((normalInv.xy * refractionBase)); 
	vec2 texcoord1 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.125)); 
	vec2 texcoord2 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.250)); 
	vec2 texcoord3 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.375)); 
	vec2 texcoord4 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.500)); 
	vec2 texcoord5 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.625)); 
	vec2 texcoord6 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.750)); 
	vec2 texcoord7 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.875)); 
	vec2 texcoord8 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.875)); 
	vec2 texcoord9 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 1.000)); 

	intensity *= 
		smoothstep(NEAR_EDGE_START, NEAR_EDGE_END, texcoord0.x) * smoothstep(FAR_EDGE_START, FAR_EDGE_END, texcoord0.x) *
		smoothstep(NEAR_EDGE_START, NEAR_EDGE_END, texcoord0.y) * smoothstep(FAR_EDGE_START, FAR_EDGE_END, texcoord0.y);

	lightAccum += texture(sourceTexture, texcoord0).rgb * (vec3(1.00, 0.00, 0.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord1).rgb * (vec3(0.75, 0.25, 0.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord2).rgb * (vec3(0.50, 0.50, 0.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord3).rgb * (vec3(0.25, 0.75, 0.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord4).rgb * (vec3(0.00, 1.00, 0.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord5).rgb * (vec3(0.00, 0.75, 0.25)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord6).rgb * (vec3(0.00, 0.50, 0.50)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord7).rgb * (vec3(0.00, 0.25, 0.75)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord8).rgb * (vec3(0.00, 0.00, 1.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord9).rgb * (vec3(0.00, 0.00, 0.75)) * color_var.a;	

	lightAccum *= 0.35;
#end

#section RefractionQuality_Superior
	vec2 texcoord0 = source_UV + ((normalInv.xy * refractionBase)); 
	vec2 texcoord1 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 0.5)); 
	vec2 texcoord2 = source_UV + ((normalInv.xy * refractionBase) + (normalInv.xy * spreadAmount * spreadFactor * 1.0)); 	

	intensity *= 
		smoothstep(NEAR_EDGE_START, NEAR_EDGE_END, texcoord0.x) * smoothstep(FAR_EDGE_START, FAR_EDGE_END, texcoord0.x) *
		smoothstep(NEAR_EDGE_START, NEAR_EDGE_END, texcoord0.y) * smoothstep(FAR_EDGE_START, FAR_EDGE_END, texcoord0.y);

	lightAccum += texture(sourceTexture, texcoord0).rgb * (vec3(0.75, 0.25, 0.00)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord1).rgb * (vec3(0.25, 0.50, 0.25)) * color_var.a;
	lightAccum += texture(sourceTexture, texcoord2).rgb * (vec3(0.00, 0.25, 0.75)) * color_var.a;	
#end

#section RefractionQuality_Acceptable
	vec2 texcoord0 = source_UV + (normalInv.xy * refractionBase); 

	intensity *= 
		smoothstep(NEAR_EDGE_START, NEAR_EDGE_END, texcoord0.x) * smoothstep(FAR_EDGE_START, FAR_EDGE_END, texcoord0.x) *
		smoothstep(NEAR_EDGE_START, NEAR_EDGE_END, texcoord0.y) * smoothstep(FAR_EDGE_START, FAR_EDGE_END, texcoord0.y);

	lightAccum += texture(sourceTexture, texcoord0).rgb * surfaceColor.a;
#end

	lightAccum *= intensity;

	// it maybe that we need to flip the normal here or get the reflection of the world 
	// this should possibly use the reflection vector rather than just the surface normal
	lightAccum += 
		mix(texture(sourceTexture, source_UV).rgb, 
			texture(reflectionsCubeMap, reflection_var).rgb * 3.0, reflectiveness) * (1.0 - intensity);

	//lightAccum += texture(reflectionsCubeMap, reflection_var).rgb * 3.0 * reflectiveness;

	finalColor.rgb = lightAccum * surfaceColor.rgb; 	

#section !RefractionQuality_Terrible
	finalColor.a = 1.0; 
	//finalColor.a = mix(intensity, 1.0, reflectiveness); //mix( , 1.0, intensity); // * (1.0 - intensity);
#end

#section RefractionQuality_Terrible
	finalColor.a = max((1.0 - intensity), (1.0 - surfaceColor.a));
#end
	
	//finalColor = pow(texture(reflectionsCubeMap, surfaceNormalInWorldFrame), vec4(2.0)) * 16.0;
	//finalColor = texture(reflectionsCubeMap, surfaceNormalInWorldFrame) * 5.0;
	//finalColor.a = 1f; 

	return finalColor; 
}