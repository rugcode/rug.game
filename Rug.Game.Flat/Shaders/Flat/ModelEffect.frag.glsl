#version 410
#extension GL_EXT_gpu_shader4 : enable
#extension GL_ARB_shading_language_packing : enable

in vec4 viewspace_var;
#section Lighting_UseRefractions
in vec3 reflection_var; 
#end

#section IncludeNormal
in vec3 normal_var;
#end
#section IncludeNormal_World				
in vec3 world_normal_var;
#end

#section !Lighting_Shadow
#section !Lighting_Normal
	#include ~/Shaders/Flat/_Fog.glsl
#end
#end

#section UsePerPixelLighting
	#include ~/Shaders/Flat/_Lighting.glsl

	layout(std140) uniform modelLightBlock
	{
		ModelLight ModelLights[$Lights + $ShadowLights];
	};
#end


#section !Lighting_Shadow
	#section Lighting_UseShadowMaps
		#include ~/Shaders/Flat/_Shadows.glsl
	#end 

	layout(location = 0) out vec4 colorFrag;
	in vec4 color_var;
	flat in vec4 instance_color_var;

	#section Mode_Extruded
		in float line_instance_alpha_var; 
	#end 
				
	#section UseTextures
		uniform sampler2D textureSource;
		in vec2 texture_var;
	#end 			

	#section Lighting_UseRefractions					
		uniform sampler2D sourceTexture; 
		uniform samplerCube reflectionsCubeMap;

		uniform mat4 perspectiveMatrix;
		uniform vec4 reflectionParams = vec4(0.06, 0.04, -1, 1.0);  

		#include ~/Shaders/Flat/_Reflections.glsl			
	#end 

	#section !Lighting_NoLighting


		#section !Lighting_CelPerPixel
			in vec4 lighting_points;
		#end 

		#section Lighting_CelPerPixel
			#include ~/Shaders/Flat/_Lighting.glsl
			#foreach $i in Lights
				in vec3 light_dir_$i;
				in vec4 light_color_$i;
			#forend
		#end 

		#section !Lighting_CelPerPixel
			#foreach $i in ShadowLights				
				in vec4 lighting_shadow_$i;		
				#foreach $j in ShadowsPerLight	
					in vec4 shadow_var_$i_$j;
					uniform vec3 shadow_min_max_$i_$j;
					uniform sampler2DShadow shadowTexture_$i_$j; 
				#forend
			#forend
		#end
	#end
#end

#section Lighting_Shadow 
	layout(location = 0) out float fragmentdepth;
#end

#section UsePerPixelLighting
	in float specularFactor_var;
	in float specularAmount_var;
#end

void main()
{
#section !Lighting_Shadow
	
	vec4 diffuseColor = color_var; 

	#section UseTextures
		diffuseColor *= texture(textureSource, texture_var); 
	#end

	#section !Lighting_NoLighting
		
		vec2 temp_emmisive_alpha = unpackHalf2x16(floatBitsToUint(instance_color_var.a));	
		float emissiveness = temp_emmisive_alpha.x; 
		float final_alpha = temp_emmisive_alpha.y; 

		vec3 lighting_ambient = vec3(0.0, 0.0, 0.0); // vec3(0.075, 0.075, 0.075); 
		vec4 lightAccum = vec4(0.0); 		

		#section !Lighting_CelPerPixel
			vec2 shadowValue = vec2(0.0, 1.0); 
			float depthValue = gl_FragCoord.z / gl_FragCoord.w; 

			#foreach $i in ShadowLights	
				shadowValue = vec2(0.0, 1.0); 

				#foreach $j in ShadowsPerLight		
					//$ifelse_j (depthValue > shadow_min_max_$i_$j.x && depthValue < shadow_min_max_$i_$j.y) 
					if (shadowValue.x == 0.0 && depthValue > shadow_min_max_$i_$j.x && depthValue < shadow_min_max_$i_$j.y) 
					{
					#section Shadows_Preposterous 
						shadowValue = SampleShadow_x9_Cubic(shadowTexture_$i_$j, shadow_var_$i_$j, shadow_min_max_$i_$j.z);
					#end
					#section Shadows_Superior 
						shadowValue = SampleShadow_x9(shadowTexture_$i_$j, shadow_var_$i_$j, shadow_min_max_$i_$j.z);
					#end
					#section Shadows_Acceptable 
						shadowValue = SampleShadow_x4(shadowTexture_$i_$j, shadow_var_$i_$j, shadow_min_max_$i_$j.z);
					#end
					#section Shadows_Terrible
						shadowValue = SampleShadow_Nearest(shadowTexture_$i_$j, shadow_var_$i_$j);
					#end
					}
				#forend

				#section !Shadows_Disabled
					lightAccum += mix(vec4(instance_color_var.rgb, 0), lighting_shadow_$i * shadowValue.y, emissiveness); 
				#end 

				#section Shadows_Disabled
					lightAccum += mix(vec4(instance_color_var.rgb, 0), lighting_shadow_$i, emissiveness); 
				#end
			#forend
		#end

		#section Lighting_CelPerPixel
			#foreach $i in Lights				
				lightAccum += GetLighting_CelPerPixel_Diffuse(normal_var, light_dir_$i, light_color_$i, 0.35, 32);
			#forend

			colorFrag = vec4(max(lightAccum.rgb * diffuseColor.rgb, lighting_ambient * diffuseColor.rgb) + vec3(lightAccum.a), diffuseColor.a);
		#end

		#section !Lighting_CelPerPixel
			vec4 lighting_points_final = lighting_points; 

			#section UsePerPixelLighting
				#foreach $i in Lights
					#section Lighting_DiffuseSpecular
						lighting_points_final += GetLighting_DiffuseSpecular(viewspace_var, normal_var.xyz, ModelLights[$ShadowLights + $i], specularFactor_var, specularAmount_var);
					#end
					#section Lighting_Diffuse
						lighting_points_final += GetLighting_Diffuse(viewspace_var, normal_var.xyz, ModelLights[$ShadowLights + $i]);
					#end
				#forend
			#end

			lightAccum.rgb = mix(diffuseColor.rgb, (lightAccum.rgb + lighting_points_final.rgb) * diffuseColor.rgb, emissiveness);
			lightAccum.a += lighting_points.a;

			lightAccum.rgb = max(lightAccum.rgb, lighting_ambient * diffuseColor.rgb);

			#section Lighting_UseRefractions
				colorFrag = SampleRefraction(diffuseColor, normal_var, world_normal_var);
				colorFrag.rgb += vec3(lightAccum.a);
			#end
			#section !Lighting_UseRefractions
				colorFrag = vec4(lightAccum.rgb + vec3(lightAccum.a), diffuseColor.a);
				//colorFrag = vec4(lighting_points.rgb, 1.0);
			#end
		#end	

		colorFrag.a *= final_alpha; 
	#end
#end

#section Lighting_Outline
	colorFrag = vec4(0.0, 0.0, 0.0, color_var.a);
#end

#section Lighting_Color
	#section !Lighting_UseRefractions
		colorFrag = diffuseColor;	
	#end
	#section Lighting_UseRefractions
		colorFrag = SampleRefraction(diffuseColor, normal_var, world_normal_var);
	#end
#end

#section Lighting_Normal
	colorFrag = diffuseColor;	
#end

#section Lighting_Shadow
	fragmentdepth = gl_FragCoord.z;	
#end

#section !Lighting_Shadow
#section !Lighting_Normal
	#section Lighting_UseRefractions
		#section Mode_Extruded
			colorFrag.rgb = applyFog_NoRed(colorFrag.rgb, length(viewspace_var.z));
		#end

		#section !Mode_Extruded
			colorFrag.rgb = applyFog(colorFrag.rgb, length(viewspace_var.z));
		#end
	#end

	#section !Lighting_UseRefractions
		colorFrag.rgb = applyFog(colorFrag.rgb, length(viewspace_var.z));
	#end 

	#section Mode_Extruded
		colorFrag.a *= line_instance_alpha_var; 
	#end
#end
#end
}