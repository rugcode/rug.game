﻿#version 410

layout(location = 0) out vec4 colorFrag;

in VertexData{
	vec4 color_var;
	vec3 normal_var;
} VertexOut;

//in vec4 color_var;

void main()
{
	colorFrag = VertexOut.color_var;
	//colorFrag = vec4(VertexOut.normal_var * 0.5f + 0.5f, 1.0);
}