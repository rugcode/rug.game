﻿#version 410

uniform mat4 objectMatrix;
uniform mat4 worldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec3 normal;

out VertexData{
	vec4 color_var;
	vec3 normal_var;
	float radius_var;
} VertexIn;

void main()
{
	/* 
	vec4 localSpace = objectMatrix * vec4(position, 1);
	vec4 worldSpace = worldMatrix * localSpace;
	gl_Position = perspectiveMatrix * worldSpace;
	*/ 

	gl_Position = vec4(position.xyz, 1);
	
	VertexIn.color_var = color;
	VertexIn.normal_var = normal;
	VertexIn.radius_var = position.w;
}