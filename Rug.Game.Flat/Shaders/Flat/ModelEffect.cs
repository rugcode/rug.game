﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;

namespace Rug.Game.Flat.Shaders.Flat
{
	public enum ModelEffectMode 
	{
		Batch, 
		FixedBatch,
		NonBatch,
        Extruded,
	}

    [Flags] 
	public enum ModelLightingMode : int
	{
        None = 0, 
		Diffuse = 1, 
		DiffuseSpecular = 2,
        CelPerPixel = 4,

        Refractive = 8,
        RefractiveSpecular = 16,

        // extra ones 
        Outline = 32,
        Color = 64, 
        Shadow = 128,
        Normal = 256,

        DiffuseSpecularPerPixel = 512,

        All = Diffuse | DiffuseSpecular | CelPerPixel | Refractive | RefractiveSpecular | Outline | Color | Shadow | Normal | DiffuseSpecularPerPixel, 
	}

    public enum ModelTextureMode
    {
        Untextured = 0, 
        Textured = 1,
    }

    public struct RefractionParams
    {
        public float Coverage;
        public float PrismQuality;
        public float Reflectiveness;
        public bool Flip;

        public static RefractionParams FromVector4(Vector4 vector)
        {
            RefractionParams @params = new RefractionParams()
            {
                Coverage = vector.X * 4f,
                PrismQuality = 1f - vector.Y,
                Flip = vector.Z > 0,
                Reflectiveness = vector.W, 
            };

            return @params;
        }

        public static implicit operator Vector4(RefractionParams @params)
        {
            return new Vector4(@params.Coverage * 0.25f, 1f - @params.PrismQuality, @params.Flip ? 1f : -1f, @params.Reflectiveness);
        }
    }

    public struct FogParams
    {
        public static FogParams Default = new FogParams()
        {
            Color = new Color3(0.4f, 0.5f, 0.6f), 
            Factor = 0.0025f,
            //Color = new Color3(0.4f, 0.5f, 0.6f),
            //Factor = 0.0025f * 0.25f,
            //Color = new Color3(0.4f, 0.5f, 0.6f),
            //Factor = 0.0000625f,
        };

        public static FogParams RedDefault = new FogParams()
        {
            Color = new Color3(0.486f, 0.055f, 0.055f),
            Factor = 32.0f,
        };

        public Color3 Color;

        public float Factor;

        public static implicit operator Vector4(FogParams @params)
        {
            return new Vector4(@params.Color.R, @params.Color.G, @params.Color.B, @params.Factor);
        }
    }

    public class LightingArguments
    {
        public Matrix4 WorldMatrix; 
        public Matrix4 NormalWorldMatrix; 
        public Matrix4 ProjectionMatrix; 
        
        public Matrix4[,] ShadowMatrices; 
        public Texture2D[,] ShadowTextures;
        public Vector3[,] ShadowMinMax;

        public TextureCube AmbiantCubeMap;

        public Texture2D SourceTexture; 
        public Texture2D DepthTexture;
        public TextureCube ReflectionsCubeMap;
        public RefractionParams RefractionParams;

        public FogParams FogParams = FogParams.Default;
        public FogParams RedParams = FogParams.RedDefault;
        public Vector3 CameraCenter;

        public void Update(Core.Rendering.View3D view)
        {
            WorldMatrix = view.World; 
            NormalWorldMatrix = view.NormalWorld;
            ProjectionMatrix = view.Projection;

            CameraCenter = Vector3.Transform(Vector3.Zero,
                Matrix4.CreateTranslation(view.Camera.Forward * view.Camera.Offset) *
                Matrix4.CreateTranslation(view.Camera.Center));
        }

        public void Update_Center(Core.Rendering.View3D view)
        {
            Matrix4 world = view.World;

            world = world.ClearTranslation();

            WorldMatrix = world;
            NormalWorldMatrix = view.NormalWorld;
            ProjectionMatrix = view.Projection;

            CameraCenter = Vector3.Transform(Vector3.Zero,
                Matrix4.CreateTranslation(view.Camera.Forward * view.Camera.Offset) *
                Matrix4.CreateTranslation(view.Camera.Center));
        }
    }
	
	public class ModelEffect : BasicEffectBase
	{
		#region Register Shaders Helper

        public static void RegisterAllShaders(int shadowlightCount, int maxLights, ModelTextureMode textured)
        {
            RegisterAllShaders(ModelLightingMode.All, shadowlightCount, maxLights, textured);
        }

        public static void RegisterAllShaders(ModelLightingMode modeFlags, int shadowlightCount, int numberOfLights, ModelTextureMode textured)
		{
            /* 
            for (int i = 1; i < 4; i++)
            {
                ModelEffect.RegisterShaders(modeFlags, shadowlightCount, i, textured);                
            }

            for (int i = 4; i <= numberOfLights;)
            {
                ModelEffect.RegisterShaders(modeFlags, shadowlightCount, i, textured);

                i *= 2; 
            }
            */ 

            ModelEffect.RegisterShaders(modeFlags, shadowlightCount, numberOfLights, textured);            

            if ((modeFlags & ModelLightingMode.Outline) == ModelLightingMode.Outline)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.Outline, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.Outline, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.Outline, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.Outline, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
            }

            if ((modeFlags & ModelLightingMode.Color) == ModelLightingMode.Color)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.Color, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.Color, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.Color, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.Color, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
            }

            if ((modeFlags & ModelLightingMode.Shadow) == ModelLightingMode.Shadow)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.Shadow, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.Shadow, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.Shadow, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.Shadow, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
            }

            if ((modeFlags & ModelLightingMode.Normal) == ModelLightingMode.Normal)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.Normal, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.Normal, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.Normal, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.Normal, VisualQuality.Disabled, 0, 0, textured, VisualQuality.Disabled);
            }
		}

        public static void RegisterShaders(ModelLightingMode modeFlags, int shadowlightCount, int lightCount, ModelTextureMode textured)
        {
            RegisterShaders(modeFlags, VisualQuality.Preposterous, shadowlightCount, lightCount, textured);
            RegisterShaders(modeFlags, VisualQuality.Superior, shadowlightCount, lightCount, textured);
            RegisterShaders(modeFlags, VisualQuality.Acceptable, shadowlightCount, lightCount, textured);
            RegisterShaders(modeFlags, VisualQuality.Terrible, shadowlightCount, lightCount, textured);
            RegisterShaders(modeFlags, VisualQuality.Disabled, shadowlightCount, lightCount, textured);

            if ((SharedEffects.Effects.ContainsKey(EffectName(ModelEffectMode.Batch, ModelLightingMode.CelPerPixel, VisualQuality.Disabled, 0, lightCount, textured, VisualQuality.Disabled)) == false) && 
                ((modeFlags & ModelLightingMode.CelPerPixel) == ModelLightingMode.CelPerPixel))
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.CelPerPixel, VisualQuality.Disabled, 0, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.CelPerPixel, VisualQuality.Disabled, 0, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.CelPerPixel, VisualQuality.Disabled, 0, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.CelPerPixel, VisualQuality.Disabled, 0, lightCount, textured, VisualQuality.Disabled);
            }
        }

        public static void RegisterShaders(ModelLightingMode modeFlags, 
            VisualQuality shadowQuality, int shadowlightCount, int lightCount, 
            ModelTextureMode textured)
        {
            if ((modeFlags & ModelLightingMode.DiffuseSpecular) == ModelLightingMode.DiffuseSpecular)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.DiffuseSpecular, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.DiffuseSpecular, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.DiffuseSpecular, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.DiffuseSpecular, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
            }

            if ((modeFlags & ModelLightingMode.Diffuse) == ModelLightingMode.Diffuse)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.Diffuse, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.Diffuse, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.Diffuse, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.Diffuse, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
            }

            if ((modeFlags & ModelLightingMode.DiffuseSpecularPerPixel) == ModelLightingMode.DiffuseSpecularPerPixel)
            {
                RegisterShader(ModelEffectMode.Batch, ModelLightingMode.DiffuseSpecularPerPixel, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.DiffuseSpecularPerPixel, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.DiffuseSpecularPerPixel, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
                RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.DiffuseSpecularPerPixel, shadowQuality, shadowlightCount, lightCount, textured, VisualQuality.Disabled);
            }

            for (int i = 0; i <= (int)VisualQuality.Preposterous; i++)
            {
                if ((modeFlags & ModelLightingMode.Refractive) == ModelLightingMode.Refractive)
                {
                    RegisterShader(ModelEffectMode.Batch, ModelLightingMode.Refractive, VisualQuality.Disabled, 0, 0, textured, (VisualQuality)i);
                    RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.Refractive, VisualQuality.Disabled, 0, 0, textured, (VisualQuality)i);
                    RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.Refractive, VisualQuality.Disabled, 0, 0, textured, (VisualQuality)i);
                    RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.Refractive, VisualQuality.Disabled, 0, 0, textured, (VisualQuality)i);
                }

                if ((modeFlags & ModelLightingMode.RefractiveSpecular) == ModelLightingMode.RefractiveSpecular)
                {
                    RegisterShader(ModelEffectMode.Batch, ModelLightingMode.RefractiveSpecular, shadowQuality, shadowlightCount, lightCount, textured, (VisualQuality)i);
                    RegisterShader(ModelEffectMode.FixedBatch, ModelLightingMode.RefractiveSpecular, shadowQuality, shadowlightCount, lightCount, textured, (VisualQuality)i);
                    RegisterShader(ModelEffectMode.NonBatch, ModelLightingMode.RefractiveSpecular, shadowQuality, shadowlightCount, lightCount, textured, (VisualQuality)i);
                    RegisterShader(ModelEffectMode.Extruded, ModelLightingMode.RefractiveSpecular, shadowQuality, shadowlightCount, lightCount, textured, (VisualQuality)i);
                }
            }
        }

        public static void RegisterShader(ModelEffectMode effectMode, 
            ModelLightingMode lightingMode, 
            VisualQuality shadowQuality, int shadowlightCount, int lightCount,
            ModelTextureMode textured,
            VisualQuality refractionQuality)
		{
            string name = EffectName(effectMode, lightingMode, shadowQuality, shadowlightCount, lightCount, textured, refractionQuality);

            if (SharedEffects.Effects.ContainsKey(name) == true)
            {
                return; 
            }

            SharedEffects.Effects.Add(name, new ModelEffect(effectMode, lightingMode, shadowQuality, shadowlightCount, lightCount, textured, refractionQuality));
		}

        public static string EffectName(ModelEffectMode mode, 
            ModelLightingMode lighting, 
            VisualQuality shadowQuality, int shadowlightCount, int lightCount, 
            ModelTextureMode textured, 
            VisualQuality refractionQuality)
		{
            if (lighting == ModelLightingMode.Shadow || 
                lighting == ModelLightingMode.Outline || 
                lighting == ModelLightingMode.Normal ||
                lighting == ModelLightingMode.Color)
            {
                return "ModelEffect_" + mode.ToString() + "_" + lighting.ToString() + "_" + textured.ToString();
            }
            else if (lighting == ModelLightingMode.CelPerPixel)
            {
                return "ModelEffect_" + mode.ToString() + "_" + lighting.ToString() + "_Point:x" + Math.Min(lightCount, 4).ToString() + "_" + textured.ToString();
            }
            else if (lighting == ModelLightingMode.Refractive) 
            {
                return "ModelEffect_" + mode.ToString() + "_" + lighting.ToString() +  "_" + textured.ToString() + "_Refraction:" + refractionQuality.ToString();
            }
            else if (lighting == ModelLightingMode.RefractiveSpecular)
            {
                return "ModelEffect_" + mode.ToString() + "_" + lighting.ToString() + "_Shadow:" + shadowQuality + "x" + shadowlightCount.ToString() + "_Point:x" + lightCount.ToString() + "_" + textured.ToString() + "_Refraction:" + refractionQuality.ToString();
            }
            else
            {
                return "ModelEffect_" + mode.ToString() + "_" + lighting.ToString() + "_Shadow:" + shadowQuality + "x" + shadowlightCount.ToString() + "_Point:x" + lightCount.ToString() + "_" + textured.ToString();
            }
		}

		#endregion

		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Flat/ModelEffect";

		private int uObjectMatrix;
		private int uNormalMatrix;

		private int uWorldMatrix;
        private int uNormalWorldMatrix;        
		private int uPerspectiveMatrix;		

		private int uModelLights;
		private uint uModelLightBufferIndex = 0;
		private int uInstanceColor;
        private int[,] uShadowMatrix;
        private int[,] uShadowMinMax;
        private int uReflectionParams;
        private int uToCameraA;
        private int uToCameraB;
        private int uFogFactor;
        private int uRedFactor;

		#endregion

		#region Public Members

        public static int DefaultShadowsPerLight = 3;
        private int uCameraCenter;

        public override string Name { get { return "Flat: Model Effect (" + EffectName(EffectMode, LightingMode, ShadowQuality, ShadowLightCount, LightCount, TextureMode, RefractionQuality) + ")"; } }

		public override string ShaderLocation { get { return m_ShaderLocation; } }        

        public int ShadowLightCount { get; private set; }

        public int ShadowsPerLight { get; private set; }

		public int LightCount { get; private set; }

		public ModelEffectMode EffectMode { get; private set; }
		
		public ModelLightingMode LightingMode { get; private set; }

        public VisualQuality ShadowQuality { get; private set; }

        public ModelTextureMode TextureMode { get; private set; }

        public VisualQuality RefractionQuality { get; private set; }

		#endregion

        #region Constructor

        private ModelEffect(ModelEffectMode mode, ModelLightingMode lighting, 
            VisualQuality shadowQuality, int shadowlightCount, int lightCount, 
            ModelTextureMode textured, VisualQuality refractionQuality) 
		{
			EffectMode = mode; 
			LightingMode = lighting;

            RefractionQuality = refractionQuality;

            TextureMode = textured; 
            LightCount = lightCount;

            if (lighting == ModelLightingMode.CelPerPixel)
            {
                LightCount = Math.Min(LightCount, 4);             
            }

            ShadowQuality = shadowQuality; 
            ShadowLightCount = shadowlightCount;
            ShadowsPerLight = DefaultShadowsPerLight; 

            List<string> defines = new List<string>();

            ModelLightingMode effectiveLightingMode = lighting;

            if (lighting == ModelLightingMode.Refractive)
            {
                effectiveLightingMode = ModelLightingMode.Color; 
            }
            else if (lighting == ModelLightingMode.RefractiveSpecular)
            {
                effectiveLightingMode = ModelLightingMode.DiffuseSpecular; 
            }
            else if (lighting == ModelLightingMode.DiffuseSpecularPerPixel)
            {
                effectiveLightingMode = ModelLightingMode.DiffuseSpecular;

                defines.Add("UsePerPixelLighting");
            }

            defines.Add("Mode_" + mode.ToString());
            defines.Add("Lighting_" + effectiveLightingMode.ToString());
            defines.Add("Shadows_" + shadowQuality.ToString());

            if (textured == ModelTextureMode.Textured)
            {
                defines.Add("UseTextures");
            }

            if (lighting == ModelLightingMode.Refractive ||
                lighting == ModelLightingMode.RefractiveSpecular)
            {
                defines.Add("RefractionQuality_" + RefractionQuality.ToString());
                defines.Add("Lighting_UseRefractions");
            }

            bool noLighting = 
                lighting == ModelLightingMode.Color || 
                lighting == ModelLightingMode.Outline ||
                lighting == ModelLightingMode.Normal ||
                lighting == ModelLightingMode.Refractive;
           
            if (noLighting == true)
            {
                defines.Add("Lighting_NoLighting");
            }            

            bool useShadowMaps = 
                lighting != ModelLightingMode.Shadow || 
                lighting != ModelLightingMode.CelPerPixel || 
                noLighting == false;

            if (useShadowMaps == true)
            {
                defines.Add("Lighting_UseShadowMaps");
            }

            bool includeNormal = 
                lighting == ModelLightingMode.Normal ||
                lighting == ModelLightingMode.Refractive || 
                noLighting == false;

            if (includeNormal == true)
            {
                defines.Add("IncludeNormal");
            }

            if (lighting == ModelLightingMode.Refractive ||
                lighting == ModelLightingMode.RefractiveSpecular)
            {
                defines.Add("IncludeNormal_World");
            }

            Defines = defines.ToArray(); 

            Loops.Add("ShadowLights", ShadowLightCount);
            Loops.Add("ShadowsPerLight", ShadowsPerLight);
            Loops.Add("Lights", LightCount);
		}

        #endregion

        #region Lights

        public void SetupLights(UniformBuffer lightBuffer)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

            if (LightingMode == ModelLightingMode.Shadow)
            {
                return;
            }

			// Bind the created DiffuseSpecular Buffer to the Buffer Index
			GL.BindBufferRange(BufferRangeTarget.UniformBuffer, uModelLightBufferIndex, lightBuffer.ResourceHandle, (IntPtr)0, (IntPtr)(lightBuffer.ResourceInfo.Stride * lightBuffer.ResourceInfo.Count)); 

			uModelLights = GL.GetUniformBlockIndex(ProgramHandle, "modelLightBlock");

			GL.UniformBlockBinding(ProgramHandle, uModelLights, (int)uModelLightBufferIndex);
		}

		#endregion

        private void SetLightingArguments(Texture2D modelTexture, LightingArguments lightingArguments)
        {
            int textureIndex = 0;

            if (LightingMode != ModelLightingMode.Shadow &&
                LightingMode != ModelLightingMode.Color &&
                LightingMode != ModelLightingMode.Outline &&
                LightingMode != ModelLightingMode.Refractive)
            {
                for (int i = 0; i < ShadowLightCount; i++)
                {
                    for (int j = 0; j < ShadowsPerLight; j++)
                    {
                        GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), TextureTarget.Texture2D, lightingArguments.ShadowTextures[i, j]);

                        //GLState.ActiveTexture(TextureUnit.Texture0 + (textureIndex++));
                        //GL.BindTexture(TextureTarget.Texture2D, lightingArguments.ShadowTextures[i, j] != null ? lightingArguments.ShadowTextures[i, j].ResourceHandle : 0);

                        GL.Uniform3(uShadowMinMax[i, j], lightingArguments.ShadowMinMax[i, j]);
                        GL.UniformMatrix4(uShadowMatrix[i, j], false, ref lightingArguments.ShadowMatrices[i, j]);
                    }
                }
            }

            if (LightingMode != ModelLightingMode.Shadow &&
                LightingMode != ModelLightingMode.Normal)
            {
                GL.Uniform4(uFogFactor, lightingArguments.FogParams);
                GL.Uniform4(uRedFactor, lightingArguments.RedParams);
            }

            if (TextureMode == ModelTextureMode.Textured)
            {
                GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), modelTexture); 
                /* 
                if (modelTexture != null)
                {
                    GLState.ActiveTexture(TextureUnit.Texture0 + (textureIndex++));
                    GL.BindTexture(modelTexture.TextureTarget, modelTexture.ResourceHandle);
                }
                else
                {
                    GLState.ActiveTexture(TextureUnit.Texture0 + (textureIndex++));
                    GL.BindTexture(TextureTarget.Texture2D, 0);
                }
                */
            }

            if (LightingMode == ModelLightingMode.Refractive ||
                LightingMode == ModelLightingMode.RefractiveSpecular)
            {
                GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), lightingArguments.SourceTexture);
                GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), lightingArguments.DepthTexture);
                GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), lightingArguments.ReflectionsCubeMap); 

                /* 
                GLState.ActiveTexture(TextureUnit.Texture0 + (textureIndex++));
                GL.BindTexture(lightingArguments.SourceTexture.TextureTarget, lightingArguments.SourceTexture.ResourceHandle);

                GLState.ActiveTexture(TextureUnit.Texture0 + (textureIndex++));
                GL.BindTexture(lightingArguments.DepthTexture.TextureTarget, lightingArguments.DepthTexture.ResourceHandle);

                GLState.ActiveTexture(TextureUnit.Texture0 + (textureIndex++));
                GL.BindTexture(lightingArguments.ReflectionsCubeMap.TextureTarget, lightingArguments.ReflectionsCubeMap.ResourceHandle);
                */ 

                GL.Uniform4(uReflectionParams, lightingArguments.RefractionParams);
            }

            GLState.BindTexture(TextureUnit.Texture0 + (textureIndex++), lightingArguments.AmbiantCubeMap);

            GL.Uniform3(uCameraCenter, lightingArguments.CameraCenter);

            GL.UniformMatrix4(uWorldMatrix, false, ref lightingArguments.WorldMatrix);
            GL.UniformMatrix4(uNormalWorldMatrix, false, ref lightingArguments.NormalWorldMatrix);
            GL.UniformMatrix4(uPerspectiveMatrix, false, ref lightingArguments.ProjectionMatrix);
        }

		#region Render Single

		public void Render_Single(
			ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, ref Color4 color, Texture2D modelTexture,
            LightingArguments lightingArguments, 
            VertexArrayObject vao, int vertexCount)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			if (EffectMode != ModelEffectMode.Batch)
			{			
				GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
				GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			}

            SetLightingArguments(modelTexture, lightingArguments);

			if (EffectMode == ModelEffectMode.NonBatch)
			{
				GL.Uniform4(uInstanceColor, color);
			}

			vao.Bind();

			GL.DrawArrays(PrimitiveType.Triangles, 0, vertexCount);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			vao.Unbind();
		}


		public void Render_Single(
            ref Matrix4 objectMatrix, ref Matrix4 normalMatrix, ref Color4 color, Texture2D modelTexture,
            LightingArguments lightingArguments, 
			VertexArrayObject vao, int vertexCount,
            IndexBuffer indices, DrawElementsType drawElementsType)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			if (EffectMode != ModelEffectMode.Batch)
			{			
				GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
				GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			}

            SetLightingArguments(modelTexture, lightingArguments);
			
			if (EffectMode == ModelEffectMode.NonBatch)
			{
				GL.Uniform4(uInstanceColor, color); 				
			}

			vao.Bind();
			
			GL.DrawElements(PrimitiveType.Triangles, indices.ResourceInfo.Count, drawElementsType, IntPtr.Zero);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			vao.Unbind();
		}

		#endregion 

		#region Render Batched

		public void Render_Batched(
            ref Matrix4 objectMatrix, Texture2D modelTexture,
            LightingArguments lightingArguments, 
			VertexArrayObject vao, int vertexCount, int instanceCount)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}
			
			GL.UseProgram(ProgramHandle);

			if (EffectMode != ModelEffectMode.Batch)
			{
                /* 
                Matrix4 normalMatrix = objectMatrix;
                normalMatrix.ClearScale();
                normalMatrix.ClearTranslation();
                normalMatrix.Invert();
                normalMatrix.Transpose();
                */

                Matrix4 normalMatrix = Matrix4.Identity;

				GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
				GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			}

            SetLightingArguments(modelTexture, lightingArguments);
			
			vao.Bind();

			GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, vertexCount, instanceCount);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			vao.Unbind();
		}

		public void Render_Batched(
            ref Matrix4 objectMatrix, Texture2D modelTexture,
            LightingArguments lightingArguments, 
			VertexArrayObject vao, int vertexCount, int instanceCount,
            IndexBuffer indices, DrawElementsType drawElementsType)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			if (EffectMode != ModelEffectMode.Batch)
			{
                /* 
                Matrix4 normalMatrix = objectMatrix;
                normalMatrix.ClearScale();
                normalMatrix.ClearTranslation();
                normalMatrix.Invert();
                normalMatrix.Transpose();
                */

                Matrix4 normalMatrix = Matrix4.Identity;

				GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
				GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			}

            SetLightingArguments(modelTexture, lightingArguments);

			vao.Bind();

			GL.DrawElementsInstanced(PrimitiveType.Triangles, indices.ResourceInfo.Count, drawElementsType, IntPtr.Zero, instanceCount);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			vao.Unbind();
		}

		#endregion


        #region Render Extruded

        public void Render_Extruded(
            ref Matrix4 objectMatrix, Texture2D modelTexture,
            LightingArguments lightingArguments, Vector3 toCameraA, Vector3 toCameraB,  
            VertexArrayObject vao, int vertexCount, int instanceCount)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            GL.UseProgram(ProgramHandle);

            if (EffectMode != ModelEffectMode.Batch)
            {
                /* 
                Matrix4 normalMatrix = objectMatrix;
                normalMatrix.ClearScale();
                normalMatrix.ClearTranslation();
                normalMatrix.Invert();
                normalMatrix.Transpose();
                */ 

                Matrix4 normalMatrix = Matrix4.Identity;

                GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
                GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
            }

            if (EffectMode == ModelEffectMode.Extruded)
            {
                GL.Uniform3(uToCameraA, toCameraA);
                GL.Uniform3(uToCameraB, toCameraB);
            }

            SetLightingArguments(modelTexture, lightingArguments);

            vao.Bind();

            GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, vertexCount, instanceCount);
            Rug.Game.Environment.DrawCallsAccumulator++;

            GL.UseProgram(0);

            vao.Unbind();
        }

        public void Render_Extruded_Identity(
            ref Matrix4 objectMatrix, ref Matrix4 perspectiveMatrix, Texture2D modelTexture,
            LightingArguments lightingArguments, Vector3 toCameraA, Vector3 toCameraB,  
            VertexArrayObject vao, int vertexCount, int instanceCount)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            GL.UseProgram(ProgramHandle);

            if (EffectMode != ModelEffectMode.Batch)
            {
                /* 
                Matrix4 normalMatrix = objectMatrix;
                normalMatrix.ClearScale();
                normalMatrix.ClearTranslation();
                normalMatrix.Invert();
                normalMatrix.Transpose();
                */

                Matrix4 normalMatrix = Matrix4.Identity;

                GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
                GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
            }

            if (EffectMode == ModelEffectMode.Extruded)
            {
                GL.Uniform3(uToCameraA, toCameraA);
                GL.Uniform3(uToCameraB, toCameraB);
            }

            SetLightingArguments(modelTexture, lightingArguments);

            // This is overriding the previous value which is not great
            GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);

            vao.Bind();

            GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, vertexCount, instanceCount);
            Rug.Game.Environment.DrawCallsAccumulator++;

            GL.UseProgram(0);

            vao.Unbind();
        }

        public void Render_Extruded(
            ref Matrix4 objectMatrix, Texture2D modelTexture,
            LightingArguments lightingArguments, Vector3 toCameraA, Vector3 toCameraB,  
            VertexArrayObject vao, int vertexCount, int instanceCount,
            IndexBuffer indices, DrawElementsType drawElementsType)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            GL.UseProgram(ProgramHandle);

            if (EffectMode != ModelEffectMode.Batch)
            {
                /* 
                Matrix4 normalMatrix = objectMatrix;
                normalMatrix.ClearScale();
                normalMatrix.ClearTranslation();
                normalMatrix.Invert();
                normalMatrix.Transpose();
                */

                Matrix4 normalMatrix = Matrix4.Identity;
                GL.UniformMatrix4(uNormalMatrix, false, ref normalMatrix);
                GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
            }

            if (EffectMode == ModelEffectMode.Extruded) 
            {
                GL.Uniform3(uToCameraA, toCameraA);
                GL.Uniform3(uToCameraB, toCameraB);
            }

            SetLightingArguments(modelTexture, lightingArguments);

            vao.Bind();

            GL.DrawElementsInstanced(PrimitiveType.Triangles, indices.ResourceInfo.Count, drawElementsType, IntPtr.Zero, instanceCount);
            Rug.Game.Environment.DrawCallsAccumulator++;

            GL.UseProgram(0);

            vao.Unbind();
        }

        #endregion

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			if (EffectMode != ModelEffectMode.Batch)
			{
				uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
				uNormalMatrix = GL.GetUniformLocation(ProgramHandle, "normalMatrix");
			}

			if (EffectMode == ModelEffectMode.NonBatch)
			{
				uInstanceColor = GL.GetUniformLocation(ProgramHandle, "instance_color");
			}

            if (EffectMode == ModelEffectMode.Extruded)
            {
                uToCameraA = GL.GetUniformLocation(ProgramHandle, "toCameraA");
                uToCameraB = GL.GetUniformLocation(ProgramHandle, "toCameraB");   
            }

            int textureIndex = 0;
            uShadowMatrix = new int[ShadowLightCount, ShadowsPerLight];
            uShadowMinMax = new int[ShadowLightCount, ShadowsPerLight];

            if (LightingMode != ModelLightingMode.Shadow &&
                LightingMode != ModelLightingMode.Color &&
                LightingMode != ModelLightingMode.Outline &&
                LightingMode != ModelLightingMode.Refractive)
            {             
                for (int i = 0; i < ShadowLightCount; i++)
                {
                    for (int j = 0; j < ShadowsPerLight; j++)
                    {
                        uShadowMatrix[i,j] = GL.GetUniformLocation(ProgramHandle, "shadowMatrix_" + i + "_" + j);
                        uShadowMinMax[i, j] = GL.GetUniformLocation(ProgramHandle, "shadow_min_max_" + i + "_" + j);

                        GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "shadowTexture_" + i + "_" + j), textureIndex++);
                    }
                }                                   
            }

            if (LightingMode != ModelLightingMode.Shadow &&
                LightingMode != ModelLightingMode.Normal)
            {
                uFogFactor = GL.GetUniformLocation(ProgramHandle, "FogFactor");
                uRedFactor = GL.GetUniformLocation(ProgramHandle, "RedFactor"); 
            }

            if (TextureMode == ModelTextureMode.Textured)
            {
                GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "textureSource"), textureIndex++);
            }

            if (LightingMode == ModelLightingMode.Refractive ||
                LightingMode == ModelLightingMode.RefractiveSpecular)
            {
                GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "sourceTexture"), textureIndex++);
                GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "depthTexture"), textureIndex++);
                GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "reflectionsCubeMap"), textureIndex++);

                uReflectionParams = GL.GetUniformLocation(ProgramHandle, "reflectionParams");   
            }

            GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "ambientCubeMap"), textureIndex++);

            uCameraCenter = GL.GetUniformLocation(ProgramHandle, "cameraCenter"); 
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
            uNormalWorldMatrix = GL.GetUniformLocation(ProgramHandle, "normalWorldMatrix"); 
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
    }
}
