#version 410
#extension GL_ARB_shading_language_packing : enable

#include ~/Shaders/Flat/_Lighting.glsl

uniform samplerCube ambientCubeMap;

uniform mat4 worldMatrix;
uniform mat4 normalWorldMatrix;
uniform mat4 perspectiveMatrix;
uniform vec3 cameraCenter = vec3(0, 1, 0);

#section !Mode_Batch
	uniform mat4 objectMatrix;
	uniform mat4 normalMatrix;
#end

#section Mode_NonBatch
	uniform vec4 instance_color;
#end

layout(location = $Location(1)) in vec3 position;
layout(location = $Location(1)) in vec3 normal;
layout(location = $Location(1)) in vec4 color;
layout(location = $Location(1)) in vec4 face;

#section UseTextures
	layout(location = $Location(1)) in vec2 textureCoords;
#end 

#section Mode_FixedBatch
	layout(location = $Location(1)) in vec4 instance_center;
	layout(location = $Location(1)) in vec4 instance_color;
#end

#section Mode_Batch
	layout(location = $Location(4)) in mat4 objectMatrix;
	layout(location = $Location(4)) in mat4 normalMatrix;
	layout(location = $Location(1)) in vec4 instance_color;
#end

#section Mode_Extruded
	layout(location = $Location(1)) in vec4 instance_center1;
	layout(location = $Location(1)) in vec4 instance_center2;
	layout(location = $Location(1)) in vec2 instance_size;
	layout(location = $Location(1)) in vec4 instance_color;

	uniform vec3 toCameraA; 
	uniform vec3 toCameraB; 
#end

out vec4 viewspace_var;

#section Lighting_UseRefractions
out vec3 reflection_var; 
#end

#section IncludeNormal
out vec3 normal_var;
#end
#section IncludeNormal_World				
out vec3 world_normal_var;	
#end

#section !Lighting_Shadow

	out vec4 color_var;
	flat out vec4 instance_color_var;

	#section UseTextures
		out vec2 texture_var;
	#end 

	#section Mode_Extruded
		out float line_instance_alpha_var; 
	#end 

	#section !Lighting_NoLighting

		layout(std140) uniform modelLightBlock
		{
			ModelLight ModelLights[$Lights + $ShadowLights];
		};

		#section !Lighting_CelPerPixel
			out vec4 lighting_points;
			#foreach $i in ShadowLights
				#foreach $j in ShadowsPerLight
					uniform mat4 shadowMatrix_$i_$j;
					out vec4 shadow_var_$i_$j;
				#forend
				out vec4 lighting_shadow_$i;
			#forend 
		#end

		#section Lighting_CelPerPixel
			#foreach $i in Lights
				out vec3 light_dir_$i;
				out vec4 light_color_$i;
			#forend
		#end
	#end
#end

#section UsePerPixelLighting
	out float specularFactor_var;
	out float specularAmount_var; 
#end

void main()
{
#section Mode_FixedBatch
	float instance_scale = instance_center.w; 

	#section Lighting_Outline
		instance_scale = instance_scale * 1.075;
	#end

	vec3 vert = (instance_center.xyz) + (position * instance_scale); 
	vec3 face_vert = (instance_center.xyz) + (face.xyz * instance_scale); 	
#end


#section Mode_Extruded
	vec3 center1 = (objectMatrix * vec4(instance_center1.xyz, 1.0)).xyz; 
	vec3 center2 = (objectMatrix * vec4(instance_center2.xyz, 1.0)).xyz; 

	vec3 line = center2 - center1; 

	float lineWidth1 = instance_center1.w;  
	float lineWidth2 = instance_center2.w;  

	float lineLength = length(line); 	

	vec3 vectorFwd = normalize(line);

	vec3 vectorUp = toCameraA;

	float vectorDot = dot(vectorFwd, vectorUp); 

	if (vectorDot < -0.5 || vectorDot > 0.5) 
	{
		vectorUp = toCameraB;		
	}
			
	vec3 vectorRight = normalize(cross(vectorFwd, vectorUp)); 
			
	float lineWidth = mix(lineWidth1, lineWidth2, position.x); 

#section !Lighting_Shadow
	line_instance_alpha_var = mix(instance_size.x, instance_size.y, position.x); 
#end

#section Lighting_Outline
	lineWidth *= 1.75; 	
	line_instance_alpha_var = 1;
#end

	vec3 vert = center1 + 
		(
			(position.x * vectorFwd * lineLength) + 
			(position.y * vectorUp * lineWidth) + 
			(position.z * vectorRight * lineWidth)
		); 

	vec3 face_vert = center1 + 
		(
			(face.x * vectorFwd * lineLength) + 
			(face.y * vectorUp * lineWidth) + 
			(face.z * vectorRight * lineWidth)
		); 

	#section IncludeNormal	
		vec3 vert_normal = 
			(
				(normal.x * vectorFwd) + 
				(normal.y * vectorUp) + 
				(normal.z * vectorRight)
			); 
	#end
#end

#section !Mode_FixedBatch
#section !Mode_Extruded
	vec3 vert = position; 
	vec3 face_vert = face.xyz; 

	#section Lighting_Outline
		vert = vert * 1.075;
	#end
#end	
#end	

    vec4 localSpace = objectMatrix * vec4(vert, 1);
	vec4 worldSpace = worldMatrix * localSpace;
	viewspace_var = worldSpace; 
	gl_Position = perspectiveMatrix * worldSpace;  

#section !IncludeNormal_World
	vec3 world_normal_var;	
#end 

#section IncludeNormal		
	#section Mode_Extruded
		// lighting is done in camera space so we need to transform the normal into the camera frame
		world_normal_var = (normalMatrix * vec4(vert_normal, 0.0)).xyz; 	
	#end
	#section !Mode_Extruded
		// lighting is done in camera space so we need to transform the normal into the camera frame
		world_normal_var = (normalMatrix * vec4(normal, 0.0)).xyz; 	
	#end

	normal_var = (normalWorldMatrix * vec4(world_normal_var, 0.0)).xyz; 	
#end 

#section Lighting_Normal
	color_var = vec4(normal_var.xyz * 0.5 + 0.5, 1);	
#end

#section Lighting_NoLighting
	#section !Lighting_Normal
		color_var = vec4(color.rgb * instance_color.rgb, 1);	 
	#end
#end 

#section !Lighting_NoLighting

	#section !Lighting_Shadow	
		#section UseTextures
			texture_var = textureCoords;
		#end 

		instance_color_var = instance_color; 

		vec2 temp_emmisive_alpha = unpackHalf2x16(floatBitsToUint(instance_color_var.a));	
		float emmisive_value = temp_emmisive_alpha.x; 

		// get the vert color	
		color_var = vec4(color.rgb * instance_color.rgb, 1);

		// only need to transform the face by the camera space
		vec4 face_pos = objectMatrix * vec4(face_vert, 1);  

		#section Lighting_UseRefractions
			reflection_var = reflect(-world_normal_var.xyz, normalize(localSpace.xyz - (-cameraCenter))); 
		#end 
				
		vec3 reflection_face_var = reflect(-world_normal_var.xyz, normalize(face_pos.xyz - (-cameraCenter))); 
		//vec3 reflection_face_var = reflect(world_normal_var.xyz, normalize(localSpace.xyz - (-cameraCenter))); 

		face_pos = worldMatrix * face_pos;	
    	
		#section !Lighting_CelPerPixel

			// extract the specular factor from the alpha channel of the face color
			float specularFactor = color.a; 

			// extract the specular amount from the w channel of the unmodified face position 
			float specularAmount = face.w; 

			// extract and multiply up the specular factor
			specularFactor = 8.0 + (248.0 * specularFactor); 

			vec4 lightAccum = vec4(0); 		
			vec4 shadowlightTemp = vec4(0); 

			#foreach $i in ShadowLights
				#section Lighting_DiffuseSpecular
					shadowlightTemp = GetLighting_Directional_DiffuseSpecular(face_pos, normal_var.xyz, ModelLights[$i], specularFactor, specularAmount); 		
				#end
				#section Lighting_Diffuse
					shadowlightTemp = GetLighting_Directional_Diffuse(face_pos, normal_var.xyz, ModelLights[$i]);	
				#end 
				lighting_shadow_$i = vec4(shadowlightTemp.rgb * color_var.rgb, shadowlightTemp.w);
				
				#foreach $j in ShadowsPerLight	
					shadow_var_$i_$j = (shadowMatrix_$i_$j * vec4(localSpace.xyz, 1.0));
				#forend 
			#forend 

			#section !UsePerPixelLighting					
				#foreach $i in Lights
					#section Lighting_DiffuseSpecular
						lightAccum += GetLighting_DiffuseSpecular(face_pos, normal_var.xyz, ModelLights[$ShadowLights + $i], specularFactor, specularAmount); 	
					#end
					#section Lighting_Diffuse
						lightAccum += GetLighting_Diffuse(face_pos, normal_var.xyz, ModelLights[$ShadowLights + $i]);
					#end
				#forend 
			#end

			#section UsePerPixelLighting
				specularFactor_var = specularFactor;
				specularAmount_var = specularAmount;
			#end

			// add ambient term 		
			vec3 sampleReflection = textureLod(ambientCubeMap, reflection_face_var.xyz, 4.0 - (specularFactor * 4.0)).rgb  * clamp(specularAmount, 0.0, 1.0); // * 4.0; 
			sampleReflection += textureLod(ambientCubeMap, world_normal_var.xyz, 4.0 - (specularFactor * 4.0)).rgb  * (1 - clamp(specularAmount, 0.0, 1.0)); // * 4.0;
			sampleReflection *= 0.5; 

			lightAccum.rgb += clamp(sampleReflection * sampleReflection, vec3(0), vec3(2.0)); // * 0.25; 
			lightAccum.rgb += clamp(texture(ambientCubeMap, world_normal_var.xyz).rgb, vec3(0.0), vec3(1.0)) * 0.75; 

			// do final sum
			lighting_points = lightAccum; 
		#end

		#section Lighting_CelPerPixel
			#foreach $i in Lights
				light_dir_$i = normalize(ModelLights[$ShadowLights + $i].position.xyz - face_pos.xyz).xyz; 
				light_color_$i = ModelLights[$ShadowLights + $i].color;
			#forend 
	
			// IS THIS CORRECT? 
			color_var.a = instance_color.a; 
		#end
	#end
#end
}
