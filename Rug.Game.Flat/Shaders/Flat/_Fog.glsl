﻿uniform vec4 FogFactor = vec4(0.4, 0.5, 0.6, 0.0025);
uniform vec4 RedFactor = vec4(0.486, 0.055, 0.055, 32.0);

vec3 applyFog( in vec3  rgb,       // original color of the pixel
               in float distance) // camera to point distance
{
	if (FogFactor.w <= 0) 
	{
		return rgb; 
	}

	float fogDist = abs(distance) * FogFactor.w;
 
	float fogAmount = 1.0 - clamp(1.0 / exp(fogDist), 0.0, 1.0);

	fogAmount = fogAmount * fogAmount; 
	fogAmount = 1.0 - fogAmount; 

	vec3 fogColor = FogFactor.rgb; 

	if (RedFactor.w >= 1) 
	{ 
		float inverseFogFactor = clamp(fogAmount, 0, 1); 

		inverseFogFactor = pow(inverseFogFactor, RedFactor.w);

		fogColor = mix(fogColor, RedFactor.rgb, clamp(inverseFogFactor, 0, 1));	
	}

	vec3 fogMix = mix(fogColor, rgb, clamp(fogAmount, 0.25, 0.8));
	
	return fogMix; 
}

vec3 applyFog_NoRed( in vec3  rgb,       // original color of the pixel
               in float distance) // camera to point distance
{
	if (FogFactor.w <= 0) 
	{
		return rgb; 
	}

	float fogDist = abs(distance) * FogFactor.w;
 
	float fogAmount = 1.0 - clamp(1.0 / exp(fogDist), 0.0, 1.0);

	fogAmount = fogAmount * fogAmount; 
	fogAmount = 1.0 - fogAmount; 

	vec3 fogColor = FogFactor.rgb; 

	vec3 fogMix = mix(fogColor, rgb, clamp(fogAmount, 0.25, 0.8));
	
	return fogMix; 
}

