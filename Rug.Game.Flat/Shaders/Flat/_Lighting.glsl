﻿struct ModelLight
{
	vec3 position;
	float intensity;
	vec4 color;  
	vec4 attenuation;  
};

#section Pass_Fragment
#section Lighting_CelPerPixel
vec3 GetLighting_CelPerPixel(in vec3 normal, in vec3 dir, in vec4 lightColor, in float minShade, in float shadeOffset) 
{
	float intensity = max(dot(normal, dir), 0.0);	

	intensity = max(minShade, ceil(intensity) - shadeOffset);

	return intensity * lightColor.xyz;
}

float stepmix(float edge0, float edge1, float E, float x)
{
	float T = clamp(0.5 * (x - edge0 + E) / E, 0.0, 1.0);

	return mix(edge0, edge1, T);
}

vec4 GetLighting_CelPerPixel_Diffuse(in vec3 normal, in vec3 dir, in vec4 lightColor, in float ambient, in float shininess)
{
	float diffuse = max(dot(normal, dir), 0.0);	

	const float A = 0.1;
	const float B = 1;

	float E = fwidth(diffuse);

	if (diffuse > A - E && diffuse < A + E) diffuse = stepmix(A, B, E, diffuse) + ambient;
	else if (diffuse < A) diffuse = ambient;
	else diffuse = B;

	return vec4(clamp(diffuse, 0, 1) * lightColor.rgb, 0);
}


vec4 GetLighting_CelPerPixel_DiffuseSpecular(in vec3 normal, in vec3 dir, in vec4 lightColor, in float ambient, in float shininess)
{
	vec3 N = normal;
	vec3 L = dir;
	vec3 Eye = vec3(0, 0, 1);

	vec3 H = normalize(L + Eye);

	float diffuse = max(0.0, dot(N, L));

	float specular = max(0.0, dot(N, H));

	specular = pow(specular, shininess);

	const float A = 0.1;
	const float B = 1;

	float E = fwidth(diffuse);

	if (diffuse > A - E && diffuse < A + E) diffuse = stepmix(A, B, E, diffuse) + ambient;
	else if (diffuse < A) diffuse = ambient;
	else diffuse = B;

	E = fwidth(specular);

	if (specular > 0.5 - E && specular < 0.5 + E)
	{
		specular = smoothstep(0.5 - E, 0.5 + E, specular);
	}
	else	
	{
		specular = step(0.5, specular);
	}

	return vec4(clamp(diffuse, 0, 1) * lightColor.rgb, specular * lightColor.a);
}
#end 
#end 

#section Lighting_DiffuseSpecular
vec4 GetLighting_DiffuseSpecular(in vec4 pos, in vec3 normal, in ModelLight light, in float specularFactor, in float specularAmount) 
{
	vec4 lightColor = light.color;
	vec2 attenuationTerms = light.attenuation.xy;	

	// get the vector from the light to the face in camera space 
	vec3 fromLightToFace = light.position - pos.xyz;

	// get the distance to the light 
	float dist = length(fromLightToFace); 

	// get the direction from the light to the face
	vec3 L = normalize(fromLightToFace);
	
	// get the direction from the camera to the face
	vec3 E = normalize(pos.xyz);

	// get the reflection vector of the light off the normal 
	vec3 R = normalize(reflect(-L, normal)); 

	// attenuate the light by the distance
	float attenuation = 1.0 / (1.0 + attenuationTerms.x * dist + attenuationTerms.y * dist * dist); 

	float intensity = max(dot(normal, L), 0.0);
	float specScale = smoothstep(0.0, 0.01, intensity);	

	float specular = lightColor.a; 
	float spec = pow(max(dot(-R, E), 0.0), specularFactor) * specScale * specular * specularAmount;

	// subsurface 
	//intensity = intensity * 0.25 + 0.75; 

	// add the specular term 
	return vec4(intensity * lightColor.xyz, spec) * light.intensity * attenuation;
}

vec4 GetLighting_Directional_DiffuseSpecular(in vec4 pos, in vec3 normal, in ModelLight light, in float specularFactor, in float specularAmount) 
{
	vec4 lightColor = light.color;

	// get the direction from the light to the face
	vec3 L = normalize(light.position);

	// get the direction from the camera to the face
	vec3 E = normalize(pos.xyz);

	// get the reflection vector of the light off the normal 
	vec3 R = normalize(reflect(-L, normal)); 

	float intensity = max(dot(normal, L), 0.0);
	float specScale = smoothstep(0.0, 0.01, intensity);

	float specular = lightColor.a; 
	float spec = pow(max(dot(-R, E), 0.0), specularFactor) * specScale * specular * specularAmount;

	// subsurface 
	//intensity = intensity * 0.25 + 0.75; 

	// add the specular term 
	return vec4(intensity * lightColor.xyz, spec) * light.intensity;
}
#end 

#section Lighting_Diffuse
vec4 GetLighting_Diffuse(in vec4 pos, in vec3 normal, in ModelLight light) 
{
	vec4 lightColor = light.color;
	vec2 attenuationTerms = light.attenuation.xy;	

	// get the vector from the light to the face in camera space 
	vec3 fromLightToFace = light.position - pos.xyz;

	// get the distance to the light 
	float dist = length(fromLightToFace); 

	// get the direction from the light to the face
	vec3 L = normalize(fromLightToFace);
	
	// attenuate the light by the distance
	float attenuation = 1.0 / (1.0 + attenuationTerms.x * dist + attenuationTerms.y * dist * dist); 

	float intensity = max(dot(normal, L), 0.0);

	// subsurface 
	//intensity = intensity * 0.25 + 0.75; 

	// add the specular term 
	return vec4(intensity * lightColor.xyz, 0.0) * light.intensity * attenuation;
}

vec4 GetLighting_Directional_Diffuse(in vec4 pos, in vec3 normal, in ModelLight light) 
{
	vec4 lightColor = light.color;
	
	// get the direction from the light to the face
	vec3 L = normalize(light.position);
	
	float intensity = max(dot(normal, L), 0.0);

	// subsurface 
	//intensity = intensity * 0.25 + 0.75; 

	// add the specular term 
	return vec4(intensity * lightColor.xyz, 0.0) * light.intensity;
}
#end 
