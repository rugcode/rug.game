﻿
namespace Rug.Game.Flat
{
    /* 
    public class ModelNormalPipeline : IResizablePipeline, IResourceManager
    {
        private DownSample m_DownSample;

        public readonly ResourceManager ResourceManagers = new ResourceManager();

		public readonly ResourceSet Resources;

		public readonly FrameBuffer Buffer;
		
		private readonly TextureBox Box;

		public bool Disposed { get; private set; }

		public bool IsEnabled { get; set; }

		public string Name { get; set; }

		public PipelineMode PipelineMode
		{
			get 
			{
				return
					PipelineMode.BeginEndBlock |
					PipelineMode.Render |
					PipelineMode.Update; 
			}
		}

        public ModelScene Scene { get; set; }

        public Texture2D ColorBuffer { get { return Buffer[FramebufferAttachment.ColorAttachment0]; } }

        public Texture2D SourceImageTexture { get; set; }
        
        public Texture2D SourceDepthTexture { get; set; }

        public TextureDDS ReflectionsCubeMap { get; set; }

        public bool UseMultisample { get; set; }

        public CullFaceMode CullFaceMode { get; set; } 

        public ModelReflectionsPipeline(string name)
		{
            CullFaceMode = OpenTK.Graphics.OpenGL.CullFaceMode.Back; 

			Resources = new ResourceSet(name + " Pipeline Resource Set", ResourceMode.Static);

			Disposed = true;
            
			Buffer = new FrameBuffer(name + " Pipeline Frame Buffer", ResourceMode.Static,
					new FrameBufferInfo(
						new FrameBufferTexture2DInfo(name + " Color", FramebufferAttachment.ColorAttachment0)
						{
							Size = new Size(32, 32),
                            InternalFormat = PixelInternalFormat.R11fG11fB10f, // PixelInternalFormat.Rgba16f,                            
							PixelFormat = PixelFormat.Rgba,
							PixelType = PixelType.Float,
							MagFilter = TextureMagFilter.Nearest,
							MinFilter = TextureMinFilter.Nearest,
							WrapS = TextureWrapMode.ClampToBorder,
							WrapT = TextureWrapMode.ClampToBorder,
							Samples = 1,
						},
						new FrameBufferTexture2DInfo(name + " Depth", FramebufferAttachment.DepthAttachment)
						{
							Size = new Size(32, 32),
							InternalFormat = (PixelInternalFormat)All.DepthComponent24,
							PixelFormat = PixelFormat.DepthComponent,
							PixelType = PixelType.UnsignedInt,
							MagFilter = TextureMagFilter.Nearest,
							MinFilter = TextureMinFilter.Nearest,
							WrapS = TextureWrapMode.ClampToBorder,
							WrapT = TextureWrapMode.ClampToBorder,
							Samples = 1,
						}
						));

			Rug.Game.Environment.ResolutionDependentResources.Add(Buffer);

			Box = new TextureBox();
			Box.FlipVertical = true;

			ResourceManagers.Add(Box);
		}

		public void Resize(int width, int height, MultiSamples samples)
        {
            if (UseMultisample == true)
            {
                Buffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples = DisplaySettings.MultiSampleToSampleCount(samples);
                Buffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Samples = DisplaySettings.MultiSampleToSampleCount(samples);
            }
            else
            {
                Buffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples = 1;
                Buffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Samples = 1;
            }

            Buffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = new Size(width, height);           			
            Buffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Size = new Size(width, height);

            m_DownSample = PostProcessHelper.SelectDownSample(samples);
		}

        public virtual void Update(View3D view)
		{
			
		}

		public virtual void Begin()
		{

		}

		public virtual void End()
		{

		}
        
        public void BeginRender(View3D view)
		{           
			Buffer.Bind();

			GLState.EnableBlend = false;
			GLState.ClearDepth(1.0f);
            GLState.Viewport = view.Viewport; 

            GLState.Apply(view);
		}

        public void Render(Rug.Game.Core.Rendering.View3D view)
        {
            BeginRender(view);

            RenderContent(view);

            EndRender(view);
        }

		public virtual void RenderContent(Rug.Game.Core.Rendering.View3D view)
		{
            GLState.CullFace(CullFaceMode);
            GLState.EnableCullFace = true;
            GLState.EnableDepthMask = true;
            GLState.EnableDepthTest = true;
            GLState.Apply(view);

            GL.Clear(ClearBufferMask.DepthBufferBit);
            Scene.LightingMode = Shaders.Flat.ModelLightingMode.Color;
            Scene.Render(view);
            
            GLState.EnableCullFace = false;
            GLState.EnableDepthMask = false;
            GLState.EnableDepthTest = false;
            GLState.Apply(view);

            Box.FlipVertical = true;
            Box.CheckAndWriteRectangle();

            if (SourceImageTexture.ResourceInfo.Samples > 1)
            {
                m_DownSample.Render(SourceImageTexture, Box.Vertices);
            }
            else
            {
                Box.Texture = SourceImageTexture;
                Box.Render(view);
            }

            GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GLState.BlendEquation(BlendEquationMode.FuncAdd);
            GLState.EnableBlend = true;
            GLState.EnableCullFace = true;
            GLState.EnableDepthMask = true;
            GLState.EnableDepthTest = true;
            GLState.Apply(view);            

            Scene.LightingMode = Shaders.Flat.ModelLightingMode.Diffuse;

            //Scene.RenderReflectiveObjects(view, ColorBuffer, SourceDepthTexture, ReflectionsCubeMap);
            Scene.RenderReflectiveObjects(view, SourceImageTexture, SourceDepthTexture, ReflectionsCubeMap);
		}

		public void EndRender(Rug.Game.Core.Rendering.View3D view)
		{
			Buffer.Unbind();
		}

		public void LoadResources()
		{			
			ResourceManagers.LoadResources();
			Resources.LoadResources(); 
		}

		public void UnloadResources()
		{
			ResourceManagers.UnloadResources();
			Resources.UnloadResources(); 
		}

		public void Dispose()
		{
			ResourceManagers.Dispose(); 
		}
    }
    */ 
}
