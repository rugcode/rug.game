﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Data;
using Rug.Game.Flat.Models;
using Rug.Game.Flat.Models.Data;
using System;

namespace Rug.Game.Flat
{
    public class SFXLineChain
    {
        public int Index;

		public bool IsPartOfBatch { get { return Index >= 0; } }

        public object Tag;

        public bool IsVisible;

        public Vector4[] Links;
        public float[] LinkAlphas;

        public Vector3 Center;

        public float Scale;

        public Color3 Color;

        public float Emissivity;

        public int InstanceCount;	

		public Color4 ColorArg { get; protected set; }

        public SFXLineChain(int linkCount)
		{
			Index = -1;
			Scale = 1f;
			Color = Color4.White;

			Emissivity = 0f;
            IsVisible = true; 

            Links = new Vector4[linkCount];
            LinkAlphas = new float[linkCount];
            InstanceCount = linkCount;
		}

		public virtual void Update()
		{
			ColorArg = Color3.ToColor4(Color, 1f - (Emissivity * Emissivity));
		}

        public virtual void AddToBatch(SFXLineBatch batch)
		{
            Vector4 lastPoint = Links[0];
            Vector4 v0, v1, v2, v3;
            Color4 color0 = Color3.ToColor4(Color, 1);
            Color4 color1 = color0;

            int pointIndex = batch.InstanceCount * 4; 
            int colorIndex = batch.InstanceCount * 2;

            //float inc = 1f / (float)Links.Length;

            int maxCount = batch.MaxCount; 
            int instanceCount = batch.InstanceCount;
            int desiredMaxCount = batch.DesiredMaxCount;

            for (int i = 0; i < Math.Min(InstanceCount - 1, Links.Length - 1); i++)
            {
                if (instanceCount < maxCount)
                {     
                    v0 = lastPoint;
                    v1 = Links[i];                    
                    v2 = Links[i + 1];
                    v3 = i < Math.Min(InstanceCount - 2, Links.Length - 2) ? Links[i + 2] : v2;
                    lastPoint = v1;

                    color0.A = LinkAlphas[i];
                    color1.A = LinkAlphas[i + 1]; 

                    batch.Points[pointIndex++] = new SFXLineVertex() 
                    {                        
                        Position = v0,
                        Color = color0,
                        //Normal = Vector3.NormalizeFast(v0.Xyz - Center), 
                    };                                       
                   
                    batch.Points[pointIndex++] = new SFXLineVertex() 
                    {                        
                        Position = v1,
                        Color = color0,
                        //Normal = Vector3.NormalizeFast(v1.Xyz - Center), 
                    };

                    
                    batch.Points[pointIndex++] = new SFXLineVertex()
                    {
                        Position = v2,
                        Color = color1,
                        //Normal = Vector3.NormalizeFast(v2.Xyz - Center),
                    };

                    
                    batch.Points[pointIndex++] = new SFXLineVertex()
                    {
                        Position = v3,
                        Color = color1,
                        //Normal = Vector3.NormalizeFast(v3.Xyz - Center),
                    };

                    instanceCount++;                    
                }

                desiredMaxCount++;                
            }

            batch.InstanceCount = instanceCount;
            batch.DesiredMaxCount = desiredMaxCount; 
		}
    }
}
