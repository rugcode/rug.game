﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Data;
using Rug.Game.Flat.Models;

namespace Rug.Game.Flat
{
    public class ModelChain
    {
        public int Index { get; set; }

		public bool IsPartOfBatch { get { return Index >= 0; } }

		public object Tag { get; set; }

        public bool IsVisible { get; set; } 

		public string Model { get; set; } 

		public Vector4[] Links { get; set; }
        public float[] LinkAlphas { get; set; }

		public float Scale { get; set; }

		public Color3 Color { get; set; }

		public float Emissivity { get; set; }		


		public Color4 ColorArg { get; protected set; }

        public ModelChain(int linkCount)
		{
			Index = -1;
			Scale = 1f;
			Color = Color4.White;
			Emissivity = 0f;
            IsVisible = true; 

            Links = new Vector4[linkCount];
            LinkAlphas = new float[linkCount]; 
		}

		public virtual void Update()
		{
			ColorArg = Color3.ToColor4(Color, 1f - (Emissivity * Emissivity));
		}

		public void AddToBatch(ExtrudedModelBatch batch)
		{
            for (int i = 0; i < Links.Length - 1; i++)
            {
                if (batch.InstanceCount < batch.MaxCount)
                {
                    batch.Instances[batch.InstanceCount++] = new ExtrudedModelInstance()
                    {
                        Center1 = Links[i],
                        Center2 = Links[i + 1],
                        Size = new Vector2(LinkAlphas[i], LinkAlphas[i + 1]),
                        Color = ColorArg,
                    };                    
                }

                batch.DesiredMaxCount++;
            }
		}
    }
}
