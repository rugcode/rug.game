﻿using System.Runtime.InteropServices;

namespace Rug.Game.Flat.Models
{	
	[StructLayout(LayoutKind.Sequential)]
	public struct ModelLightAttenuation
	{
		public float A;
		public float B;
		public float Padding1;
		public float Padding2;

		public ModelLightAttenuation(float a, float b)
		{
			A = a;
			B = b;
			Padding1 = 0;
			Padding2 = 0;	
		}
	}
}
