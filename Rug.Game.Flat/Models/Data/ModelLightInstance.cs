﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Flat.Models 
{
	[StructLayout(LayoutKind.Explicit)]
	public struct ModelLightInstance
	{
		public static ModelLightInstance Zero = new ModelLightInstance()
		{
			Position = Vector3.Zero,
			Intensity = 0f,
			Color = new Color4(0.0f, 0.0f, 0.0f, 0.0f),
			Attenuation = new ModelLightAttenuation(0.0f, 0.0f),
		};


		[FieldOffset(0)]
		public Vector3 Position;
		
		[FieldOffset(12)]
		public float Intensity;
		
		[FieldOffset(16)]
		public Color4 Color;	
	
		[FieldOffset(32)]
		public ModelLightAttenuation Attenuation;

		public enum Elements : int { Position = 0, Intensity = 1, Color = 2, Attenuation };

		public static readonly int Stride;

		public static readonly int PositionOffset;
		public static readonly int IntensityOffset;
		public static readonly int ColorOffset;
		public static readonly int AttenuationOffset;

		static ModelLightInstance()
		{
			Stride = BlittableValueType<ModelLightInstance>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Position");
			IntensityOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Intensity");
			ColorOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Color");
			AttenuationOffset = (int)Marshal.OffsetOf(typeof(ModelLightInstance), "Attenuation");

			if (AttenuationOffset + BlittableValueType<ModelLightAttenuation>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total");
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.Intensity, 1, VertexAttribPointerType.Float, false, Stride, IntensityOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
			GL.VertexAttribPointer((int)Elements.Attenuation, 4, VertexAttribPointerType.Float, false, Stride, AttenuationOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.Intensity);
			GL.EnableVertexAttribArray((int)Elements.Color);
			GL.EnableVertexAttribArray((int)Elements.Attenuation);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.Intensity);
			GL.DisableVertexAttribArray((int)Elements.Color);
			GL.DisableVertexAttribArray((int)Elements.Attenuation);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return ModelLightInstance.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Intensity + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, IntensityOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.Attenuation + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, AttenuationOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Intensity + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Attenuation + baseLocation);
				
				baseLocation += 4;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Intensity + baseLocation, 1, VertexAttribPointerType.Float, false, Stride, IntensityOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.Attenuation + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, AttenuationOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Intensity + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Attenuation + baseLocation);

				GL.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.VertexAttribDivisor((int)Elements.Intensity + baseLocation, devisor);
				GL.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
				GL.VertexAttribDivisor((int)Elements.Attenuation + baseLocation, devisor);

				baseLocation += 4;
			}

			#endregion
		}

		#endregion
	}
}
