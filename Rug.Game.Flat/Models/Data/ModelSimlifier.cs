﻿//using Microsoft.Xna.Framework;
//using RTC5.Extensions;

namespace Rug.Game.Flat.Models.Data
{
    /* 
    /// <summary>
    /// Based on CPP version by http://www.melax.com/polychop
    /// </summary>
    public class ProgressiveMesh
    {
        
        public class Triangle 
        {
            public Vertex[] Vertexes = new Vertex[3] { null, null, null }; // the 3 points that make this tri
	        public Vector3 Normal;                                 // unit vector othogonal to this face
            public Edge[] Edges = new Edge[3] { null, null, null };

            public static float Area(float a, float b, float c)
            {
                float s = (a + b+ c)/2f; 
                float area = (float)Math.Sqrt(s*(s-a)*(s-b)*(s-c));
                return area;
            }

            public Triangle(Vertex v0,Vertex v1,Vertex v2,ProgressiveMesh owningMesh)
            {
                if (!(v0.Source != v1.Source && v1.Source != v2.Source && v2.Source != v0.Source))
                {
                    // Not a well formed triangle. Bail out.
                    return;
                }
                this.Vertexes[0] = v0;
                this.Vertexes[1] = v1;
                this.Vertexes[2] = v2;
                this.ComputeNormal();

                owningMesh.Triangles.Add(this);
                for (int i = 0; i < 3; i++)
                {
                    this.Vertexes[i].Faces.Add(this);
                    for (int j = 0; j < 3; j++) if (i != j)
                    {
                        if(! Vertexes[i].Neighbours.Contains(Vertexes[j]))
                        {
                            Vertexes[i].Neighbours.Add(Vertexes[j]);
                        }
                    }
                }

                Edge e1 = new Edge() { V1 = v0, V2 = v1 };
                Edge e2 = new Edge() { V1 = v1, V2 = v2 };
                Edge e3 = new Edge() { V1 = v2, V2 = v0 };
                this.Edges[0] = owningMesh.AddEdge(e1, this);
                this.Edges[1] = owningMesh.AddEdge(e2, this);
                this.Edges[2] = owningMesh.AddEdge(e3, this);
                
            }

	        public void ComputeNormal()
            {
                this.Normal = ComputeNormal(Vertexes[0].Source, Vertexes[1].Source, Vertexes[2].Source);
            }

            public static bool IsWellFormed(Vector3 v0, Vector3 v1, Vector3 v2)
            {
                return v0 != v1 && v1 != v2 && v2 != v0;
            }

            public static Vector3 ComputeNormal(Vector3 v0, Vector3 v1, Vector3 v2)
            {
                return new Vector3().CalculatePlaneNormal(v0, v1, v2);
                
            }

	        public void ReplaceVertex(Vertex vold,Vertex vnew)
            {
                System.Diagnostics.Debug.Assert(vold != null && vnew != null);
                System.Diagnostics.Debug.Assert(vold == Vertexes[0] || vold == Vertexes[1] || vold == Vertexes[2]);
                System.Diagnostics.Debug.Assert(vnew != Vertexes[0] && vnew != Vertexes[1] && vnew != Vertexes[2]);
                if (vold == Vertexes[0])
                {
                    Vertexes[0] = vnew;
                }
                else if (vold == Vertexes[1])
                {
                    Vertexes[1] = vnew;
                }
                else
                {
                    System.Diagnostics.Debug.Assert(vold == Vertexes[2]);
                    Vertexes[2] = vnew;
                }
                int i;
                vold.Faces.Remove(this);
                System.Diagnostics.Debug.Assert(!vnew.Faces.Contains(this));
                vnew.Faces.Add(this);
                for (i = 0; i < 3; i++)
                {
                    vold.RemoveIfNonNeighbor(Vertexes[i]);
                    Vertexes[i].RemoveIfNonNeighbor(vold);
                }
                for (i = 0; i < 3; i++)
                {
                    System.Diagnostics.Debug.Assert(Vertexes[i].Faces.Contains(this) );
                    for (int j = 0; j < 3; j++) if (i != j)
                        {
                            if (!Vertexes[i].Neighbours.Contains(Vertexes[j]))
                            {
                                Vertexes[i].Neighbours.Add(Vertexes[j]);
                            }
                        }
                }
                ComputeNormal();
            }
	        public bool HasVertex(Vertex v)
            {
                return (v == Vertexes[0] || v == Vertexes[1] || v == Vertexes[2]);
            }

            public void Delete(ProgressiveMesh owningMesh)
            {
                int i;
                owningMesh.Triangles.Remove(this);
                for (i = 0; i < 3; i++)
                {
                    if (Vertexes[i] != null) Vertexes[i].Faces.Remove(this);
                }
                for (i = 0; i < 3; i++)
                {
                    int i2 = (i + 1) % 3;
                    if (Vertexes[i] != null || Vertexes[i2] == null) continue;
                    Vertexes[i].RemoveIfNonNeighbor(Vertexes[i2]);
                    Vertexes[i2].RemoveIfNonNeighbor(Vertexes[i]);
                }
            }
            
        }

        public class Vertex 
        {
            public Vector3 Source; // location of point in euclidean space
            public int ID;       // place of vertex in original list
            public List<Vertex> Neighbours = new List<Vertex>(); // adjacent vertices
            public List<Triangle> Faces = new List<Triangle>();     // adjacent triangles
            public float CollapseCost;  // cached cost of collapsing edge
            public Vertex CollapseToCandidate; // candidate vertex for collapse. This is the vertex we will be replaced with when ordered to collapse
            public float VertexWeight = 0;

            public Vertex(Vector3 position, int _id, ProgressiveMesh owningMesh)
            {
                this.Source = position;
                this.ID = _id;
                owningMesh.Vertices.Add(this);
            }
            public void Delete(ProgressiveMesh owningMesh)
            {
                while (Neighbours.Count > 0)
                {
                    Neighbours[0].Neighbours.Remove(this);
                    Neighbours.Remove(Neighbours[0]);
                }
                owningMesh.Vertices.Remove(this);
            }
            public void RemoveIfNonNeighbor(Vertex n)
            {
                // removes n from neighbor list if n isn't a neighbor.
                if (!Neighbours.Contains(n)) return;
                for (int i = 0; i < Faces.Count; i++)
                {
                    if (Faces[i].HasVertex(n)) return;
                }
                Neighbours.Remove(n);
            }
        }

        public List<Vertex>   Vertices = new List<Vertex>();
        // For post-processing we might want access to the vertex list above, which is destroyed the the Generate step.
        public List<Vertex> PreCollapseVertices = new List<Vertex>();
        public List<Triangle> Triangles = new List<Triangle>();
        // Initial set of uncollapsed edges.
        public Dictionary<Vector3, Edge> Edges = new Dictionary<Vector3, Edge>();


        public float ComputeEdgeCollapseCost(Vertex moveFrom, Vertex moveTo)
        {
            // if we collapse edge uv by moving u to v then how 
            // much different will the model change, i.e. how much "error".
            
            // Texture, vertex normal, and border vertex code was removed
            // to keep this demo as simple as possible.
            // The method of determining cost was designed in order 
            // to exploit small and coplanar regions for
            // effective polygon reduction.
            
            // Is is possible to add some checks here to see if "folds"
            // would be generated.  i.e. normal of a remaining face gets
            // flipped.  I never seemed to run into this problem and
            // therefore never added code to detect this case.

            int i;
            float edgelength = (moveTo.Source - moveFrom.Source).Length;
            float curvature = 0;

            // find the "sides" triangles that are on the edge uv
            List<Triangle> sides = new List<Triangle>();
            for (i = 0; i < moveFrom.Faces.Count; i++)
            {
                if (moveFrom.Faces[i].HasVertex(moveTo))
                {
                    sides.Add(moveFrom.Faces[i]);
                }
            }

            // use the triangle facing most away from the sides 
            // to determine our curvature term
            for (i = 0; i < moveFrom.Faces.Count; i++)
            {
                float mincurv = 1; // curve for face i and closer side to it
                for (int j = 0; j < sides.Count; j++)
                {
                    // use dot product of face normals. '^' defined in vector
                    float dotprod = Vector3.Dot(moveFrom.Faces[i].Normal ,sides[j].Normal);
                    mincurv = Math.Min(mincurv, (1 - dotprod) / 2.0f);
                }
                curvature = Math.Max(curvature, mincurv);
            }

            // the more coplanar the lower the curvature term   
            float standardChangeFactor = edgelength * curvature;

            // Note the cost of collapse is the vertex which will be moved, not the destination vertex (v).
            standardChangeFactor += moveFrom.VertexWeight;

            // Test acuteness of resulting triangle - we want the least acute.

            // check for normal flipping generating folds. This will occur if a face adjoining the moved U vector changes its normal to flip the other way
            List<Triangle> adjacentTriangles = new List<Triangle>();
            adjacentTriangles.AddRange(moveFrom.Faces);
            // We will be moving U to V 
            foreach (Triangle adjacentTriangle in adjacentTriangles)
            {
                Vector3 existingTriangle0 = adjacentTriangle.Vertexes[0].Source;
                Vector3 existingTriangle1 = adjacentTriangle.Vertexes[1].Source;
                Vector3 existingTriangle2 = adjacentTriangle.Vertexes[2].Source;

                /*
                // We should remove small triangles more easily than big ones.
                float edge1Distance = (v0.Position + v1.Position).Length();
                float edge2Distance = (v1.Position + v2.Position).Length();
                float edge3Distance = (v2.Position + v0.Position).Length();

                standardChangeFactor += Triangle.Area(edge1Distance, edge2Distance, edge3Distance) /10;
                * /

               Vector3 newTriangle0 = adjacentTriangle.Vertexes[0].Source;
               Vector3 newTriangle1 = adjacentTriangle.Vertexes[1].Source;
               Vector3 newTriangle2 = adjacentTriangle.Vertexes[2].Source;

                // Swap the vector which will replace U.
                if (existingTriangle0 == moveFrom.Source) newTriangle0 = moveTo.Source;
                if (existingTriangle1 == moveFrom.Source) newTriangle1 = moveTo.Source;
                if (existingTriangle2 == moveFrom.Source) newTriangle2 = moveTo.Source;

                // Check that the resultant triangle has an area before checking its normal
                if (Triangle.IsWellFormed(newTriangle0,newTriangle1, newTriangle2))
                {
                    Vector3 currentFaceNormal = Triangle.ComputeNormal(existingTriangle0, existingTriangle1, existingTriangle2);
                    Vector3 newFaceNormal = Triangle.ComputeNormal(newTriangle0, newTriangle1, newTriangle2);
                    
                    // The dot product is equal to the cosine of the angle between the two input vectors. 
                    // This means that it is 1 if both vectors have the same direction, 0 if they are orthogonal to each other and -1 
                    // if they have opposite directions (v1 = -v2). 
                    if(currentFaceNormal.IsVectorDirectionFlipped(newFaceNormal))
                    {
                        // Must not let this occur.
                        standardChangeFactor = float.MaxValue;
                        break;
                    }
                    
                }
                else
                {
                    // This operation would lead to the deletion of this triangle. One of the vertexes will be destroyed. We should take acccount
                    // its vertex weighting as it might not want to be destroyed.
                    Vertex deletedVertex = null;
                    if (newTriangle0 != moveTo.Source && newTriangle0 != moveFrom.Source)
                    {
                        // This vertex would be deleted.
                        deletedVertex = adjacentTriangle.Vertexes[0];
                    }
                    if (newTriangle1 != moveTo.Source && newTriangle1 != moveFrom.Source)
                    {
                        // This vertex would be deleted.
                        deletedVertex = adjacentTriangle.Vertexes[1];
                    }
                    if (newTriangle2 != moveTo.Source && newTriangle2 != moveFrom.Source)
                    {
                        // This vertex would be deleted.
                        deletedVertex = adjacentTriangle.Vertexes[2];
                    }
                    System.Diagnostics.Debug.Assert(deletedVertex != null);
                    standardChangeFactor += deletedVertex.VertexWeight;
                }
            }

            return standardChangeFactor;
        }


        public void ComputeEdgeCostAtVertex(Vertex v)
        {
            // compute the edge collapse cost for all edges that start
            // from vertex v.  Since we are only interested in reducing
            // the object by selecting the min cost edge at each step, we
            // only cache the cost of the least cost edge at this vertex
            // (in member variable collapse) as well as the value of the 
            // cost (in member variable objdist).
            if (v.Neighbours.Count == 0)
            {
                // v doesn't have neighbors so it costs nothing to collapse
                v.CollapseToCandidate = null;
                v.CollapseCost = -0.01f;
                return;
            }
            v.CollapseCost = float.MaxValue;
            v.CollapseToCandidate = null;
            
            // search all neighboring edges for "least cost" edge
            for (int i = 0; i < v.Neighbours.Count; i++)
            {
                float dist;
                dist = ComputeEdgeCollapseCost(v, v.Neighbours[i]);
                if (dist < v.CollapseCost)
                {
                    v.CollapseToCandidate = v.Neighbours[i];  // candidate for edge collapse
                    v.CollapseCost = dist;             // cost of the collapse
                }
            }

            // Sometimes there is no candidate for collapse. 
            if (v.CollapseToCandidate == null)
            {
                //System.Diagnostics.Trace.WriteLine("No collapse candidate found");
            }
        }

        public void ComputeAllEdgeCollapseCosts()
        {
            // For all the edges, compute the difference it would make
            // to the model if it was collapsed.  The least of these
            // per vertex is cached in each vertex object.
            for (int i = 0; i < Vertices.Count; i++)
            {
                ComputeEdgeCostAtVertex(Vertices[i]);
            }
        }


        public void Collapse(Vertex fromVertex, Vertex toVertex)
        {
            // Collapse the edge uv by moving vertex u onto v
            // Actually remove tris on uv, then update tris that
            // have u to have v, and then remove u.
            if (toVertex == null)
            {
                // u is a vertex all by itself so just delete it
                fromVertex.Delete(this);
                return;
            }
            int i;
            List<Vertex> tmp = new List<Vertex>();
            // make tmp a list of all the neighbors of u
            for (i = 0; i < fromVertex.Neighbours.Count; i++)
            {
                tmp.Add(fromVertex.Neighbours[i]);
            }
            // delete triangles on edge uv:
            for (i = fromVertex.Faces.Count - 1; i >= 0; i--)
            {
                if (fromVertex.Faces[i].HasVertex(toVertex))
                {
                    fromVertex.Faces[i].Delete(this);
                }
            }
            // update remaining triangles to have v instead of u
            for (i = fromVertex.Faces.Count - 1; i >= 0; i--)
            {
                fromVertex.Faces[i].ReplaceVertex(fromVertex, toVertex);
            }
            fromVertex.Delete(this);
            // recompute the edge collapse costs for neighboring vertices
            for (i = 0; i < tmp.Count; i++)
            {
                ComputeEdgeCostAtVertex(tmp[i]);
            }
        }


        public void AddVertex(List<Vector3> vert)
        {
	        for(int i=0;i<vert.Count;i++) 
            {
                Vertex v = new Vertex(vert[i], i, this);
            }
	    }

        public class TriangleIndexes
        {
            public int[] VertexIndex = new int[3] { 0, 0, 0 };
        }

        public void AddFaces(List<TriangleIndexes> tri)
        {
	        for(int i=0;i<tri.Count;i++) 
            {
		        Triangle t=
                    new Triangle(
					    this.Vertices[tri[i].VertexIndex[0]],
					    this.Vertices[tri[i].VertexIndex[1]],
					    this.Vertices[tri[i].VertexIndex[2]],
                        this);
	        }
        }


        public Vertex MinimumCostEdge()
        {
            // Find the edge that when collapsed will affect model the least.
            // This funtion actually returns a Vertex, the second vertex
            // of the edge (collapse candidate) is stored in the vertex data.
            // Serious optimization opportunity here: this function currently
            // does a sequential search through an unsorted list :-(
            // Our algorithm could be O(n*lg(n)) instead of O(n*n)
            Vertex mn = Vertices[0];
            for (int i = 0; i < Vertices.Count; i++)
            {
                if (Vertices[i].CollapseCost < mn.CollapseCost)
                {
                    mn = Vertices[i];
                }
            }
            return mn;
        }


        public void Generate(
            List<int> simplificationMap, 
            List<int> permutation)
        {

            ComputeAllEdgeCollapseCosts(); // cache all edge collapse costs
            simplificationMap.Clear();
            simplificationMap.AddRange(new int[this.Vertices.Count]);

            permutation.Clear();
            permutation.AddRange(new int[this.Vertices.Count]);

            this.PreCollapseVertices.AddRange(this.Vertices);

            // reduce the object down to nothing:
            while (Vertices.Count > 0)
            {
                // get the next vertex to collapse
                Vertex minimumCostVertex = MinimumCostEdge();
                // keep track of this vertex, i.e. the collapse ordering
                permutation[minimumCostVertex.ID] = Vertices.Count - 1;
                // keep track of vertex to which we collapse to
                simplificationMap[Vertices.Count - 1] = (minimumCostVertex.CollapseToCandidate != null) ? minimumCostVertex.CollapseToCandidate.ID : -1;
                // Collapse this edge
                Collapse(minimumCostVertex, minimumCostVertex.CollapseToCandidate);
            }

            // reorder the map list based on the collapse ordering
            for (int i = 0; i < simplificationMap.Count; i++)
            {
                if (simplificationMap[i] > -1)
                {
                    simplificationMap[i] = permutation[simplificationMap[i]];
                }
                else
                {
                    // There is no simplificaiton possible
                }
            }
            // The caller of this function should reorder their vertices
            // according to the returned "permutation".
        }
        
        public void AddVertexWeightings(Func<Vertex, float> getVertexWeight)
        {
            ComputeVertexWeights(getVertexWeight);
        }

        public ProgressiveMesh(
            List<Vector3> vertexes, 
            List<TriangleIndexes> triangles
           )
        {
	        AddVertex(vertexes);  // put input data into our data structures
	        AddFaces(triangles);
        }


        public class Edge
        {
            public Vertex V1;
            public Vertex V2;
            public HashSet<Triangle> Faces = new HashSet<Triangle>();

            public Vector3 HalfEdgePosition
            {
                get
                {
                    return (this.V1.Source + this.V2.Source) / 2;
                }
            }

            public bool Contains(Vertex v)
            {
                return this.V1 == v || this.V2 == v;
            }
        }


        internal Edge AddEdge(Edge edge, Triangle borderingTriangle)
        {
            Edge storedEdge = null;
            if (!this.Edges.TryGetValue(edge.HalfEdgePosition, out storedEdge))
            {
                this.Edges.Add(edge.HalfEdgePosition, edge);
                storedEdge = edge;
            }
            storedEdge.Faces.Add(borderingTriangle);
            return storedEdge;
        }

        public void ComputeVertexWeights(Func<Vertex, float> getVertexWeight)
        {
            foreach (Vertex v in this.Vertices)
            {
                v.VertexWeight += getVertexWeight(v);
            }
        }



        /// <summary>
        /// Returns TRUE if this vertex is an internal triangle, otherwise it lies on the hull, and a weighting should be applied to it.
        /// 
        /// The rule is - verticies on an adge which belongs only to one Face form the hull.
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        public static bool IsHullVertex(Vertex vertex)
        {
            foreach (Triangle vertexFace in vertex.Faces)
            {
                foreach (Edge edge in vertexFace.Edges)
                {
                    if (edge.Contains(vertex))
                    {
                        // This edge describes a relationship between this vertex and some faces.
                        // Does this edge have only one attached face ? In which case it must be a hull edge.
                        if (edge.Faces.Count == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
    */ 
}