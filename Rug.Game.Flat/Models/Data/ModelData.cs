﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Data;
using Rug.Game.Core.Maths;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

namespace Rug.Game.Flat.Models 
{
	public class ShapeInfo
	{
		public Vector3 Center;
		public Vector3 Direction;

		public ModelVertex[] ModelVerts;
		public Vector3[] ConvexHull;
	}
        
	public enum ExtraTypes : int
	{
        Volume = 0,
		MainThruster = 1, 
		Thruster = 2, 
		HardPoint = 3,
		Turret = 4,
		Pylon = 5,         
	}

    public enum ModelArrays
    {
        Verts,
        Faces,
        HullVerts,
        Extras,
        Objects,
    }

	public struct FaceData
	{
		public int V0; 
		public int V1; 
		public int V2;

		public int Group; 

		public Color4 Color;

		public Vector3 Normal;

		public void FlipNormal()
		{
			int temp = V0;
			V0 = V1;
			V1 = temp;

			Normal *= -1f;
		}

		public bool Contains(int index)
		{
			return V0 == index || V1 == index || V2 == index;
		}

		public bool HasGreater(int index)
		{
			return V0 > index || V1 > index || V2 > index;
		}

		public void AdjustIndices(int index)
		{
			if (V0 > index)
			{
				V0--;
			}

			if (V1 > index)
			{
				V1--;
			}

			if (V2 > index)
			{
				V2--;
			}
		}
    }

	public struct ExtraData 
	{
		public string Key; 

		public Vector3 Position;
		public Vector3 Direction;
		public ExtraTypes Type;

        public ExtraData Clone(Vector3 offset, float scale)
        {
            return new ExtraData()
            {
                Key = Key,
                Position = (Position * scale) + offset, 
                Direction = Direction, 
                Type = Type, 
            }; 
        }

        public ExtraData Clone(ref Matrix4 transform)
        {
            Vector3 pos, dir = Direction;

            Vector3.Transform(ref Position, ref transform, out pos);

            switch (Type)
            {
                case ExtraTypes.Volume:
                    break;
                case ExtraTypes.MainThruster:
                case ExtraTypes.Thruster:
                case ExtraTypes.HardPoint:
                case ExtraTypes.Turret:
                case ExtraTypes.Pylon:
                    Vector3.TransformVector(ref Direction, ref transform, out dir);
                    break; 
                default:
                    break;
            }
            
            return new ExtraData()
            {
                Key = Key,
                Position = pos,
                Direction = dir,
                Type = Type,
            };
        }
    }

	public class FaceGroup
	{
        public List<FaceData> Faces = new List<FaceData>();

		public Vector3 Normal;

		public Vector3 Center; 
	}

    public enum HullType
    {
        Box, 
        
        ConvexMesh
    }

    public class HullData
    {
        public Vector3 Center;
        public readonly List<FaceData> Faces = new List<FaceData>();

        public HullData Clone()
        {
            HullData data = new HullData()
            {
                Center = this.Center,
            };

            data.Faces.AddRange(Faces);

            return data; 
        }

        public HullData Clone(Vector3 offset, float scale)
        {
            HullData data = new HullData()
            {
                Center = (this.Center * scale) + offset,
            };

            data.Faces.AddRange(Faces);

            return data;
        }

        public HullData Clone(ref Matrix4 transform)
        {
            Vector3 center;

            Vector3.Transform(ref Center, ref transform, out center);

            HullData data = new HullData()
            {
                Center = center,
            };

            data.Faces.AddRange(Faces);

            return data;
        }

        public HullData Clone(int startIndex)
        {
            HullData data = new HullData()
            {
                Center = this.Center,
            };

            CloneFaces(data, startIndex);

            return data; 
        }

        public HullData Clone(int startIndex, ref Matrix4 transform)
        {
            Vector3 center;

            Vector3.Transform(ref Center, ref transform, out center);

            HullData data = new HullData()
            {
                Center = center,
            };

            CloneFaces(data, startIndex);

            return data;
        }

        private void CloneFaces(HullData data, int startIndex)
        {
            foreach (FaceData face in Faces)
            {
                FaceData faceClone = face; 

                faceClone.V0 += startIndex;
                faceClone.V1 += startIndex;
                faceClone.V2 += startIndex;

                data.Faces.Add(faceClone); 
            }
        }
    }

    public enum PrimitiveType
    {
        Sphere,
        Capsule,
        Cylinder,         
        Cone, 
        Box, 
    }

    public struct ModelPrimitive
    {
        public PrimitiveType PrimitiveType; 

        public Vector3 Center;

        public Vector3 Scale;

        public Quaternion Orientation;

        public float Margin;

        public Matrix4 Transform
        {
            get
            {
                return Matrix4.CreateFromQuaternion(Orientation) * Matrix4.CreateTranslation(Center); 
            }
        }

        public ModelPrimitive Clone(Vector3 offset, float scale)
        {
            return new ModelPrimitive()
            {
                PrimitiveType = PrimitiveType,
                Center = (Center * scale) + offset,
                Scale = Scale * scale,
                Orientation = Orientation,
                Margin = Margin * scale, 
            }; 
        }

        public ModelPrimitive Clone(ref Matrix4 transform)
        {
            Vector3 center;

            Vector3.Transform(ref Center, ref transform, out center);

            Vector3 scale = transform.ExtractScale();

            Quaternion orientation = Orientation * transform.ExtractRotation(); 

            return new ModelPrimitive()
            {
                PrimitiveType = PrimitiveType,
                Center = center,
                Scale = Scale * scale,
                Orientation = orientation,
                Margin = Margin * scale.Length,
            }; 
        }
    }

	public class ModelData
	{
		public bool HasChanged { get; set; }

		public string FilePath { get; set; } 

		public List<Vector3> Verts = new List<Vector3>();
		
		public List<FaceData> Faces = new List<FaceData>();

		public List<int> HullVerts = new List<int>();

		public List<ExtraData> Extras = new List<ExtraData>();

        public List<ModelPrimitive> HullPrimitives = new List<ModelPrimitive>();

        public List<HullData> ConvexHullData = new List<HullData>(); 

        public Vector3 Center
        {
            get
            {
                BoundingBox bounds = Bounds;

                return bounds.Center; 
            }
        }

        public BoundingBox Bounds
        {
            get
            {
                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

                foreach (Vector3 vert in Verts)
                {
                    min = Vector3.ComponentMin(min, vert);
                    max = Vector3.ComponentMax(max, vert); 
                }

                return new BoundingBox(min, max); 
            }
        }

        public ModelData() { }

        public ModelData(string path)
        {
            Load(path); 
        }

        public void CopyTo(ModelData other)
        {
            other.Verts.Clear(); 
            other.Faces.Clear(); 
            other.HullVerts.Clear(); 
            other.Extras.Clear();
            other.HullPrimitives.Clear();
            other.ConvexHullData.Clear();

            other.FilePath = FilePath;
            other.Verts.AddRange(Verts);
            other.Faces.AddRange(Faces);
            other.HullVerts.AddRange(HullVerts);
            other.Extras.AddRange(Extras);
            other.HullPrimitives.AddRange(HullPrimitives);

            foreach (HullData data in ConvexHullData)
            {
                other.ConvexHullData.Add(data.Clone());
            } 
        }

        public void Merge(ModelData other)
        {
            int startIndex = Verts.Count;

            Verts.AddRange(other.Verts);

            for (int i = 0; i < other.Faces.Count; i++)
            {
                FaceData data = other.Faces[i]; 

                data.V0 += startIndex;
                data.V1 += startIndex;
                data.V2 += startIndex;

                Faces.Add(data); 
            }

            for (int i = 0; i < other.HullVerts.Count; i++)
            {
                HullVerts.Add(i + startIndex); 
            }

            Extras.AddRange(other.Extras);
            HullPrimitives.AddRange(other.HullPrimitives);

            foreach (HullData data in other.ConvexHullData)
            {
                ConvexHullData.Add(data.Clone(startIndex));
            } 
        }

        public void Merge(ModelData other, ref Matrix4 transform) // Vector3 offset, float scale)
        {
            int startIndex = Verts.Count;

            foreach (Vector3 vert in other.Verts)
            {
                Vector3 v = vert; 
                Vector3 result;

                Vector3.Transform(ref v, ref transform, out result);

                Verts.Add(result);
            }

            int groupBase = Faces.Count; 

            for (int i = 0; i < other.Faces.Count; i++)
            {
                FaceData data = other.Faces[i];

                if (data.Group >= 0)
                {
                    data.Group += groupBase; 
                }

                data.V0 += startIndex;
                data.V1 += startIndex;
                data.V2 += startIndex;

                Faces.Add(data);
            }

            for (int i = 0; i < other.HullVerts.Count; i++)
            {
                HullVerts.Add(i + startIndex);
            }

            foreach (ExtraData data in other.Extras)
            {
                Extras.Add(data.Clone(ref transform));
            }

            foreach (ModelPrimitive data in other.HullPrimitives)
            {
                HullPrimitives.Add(data.Clone(ref transform));
            }

            foreach (HullData data in other.ConvexHullData)
            {
                ConvexHullData.Add(data.Clone(startIndex, ref transform));
            }
        }

		public bool HasExtra(ExtraTypes type, string key)
		{
			foreach (ExtraData data in Extras)
			{
				if (data.Type == type &&
					data.Key == key)
				{
					return true;
				}
			}

			return false; 
		}

		public int GetExtraCount(ExtraTypes type)
		{
			int count = 0; 

			foreach (ExtraData data in Extras)
			{
				if (data.Type == type)
				{
					count++; 
				}
			}

			return count; 
		}

		public ExtraData FindExtra(ExtraTypes type, string key)
		{
			foreach (ExtraData data in Extras)
			{
				if (data.Type == type &&
					data.Key == key)
				{
					return data;
				}
			}

			return new ExtraData()
			{
				Direction = Vector3.UnitY,
				Position = Vector3.Zero,
				Key = key,
				Type = type,
			};
		}

        public Dictionary<int, FaceGroup> GetFaceGroups()
		{
			Dictionary<int, FaceGroup> faceGroups = new Dictionary<int, FaceGroup>(); 

			foreach (FaceData face in Faces)
			{
				if (face.Group == -1)
				{
					continue; 
				}

				if (faceGroups.ContainsKey(face.Group) == false)
				{
					faceGroups.Add(face.Group, new FaceGroup()); 
				}

				faceGroups[face.Group].Faces.Add(face); 
			}

			foreach (FaceGroup group in faceGroups.Values)
			{
				Vector3 center = Vector3.Zero;
				Vector3 normal = Vector3.Zero; 

				foreach (FaceData face in group.Faces)
				{
					Vector3 v0 = Verts[face.V0];
					Vector3 v1 = Verts[face.V1];
					Vector3 v2 = Verts[face.V2];

					Vector3 faceCenter = (v0 + v1 + v2) / 3f;

					Vector3 faceNormal = face.Normal;

					faceNormal.Normalize();

					center += faceCenter;
					normal += faceNormal; 
				}

				center /= group.Faces.Count;
				normal /= group.Faces.Count;

				normal.Normalize();

				group.Center = center;
				group.Normal = normal;
			}

			return faceGroups; 
		}

		public Dictionary<int, FaceGroup> GetFaceGroups(Matrix4 matrix)
		{
			Dictionary<int, FaceGroup> faceGroups = new Dictionary<int, FaceGroup>(); 

			foreach (FaceData face in Faces)
			{
				if (face.Group == -1)
				{
					continue; 
				}

				if (faceGroups.ContainsKey(face.Group) == false)
				{
					faceGroups.Add(face.Group, new FaceGroup()); 
				}

				faceGroups[face.Group].Faces.Add(face); 
			}

			foreach (FaceGroup group in faceGroups.Values)
			{
				Vector3 center = Vector3.Zero;
				Vector3 normal = Vector3.Zero; 

				foreach (FaceData face in group.Faces)
				{
					Vector3 v0 = Vector3.Transform(Verts[face.V0], matrix);
					Vector3 v1 = Vector3.Transform(Verts[face.V1], matrix);
					Vector3 v2 = Vector3.Transform(Verts[face.V2], matrix);

					//v0.Z = -v0.Z;
					//v1.Z = -v1.Z;
					//v2.Z = -v2.Z;

					Vector3 faceCenter = (v0 + v1 + v2) / 3f;

					Vector3 faceNormal = Vector3.TransformNormal(face.Normal, matrix);

					faceNormal.Normalize();

					center += faceCenter;
					normal += faceNormal; 
				}

				center /= group.Faces.Count;
				normal /= group.Faces.Count;

				normal.Normalize();

				group.Center = center;
				group.Normal = normal;
			}

			return faceGroups; 
		}

        public ShapeInfo GetShapeInfo()
        {
            List<ModelVertex> verts = new List<ModelVertex>();
            List<Vector3> convexHull = new List<Vector3>();

            Dictionary<int, FaceGroup> groups = GetFaceGroups();

            foreach (FaceData face in Faces)
            {
                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];

                Vector3 faceCenter;
                Vector3 normal;

                if (face.Group == -1)
                {
                    faceCenter = (v0 + v1 + v2) / 3f;

                    normal = face.Normal;
                }
                else
                {
                    FaceGroup group = groups[face.Group];

                    faceCenter = group.Center;
                    normal = group.Normal;
                }		

                Color3 color = face.Color;

                float specularFactor = ModelHelper.DefaultSpecFactor;
                float specularAmount = ModelHelper.DefaultSpecAmount; // face.Color == Color4.Black ? 256f : 3f;

                //float shininess = face.Color == Color4.Black ? 8f : 3f;

                verts.Add(new ModelVertex() { Position = v0, Normal = normal, Color = color, Face = faceCenter, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
                verts.Add(new ModelVertex() { Position = v1, Normal = normal, Color = color, Face = faceCenter, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
                verts.Add(new ModelVertex() { Position = v2, Normal = normal, Color = color, Face = faceCenter, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
            }

            foreach (int index in HullVerts)
            {
                convexHull.Add(new Vector3(Verts[index].Xy));
            }

            return new ShapeInfo()
            {
                Center = Vector3.Zero,
                Direction = -Vector3.UnitY,
                ModelVerts = verts.ToArray(),
                ConvexHull = convexHull.ToArray(),
            };
        }

		public ShapeInfo GetShapeInfo(Matrix4 matrix)
		{
			List<ModelVertex> verts = new List<ModelVertex>();
			List<Vector3> convexHull = new List<Vector3>();
			
			Dictionary<int, FaceGroup> groups = GetFaceGroups(matrix);

			foreach (FaceData face in Faces) 
			{
				Vector3 v0 = Vector3.Transform(Verts[face.V0], matrix);				
				Vector3 v1 = Vector3.Transform(Verts[face.V2], matrix);
				Vector3 v2 = Vector3.Transform(Verts[face.V1], matrix);

				//v0.Z = -v0.Z;
				//v1.Z = -v1.Z;
				//v2.Z = -v2.Z;

				Vector3 faceCenter; 
				Vector3 normal;

				if (face.Group == -1)
				{
					faceCenter = (v0 + v1 + v2) / 3f;

                    normal = face.Normal; 
				}
				else
				{
					FaceGroup group = groups[face.Group];

					faceCenter = group.Center;
					normal = group.Normal; 
				}

                normal = Vector3.TransformNormal(normal, matrix);

                normal.Normalize();

				//normal = -normal; 			
				
				Color3 color = face.Color;

                faceCenter = Vector3.Transform(faceCenter, matrix);

				float specularFactor = ModelHelper.DefaultSpecFactor;
				float specularAmount = ModelHelper.DefaultSpecAmount; // face.Color == Color4.Black ? 256f : 3f;

				//float shininess = face.Color == Color4.Black ? 8f : 3f;

				verts.Add(new ModelVertex() { Position = v0, Normal = normal, Color = color, Face = faceCenter, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
				verts.Add(new ModelVertex() { Position = v1, Normal = normal, Color = color, Face = faceCenter, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
				verts.Add(new ModelVertex() { Position = v2, Normal = normal, Color = color, Face = faceCenter, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
			}

			foreach (int index in HullVerts)
			{
				convexHull.Add(Vector3.Transform(new Vector3(Verts[index].Xy), matrix));
			}

			return new ShapeInfo()
			{
				Center = Vector3.Transform(Vector3.Zero, matrix),
				Direction = Vector3.TransformNormal(-Vector3.UnitY, matrix),
				ModelVerts = verts.ToArray(),
				ConvexHull = convexHull.ToArray(), 
			};
		}

        public ShapeInfo GetShapeInfo_Smoothed(Matrix4 matrix)
        {
            List<ModelVertex> verts = new List<ModelVertex>();
            List<Vector3> convexHull = new List<Vector3>();

            Dictionary<int, FaceGroup> groups = GetFaceGroups(matrix);

            Vector3[] normals = new Vector3[Verts.Count];

            for (int i = 0; i < Verts.Count; i++)
            {
                List<int> groupsIncluded = new List<int>(); 

                Vector3 normal = new Vector3(0.0f, 0.0f, 0.0f);
                int count = 0; 

                foreach (FaceData face in Faces)
                {
                    if (face.Contains(i) == true)
                    {                        
                        if (face.Group == -1)
                        {
                            normal += face.Normal;
                        }
                        else if (groupsIncluded.Contains(face.Group) == true)
                        {
                            continue; 
                        }
                        else
                        {
                            FaceGroup group = groups[face.Group];

                            normal += group.Normal;

                            groupsIncluded.Add(face.Group); 
                        }

                        count++;
                    }
                }

                normal /= (float)count;

                normal = Vector3.TransformNormal(normal, matrix);

                normal.Normalize();

                normals[i] = normal;
            }

            foreach (FaceData face in Faces)
            {
                Vector3 v0 = Vector3.Transform(Verts[face.V0], matrix);
                Vector3 v1 = Vector3.Transform(Verts[face.V2], matrix);
                Vector3 v2 = Vector3.Transform(Verts[face.V1], matrix);

                Vector3 normal0 = normals[face.V0];
                Vector3 normal1 = normals[face.V1];
                Vector3 normal2 = normals[face.V2];

                Vector3 faceCenter;
                
                if (face.Group == -1)
                {
                    faceCenter = (v0 + v1 + v2) / 3f;
                }
                else
                {
                    FaceGroup group = groups[face.Group];

                    faceCenter = group.Center;
                }

                faceCenter = Vector3.Transform(faceCenter, matrix);

                Color3 color = face.Color;

                float specularFactor = ModelHelper.DefaultSpecFactor;
                float specularAmount = ModelHelper.DefaultSpecAmount;

                verts.Add(new ModelVertex() { Position = v0, Normal = normal0, Color = color, Face = v0, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
                verts.Add(new ModelVertex() { Position = v1, Normal = normal1, Color = color, Face = v1, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
                verts.Add(new ModelVertex() { Position = v2, Normal = normal2, Color = color, Face = v2, SpecularFactor = specularFactor, SpecularAmount = specularAmount });
            }

            foreach (int index in HullVerts)
            {
                convexHull.Add(Vector3.Transform(new Vector3(Verts[index].Xy), matrix));
            }

            return new ShapeInfo()
            {
                Center = Vector3.Transform(Vector3.Zero, matrix),
                Direction = Vector3.TransformNormal(-Vector3.UnitY, matrix),
                ModelVerts = verts.ToArray(),
                ConvexHull = convexHull.ToArray(),
            };
        }

        public int[] GetIndices()
        {
            int[] indices = new int[Faces.Count * 3];

            int i = 0; 
            foreach (FaceData face in Faces)
            {
                indices[i++] = face.V0;
                indices[i++] = face.V1;
                indices[i++] = face.V2;
            }

            return indices; 
        }

		public void CalcuateFaceNormals()
		{
			for (int i = 0; i < Faces.Count; i++)
			{
				FaceData face = Faces[i];

				face.Normal = CalcNormal(Verts[face.V0], Verts[face.V1], Verts[face.V2]);

				Faces[i] = face; 
			}
		}

		//  Modified from http://www.fullonsoftware.co.uk/snippets/content/Math_-_Calculating_Face_Normals.pdf
		public static Vector3 CalcNormal(Vector3 p1, Vector3 p2, Vector3 p3)
		{
			Vector3 v1 = (p2 - p1);
			Vector3 v2 = (p3 - p1);

			Vector3 Normal = new Vector3();

			Normal.X = (v1.Y * v2.Z) - (v1.Z * v2.Y);
			Normal.Y = -((v2.Z * v1.X) - (v2.X * v1.Z));
			Normal.Z = (v1.X * v2.Y) - (v1.Y * v2.X);

			/* 
			Vector3 surfaceNormal = new Vector3((V1.Y * V2.Z) - (V1.Z - V2.Y),
												-((V2.Z * V1.X) - (V2.X * V1.Z)),
												  (V1.X - V2.Y) - (V1.Y - V2.X));
			*/
			Normal.Normalize();

			// Dont forget to normalize if needed
			return Normal;
		}

		public void Load(string path)
		{
			FilePath = path; 

			try
			{
				XmlDocument doc = new XmlDocument();

				// load the options from the resolved path				
                FileHelper.LoadXml(doc, path); 

				XmlNode node = doc.DocumentElement;

				// if the node is not null 
				if (node != null)
				{
					Verts = LoadVerts(node);
					Faces = LoadFaces(node);
					HullVerts = LoadHullVerts(node);
					Extras = LoadExtras(node);
                    HullPrimitives = LoadHullPrimitives(node);
                    ConvexHullData = LoadConvexHullData(node); 

					CalcuateFaceNormals(); 
				}
			}
			catch (Exception ex)
			{
				// somthing went wrong, tell the user
				throw new Exception("Could load not model data", ex);
			}
		}

		public void Save(string filepath)
		{
			FilePath = filepath; 

			string path = Helper.ResolvePath(filepath);

			try
			{
				XmlDocument doc = new XmlDocument();

				XmlElement node = Helper.CreateElement(doc, "Model");
				
				SaveVerts(node, Verts);
				SaveFaces(node, Faces);
				SaveHullVerts(node, HullVerts);
				SaveExtras(node, Extras);
                SaveHullPrimitives(node, HullPrimitives);
                SaveConvexHullData(node, ConvexHullData); 

				doc.AppendChild(node);

				Helper.EnsurePathExists(path);

				doc.Save(path);
			}
			catch (Exception ex)
			{
				throw new Exception("Could save not model data", ex);
			}
		}

		private List<Vector3> LoadVerts(XmlNode node)
		{
			List<Vector3> data = new List<Vector3>();

			foreach (XmlNode n in node.SelectNodes("Verts/v"))
			{
				data.Add(Helper.GetAttributeValue(n, "value", Vector3.Zero));				
			}

			return data;
		}

		private void SaveVerts(XmlElement node, List<Vector3> verts)
		{
			XmlElement listNode = node.AppendChild(Helper.CreateElement(node, "Verts")) as XmlElement;

			foreach (Vector3 vert in verts)
			{
				XmlElement vertNode = listNode.AppendChild(Helper.CreateElement(listNode, "v")) as XmlElement;

				Helper.AppendAttributeAndValue(vertNode, "value", vert);
			}
		}

		private List<FaceData> LoadFaces(XmlNode node)
		{
			List<FaceData> data = new List<FaceData>();

			foreach (XmlNode n in node.SelectNodes("Faces/f"))
			{
				FaceData face = new FaceData()
				{
					Color = Helper.GetAttributeValue(n, "Color", Color4.White),
					Group = Helper.GetAttributeValue(n, "Group", -1),		
					V0 = Helper.GetAttributeValue(n, "V0", -1),
					V1 = Helper.GetAttributeValue(n, "V1", -1),
					V2 = Helper.GetAttributeValue(n, "V2", -1),								
				};

				data.Add(face);
			}

			return data;
		}

		private void SaveFaces(XmlElement node, List<FaceData> faces)
		{
			XmlElement listNode = node.AppendChild(Helper.CreateElement(node, "Faces")) as XmlElement;

			foreach (FaceData face in faces)
			{
				XmlElement faceNode = listNode.AppendChild(Helper.CreateElement(listNode, "f")) as XmlElement;

				Helper.AppendAttributeAndValue(faceNode, "Color", face.Color);

				if (face.Group != -1)
				{
					Helper.AppendAttributeAndValue(faceNode, "Group", face.Group);
				}

				Helper.AppendAttributeAndValue(faceNode, "V0", face.V0);
				Helper.AppendAttributeAndValue(faceNode, "V1", face.V1);
				Helper.AppendAttributeAndValue(faceNode, "V2", face.V2);
			}
		}

		private List<int> LoadHullVerts(XmlNode node)
		{
			List<int> data = new List<int>();

			foreach (XmlNode n in node.SelectNodes("HullVerts/i"))
			{
				data.Add(Helper.GetAttributeValue(n, "value", -1));
			}

			return data;
		}

		private void SaveHullVerts(XmlElement node, List<int> hullVerts)
		{
			XmlElement listNode = node.AppendChild(Helper.CreateElement(node, "HullVerts")) as XmlElement;

			foreach (int vert in hullVerts)
			{
				XmlElement vertNode = listNode.AppendChild(Helper.CreateElement(listNode, "i")) as XmlElement;

				Helper.AppendAttributeAndValue(vertNode, "value", vert);
			}
		}

		private List<ExtraData> LoadExtras(XmlNode node)
		{
			List<ExtraData> data = new List<ExtraData>();

			foreach (XmlNode n in node.SelectNodes("Extras/extra"))
			{
				ExtraData extra = new ExtraData()
				{
					Key = Helper.GetAttributeValue(n, "Key", ""),
					Position = Helper.GetAttributeValue(n, "Position", Vector3.Zero),
					Direction = Helper.GetAttributeValue(n, "Direction", Vector3.UnitY),
					Type = Helper.GetAttributeValue(n, "Type", ExtraTypes.MainThruster),
				};

				data.Add(extra);
			}

			return data;
		}

		private void SaveExtras(XmlElement node, List<ExtraData> extras)
		{
			XmlElement listNode = node.AppendChild(Helper.CreateElement(node, "Extras")) as XmlElement;

			foreach (ExtraData extra in extras)
			{
				XmlElement extraNode = listNode.AppendChild(Helper.CreateElement(listNode, "extra")) as XmlElement;

				Helper.AppendAttributeAndValue(extraNode, "Key", extra.Key);
				Helper.AppendAttributeAndValue(extraNode, "Position", extra.Position);
				Helper.AppendAttributeAndValue(extraNode, "Direction", extra.Direction);
				Helper.AppendAttributeAndValue(extraNode, "Type", extra.Type);
			}
		}


        private List<ModelPrimitive> LoadHullPrimitives(XmlNode node)
		{
            List<ModelPrimitive> primitives = new List<ModelPrimitive>();

            foreach (XmlNode n in node.SelectNodes("HullPrimitives/Primitive"))
			{
                ModelPrimitive primitive = new ModelPrimitive()
				{
                    PrimitiveType = Helper.GetAttributeValue(n, "PrimitiveType", PrimitiveType.Sphere),
                    Center = Helper.GetAttributeValue(n, "Center", Vector3.Zero),
                    Scale = Helper.GetAttributeValue(n, "Scale", new Vector3(1f, 1f, 1f)),
                    Orientation = Helper.GetAttributeValue(n, "Orientation", Quaternion.Identity),
                    Margin = Helper.GetAttributeValue(n, "Margin", 1f),
				};

				primitives.Add(primitive);
			}

			return primitives;
		}

        private void SaveHullPrimitives(XmlElement node, List<ModelPrimitive> primitives)
		{
            XmlElement listNode = node.AppendChild(Helper.CreateElement(node, "HullPrimitives")) as XmlElement;

            foreach (ModelPrimitive primitive in primitives)
			{
                XmlElement primitiveNode = listNode.AppendChild(Helper.CreateElement(listNode, "Primitive")) as XmlElement;

                Helper.AppendAttributeAndValue(primitiveNode, "PrimitiveType", primitive.PrimitiveType);
                Helper.AppendAttributeAndValue(primitiveNode, "Center", primitive.Center);
                Helper.AppendAttributeAndValue(primitiveNode, "Scale", primitive.Scale);
                Helper.AppendAttributeAndValue(primitiveNode, "Orientation", primitive.Orientation);
                Helper.AppendAttributeAndValue(primitiveNode, "Margin", primitive.Margin);
			}
		}

        private List<HullData> LoadConvexHullData(XmlNode node)
        {
            List<HullData> hullData = new List<HullData>();

            foreach (XmlNode n in node.SelectNodes("HullData/Hull"))
            {
                HullData hull = new HullData()
                {
                    Center = Helper.GetAttributeValue(n, "Center", Vector3.Zero),
                };

                hull.Faces.AddRange(LoadFaces(n));

                hullData.Add(hull);
            }

            return hullData;
        }

        private void SaveConvexHullData(XmlElement node, List<HullData> convexHullData)
        {
            XmlElement listNode = node.AppendChild(Helper.CreateElement(node, "HullData")) as XmlElement;

            foreach (HullData hullData in convexHullData)
            {
                XmlElement hullNode = listNode.AppendChild(Helper.CreateElement(listNode, "Hull")) as XmlElement;

                Helper.AppendAttributeAndValue(hullNode, "Center", hullData.Center);

                SaveFaces(hullNode, hullData.Faces);
            }
        }

        public bool Intersects(ref Core.Maths.Ray ray, out Vector3 point)
        {
            bool found = false; 
            float dist = float.MaxValue;
            point = Vector3.Zero; 

            foreach (FaceData face in Faces)
			{
                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];

                float foundDist; 
                if (Collision.RayIntersectsTriangle(ref ray, ref v0, ref v1, ref v2, out foundDist) == false) 
                {
                    continue;                     
                }

                if (foundDist > dist)
                {
                    continue;
                }

                found = true; 

                Collision.RayIntersectsTriangle(ref ray, ref v0, ref v1, ref v2, out point);
            }

            return found; 
        }

        public bool IntersectsSlope(ref Ray ray, float minSlope, float maxSlope, out Vector3 point)
        {
            bool found = false;
            float dist = float.MaxValue;
            point = Vector3.Zero;

            foreach (FaceData face in Faces)
            {
                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];

                float foundDist;
                if (Collision.RayIntersectsTriangle(ref ray, ref v0, ref v1, ref v2, out foundDist) == false)
                {
                    continue;
                }

                if (foundDist > dist)
                {
                    continue;
                }

                float dot = Vector3.Dot(face.Normal, -ray.Direction);

                //if (dot > minSlope  || dot < maxSlope)
                //if (dot < maxSlope)
                if (dot < minSlope || dot > maxSlope)
                {
                    continue; 
                }

                dist = foundDist; 

                found = true;

                Collision.RayIntersectsTriangle(ref ray, ref v0, ref v1, ref v2, out point);
            }

            return found;
        }

        public bool FindIntersectionFaces(ref Ray ray, out int[] faces)
        {
            List<int> found = new List<int>(); 

            for (int i = 0; i < Faces.Count; i++)
            {
                FaceData face = Faces[i];

                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];
                
                float foundDist;
                if (Collision.RayIntersectsTriangle(ref ray, ref v0, ref v1, ref v2, out foundDist) == false)
                {
                    continue;
                }

                found.Add(i); 
            }

            faces = found.ToArray(); 

            return found.Count > 0; 
        }

        public bool FindIntersectionFaces_Faceing(ref Ray ray, out int[] faces)
        {
            int found = -1;
            float dist = float.MaxValue;

            for (int i = 0; i < Faces.Count; i++)
            {
                FaceData face = Faces[i];

                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];
                
                float foundDist;
                if (Collision.RayIntersectsTriangle(ref ray, ref v0, ref v1, ref v2, out foundDist) == false)
                {
                    continue;
                }

                if (foundDist > dist)
                {
                    continue;
                }

                float dot = Vector3.Dot(face.Normal, -ray.Direction);

                if (dot <= 0)
                {
                    continue;
                }

                found = i; 
                dist = foundDist; 
            }

            faces = new int[] { found };

            return found != -1; 
        }

        public bool FindIntersectionFaces_Faceing(Plane[] planes, ref Vector3 rayNormal, out int[] faces)
        {
            List<int> found = new List<int>();

            for (int i = 0; i < Faces.Count; i++)
            {
                FaceData face = Faces[i];

                float dot = Vector3.Dot(face.Normal, -rayNormal);

                if (dot <= 0)
                {
                    continue;
                }

                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];

                bool inside0 = true;

                for (int j = 0; j < planes.Length; j++)
                {
                    if (Collision.PlaneIntersectsPoint(ref planes[j], ref v0) != PlaneIntersectionType.Front)
                    {
                        inside0 = false;
                        break;
                    }
                }


                bool inside1 = true;

                for (int j = 0; j < planes.Length; j++)
                {
                    if (Collision.PlaneIntersectsPoint(ref planes[j], ref v1) != PlaneIntersectionType.Front)
                    {
                        inside1 = false;
                        break;
                    }
                }


                bool inside2 = true;

                for (int j = 0; j < planes.Length; j++)
                {
                    if (Collision.PlaneIntersectsPoint(ref planes[j], ref v2) != PlaneIntersectionType.Front)
                    {
                        inside2 = false;
                        break;
                    }
                }

                if (inside0 == false && inside1 == false && inside2 == false)
                {
                    continue;
                }


                found.Add(i);
            }

            faces = found.ToArray();

            return found.Count > 0; 
        }

        public bool FindIntersectionFaces(Plane[] planes, out int[] faces)
        {
            List<int> found = new List<int>();

            for (int i = 0; i < Faces.Count; i++)
            {
                FaceData face = Faces[i];

                Vector3 v0 = Verts[face.V0];
                Vector3 v1 = Verts[face.V2];
                Vector3 v2 = Verts[face.V1];

                bool inside0 = true; 

                for (int j = 0; j < planes.Length; j++) 
                {
                    if (Collision.PlaneIntersectsPoint(ref planes[j], ref v0) != PlaneIntersectionType.Front)
                    {
                        inside0 = false;
                        break; 
                    }
                }


                bool inside1 = true;

                for (int j = 0; j < planes.Length; j++)
                {
                    if (Collision.PlaneIntersectsPoint(ref planes[j], ref v1) != PlaneIntersectionType.Front)
                    {
                        inside1 = false;
                        break;
                    }
                }


                bool inside2 = true;

                for (int j = 0; j < planes.Length; j++)
                {
                    if (Collision.PlaneIntersectsPoint(ref planes[j], ref v2) != PlaneIntersectionType.Front)
                    {
                        inside2 = false;
                        break;
                    }
                }

                if (inside0 == false && inside1 == false && inside2 == false)
                {
                    continue; 
                }

                found.Add(i);
            }

            faces = found.ToArray();

            return found.Count > 0; 
        }

        public Vector3 CalculateFaceCenter(FaceData face)
        {
            Vector3 v0 = Verts[face.V0];
            Vector3 v1 = Verts[face.V2];
            Vector3 v2 = Verts[face.V1];

            Vector3 min = Vector3.ComponentMin(v0, Vector3.ComponentMin(v1, v2));
            Vector3 max = Vector3.ComponentMax(v0, Vector3.ComponentMax(v1, v2));

            return ((max - min) * 0.5f) + min; 
        }


        public void WeldVerts(float distance)
        {
            float distSquared = distance * distance; 

            for (int i = 0; i < Verts.Count; i++)
            {
                Vector3 vert = Verts[i]; 

                for (int j = i + 1; j < Verts.Count; j++)
                {
                    Vector3 other = Verts[j];

                    if ((other - vert).LengthSquared < distSquared)
                    {
                        for (int f = 0; f < Faces.Count; f++)
                        {
                            FaceData data = Faces[f];

                            if (j == data.V0) data.V0 = i;
                            if (j == data.V1) data.V1 = i;
                            if (j == data.V2) data.V2 = i;

                            data.AdjustIndices(j);

                            Faces[f] = data;
                        }

                        Verts.RemoveAt(j);

                        j--; 
                    }
                }
            }
        }

        public void CullDeadVerts()
        {
            int count = 0; 
            for (int i = 0; i < Verts.Count; i++)
            {
                bool found = false; 

                for (int f = 0; f < Faces.Count; f++)
                {
                    FaceData data = Faces[f];

                    if (i == data.V0) found = true;
                    if (i == data.V1) found = true;
                    if (i == data.V2) found = true;
                }

                if (found == false)
                {
                    count++; 
                }
            }

            if (count > 0)
            {
                throw new Exception(count + " dead verts detected.");
            }
        }
    }
}
