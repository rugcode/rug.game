﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Flat.Models 
{
	[StructLayout(LayoutKind.Sequential)]
	public struct ExtrudedModelInstance
	{
		public Vector4 Center1;
		public Vector4 Center2;
		public Vector2 Size;
		public Color4 Color;

		public enum Elements : int { Center1 = 0, Center2 = 1, Size = 2, Color = 3 };
		public static readonly int Stride;

		public static readonly int Center1Offset;
		public static readonly int Center2Offset;
		public static readonly int SizeOffset;
		public static readonly int ColorOffset;

		static ExtrudedModelInstance()
		{
			Stride = BlittableValueType<ExtrudedModelInstance>.Stride;

			Center1Offset = (int)Marshal.OffsetOf(typeof(ExtrudedModelInstance), "Center1");
			Center2Offset = (int)Marshal.OffsetOf(typeof(ExtrudedModelInstance), "Center2");
			SizeOffset = (int)Marshal.OffsetOf(typeof(ExtrudedModelInstance), "Size");
			ColorOffset = (int)Marshal.OffsetOf(typeof(ExtrudedModelInstance), "Color");

			if (ColorOffset + BlittableValueType<Color4>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Center1, 4, VertexAttribPointerType.Float, false, Stride, Center1Offset);
			GL.VertexAttribPointer((int)Elements.Center2, 4, VertexAttribPointerType.Float, false, Stride, Center2Offset);
			GL.VertexAttribPointer((int)Elements.Size, 2, VertexAttribPointerType.Float, false, Stride, SizeOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

			GL.EnableVertexAttribArray((int)Elements.Center1);
			GL.EnableVertexAttribArray((int)Elements.Center2);
			GL.EnableVertexAttribArray((int)Elements.Size);
			GL.EnableVertexAttribArray((int)Elements.Color);	
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Center1);
			GL.DisableVertexAttribArray((int)Elements.Center2);
			GL.DisableVertexAttribArray((int)Elements.Size);
			GL.DisableVertexAttribArray((int)Elements.Color);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return ExtrudedModelInstance.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Center1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Center1Offset);
				GL.VertexAttribPointer((int)Elements.Center2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Center2Offset);
				GL.VertexAttribPointer((int)Elements.Size + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, SizeOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Center1 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Center2 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Size + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				baseLocation += 4;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Center1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Center1Offset);
				GL.VertexAttribPointer((int)Elements.Center2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Center2Offset);
				GL.VertexAttribPointer((int)Elements.Size + baseLocation, 2, VertexAttribPointerType.Float, false, Stride, SizeOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Center1 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Center2 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Size + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Center1 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Center2 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Size + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);

				baseLocation += 4;
			}

			#endregion
		}

		#endregion
	}
}
