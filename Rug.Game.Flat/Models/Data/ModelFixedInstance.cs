﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Flat.Models 
{
	[StructLayout(LayoutKind.Sequential)]
	public struct ModelFixedInstance
	{		
		public Vector4 Center;
		public Color4 Color;

		public enum Elements : int { Center = 0, Color = 1, };
		public static readonly int Stride;

		public static readonly int CenterOffset;
		public static readonly int ColorOffset;

		static ModelFixedInstance()
		{
			Stride = BlittableValueType<ModelFixedInstance>.Stride;

			CenterOffset = (int)Marshal.OffsetOf(typeof(ModelFixedInstance), "Center");
			ColorOffset = (int)Marshal.OffsetOf(typeof(ModelFixedInstance), "Color");

			if (ColorOffset + BlittableValueType<Color4>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Center, 4, VertexAttribPointerType.Float, false, Stride, CenterOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

			GL.EnableVertexAttribArray((int)Elements.Center);
			GL.EnableVertexAttribArray((int)Elements.Color);	
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Center);
			GL.DisableVertexAttribArray((int)Elements.Color);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return ModelFixedInstance.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Center + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, CenterOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Center + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				baseLocation += 2;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Center + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, CenterOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Center + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Center + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);

				baseLocation += 2;
			}

			#endregion
		}

		#endregion
	}
}
