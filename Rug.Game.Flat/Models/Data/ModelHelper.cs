﻿using OpenTK;
using Rug.Game.Core.MeshData;

namespace Rug.Game.Flat.Models
{
	public static class ModelHelper
	{
		public static float DefaultSpecFactor = 0.5f;
		public static float DefaultSpecAmount = 1f; 

		#region Mesh To Model Smooth

		public static void MeshToModel_Smooth(string path, out ModelVertex[] modelVerts)
		{
			MeshVertex[] verts;
			ushort[] indices;

			ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

			modelVerts = new ModelVertex[indices.Length];

			int i = 0;
			for (int j = 0; j < indices.Length; j += 3)
			{
				MeshVertex vert1 = verts[j + 0];
				MeshVertex vert2 = verts[j + 2];
				MeshVertex vert3 = verts[j + 1];

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert1.Position,
					Normal = vert1.Normal,
					Color = vert1.Color,					
					Face = vert1.Position, 
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert2.Position,
					Normal = vert2.Normal,
					Color = vert2.Color,
					Face = vert2.Position,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert3.Position,
					Normal = vert3.Normal,
					Color = vert3.Color,
					Face = vert3.Position,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};
			}
		}

		public static void MeshToModel_Smooth(string path, out ModelVertex[] modelVerts, out ushort[] indices)
		{
			MeshVertex[] verts;

			ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

			modelVerts = new ModelVertex[indices.Length];

			int i = 0;
			for (int j = 0; j < indices.Length; j += 3)
			{
				MeshVertex vert1 = verts[j + 0];
				MeshVertex vert2 = verts[j + 2];
				MeshVertex vert3 = verts[j + 1];

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert1.Position,
					Normal = vert1.Normal,
					Color = vert1.Color,
					Face = vert1.Position,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert2.Position,
					Normal = vert2.Normal,
					Color = vert2.Color,
					Face = vert2.Position,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert3.Position,
					Normal = vert3.Normal,
					Color = vert3.Color,
					Face = vert3.Position,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};
			}
		}


        public static void MeshToModel_Smooth(string path, out TexturedModelVertex[] modelVerts)
        {
            MeshVertex[] verts;
            ushort[] indices;

            ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

            modelVerts = new TexturedModelVertex[indices.Length];

            int i = 0;
            for (int j = 0; j < indices.Length; j += 3)
            {
                MeshVertex vert1 = verts[j + 0];
                MeshVertex vert2 = verts[j + 2];
                MeshVertex vert3 = verts[j + 1];

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert1.Position,
                    Normal = vert1.Normal,
                    Color = vert1.Color,
                    Face = vert1.Position,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert1.TextureCoords,
                };

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert2.Position,
                    Normal = vert2.Normal,
                    Color = vert2.Color,
                    Face = vert2.Position,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert2.TextureCoords,
                };

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert3.Position,
                    Normal = vert3.Normal,
                    Color = vert3.Color,
                    Face = vert3.Position,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert3.TextureCoords,
                };
            }
        }

        public static void MeshToModel_Smooth(string path, out TexturedModelVertex[] modelVerts, out ushort[] indices)
        {
            MeshVertex[] verts;

            ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

            modelVerts = new TexturedModelVertex[indices.Length];

            int i = 0;
            for (int j = 0; j < indices.Length; j += 3)
            {
                MeshVertex vert1 = verts[j + 0];
                MeshVertex vert2 = verts[j + 2];
                MeshVertex vert3 = verts[j + 1];

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert1.Position,
                    Normal = vert1.Normal,
                    Color = vert1.Color,
                    Face = vert1.Position,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert1.TextureCoords,
                };

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert2.Position,
                    Normal = vert2.Normal,
                    Color = vert2.Color,
                    Face = vert2.Position,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert2.TextureCoords,
                };

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert3.Position,
                    Normal = vert3.Normal,
                    Color = vert3.Color,
                    Face = vert3.Position,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert3.TextureCoords,
                };
            }
        }

		#endregion

		#region Mesh To Model Flat

		public static void MeshToModel_Flat(string path, out ModelVertex[] modelVerts)
		{
			MeshVertex[] verts;
			ushort[] indices;

			ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

			modelVerts = new ModelVertex[indices.Length];

			int i = 0;
			for (int j = 0; j < indices.Length; j += 3) 
			{
				MeshVertex vert1 = verts[j + 0];				
				MeshVertex vert2 = verts[j + 2];
				MeshVertex vert3 = verts[j + 1];

				Vector3 faceCenter = (vert1.Position + vert2.Position + vert3.Position) / 3f; 
				Vector3 faceNormal = (vert1.Normal + vert2.Normal + vert3.Normal) / 3f;

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert1.Position,
					Normal = faceNormal,
					Color = vert1.Color,
					Face = faceCenter,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert2.Position,
					Normal = faceNormal,
					Color = vert2.Color,
					Face = faceCenter,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelVerts[i++] = new ModelVertex()
				{
					Position = vert3.Position,
					Normal = faceNormal,
					Color = vert3.Color,
					Face = faceCenter,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};
			}
		}


		public static void MeshToModel_Flat(string path, out ModelVertex[] modelVerts, out ushort[] modelIndices)
		{
			MeshVertex[] verts;
			ushort[] indices;

			ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

			modelVerts = new ModelVertex[indices.Length];
			modelIndices = new ushort[indices.Length]; 

			int i = 0;
			for (int j = 0; j < indices.Length; j += 3)
			{
				MeshVertex vert1 = verts[j + 0];				
				MeshVertex vert2 = verts[j + 2];
				MeshVertex vert3 = verts[j + 1];

				Vector3 faceCenter = (vert1.Position + vert2.Position + vert3.Position) / 3f;
				Vector3 faceNormal = (vert1.Normal + vert2.Normal + vert3.Normal) / 3f;

				modelIndices[i] = (ushort)i;
				modelVerts[i++] = new ModelVertex()
				{
					Position = vert1.Position,
					Normal = faceNormal,
					Color = vert1.Color,
					Face = faceCenter,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelIndices[i] = (ushort)i;
				modelVerts[i++] = new ModelVertex()
				{
					Position = vert2.Position,
					Normal = faceNormal,
					Color = vert2.Color,
					Face = faceCenter,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};

				modelIndices[i] = (ushort)i;
				modelVerts[i++] = new ModelVertex()
				{
					Position = vert3.Position,
					Normal = faceNormal,
					Color = vert3.Color,
					Face = faceCenter,
					SpecularFactor = DefaultSpecFactor,
					SpecularAmount = DefaultSpecAmount,
				};
			}
		}



        public static void MeshToModel_Flat(string path, out TexturedModelVertex[] modelVerts)
        {
            MeshVertex[] verts;
            ushort[] indices;

            ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

            modelVerts = new TexturedModelVertex[indices.Length];

            int i = 0;
            for (int j = 0; j < indices.Length; j += 3)
            {
                MeshVertex vert1 = verts[j + 0];
                MeshVertex vert2 = verts[j + 2];
                MeshVertex vert3 = verts[j + 1];

                Vector3 faceCenter = (vert1.Position + vert2.Position + vert3.Position) / 3f;
                Vector3 faceNormal = (vert1.Normal + vert2.Normal + vert3.Normal) / 3f;

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert1.Position,
                    Normal = faceNormal,
                    Color = vert1.Color,
                    Face = faceCenter,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert1.TextureCoords,
                };

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert2.Position,
                    Normal = faceNormal,
                    Color = vert2.Color,
                    Face = faceCenter,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert2.TextureCoords,
                };

                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert3.Position,
                    Normal = faceNormal,
                    Color = vert3.Color,
                    Face = faceCenter,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert3.TextureCoords,
                };
            }
        }


        public static void MeshToModel_Flat(string path, out TexturedModelVertex[] modelVerts, out ushort[] modelIndices)
        {
            MeshVertex[] verts;
            ushort[] indices;

            ObjLoader.LoadObject(path, Vector4.Zero, out verts, out indices);

            modelVerts = new TexturedModelVertex[indices.Length];
            modelIndices = new ushort[indices.Length];

            int i = 0;
            for (int j = 0; j < indices.Length; j += 3)
            {
                MeshVertex vert1 = verts[j + 0];
                MeshVertex vert2 = verts[j + 2];
                MeshVertex vert3 = verts[j + 1];

                Vector3 faceCenter = (vert1.Position + vert2.Position + vert3.Position) / 3f;
                Vector3 faceNormal = (vert1.Normal + vert2.Normal + vert3.Normal) / 3f;

                modelIndices[i] = (ushort)i;
                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert1.Position,
                    Normal = faceNormal,
                    Color = vert1.Color,
                    Face = faceCenter,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert1.TextureCoords,
                };

                modelIndices[i] = (ushort)i;
                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert2.Position,
                    Normal = faceNormal,
                    Color = vert2.Color,
                    Face = faceCenter,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert2.TextureCoords,
                };

                modelIndices[i] = (ushort)i;
                modelVerts[i++] = new TexturedModelVertex()
                {
                    Position = vert3.Position,
                    Normal = faceNormal,
                    Color = vert3.Color,
                    Face = faceCenter,
                    SpecularFactor = DefaultSpecFactor,
                    SpecularAmount = DefaultSpecAmount,
                    TextureCoords = vert3.TextureCoords,
                };
            }
        }

		#endregion

		#region Get Model Data

        public static void GetModelData_Flat(string filePath, out ModelVertex[] modelVerts)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Flat(data, out modelVerts); 
        }

        public static void GetModelData_Flat(string filePath, out ModelVertex[] modelVerts, out ExtraData[] extras)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            extras = data.Extras.ToArray(); 

            GetModelData_Flat(data, out modelVerts);
        }

        public static void GetModelData_Flat(ModelData data, out ModelVertex[] modelVerts)
		{
			ShapeInfo shape = data.GetShapeInfo(Matrix4.Identity); 

			modelVerts = shape.ModelVerts;
		}

        public static void GetModelData_Flat(string filePath, out ModelVertex[] modelVerts, out ushort[] indices)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Flat(data, out modelVerts, out indices);
        }

        public static void GetModelData_Flat(ModelData data, out ModelVertex[] modelVerts, out ushort[] indices)
		{	
			ShapeInfo shape = data.GetShapeInfo(Matrix4.Identity); 

			modelVerts = shape.ModelVerts;

			indices = new ushort[modelVerts.Length];

			for (ushort i = 0; i < indices.Length; i++)
			{
				indices[i] = i;
			}			
		}


        public static void GetModelData_Flat(string filePath, out TexturedModelVertex[] modelVerts)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Flat(data, out modelVerts);
        }

        public static void GetModelData_Flat(string filePath, out TexturedModelVertex[] modelVerts, out ExtraData[] extras)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            extras = data.Extras.ToArray(); 

            GetModelData_Flat(data, out modelVerts);
        }

        public static void GetModelData_Flat(ModelData data, out TexturedModelVertex[] modelVerts)
        {
            ShapeInfo shape = data.GetShapeInfo(Matrix4.Identity);

            modelVerts = ToTexturedModelVertices(shape.ModelVerts);
        }

        public static void GetModelData_Flat(string filePath, out TexturedModelVertex[] modelVerts, out ushort[] indices)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Flat(data, out modelVerts, out indices);
        }

        public static void GetModelData_Flat(ModelData data, out TexturedModelVertex[] modelVerts, out ushort[] indices)
        {
            ShapeInfo shape = data.GetShapeInfo(Matrix4.Identity);

            modelVerts = ToTexturedModelVertices(shape.ModelVerts);

            indices = new ushort[modelVerts.Length];

            for (ushort i = 0; i < indices.Length; i++)
            {
                indices[i] = i;
            }
        }



        public static void GetModelData_Smooth(string filePath, out ModelVertex[] modelVerts)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Smooth(data, out modelVerts);
        }

        public static void GetModelData_Smooth(string filePath, out ModelVertex[] modelVerts, out ExtraData[] extras)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            extras = data.Extras.ToArray(); 

            GetModelData_Smooth(data, out modelVerts);
        }

        public static void GetModelData_Smooth(ModelData data, out ModelVertex[] modelVerts)
        {
            ShapeInfo shape = data.GetShapeInfo_Smoothed(Matrix4.Identity);

            modelVerts = shape.ModelVerts;
        }

        public static void GetModelData_Smooth(string filePath, out ModelVertex[] modelVerts, out ushort[] indices)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Smooth(data, out modelVerts, out indices);
        }

        public static void GetModelData_Smooth(ModelData data, out ModelVertex[] modelVerts, out ushort[] indices)
        {
            ShapeInfo shape = data.GetShapeInfo_Smoothed(Matrix4.Identity);

            modelVerts = shape.ModelVerts;

            indices = new ushort[modelVerts.Length];

            for (ushort i = 0; i < indices.Length; i++)
            {
                indices[i] = i;
            }
        }


        public static void GetModelData_Smooth(string filePath, out TexturedModelVertex[] modelVerts)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Smooth(data, out modelVerts);
        }

        public static void GetModelData_Smooth(string filePath, out TexturedModelVertex[] modelVerts, out ExtraData[] extras)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            extras = data.Extras.ToArray(); 

            GetModelData_Smooth(data, out modelVerts);
        }

        public static void GetModelData_Smooth(ModelData data, out TexturedModelVertex[] modelVerts)
        {
            ShapeInfo shape = data.GetShapeInfo_Smoothed(Matrix4.Identity);

            modelVerts = ToTexturedModelVertices(shape.ModelVerts);
        }

        public static void GetModelData_Smooth(string filePath, out TexturedModelVertex[] modelVerts, out ushort[] indices)
        {
            ModelData data = new ModelData();

            data.Load(filePath);

            GetModelData_Smooth(data, out modelVerts, out indices);
        }

        public static void GetModelData_Smooth(ModelData data, out TexturedModelVertex[] modelVerts, out ushort[] indices)
        {
            ShapeInfo shape = data.GetShapeInfo_Smoothed(Matrix4.Identity);

            modelVerts = ToTexturedModelVertices(shape.ModelVerts);

            indices = new ushort[modelVerts.Length];

            for (ushort i = 0; i < indices.Length; i++)
            {
                indices[i] = i;
            }
        }



        private static TexturedModelVertex[] ToTexturedModelVertices(ModelVertex[] modelVertex)
        {
            TexturedModelVertex[] result = new TexturedModelVertex[modelVertex.Length];

            for (int i = 0; i < modelVertex.Length; i++)
            {
                ModelVertex vert = modelVertex[i]; 

                result[i] = new TexturedModelVertex()
                {
                    Position = vert.Position,
                    Normal = vert.Normal,
                    Color = vert.Color,
                    Face = vert.Face,
                    SpecularFactor = vert.SpecularFactor,
                    SpecularAmount = vert.SpecularAmount,
                    //TextureCoords = vert.Position.Xz * 0.5f + new Vector2(0.5f),
                    TextureCoords = new Vector2(vert.Position.X * 0.5f + 0.5f, -vert.Position.Z * 0.5f + 0.5f),
                };
            }

            return result; 
        }

		#endregion
	}
}
