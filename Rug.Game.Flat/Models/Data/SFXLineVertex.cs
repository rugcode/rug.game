﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Rug.Game.Flat.Models.Data
{
    [StructLayout(LayoutKind.Sequential)]
	public struct SFXLineVertex
	{		
		public Vector4 Position;
		public Color4 Color;
        public Vector3 Normal;

		public enum Elements : int { Position = 0, Color = 1, Normal = 2 };
		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int ColorOffset;
        public static readonly int NormalOffset;

        static SFXLineVertex()
		{
            Stride = BlittableValueType<SFXLineVertex>.Stride;

            PositionOffset = (int)Marshal.OffsetOf(typeof(SFXLineVertex), "Position");
            ColorOffset = (int)Marshal.OffsetOf(typeof(SFXLineVertex), "Color");
            NormalOffset = (int)Marshal.OffsetOf(typeof(SFXLineVertex), "Normal");

            if (NormalOffset + BlittableValueType<Vector3>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total"); 
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
            GL.VertexAttribPointer((int)Elements.Normal, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.Color);
            GL.EnableVertexAttribArray((int)Elements.Normal);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.Color);
            GL.DisableVertexAttribArray((int)Elements.Normal);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
                get { return SFXLineVertex.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
                GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);

				baseLocation += 3;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
                GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);				
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
                GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
                GL.Arb.VertexAttribDivisor((int)Elements.Normal + baseLocation, devisor);

				baseLocation += 3;
			}

			#endregion
		}

		#endregion
	}
}
