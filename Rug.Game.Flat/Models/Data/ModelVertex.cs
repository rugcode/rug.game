﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using System;
using System.Runtime.InteropServices;

namespace Rug.Game.Flat.Models 
{
	[StructLayout(LayoutKind.Explicit)]
	public struct ModelVertex
	{
		[FieldOffset(0)]
		public Vector3 Position;

		[FieldOffset(12)]
		public Vector3 Normal;

		[FieldOffset(24)]
		public Color3 Color;

		/// <summary>
		/// Specular factor, gets bundled in with Color when its posted to the GPU.
		/// </summary>
		[FieldOffset(36)]
		public float SpecularFactor;

		[FieldOffset(40)]
		public Vector3 Face;

		/// <summary>
		/// Specular Amount, gets bundled in with Face when its posted to the GPU.
		/// </summary>
		[FieldOffset(52)]
		public float SpecularAmount; 

		public enum Elements : int { Position = 0, Normal = 1, Color = 2, Face = 3 };

		public static readonly int Stride;
		public static readonly int PositionOffset;
		public static readonly int NormalOffset;
		public static readonly int ColorOffset;
		public static readonly int FaceOffset;

		static ModelVertex()
		{
			Stride = BlittableValueType<ModelVertex>.Stride;

			PositionOffset = (int)Marshal.OffsetOf(typeof(ModelVertex), "Position");
			NormalOffset = (int)Marshal.OffsetOf(typeof(ModelVertex), "Normal");
			ColorOffset = (int)Marshal.OffsetOf(typeof(ModelVertex), "Color");
			FaceOffset = (int)Marshal.OffsetOf(typeof(ModelVertex), "Face");

			if (FaceOffset + BlittableValueType<Vector4>.Stride != Stride)
			{
				throw new Exception("Stride does not match offset total");
			}
		}

		public static void Bind()
		{
			GL.VertexAttribPointer((int)Elements.Position, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
			GL.VertexAttribPointer((int)Elements.Normal, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
			GL.VertexAttribPointer((int)Elements.Face, 4, VertexAttribPointerType.Float, false, Stride, FaceOffset);

			GL.EnableVertexAttribArray((int)Elements.Position);
			GL.EnableVertexAttribArray((int)Elements.Normal);
			GL.EnableVertexAttribArray((int)Elements.Color);
			GL.EnableVertexAttribArray((int)Elements.Face);
		}

		public static void Unbind()
		{
			GL.DisableVertexAttribArray((int)Elements.Position);
			GL.DisableVertexAttribArray((int)Elements.Normal);
			GL.DisableVertexAttribArray((int)Elements.Color);
			GL.DisableVertexAttribArray((int)Elements.Face);
		}


		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return ModelVertex.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.Face + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, FaceOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Face + baseLocation);

				baseLocation += 4;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Position + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, PositionOffset);
				GL.VertexAttribPointer((int)Elements.Normal + baseLocation, 3, VertexAttribPointerType.Float, false, Stride, NormalOffset);
				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				GL.VertexAttribPointer((int)Elements.Face + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, FaceOffset);

				GL.EnableVertexAttribArray((int)Elements.Position + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Face + baseLocation);

				GL.VertexAttribDivisor((int)Elements.Position + baseLocation, devisor);
				GL.VertexAttribDivisor((int)Elements.Normal + baseLocation, devisor);
				GL.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);
				GL.VertexAttribDivisor((int)Elements.Face + baseLocation, devisor);

				baseLocation += 4;
			}

			#endregion
		}

		#endregion
	}
}