﻿using OpenTK;
using OpenTK.Graphics;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game.Flat.Models 
{
	public class IconLoader
	{
		public static ModelVertex[] Load_Verticle(string path, float depth, float scale)
		{
			List<ModelVertex> verts = new List<ModelVertex>(); 

			Vector3[] v = new Vector3[]
			{
				new Vector3(0f, 0f,  depth),	// A = 0
				new Vector3(1f, 0f,  depth),	// B = 1
				new Vector3(1f, 1f,  depth),	// C = 2
				new Vector3(0f, 1f,  depth),	// D = 3

				new Vector3(0f, 0f, 0f),	// E = 4
				new Vector3(1f, 0f, 0f),	// F = 5
				new Vector3(1f, 1f, 0f),	// G = 6
				new Vector3(0f, 1f, 0f),	// H = 7
			};

			Vector3[] normals = new Vector3[] 
			{
				new Vector3(1.0f,  0.0f,  0.0f),	// +r face 0 
				new Vector3(-1.0f,  0.0f,  0.0f),	// -r face 1
				new Vector3(0.0f,  1.0f,  0.0f),	// +g face 2
				new Vector3(0.0f, -1.0f,  0.0f),	// -g face 3 
				new Vector3(0.0f,  0.0f,  1.0f),	// +b face 4
				new Vector3(0.0f,  0.0f, -1.0f)		// -b face 5
			};

			Color4 color = new Color4(1f, 1f, 1f, 1f); 

			using (Bitmap bitmap = FileHelper.LoadBitmap(path))
			{
				bool[,] values = new bool[bitmap.Width, bitmap.Height];

				//Vector3 offset = new Vector3((float)bitmap.Width * -0.5f, (float)bitmap.Height * -0.5f, depth * -0.5f);
				Vector3 offset = new Vector3((float)bitmap.Width * -0.5f, (float)bitmap.Height * -0.5f, depth * -0.5f); 

				Color black = Color.FromArgb(255, 0, 0, 0);

				for (int y = 0; y < bitmap.Height; y++)
				{
					for (int x = 0; x < bitmap.Width; x++)
					{
						values[x, y] = bitmap.GetPixel(x, (bitmap.Height - 1) - y) == black; 
					}
				}
				
				for (int y = 0; y < bitmap.Height; y++)
				{
					bool lastX = false; 

					for (int x = 0; x < bitmap.Width; x++)
					{
						Vector3 off = new Vector3((float)x, (float)y, 0) + offset;

						bool value = values[x, y];
						
						bool nextX = x < (bitmap.Width - 1) ? values[x + 1, y] : false;

						bool lastY = y > 0 ? values[x, y - 1] : false;
						bool nextY = y < (bitmap.Height - 1) ? values[x, y + 1] : false;

						if (value == true)
						{
							verts.AddRange(UnitCubeAddFace(off, scale, v[0], v[1], v[2], v[3], normals[4], color)); // +b face
							verts.AddRange(UnitCubeAddFace(off, scale, v[5], v[4], v[7], v[6], normals[5], color)); // -b face

							if (nextX == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[1], v[5], v[6], v[2], normals[0], color)); // +r face
							}

							if (lastX == false) 
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[4], v[0], v[3], v[7], normals[1], color)); // -r face
							}

							if (nextY == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[3], v[2], v[6], v[7], normals[2], color)); // +g face
							}

							if (lastY == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[4], v[5], v[1], v[0], normals[3], color)); // -g face
							}
						}

						lastX = value;
					}
				}
			}

			return verts.ToArray();
		}

		public static ModelVertex[] Load_Horizontal(string path, float depth, float scale)
		{
			List<ModelVertex> verts = new List<ModelVertex>();

			Vector3[] v = new Vector3[]
			{
				new Vector3(0f, 0f,  1f),	// A = 0
				new Vector3(1f, 0f,  1f),	// B = 1
				new Vector3(1f, depth,  1f),	// C = 2
				new Vector3(0f, depth,  1f),	// D = 3

				new Vector3(0f, 0f, 0f),	// E = 4
				new Vector3(1f, 0f, 0f),	// F = 5
				new Vector3(1f, depth, 0f),	// G = 6
				new Vector3(0f, depth, 0f),	// H = 7
			};

			Vector3[] normals = new Vector3[] 
			{
				new Vector3(1.0f,  0.0f,  0.0f),	// +r face 0 
				new Vector3(-1.0f,  0.0f,  0.0f),	// -r face 1
				new Vector3(0.0f,  1.0f,  0.0f),	// +g face 2
				new Vector3(0.0f, -1.0f,  0.0f),	// -g face 3 
				new Vector3(0.0f,  0.0f,  1.0f),	// +b face 4
				new Vector3(0.0f,  0.0f, -1.0f)		// -b face 5
			};

			Color4 color = new Color4(1f, 1f, 1f, 1f);

            using (Bitmap bitmap = FileHelper.LoadBitmap(path))
			{
				bool[,] values = new bool[bitmap.Width, bitmap.Height];

				//Vector3 offset = new Vector3((float)bitmap.Width * -0.5f, (float)bitmap.Height * -0.5f, depth * -0.5f);
				Vector3 offset = new Vector3((float)bitmap.Width * -0.5f, depth * -0.5f, (float)bitmap.Height * -0.5f);

				Color black = Color.FromArgb(255, 0, 0, 0);

				for (int y = 0; y < bitmap.Height; y++)
				{
					for (int x = 0; x < bitmap.Width; x++)
					{
						values[x, y] = bitmap.GetPixel(x, y) == black;
					}
				}

				for (int y = 0; y < bitmap.Height; y++)
				{
					bool lastX = false;

					for (int x = 0; x < bitmap.Width; x++)
					{
						Vector3 off = new Vector3((float)x, 0, (float)y) + offset;

						bool value = values[x, y];

						bool nextX = x < (bitmap.Width - 1) ? values[x + 1, y] : false;

						bool lastY = y > 0 ? values[x, y - 1] : false;
						bool nextY = y < (bitmap.Height - 1) ? values[x, y + 1] : false;

						if (value == true)
						{
							if (nextY == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[0], v[1], v[2], v[3], normals[4], color)); // +b face									
							}

							if (lastY == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[5], v[4], v[7], v[6], normals[5], color)); // -b face								
							}

							if (nextX == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[1], v[5], v[6], v[2], normals[0], color)); // +r face
							}

							if (lastX == false)
							{
								verts.AddRange(UnitCubeAddFace(off, scale, v[4], v[0], v[3], v[7], normals[1], color)); // -r face
							}

							verts.AddRange(UnitCubeAddFace(off, scale, v[3], v[2], v[6], v[7], normals[2], color)); // +g face
							verts.AddRange(UnitCubeAddFace(off, scale, v[4], v[5], v[1], v[0], normals[3], color)); // -g face
						}

						lastX = value;
					}
				}
			}

			return verts.ToArray();
		}

		static ModelVertex[] UnitCubeAddFace(Vector3 offset, float scale, Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 n, Color4 color)
		{
			ModelVertex[] verts = new ModelVertex[6];

			Vector3 center =
				(v1 + offset) +
				(v2 + offset) +
				(v3 + offset) +
				(v4 + offset);

			center *= 0.25f;

			Vector4 face = new Vector4(center, 1f);

			verts[0].Position = (v1 + offset) * scale;
			verts[0].Normal = n;
			verts[0].Color = color;
			verts[0].Face = face.Xyz;
            verts[0].SpecularAmount = ModelHelper.DefaultSpecAmount;
            verts[0].SpecularFactor = ModelHelper.DefaultSpecFactor;

			verts[1].Position = (v2 + offset) * scale;
			verts[1].Normal = n;
			verts[1].Color = color;
            verts[1].Face = face.Xyz;
            verts[1].SpecularAmount = ModelHelper.DefaultSpecAmount;
            verts[1].SpecularFactor = ModelHelper.DefaultSpecFactor;

			verts[2].Position = (v3 + offset) * scale;
			verts[2].Normal = n;
			verts[2].Color = color;
            verts[2].Face = face.Xyz;
            verts[2].SpecularAmount = ModelHelper.DefaultSpecAmount;
            verts[2].SpecularFactor = ModelHelper.DefaultSpecFactor;

			verts[3].Position = (v1 + offset) * scale;
			verts[3].Normal = n;
			verts[3].Color = color;
            verts[3].Face = face.Xyz;
            verts[3].SpecularAmount = ModelHelper.DefaultSpecAmount;
            verts[3].SpecularFactor = ModelHelper.DefaultSpecFactor;

			verts[4].Position = (v3 + offset) * scale;
			verts[4].Normal = n;
			verts[4].Color = color;
            verts[4].Face = face.Xyz;
            verts[4].SpecularAmount = ModelHelper.DefaultSpecAmount;
            verts[4].SpecularFactor = ModelHelper.DefaultSpecFactor;

			verts[5].Position = (v4 + offset) * scale;
			verts[5].Normal = n;
			verts[5].Color = color;
            verts[5].Face = face.Xyz;
            verts[5].SpecularAmount = ModelHelper.DefaultSpecAmount;
            verts[5].SpecularFactor = ModelHelper.DefaultSpecFactor;

			return verts;
		}
	}
}
