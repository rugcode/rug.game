﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using System.Runtime.InteropServices;

namespace Rug.Game.Flat.Models 
{
	[StructLayout(LayoutKind.Sequential)]
	public struct ModelInstance
	{		
		public Matrix4 Object;
		public Matrix4 Normal;
		
		public Color4 Color;		

		public enum Elements : int 
		{ 
			Object_Row0 = 0, Object_Row1 = 1, Object_Row2 = 2, Object_Row3 = 3, 
			Normal_Row0 = 4, Normal_Row1 = 5, Normal_Row2 = 6, Normal_Row3 = 7,

			Color = 8,
		};

		public static readonly int Stride;

		public static readonly int Object_Row0Offset;
		public static readonly int Object_Row1Offset;
		public static readonly int Object_Row2Offset;
		public static readonly int Object_Row3Offset;

		public static readonly int Normal_Row0Offset;
		public static readonly int Normal_Row1Offset;
		public static readonly int Normal_Row2Offset;
		public static readonly int Normal_Row3Offset;

		public static readonly int ColorOffset;

		static ModelInstance()
		{
			Stride = BlittableValueType<ModelInstance>.Stride;

			int ObjectBase = (int)Marshal.OffsetOf(typeof(ModelInstance), "Object"); 

			Object_Row0Offset = ObjectBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row0");
			Object_Row1Offset = ObjectBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row1");
			Object_Row2Offset = ObjectBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row2");
			Object_Row3Offset = ObjectBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row3");

			int NormalBase = (int)Marshal.OffsetOf(typeof(ModelInstance), "Normal");

			Normal_Row0Offset = NormalBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row0");
			Normal_Row1Offset = NormalBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row1");
			Normal_Row2Offset = NormalBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row2");
			Normal_Row3Offset = NormalBase + (int)Marshal.OffsetOf(typeof(Matrix4), "Row3");

			ColorOffset = (int)Marshal.OffsetOf(typeof(ModelInstance), "Color");
		}

		public static void Bind()
		{			
			GL.VertexAttribPointer((int)Elements.Object_Row0, 4, VertexAttribPointerType.Float, false, Stride, Object_Row0Offset);
			GL.VertexAttribPointer((int)Elements.Object_Row1, 4, VertexAttribPointerType.Float, false, Stride, Object_Row1Offset);
			GL.VertexAttribPointer((int)Elements.Object_Row2, 4, VertexAttribPointerType.Float, false, Stride, Object_Row2Offset);
			GL.VertexAttribPointer((int)Elements.Object_Row3, 4, VertexAttribPointerType.Float, false, Stride, Object_Row3Offset);

			GL.VertexAttribPointer((int)Elements.Normal_Row0, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row0Offset);
			GL.VertexAttribPointer((int)Elements.Normal_Row1, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row1Offset);
			GL.VertexAttribPointer((int)Elements.Normal_Row2, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row2Offset);
			GL.VertexAttribPointer((int)Elements.Normal_Row3, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row3Offset);

			GL.VertexAttribPointer((int)Elements.Color, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

			GL.EnableVertexAttribArray((int)Elements.Object_Row0);
			GL.EnableVertexAttribArray((int)Elements.Object_Row1);
			GL.EnableVertexAttribArray((int)Elements.Object_Row2);
			GL.EnableVertexAttribArray((int)Elements.Object_Row3);

			GL.EnableVertexAttribArray((int)Elements.Normal_Row0);
			GL.EnableVertexAttribArray((int)Elements.Normal_Row1);
			GL.EnableVertexAttribArray((int)Elements.Normal_Row2);
			GL.EnableVertexAttribArray((int)Elements.Normal_Row3);

			GL.EnableVertexAttribArray((int)Elements.Color);	
		}

		public static void Unbind()
		{		
			GL.DisableVertexAttribArray((int)Elements.Object_Row0);
			GL.DisableVertexAttribArray((int)Elements.Object_Row1);
			GL.DisableVertexAttribArray((int)Elements.Object_Row2);
			GL.DisableVertexAttribArray((int)Elements.Object_Row3);

			GL.DisableVertexAttribArray((int)Elements.Normal_Row0);
			GL.DisableVertexAttribArray((int)Elements.Normal_Row1);
			GL.DisableVertexAttribArray((int)Elements.Normal_Row2);
			GL.DisableVertexAttribArray((int)Elements.Normal_Row3);

			GL.DisableVertexAttribArray((int)Elements.Color);
		}

		public readonly static IVertexFormat Format = new FormatInfo();

		#region Format Class

		private class FormatInfo : IVertexFormat
		{
			#region IVertexFormat Members

			public int Stride
			{
				get { return ModelInstance.Stride; }
			}

			public void CreateLayout(ref int baseLocation)
			{				
				GL.VertexAttribPointer((int)Elements.Object_Row0 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row0Offset);
				GL.VertexAttribPointer((int)Elements.Object_Row1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row1Offset);
				GL.VertexAttribPointer((int)Elements.Object_Row2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row2Offset);
				GL.VertexAttribPointer((int)Elements.Object_Row3 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row3Offset);

				GL.VertexAttribPointer((int)Elements.Normal_Row0 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row0Offset);
				GL.VertexAttribPointer((int)Elements.Normal_Row1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row1Offset);
				GL.VertexAttribPointer((int)Elements.Normal_Row2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row2Offset);
				GL.VertexAttribPointer((int)Elements.Normal_Row3 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row3Offset);

				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);
				
				GL.EnableVertexAttribArray((int)Elements.Object_Row0 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Object_Row1 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Object_Row2 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Object_Row3 + baseLocation);

				GL.EnableVertexAttribArray((int)Elements.Normal_Row0 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal_Row1 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal_Row2 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal_Row3 + baseLocation);

				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				baseLocation += 9;
			}

			public void CreateLayout(ref int baseLocation, int devisor)
			{
				GL.VertexAttribPointer((int)Elements.Object_Row0 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row0Offset);
				GL.VertexAttribPointer((int)Elements.Object_Row1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row1Offset);
				GL.VertexAttribPointer((int)Elements.Object_Row2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row2Offset);
				GL.VertexAttribPointer((int)Elements.Object_Row3 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Object_Row3Offset);

				GL.VertexAttribPointer((int)Elements.Normal_Row0 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row0Offset);
				GL.VertexAttribPointer((int)Elements.Normal_Row1 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row1Offset);
				GL.VertexAttribPointer((int)Elements.Normal_Row2 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row2Offset);
				GL.VertexAttribPointer((int)Elements.Normal_Row3 + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, Normal_Row3Offset);

				GL.VertexAttribPointer((int)Elements.Color + baseLocation, 4, VertexAttribPointerType.Float, false, Stride, ColorOffset);

				GL.EnableVertexAttribArray((int)Elements.Object_Row0 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Object_Row1 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Object_Row2 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Object_Row3 + baseLocation);

				GL.EnableVertexAttribArray((int)Elements.Normal_Row0 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal_Row1 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal_Row2 + baseLocation);
				GL.EnableVertexAttribArray((int)Elements.Normal_Row3 + baseLocation);

				GL.EnableVertexAttribArray((int)Elements.Color + baseLocation);

				GL.Arb.VertexAttribDivisor((int)Elements.Object_Row0 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Object_Row1 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Object_Row2 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Object_Row3 + baseLocation, devisor);

				GL.Arb.VertexAttribDivisor((int)Elements.Normal_Row0 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Normal_Row1 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Normal_Row2 + baseLocation, devisor);
				GL.Arb.VertexAttribDivisor((int)Elements.Normal_Row3 + baseLocation, devisor);

				GL.Arb.VertexAttribDivisor((int)Elements.Color + baseLocation, devisor);

				baseLocation += 9;
			}

			#endregion
		}

		#endregion
	}
}
