﻿using OpenTK;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;
using System;

namespace Rug.Game.Flat.Models
{
	public class ExtrudedModelBatch : IResourceManager
    { 		
		private VertexArrayObject m_VAO;

        public int DesiredMaxCount { get; set; } 

		public int MaxCount { get; set; } 

		public int InstanceCount { get; set; }

		public InstanceBuffer InstanceBuffer { get; private set; }

		public ExtrudedModelInstance[] Instances { get; private set; }

        public VertexBuffer ModelBuffer { get; private set; }

        public ModelBase Model { get; private set; }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public ExtrudedModelBatch(ModelBase model, int maxCount)
		{
            Disposed = true;

			MaxCount = maxCount;

            Model = model; 

            Instances = new ExtrudedModelInstance[MaxCount];

            ModelBuffer = new VertexBuffer(Model.Name + " Extruded Verts", ResourceMode.Static, new VertexBufferInfo(Model.ModelBuffer.ResourceInfo.Format, Model.ModelBuffer.ResourceInfo.Count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

            InstanceBuffer = new InstanceBuffer(Model.Name + " Extruded Instances", ResourceMode.Dynamic, new InstanceBufferInfo(ExtrudedModelInstance.Format, MaxCount, OpenTK.Graphics.OpenGL.BufferUsageHint.DynamicDraw));

            m_VAO = new VertexArrayObject(Model.Name + " Extruded VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer, InstanceBuffer });
        }

        public void CheckResize()
        {
            if (DesiredMaxCount > MaxCount)
            {
                int value = DesiredMaxCount;
                float factor = 128.0f;

                MaxCount = (int)(Math.Max(1, Math.Round(((float)value / (float)factor) + 0.5f, MidpointRounding.AwayFromZero)) * factor);

                InstanceBuffer.ResourceInfo.Count = MaxCount;

                if (Disposed == false)
                {
                    UnloadResources();

                    LoadResources();
                }
            }

            if (MaxCount != Instances.Length)
            {
                Instances = new ExtrudedModelInstance[MaxCount];

                InstanceCount = 0;
            }
        }

		public virtual void Update()
		{
			if (InstanceCount == 0)
			{
				return;
			}

            if (Disposed == true)
            {
                return; 
            }

			Rug.Game.Core.Buffers.DataStream stream;

			InstanceBuffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(Instances, 0, InstanceCount);

			InstanceBuffer.UnmapBuffer();	
		}

        public void Render(View3D view, Texture2D modelTexture, ModelLighting lighting, ModelLightingMode lightingMode)
        {
            if (InstanceCount == 0)
            {
                return;
            }

            if (Disposed == true)
            {
                return;
            }

            Matrix4 objectMatrix = Matrix4.Identity;

            bool isTextured = Model.IsTextured;

            int textureIndex = isTextured ? 1 : 0;

            ModelEffect effect = null;

            if ((lighting.LightingModeFlags[textureIndex] & lightingMode) != lightingMode)
            {
                return;
            }

            effect = lighting.SelectEffect(isTextured, ModelEffectMode.Extruded, lightingMode);

            if (effect != null)
            {
                effect.Render_Extruded(
                        ref objectMatrix, modelTexture,
                        lighting.LightingArguments, new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), // view.Camera.Up * -1.0f, view.Camera.Forward * -1.0f,
                        m_VAO, ModelBuffer.ResourceInfo.Count, InstanceCount);
            }
        }

        public void Render_Identity(View3D view, Texture2D modelTexture, ModelLighting lighting, ModelLightingMode lightingMode)
        {
            if (InstanceCount == 0)
            {
                return;
            }

            if (Disposed == true)
            {
                return;
            }

            Matrix4 objectMatrix = Matrix4.Identity;
            Matrix4 pixelScale = Matrix4.CreateScale((float)view.Viewport.Height / (float)view.Viewport.Width, 1f, 1f);

            bool isTextured = Model.IsTextured;

            int textureIndex = isTextured ? 1 : 0;

            ModelEffect effect = null;

            if ((lighting.LightingModeFlags[textureIndex] & lightingMode) != lightingMode)
            {
                return;
            }

            effect = lighting.SelectEffect(isTextured, ModelEffectMode.Extruded, lightingMode);

            if (effect != null)
            {
                effect.Render_Extruded_Identity(
                        ref objectMatrix, ref pixelScale, modelTexture,
                        lighting.LightingArguments, new Vector3(0.0f, 1.0f, 0.0f), new Vector3(1.0f, 0.0f, 0.0f), // view.Camera.Up * -1.0f, view.Camera.Forward * -1.0f,
                        m_VAO, ModelBuffer.ResourceInfo.Count, InstanceCount);
            }
        }

		#region IResourceManager Members

		public bool Disposed { get; private set; }

		public void LoadResources()
		{
			if (Disposed == true)
			{			
				m_VAO.LoadResources();

				//BuildBillboardVerts();
                Model.BuildModelVerts(ModelBuffer); 

				CheckResize();

				Disposed = false; 
			}
		}

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop(); 
        }

		public void UnloadResources()
		{
			if (Disposed == false)
			{
				m_VAO.UnloadResources();

				Disposed = true;
			}
		}

        /* 
		private void BuildBillboardVerts()
		{
			Color3 color = (Color3)Color4.White;

			#region Billboard

			DataStream stream;
			ModelBuffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

            ModelVertex[] vertex = Cube(1f, 1f, 1f, color);

			stream.WriteRange(vertex, 0, vertex.Length); 

			ModelBuffer.UnmapBuffer();

			#endregion
		}
        */ 

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
    }  
}
