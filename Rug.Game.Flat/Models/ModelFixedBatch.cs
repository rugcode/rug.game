﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;
using System;

namespace Rug.Game.Flat.Models 
{
	public class ModelFixedBatch : IResourceManager
	{
		private VertexArrayObject m_VAO;

		public bool Disposed { get; private set; }

        public int DesiredMaxCount;

        public int MaxCount;

        public int InstanceCount;

		public InstanceBuffer InstanceBuffer { get; private set; } 

		public ModelFixedInstance[] Instances { get; private set; }

		public VertexBuffer ModelBuffer { get; private set; }

		public ModelBase Model { get; private set; }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

        public ModelFixedBatch(ModelBase model, int maxCount, BufferUsageHint usage)
		{
			Disposed = true;

			Model = model;

			MaxCount = Math.Max(128, maxCount);
			
			DesiredMaxCount = 0; 

			Instances = new ModelFixedInstance[MaxCount];

            InstanceBuffer = new InstanceBuffer(Model.Name + " Fixed Batch Instances", ResourceMode.Dynamic, new InstanceBufferInfo(ModelFixedInstance.Format, MaxCount, usage));

            ModelBuffer = new VertexBuffer(Model.Name + " Fixed Batch Verts", ResourceMode.Static, new VertexBufferInfo(Model.ModelBuffer.ResourceInfo.Format, Model.ModelBuffer.ResourceInfo.Count, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

			m_VAO = new VertexArrayObject(Model.Name + " Fixed Batch VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer, InstanceBuffer });			
		}

		public void CheckResize()
		{
			if (DesiredMaxCount > MaxCount)
			{
				int value = DesiredMaxCount;
				float factor = 128.0f;

                MaxCount = (int)(Math.Max(1, Math.Round(((float)value / (float)factor) + 0.5f, MidpointRounding.AwayFromZero)) * factor);

				InstanceBuffer.ResourceInfo.Count = MaxCount;

				if (Disposed == false)
				{
					UnloadResources();

					LoadResources();
				}
			}

			if (MaxCount != Instances.Length)
			{
				Instances = new ModelFixedInstance[MaxCount];

				InstanceCount = 0; 
			}
		}

		public virtual void Update()
		{
			if (Disposed == true)
			{
				return;
			}

			if (InstanceCount == 0)
			{
				return;
			}

			Rug.Game.Core.Buffers.DataStream stream;

			InstanceBuffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(Instances, 0, InstanceCount);

			InstanceBuffer.UnmapBuffer();				
		}

		public void Render(View3D view, Matrix4 objectMatrix, Texture2D modelTexture,
            ModelLighting lighting, ModelLightingMode lightingMode)
		{
			if (InstanceCount == 0)
			{
				return;
			}

            ModelEffect effect = null;

            int textureIndex = Model.IsTextured ? 1 : 0;

            if ((lighting.LightingModeFlags[textureIndex] & lightingMode) != lightingMode)
            {
                return;
            }

            effect = lighting.SelectEffect(Model.IsTextured, ModelEffectMode.FixedBatch, lightingMode);

            if (effect != null) 
            {
                effect.Render_Batched(ref objectMatrix, modelTexture,
                        lighting.LightingArguments, 
                        m_VAO, ModelBuffer.ResourceInfo.Count, InstanceCount);
            }

			//StaticObjects.DrawCallsAccumulator++;
		}

		#region Resource Management

		public void LoadResources()
		{
			if (Disposed == true)
			{
				CheckResize();

				m_VAO.LoadResources();

				Model.BuildModelVerts(ModelBuffer); 

				Disposed = false; 
			}
		}

		public void UnloadResources()
		{
			if (Disposed == false)
			{
				m_VAO.UnloadResources();

				Disposed = true;
			}
		}

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
