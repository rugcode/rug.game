﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;
using System.Collections.Generic;
using Rug.Game.Core.Maths;

namespace Rug.Game.Flat.Models
{
    public abstract class ModelBase : IResourceManager
    {
        public string Name { get; protected set; }

        public VertexBuffer ModelBuffer { get; protected set; }

        public bool IsShadowCaster;

        public bool IsReflective;

        public bool IsTextured { get; protected set; }

        public Texture2D Texture;

        public RefractionParams RefractionParams;

        public FogParams FogParams;

        public bool OverrideFog;

        public BoundingBox Bounds;

        public List<ExtraData> Extras { get; protected set; }

        public abstract Vector3[] GetVertices();

        public abstract void BuildModelVerts(VertexBuffer buffer);

        public abstract void Update();

        public abstract void Render(View3D view, Matrix4 objectMatrix, Matrix4 normalMatrix, Color4 color, Texture2D modelTexture, ModelLighting lighting, ModelLightingMode lightingMode);

        public bool Disposed { get; protected set; }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public abstract void LoadNext(ResourceManagerLoadProcess process);

        public abstract void LoadResources();

        public abstract void UnloadResources();

        public abstract void Dispose();

        public ExtraData GetExtra(string key)
        {
            foreach (ExtraData data in Extras)
            {
                if (data.Key == key)
                {
                    return data;
                }
            }

            return default(ExtraData); 
        }
    }
}
