﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;
using System;
using System.Collections.Generic;
using Rug.Game.Core.Maths;

namespace Rug.Game.Flat.Models 
{
    public class Model : ModelBase
	{
        private ModelVertex[] m_ModelVerts;

        public VertexArrayObject VAO { get; private set; }

        public ModelVertex[] ModelVerts { get { return m_ModelVerts; } }

        public Model(string name, ModelData data, bool flat = true)
        {
            Disposed = true; 

            Name = name;
            
            IsShadowCaster = true; 

            ShapeInfo shape;

            if (flat == true)
            {
                shape = data.GetShapeInfo(Matrix4.Identity);
            }
            else
            {
                shape = data.GetShapeInfo_Smoothed(Matrix4.Identity);
            }

            Bounds = data.Bounds; 

            Extras = new List<ExtraData>(data.Extras);

            m_ModelVerts = shape.ModelVerts;

            ModelBuffer = new VertexBuffer(Name + " Verts", ResourceMode.Static, new VertexBufferInfo(ModelVertex.Format, m_ModelVerts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

            VAO = new VertexArrayObject(Name + " VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer });	
        }

		public Model(string name, string filePath, bool flat)
		{
            Disposed = true; 

            Extras = new List<ExtraData>();

			if (FileHelper.Exists(filePath) == false) 
			{
				throw new Exception(string.Format("Model file '{0}' does not exist.", filePath)); 
			}

            if (filePath.ToLowerInvariant().EndsWith(".obj") == true)
			{
				if (flat == true) 
				{
					ModelHelper.MeshToModel_Flat(filePath, out m_ModelVerts);
				}
				else 
				{
					ModelHelper.MeshToModel_Smooth(filePath, out m_ModelVerts);
				}
			}
			else
			{
                ExtraData[] extras = null; 

                if (flat == true)
                {
                    ModelHelper.GetModelData_Flat(filePath, out m_ModelVerts, out extras);
                }
                else
                {
                    ModelHelper.GetModelData_Smooth(filePath, out m_ModelVerts, out extras);
                }

                Extras.AddRange(extras); 
			}

			Name = name;

            IsShadowCaster = true;

            Bounds = CalculateBounds(); 

			ModelBuffer = new VertexBuffer(Name + " Verts", ResourceMode.Static, new VertexBufferInfo(ModelVertex.Format, m_ModelVerts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
			
			VAO = new VertexArrayObject(Name + " VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer });			
		}

        private BoundingBox CalculateBounds()
        {
            Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            foreach (ModelVertex vert in m_ModelVerts)
            {
                min = Vector3.ComponentMin(min, vert.Position);
                max = Vector3.ComponentMax(max, vert.Position);
            }

            return new BoundingBox(min, max);
        }

        public override Vector3[] GetVertices()
        {
            Vector3[] verts = new Vector3[m_ModelVerts.Length];

            for (int i = 0; i < m_ModelVerts.Length; i++)
            {
                verts[i] = m_ModelVerts[i].Position; 
            }

            return verts; 
        }

        public override void Update()
		{
			
		}

        public override void Render(View3D view, 
			Matrix4 objectMatrix, Matrix4 normalMatrix, Color4 color, Texture2D modelTexture,
			ModelLighting lighting, ModelLightingMode lightingMode)
		{
            ModelEffect effect = null;

            int textureIndex = IsTextured ? 1 : 0;

            if ((lighting.LightingModeFlags[textureIndex] & lightingMode) != lightingMode)
            {
                return;
            }

            effect = lighting.SelectEffect(IsTextured, ModelEffectMode.NonBatch, lightingMode);

            if (effect != null) 
            {
                effect.Render_Single(
                        ref objectMatrix, ref normalMatrix, ref color, modelTexture,
                        lighting.LightingArguments, 
                        VAO, ModelBuffer.ResourceInfo.Count);
            }

			//StaticObjects.DrawCallsAccumulator++;
		}

		#region IResourceManager Members

        public override void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

        public override void LoadResources()
		{
			if (Disposed == false)
			{
                return; 
            }

            if (m_ModelVerts.Length != 0)
            {
                VAO.LoadResources();

                BuildModelVerts(ModelBuffer);
            }

			Disposed = false; 
		}

        public override void UnloadResources()
		{
            if (Disposed == true)
            {
                return;
            }

            if (m_ModelVerts.Length != 0)
            {
                VAO.UnloadResources();
            }

			Disposed = true;
		}

        public override void BuildModelVerts(VertexBuffer buffer)
		{
            if (m_ModelVerts.Length != 0)
            {
                DataStream stream;

                buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);
                stream.WriteRange(m_ModelVerts, 0, m_ModelVerts.Length);
                buffer.UnmapBuffer();
            }
		}

        public override void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
