﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;
using System;
using System.Collections.Generic;
using System.IO;

namespace Rug.Game.Flat.Models 
{
    public class TexturedModel : ModelBase
	{
        private TexturedModelVertex[] m_ModelVerts;
		private VertexArrayObject m_VAO;

        public TexturedModelVertex[] ModelVerts { get { return m_ModelVerts; } }

        public TexturedModel(string name, ModelData data, bool flat = true)
        {
            Disposed = true; 

            IsTextured = true; 

            Name = name;
            
            IsShadowCaster = true;

            if (flat == true)
            {
                ModelHelper.GetModelData_Flat(data, out m_ModelVerts);
            }
            else
            {
                ModelHelper.GetModelData_Smooth(data, out m_ModelVerts);
            }

            Extras = new List<ExtraData>(data.Extras);
            
            ModelBuffer = new VertexBuffer(Name + " Verts", ResourceMode.Static, new VertexBufferInfo(TexturedModelVertex.Format, m_ModelVerts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

            m_VAO = new VertexArrayObject(Name + " VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer });	
        }

        public TexturedModel(string name, string filePath, bool flat)
		{
            Disposed = true; 

            IsTextured = true; 

            Extras = new List<ExtraData>();

			FileInfo fileInfo = new FileInfo(Helper.ResolvePath(filePath)); 

			if (fileInfo.Exists == false) 
			{
				throw new Exception("Model file does not exist."); 
			}

			if (fileInfo.Extension.ToLowerInvariant() == ".obj")
			{
				if (flat == true) 
				{
					ModelHelper.MeshToModel_Flat(filePath, out m_ModelVerts);
				}
				else 
				{
					ModelHelper.MeshToModel_Smooth(filePath, out m_ModelVerts);
				}
			}
			else
			{
                ExtraData[] extras = null; 

                if (flat == true)
                {
                    ModelHelper.GetModelData_Flat(filePath, out m_ModelVerts, out extras);
                }
                else
                {
                    ModelHelper.GetModelData_Smooth(filePath, out m_ModelVerts, out extras);
                }

                Extras.AddRange(extras); 
			}

			Name = name;

            IsShadowCaster = true; 

            ModelBuffer = new VertexBuffer(Name + " Verts", ResourceMode.Static, new VertexBufferInfo(TexturedModelVertex.Format, m_ModelVerts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
			
			m_VAO = new VertexArrayObject(Name + " VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer });			
		}

        public override Vector3[] GetVertices()
        {
            Vector3[] verts = new Vector3[m_ModelVerts.Length];

            for (int i = 0; i < m_ModelVerts.Length; i++)
            {
                verts[i] = m_ModelVerts[i].Position; 
            }

            return verts; 
        }

        public override void Update()
		{
			
		}

        public override void Render(View3D view,
            Matrix4 objectMatrix, Matrix4 normalMatrix, Color4 color, Texture2D modelTexture,
			ModelLighting lighting, ModelLightingMode lightingMode)
		{
            ModelEffect effect = null; 

            int textureIndex = IsTextured ? 1 : 0;

            if ((lighting.LightingModeFlags[textureIndex] & lightingMode) != lightingMode)
            {
                return;
            }

            effect = lighting.SelectEffect(IsTextured, ModelEffectMode.NonBatch, lightingMode);

            if (effect != null) 
            {
                effect.Render_Single(
                            ref objectMatrix, ref normalMatrix, ref color, modelTexture, 
                            lighting.LightingArguments, 
                            m_VAO, ModelBuffer.ResourceInfo.Count);
            }

			//StaticObjects.DrawCallsAccumulator++;
		}


		#region IResourceManager Members

        public override void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

        public override void LoadResources()
		{
			if (Disposed == false)
			{
                return; 
            }

            if (m_ModelVerts.Length != 0)
            {
                m_VAO.LoadResources();

                BuildModelVerts(ModelBuffer);
            }

			Disposed = false; 
		}

        public override void UnloadResources()
		{
            if (Disposed == true)
            {
                return;
            }

            if (m_ModelVerts.Length != 0)
            {
                m_VAO.UnloadResources();
            }

            Disposed = true;
		}

        public override void BuildModelVerts(VertexBuffer buffer)
		{
            if (m_ModelVerts.Length != 0)
            {
                DataStream stream;

                buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);
                stream.WriteRange(m_ModelVerts, 0, m_ModelVerts.Length);
                buffer.UnmapBuffer();
            }
		}

		public override void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
