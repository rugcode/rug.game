﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;

namespace Rug.Game.Flat.Models
{
    public class ModelShaderSet
    {
        public readonly ModelEffect[] DiffuseSpecularPerPixel;
        public readonly ModelEffect[] DiffuseSpecular;
        public readonly ModelEffect[] Diffuse;

        public readonly ModelEffect[][] RefractiveSpecular;
        public readonly ModelEffect[] Refractive;

        public readonly ModelEffect CelPerPixel;
        public readonly ModelEffect Color;
        public readonly ModelEffect Outline;
        public readonly ModelEffect Shadow;
        public readonly ModelEffect Normal;
        

        public int ShadowLights { get; private set; }
        public int ShadowsPerLight { get; private set; }

        public ModelLightingMode LightingModeFlags { get; private set; }

        public ModelTextureMode TextureMode { get; private set; }

        public ModelShaderSet(ModelEffectMode effectMode, ModelLightingMode modeFlags, int shadowLights, int count, ModelTextureMode textureMode)
        {
            LightingModeFlags = modeFlags;

            ShadowLights = shadowLights;
            ShadowsPerLight = ModelEffect.DefaultShadowsPerLight;

            TextureMode = textureMode;

            DiffuseSpecularPerPixel = GetShaders(effectMode, ModelLightingMode.DiffuseSpecularPerPixel, shadowLights, count, textureMode);
            DiffuseSpecular = GetShaders(effectMode, ModelLightingMode.DiffuseSpecular, shadowLights, count, textureMode);
            Diffuse = GetShaders(effectMode, ModelLightingMode.Diffuse, shadowLights, count, textureMode);
            RefractiveSpecular = GetShaders_RefractiveSpecular(effectMode, shadowLights, count, textureMode);
            Refractive = GetShaders_Refractive(effectMode, textureMode);

            CelPerPixel = GetShader(effectMode, ModelLightingMode.CelPerPixel, VisualQuality.Disabled, 0, count, textureMode);
            Color = GetShader(effectMode, ModelLightingMode.Color, VisualQuality.Disabled, 0, count, textureMode);
            Outline = GetShader(effectMode, ModelLightingMode.Outline, VisualQuality.Disabled, 0, 0, textureMode);
            Shadow = GetShader(effectMode, ModelLightingMode.Shadow, VisualQuality.Disabled, 0, 0, textureMode);
            Normal = GetShader(effectMode, ModelLightingMode.Normal, VisualQuality.Disabled, 0, 0, textureMode);
        }

        private ModelEffect GetShader(ModelEffectMode modelEffectMode, 
            ModelLightingMode modelLightingMode, 
            VisualQuality visualQuality, int shadowLights, int count, 
            ModelTextureMode texture)
        {
            if ((LightingModeFlags & modelLightingMode) != modelLightingMode)
            {
                return null;
            }

            return SharedEffects.Effects[ModelEffect.EffectName(modelEffectMode, modelLightingMode, visualQuality, shadowLights, count, texture, VisualQuality.Disabled)] as ModelEffect;
        }

        private ModelEffect[] GetShaders(ModelEffectMode modelEffectMode, ModelLightingMode modelLightingMode, int shadowLights, int count, ModelTextureMode textured)
        {
            ModelEffect[] effects = new ModelEffect[5];

            if ((LightingModeFlags & modelLightingMode) != modelLightingMode)
            {
                return null;
            }

            for (int i = 0; i < effects.Length; i++)
            {
                effects[i] = SharedEffects.Effects[ModelEffect.EffectName(modelEffectMode, modelLightingMode, (VisualQuality)i, shadowLights, count, textured, VisualQuality.Disabled)] as ModelEffect;
            }

            return effects;
        }

        private ModelEffect[][] GetShaders_RefractiveSpecular(ModelEffectMode modelEffectMode, int shadowLights, int count, ModelTextureMode textured)
        {           
            ModelEffect[][] effects = new ModelEffect[5][];

            for (int i = 0; i < effects.Length; i++)
            {
                effects[i] = new ModelEffect[5]; 
            }

            if ((LightingModeFlags & ModelLightingMode.RefractiveSpecular) != ModelLightingMode.RefractiveSpecular)
            {
                return null;
            }

            for (int i = 0; i < effects.Length; i++)
            {
                for (int j = 0; j < effects[i].Length; j++)
                {
                    effects[i][j] = SharedEffects.Effects[ModelEffect.EffectName(modelEffectMode, ModelLightingMode.RefractiveSpecular, (VisualQuality)i, shadowLights, count, textured, (VisualQuality)j)] as ModelEffect;
                }
            }

            return effects;
        }

        private ModelEffect[] GetShaders_Refractive(ModelEffectMode modelEffectMode, ModelTextureMode textured)
        {
            ModelEffect[] effects = new ModelEffect[5];

            if ((LightingModeFlags & ModelLightingMode.Refractive) != ModelLightingMode.Refractive)
            {
                return null;
            }

            for (int i = 0; i < effects.Length; i++)
            {
                effects[i] = SharedEffects.Effects[ModelEffect.EffectName(modelEffectMode, ModelLightingMode.Refractive, VisualQuality.Disabled, 0, 0, textured, (VisualQuality)i)] as ModelEffect;
            }

            return effects;
        }

        internal void SetupLights(UniformBuffer LightBuffer)
        {
            if ((LightingModeFlags & ModelLightingMode.DiffuseSpecularPerPixel) == ModelLightingMode.DiffuseSpecularPerPixel)
            {
                for (int i = 0; i <= (int)VisualQuality.Preposterous; i++)
                {
                    DiffuseSpecularPerPixel[i].SetupLights(LightBuffer);
                }
            }

            if ((LightingModeFlags & ModelLightingMode.DiffuseSpecular) == ModelLightingMode.DiffuseSpecular)
            {
                for (int i = 0; i <= (int)VisualQuality.Preposterous; i++)
                {
                    DiffuseSpecular[i].SetupLights(LightBuffer);
                }
            }

            if ((LightingModeFlags & ModelLightingMode.Diffuse) == ModelLightingMode.Diffuse)
            {
                for (int i = 0; i <= (int)VisualQuality.Preposterous; i++)
                {
                    Diffuse[i].SetupLights(LightBuffer);
                }
            }

            if ((LightingModeFlags & ModelLightingMode.CelPerPixel) == ModelLightingMode.CelPerPixel)
            {
                CelPerPixel.SetupLights(LightBuffer);
            }
        }
    }

	public class ModelLighting : IResourceManager
	{
        private bool m_TexturesSetForShadowCast;

        public readonly ModelShaderSet[] NonBatch;
        public readonly ModelShaderSet[] FixedBatch;
        public readonly ModelShaderSet[] Batch;
        public readonly ModelShaderSet[] Extruded;

        public UniformBuffer LightBuffer { get; private set; }

		public ModelLightInstance[] Lights { get; private set; }
        public int ShadowLights { get; private set; }
        public int ShadowsPerLight { get; private set; }
        
        public readonly Matrix4[,] ShadowMatrices;
        public readonly Texture2D[,] ShadowTextures;
        public readonly Vector3[,] ShadowMinMax;
        public LightingArguments LightingArguments; 

        public VisualQuality ShadowFilterQuality { get; set; }

        public VisualQuality RefractionQuality { get; set; }

        public ModelLightingMode[] LightingModeFlags { get; private set; }

        public ModelLighting(ModelLightingMode[] modeFlags, int shadowLights, int count)
		{
            LightingArguments = new Shaders.Flat.LightingArguments(); 

			Disposed = true;

            LightingModeFlags = new ModelLightingMode[(int)ModelTextureMode.Textured + 1]; 

            ShadowLights = shadowLights;
            ShadowsPerLight = ModelEffect.DefaultShadowsPerLight;

            ShadowMatrices = new Matrix4[ShadowLights, ShadowsPerLight];
            ShadowTextures = new Texture2D[ShadowLights, ShadowsPerLight];
            ShadowMinMax = new Vector3[ShadowLights, ShadowsPerLight];

            NonBatch = new ModelShaderSet[(int)ModelTextureMode.Textured + 1];
            FixedBatch = new ModelShaderSet[(int)ModelTextureMode.Textured + 1];
            Batch = new ModelShaderSet[(int)ModelTextureMode.Textured + 1];
            Extruded = new ModelShaderSet[(int)ModelTextureMode.Textured + 1];

            for (int i = 0; i <= (int)ModelTextureMode.Textured; i++)
            {
                if (i < modeFlags.Length)
                {
                    LightingModeFlags[i] = modeFlags[i];
                }
                else
                {
                    LightingModeFlags[i] = ModelLightingMode.None;
                }

                NonBatch[i] = new ModelShaderSet(ModelEffectMode.NonBatch, LightingModeFlags[i], shadowLights, count, (ModelTextureMode)i);
                FixedBatch[i] = new ModelShaderSet(ModelEffectMode.FixedBatch, LightingModeFlags[i], shadowLights, count, (ModelTextureMode)i);
                Batch[i] = new ModelShaderSet(ModelEffectMode.Batch, LightingModeFlags[i], shadowLights, count, (ModelTextureMode)i);
                Extruded[i] = new ModelShaderSet(ModelEffectMode.Extruded, LightingModeFlags[i], shadowLights, count, (ModelTextureMode)i);
            }

            LightBuffer = new UniformBuffer("Lights", ResourceMode.Dynamic, new UniformBufferInfo(ModelLightInstance.Format, shadowLights + count, OpenTK.Graphics.OpenGL.BufferUsageHint.DynamicDraw));

			Lights = new ModelLightInstance[LightBuffer.ResourceInfo.Count]; 
		}

        public void UpdateLightingArguments(View3D view)
        {
            LightingArguments.Update(view);                     
            LightingArguments.ShadowMatrices = ShadowMatrices;
            LightingArguments.ShadowTextures = ShadowTextures; 
            LightingArguments.ShadowMinMax = ShadowMinMax;
        }

		public void Update()
		{
			Rug.Game.Core.Buffers.DataStream stream;

			LightBuffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(Lights, 0, Lights.Length);

			LightBuffer.UnmapBuffer();
		}

		public bool Disposed { get; private set; }

		public void LoadResources()
		{
			if (Disposed == true)
			{		
				LightBuffer.LoadResources();

                AttachLightBuffers();

                m_TexturesSetForShadowCast = false; 

				Disposed = false;
			}
		}

        public void AttachLightBuffers()
        {
            for (int i = 0; i <= (int)ModelTextureMode.Textured; i++)
            {
                NonBatch[i].SetupLights(LightBuffer);
                FixedBatch[i].SetupLights(LightBuffer);
                Batch[i].SetupLights(LightBuffer);
            }
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == false)
			{
				LightBuffer.UnloadResources();

				Disposed = true;
			}
		}

		public void Dispose()
		{
			UnloadResources();
		}

        public void ResetTexturesSetForShadowCastFlag()
        {
            m_TexturesSetForShadowCast = false; 
        }

        public void ConfigureTexturesForShadowCast()
        {
            if (m_TexturesSetForShadowCast == true)
            {
                return; 
            }

            m_TexturesSetForShadowCast = true;

            for (int i = 0; i < ShadowLights; i++)
            {
                for (int j = 0; j < ShadowsPerLight; j++)
                {
                    Texture2D shadow = ShadowTextures[i, j];

                    if (shadow != null && shadow.IsLoaded == true)
                    {
                        shadow.Bind();

                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.CompareRefToTexture);

                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareFunc, (int)All.Lequal);

                        shadow.Unbind();
                    }
                }
            }
        }

        public void ConfigureTexturesDebugRendering()
        {
            if (m_TexturesSetForShadowCast == false)
            {
                return;
            }

            m_TexturesSetForShadowCast = false;

            for (int i = 0; i < ShadowLights; i++)
            {
                for (int j = 0; j < ShadowsPerLight; j++)
                {
                    Texture2D shadow = ShadowTextures[i, j];

                    if (shadow != null && shadow.IsLoaded == true)
                    {
                        shadow.Bind();

                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureCompareMode, (int)TextureCompareMode.None);

                        shadow.Unbind();
                    }
                }
            }
        }

        internal ModelEffect SelectEffect(bool textured, ModelEffectMode modelEffectMode, ModelLightingMode lightingMode)
        {
            int textureIndex = textured ? 1 : 0;

            ModelShaderSet effectSet = null;

            switch (modelEffectMode)
            {
                case ModelEffectMode.Batch:
                    effectSet = Batch[textureIndex]; 
                    break;
                case ModelEffectMode.FixedBatch:
                    effectSet = FixedBatch[textureIndex]; 
                    break;
                case ModelEffectMode.NonBatch:
                    effectSet = NonBatch[textureIndex]; 
                    break;
                case ModelEffectMode.Extruded:
                    effectSet = Extruded[textureIndex];
                    break; 
                default:
                    return null;
            }


            switch (lightingMode)
            {
                case ModelLightingMode.DiffuseSpecularPerPixel:
                    return effectSet.DiffuseSpecularPerPixel[(int)ShadowFilterQuality];
                case ModelLightingMode.DiffuseSpecular:
                    return effectSet.DiffuseSpecular[(int)ShadowFilterQuality];                    
                case ModelLightingMode.Diffuse:
                    return effectSet.Diffuse[(int)ShadowFilterQuality];
                case ModelLightingMode.RefractiveSpecular:
                    return effectSet.RefractiveSpecular[(int)ShadowFilterQuality][(int)RefractionQuality];
                case ModelLightingMode.Refractive:
                    return effectSet.Refractive[(int)RefractionQuality];
                case ModelLightingMode.CelPerPixel:
                    return effectSet.CelPerPixel;
                case ModelLightingMode.Outline:
                    return effectSet.Outline;
                case ModelLightingMode.Color:
                    return effectSet.Color;
                case ModelLightingMode.Shadow:
                    return effectSet.Shadow;
                case ModelLightingMode.Normal:
                    return effectSet.Normal;
                default:
                    return null;
            }
        }
    }
}
