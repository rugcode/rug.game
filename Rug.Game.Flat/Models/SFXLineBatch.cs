﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Flat.Models.Data;
using Rug.Game.Flat.Shaders.Flat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Flat.Models
{
    public class SFXLineBatch : IResourceManager
	{
		protected ModelLineEffect Effect;

		private VertexBuffer m_Vertices;

        public int DesiredMaxCount;

        public int MaxCount;

        public int InstanceCount;

        public SFXLineVertex[] Points { get; private set; } 

		public VertexBuffer Vertices { get { return m_Vertices; } }

        public Matrix4 ObjectMatrix;

        public Vector3 Normal;

        public bool NormalOverride;

        public SFXLineBatch(int count)
		{
            Disposed = true; 
                 
			if (Effect == null)
			{
                Effect = SharedEffects.Effects["ModelLineEffect"] as ModelLineEffect;
			}

            ObjectMatrix = Matrix4.Identity;

            MaxCount = count;

            Points = new SFXLineVertex[count * 4];

            m_Vertices = new VertexBuffer("Texture Box Vertices", ResourceMode.Static, new VertexBufferInfo(SFXLineVertex.Format, count * 4, OpenTK.Graphics.OpenGL.BufferUsageHint.StreamDraw));			
		}

        public void CheckResize()
        {
            if (DesiredMaxCount > MaxCount)
            {
                int value = DesiredMaxCount;
                float factor = 128.0f;

                MaxCount = (int)(Math.Max(1, Math.Round(((float)value / (float)factor) + 0.5f, MidpointRounding.AwayFromZero)) * factor);

                m_Vertices.ResourceInfo.Count = MaxCount * 4;

                if (Disposed == false)
                {
                    UnloadResources();

                    LoadResources();
                }
            }

            if (MaxCount * 4 != Points.Length)
            {
                Points = new SFXLineVertex[MaxCount * 4];

                InstanceCount = 0;
            }
        }

        public virtual void Update()
        {
            if (Disposed == true)
            {
                return;
            }

            if (InstanceCount == 0)
            {
                return;
            }

            DataStream stream;
            m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);
            
            stream.WriteRange(Points, 0, InstanceCount * 4);

            m_Vertices.UnmapBuffer();
        }

		public virtual void Render(View3D view)
		{
            if (InstanceCount <= 0)
            {
                return;                    
            }

			Effect.Begin(ref view.World, ref view.Projection, ref Normal, NormalOverride, view);

            Matrix4 obj = ObjectMatrix;

            Effect.Render(ref obj, m_Vertices, InstanceCount * 4);
		}

        public virtual void Render_Identity(View3D view)
        {
            if (InstanceCount <= 0)
            {
                return;
            }

            Matrix4 objectMatrix = Matrix4.Identity;
            Matrix4 pixelScale = Matrix4.CreateScale((float)view.Viewport.Height / (float)view.Viewport.Width, 1f, 1f);

            Effect.Begin(ref objectMatrix, ref pixelScale, ref Normal, NormalOverride, view);

            Effect.Render(ref objectMatrix, m_Vertices, InstanceCount * 4);
        }

		#region IResourceManager Members

        public bool Disposed { get; private set; }

		public void LoadResources()
		{
			if (Disposed == false)
			{
                return; 
            }

            m_Vertices.LoadResources();

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
                return; 
            }

            m_Vertices.UnloadResources();

			Disposed = true;
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
