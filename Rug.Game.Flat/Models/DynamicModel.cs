﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Flat.Shaders.Flat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Flat.Models
{
    public class DynamicModel : ModelBase
	{       
        private ModelVertex[] m_ModelVerts;

        public int DesiredMaxCount { get; set; }

        public int MaxCount { get; set; }

        public int InstanceCount { get; set; }

        public VertexArrayObject VAO { get; private set; }

        public ModelVertex[] ModelVerts { get { return m_ModelVerts; } }

        public ModelData Data { get; set; }

        public ModelData ActiveData { get; set; } 

        public DynamicModel(string name)
        {
            Disposed = true; 

            Name = name;
            
            IsShadowCaster = true;

            Extras = new List<ExtraData>();

            MaxCount = 128;

            m_ModelVerts = new ModelVertex[MaxCount];
            
            ModelBuffer = new VertexBuffer(Name + " Verts", ResourceMode.Static, new VertexBufferInfo(ModelVertex.Format, m_ModelVerts.Length, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

            VAO = new VertexArrayObject(Name + " VAO", ResourceMode.Dynamic, new IBuffer[] { ModelBuffer });

            ActiveData = new ModelData(); 
        }

        public void CopyData()
        {
            Data.CopyTo(ActiveData); 
        }

        public void FlushModelData()
        {
            if (Data == null)
            {
                return; 
            }

            ShapeInfo shape = ActiveData.GetShapeInfo();

            DesiredMaxCount = shape.ModelVerts.Length;

            CheckResize(); 

            shape.ModelVerts.CopyTo(m_ModelVerts, 0);

            InstanceCount = shape.ModelVerts.Length;

            Update();
        }

        public override Vector3[] GetVertices()
        {
            Vector3[] verts = new Vector3[m_ModelVerts.Length];

            for (int i = 0; i < m_ModelVerts.Length; i++)
            {
                verts[i] = m_ModelVerts[i].Position; 
            }

            return verts; 
        }

        public void CheckResize()
        {
            if (DesiredMaxCount > MaxCount)
            {
                int value = DesiredMaxCount;
                float factor = 128.0f;

                MaxCount = (int)(Math.Max(1, Math.Round(((float)value / (float)factor) + 0.5f, MidpointRounding.AwayFromZero)) * factor);

                ModelBuffer.ResourceInfo.Count = MaxCount;

                if (Disposed == false)
                {
                    UnloadResources();

                    LoadResources();
                }
            }

            if (MaxCount != m_ModelVerts.Length)
            {
                m_ModelVerts = new ModelVertex[MaxCount];

                InstanceCount = 0;
            }
        }

        public override void Update()
        {
            if (Disposed == true)
            {
                return;
            }

            if (InstanceCount == 0)
            {
                return;
            }

            BuildModelVerts(ModelBuffer);
        }

        public override void Render(View3D view, 
			Matrix4 objectMatrix, Matrix4 normalMatrix, Color4 color, Texture2D modelTexture,
			ModelLighting lighting, ModelLightingMode lightingMode)
		{
            ModelEffect effect = null;

            int textureIndex = IsTextured ? 1 : 0;

            if ((lighting.LightingModeFlags[textureIndex] & lightingMode) != lightingMode)
            {
                return;
            }

            effect = lighting.SelectEffect(IsTextured, ModelEffectMode.NonBatch, lightingMode);

            if (effect != null) 
            {
                effect.Render_Single(
                        ref objectMatrix, ref normalMatrix, ref color, modelTexture,
                        lighting.LightingArguments,
                        VAO, InstanceCount);
            }

			//StaticObjects.DrawCallsAccumulator++;
		}

		#region IResourceManager Members

        public override void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

        public override void LoadResources()
		{
			if (Disposed == false)
			{
                return; 
            }

            VAO.LoadResources();	

			Disposed = false; 
		}

        public override void UnloadResources()
		{
            if (Disposed == true)
            {
                return;
            }

			VAO.UnloadResources();

			Disposed = true;
		}

        public override void BuildModelVerts(VertexBuffer buffer)
		{
			DataStream stream;

			buffer.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);
			stream.WriteRange(m_ModelVerts, 0, InstanceCount);
			buffer.UnmapBuffer();
		}

        public override void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
    }
}
