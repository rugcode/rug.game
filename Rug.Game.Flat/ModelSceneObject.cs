﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Data;
using Rug.Game.Flat.Models;
using System;

namespace Rug.Game.Flat
{
	public class ModelSceneObject
	{
        public int Index; // { get; set; }

		public bool IsPartOfBatch { get { return Index >= 0; } }

        public bool DoNotBatch;

        public object Tag;

        public bool IsFixed;

        public bool IsStatic;

        public bool CanBeStatic;

        public string Model;

        public Vector3 Center;

        public Quaternion Rotation;

        public float Scale;

        public Color3 Color;

        public float Emissivity;

        public float Alpha;	

		public Color4 ColorArg; // { get; protected set; } 

        public Matrix4 ObjectMatrix { get; set; }

        public Matrix4 NormalMatrix; // { get; set; }


		public ModelSceneObject()
		{
			Index = -1;
			Scale = 1f;
			Color = Color4.White;
			Emissivity = 0f;
            Rotation = Quaternion.Identity;
            Alpha = 1f;
            CanBeStatic = true; 
		}

		public virtual void Update()
		{
			if (IsFixed == false)
			{
				Matrix4 objectMatrix = Matrix4.Identity;
                Matrix4 normalMatrix = Matrix4.Identity; 

				if (Scale != 1f)
				{
					objectMatrix *= Matrix4.CreateScale(Scale);
				}

                if (Rotation != Quaternion.Identity)
				{
                    normalMatrix = Matrix4.CreateFromQuaternion(Rotation);
                    objectMatrix *= normalMatrix;
				}

				if (Center != Vector3.Zero)
				{
					objectMatrix *= Matrix4.CreateTranslation(Center);
				}
               
                //q_inverse q = (q_conjugate q) * (scalar_to_q (m * m))
                //where
                //m = q_modulus q

                //NormalMatrix = Matrix4.CreateFromQuaternion(Rotation);
                //NormalMatrix.Transpose();
                //NormalMatrix.Invert();

                //Quaternion normalQuaternion = Rotation;
                //normalQuaternion.Conjugate(); 
                //normalQuaternion.Invert();
                //normalQuaternion.Normalize();                
                //NormalMatrix = Matrix4.CreateFromQuaternion(normalQuaternion);

                NormalMatrix = normalMatrix;

				ObjectMatrix = objectMatrix;                
				
			}
			else
			{
				Matrix4 objectMatrix = Matrix4.Identity;

				if (Scale != 1f)
				{
					objectMatrix *= Matrix4.CreateScale(Scale);
				}

				if (Center != Vector3.Zero)
				{
					objectMatrix *= Matrix4.CreateTranslation(Center);
				}

				ObjectMatrix = objectMatrix;
				NormalMatrix = Matrix4.Identity; 
			}

			ColorArg = Color3.ToColor4(Color, BitConverter.ToSingle(Vector2h.GetBytes(new Vector2h(1f - (Emissivity * Emissivity), Alpha)), 0));
		}

		public ModelInstance ToModelInstance()
		{
			return new ModelInstance()
			{
				Color = ColorArg, 
				Object = ObjectMatrix,
				Normal = NormalMatrix,
			};
		}

		public ModelFixedInstance ToModelFixedInstance()
		{
			return new ModelFixedInstance()
			{
				Color = ColorArg,
				Center = new Vector4(Center, Scale)
			};
		}	
	}
}
