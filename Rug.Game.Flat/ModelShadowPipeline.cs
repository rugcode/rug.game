﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using System;
using System.Drawing;

namespace Rug.Game.Flat
{
    public class ModelShadowPipeline : IResizablePipeline, IResourceManager
    { 
        public readonly ResourceManager ResourceManagers = new ResourceManager();

		public readonly ResourceSet Resources;

		public readonly FrameBuffer ShadowBuffer;

		private readonly TextureBox Box;
        public float Spread;
        
        public Vector2 ShadowMinMax { get; set; }     

		public bool Disposed { get; private set; }

		public bool IsEnabled { get; set; }

		public string Name { get; set; }

        public View3D View { get; set; } 

		public PipelineMode PipelineMode
		{
			get 
			{
				return
					PipelineMode.BeginEndBlock |
					PipelineMode.Render |
					PipelineMode.Update; 
			}
		}

        public Size Size { get; set; } 

        public ModelScene Scene { get; set; }

        public Matrix4 ShadowMatrix { get; set; }

        public ModelSceneLight Light { get; set; }

        public Vector3 Volume { get; set; }

        public Vector3 Center { get; set; }

        public Quaternion Rotation { get; set; }

        public Vector3 UpVector { get; set; }

        public Texture2D ShadowTexture { get { return ShadowBuffer[FramebufferAttachment.DepthAttachment]; } }

        public bool DistantMode = false;

        public float VariableOffset = 1f; 
        public float FixedOffset = 0f; 


        public ModelShadowPipeline(string name, bool border)
		{
            UpVector = Vector3.UnitY; 

            Rotation = Quaternion.Identity; 

			Resources = new ResourceSet(name + " Shadow Pipeline Resource Set", ResourceMode.Static);

			Disposed = true;

            ShadowBuffer = new FrameBuffer(name + " Shadow Pipeline Frame Buffer", ResourceMode.Static,
					new FrameBufferInfo(
						new FrameBufferTexture2DInfo(name + " Depth", FramebufferAttachment.DepthAttachment)
						{
							Size = new Size(32, 32),
							InternalFormat = (PixelInternalFormat)All.DepthComponent16,
							PixelFormat = PixelFormat.DepthComponent,
							PixelType = PixelType.Float,
							MagFilter = TextureMagFilter.Nearest,
							MinFilter = TextureMinFilter.Nearest,
                            WrapS = border ? TextureWrapMode.ClampToBorder : TextureWrapMode.ClampToEdge,
                            WrapT = border ? TextureWrapMode.ClampToBorder : TextureWrapMode.ClampToEdge,
                            Border = 0,                            
							Samples = 1,                            
						}
						));

			Rug.Game.Environment.ResolutionDependentResources.Add(ShadowBuffer);

            //Resources.Add(ShadowBuffer);

			Box = new TextureBox();
			Box.FlipVertical = true;

			ResourceManagers.Add(Box);

            View = new View3D(new Rectangle(0, 0, 32, 32), 32, 32, (float)Math.PI / 4, 1f);
            View.ProjectionKind = ProjectionKind.Orthographic; 
            View.Camera.SmoothCamera = false; 

            View.Camera.Center = new Vector3(0, -60, 0);
            View.Camera.Rotate(0, -MathHelper.PiOver2, 0);
            View.UpdateFrustum = true; 
		}

		public void Resize(int width, int height, MultiSamples samples)
        {
            View.Resize(new Rectangle(0, 0, Size.Width, Size.Height), Size.Width, Size.Height);
            View.ProjectionKind = ProjectionKind.Orthographic; 
    
			//ShadowBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Samples = 1;
            ShadowBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Size = Size;
		}

        public virtual void Update(View3D view)
		{
			
		}

		public virtual void Begin()
		{

		}

		public virtual void End()
		{

		}

		public void BeginRender(View3D view)
		{           
			ShadowBuffer.Bind();

			GLState.EnableBlend = false;
			GLState.ClearDepth(1.0f);
            GLState.Viewport = view.Viewport; 

            GLState.Apply(view);

            if (DistantMode == true)
            {
                GL.Enable(EnableCap.PolygonOffsetFill);
                //GL.PolygonOffset(14f, 1f);
                //GL.PolygonOffset(2f, 1f);
                //GL.PolygonOffset(4f, 1f);
                //GL.PolygonOffset(4f, 0.75f);
                GL.PolygonOffset(VariableOffset, FixedOffset);
            }
		}

        public void Render(Rug.Game.Core.Rendering.View3D view)
        {
            GLState.CullFace(OpenTK.Graphics.OpenGL.CullFaceMode.Back);
            GLState.EnableCullFace = false;
            GLState.EnableDepthMask = false;
            GLState.EnableDepthTest = false;
            GLState.ClearDepth(1.0f);
            GLState.Viewport = view.Viewport;
            GLState.ApplyAll(view);

            Box.FlipVertical = true;
            Box.Rectangle = new RectangleF(-1f, 1f - 0.5f, 0.5f, 0.5f);
            Box.CheckAndWriteRectangle();
            Box.Texture = ShadowBuffer[FramebufferAttachment.DepthAttachment];
            Box.Render(view);
        }

        public void RenderToShadowMap()
		{
            View.OrthographicBoxSize = Volume; //  new Vector3(100, 100, 200);

            Vector3 lightCenter = Light.Center;
            Vector3 boundsCenter = new Vector3(0, 0, 0);
            Vector3 lightOffset = lightCenter;

            lightOffset.Normalize();
            
            //View.Camera.SetRotation(Quaternion.FromAxisAngle(Vector3.UnitY, MathHelper.PiOver4) * Rotation * Quaternion.FromMatrix(LookAt(lightOffset, Vector3.Zero, Vector3.UnitY)));
            View.Camera.SetRotation(Quaternion.FromMatrix(LookAt(lightOffset, Vector3.Zero, UpVector)));
            View.Camera.Offset = 0;
            View.Camera.Center = Center; // +(lightOffset * -100f);

            View.Camera.Update();
            View.UpdateProjection(); 

            Matrix4 biasMatrix = Matrix4.Identity;

            biasMatrix *= Matrix4.CreateScale(0.5f);
            biasMatrix *= Matrix4.CreateTranslation(new Vector3(0.5f));            

            ShadowMatrix = View.World * View.Projection * biasMatrix;
            
            BeginRender(View);

            RenderContent(View);

            EndRender(View);
		}

        public static Matrix3 LookAt(Vector3 eye, Vector3 target, Vector3 up)
        {
            Matrix3 matrix;
            Vector3 right = Vector3.Normalize(eye - target);
            Vector3 vector2 = Vector3.Normalize(Vector3.Cross(up, right));
            Vector3 vector3 = Vector3.Normalize(Vector3.Cross(right, vector2));
            matrix.Row0.X = vector2.X;
            matrix.Row0.Y = vector3.X;
            matrix.Row0.Z = right.X;
            matrix.Row1.X = vector2.Y;
            matrix.Row1.Y = vector3.Y;
            matrix.Row1.Z = right.Y;
            matrix.Row2.X = vector2.Z;
            matrix.Row2.Y = vector3.Z;
            matrix.Row2.Z = right.Z;
            return matrix;
        }

		public virtual void RenderContent(Rug.Game.Core.Rendering.View3D view)
		{
            if (DistantMode == true)
            {
                GLState.CullFace(OpenTK.Graphics.OpenGL.CullFaceMode.Back);
            }
            else
            {
                GLState.CullFace(OpenTK.Graphics.OpenGL.CullFaceMode.Front);
            }
            
            GLState.EnableCullFace = true;
            GLState.EnableDepthMask = true;
            GLState.EnableDepthTest = true;
            GLState.Apply(Size.Width, Size.Height);
            
            GL.Clear(ClearBufferMask.DepthBufferBit);

            Scene.LightingMode = Shaders.Flat.ModelLightingMode.Shadow; 
            Scene.Render(view);
		}

		public void EndRender(Rug.Game.Core.Rendering.View3D view)
		{
			ShadowBuffer.Unbind();

            if (DistantMode == true)
            {
                GL.Disable(EnableCap.PolygonOffsetFill);
            }
		}

		public void LoadResources()
		{			
			ResourceManagers.LoadResources();
			Resources.LoadResources(); 
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1 + ResourceManagers.Total; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= (this as IResourceManager).Count)
            {
                process.Pop();

                Disposed = false;
            }
            else
            {
                process.Increment();
                process.Push(ResourceManagers);
            }
        }

		public void UnloadResources()
		{
			ResourceManagers.UnloadResources();
			Resources.UnloadResources(); 
		}

		public void Dispose()
		{
			ResourceManagers.Dispose(); 
		}
    }
}
