﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Pipeline;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using Rug.Game.Effect.PostProcess;
using Rug.Game.Flat.Shaders.Flat;
using Rug.Game.Shaders.PostProcess;
using System;
using System.Drawing;

namespace Rug.Game.Flat
{
    public class ModelReflectionsPipeline : IResizablePipeline, IResourceManager
    {
        private DownSample m_DownSample;

        public readonly ResourceManager ResourceManagers = new ResourceManager();

		public readonly ResourceSet Resources;

		public readonly FrameBuffer FrameBuffer;

        public readonly FrameBuffer SourceTempBuffer;
		
		private readonly TextureBox Box;

		public bool Disposed { get; private set; }

		public bool IsEnabled { get; set; }

		public string Name { get; set; }

		public PipelineMode PipelineMode
		{
			get 
			{
				return
					PipelineMode.BeginEndBlock |
					PipelineMode.Render |
					PipelineMode.Update; 
			}
		}

        public ModelScene Scene { get; set; }

        public Texture2D ColorBuffer { get { return FrameBuffer[FramebufferAttachment.ColorAttachment0]; } }

        public FrameBuffer SourceFrameBuffer { get; set; }

        public Texture2D SourceImageTexture { get; set; }
        
        public Texture2D SourceDepthTexture { get; set; }

        public TextureCube ReflectionsCubeMap { get; set; }

        public bool UseMultisample { get; set; }

        public CullFaceMode CullFaceMode { get; set; }

        public ModelLightingMode ReflectionMode { get; set; } 

        public ModelReflectionsPipeline(string name)
		{
            ReflectionMode = ModelLightingMode.RefractiveSpecular;

            CullFaceMode = OpenTK.Graphics.OpenGL.CullFaceMode.Back; 

			Resources = new ResourceSet(name + " Pipeline Resource Set", ResourceMode.Static);

			Disposed = true;
            
			FrameBuffer = new FrameBuffer(name + " Pipeline Frame Buffer", ResourceMode.Static,
					new FrameBufferInfo(
						new FrameBufferTexture2DInfo(name + " Color", FramebufferAttachment.ColorAttachment0)
						{
							Size = new Size(32, 32),
                            InternalFormat = PixelInternalFormat.R11fG11fB10f, // PixelInternalFormat.Rgba16f,                            
							PixelFormat = PixelFormat.Rgba,
							PixelType = PixelType.Float,
							MagFilter = TextureMagFilter.Nearest,
							MinFilter = TextureMinFilter.Nearest,
							WrapS = TextureWrapMode.ClampToBorder,
							WrapT = TextureWrapMode.ClampToBorder,
							Samples = 1,
						},
						new FrameBufferTexture2DInfo(name + " Depth", FramebufferAttachment.DepthAttachment)
						{
							Size = new Size(32, 32),
							InternalFormat = (PixelInternalFormat)All.DepthComponent24,
							PixelFormat = PixelFormat.DepthComponent,
							PixelType = PixelType.UnsignedInt,
							MagFilter = TextureMagFilter.Nearest,
							MinFilter = TextureMinFilter.Nearest,
							WrapS = TextureWrapMode.ClampToBorder,
							WrapT = TextureWrapMode.ClampToBorder,
							Samples = 1,
						}
						));

			Rug.Game.Environment.ResolutionDependentResources.Add(FrameBuffer);

            SourceTempBuffer = new FrameBuffer(name + " Pipeline Temp Frame Buffer", ResourceMode.Static,
                    new FrameBufferInfo(
                        new FrameBufferTexture2DInfo(name + " Color", FramebufferAttachment.ColorAttachment0)
                        {
                            Size = new Size(32, 32),
                            InternalFormat = PixelInternalFormat.R11fG11fB10f, // PixelInternalFormat.Rgba16f,                            
                            PixelFormat = PixelFormat.Rgba,
                            PixelType = PixelType.Float,
                            MagFilter = TextureMagFilter.Linear,
                            MinFilter = TextureMinFilter.Linear,
                            WrapS = TextureWrapMode.ClampToBorder,
                            WrapT = TextureWrapMode.ClampToBorder,
                            Samples = 1,
                        }                        
                        ));

            Rug.Game.Environment.ResolutionDependentResources.Add(SourceTempBuffer);

			Box = new TextureBox();
			Box.FlipVertical = true;

			ResourceManagers.Add(Box);
		}

		public void Resize(int width, int height, MultiSamples samples)
        {
            if (UseMultisample == true)
            {
                FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples = GLHelper.MultiSampleToSampleCount(samples);
                FrameBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Samples = GLHelper.MultiSampleToSampleCount(samples);
            }
            else
            {
                FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples = 1;
                FrameBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Samples = 1;
            }

            SourceTempBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples = 1;

            FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = new Size(width, height);           			
            FrameBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Size = new Size(width, height);
            SourceTempBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = new Size(width, height);

            m_DownSample = PostProcessHelper.SelectDownSample(samples);
		}

        public virtual void Update(View3D view)
		{
			
		}

		public virtual void Begin()
		{

		}

		public virtual void End()
		{

		}
        
        public void BeginRender(View3D view)
		{
            Rectangle rect = new Rectangle(new Point(0, 0), FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size);

            if (SourceFrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples > 1)
            {
                SourceFrameBuffer.BlitTo(SourceTempBuffer, ReadBufferMode.ColorAttachment0, DrawBufferMode.ColorAttachment0, rect, rect, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);
            }

            SourceFrameBuffer.BlitTo(FrameBuffer, ReadBufferMode.ColorAttachment0, DrawBufferMode.ColorAttachment0, rect, rect, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);
            SourceFrameBuffer.BlitTo(FrameBuffer, ReadBufferMode.None, DrawBufferMode.None, rect, rect, ClearBufferMask.DepthBufferBit, BlitFramebufferFilter.Nearest);

			FrameBuffer.Bind();

			GLState.EnableBlend = false;
			GLState.ClearDepth(1.0f);
            GLState.Viewport = view.Viewport; 

            GLState.Apply(view);
		}

        public void Render(Rug.Game.Core.Rendering.View3D view)
        {
            //GL.Flush();

            BeginRender(view);

            RenderContent(view);

            EndRender(view);

            //GL.Flush();
        }

		public virtual void RenderContent(Rug.Game.Core.Rendering.View3D view)
		{
            GLState.CullFace(CullFaceMode);
            GLState.EnableCullFace = true;
            GLState.EnableDepthMask = true;
            GLState.EnableDepthTest = true;
            GLState.Apply(view);            

            //GL.Clear(ClearBufferMask.DepthBufferBit);
            //Scene.LightingMode = Shaders.Flat.ModelLightingMode.Color;
            //Scene.Render(view);
            
            GLState.EnableCullFace = false;
            GLState.EnableDepthMask = false;
            GLState.EnableDepthTest = false;
            GLState.Apply(view);

            Box.FlipVertical = true;
            Box.CheckAndWriteRectangle();             

            /* 
            if (SourceImageTexture.ResourceInfo.Samples > 1)
            {
                m_DownSample.Render(SourceImageTexture, Box.Vertices);
            }
            else
            {
                Box.Texture = SourceImageTexture;
                Box.Render(view);
            }
            */ 

            GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GLState.BlendEquation(BlendEquationMode.FuncAdd);
            GLState.EnableBlend = true;
            GLState.EnableCullFace = true;
            GLState.EnableDepthMask = true;
            GLState.EnableDepthTest = true;
            GLState.Apply(view);

            Scene.LightingMode = ReflectionMode;

            Texture2D source = SourceImageTexture;

            if (SourceFrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples > 1)
            {
                source = SourceTempBuffer[FramebufferAttachment.ColorAttachment0];
            }

            //Scene.RenderReflectiveObjects(view, ColorBuffer, SourceDepthTexture, ReflectionsCubeMap);
            Scene.RenderReflectiveObjects(view, source, SourceDepthTexture, ReflectionsCubeMap);
		}

		public void EndRender(Rug.Game.Core.Rendering.View3D view)
		{
			FrameBuffer.Unbind();
		}

		public void LoadResources()
		{			
			ResourceManagers.LoadResources();
			Resources.LoadResources(); 
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1 + ResourceManagers.Total; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= (this as IResourceManager).Count)
            {
                process.Pop();

                Disposed = false;
            }
            else
            {
                process.Increment();
                process.Push(ResourceManagers);
            }
        }

		public void UnloadResources()
		{
			ResourceManagers.UnloadResources();
			Resources.UnloadResources(); 
		}

		public void Dispose()
		{
			ResourceManagers.Dispose(); 
		}

    }
}
