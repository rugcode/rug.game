﻿using BulletSharp.SoftBody;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Physics
{
    /* 
    public class PhysicsRopeObject<T> where T : IPhysicalObject
    {
        public SoftBody Body;

        public T Start;

        public T End;

        public int Count;

        public float DesiredLength;

        public int Restitution;

        public Vector3[] PhysicsPoints;

        public Vector3[] Points;

        public bool IsValid; 

        public void UpdatePhysics()
        {
            if (Body == null || IsValid == false)
            {
                return; 
            }

            for (int i = 0; i < Count; i++)
            {
                PhysicsPoints[i] = Body.Nodes[i].X;
            }
        }

        public void Update()
        {
            PhysicsPoints.CopyTo(Points, 0); 
        }

        public void Connect(SoftBodyWorld T start, T end)
        {

            if (traction != null &&
                (m_TractorBeamRope == null || m_TractorBeamRope.IsDisposed == true))
            {
                SoftBodyWorldInfo.SparseSdf.RemoveReferences(null);

                if (traction.Body.IsStaticObject == true)
                {
                    if (traction.Entity != null && traction.Entity is NoThinkMobileScenery)
                    {
                        NoThinkMobileScenery scenery = traction.Entity as NoThinkMobileScenery;

                        this.World.RemoveRigidBody(traction.Body);

                        Vector3 localInertia = Vector3.Zero;

                        Shapes[traction.ShapeName].CollisionShape.CalculateLocalInertia(scenery.MobileMass, out localInertia);
                        traction.Body.CollisionFlags = traction.Body.CollisionFlags & ~CollisionFlags.StaticObject;

                        traction.Body.SetMassProps(scenery.MobileMass, localInertia);

                        traction.IsStatic = false;
                        traction.IsFixed = false;

                        this.World.AddRigidBody(traction.Body);
                    }
                }

                Vector3 dir = traction.Body.CenterOfMassPosition - StaticObjects.ObjectCameraController.Object.Body.CenterOfMassPosition;

                float dist = 3f; //  Math.Min(dir.Length, 3f);

                m_TractorBeamRope = Create_Rope(StaticObjects.ObjectCameraController.Object.Body.CenterOfMassPosition, traction.Body.CenterOfMassPosition, dist);

                traction.Body.SetDamping(0.06f, 0.06f);

                m_TractorBeamRope.AppendAnchor(0, StaticObjects.ObjectCameraController.Object.Body, new Vector3(0, 0f, 0), true);
                m_TractorBeamRope.AppendAnchor(m_TractorBeamRope.Nodes.Count - 1, traction.Body, new Vector3(0f, -0.25f, 0f), true);

                //traction.Emissivity = 1f; //  0.125f * 0.1f;
                //traction.Color = Color3.Lerp(Color4.White, Color4.LimeGreen, traction.Emissivity);

                StaticObjects.ObjectCameraController.Object.RopedObject = traction;
            }
        }

        public void Disconnect()
        {

        }
    }
     * */ 

}
