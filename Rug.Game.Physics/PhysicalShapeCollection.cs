﻿using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;

namespace Rug.Game.Physics
{
	public class PhysicalShapeCollection : IResourceManager, IEnumerable<IPhysicalShape>
	{
		private Dictionary<string, IPhysicalShape> m_Lookup = new Dictionary<string, IPhysicalShape>();

		public bool Disposed { get; private set; }

		public int Count { get { return m_Lookup.Count; } }

		public IPhysicalShape this[string name] { get { return m_Lookup[name]; } }

        public event Action<IPhysicalShape> ShapeRemoved; 

		public PhysicalShapeCollection()
		{
			Disposed = true; 
		}

		public bool Contains(string name)
		{
			return m_Lookup.ContainsKey(name);
		}

		public void Add(IPhysicalShape item)
		{
			m_Lookup.Add(item.Name, item);

            if (Disposed == false)
            {
                item.LoadResources(); 
            }
		}

		public bool Remove(IPhysicalShape item)
		{
			return Remove(item.Name);
		}

        public bool Remove(string name)
        {
            if (Contains(name) == false)
            {
                return false; 
            }

            IPhysicalShape shape = this[name]; 

            m_Lookup.Remove(name);

            if (Disposed == false)
            {
                shape.UnloadResources();

                if (ShapeRemoved != null)
                {
                    ShapeRemoved(shape); 
                }
            }

            return true; 
        }

		public void Clear()
		{
			m_Lookup.Clear(); 
		}

		public IEnumerator<IPhysicalShape> GetEnumerator()
		{
			return m_Lookup.Values.GetEnumerator();
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return (m_Lookup.Values as System.Collections.IEnumerable).GetEnumerator();
		}

		public void LoadResources()
		{
			if (Disposed == false)
			{
				return; 
			}

			foreach (IPhysicalShape shape in this)
			{
				shape.LoadResources(); 
			}

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        // CANDIDATE FOR INCREMENTAL LOAD
        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

			foreach (IPhysicalShape shape in this)
			{
				shape.UnloadResources();
			}

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
    }
}
