﻿using BulletSharp;
using OpenTK;

namespace Rug.Game.Physics
{ 
    /// <summary>
    /// Interface for physical objects.
    /// </summary>
	public interface IPhysicalObject
	{
        /// <summary>
        /// Gets the name of the IPhysicalShape for this object.
        /// </summary>
		string ShapeName { get; } 

        /// <summary>
        /// Gets or sets the RigidBody for this object.
        /// </summary>
		RigidBody Body { get; set; }


        /// <summary>
        /// Gets or sets the objects center in physical space.
        /// </summary>
		//Vector3 Center { get; set; }

        /// <summary>
        /// Gets or sets the objects rotation in physical space.
        /// </summary>
        //Quaternion Rotation { get; set; }

        /// <summary>
        /// Gets or sets the objects mass
        /// </summary>
		float Mass { get; set; }

        float LocalGravity { get; set; } 

        /// <summary>
        /// Gets or sets the objects matrix, this should include translation and rotation.
        /// </summary>
		Matrix4 ObjectMatrix { get; set; }

        CollisionFilterGroups CollisionGroup { get; set; }

        CollisionFilterGroups CollidesWithGroup { get; set; } 

        //bool IsStatic { get; set; }

        bool DebugShape { get; set; } 

        bool ShouldRemove { get; set; }

        bool IsTriggerVolume { get; set; }

        bool IsKinematic { get; set; } 

        /// <summary>
        /// Sync physics to display properties
        /// </summary>
        void UpdatePhysics(); 
	}
}
