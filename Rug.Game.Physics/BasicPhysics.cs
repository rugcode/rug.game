﻿using BulletSharp;
using OpenTK;

namespace Rug.Game.Physics
{
    public class BasicPhysics<T, C> : PhysicsBase<T, C>
        where T : IPhysicalObject
        where C : IPhysicalConstraint
    {
        protected override void OnInitialize()
        {
        }

        protected override void OnInitializePhysics()
        {
            // collision configuration contains default setup for memory, collision setup
            CollisionConfiguration = new DefaultCollisionConfiguration();

            Dispatcher = new CollisionDispatcher(CollisionConfiguration);

            Vector3 worldMin = new Vector3(-1000, -1000, -1000);
            Vector3 worldMax = new Vector3(1000, 1000, 1000);
            Broadphase = new AxisSweep3(worldMin, worldMax);
            //Broadphase = new DbvtBroadphase(); 
            Solver = new SequentialImpulseConstraintSolver();

            World = new DiscreteDynamicsWorld(Dispatcher, Broadphase, Solver, CollisionConfiguration);
            
            World.Gravity = new Vector3(0, -40, 0);

			foreach (T obj in Objects)
			{
				Matrix4 startTransform = obj.ObjectMatrix;

                IPhysicalShape physicsShape = Shapes[obj.ShapeName];
                CollisionShape shape = physicsShape.CollisionShape;

                startTransform = Matrix4.CreateTranslation(physicsShape.Center) * startTransform;

				//CollisionShape shape = Shapes[obj.ShapeName].CollisionShape;

				obj.Body = LocalCreateRigidBody(obj.Mass, obj.CollisionGroup, obj.CollidesWithGroup, startTransform, shape);
                obj.Body.UserObject = obj;

                if (obj.DebugShape != true) 
                {
                    obj.Body.CollisionFlags |= CollisionFlags.DisableVisualizeObject;
                }

                if (obj.IsTriggerVolume == true)
                {
                    obj.Body.CollisionFlags |= CollisionFlags.NoContactResponse;
                }

                if (obj.IsKinematic == true)
                {
                    obj.Body.CollisionFlags &= ~CollisionFlags.StaticObject;      
                    obj.Body.CollisionFlags |= CollisionFlags.KinematicObject; 
                }

                obj.Body.Gravity = World.Gravity * obj.LocalGravity;
			}
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
