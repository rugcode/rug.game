﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Effect;
using System;

namespace Rug.Game.Physics
{
    public class PhysicsDebugLines : IResourceManager
	{
		protected Debug_Lines Effect;

		private VertexBuffer m_Vertices;

        public int DesiredMaxCount { get; set; }

		public int MaxCount { get; set; } 

		public int InstanceCount { get; set; }

        public Color4[] Colors { get; private set; } 

        public Vector3[] Starts { get; private set; } 
        public Vector3[] Ends { get; private set; }

		public VertexBuffer Vertices { get { return m_Vertices; } }
        
        public Matrix4 ObjectMatrix { get; set; }

        public PhysicsDebugLines(int count)
		{
            Disposed = true; 
                 
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Debug_Lines"] as Debug_Lines;
			}

            ObjectMatrix = Matrix4.Identity;

            MaxCount = count; 

            Colors = new Color4[count];
            Starts = new Vector3[count];
            Ends = new Vector3[count];

            m_Vertices = new VertexBuffer("Texture Box Vertices", ResourceMode.Static, new VertexBufferInfo(DebugVertex.Format, count * 2, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));			
		}

        public void CheckResize()
        {
            if (DesiredMaxCount > MaxCount)
            {
                int value = DesiredMaxCount;
                float factor = 128.0f;

                MaxCount = (int)(Math.Max(1, Math.Round(((float)value / (float)factor) + 0.5f, MidpointRounding.AwayFromZero)) * factor);

                m_Vertices.ResourceInfo.Count = MaxCount * 2;

                if (Disposed == false)
                {
                    UnloadResources();

                    LoadResources();
                }
            }

            if (MaxCount != Colors.Length)
            {
                Colors = new Color4[MaxCount];
                Starts = new Vector3[MaxCount];
                Ends = new Vector3[MaxCount];

                InstanceCount = 0;
            }
        }

        public virtual void Update()
        {
            if (Disposed == true)
            {
                return;
            }

            if (InstanceCount == 0)
            {
                return;
            }

            DataStream stream;
            m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

            for (int i = 0; i < InstanceCount; i++)
            {
                Color4 color = Colors[i];

                stream.WriteRange(new DebugVertex[] {					
						new DebugVertex() { Position = Starts[i], Color = color }, 
						new DebugVertex() { Position = Ends[i], Color = color }, 
					});
            }

            m_Vertices.UnmapBuffer();
        }

		public virtual void Render(View3D view)
		{
            if (InstanceCount <= 0)
            {
                return;                    
            }

			Effect.Begin(ref view.World, ref view.Projection);

            Matrix4 obj = ObjectMatrix;

            Effect.Render(ref obj, m_Vertices, InstanceCount * 2);
		}

        public virtual void Render(View3D view, ref Matrix4 objectMatrix)
        {
            if (InstanceCount <= 0)
            {
                return;
            }

            Effect.Begin(ref view.World, ref view.Projection);
            Effect.Render(ref objectMatrix, m_Vertices, InstanceCount * 2);
        }

        public virtual void Render_Identity(View3D view)
        {
            if (InstanceCount <= 0)
            {
                return;
            }

            Matrix4 identity = Matrix4.Identity;

            Effect.Begin(ref identity, ref identity);

            Effect.Render(ref identity, m_Vertices, InstanceCount * 2);
        }

		#region IResourceManager Members

        public bool Disposed { get; private set; }

		public void LoadResources()
		{
			if (Disposed == false)
			{
                return; 
            }

            m_Vertices.LoadResources();

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
                return; 
            }

            m_Vertices.UnloadResources();

			Disposed = true;
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
