﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;
using Rug.Game.Flat.Models;

namespace Rug.Game.Physics.Shapes
{
    public class StaticMesh : IPhysicalShape
	{
        private TriangleIndexVertexArray m_IndexVertexArray;
        private BvhTriangleMeshShape m_GroundShape;

        public string Name { get; private set; }

        public BulletSharp.CollisionShape CollisionShape { get { return m_GroundShape; } }

		public bool Disposed { get; private set; }

        public Vector3[] Vertices { get; private set; }

        public int[] Indices { get; private set; }

        public float Scale { get; private set; }

        public Vector3 Center { get; private set; } 

        public StaticMesh(string name, ModelData originalData, float scale)
        {
            Name = name;

            ModelData data = new ModelData();

            originalData.CopyTo(data);

            //data.CullDeadVerts(); 

            //data.WeldVerts(0.3f); 

            Vertices = data.Verts.ToArray();

            Indices = new int[data.Faces.Count * 3];

            int i = 0; 
            foreach (FaceData face in data.Faces)
            {
                Indices[i++] = face.V0;
                Indices[i++] = face.V1;
                Indices[i++] = face.V2;
            }

            Scale = scale;

            Center = Vector3.Zero;

            Disposed = true;
        }

        void SetVertexPositions()
        {
            BulletSharp.DataStream vertexBuffer, indexBuffer;
            
            int numVerts, numFaces, vertexStride, indexStride;
            
            PhyScalarType vertsType, indicesType;
            
            m_IndexVertexArray.GetLockedVertexIndexData(out vertexBuffer, out numVerts, out vertsType, out vertexStride, out indexBuffer, out indexStride, out numFaces, out indicesType);

            foreach (Vector3 vert in Vertices)
            {
                vertexBuffer.Write(vert * Scale);
            }

            vertexBuffer.Dispose();
            indexBuffer.Dispose();
        }

		public void LoadResources()
        {
            if (Disposed == false)
            {
                return;
            }

            int totalVerts = Vertices.Length;
            int totalTriangles = Indices.Length / 3;

            const bool useQuantizedAabbCompression = false;
            
            m_IndexVertexArray = new TriangleIndexVertexArray();

            IndexedMesh mesh = new IndexedMesh();

            mesh.Allocate(totalVerts, Vector3.SizeInBytes, Indices.Length, sizeof(int) * 3);

            DataStream indices = mesh.LockIndices();

            foreach (int index in Indices)
            {
                indices.Write(index);
            }
            
            indices.Dispose();

            m_IndexVertexArray.AddIndexedMesh(mesh);

            SetVertexPositions();

            m_GroundShape = new BvhTriangleMeshShape(m_IndexVertexArray, useQuantizedAabbCompression, true);

            Disposed = false;
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}
            
            m_GroundShape.Dispose();
            m_IndexVertexArray.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
    }
}
