﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;

namespace Rug.Game.Physics.Shapes
{
    public enum ModelShapeMode
    {
        StaticConvave, 
        Convex, 
        ConvexContracted, 
    }

    public class ModelShape : IPhysicalShape
	{
        // TriangleMesh m_TriangleMesh;
        CollisionShape m_ConvexShape;

        public ModelShapeMode ShapeMode { get; private set; } 

        public string Name { get; private set; }

        public BulletSharp.CollisionShape CollisionShape { get { return m_ConvexShape; } }

		public bool Disposed { get; private set; }

        public ModelData Model { get; private set; } 

        public Vector3[] Vertices { get; private set; }

        public int[] Indices { get; private set; }

        public float Scale { get; private set; }

        public bool EnableSAT { get; private set; }

        public int Subdivide { get; private set; }

        public Vector3 Center { get; private set; } 

        public ModelShape(string name, ModelData model, float scale, bool enableSAT, int subdivide, ModelShapeMode mode)
		{
            Name = name;

            Model = model;
            
            Vertices = model.Verts.ToArray();

            Indices = model.GetIndices(); 

            Scale = scale;

            EnableSAT = enableSAT;

            Subdivide = subdivide;

            ShapeMode = mode;

            Center = model.Center * scale; 

			Disposed = true;
		}

		public void LoadResources()
        {
            if (Disposed == false)
            {
                return;
            }

            float margin = 0.1f; 

            switch (ShapeMode)
            {
                case ModelShapeMode.StaticConvave:
                    CreateStaticConvave(margin); 
                    break;
                case ModelShapeMode.Convex:
                    CreateConvex(margin); 
                    break;
                case ModelShapeMode.ConvexContracted:
                    CreateConvexContracted(margin); 
                    break;
                default:
                    break;
            }

            /* 
            if (m_ConvexShape != null)
            {
                m_ConvexShape.Margin = 3f; 
            }
            */ 

            Disposed = false;
        }

        private void CreateStaticConvave(float deafultMargin)
        {
            using (TriangleMesh triangleMesh = new TriangleMesh())
            {
                int[] indices = Model.GetIndices();
                List<Vector3> vertices = Model.Verts;

                for (int i = 0; i < Model.Faces.Count; i++)
                {
                    int index0 = indices[i * 3 + 1];
                    int index1 = indices[i * 3];
                    int index2 = indices[i * 3 + 2];

                    Vector3 vertex0 = (vertices[index0] * Scale) - Center;
                    Vector3 vertex1 = (vertices[index1] * Scale) - Center;
                    Vector3 vertex2 = (vertices[index2] * Scale) - Center;

                    AddTriangle(triangleMesh, vertex0, vertex1, vertex2, Subdivide);
                }
                
                BvhTriangleMeshShape shape = new BvhTriangleMeshShape(triangleMesh, false, false);

                shape.Margin = deafultMargin; 

                shape.BuildOptimizedBvh();

                m_ConvexShape = shape;
            }
        }

        private void CreateConvex(float deafultMargin)
        {
            ConvexHullShape hullSHape = new ConvexHullShape();

            foreach (Vector3 v in Model.Verts)
            {
                hullSHape.AddPoint((v * Scale) - Center);
            }

            hullSHape.Margin = deafultMargin;

            if (EnableSAT)
            {
                hullSHape.InitializePolyhedralFeatures();
            }

            m_ConvexShape = hullSHape;
        }

        private void CreateConvexContracted(float deafultMargin)
        {
            using (TriangleMesh triangleMesh = new TriangleMesh())
            {
                int[] indices = Model.GetIndices();
                List<Vector3> vertices = Model.Verts;

                for (int i = 0; i < Model.Faces.Count; i++)
                {
                    int index0 = indices[i * 3 + 1];
                    int index1 = indices[i * 3];
                    int index2 = indices[i * 3 + 2];

                    Vector3 vertex0 = (vertices[index0] * Scale) - Center;
                    Vector3 vertex1 = (vertices[index1] * Scale) - Center;
                    Vector3 vertex2 = (vertices[index2] * Scale) - Center;

                    AddTriangle(triangleMesh, vertex0, vertex1, vertex2, Subdivide);
                }

                //HeightfieldTerrainShape shape = new HeightfieldTerrainShape(10, 10, null, 10, 0, 200, 1, PhyScalarType.PhyFloat, false);
                // ConvexTriangleMeshShape // (m_TriangleMesh, true);
                using (ConvexTriangleMeshShape tempConvexShape = new ConvexTriangleMeshShape(triangleMesh, true))
                {
                    //tempConvexShape.Margin = 0;
                    //tempConvexShape.SetSafeMargin(Vector3.Zero, 0); 
                    //m_ConvexShape = tempConvexShape; 
                    //float collisionMargin = 0.01f;

                    float margin = Math.Max(deafultMargin, tempConvexShape.Margin);

                    ConvexHullShape hullSHape;

                    AlignedVector3Array planeEquations;
                    AlignedVector3Array vertsArray = new AlignedVector3Array();

                    Vector3 centroid = Vector3.Zero;

                    if (true)
                    {
                        foreach (Vector3 vertex in Model.Verts)
                        {
                            centroid += vertex;
                        }
                    }

                    centroid /= (float)Model.Verts.Count;

                    for (int i = 0; i < Model.Verts.Count; i++)
                    {

                        vertsArray.Add(Model.Verts[i] - centroid);
                    }

                    GeometryUtil.GetPlaneEquationsFromVertices(vertsArray, out planeEquations);

                    AlignedVector3Array shiftedPlaneEquations = new AlignedVector3Array();

                    for (int p = 0; p < planeEquations.Count; p++)
                    {
                        Vector3 plane = planeEquations[p];
                        //plane[2] += margin;
                        shiftedPlaneEquations.Add(plane);
                    }

                    AlignedVector3Array shiftedVertices;
                    GeometryUtil.GetVerticesFromPlaneEquations(shiftedPlaneEquations, out shiftedVertices);

                    //hullSHape = new ConvexHullShape(vertsArray); // & (shiftedVertices[0].getX()), shiftedVertices.size());
                    hullSHape = new ConvexHullShape(shiftedVertices); // & (shiftedVertices[0].getX()), shiftedVertices.size());

                    hullSHape.Margin = margin;

                    if (EnableSAT)
                    {
                        hullSHape.InitializePolyhedralFeatures();
                    }

                    m_ConvexShape = hullSHape;
                }
            } 
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }
        
        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

        private void AddTriangle(TriangleMesh mesh, Vector3 v0, Vector3 v1, Vector3 v2, int subdivide)
        {
            if (subdivide > 0)
            {

                Vector3 v0_v1 = (v0 + v1) / 2f;
                Vector3 v1_v2 = (v1 + v2) / 2f;
                Vector3 v2_v0 = (v2 + v0) / 2f;

                AddTriangle(mesh, v0, v0_v1, v2_v0, subdivide - 1);

                AddTriangle(mesh, v1, v1_v2, v0_v1, subdivide - 1);

                AddTriangle(mesh, v2, v2_v0, v1_v2, subdivide - 1);

                AddTriangle(mesh, v0_v1, v1_v2, v2_v0, subdivide - 1);            
            }
            else
            {
                mesh.AddTriangle(v0, v2, v1);
            }
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

            m_ConvexShape.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
    }
}


/* 

                TriangleMesh trimesh = new TriangleMesh();
                trimeshes.Add(trimesh);

                Vector3 localScaling = new Vector3(6, 6, 6);
                List<int> indices = wo.Indices;
                List<Vector3> vertices = wo.Vertices;

                int i;
                for (i = 0; i < tcount; i++)
                {
                    int index0 = indices[i * 3];
                    int index1 = indices[i * 3 + 1];
                    int index2 = indices[i * 3 + 2];

                    Vector3 vertex0 = vertices[index0] * localScaling;
                    Vector3 vertex1 = vertices[index1] * localScaling;
                    Vector3 vertex2 = vertices[index2] * localScaling;

                    trimesh.AddTriangle(vertex0, vertex1, vertex2);
                }

                ConvexShape tmpConvexShape = new ConvexTriangleMeshShape(trimesh);

                //create a hull approximation
                ShapeHull hull = new ShapeHull(tmpConvexShape);
                float margin = tmpConvexShape.Margin;
                hull.BuildHull(margin);
                tmpConvexShape.UserObject = hull;

                ConvexHullShape convexShape = new ConvexHullShape();
                foreach (Vector3 v in hull.Vertices)
                {
                    convexShape.AddPoint(v);
                }

                if (sEnableSAT)
                {
                    convexShape.InitializePolyhedralFeatures();
                }
                tmpConvexShape.Dispose();
                //hull.Dispose();


                CollisionShapes.Add(convexShape);

                float mass = 1.0f;

                LocalCreateRigidBody(mass, Matrix.Translation(0, 2, 14), convexShape);

                const bool useQuantization = true;
                CollisionShape concaveShape = new BvhTriangleMeshShape(trimesh, useQuantization);
                LocalCreateRigidBody(0, Matrix.Translation(convexDecompositionObjectOffset), concaveShape);

                CollisionShapes.Add(concaveShape);


                // Bullet Convex Decomposition

                FileStream outputFile = new FileStream("file_convex.obj", FileMode.Create, FileAccess.Write);
                StreamWriter writer = new StreamWriter(outputFile);

                DecompDesc desc = new DecompDesc
                {
                    mVertices = wo.Vertices.ToArray(),
                    mTcount = tcount,
                    mIndices = wo.Indices.ToArray(),
                    mDepth = 5,
                    mCpercent = 5,
                    mPpercent = 15,
                    mMaxVertices = 16,
                    mSkinWidth = 0.0f
                };

                MyConvexDecomposition convexDecomposition = new MyConvexDecomposition(writer, this);
                desc.mCallback = convexDecomposition;


                // HACD

                Hacd myHACD = new Hacd();
                myHACD.SetPoints(wo.Vertices);
                myHACD.SetTriangles(wo.Indices);
                myHACD.CompacityWeight = 0.1;
                myHACD.VolumeWeight = 0.0;

                // HACD parameters
                // Recommended parameters: 2 100 0 0 0 0
                int nClusters = 2;
                const double concavity = 100;
                //bool invert = false;
                const bool addExtraDistPoints = false;
                const bool addNeighboursDistPoints = false;
                const bool addFacesPoints = false;

                myHACD.NClusters = nClusters;                     // minimum number of clusters
                myHACD.VerticesPerConvexHull = 100;               // max of 100 vertices per convex-hull
                myHACD.Concavity = concavity;                     // maximum concavity
                myHACD.AddExtraDistPoints = addExtraDistPoints;
                myHACD.AddNeighboursDistPoints = addNeighboursDistPoints;
                myHACD.AddFacesPoints = addFacesPoints;

                myHACD.Compute();
                nClusters = myHACD.NClusters;

                myHACD.Save("output.wrl", false);


                if (true)
                {
                    CompoundShape compound = new CompoundShape();
                    CollisionShapes.Add(compound);

                    Matrix trans = Matrix.Identity;

                    for (int c = 0; c < nClusters; c++)
                    {
                        //generate convex result
                        Vector3[] points;
                        int[] triangles;
                        myHACD.GetCH(c, out points, out triangles);

                        ConvexResult r = new ConvexResult(points, triangles);
                        convexDecomposition.ConvexDecompResult(r);
                    }

                    for (i = 0; i < convexDecomposition.convexShapes.Count; i++)
                    {
                        Vector3 centroid = convexDecomposition.convexCentroids[i];
                        trans = Matrix.Translation(centroid);
                        ConvexHullShape convexShape2 = convexDecomposition.convexShapes[i] as ConvexHullShape;
                        compound.AddChildShape(trans, convexShape2);

                        RigidBody body = LocalCreateRigidBody(1.0f, trans, convexShape2);
                    }

#if true
                    mass = 10.0f;
                    trans = Matrix.Translation(-convexDecompositionObjectOffset);
                    RigidBody body2 = LocalCreateRigidBody(mass, trans, compound);
                    body2.CollisionFlags |= CollisionFlags.CustomMaterialCallback;

                    convexDecompositionObjectOffset.Z = 6;
                    trans = Matrix.Translation(-convexDecompositionObjectOffset);
                    body2 = LocalCreateRigidBody(mass, trans, compound);
                    body2.CollisionFlags |= CollisionFlags.CustomMaterialCallback;

                    convexDecompositionObjectOffset.Z = -6;
                    trans = Matrix.Translation(-convexDecompositionObjectOffset);
                    body2 = LocalCreateRigidBody(mass, trans, compound);
                    body2.CollisionFlags |= CollisionFlags.CustomMaterialCallback;
#endif
                }

                writer.Dispose();
                outputFile.Dispose();
*/