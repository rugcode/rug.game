﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Physics.Shapes
{
    public class CompoundFromHullDataShape : IPhysicalShape
	{
        // TriangleMesh m_TriangleMesh;
        CompoundShape m_CompundShape;

        public string Name { get; private set; }

        public BulletSharp.CollisionShape CollisionShape { get { return m_CompundShape; } }

		public bool Disposed { get; private set; }

        public ModelData Model { get; private set; } 

        public float Scale { get; private set; }

        public Vector3 Center { get; private set; }

        public CompoundFromHullDataShape(string name, ModelData model, float scale)
		{
            Name = name;

            Model = model;           

            Scale = scale;

            Center = model.Center * scale; 

			Disposed = true;
		}

		public void LoadResources()
        {
            if (Disposed == false)
            {
                return;
            }

            Create(); 

            Disposed = false;
        }

        private void Create() 
        {
            m_CompundShape = new CompoundShape();

            foreach (HullData hull in Model.ConvexHullData)
            {
                AlignedVector3Array vertices = new AlignedVector3Array();

                foreach (FaceData face in hull.Faces)
                {

                    vertices.Add((Model.Verts[face.V0] - hull.Center) * Scale);
                    vertices.Add((Model.Verts[face.V1] - hull.Center) * Scale);
                    vertices.Add((Model.Verts[face.V2] - hull.Center) * Scale);
                }

                Matrix4 transform = Matrix4.CreateTranslation(hull.Center * Scale - Center); 

                ConvexHullShape shape = new ConvexHullShape(vertices);

                shape.Margin = 0.09f;

                m_CompundShape.AddChildShape(transform, shape);
            }
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }
        
        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

            m_CompundShape.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
    }
}
