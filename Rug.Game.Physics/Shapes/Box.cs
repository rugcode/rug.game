﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;

namespace Rug.Game.Physics.Shapes
{
	public class Box : IPhysicalShape
	{
        public static string GetName(float size)
        {
            return GetName(new Vector3(size));
        }

        public static string GetName(Vector3 size) 
        {
            return "Box " + Helper.SerializeVector3(size); 
        }

		public string Name { get { return GetName(Size); } }

		public BulletSharp.CollisionShape CollisionShape { get; private set; }

		public bool Disposed { get; private set; }

        public Vector3 Size { get; private set; }

        public Vector3 Center { get; private set; } 

        public Box(float size)
            : this(new Vector3(size))
        {            
        }

        public Box(float x, float y, float z)
            : this(new Vector3(x, y, z))
        {

        }

        public Box(Vector3 size)
		{
            Size = size;
            
            Center = Vector3.Zero; 

			Disposed = true;
		}

		public void LoadResources()
		{
			if (Disposed == false)
			{
				return; 
			}

            CollisionShape = new BoxShape(Size);

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

			CollisionShape.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
	}
}
