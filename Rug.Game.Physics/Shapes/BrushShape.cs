﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;

namespace Rug.Game.Physics.Shapes
{
    public class BrushShape : IPhysicalShape
	{
        CollisionShape m_ConvexShape;
        
        public string Name { get; private set; }

        public Vector3[] Vertices { get; private set; }         

        public BulletSharp.CollisionShape CollisionShape { get { return m_ConvexShape; } }

		public bool Disposed { get; private set; }

        public float Scale { get; private set; }

        public Vector3 Center { get; private set; } 

        public BrushShape(string name, Vector3[] verts, float scale)
		{
            Name = name;

            Vertices = verts; 

            Scale = scale;

            Center = Vector3.Zero; 

			Disposed = true;
		}

		public void LoadResources()
        {
            if (Disposed == false)
            {
                return;
            }
            
            AlignedVector3Array vertices = new AlignedVector3Array();

            foreach (Vector3 vert in Vertices)
            {
                vertices.Add(vert * Scale); 
            }

            m_ConvexShape = new ConvexHullShape(vertices);

            Disposed = false;
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

            m_ConvexShape.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
    }
}
