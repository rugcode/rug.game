﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;
using System.Globalization;

namespace Rug.Game.Physics.Shapes
{
	public class Sphere : IPhysicalShape
	{
        public static string GetName(float radius)
        {
            return "Sphere " + radius.ToString(CultureInfo.InvariantCulture);
        }

        public string Name { get { return GetName(Radius); } }

		public BulletSharp.CollisionShape CollisionShape { get; private set; }

		public bool Disposed { get; private set; }

        public float Radius { get; private set; }

        public Vector3 Center { get; private set; } 

		public Sphere(float radius)
		{
            Radius = radius;

            Center = Vector3.Zero; 

			Disposed = true;
		}

		public void LoadResources()
		{
			if (Disposed == false)
			{
				return; 
			}

            CollisionShape = new SphereShape(Radius);

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

			CollisionShape.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
	}
}
