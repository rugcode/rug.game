﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Physics.Shapes
{
    public class CompoundFromHullPrimitivesShape : IPhysicalShape
	{
        // TriangleMesh m_TriangleMesh;
        CompoundShape m_CompundShape;

        public string Name { get; private set; }

        public BulletSharp.CollisionShape CollisionShape { get { return m_CompundShape; } }

		public bool Disposed { get; private set; }

        public ModelData Model { get; private set; } 

        public float Scale { get; private set; }

        public Vector3 Center { get; private set; } 

        public CompoundFromHullPrimitivesShape(string name, ModelData model, float scale)
		{
            Name = name;

            Model = model;           

            Scale = scale;

            Center = model.Center * scale; 

			Disposed = true;
		}

		public void LoadResources()
        {
            if (Disposed == false)
            {
                return;
            }

            Create(); 

            Disposed = false;
        }

        private void Create() 
        {
            m_CompundShape = new CompoundShape();

            foreach (ModelPrimitive primitive in Model.HullPrimitives)
            {
                Matrix4 transform = primitive.Transform;

                ConvexShape shape = null; 

                switch (primitive.PrimitiveType)
	            {
		            case PrimitiveType.Sphere:
                        shape = new SphereShape(Math.Abs(primitive.Scale.X * Scale));
                        break;
                    case PrimitiveType.Capsule:
                        shape = new CapsuleShape(Math.Abs(primitive.Scale.X * Scale), Math.Abs(primitive.Scale.Y * Scale));
                        break;
                    case PrimitiveType.Cylinder:
                        shape = new CylinderShape(Math.Abs(primitive.Scale.X * Scale), Math.Abs(primitive.Scale.Y * Scale), Math.Abs(primitive.Scale.Z * Scale));
                        break;
                    case PrimitiveType.Cone:
                        shape = new ConeShape(Math.Abs(primitive.Scale.X * Scale), Math.Abs(primitive.Scale.Y * Scale));
                        break;
                    case PrimitiveType.Box:
                        shape = new BoxShape(Math.Abs(primitive.Scale.X * Scale), Math.Abs(primitive.Scale.Y * Scale), Math.Abs(primitive.Scale.Z * Scale));
                        break;
                    default:
                        break;
	            }

                if (shape != null)
                {
                    //shape.Margin = shape.Margin + 0.25f;
                    m_CompundShape.AddChildShape(transform, shape);
                }
            }
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }
        
        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

            m_CompundShape.Dispose(); 

			Disposed = true;
		}

		public void Dispose()
		{
			UnloadResources();
		}
    }
}
