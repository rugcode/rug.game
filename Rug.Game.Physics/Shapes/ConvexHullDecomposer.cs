﻿using BulletSharp;
using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Flat.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Rug.Game.Physics.Shapes
{ 
    /*
    class ConvexResult
    {
        public ConvexResult()
        {
        }

        public ConvexResult(Vector3[] hvertices, int[] hindices)
        {
            mHullVertices = (Vector3[])hvertices.Clone();
            mHullIndices = (int[])hindices.Clone();
        }

        public ConvexResult(ConvexResult r)
        {
            mHullVertices = (Vector3[])r.mHullVertices.Clone();
            mHullIndices = (int[])r.mHullIndices.Clone();
        }

        // the convex hull.
        public Vector3[] mHullVertices;
        public int[] mHullIndices;

        /*
        float mHullVolume;		    // the volume of the convex hull.

        Vector3 mOBBSides;			  // the width, height and breadth of the best fit OBB
        Vector3 mOBBCenter;      // the center of the OBB
        Quaternion mOBBOrientation; // the quaternion rotation of the OBB.
        Matrix mOBBTransform;  // the 4x4 transform of the OBB.
        float mOBBVolume;         // the volume of the OBB

        float mSphereRadius;      // radius and center of best fit sphere
        Vector3 mSphereCenter;
        float mSphereVolume;      // volume of the best fit sphere
        * /
    };

    abstract class IConvexDecomposition
    {
        public void ConvexDebugTri(float[] p1, float[] p2, float[] p3, uint color) { }
        public void ConvexDebugPoint(float[] p, float dist, uint color) { }
        public void ConvexDebugBound(float[] bmin, float[] bmax, uint color) { }
        public void ConvexDebugOBB(float[] sides, float[] matrix, uint color) { }

        public abstract void ConvexDecompResult(ConvexResult result);
    };

    // just to avoid passing a zillion parameters to the method the
    // options are packed into this descriptor.
    class DecompDesc
    {
        public DecompDesc()
        {
            mDepth = 5;
            mCpercent = 5;
            mPpercent = 5;
            mMaxVertices = 32;
        }

        // describes the input triangle.
        public Vector3[] mVertices;
        public int mTcount;   // the number of triangles in the source mesh.
        public int[] mIndices;  // the indexed triangle list array (zero index based)

        // options
        public int mDepth;    // depth to split, a maximum of 10, generally not over 7.
        public float mCpercent; // the concavity threshold percentage.  0=20 is reasonable.
        public float mPpercent; // the percentage volume conservation threshold to collapse hulls. 0-30 is reasonable.

        // hull output limits.
        public int mMaxVertices; // maximum number of vertices in the output hull. Recommended 32 or less.
        public float mSkinWidth;   // a skin width to apply to the output hulls.

        public IConvexDecomposition mCallback; // the interface to receive back the results.
    };

    class ConvexDecomposition
    {

    }

 
    class MyConvexDecomposition : IConvexDecomposition
    {        
        public ModelData Model = new ModelData(); 

        public MyConvexDecomposition(ConvexHullDecomposer demo)
        {
        }

        public override void ConvexDecompResult(ConvexResult result)
        {
            Model = new ModelData(); 

            foreach (Vector3 p in result.mHullVertices)
            {
                Model.Verts.Add(p); 
            }

            int[] src = result.mHullIndices;
            for (int i = 0; i < src.Length; i += 3)
            {
                int index0 = src[i];
                int index1 = src[i + 1];
                int index2 = src[i + 2];

                Model.Faces.Add(new FaceData() 
                {
                    Group = -1,
                    Color = Color4.White, 
                    V0 = index0, 
                    V1 = index1, 
                    V2 = index2, 
                }); 

                /* 
                Vector3 vertex0 = result.mHullVertices[index0] * localScaling - demo.centroid;
                Vector3 vertex1 = result.mHullVertices[index1] * localScaling - demo.centroid;
                Vector3 vertex2 = result.mHullVertices[index2] * localScaling - demo.centroid;

                trimesh.AddTriangle(vertex0, vertex1, vertex2);

                index0 += mBaseCount;
                index1 += mBaseCount;
                index2 += mBaseCount;

                output.WriteLine("f {0} {1} {2}", index0 + 1, index1 + 1, index2 + 1);
                * / 
            }

            //TriangleMesh trimesh = new TriangleMesh();
            //demo.trimeshes.Add(trimesh);

            /* 
            Vector3 localScaling = new Vector3(6.0f, 6.0f, 6.0f);

            output.WriteLine("## Hull Piece {0} with {1} vertices and {2} triangles.", mHullCount, result.mHullVertices.Length, result.mHullIndices.Length / 3);

            output.WriteLine("usemtl Material{0}", mBaseCount);
            output.WriteLine("o Object{0}", mBaseCount);

            foreach (Vector3 p in result.mHullVertices)
            {
                output.WriteLine(string.Format(floatFormat, "v {0:F9} {1:F9} {2:F9}", p.X, p.Y, p.Z));
            }

            //calc centroid, to shift vertices around center of mass
            demo.centroid = Vector3.Zero;

            AlignedVector3Array vertices = new AlignedVector3Array();
            if (true)
            {
                foreach (Vector3 vertex in result.mHullVertices)
                {
                    demo.centroid += vertex * localScaling;
                }
            }

            demo.centroid /= (float)result.mHullVertices.Length;

            if (true)
            {
                foreach (Vector3 vertex in result.mHullVertices)
                {
                    vertices.Add(vertex * localScaling - demo.centroid);
                }
            }

            if (true)
            {            
                int[] src = result.mHullIndices;
                for (int i = 0; i < src.Length; i += 3)
                {
                    int index0 = src[i];
                    int index1 = src[i + 1];
                    int index2 = src[i + 2];

                    Vector3 vertex0 = result.mHullVertices[index0] * localScaling - demo.centroid;
                    Vector3 vertex1 = result.mHullVertices[index1] * localScaling - demo.centroid;
                    Vector3 vertex2 = result.mHullVertices[index2] * localScaling - demo.centroid;

                    trimesh.AddTriangle(vertex0, vertex1, vertex2);

                    index0 += mBaseCount;
                    index1 += mBaseCount;
                    index2 += mBaseCount;

                    output.WriteLine("f {0} {1} {2}", index0 + 1, index1 + 1, index2 + 1);
                }
            }

            //this is a tools issue: due to collision margin, convex objects overlap, compensate for it here:
            //#define SHRINK_OBJECT_INWARDS 1
#if SHRINK_OBJECT_INWARDS

			float collisionMargin = 0.01f;
					
			btAlignedObjectArray<btVector3> planeEquations;
			btGeometryUtil::getPlaneEquationsFromVertices(vertices,planeEquations);

			btAlignedObjectArray<btVector3> shiftedPlaneEquations;
			for (int p=0;p<planeEquations.size();p++)
			{
				btVector3 plane = planeEquations[p];
				plane[3] += collisionMargin;
				shiftedPlaneEquations.push_back(plane);
			}
			btAlignedObjectArray<btVector3> shiftedVertices;
			btGeometryUtil::getVerticesFromPlaneEquations(shiftedPlaneEquations,shiftedVertices);

					
			btConvexHullShape* convexShape = new btConvexHullShape(&(shiftedVertices[0].getX()),shiftedVertices.size());
					
#else //SHRINK_OBJECT_INWARDS

            ConvexHullShape convexShape = new ConvexHullShape(vertices);
#endif

            if (demo.sEnableSAT == true)
            {
                convexShape.InitializePolyhedralFeatures();
            }

            convexShape.Margin = 0.01f;
            convexShapes.Add(convexShape);
            convexCentroids.Add(demo.centroid);
            demo.CollisionShapes.Add(convexShape);
            mBaseCount += result.mHullVertices.Length; // advance the 'base index' counter.
            * /
        }
    }
     * */ 

    public class ConvexHullDecomposer
    {
        public ModelData Original; 
        public readonly List<ModelData> Results = new List<ModelData>();

        private Color4[] m_ClusterColors = new Color4[]
        {
            Color4.DarkCyan, 
            Color4.Maroon, 
            Color4.LawnGreen,
            Color4.Honeydew, 
            Color4.HotPink, 
            Color4.Orange,
            Color4.Aquamarine, 
            Color4.Indigo,
        };

        public ConvexHullDecomposer(ModelData original)
        {
            Original = original; 
        }

        public void Decompose()
        {
            /* 
            Vector3[] vertices = Original.Verts.ToArray();

            int trianglesCount = Original.Faces.Count;
            int[] indices = new int[Original.Faces.Count * 3];

            int index = 0;

            foreach (FaceData face in Original.Faces)
            {
                indices[index++] = face.V1;
                indices[index++] = face.V0;
                indices[index++] = face.V2;
            }

            Results.Clear(); 

            // HACD
            Hacd hacd = new Hacd();
            hacd.SetPoints(vertices);
            hacd.SetTriangles(indices);
            hacd.CompacityWeight = 0.1;
            hacd.VolumeWeight = 0; //  1.0;

            // HACD parameters
            // Recommended parameters: 2 100 0 0 0 0
            int nClusters = 1;
            const double concavity = 1; // 100;

            //bool invert = false;
            const bool addExtraDistPoints = false;
            const bool addNeighboursDistPoints = false;
            const bool addFacesPoints = false;

            hacd.NClusters = nClusters;                     // minimum number of clusters
            hacd.VerticesPerConvexHull = 512; // vertices.Length;   // max of 100 vertices per convex-hull
            hacd.Concavity = concavity;                     // maximum concavity
            hacd.AddExtraDistPoints = addExtraDistPoints;
            hacd.AddNeighboursDistPoints = addNeighboursDistPoints;
            hacd.AddFacesPoints = addFacesPoints;
            
            hacd.Compute();
            nClusters = hacd.NClusters;
            
            //hacd.Save(Helper.ResolvePath("~/output.wrl"), false);

            for (int c = 0; c < nClusters; c++)
            {
                //generate convex result
                Vector3[] points;
                int[] triangles;
                hacd.GetCH(c, out points, out triangles);

                ModelData model = new ModelData();

                foreach (Vector3 p in points)
                {
                    model.Verts.Add(p);
                }

                for (int i = 0; i < triangles.Length; i += 3)
                {
                    //int index0 = triangles[i + 1];
                    //int index1 = triangles[i];
                    //int index2 = triangles[i + 2];

                    int index0 = triangles[i];
                    int index1 = triangles[i + 1];
                    int index2 = triangles[i + 2];

                    model.Faces.Add(new FaceData()
                    {
                        Group = -1,
                        Color = m_ClusterColors[c % m_ClusterColors.Length],
                        V0 = index0,
                        V1 = index1,
                        V2 = index2,
                    });
                }

                model.CalcuateFaceNormals(); 

                Results.Add(model);
            }
            */
        }
    }
}
