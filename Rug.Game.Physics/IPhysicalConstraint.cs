﻿using BulletSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Physics
{
    public interface IPhysicalConstraint
    {
        bool ShouldRemove { get; set; }

        void CreateConstraints(DynamicsWorld world); 

        void RemoveConstraints(DynamicsWorld world); 
    }
}
