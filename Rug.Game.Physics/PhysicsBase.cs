﻿using BulletSharp;
using OpenTK;
using Rug.Cmd;
using Rug.Game.Core.Resources;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Rug.Game.Physics
{
    public abstract class PhysicsBase<T, C> : IResourceManager 
        where T : IPhysicalObject 
        where C : IPhysicalConstraint
    {
        // Physics
        private DynamicsWorld m_World;
        
        private ManualResetEvent m_ObjectsChanging = new ManualResetEvent(true); 

        public DynamicsWorld World { get { return m_World; } protected set { m_World = value; } }

        protected CollisionConfiguration CollisionConfiguration;
        protected CollisionDispatcher Dispatcher;
        protected BroadphaseInterface Broadphase;
        protected ConstraintSolver Solver;

		protected AlignedCollisionShapeArray CollisionShapes { get; private set; }

		public readonly PhysicalShapeCollection Shapes = new PhysicalShapeCollection();

		public readonly List<T> Objects = new List<T>();

        public readonly List<C> Constraints = new List<C>(); 

        public float Speed { get; set; }
        public int Iterations { get; set; } 

		public bool Disposed { get; private set; }

		public PhysicsBase()
        {
            Speed = 2f; //  1.5f;
            Iterations = 10; // 12; 

			Disposed = true;

            Shapes.ShapeRemoved += Shapes_ShapeRemoved;

			CollisionShapes = new AlignedCollisionShapeArray(); 
        }

        void Shapes_ShapeRemoved(IPhysicalShape obj)
        {
            if (m_World != null)
            {
                RC.WriteLine("Begin: ShapeRemoved - " + obj.Name); 

                CollisionShapes.Remove(obj.CollisionShape);

                RC.WriteLine("End: ShapeRemoved - " + obj.Name); 
            }
        }

        protected virtual void OnInitialize() { }

        protected abstract void OnInitializePhysics();        

        public virtual void ExitPhysics()
        {
            RC.WriteLine("Begin: ExitPhysics"); 

            if (m_World != null)
            {
                RC.WriteLine(" -- World.NumConstraints"); 
                // remove / dispose constraints
				for (int i = m_World.NumConstraints - 1; i >= 0; i--)
                {
                    TypedConstraint constraint = m_World.GetConstraint(i);
                    
					m_World.RemoveConstraint(constraint);

                    constraint.Dispose();
                }

                RC.WriteLine(" -- World.NumCollisionObjects "); 
                // remove the rigidbodies from the dynamics world and delete them
                for (int i = m_World.NumCollisionObjects - 1; i >= 0; i--)
                {
                    if (m_World.CollisionObjectArray.Count <= i)
                    {
                        continue; 
                    }

                    CollisionObject obj = m_World.CollisionObjectArray[i];
                    RigidBody body = obj as RigidBody;
                    if (body != null && body.MotionState != null)
                    {
                        body.MotionState.Dispose();
                    }
                    m_World.RemoveCollisionObject(obj);
                    obj.Dispose();
                }

                RC.WriteLine(" -- CollisionShapes"); 
                if (CollisionShapes.Count > 0)
                {
                    // delete collision shapes
                    foreach (CollisionShape shape in CollisionShapes)
                    {
                        if (shape.IsDisposed == true)
                        {
                            continue; 
                        }

                        shape.Dispose();
                    }

                    CollisionShapes.Clear();
                }

                RC.WriteLine(" -- World.Dispose"); 
                m_World.Dispose();

                m_World = null;

                RC.WriteLine(" -- Broadphase.Dispose"); 
                Broadphase.Dispose();

                RC.WriteLine(" -- Dispatcher.Dispose"); 
                Dispatcher.Dispose();

                RC.WriteLine(" -- CollisionConfiguration.Dispose"); 
                CollisionConfiguration.Dispose();
            }

            RC.WriteLine("End: ExitPhysics"); 

            /*
            if (Broadphase != null)
            {
                Broadphase.Dispose();
            }

            if (Dispatcher != null)
            {
                Dispatcher.Dispose();
            }

            if (CollisionConfiguration != null)
            {
                CollisionConfiguration.Dispose();
            }
            */ 
        }

        public virtual void Update()
        {
			Update(Rug.Game.Environment.FrameDelta);
        }

        public virtual void Update(float delta)
        {
            bool changeState = false;

            changeState |= RemoveDeadConstraints(); 

            changeState |= AddNewObjects();

            changeState |= RemoveDeadObjects();

            changeState |= AddNewConstraints(); 

            if (changeState == true)
            {
                m_ObjectsChanging.Set();
            }

            try
            {
                m_World.StepSimulation(delta * Speed, Iterations);
            }
            catch
            {

            }
        }

        public void ForceWait()
        {
            m_ObjectsChanging.Reset(); 
        }

        public void WaitForObjects()
        {
            m_ObjectsChanging.WaitOne(); 
        }

        public void ForceSetWait()
        {
            m_ObjectsChanging.Set();
        }

        public virtual bool AddNewObjects()
        {
            bool result = false; 

            foreach (T obj in Objects)
            {
                if (obj.Body != null || obj.ShouldRemove == true)
                {
                    continue;
                }

                m_ObjectsChanging.Reset();

                Matrix4 startTransform = obj.ObjectMatrix;

                IPhysicalShape physicsShape = Shapes[obj.ShapeName]; 
                CollisionShape shape = physicsShape.CollisionShape;

                startTransform = Matrix4.CreateTranslation(physicsShape.Center) * startTransform;

                obj.Body = LocalCreateRigidBody(obj.Mass, obj.CollisionGroup, obj.CollidesWithGroup, startTransform, shape);
                obj.Body.UserObject = obj;

                if (obj.DebugShape != true)
                {
                    obj.Body.CollisionFlags |= CollisionFlags.DisableVisualizeObject;
                }

                if (obj.IsTriggerVolume == true)
                {
                    obj.Body.CollisionFlags |= CollisionFlags.NoContactResponse;
                }

                if (obj.IsKinematic == true)
                {

                    obj.Body.CollisionFlags &= ~CollisionFlags.StaticObject;      
                    obj.Body.CollisionFlags |= CollisionFlags.KinematicObject;
                    //obj.Body.CollisionFlags &= ~CollisionFlags.StaticObject;
                    //obj.Body.CollisionFlags = CollisionFlags.KinematicObject;
                }

                obj.Body.Gravity = World.Gravity * obj.LocalGravity;

                result = true; 
            }

            return result; 
        }

        public virtual bool RemoveDeadObjects()
        {
            bool result = false; 

            for (int i = Objects.Count - 1; i >= 0; i--)
            {
                T obj = Objects[i];

                if (obj.ShouldRemove == true)                
                {
                    m_ObjectsChanging.Reset();
                    
                    Remove(obj);

                    result = true; 
                }
            }

            return result; 
        }


        public virtual bool AddNewConstraints()
        {
            bool result = false;

            foreach (C con in Constraints)
            {
                if (con.ShouldRemove == true)
                {
                    continue;
                }

                m_ObjectsChanging.Reset();

                con.CreateConstraints(m_World); 

                result = true;
            }

            return result; 
        }

        public virtual bool RemoveDeadConstraints()
        {
            bool result = false;

            for (int i = Constraints.Count - 1; i >= 0; i--)
            {
                C con = Constraints[i];
                
                if (con.ShouldRemove == true)
                {
                    m_ObjectsChanging.Reset();

                    Remove(con);

                    result = true;
                }
            }

            return result; 
        }

        public virtual void Remove(T obj)
        {
            Objects.Remove(obj);

            if (obj.Body != null)
            {
                RigidBody body = obj.Body as RigidBody;

                if (body != null && body.MotionState != null)
                {
                    body.MotionState.Dispose();
                }

                m_World.RemoveCollisionObject(obj.Body);
                body.Dispose();
                obj.Body = null;
            }
        }

        public virtual void Remove(C obj)
        {
            Constraints.Remove(obj);

            obj.RemoveConstraints(m_World); 
        }

        public void Dispose()
        {
			UnloadResources();

            GC.SuppressFinalize(this);
        }

        public virtual RigidBody LocalCreateRigidBody(float mass, CollisionFilterGroups filter, CollisionFilterGroups mask, Matrix4 startTransform, CollisionShape shape)
        {
            //rigidbody is dynamic if and only if mass is non zero, otherwise static
            bool isDynamic = (mass != 0.0f);

            Vector3 localInertia = Vector3.Zero;
			
			if (isDynamic)
			{
				shape.CalculateLocalInertia(mass, out localInertia);
			}

            //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
            DefaultMotionState myMotionState = new DefaultMotionState(startTransform);

            RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
            RigidBody body = new RigidBody(rbInfo);
            rbInfo.Dispose();

            body.Friction = 1f; // 1f;
            
            if (isDynamic == false)
            {
                //body.IsStaticObject = true; 
                body.CollisionFlags |= CollisionFlags.StaticObject;                
            }
            else
            {
                body.RollingFriction = 1f;
                body.SetAnisotropicFriction(shape.AnisotropicRollingFrictionDirection, AnisotropicFrictionFlags.AnisotropicRollingFriction);
            }


            m_World.AddRigidBody(body, filter, mask);

            return body;
        }

		public void LoadResources()
		{
			if (Disposed == false)
			{
				return; 
			}

			OnInitialize();

			Shapes.LoadResources();

			foreach (IPhysicalShape shape in Shapes)
			{
				CollisionShapes.Add(shape.CollisionShape); 
			}            

			OnInitializePhysics();

			Disposed = false;
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (Disposed == true)
			{
				return;
			}

			ExitPhysics();

			Shapes.UnloadResources(); 

			CollisionShapes.Clear(); 

			Disposed = true;
		}
	}
}
