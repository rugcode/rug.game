﻿using BulletSharp;
using OpenTK;
using System.Collections.Generic;

namespace Rug.Game.Physics.Bsp
{
    /* 
    public class BspToBulletConverter : BspConverter
    {
        public readonly List<CollisionShape> Shapes = new List<CollisionShape>(); 

        public override void AddConvexVerticesCollider(AlignedVector3Array vertices, bool isEntity, Vector3 entityTargetLocation)
        {
            // perhaps we can do something special with entities (isEntity)
            // like adding a collision Triggering (as example)

            if (vertices.Count == 0)
                return;

            CollisionShape shape = new ConvexHullShape(vertices);

            Shapes.Add(shape);

            // demo.LocalCreateRigidBody(mass, startTransform, shape);
        }

        public void Create<T>(PhysicsBase<T> world) where T : IPhysicalObject
        {
            foreach (CollisionShape shape in Shapes) 
            {
                float mass = 0.0f;
            
                // can use a shift
                Matrix4 startTransform = Matrix4.CreateTranslation(0, 0, -10.0f);

                world.LocalCreateRigidBody(mass, startTransform, shape); 
            }
        }
    }
    */ 

    /* 
    class BspDemo : Demo
    {
        Vector3 eye = new Vector3(10, 10, 10);
        Vector3 target = new Vector3(0, 0, 0);

        protected override void OnInitialize()
        {
            Freelook.Up = Vector3.UnitZ;
            Freelook.SetEyeTarget(eye, target);

            Graphics.SetFormText("BulletSharp - Quake BSP Physics Viewer");
            Graphics.SetInfoText("Move using mouse and WASD+shift\n" +
                "F3 - Toggle debug\n" +
                //"F11 - Toggle fullscreen\n" +
                "Space - Shoot box");
        }

        protected override void OnInitializePhysics()
        {
            // collision configuration contains default setup for memory, collision setup
            CollisionConf = new DefaultCollisionConfiguration();
            Dispatcher = new CollisionDispatcher(CollisionConf);

            Broadphase = new DbvtBroadphase();
            Solver = new SequentialImpulseConstraintSolver();

            World = new DiscreteDynamicsWorld(Dispatcher, Broadphase, Solver, CollisionConf);
            World.Gravity = Freelook.Up * -10.0f;

            BspLoader bspLoader = new BspLoader();
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length == 1)
            {
                bspLoader.LoadBspFile("data/BspDemo.bsp");
            }
            else
            {
                bspLoader.LoadBspFile(args[1]);
            }
            BspConverter bsp2Bullet = new BspToBulletConverter(this);
            bsp2Bullet.ConvertBsp(bspLoader, 0.1f);
        }
    }

    static class Program
    {
        [STAThread]
        static void Main()
        {
            using (Demo demo = new BspDemo())
            {
                LibraryManager.Initialize(demo);
            }
        }
    }
    */ 
}
