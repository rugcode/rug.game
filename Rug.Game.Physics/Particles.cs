﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Effect;
using System;

namespace Rug.Game.Core.Simple
{	
	public class Particles : IResourceManager
	{
		static Simple_Particles Effect; 

		private bool m_Disposed = true;
		private string m_ParticleTexturePath;
		private Texture2D m_ParticleTexture;

		public Texture2D ParticleTexture
		{
			get { return m_ParticleTexture; }			
		}

		private VertexBuffer m_BillboardVertices;
		private InstanceBuffer m_Vertices;
		private VertexArrayObject m_VAO;
        private float m_ParticleScale;

        public int DesiredMaxCount { get; set; }

        public int MaxCount { get; set; }

        public int InstanceCount { get; set; }

        public float ParticleScale { get; set; }

		public InstanceBuffer Vertices { get { return m_Vertices; } }

        public BasicVertex[] Instances { get; private set; } 

        public Particles(string particleTexture, int maxCount)
            : this(particleTexture, maxCount, BufferUsageHint.DynamicDraw)
        {
        }

        public Particles(string particleTexture, int maxCount, BufferUsageHint usage)
		{
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Simple_Particles"] as Simple_Particles; 
			}

			m_ParticleTexturePath = particleTexture; 
			MaxCount = maxCount;

			m_ParticleTexture = new BitmapTexture2D("Particle Texture", m_ParticleTexturePath, Resources.ResourceMode.Static, new Texture2DInfo()
			{
				 Border = 0, 
				 InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba, 
				 MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear, 
				 MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear, 
				 PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba, 
				 PixelType = OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, 
				 Size = new System.Drawing.Size(32, 32),
				 WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				 WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});

			m_BillboardVertices = new VertexBuffer("Particle Billboard Vertices", ResourceMode.Static, new VertexBufferInfo(SimpleVertex.Format, 4, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

            m_Vertices = new InstanceBuffer("Particle Instance Vertices", ResourceMode.Dynamic, new InstanceBufferInfo(BasicVertex.Format, MaxCount, usage));

			m_VAO = new VertexArrayObject("Particles", ResourceMode.Dynamic, new IBuffer[] { m_BillboardVertices, m_Vertices });

            Instances = new BasicVertex[MaxCount]; 
		}

        public void CheckResize()
        {
            if (DesiredMaxCount > MaxCount)
            {
                int value = DesiredMaxCount;
                float factor = 128.0f;

                MaxCount = (int)(Math.Max(1, Math.Round(((float)value / (float)factor) + 0.5f, MidpointRounding.AwayFromZero)) * factor);

                m_Vertices.ResourceInfo.Count = MaxCount;

                if (Disposed == false)
                {
                    UnloadResources();

                    LoadResources();
                }
            }

            if (MaxCount != Instances.Length)
            {
                Instances = new BasicVertex[MaxCount];

                InstanceCount = 0;
            }
        }

        public virtual void Update()
        {
            if (Disposed == true)
            {
                return;
            }

            if (InstanceCount == 0)
            {
                return;
            }

            DataStream stream;
            m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

            stream.WriteRange(Instances, 0, InstanceCount);

            m_Vertices.UnmapBuffer();
        }

		public void Render(View3D view)
		{
            if (ParticleScale != m_ParticleScale)
            {
                WriteBillboard(); 
            }

			Matrix4 viewProjWorld = view.View * view.Projection;
			Matrix4 objectMatrix = Matrix4.Identity;

            Effect.Render(ref objectMatrix, ref view.View, ref view.Projection, m_ParticleTexture, m_VAO, InstanceCount);
		}

        public void Render(View3D view, ref Matrix4 objectMatrix)
        {
            if (ParticleScale != m_ParticleScale)
            {
                WriteBillboard();
            }

            Matrix4 viewProjWorld = view.View * view.Projection;

            Effect.Render(ref objectMatrix, ref view.View, ref view.Projection, m_ParticleTexture, m_VAO, InstanceCount);
        }

        public void Render_Centered(View3D view, Matrix4 objectMatrix)
        {
            if (ParticleScale != m_ParticleScale)
            {
                WriteBillboard();
            }

            Matrix4 worldMatrix = view.World;

            worldMatrix = worldMatrix.ClearTranslation();

            Matrix4 viewProjWorld = view.View * view.Projection;

            Effect.Render(ref objectMatrix, ref worldMatrix, ref view.Projection, m_ParticleTexture, m_VAO, InstanceCount);
        }


        public void Render_Identity(View3D view)
        {
            if (ParticleScale != m_ParticleScale)
            {
                WriteBillboard();
            }

            Matrix4 objectMatrix = Matrix4.Identity;
            Matrix4 pixelScale = Matrix4.CreateScale((float)view.Viewport.Height / (float)view.Viewport.Width, 1f, 1f);

            Effect.Render(ref objectMatrix, ref objectMatrix, ref pixelScale, m_ParticleTexture, m_VAO, InstanceCount);
        }

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{			
				m_ParticleTexture.LoadResources();

				m_VAO.LoadResources();

                WriteBillboard();

				#region Instances

				//m_Instances.LoadResources(); 

				#endregion

				m_Disposed = false; 
			}
		}

        private void WriteBillboard()
        {
            m_ParticleScale = ParticleScale; 

            float minX, maxX, minY, maxY, depth;

            minX = -ParticleScale;
            maxX = ParticleScale;

            minY = -ParticleScale;
            maxY = ParticleScale;

            depth = 0f;

            Color4 color = Color4.White;

            float minU, maxU, minV, maxV;

            minU = 0;
            maxU = 1;
            minV = 0;
            maxV = 1;

            #region Billboard

            DataStream stream;
            m_BillboardVertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

            stream.WriteRange(new SimpleVertex[] {					
						new SimpleVertex() { Position = new Vector3(maxX, minY, depth), TextureCoords =  new Vector2(maxU, minV), Color = color }, 
						new SimpleVertex() { Position = new Vector3(minX, minY, depth), TextureCoords =  new Vector2(minU, minV), Color = color }, 
						new SimpleVertex() { Position = new Vector3(maxX, maxY, depth), TextureCoords = new Vector2(maxU, maxV), Color = color },  
						new SimpleVertex() { Position = new Vector3(minX, maxY, depth), TextureCoords =  new Vector2(minU, maxV), Color = color } 
					});

            m_BillboardVertices.UnmapBuffer();

            #endregion
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_ParticleTexture.UnloadResources();

				m_VAO.UnloadResources(); 

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}	 
}
