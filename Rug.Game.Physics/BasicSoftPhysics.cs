﻿using BulletSharp;
using BulletSharp.SoftBody;
using OpenTK;

namespace Rug.Game.Physics
{
    public class BasicSoftPhysics<T, C> : PhysicsBase<T, C> 
        where T : IPhysicalObject
        where C : IPhysicalConstraint
    {
        public SoftBodyWorldInfo SoftBodyWorldInfo { get; private set; }

        public SoftRigidDynamicsWorld SoftWorld
        {
            get { return World as SoftRigidDynamicsWorld; }
        }

        protected override void OnInitialize()
        {
        }

        protected override void OnInitializePhysics()
        {
            // collision configuration contains default setup for memory, collision setup
            CollisionConfiguration = new SoftBodyRigidBodyCollisionConfiguration();
            
            Dispatcher = new CollisionDispatcher(CollisionConfiguration);

            Vector3 worldMin = new Vector3(-6000, -6000, -6000);
            Vector3 worldMax = new Vector3(6000, 6000, 6000);
            //Broadphase = new AxisSweep3_32Bit(worldMin, worldMax);
            Broadphase = new AxisSweep3(worldMin, worldMax);
            //Broadphase = new DbvtBroadphase(); 
            //Broadphase = new DbvtBroadphase();
            Solver = new SequentialImpulseConstraintSolver();
            
            SoftBodyWorldInfo = new SoftBodyWorldInfo
            {
                AirDensity = 1.2f,
                WaterDensity = 100,
                WaterOffset = 0,
                WaterNormal = -Vector3.UnitY,
                Gravity = new Vector3(0, -40, 0),
                Dispatcher = Dispatcher,
                Broadphase = Broadphase
            };
            SoftBodyWorldInfo.SparseSdf.Initialize();
            
            World = new SoftRigidDynamicsWorld(Dispatcher, Broadphase, Solver, CollisionConfiguration);
                        
            //World.Gravity = new Vector3(0, -10, 0);
            //World.DispatchInfo.EnableSpu = true;
            //World.DispatchInfo.UseContinuous = false;
            //World.DispatchInfo.EnableSatConvex = true;
            //World.DispatchInfo.AllowedCcdPenetration = 3;

            /* 
            CollisionShape shape = new StaticPlaneShape(Vector3.UnitY, -0.5f);
            CollisionShapes.Add(shape);
			LocalCreateRigidBody(0, Matrix4.CreateTranslation(0, -200f, 0), shape);
            */ 

            AddNewObjects();
        }

        public override void Update()
        {
            SoftBodyWorldInfo.SparseSdf.GarbageCollect();

            base.Update();
        }

        public override void Update(float delta)
        {
            SoftBodyWorldInfo.SparseSdf.GarbageCollect();

            base.Update(delta);
        }

        /* 
        public override void Remove(T obj)
        {
            base.Remove(obj); 

            // need to deal with soft bodys
            /* 
            Objects.Remove(obj);

            if (obj.Body != null)
            {
                World.RemoveCollisionObject(obj.Body);
                obj.Body = null;
            }
            * / 
        }
        */ 
    }
}
