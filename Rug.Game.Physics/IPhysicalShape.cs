﻿using BulletSharp;
using OpenTK;
using Rug.Game.Core.Resources;

namespace Rug.Game.Physics
{
    /// <summary>
    ///  Interface for physical shapes.
    /// </summary>
	public interface IPhysicalShape : IResourceManager
	{		
        /// <summary>
        /// Gets the name of this shape.
        /// </summary>
		string Name { get; }

        /// <summary>
        /// Gets the resolved collision shape.
        /// </summary>
		CollisionShape CollisionShape { get; }

        Vector3 Center { get; } 
	}
}
