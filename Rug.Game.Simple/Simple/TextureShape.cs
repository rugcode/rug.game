﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Effect;

namespace Rug.Game.Core.Simple
{
	public class TextureShape : IResourceManager
	{
		protected Simple_Box[] Effects;

		private Texture2D m_Texture;
		private VertexBuffer m_Vertices;

		private bool m_Disposed = true;
		private System.Drawing.RectangleF m_CurrentTextureRectangle;
		private bool m_CurrentFlipHorizontal = false;
		private bool m_CurrentFlipVertical = false;
		private float m_CurrentDepth = -1f;
		private Color4 m_CurrentColor = new Color4(1f, 1f, 1f, 1f);

		public Texture2D Texture { get { return m_Texture; } set { m_Texture = value; } }
		public VertexBuffer Vertices { get { return m_Vertices; } }

		public System.Drawing.PointF[] Points = new System.Drawing.PointF[4];
		public bool PointsChanged = true; 

		public System.Drawing.RectangleF TextureRectangle = new System.Drawing.RectangleF(0, 0, 1, 1);

		public Color4 Color = new Color4(1f, 1f, 1f, 1f);

		public float Depth;

		public bool FlipHorizontal = false;
		public bool FlipVertical = false;

		public TextureShape()
		{
			if (Effects == null)
			{
                Effects = new Simple_Box[3];

                for (int i = 0; i < 3; i++)
                {
                    Effects[i] = SharedEffects.Effects[Simple_Box.GetName((BoxMode)i)] as Simple_Box;
                }
			}

			m_Vertices = new VertexBuffer("Texture Box Vertices", ResourceMode.Static, new VertexBufferInfo(SimpleVertex.Format, 4, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));
		}

		public virtual void Render(View3D view)
		{
			CheckAndWriteRectangle();

            Effects[(int)BoxMode.Textured].Render(OpenTK.Graphics.OpenGL.PrimitiveType.TriangleStrip, 4, m_Texture, m_Vertices);
		}

        public virtual void Render(View3D view, Color4 color, bool invert)
        {
            CheckAndWriteRectangle();

            Effects[(int)BoxMode.TexturedColor].Render(OpenTK.Graphics.OpenGL.PrimitiveType.TriangleStrip, 4, m_Texture, color, invert ? new Vector2(-1f, 1f) : new Vector2(1f, 0f), m_Vertices);
        }

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				m_Vertices.LoadResources();

				WriteRectangle();

				m_Disposed = false;
			}
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		protected void CheckAndWriteRectangle()
		{
			if (FlipHorizontal != m_CurrentFlipHorizontal ||
				FlipVertical != m_CurrentFlipVertical ||
				TextureRectangle != m_CurrentTextureRectangle ||
				PointsChanged == true || 
				Depth != m_CurrentDepth ||
				Color != m_CurrentColor)
			{
				WriteRectangle();
			}
		}

		private void WriteRectangle()
		{
			PointsChanged = false; 

			m_CurrentFlipHorizontal = FlipHorizontal;
			m_CurrentFlipVertical = FlipVertical;

			m_CurrentTextureRectangle = TextureRectangle;

			m_CurrentDepth = Depth;

			m_CurrentColor = Color;
			
			float minU = m_CurrentTextureRectangle.Left, minV = m_CurrentTextureRectangle.Bottom, maxU = m_CurrentTextureRectangle.Right, maxV = m_CurrentTextureRectangle.Top;

			if (FlipVertical == true)
			{
				float t = minV;
				minV = maxV;
				maxV = t;
			}

			if (FlipHorizontal == true)
			{
				float t = minU;
				minU = maxU;
				maxU = t;
			}

			DataStream stream;
			m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(new SimpleVertex[] {					
						new SimpleVertex() { Position = new Vector3(Points[0].X, Points[0].Y, m_CurrentDepth), TextureCoords =  new Vector2(maxU, minV), Color = m_CurrentColor }, 
						new SimpleVertex() { Position = new Vector3(Points[1].X, Points[1].Y, m_CurrentDepth), TextureCoords =  new Vector2(minU, minV), Color = m_CurrentColor }, 
						new SimpleVertex() { Position = new Vector3(Points[2].X, Points[2].Y, m_CurrentDepth), TextureCoords = new Vector2(maxU, maxV), Color = m_CurrentColor },  
						new SimpleVertex() { Position = new Vector3(Points[3].X, Points[3].Y, m_CurrentDepth), TextureCoords =  new Vector2(minU, maxV), Color = m_CurrentColor } 
					});

			m_Vertices.UnmapBuffer();
		}

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Vertices.UnloadResources();

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources();
		}

		#endregion
	}
}
