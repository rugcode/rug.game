﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Effect;

namespace Rug.Game.Core.Simple
{	
	public class ParticleRender : IResourceManager
	{
		static Simple_Particles Effect; 

		private bool m_Disposed = true;
		private string m_ParticleTexturePath;
		private Texture2D m_ParticleTexture;

		public Texture2D ParticleTexture
		{
			get { return m_ParticleTexture; }			
		}

		private VertexBuffer m_BillboardVertices;
		private InstanceBuffer m_Instances;
		private VertexArrayObject m_VAO; 

		private int m_MaxCount = 100;
		private int m_InstanceCount = 0;

		private float m_ParticleScale = 1f;

		public int MaxCount { get { return m_MaxCount; } set { m_MaxCount = value; } }
		public int InstanceCount { get { return m_InstanceCount; } set { m_InstanceCount = value; } }

		public float ParticleScale { get { return m_ParticleScale; } set { m_ParticleScale = value; } }

		public InstanceBuffer Instances { get { return m_Instances; } }

        public ParticleRender(string particleTexture, int maxCount)
            : this(particleTexture, maxCount, BufferUsageHint.DynamicDraw)
        {
        }

		public ParticleRender(string particleTexture, int maxCount, BufferUsageHint usage)
		{
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Simple_Particles"] as Simple_Particles; 
			}

			m_ParticleTexturePath = particleTexture; 
			m_MaxCount = maxCount;

			m_ParticleTexture = new BitmapTexture2D("Particle Texture", m_ParticleTexturePath, Resources.ResourceMode.Static, new Texture2DInfo()
			{
				 Border = 0, 
				 InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba, 
				 MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear, 
				 MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.LinearMipmapLinear, 
				 PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba, 
				 PixelType = OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, 
				 Size = new System.Drawing.Size(32, 32),
				 WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				 WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
                 AnisotropicFiltering = AnisotropicFiltering.Maximum, 
                 MipMap = true, 
			});

			m_BillboardVertices = new VertexBuffer("Particle Billboard Vertices", ResourceMode.Static, new VertexBufferInfo(SimpleVertex.Format, 4, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

            m_Instances = new InstanceBuffer("Particle Instance Vertices", ResourceMode.Dynamic, new InstanceBufferInfo(BasicVertex.Format, m_MaxCount, usage));

			m_VAO = new VertexArrayObject("Particles", ResourceMode.Dynamic, new IBuffer[] { m_BillboardVertices, m_Instances });
		}

		public virtual void Update(View3D view)
		{

		}

		public void Render(View3D view)
		{
			GLState.EnableDepthTest = false;
			GLState.EnableDepthMask = false;
			GLState.EnableCullFace = false;
			GLState.EnableAlphaTest = false;
			GLState.EnableBlend = true;
			GLState.BlendFunc(OpenTK.Graphics.OpenGL.BlendingFactorSrc.One, OpenTK.Graphics.OpenGL.BlendingFactorDest.One);
			GLState.BlendEquation(OpenTK.Graphics.OpenGL.BlendEquationMode.FuncAdd); 
			GLState.Apply(view); 

			Matrix4 viewProjWorld = view.View * view.Projection;
			Matrix4 objectMatrix = Matrix4.Identity;

			Effect.Render(ref objectMatrix, ref view.View, ref view.Projection, m_ParticleTexture, m_VAO, m_InstanceCount);
		}

        public void Render(View3D view, bool testDepth)
        {
            GLState.EnableDepthTest = testDepth;
            GLState.EnableDepthMask = false;
            GLState.EnableCullFace = false;
            GLState.EnableAlphaTest = false;
            GLState.EnableBlend = true;
            GLState.BlendFunc(OpenTK.Graphics.OpenGL.BlendingFactorSrc.One, OpenTK.Graphics.OpenGL.BlendingFactorDest.One);
            GLState.BlendEquation(OpenTK.Graphics.OpenGL.BlendEquationMode.FuncAdd);
            GLState.Apply(view);

            Matrix4 viewProjWorld = view.View * view.Projection;
            Matrix4 objectMatrix = Matrix4.Identity;

            Effect.Render(ref objectMatrix, ref view.View, ref view.Projection, m_ParticleTexture, m_VAO, m_InstanceCount);
        }

        public void Render_Centered(View3D view, Matrix4 objectMatrix)
        {
            GLState.EnableDepthTest = false;
            GLState.EnableDepthMask = false;
            GLState.EnableCullFace = false;
            GLState.EnableAlphaTest = false;
            GLState.EnableBlend = true;
            GLState.BlendFunc(OpenTK.Graphics.OpenGL.BlendingFactorSrc.One, OpenTK.Graphics.OpenGL.BlendingFactorDest.One);
            GLState.BlendEquation(OpenTK.Graphics.OpenGL.BlendEquationMode.FuncAdd);
            GLState.Apply(view);

            Matrix4 worldMatrix = view.World;

            worldMatrix = worldMatrix.ClearTranslation();

            Matrix4 viewProjWorld = view.View * view.Projection;

            Effect.Render(ref objectMatrix, ref worldMatrix, ref view.Projection, m_ParticleTexture, m_VAO, m_InstanceCount);
        }

        public void Render_Centered(View3D view, Matrix4 objectMatrix, bool testDepth)
        {
            GLState.EnableDepthTest = testDepth;
            GLState.EnableDepthMask = false;
            GLState.EnableCullFace = false;
            GLState.EnableAlphaTest = false;
            GLState.EnableBlend = true;
            GLState.BlendFunc(OpenTK.Graphics.OpenGL.BlendingFactorSrc.One, OpenTK.Graphics.OpenGL.BlendingFactorDest.One);
            GLState.BlendEquation(OpenTK.Graphics.OpenGL.BlendEquationMode.FuncAdd);
            GLState.Apply(view);

            Matrix4 worldMatrix = view.World;

            worldMatrix = worldMatrix.ClearTranslation();

            Matrix4 viewProjWorld = view.View * view.Projection;

            Effect.Render(ref objectMatrix, ref worldMatrix, ref view.Projection, m_ParticleTexture, m_VAO, m_InstanceCount);
        }

        public void Render_Identity(View3D view)
        {
            GLState.EnableDepthTest = false;
            GLState.EnableDepthMask = false;
            GLState.EnableCullFace = false;
            GLState.EnableAlphaTest = false;
            GLState.EnableBlend = true;
            GLState.BlendFunc(OpenTK.Graphics.OpenGL.BlendingFactorSrc.One, OpenTK.Graphics.OpenGL.BlendingFactorDest.One);
            GLState.BlendEquation(OpenTK.Graphics.OpenGL.BlendEquationMode.FuncAdd);
            GLState.Apply(view);

            Matrix4 objectMatrix = Matrix4.Identity;
            Matrix4 pixelScale = Matrix4.CreateScale((float)view.Viewport.Height / (float)view.Viewport.Width, 1f, 1f);

            Effect.Render(ref objectMatrix, ref objectMatrix, ref pixelScale, m_ParticleTexture, m_VAO, m_InstanceCount);
        }

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{			
				m_ParticleTexture.LoadResources();

				m_VAO.LoadResources(); 

				float minX, maxX, minY, maxY, depth;

				minX = -ParticleScale;
				maxX = ParticleScale;

				minY = -ParticleScale;
				maxY = ParticleScale; 

				depth = 0f;

				Color4 color = Color4.White;

				float minU, maxU, minV, maxV;

				minU = 0;
				maxU = 1;
				minV = 0;
				maxV = 1; 

				#region Billboard

				DataStream stream;
				m_BillboardVertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

				stream.WriteRange(new SimpleVertex[] {					
						new SimpleVertex() { Position = new Vector3(maxX, minY, depth), TextureCoords =  new Vector2(maxU, minV), Color = color }, 
						new SimpleVertex() { Position = new Vector3(minX, minY, depth), TextureCoords =  new Vector2(minU, minV), Color = color }, 
						new SimpleVertex() { Position = new Vector3(maxX, maxY, depth), TextureCoords = new Vector2(maxU, maxV), Color = color },  
						new SimpleVertex() { Position = new Vector3(minX, maxY, depth), TextureCoords =  new Vector2(minU, maxV), Color = color } 
					});

				m_BillboardVertices.UnmapBuffer();
				
				#endregion

				#region Instances

				//m_Instances.LoadResources(); 

				#endregion

				m_Disposed = false; 
			}
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_ParticleTexture.UnloadResources();

				m_VAO.UnloadResources(); 

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}	 
}
