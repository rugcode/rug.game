﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Effect;

namespace Rug.Game.Core.Simple.Debug
{
	public class DebugLines : IResourceManager
	{
		protected Debug_Lines Effect;

		private VertexBuffer m_Vertices;

        public int Count { get; private set; } 

        public Color4[] Colors { get; private set; } 

        public Vector3[] Starts { get; private set; } 
        public Vector3[] Ends { get; private set; }

		public VertexBuffer Vertices { get { return m_Vertices; } }
        
        public Matrix4 ObjectMatrix { get; set; }
		
        public DebugLines(int count)
		{
            Disposed = true; 
                 
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Debug_Lines"] as Debug_Lines;
			}

            ObjectMatrix = Matrix4.Identity;

            Count = count; 

            Colors = new Color4[count];
            Starts = new Vector3[count];
            Ends = new Vector3[count];

            m_Vertices = new VertexBuffer("Texture Box Vertices", ResourceMode.Static, new VertexBufferInfo(DebugVertex.Format, count * 2, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));			
		}

		public virtual void Render(View3D view)
		{
            WriteAxis();

			Effect.Begin(ref view.World, ref view.Projection);

            Matrix4 obj = ObjectMatrix;

            Effect.Render(ref obj, m_Vertices, Count * 2);
		}

		#region IResourceManager Members

        public bool Disposed { get; private set; }

		public void LoadResources()
		{
			if (Disposed == true)
			{				
				m_Vertices.LoadResources();

				WriteAxis(); 		

				Disposed = false;
			}
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		private void WriteAxis()
		{
			DataStream stream;
			m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

            for (int i = 0; i < Count; i++)
            {
                Color4 color = Colors[i]; 

                stream.WriteRange(new DebugVertex[] {					
						new DebugVertex() { Position = Starts[i], Color = color }, 
						new DebugVertex() { Position = Ends[i], Color = color }, 
					});
            }

			m_Vertices.UnmapBuffer(); 
		}

		public void UnloadResources()
		{
			if (Disposed == false)
			{
				m_Vertices.UnloadResources();

				Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
