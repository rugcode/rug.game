﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Effect;

namespace Rug.Game.Core.Simple.Debug
{
	public class DebugCross : IResourceManager
	{
		protected Debug_Lines Effect;

		private VertexBuffer m_Vertices;		

		private bool m_Disposed = true;
		private float m_Size = 2f;
		private float m_SizeCurrent = 0;

		public float Size
		{
			get { return m_Size; }
			set { m_Size = value; }
		}

		public VertexBuffer Vertices { get { return m_Vertices; } }

		public Matrix4 ObjectMatrix { get; set; }

		public DebugCross()
		{
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Debug_Lines"] as Debug_Lines;
			}

			ObjectMatrix = Matrix4.Identity; 

			m_Vertices = new VertexBuffer("Texture Box Vertices", ResourceMode.Static, new VertexBufferInfo(DebugVertex.Format, 6, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));			
		}

		public virtual void Render(View3D view)
		{
			CheckAndWriteCross();

			Effect.Begin(ref view.World, ref view.Projection);

			Matrix4 mat = ObjectMatrix;

			Effect.Render(ref mat, m_Vertices);
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{				
				m_Vertices.LoadResources();

				WriteCross(); 		

				m_Disposed = false;
			}
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		protected void CheckAndWriteCross()
		{
			if (Size != m_SizeCurrent)
			{
				WriteCross();
			}
		}

		private void WriteCross()
		{
			m_SizeCurrent = Size;
			
			DataStream stream;
			m_Vertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

			stream.WriteRange(new DebugVertex[] {					
						new DebugVertex() { Position = Vector3.UnitX * -m_SizeCurrent, Color = Color4.Red }, 
						new DebugVertex() { Position = Vector3.UnitX * m_SizeCurrent, Color = Color4.Red }, 
						
						new DebugVertex() { Position = Vector3.UnitY * -m_SizeCurrent, Color = Color4.Green },  
						new DebugVertex() { Position = Vector3.UnitY * m_SizeCurrent, Color = Color4.Green }, 
						
						new DebugVertex() { Position = Vector3.UnitZ * -m_SizeCurrent, Color = Color4.Blue },
						new DebugVertex() { Position = Vector3.UnitZ * m_SizeCurrent, Color = Color4.Blue } 
					});

			m_Vertices.UnmapBuffer(); 
		}

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_Vertices.UnloadResources();

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
