﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;

namespace Rug.Game.Core.Simple.Debug
{
	public class DebugFrustum : IResourceManager
	{
		DebugLines m_Lines; 

        //public Matrix4 ObjectMatrix { get; set; }
        //public Matrix4 ProjectionMatrix { get; set; }

        public ViewFrustum Frustum { get; set; }

        public Color4 Color { get; set; } 

        public DebugFrustum()
		{
            Disposed = true;

            Color = Color4.Red; 

            m_Lines = new DebugLines(12); 
            m_Lines.ObjectMatrix = Matrix4.Identity; 
		}

		public virtual void Render(View3D view)
		{
            WriteAxis();

            m_Lines.Render(view); 
		}

		#region IResourceManager Members

        public bool Disposed { get; private set; }

		public void LoadResources()
		{
			if (Disposed == true)
			{
                m_Lines.LoadResources();

				WriteAxis(); 		

				Disposed = false;
			}
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		private void WriteAxis()
		{
            if (Frustum == null)
            {
                return; 
            }

            int i = 0;

            Color4 color = Color;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NTL;
            m_Lines.Ends[i] = Frustum.NTR;
            i++; 

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NBL;
            m_Lines.Ends[i] = Frustum.NBR;
            i++; 

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NTL;
            m_Lines.Ends[i] = Frustum.NBL;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NTR;
            m_Lines.Ends[i] = Frustum.NBR;
            i++;




            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.FTL;
            m_Lines.Ends[i] = Frustum.FTR;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.FBL;
            m_Lines.Ends[i] = Frustum.FBR;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.FTL;
            m_Lines.Ends[i] = Frustum.FBL;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.FTR;
            m_Lines.Ends[i] = Frustum.FBR;
            i++;



            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NTR;
            m_Lines.Ends[i] = Frustum.FTR;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NBR;
            m_Lines.Ends[i] = Frustum.FBR;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NTL;
            m_Lines.Ends[i] = Frustum.FTL;
            i++;

            m_Lines.Colors[i] = color;
            m_Lines.Starts[i] = Frustum.NBL;
            m_Lines.Ends[i] = Frustum.FBL;
            i++; 
        }

		public void UnloadResources()
		{
			if (Disposed == false)
			{
                m_Lines.UnloadResources(); 

				Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}
}
