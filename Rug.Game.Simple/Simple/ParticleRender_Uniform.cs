﻿using OpenTK;
using OpenTK.Graphics;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using Rug.Game.Effect;

namespace Rug.Game.Core.Simple
{	
	public class ParticleRender_Uniform : IResourceManager
	{
		static Simple_Particles_Uniform Effect; 

		private bool m_Disposed = true;
		private string m_ParticleTexturePath;
		private Texture2D m_ParticleTexture;

		private VertexBuffer m_BillboardVertices;
		private UniformBuffer m_Instances;

		private int m_MaxCount = 100;
		private int m_InstanceCount = 0;

		private float m_ParticleScale = 1f;

		public int MaxCount { get { return m_MaxCount; } set { m_MaxCount = value; } }
		public int InstanceCount { get { return m_InstanceCount; } set { m_InstanceCount = value; } }

		public float ParticleScale { get { return m_ParticleScale; } set { m_ParticleScale = value; } }

		public Color4 Color { get; set; } 

		public UniformBuffer Instances { get { return m_Instances; } }

		public ParticleRender_Uniform(string particleTexture, int maxCount)
		{
			if (Effect == null)
			{
				Effect = SharedEffects.Effects["Simple_Particles_Uniform"] as Simple_Particles_Uniform; 
			}
			
			Color = Color4.White; 

			m_ParticleTexturePath = particleTexture; 
			m_MaxCount = maxCount;

			m_ParticleTexture = new BitmapTexture2D("Particle Texture", m_ParticleTexturePath, Resources.ResourceMode.Static, new Texture2DInfo()
			{
				 Border = 0, 
				 InternalFormat = OpenTK.Graphics.OpenGL.PixelInternalFormat.Rgba, 
				 MagFilter = OpenTK.Graphics.OpenGL.TextureMagFilter.Linear, 
				 MinFilter = OpenTK.Graphics.OpenGL.TextureMinFilter.Linear, 
				 PixelFormat = OpenTK.Graphics.OpenGL.PixelFormat.Rgba, 
				 PixelType = OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, 
				 Size = new System.Drawing.Size(32, 32),
				 WrapS = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
				 WrapT = OpenTK.Graphics.OpenGL.TextureWrapMode.ClampToEdge,
			});

			m_BillboardVertices = new VertexBuffer("Particle Billboard Vertices", ResourceMode.Static, new VertexBufferInfo(SimpleVertex.Format, 4, OpenTK.Graphics.OpenGL.BufferUsageHint.StaticDraw));

			m_Instances = new UniformBuffer("Particle Instance Vertices", ResourceMode.Dynamic, new UniformBufferInfo(BasicTypes.Vector4.Format, m_MaxCount, OpenTK.Graphics.OpenGL.BufferUsageHint.StreamRead)); 
		}

		public virtual void Update(View3D view)
		{

		}

		public void Render(View3D view)
		{
			Matrix4 viewProjWorld = (view.World * view.View) * view.Projection;

			Effect.Render(m_ParticleTexture, m_BillboardVertices, m_Instances, m_InstanceCount);
		}

		#region IResourceManager Members

		public bool Disposed
		{
			get { return m_Disposed; }
		}

		public void LoadResources()
		{
			if (m_Disposed == true)
			{
				m_ParticleTexture.LoadResources();

				float minX, maxX, minY, maxY, depth;

				minX = -ParticleScale;
				maxX = ParticleScale;

				minY = -ParticleScale;
				maxY = ParticleScale; 

				depth = 0f;

				Color4 color = Color;

				float minU, maxU, minV, maxV;

				minU = 0;
				maxU = 1;
				minV = 0;
				maxV = 1; 

				#region Billboard

				m_BillboardVertices.LoadResources(); 

				DataStream stream;
				m_BillboardVertices.MapBuffer(OpenTK.Graphics.OpenGL.BufferAccess.WriteOnly, out stream);

				stream.WriteRange(new SimpleVertex[] {					
						new SimpleVertex() { Position = new Vector3(maxX, minY, depth), TextureCoords =  new Vector2(maxU, minV), Color = color }, 
						new SimpleVertex() { Position = new Vector3(minX, minY, depth), TextureCoords =  new Vector2(minU, minV), Color = color }, 
						new SimpleVertex() { Position = new Vector3(maxX, maxY, depth), TextureCoords = new Vector2(maxU, maxV), Color = color },  
						new SimpleVertex() { Position = new Vector3(minX, maxY, depth), TextureCoords =  new Vector2(minU, maxV), Color = color } 
					});

				m_BillboardVertices.UnmapBuffer(); 

				#endregion

				#region Instances

				m_Instances.LoadResources(); 

				#endregion

				m_Disposed = false; 
			}
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			if (m_Disposed == false)
			{
				m_ParticleTexture.UnloadResources();

				m_BillboardVertices.UnloadResources();
				m_Instances.UnloadResources(); 

				m_Disposed = true;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			UnloadResources(); 
		}

		#endregion
	}	 
}
