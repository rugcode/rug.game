﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;

namespace Rug.Game.Core.MeshData
{
	public class MeshBatch : IResource
	{
		private Mesh m_Mesh;
		private InstanceBuffer m_InstanceBuffer;
		private VertexArrayObject m_VAO;

		private int m_InstanceCount;
		private int m_MaxInstanceCount;

		private string m_Name;
		private ResourceMode m_ResourceMode;

		private MeshInstance[] m_Instances;
		public bool IsVisible = true; 

		#region Public Properties

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return m_ResourceMode; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return m_VAO.ResourceInfo; }
		}

		public uint ResourceHandle
		{
			get { return m_VAO.ResourceHandle; }
		}

		public bool IsLoaded
		{
			get { return m_VAO.IsLoaded; }
		}

		public int InstanceCount
		{
			get { return m_InstanceCount; }
			set { m_InstanceCount = value; }
		}

		public int MaxInstanceCount
		{
			get { return m_MaxInstanceCount; }
		}

		public Mesh Mesh
		{
			get { return m_Mesh; }
		}

		public InstanceBuffer InstanceBuffer
		{
			get { return m_InstanceBuffer; }
		}

		public VertexArrayObject VAO
		{
			get { return m_VAO; }
		}

		public MeshInstance[] Instances
		{
			get { return m_Instances; }
		} 

		

		#endregion

		public MeshBatch(string name, ResourceMode mode, Mesh mesh, int maxInstanceCount, BufferUsageHint instanceUsage)
		{
			m_Mesh = mesh; 
			m_ResourceMode = mode;
			m_MaxInstanceCount = maxInstanceCount;
			m_InstanceCount = 0; 

			m_InstanceBuffer = new InstanceBuffer(name + " Instances", mode, new InstanceBufferInfo(MeshInstance.Format, maxInstanceCount, instanceUsage));

			m_VAO = new VertexArrayObject(name + " VAO", mode, new IBuffer[] { mesh.Vertices, mesh.Indices, m_InstanceBuffer });

			m_Instances = new MeshInstance[maxInstanceCount];
		}

		public void LoadResources()
		{
			m_VAO.LoadResources();
			
			m_Mesh.UploadData(); 
		}

		public void UnloadResources()
		{
			m_VAO.UnloadResources();
		}

		public void UploadData()
		{
			DataStream stream;
			
			m_InstanceBuffer.MapBuffer(BufferAccess.WriteOnly, out stream);

			stream.WriteRange(m_Instances);

			m_InstanceBuffer.UnmapBuffer(); 
		}
	}
}
