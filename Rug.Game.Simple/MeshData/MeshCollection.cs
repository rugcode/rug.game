﻿using Rug.Game.Core.Resources;

namespace Rug.Game.Core.MeshData
{
	public class MeshCollection : ResourceSet<Mesh>
	{
		public MeshCollection(string name, ResourceMode mode) : base(name, mode)
		{

		}
	}

	public class MeshBatchCollection : ResourceSet<MeshBatch>
	{
		public MeshBatchCollection(string name, ResourceMode mode)
			: base(name, mode)
		{

		}
	} 
}
