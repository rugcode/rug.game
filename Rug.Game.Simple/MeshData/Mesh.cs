﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;

namespace Rug.Game.Core.MeshData
{
	public class Mesh : IResource
	{
		private bool m_IsLoaded = false;
		
		private string m_Name;

		private VertexBuffer m_Vertices;
		private IndexBuffer m_Indices;

		private MeshVertex[] m_VertsData;
		private ushort[] m_IndicesData; 

		#region IResource Members

		public Matrix4 ObjectMatrix = Matrix4.Identity;
		public Matrix4 NormalMatrix = Matrix4.Identity;

		public Texture2D DiffuseTexture;
		public Texture2D NormalTexture;
		public Texture2D MaterialTexture;
		public Texture2D MaterialLookup;

		public float FloatID { get; set; }
		
		public bool IsVisible = true;

		public int IndexCount 
		{ 
			get { return m_Indices.ResourceInfo.Count; } 
		} 

		public DrawElementsType IndexType 
		{ 
			get { return m_Indices.IndexType; } 
		} 

		public string Name
		{
			get { return m_Name; }
		}

		public ResourceType ResourceType
		{
			get { return ResourceType.Custom; }
		}

		public ResourceMode ResourceMode
		{
			get { return ResourceMode.Static; }
		}

		public IResourceInfo ResourceInfo
		{
			get { return null; }
		}

		public uint ResourceHandle
		{
			get { return 0; }
		}

		public bool IsLoaded
		{
			get { return m_IsLoaded; }
		}

		public VertexBuffer Vertices
		{
			get { return m_Vertices; }
		}

		public IndexBuffer Indices
		{
			get { return m_Indices; }
		} 

		#endregion

		public Mesh(string name, MeshVertex[] verts, ushort[] indices)
		{
			m_Name = name;

			m_VertsData = verts; 
			m_IndicesData = indices; 

			m_Vertices = new VertexBuffer(m_Name + " Vertices", Resources.ResourceMode.Static, new VertexBufferInfo(MeshVertex.Format, verts.Length, BufferUsageHint.StaticDraw));
			m_Indices = new IndexBuffer(m_Name + " Indices", Resources.ResourceMode.Static, new IndexBufferInfo(DrawElementsType.UnsignedShort, sizeof(ushort), indices.Length, BufferUsageHint.StaticDraw)); 
		}

		public void LoadResources()
		{
			if (m_IsLoaded == false)
			{
				m_Vertices.LoadResources();

				m_Indices.LoadResources();

				UploadData();

				m_IsLoaded = true;
			}
		}

		public void UploadData()
		{
			DataStream stream;
			m_Vertices.MapBuffer(BufferAccess.WriteOnly, out stream);
			stream.WriteRange(m_VertsData);
			m_Vertices.UnmapBuffer();

			m_Indices.MapBuffer(BufferAccess.WriteOnly, out stream);
			stream.WriteRange(m_IndicesData);
			m_Indices.UnmapBuffer(); 
		}

		public void UnloadResources()
		{
			if (m_IsLoaded == true)
			{
				m_Vertices.UnloadResources();
				m_Indices.UnloadResources(); 

				m_IsLoaded = false;
			}	
		}

		public void Bind()
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, m_Vertices.ResourceHandle);
			MeshVertex.Bind();

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_Indices.ResourceHandle);
		}

		public void Unbind()
		{
			MeshVertex.Unbind();
		}		
	}
}
