﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Rug.Game.Core.MeshData
{
	public class ObjLoader
	{
		public static void LoadObject(string path, Vector4 material, out MeshVertex[] verts, out ushort[] indicies)
		{
			IEnumerable<string> lines = FileHelper.ReadAllLines(path);

			LoadObject(lines, material, out verts, out indicies);
		}

		public static void LoadObject(byte[] data, Vector4 material, out MeshVertex[] verts, out ushort[] indicies)
		{
			using (MemoryStream stream = new MemoryStream(data))
			using (StreamReader reader = new StreamReader(stream))
			{
				string[] lines = reader.ReadToEnd().Split(new string[] { "\n", System.Environment.NewLine }, StringSplitOptions.None); 

				LoadObject(lines, material, out verts, out indicies);
			}			
		}

		public static void LoadObject(IEnumerable<string> lines, Vector4 material, out MeshVertex[] verts, out ushort[] indicies)
		{
			List<Vector3> v = new List<Vector3>();
			List<Vector3> n = new List<Vector3>();
			List<Vector2> t = new List<Vector2>();

			List<string> faces = new List<string>(); 

			foreach (string line in lines)
			{
				if (line.StartsWith("#") == true)
				{
					continue;
				}

				if (String.IsNullOrEmpty(line.Trim()) == true)
				{
					continue; 
				}

				if (line.StartsWith("v ") == true)
				{
					v.Add(ParseVector3(line.Substring(2))); 
				}
				else if (line.StartsWith("vn ") == true)
				{
					n.Add(ParseVector3(line.Substring(3)));
				}
				else if (line.StartsWith("vt ") == true)
				{
					t.Add(ParseVector2(line.Substring(3)));
				}
				else if (line.StartsWith("f ") == true)
				{
					faces.Add(line.Substring(2)); 
				}
			}

			List<MeshVertex> vertBuilder = new List<MeshVertex>();
			List<ushort> i = new List<ushort>();
			Color4 color = new Color4(1f, 1f, 1f, 0f); 
			ushort index = 0; 

			foreach (string face in faces)
			{
				string[] faceParts = face.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

				if (faceParts.Length == 3)
				{
					int vi0, ni0, ti0;
					ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

					int vi1, ni1, ti1;
					ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

					int vi2, ni2, ti2;
					ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

					MeshVertex v1 = new MeshVertex()
					{
						Position = v[vi0 - 1],
						Normal = n[ni0 - 1],
						TextureCoords = t[ti0 - 1],
						Color = color,
						Material = material,
					};

					MeshVertex v2 = new MeshVertex()
					{
						Position = v[vi1 - 1],
						Normal = n[ni1 - 1],
						TextureCoords = t[ti1 - 1],
						Color = color,
						Material = material,
					};

					MeshVertex v3 = new MeshVertex()
					{
						Position = v[vi2 - 1],
						Normal = n[ni2 - 1],
						TextureCoords = t[ti2 - 1],
						Color = color,
						Material = material,
					};

					v1.Tangent = CalculateTangent(v1.Position, v2.Position, v3.Position,
													v1.TextureCoords, v2.TextureCoords, v3.TextureCoords,
													v1.Normal);

					v2.Tangent = CalculateTangent(v2.Position, v3.Position, v1.Position,
													v2.TextureCoords, v3.TextureCoords, v1.TextureCoords,
													v2.Normal);

					v3.Tangent = CalculateTangent(v3.Position, v1.Position, v2.Position,
													v3.TextureCoords, v1.TextureCoords, v2.TextureCoords,
													v3.Normal);

					vertBuilder.Add(v1);
					vertBuilder.Add(v2);
					vertBuilder.Add(v3);
					i.Add(index++);
					i.Add(index++);
					i.Add(index++);
				}
				else if (faceParts.Length == 4)
				{
					int vi0, ni0, ti0;
					ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

					int vi1, ni1, ti1;
					ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

					int vi2, ni2, ti2;
					ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

					int vi3, ni3, ti3;
					ParseFacePart(faceParts[3], out vi3, out ti3, out ni3);

					MeshVertex v1 = new MeshVertex()
					{
						Position = v[vi0 - 1],
						Normal = n[ni0 - 1],
						TextureCoords = t[ti0 - 1],
						Color = color,
						Material = material,
					};

					MeshVertex v2 = new MeshVertex()
					{
						Position = v[vi1 - 1],
						Normal = n[ni1 - 1],
						TextureCoords = t[ti1 - 1],
						Color = color,
						Material = material,
					};

					MeshVertex v3 = new MeshVertex()
					{
						Position = v[vi2 - 1],
						Normal = n[ni2 - 1],
						TextureCoords = t[ti2 - 1],
						Color = color,
						Material = material,
					};

					MeshVertex v4 = new MeshVertex()
					{
						Position = v[vi3 - 1],
						Normal = n[ni3 - 1],
						TextureCoords = t[ti3 - 1],
						Color = color,
						Material = material,
					};

					v1.Tangent = CalculateTangent(v1.Position, v2.Position, v3.Position,
													v1.TextureCoords, v2.TextureCoords, v3.TextureCoords,
													v1.Normal);

					v2.Tangent = CalculateTangent(v2.Position, v3.Position, v1.Position,
													v2.TextureCoords, v3.TextureCoords, v1.TextureCoords,
													v2.Normal);

					v3.Tangent = CalculateTangent(v3.Position, v1.Position, v2.Position,
													v3.TextureCoords, v1.TextureCoords, v2.TextureCoords,
													v3.Normal);
					
					v4.Tangent = CalculateTangent(v4.Position, v1.Position, v3.Position,
													v4.TextureCoords, v1.TextureCoords, v3.TextureCoords,
													v4.Normal);
					ushort baseIndex = index; 

					vertBuilder.Add(v1);
					vertBuilder.Add(v2);
					vertBuilder.Add(v3);
					vertBuilder.Add(v4);

					i.Add((ushort)(baseIndex + 0));
					i.Add((ushort)(baseIndex + 1));
					i.Add((ushort)(baseIndex + 2));

					i.Add((ushort)(baseIndex + 2));
					i.Add((ushort)(baseIndex + 3));
					i.Add((ushort)(baseIndex + 0));

					index += 4; 
				}
			}

			verts = vertBuilder.ToArray();
			indicies = i.ToArray(); 
		}



        public static void LoadObject_Indexed(string path, Vector4 material, out Vector3[] verts, out ushort[] indicies)
        {
            IEnumerable<string> lines = FileHelper.ReadAllLines(path);

            LoadObject_Indexed(lines, material, out verts, out indicies);
        }

        public static void LoadObject_Indexed(byte[] data, Vector4 material, out Vector3[] verts, out ushort[] indicies)
        {
            using (MemoryStream stream = new MemoryStream(data))
            using (StreamReader reader = new StreamReader(stream))
            {
                string[] lines = reader.ReadToEnd().Split(new string[] { "\n", System.Environment.NewLine }, StringSplitOptions.None);

                LoadObject_Indexed(lines, material, out verts, out indicies);
            }
        }

        public static void LoadObject_Indexed(IEnumerable<string> lines, Vector4 material, out Vector3[] verts, out ushort[] indicies)
        {
            List<Vector3> v = new List<Vector3>();
            List<Vector3> n = new List<Vector3>();
            List<Vector2> t = new List<Vector2>();

            List<string> faces = new List<string>();

            foreach (string line in lines)
            {
                if (line.StartsWith("#") == true)
                {
                    continue;
                }

                if (String.IsNullOrEmpty(line.Trim()) == true)
                {
                    continue;
                }

                if (line.StartsWith("v ") == true)
                {
                    v.Add(ParseVector3(line.Substring(2)));
                }
                else if (line.StartsWith("vn ") == true)
                {
                    n.Add(ParseVector3(line.Substring(3)));
                }
                else if (line.StartsWith("vt ") == true)
                {
                    t.Add(ParseVector2(line.Substring(3)));
                }
                else if (line.StartsWith("f ") == true)
                {
                    faces.Add(line.Substring(2));
                }
            }

            List<Vector3> vertBuilder = new List<Vector3>(v.ToArray());
            List<ushort> i = new List<ushort>();
            Color4 color = new Color4(1f, 1f, 1f, 0f);
            ushort index = 0;

            foreach (string face in faces)
            {
                string[] faceParts = face.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (faceParts.Length == 3)
                {
                    int vi0, ni0, ti0;
                    ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

                    int vi1, ni1, ti1;
                    ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

                    int vi2, ni2, ti2;
                    ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

                    i.Add((ushort)(vi0 - 1));
                    i.Add((ushort)(vi1 - 1));
                    i.Add((ushort)(vi2 - 1));
                }
                else if (faceParts.Length == 4)
                {
                    int vi0, ni0, ti0;
                    ParseFacePart(faceParts[0], out vi0, out ti0, out ni0);

                    int vi1, ni1, ti1;
                    ParseFacePart(faceParts[1], out vi1, out ti1, out ni1);

                    int vi2, ni2, ti2;
                    ParseFacePart(faceParts[2], out vi2, out ti2, out ni2);

                    int vi3, ni3, ti3;
                    ParseFacePart(faceParts[3], out vi3, out ti3, out ni3);

                    ushort baseIndex = index;

                    i.Add((ushort)(vi0 - 1));
                    i.Add((ushort)(vi1 - 1));
                    i.Add((ushort)(vi2 - 1));

                    i.Add((ushort)(vi2 - 1));
                    i.Add((ushort)(vi3 - 1));
                    i.Add((ushort)(vi0 - 1));
                }
            }

            verts = vertBuilder.ToArray();
            indicies = i.ToArray();
        }

		private static Vector2 ParseVector2(string str)
		{
			string[] parts = str.Split(new char[] { ' ' } , StringSplitOptions.RemoveEmptyEntries); 

			return new Vector2(float.Parse(parts[0], CultureInfo.InvariantCulture), 
							   1 - float.Parse(parts[1], CultureInfo.InvariantCulture)); 
		}

		private static Vector3 ParseVector3(string str)
		{
			string[] parts = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			return new Vector3(float.Parse(parts[0], CultureInfo.InvariantCulture),
							   float.Parse(parts[1], CultureInfo.InvariantCulture),
							   float.Parse(parts[2], CultureInfo.InvariantCulture)); 
		}		
		
		private static void ParseFacePart(string part, out int vi, out int ti, out int ni)
		{
			string[] parts = part.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

			vi = int.Parse(parts[0], CultureInfo.InvariantCulture);
			ti = int.Parse(parts[1], CultureInfo.InvariantCulture);
			ni = int.Parse(parts[2], CultureInfo.InvariantCulture); 
		}

		static Vector4 CalculateTangent(Vector3 pos1, Vector3 pos2,
										Vector3 pos3, Vector2 texCoord1,
										Vector2 texCoord2, Vector2 texCoord3,
										Vector3 normal)
		{
			// Given the 3 vertices (position and texture coordinates) of a triangle
			// calculate and return the triangle's tangent vector.

			// Create 2 vectors in object space.
			//
			// edge1 is the vector from vertex positions pos1 to pos2.
			// edge2 is the vector from vertex positions pos1 to pos3.
			Vector3 edge1 = new Vector3(pos2.X - pos1.X, pos2.Y - pos1.Y, pos2.Z - pos1.Z);
			Vector3 edge2 = new Vector3(pos3.X - pos1.X, pos3.Y - pos1.Y, pos3.Z - pos1.Z);

			edge1.Normalize();
			edge2.Normalize();

			// Create 2 vectors in tangent (texture) space that point in the same
			// direction as edge1 and edge2 (in object space).
			//
			// texEdge1 is the vector from texture coordinates texCoord1 to texCoord2.
			// texEdge2 is the vector from texture coordinates texCoord1 to texCoord3.
			Vector2 texEdge1 = new Vector2(texCoord2.X - texCoord1.X, texCoord2.Y - texCoord1.Y);
			Vector2 texEdge2 = new Vector2(texCoord3.X - texCoord1.X, texCoord3.Y - texCoord1.Y);

			texEdge1.Normalize();
			texEdge2.Normalize();

			// These 2 sets of vectors form the following system of equations:
			//
			//  edge1 = (texEdge1.r * tangent) + (texEdge1.g * bitangent)
			//  edge2 = (texEdge2.r * tangent) + (texEdge2.g * bitangent)
			//
			// Using matrix notation this system looks like:
			//
			//  [ edge1 ]     [ texEdge1.r  texEdge1.g ]  [ tangent   ]
			//  [       ]  =  [                        ]  [           ]
			//  [ edge2 ]     [ texEdge2.r  texEdge2.g ]  [ bitangent ]
			//
			// The solution is:
			//
			//  [ tangent   ]        1     [ texEdge2.g  -texEdge1.g ]  [ edge1 ]
			//  [           ]  =  -------  [                         ]  [       ]
			//  [ bitangent ]      det A   [-texEdge2.r   texEdge1.r ]  [ edge2 ]
			//
			//  where:
			//        [ texEdge1.r  texEdge1.g ]
			//    A = [                        ]
			//        [ texEdge2.r  texEdge2.g ]
			//
			//    det A = (texEdge1.r * texEdge2.g) - (texEdge1.g * texEdge2.r)
			//
			// From this solution the tangent space basis vectors are:
			//
			//    tangent = (1 / det A) * ( texEdge2.g * edge1 - texEdge1.g * edge2)
			//  bitangent = (1 / det A) * (-texEdge2.r * edge1 + texEdge1.r * edge2)
			//     normal = cross(tangent, bitangent)

			Vector3 t = new Vector3(0, 0, 0);
			Vector3 b = new Vector3(0, 0, 0);
			Vector3 n = new Vector3(normal.X, normal.Y, normal.Z);

			float det = (texEdge1.X * texEdge2.Y) - (texEdge1.Y * texEdge2.X);

			if (CloseEnough(det, 0.0f) == true)
			{
				t = new Vector3(1.0f, 0.0f, 0.0f);
				b = new Vector3(0.0f, 1.0f, 0.0f);
			}
			else
			{
				det = 1.0f / det;

				t.X = (texEdge2.Y * edge1.X - texEdge1.Y * edge2.X) * det;
				t.Y = (texEdge2.Y * edge1.Y - texEdge1.Y * edge2.Y) * det;
				t.Z = (texEdge2.Y * edge1.Z - texEdge1.Y * edge2.Z) * det;

				b.X = (-texEdge2.X * edge1.X + texEdge1.X * edge2.X) * det;
				b.Y = (-texEdge2.X * edge1.Y + texEdge1.X * edge2.Y) * det;
				b.Z = (-texEdge2.X * edge1.Z + texEdge1.X * edge2.Z) * det;

				t.Normalize();
				b.Normalize();
			}

			// Calculate the handedness of the local tangent space.
			// The bitangent vector is the cross product between the triangle face
			// normal vector and the calculated tangent vector. The resulting bitangent
			// vector should be the same as the bitangent vector calculated from the
			// set of linear equations above. If they point in different directions
			// then we need to invert the cross product calculated bitangent vector. We
			// store this scalar multiplier in the tangent vector's 'a' component so
			// that the correct bitangent vector can be generated in the normal mapping
			// shader's vertex shader.

			Vector3 bitangent = Vector3.Cross(n, t);
			float handedness = (Vector3.Dot(bitangent, b) < 0.0f) ? -1.0f : 1.0f;

			Vector4 tangent = new Vector4(); 

			tangent.X = t.X;
			tangent.Y = t.Y;
			tangent.Z = t.Z;
			tangent.W = handedness; // *-1f;

			return tangent; 
		}

		const float EPSILON = 1e-6f;

		static bool CloseEnough(float f1, float f2)
		{
			// Determines whether the two floating-point values f1 and f2 are
			// close enough together that they can be considered equal.			
			return Math.Abs((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < EPSILON;
		}
	}
}
