﻿using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using Rug.Game.Effect.PostProcess;
using Rug.Game.Shaders.PostProcess;
using System.Drawing;

namespace Rug.Game.Core.Pipeline
{
	public class BasicPipeline : IResizablePipeline, IResourceManager
	{
		public readonly ResourceManager ResourceManagers = new ResourceManager();
		public readonly ResourceSet Resources;

		public readonly FrameBuffer FrameBuffer;
		
		private BloomProcess m_BloomProcess;
		private DownSample m_DownSample;

		private readonly TextureBox Box;

		public bool Disposed { get; private set; }

		public bool IsEnabled { get; set; }

		public string Name { get; set; }

        public virtual FrameBuffer FinalFrameBuffer { get { return FrameBuffer; } } 

        public virtual Texture2D ColorBuffer { get { return FrameBuffer[FramebufferAttachment.ColorAttachment0]; } }
        public virtual Texture2D DepthBuffer { get { return FrameBuffer[FramebufferAttachment.DepthAttachment]; } }

        public Color4 BackgroundColor { get; set; } 

		public PipelineMode PipelineMode
		{
			get 
			{
				return
					PipelineMode.BeginEndBlock |
					PipelineMode.Render |
					PipelineMode.Update; 
			}
		}

		public BloomSettings BloomSettings { get { return m_BloomProcess.Settings; } set { m_BloomProcess.Settings = value; } }

		public VisualQuality BloomQuality { get; set; } 

		public BasicPipeline(string name)
		{			
			BloomQuality = VisualQuality.Disabled; 

			Resources = new ResourceSet(name + " Pipeline Resource Set", ResourceMode.Static);

			Disposed = true;

			FrameBuffer = new FrameBuffer(name + " Pipeline Frame Buffer", ResourceMode.Static,
					new FrameBufferInfo(
						new FrameBufferTexture2DInfo(name + " Color", FramebufferAttachment.ColorAttachment0)
						{
							Size = new Size(32, 32),
							InternalFormat = PixelInternalFormat.R11fG11fB10f,
							PixelFormat = PixelFormat.Rgb,
							PixelType = PixelType.Float,
							MagFilter = TextureMagFilter.Linear,
                            MinFilter = TextureMinFilter.Linear,
							WrapS = TextureWrapMode.ClampToBorder,
							WrapT = TextureWrapMode.ClampToBorder,
							Samples = 1,
						},
						new FrameBufferTexture2DInfo(name + " Depth", FramebufferAttachment.DepthAttachment)
						{
							Size = new Size(32, 32),
							InternalFormat = (PixelInternalFormat)All.DepthComponent24,
							PixelFormat = PixelFormat.DepthComponent,
							PixelType = PixelType.UnsignedInt,
                            MagFilter = TextureMagFilter.Linear,
                            MinFilter = TextureMinFilter.Linear,
							WrapS = TextureWrapMode.ClampToBorder,
							WrapT = TextureWrapMode.ClampToBorder,
							Samples = 1,
						}
						));

			Rug.Game.Environment.ResolutionDependentResources.Add(FrameBuffer);

			m_BloomProcess = new BloomProcess(32, 32, BloomQuality, MultiSamples.X1);
			m_BloomProcess.Settings = new BloomSettings();

			ResourceManagers.Add(m_BloomProcess);
		
			Box = new TextureBox();
			Box.FlipVertical = true;

			ResourceManagers.Add(Box);

            BackgroundColor = Color4.Black;
		}

        public virtual void Resize(int width, int height, MultiSamples samples)
		{
            FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Samples = GLHelper.MultiSampleToSampleCount(samples);
			FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = new Size(width, height);
            FrameBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Samples = GLHelper.MultiSampleToSampleCount(samples);
			FrameBuffer[FramebufferAttachment.DepthAttachment].ResourceInfo.Size = new Size(width, height);

			m_DownSample = PostProcessHelper.SelectDownSample(samples);

			m_BloomProcess.Resize(width, height, BloomQuality, samples);
		}

        public virtual void Update(Rug.Game.Core.Rendering.View3D view)
		{
			
		}

		public virtual void Begin()
		{

		}

		public virtual void End()
		{

		}

		public void BeginRender(Rug.Game.Core.Rendering.View3D view)
		{
  			FrameBuffer.Bind();

            GLState.CheckError(); 

			GLState.EnableBlend = true;
			GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GLState.BlendEquation(BlendEquationMode.FuncAdd);

            GLState.ClearColor(BackgroundColor); 
			GLState.EnableDepthTest = false;
			GLState.ClearDepth(1.0f);
            GLState.Viewport = view.Viewport;
			GLState.Apply(view);

            GLState.CheckError(); 

			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

            GLState.CheckError(); 
		}

		public void Render(Rug.Game.Core.Rendering.View3D view)
		{    
			BeginRender(view);

			RenderContent(view);

            EndRender(view);

            //GL.Flush();

            if (m_BloomProcess.Quality == VisualQuality.Disabled)
            {
                EndRender_NoBloom(view);
            }
			else 
			{
				EndRender_Bloom(view);
			}

            PostRender(view); 
		}


		public virtual void RenderContent(Rug.Game.Core.Rendering.View3D view)
		{

		}

        public virtual void EndRender(Rendering.View3D view)
        {
            FrameBuffer.Unbind();
        }

		public void EndRender_NoBloom(Rug.Game.Core.Rendering.View3D view)
		{
            // CANDIDATE FOR BLIT 

			Box.FlipVertical = true;
			Box.CheckAndWriteRectangle();

            if (ColorBuffer.ResourceInfo.Samples > 1)
			{
                m_DownSample.Render(ColorBuffer, Box.Vertices);
			}
			else
			{
                Box.Texture = ColorBuffer;
				Box.Render(view);
			}
		}

		public void EndRender_Bloom(Rug.Game.Core.Rendering.View3D view)
		{		

			GLState.EnableDepthTest = false;
			GLState.EnableDepthMask = false;
			GLState.DepthFunc(DepthFunction.Lequal);

			GLState.EnableBlend = true;
			GLState.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			GLState.BlendEquation(BlendEquationMode.FuncAdd);

			GLState.EnableCullFace = false;	
			GLState.CullFace(CullFaceMode.Back);
			GLState.FrontFace(FrontFaceDirection.Cw);			

			GLState.Apply(view);

            m_BloomProcess.Run(view, ColorBuffer, ColorBuffer.ResourceInfo.Samples > 1);

			//Box.FlipVertical = true;
			//Box.CheckAndWriteRectangle();

			GLState.EnableBlend = false;
			GLState.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);
			GLState.BlendEquation(BlendEquationMode.FuncAdd);
			GLState.Viewport = view.Viewport;
			GLState.Apply(view);

            //GL.Clear(ClearBufferMask.ColorBufferBit); 
            Rectangle rect = new Rectangle(new Point(0, 0), FinalFrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size);

            FinalFrameBuffer.Blit(ReadBufferMode.ColorAttachment0, DrawBufferMode.ColorAttachment0, rect, rect, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);

            /* 
            if (ColorBuffer.ResourceInfo.Samples > 1)
			{
                m_DownSample.Render(ColorBuffer, Box.Vertices);
			}
			else
			{
                Box.Texture = ColorBuffer;
				Box.Render(view);
			}
            */ 

			GLState.EnableBlend = true;
			GLState.Apply(view);

			m_BloomProcess.Render(view);
		}

        public virtual void PostRender(Rendering.View3D view)
        {

        }

		public virtual void LoadResources()
		{			
			ResourceManagers.LoadResources();
			Resources.LoadResources();

            LoadPrivateResources(); 
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1 + ResourceManagers.Total; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            ResourceManagerLoadState state = process.CurrentState;

            if (state.Index >= (this as IResourceManager).Count)
            {
                Resources.LoadResources();
                LoadPrivateResources(); 

                process.Pop();

                Disposed = false;
            }
            else
            {
                process.Increment();
                process.Push(ResourceManagers);
            }
        }

        protected virtual void LoadPrivateResources()
        {

        }

		public virtual void UnloadResources()
		{
            UnloadPrivateResources(); 

			ResourceManagers.UnloadResources();
			Resources.UnloadResources();
		}

        protected virtual void UnloadPrivateResources()
        {

        }

		public void Dispose()
		{
			ResourceManagers.Dispose(); 
		}
	}
}
