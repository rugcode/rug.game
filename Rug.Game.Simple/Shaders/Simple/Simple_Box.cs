﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Effect
{
    public enum BoxMode
    {
        Color = 0,
        Textured = 1,
        TexturedColor = 2, 
    }

	public class Simple_Box : BasicEffectBase
	{
        public static string GetName(BoxMode boxMode)
        {
            return "Simple_Box: Mode_" + boxMode.ToString();
        }

        public static void RegisterAllShaders()
        {
            SharedEffects.Effects.Add(GetName(BoxMode.Color), new Simple_Box(BoxMode.Color));
            SharedEffects.Effects.Add(GetName(BoxMode.Textured), new Simple_Box(BoxMode.Textured));
            SharedEffects.Effects.Add(GetName(BoxMode.TexturedColor), new Simple_Box(BoxMode.TexturedColor));
        }

		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Simple/Box";

		private int m_Texture;

        private int uColorModifier;
        private int uColor;

		#endregion

		public override string Name
		{
            get { return "Simple: Box (" + Mode + ")"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

        public BoxMode Mode { get; private set; }

        public Simple_Box(BoxMode mode) 
        {
            Mode = mode;

            Defines = new string[] { "Mode_" + Mode.ToString() };
        }

        public void Render(PrimitiveType primitiveType, int count, VertexBuffer vertex)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            if (vertex.ResourceInfo.Format != SimpleVertex.Format)
            {
                throw new Exception("Incorrect vertex format '" + vertex.ResourceInfo.Format.GetType().ToString() + "'");
            }

            if (Mode != BoxMode.Color)
            {
                throw new Exception("Invalid effect mode '" + Mode.ToString() + "'");
            }

            GL.UseProgram(ProgramHandle);

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
            SimpleVertex.Bind();

            GL.DrawArrays(primitiveType, 0, count);
            Rug.Game.Environment.DrawCallsAccumulator++;

            GL.UseProgram(0);

            SimpleVertex.Unbind();
        }

        public void Render(PrimitiveType primitiveType, int count, Texture2D texture, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

            if (vertex.ResourceInfo.Format != SimpleVertex.Format)
            {
                throw new Exception("Incorrect vertex format '" + vertex.ResourceInfo.Format.GetType().ToString() + "'");
            }

            if (Mode != BoxMode.Textured)
            {
                throw new Exception("Invalid effect mode '" + Mode.ToString() + "'");
            }

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, texture); 

			//GLState.ActiveTexture(TextureUnit.Texture0);
			//GL.BindTexture(TextureTarget.Texture2D, texture != null ? texture.ResourceHandle : 0);

            vertex.Bind();    
			SimpleVertex.Bind();

            GL.DrawArrays(primitiveType, 0, count);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

        public void Render(PrimitiveType primitiveType, int count, Texture2D texture, Color4 color, Vector2 mod, VertexBuffer vertex)
        {
            if (State != ProgramState.Linked)
            {
                return;
            }

            if (vertex.ResourceInfo.Format != SimpleVertex.Format)
            {
                throw new Exception("Incorrect vertex format '" + vertex.ResourceInfo.Format.GetType().ToString() + "'"); 
            }

            if (Mode != BoxMode.TexturedColor)
            {
                throw new Exception("Invalid effect mode '" + Mode.ToString() + "'"); 
            }

            GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, texture);

            GL.Uniform4(uColor, color);
            GL.Uniform2(uColorModifier, mod);

            vertex.Bind();                        
            SimpleVertex.Bind();

            //GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
            GL.DrawArrays(primitiveType, 0, count);

            Rug.Game.Environment.DrawCallsAccumulator++;

            GL.UseProgram(0);

            SimpleVertex.Unbind();
        }


		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

            switch (Mode)
            {
                case BoxMode.Color:
                    break;
                case BoxMode.Textured:
                    GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "sourceTexture"), 0);
                    break;
                case BoxMode.TexturedColor:
                    GL.Uniform1(GL.GetUniformLocation(ProgramHandle, "sourceTexture"), 0);
                    uColor = GL.GetUniformLocation(ProgramHandle, "effect_color");
                    uColorModifier = GL.GetUniformLocation(ProgramHandle, "effect_color_mod");
                    break;
                default:
                    break;
            }
            
			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion

    }
}
