#version 410

uniform mat4 objectMatrix;
uniform mat4 worldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 tex;
layout(location = 2) in vec4 col;

layout(location = 3) in vec4 instance_pos;
layout(location = 4) in vec4 instance_col;

out vec4 output_col;
out vec2 output_tex;

void main()
{
	vec4 localSpace = objectMatrix *  vec4(instance_pos.xyz, 1.0);
    vec4 worldSpace = vec4((worldMatrix * localSpace).xyz + (pos * instance_pos.w), 1.0);
    gl_Position = perspectiveMatrix * worldSpace;

	output_col = instance_col; // vec4(1, 1, 1, 1);
	output_tex = tex;
}
