﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Effect
{
	public class Simple_Particles : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Simple/Particles";

		private int m_Texture;

		private int uObjectMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;

		#endregion

		public override string Name
		{
			get { return "Simple: Particles"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Simple_Particles() { }

		public void Render(ref Matrix4 objectMatrix, ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix, Texture2D texture, VertexArrayObject vao, int instanceCount)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}
			
			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, texture);

			vao.Bind();

			GL.DrawArraysInstanced(PrimitiveType.TriangleStrip, 0, 4, instanceCount);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			vao.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Texture = GL.GetUniformLocation(ProgramHandle, "flatTexture");

			GL.Uniform1(m_Texture, 0);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");		

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
