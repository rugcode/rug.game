﻿using OpenTK.Graphics.OpenGL;
using Rug.Cmd;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Effect
{
	public class Simple_Particles_Uniform : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Simple/Particles_Uniform";

		private int m_Texture;
		private int m_InstanceData;
		private int m_InstanceData_Size;

		#endregion

		public override string Name
		{
			get { return "Simple: Particles (Using uniform buffer)"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Simple_Particles_Uniform() { }

		public void Render(Texture2D texture, VertexBuffer vertex, UniformBuffer instanceBuffer, int instanceCount)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			if (m_InstanceData_Size != instanceBuffer.ResourceInfo.Stride * instanceBuffer.ResourceInfo.Count)
			{
				RC.WriteError(03, "Instance buffer is not the correct size '" + (instanceBuffer.ResourceInfo.Stride * instanceBuffer.ResourceInfo.Count) + "' should be '" + m_InstanceData_Size + "'"); 
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, texture);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.Ext.UniformBuffer((uint)ProgramHandle, m_InstanceData, instanceBuffer.ResourceHandle);

			GL.DrawArraysInstanced(PrimitiveType.TriangleStrip, 0, 4, instanceCount);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);
			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Texture = GL.GetUniformLocation(ProgramHandle, "flatTexture");

			GL.Uniform1(m_Texture, 0);

			m_InstanceData = GL.GetUniformLocation(ProgramHandle, "instanceData");
			m_InstanceData_Size = GL.Ext.GetUniformBufferSize(ProgramHandle, m_InstanceData);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
 
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
