#version 410

uniform sampler2D flatTexture; 

in vec4 output_col;
in vec2 output_tex;

out vec4 colorFrag;

void main()
{
    colorFrag = texture(flatTexture, output_tex) * output_col;
}