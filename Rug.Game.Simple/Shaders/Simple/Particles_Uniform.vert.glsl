#version 410

#extension GL_EXT_bindable_uniform : enable
//#extension GL_EXT_gpu_shader4 : enable

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 tex;
layout(location = 2) in vec4 col;

out vec4 output_col;
out vec2 output_tex;

//bindable uniform vec4 vertices[%d];\n
bindable uniform vec4 instanceData[1024];

void main()
{
	vec3 instancePos = instanceData[gl_InstanceID].xyz;

	gl_Position = vec4(instancePos + pos, 1.0);
	output_col = col;
	output_tex = tex;
}