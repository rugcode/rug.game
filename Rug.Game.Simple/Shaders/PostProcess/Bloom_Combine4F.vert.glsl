#version 410

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 tex;
layout(location = 2) in vec4 col;

out vec2 output_tex;

void main()
{
	gl_Position = vec4(pos, 1);

	output_tex = tex;
}