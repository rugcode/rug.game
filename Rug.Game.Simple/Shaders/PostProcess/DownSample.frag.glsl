#version 410
#pragma optionNV (unroll all)

uniform sampler2DMS source;

// int samples = $Samples;
vec4 divider = vec4(1.0 / $Samples.0);

in vec2 output_tex; 

out vec4 colorFrag;

void main(void)
{
	vec4 c = vec4(0.0);	

	ivec2 texcoord0 = ivec2(textureSize(source) * output_tex); // used to fetch msaa texel location

#foreach $i in Samples
	c += texelFetch(source, texcoord0, $i);  // add  color samples together
#forend
 
	c *= divider; // divide by number of samples to get color average.

    colorFrag = c;
}