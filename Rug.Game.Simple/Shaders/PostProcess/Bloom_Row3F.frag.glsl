#version 410

uniform sampler2D source;
uniform vec3 coefficients;
uniform vec2 offset;

in vec2 output_tex; 

out vec4 colorFrag;

void main(void)
{
    vec4 c = vec4(0.0);
	
	c += coefficients.x * texture(source, output_tex - (offset * 2.0));
	c += coefficients.y * texture(source, output_tex - offset);
    c += coefficients.z * texture(source, output_tex);
    c += coefficients.y * texture(source, output_tex + offset);
	c += coefficients.x * texture(source, output_tex + (offset * 2.0));

    colorFrag = c;
}