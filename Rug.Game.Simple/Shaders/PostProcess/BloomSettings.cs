﻿using OpenTK;
using Rug.Game.Ui.Composite;

namespace Rug.Game.Effect.PostProcess
{
	public class BloomSettings
	{
		[ControlGroup(0, "General")]
		[SliderOptions_FloatAttribute(0, "Bloom Amount", 0.0f, 2f, 0.1f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float BloomAmount;

		[ControlGroup(0, "General")]
		[SliderOptions_FloatAttribute(1, "Lower", 0f, 2f, 0.5f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float Lower;

		[ControlGroup(0, "General")]
		[SliderOptions_FloatAttribute(2, "Upper", 0f, 2f, 1f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float Upper;

		[ControlGroup(0, "General")]
		[SliderOptions_FloatAttribute(3, "Spread", 0.1f, 2f, 1.5f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float Spread;

		[ControlGroup(1, "Blur")]
		[SliderOptions_FloatAttribute(0, "Blur Base", 1f, 2f, 1.5f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float BlurBase;

		[ControlGroup(1, "Blur")]
		[SliderOptions_FloatAttribute(1, "Blur Power", 1f, 8f, 1.5f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float BlurPower;

		[ControlGroup(1, "Blur")]
		[SliderOptions_FloatAttribute(2, "Blur Increment", 0f, 8f, 1.5f, Rug.Game.Ui.Dynamic.SliderScale.Linear)]
		public float BlurIncrement;

		[ControlGroup(2, "Composition")]
		[SliderOptions_FloatAttribute(0, "Base", 0.001f, 1f, 0.75f, Rug.Game.Ui.Dynamic.SliderScale.Linear)] 
		public float Base;

		[ControlGroup(2, "Composition")]
		[SliderOptions_FloatAttribute(1, "Exponent Offset", 0.00001f, 16f, 1.2f, Rug.Game.Ui.Dynamic.SliderScale.Linear)] 
		public float ExponentOffset;

		[ControlGroup(2, "Composition")]
		[SliderOptions_FloatAttribute(2, "Exponent Increment", 0f, 16f, 0.2f, Rug.Game.Ui.Dynamic.SliderScale.Linear)] 
		public float ExponentIncrement;
		
		public Vector4 Amount { get { return new Vector4(Base, ExponentOffset, ExponentIncrement, BloomAmount); } }

		public BloomSettings()
		{
			Lower = 0.5f;
			Upper = 1.0f;

			Base = 0.75f;
			ExponentOffset = 1.2f;
			ExponentIncrement = 0.2f;
			BloomAmount = 0.1f;
			Spread = 1.2f; 

			BlurBase = 1.5f;
			BlurPower = 1.5f;
			BlurIncrement = 1.5f;
		}
		
		public void Load(System.Xml.XmlNode node)
		{
			Spread = Helper.GetAttributeValue(node, "Spread", Spread);
			Base = Helper.GetAttributeValue(node, "Base", Base);
			ExponentOffset = Helper.GetAttributeValue(node, "ExponentOffset", ExponentOffset);
			ExponentIncrement = Helper.GetAttributeValue(node, "ExponentIncrement", ExponentIncrement);
			BloomAmount = Helper.GetAttributeValue(node, "BloomAmount", BloomAmount);

			Lower = Helper.GetAttributeValue(node, "Lower", Lower);
			Upper = Helper.GetAttributeValue(node, "Upper", Upper);

			BlurBase = Helper.GetAttributeValue(node, "BlurBase", BlurBase);
			BlurPower = Helper.GetAttributeValue(node, "BlurPower", BlurPower);
			BlurIncrement = Helper.GetAttributeValue(node, "BlurIncrement", BlurIncrement);
		}

		public void Save(System.Xml.XmlElement node)
		{
			Helper.AppendAttributeAndValue(node, "Spread", Spread);
			Helper.AppendAttributeAndValue(node, "Base", Base);
			Helper.AppendAttributeAndValue(node, "ExponentOffset", ExponentOffset);
			Helper.AppendAttributeAndValue(node, "ExponentIncrement", ExponentIncrement);
			Helper.AppendAttributeAndValue(node, "BloomAmount", BloomAmount);

			Helper.AppendAttributeAndValue(node, "Lower", Lower);
			Helper.AppendAttributeAndValue(node, "Upper", Upper);

			Helper.AppendAttributeAndValue(node, "BlurBase", BlurBase);
			Helper.AppendAttributeAndValue(node, "BlurPower", BlurPower);
			Helper.AppendAttributeAndValue(node, "BlurIncrement", BlurIncrement);
		}
	}
}
