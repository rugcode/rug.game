﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;

namespace Rug.Game.Effect.PostProcess
{
	public class Bloom_Combine4F : BasicEffectBase
	{
		public const int MaxPasses = 16;

		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/PostProcess/Bloom_Combine4F";

		private int[] m_Passes;
		private int m_Amount;		

		#endregion

		public override string Name
		{
			get { return "Post Process: Bloom Combine4F (" + m_Passes.Length + ")"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Bloom_Combine4F(int count) 
		{
			m_Passes = new int[Math.Max(1, count)]; 

			List<string> defines = new List<string>(); 

			Defines = defines.ToArray(); 

			Loops.Add("Passes", Math.Max(1, count));
		}

		public void Render(Texture2D[] passes, Vector4 amount, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.Uniform4(m_Amount, amount);

			for (int i = 0; i < m_Passes.Length; i++)
			{
                GLState.BindTexture(TextureUnit.Texture0 + i, TextureTarget.Texture2D, passes[i]);
			}

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Amount = GL.GetUniformLocation(ProgramHandle, "amount"); 

			for (int i = 0; i < m_Passes.Length; i++)
			{
				m_Passes[i] = GL.GetUniformLocation(ProgramHandle, "Pass" + i);
			}

			for (int i = 0; i < m_Passes.Length; i++)
			{
				GL.Uniform1(m_Passes[i], i);
			}

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
