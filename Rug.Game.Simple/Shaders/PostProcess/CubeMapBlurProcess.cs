﻿using Rug.Game.Core.Buffers;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Textures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Game.Simple.Shaders.PostProcess
{
    public class CubeMapBlurProcess : IResourceManager
    {
        private FrameBufferCube m_RenderBuffer;

        public bool Disposed { get; private set; }

        public TextureCube TextureCube { get; private set; } 

        public CubeMapBlurProcess(string name, TextureCubeInfo destInfo)
        {
            Disposed = true;

            m_RenderBuffer = new FrameBufferCube(name + " Destination Cube Map", Rug.Game.Core.Resources.ResourceMode.Dynamic, destInfo);

            TextureCube = m_RenderBuffer.TextureCube;
        }

        public void LoadResources()
        {
            m_RenderBuffer.LoadResources();
        }

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

        public void UnloadResources()
        {
            m_RenderBuffer.UnloadResources();
        }

        public void Dispose()
        {
            //m_Box.Dispose();
        }
    }
}
/* 
glm::vec3 mapToHemisphere(const glm::vec2 & point, float maxVertAngle = M_PI/2)
{
   // point on 2D square in [0, 1] range
   glm::vec2 in = point;

   // map point to [-1, 1] range
   in = in * 2.0f - 1.0f;

   // perpendicular direction
   if(in.x == 0 && in.y ==0)
   {
      // in tangent space perpendicular is parallel to Z axis
      return glm::vec3(0, 0, 1);
   }

   // deviation from perpendicular in range [0, 1]
   float sinTheta;
   if(in.y > -in.x) // above the line y=-x
   {
      if(in.y < in.x) // under the line y=x
      {
         sinTheta = in.x;
      }
      else // above the line y = x
      {
         sinTheta = in.y;
      }
   }
   else // under the line y=-x
   {
      if(in.y > in.x) // above the line y=x
      {
         sinTheta = -in.x;
      }
      else // under the line y = x
      {
         sinTheta = -in.y;
      }
   }

   // determine theta - angle with vertical axis
   float theta = asinf(sinTheta);
   // scale angle. By default theta is in range [0, M_PI/2]
   theta *= maxVertAngle/(M_PI/2);

   // normalized direction in 2D
   in = glm::normalize(in);
   // determine angle around vertical axis
   float phi = atan2(in.y, in.x);

   // transform spherical coordinates to 3D cartesian
   glm::vec3 out;
   out.x = cos(phi) * sin(theta);
   out.y = sin(phi) * sin(theta);
   out.z = cos(theta);
   return out;
}


std::unique_ptr<Texture2D> getHemisphereNormalsTexture(int size, float hemiAngle)
{
   // allocate memory for texture
   int components = 3;
   std::unique_ptr data(new GLubyte[components * size * size]);

   // half texel offset
   float base = 0.5 / size;
   // calculate direction for each texel in texture
   for(int x=0; x    {
      // position on 2D square along X axis
      float xx = float(x)/size;
      for(int y=0; y       {
         // position on 2D square along Y axis
         float yy = float(y)/size;

         // get 3D direction
         glm::vec3 res = mapToHemisphereComplex(glm::vec2(xx + base, 
                  yy + base), hemiAngle);

         // location of texel in the texture
         int offset = x * components + y * size * components;

         // save direction to texture and map to range [0, 255]
         data[offset++] = (res.x + 1.0f)/2.0f * 255;
         data[offset++] = (res.y + 1.0f)/2.0f * 255;
         data[offset++] = (res.z + 1.0f)/2.0f * 255;
      }
   }

   // create OpenGL texture and fill it with data
   std::unique_ptr<Texture2D> tex(new Texture2D());
   tex->setup(size, size, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, data.get());
   return std::move(tex);
}


#version 330

// attributes
layout(location = 0) in vec3	i_position;	// xyz - position
layout(location = 1) in vec3	i_normal;	// xyz - normal
layout(location = 3) in vec4 i_tangent; // xyz - tangent, w - handedness

// matrices
uniform mat4 u_modelViewProjectionMat;
uniform mat3 u_normalMat;

// data for fragment shader
out vec3 o_worldNormal;
out vec3 o_worldTangent;
out float o_handedness;

void main(void)
{
   // vertex in screen space
   gl_Position	= u_modelViewProjectionMat * vec4(i_position, 1);

   // transform normal and tangent to world space
   o_worldNormal	= normalize(u_normalMat * i_normal);
   o_worldTangent	= normalize(u_normalMat * i_tangent.xyz);
   o_handedness = i_tangent.w;
}


#version 330

// data from vertex shader
in vec3	o_worldNormal;
in vec3	o_worldTangent;
in float o_handedness;

// texture that should be blurred
layout(location = 0) uniform samplerCube	u_colorTexture;
// texture with directions in tangent space
layout(location = 1) uniform sampler2D u_normalsTexture;

// size of texture with directions
uniform vec2	u_normalTextureDimensions;

// color to framebuffer
out vec4	resultingColor;

void main(void)
{
   // normalize normal and tangent after interpolation
   vec3 N	= normalize(o_worldNormal);
   vec3 T	= normalize(o_worldTangent);

   // restore orthogonaliti after interpolation
   T	= T - N * dot(N, T);
   // restore bitangent
   vec3 B	= cross(N, T) * o_handedness;

   // offset with size of one texel
   vec2 texStep = vec2(1.0, 1.0) / u_normalTextureDimensions;
   // offset with size of half of a texel
   vec2 texOffset	= vec2(0.5, 0.5) / u_normalTextureDimensions;

   // matrix that transforms from tangent space to world space
   mat3 toWorldSpace = mat3(T, B, N);

   // variables to accumulate color and weight
   vec3 accumulatedColor = vec3(0, 0, 0);
   float accumulatedWeight = 0;

   // for each direction in texture with directions
   for(float x=0; x < u_normalTextureDimensions.x; x+=1)
   {
      for(float y=0; y < u_normalTextureDimensions.y; y+=1)
      {
         // texture coordinates for sampling from texture with directions
         vec2 texCoords = texOffset + texStep * vec2(x, y);

         // direction from texture with directions (tangent space)
         vec3 NT	= normalize(texture(u_normalsTexture, texCoords).rgb * 2.0 - 1.0);

         // transform direction to wold space
         vec3 NW = normalize(toWorldSpace * NT);

         // sample from input cube texture with world space direction
         vec3 sampleColor = texture(u_colorTexture, NW).rgb;

         // decrease influence with Lambert's law
         float weight = dot(NW, N);

         // add and weight color
         accumulatedColor += sampleColor * weight;
         accumulatedWeight += weight;
      }
   }

   // save average color
   resultingColor.xyz	= accumulatedColor / accumulatedWeight;
   resultingColor.a	= 1;
}
*/ 