﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Effect.PostProcess
{
	public class DownSample : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/PostProcess/DownSample";

		private int m_Source;
		private int uLevel;

		#endregion

		public override string Name
		{
			get { return "Post Process: DownSample"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public DownSample(MultiSamples samples) 
		{
			Defines = new string[] { samples.ToString() };
            Loops.Add("Samples", GLHelper.MultiSampleToSampleCount(samples)); 
		}

		public void Render(Texture2D source, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, source);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Source = GL.GetUniformLocation(ProgramHandle, "source");

			GL.Uniform1(m_Source, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
