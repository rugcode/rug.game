#version 410

#foreach $i in Passes
uniform sampler2D Pass$i;
#forend

uniform vec4 amount;

in vec2 output_tex; 

out vec4 colorFrag;

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main(void)
{
	colorFrag = vec4(0.0); 

#foreach $i in Passes
	colorFrag += texture(Pass$i, output_tex) * pow(amount.x, amount.z * $i.0 + amount.y) * amount.w;
#forend
}