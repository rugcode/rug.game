﻿using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using System.Collections.Generic;
using System.Drawing;

namespace Rug.Game.Effect.PostProcess
{
	public class BloomSurface
	{
		public int Width, Height;
		public Viewport Viewport;
		private FrameBuffer m_FrameBuffer;

		public Rug.Game.Core.Textures.Texture2D Texture { get { return m_FrameBuffer[FramebufferAttachment.ColorAttachment0]; } } 

		// void phCreateSurface(PHsurface *surface, GLboolean depth, GLboolean fp, GLboolean linear)
		public BloomSurface(int width, int height, bool depth, bool fp, bool linear)
		{
			Width = width;
			Height = height; 

			PixelInternalFormat internalFormat = fp ? PixelInternalFormat.R11fG11fB10f : PixelInternalFormat.Rgb10A2;
			PixelType type = fp ? PixelType.HalfFloat : PixelType.UnsignedByte;
			TextureMagFilter filterMag = linear ? TextureMagFilter.Linear : TextureMagFilter.Nearest;
			TextureMinFilter filterMin = linear ? TextureMinFilter.Linear : TextureMinFilter.Nearest;

			List<FrameBufferTexture2DInfo> infos = new List<FrameBufferTexture2DInfo>();

			infos.Add(
			new FrameBufferTexture2DInfo("Color", FramebufferAttachment.ColorAttachment0Ext)
			{
				Size = new System.Drawing.Size(Width, Height),
				InternalFormat = internalFormat,
				PixelFormat = PixelFormat.Rgb,
				PixelType = type,
				MagFilter = filterMag,
				MinFilter = filterMin,
				WrapS = TextureWrapMode.ClampToBorder,
				WrapT = TextureWrapMode.ClampToBorder,
			});

			if (depth == true)
			{
				infos.Add(
				new FrameBufferTexture2DInfo("Depth", FramebufferAttachment.DepthAttachmentExt)
				{
					Size = new System.Drawing.Size(Width, Height),
					InternalFormat = (PixelInternalFormat)All.DepthComponent24,
					PixelFormat = PixelFormat.DepthComponent,
					PixelType = PixelType.UnsignedInt,
					MagFilter = filterMag,
					MinFilter = filterMin,
					WrapS = TextureWrapMode.ClampToBorder,
					WrapT = TextureWrapMode.ClampToBorder,
				});
			}

			m_FrameBuffer = new FrameBuffer("Bloom Surface", ResourceMode.Static, new FrameBufferInfo(infos.ToArray()));

			Rug.Game.Environment.ResolutionDependentResources.Add(m_FrameBuffer);

			Viewport = new Viewport(0, 0, Width, Height); 
		}


		internal void Bind()
		{
			m_FrameBuffer.Bind();

			GLState.Viewport = Viewport;
			GLState.Apply(Width, Height); 
		}

		internal void Unbind()
		{
			m_FrameBuffer.Unbind(); 
		}

		internal void Clear()
		{
			GL.Clear(ClearBufferMask.ColorBufferBit); 
		}

		internal void Resize(int width, int height)
		{
			Width = width;
			Height = height; 

			Viewport = new Viewport(0, 0, Width, Height);
			
			m_FrameBuffer[FramebufferAttachment.ColorAttachment0].ResourceInfo.Size = new Size(Width, Height); 
		}
	}
}