﻿using OpenTK;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Rendering;
using Rug.Game.Core.Resources;
using Rug.Game.Core.Simple;
using Rug.Game.Core.Textures;
using System;

namespace Rug.Game.Effect.PostProcess
{
	public enum Direction { Vertical, Horizontal }

	public class BloomProcess : IResourceManager
	{		
		float[] kernel = new float[] { 11, 19, 28 }; 	

		BloomSurface[] pass0;
		BloomSurface[] pass1;
		TextureBox m_Box;
		Bloom_Combine4F m_Combiner;
		Bloom_HighPass m_HighPass;
		Bloom_HighPass m_HighPass_Downsample;

		Bloom_Row3F m_Blur;

		Texture2D[] m_PassTextures;

		int m_PassCount;

		public BloomSettings Settings { get; set; }

		public VisualQuality Quality { get; set; } 
		
		public bool Disposed { get { return m_Box.Disposed; } }

		public BloomProcess(int width, int height, VisualQuality quality, MultiSamples samples)
		{
			Settings = new BloomSettings();
			
			Quality = quality; 

			m_Blur = SharedEffects.Effects["Bloom_Row3F"] as Bloom_Row3F;

			m_Box = new TextureBox();
			m_Box.FlipVertical = true;

			NormaliseKernal();

			pass0 = new BloomSurface[Bloom_Combine4F.MaxPasses];
			pass1 = new BloomSurface[Bloom_Combine4F.MaxPasses];
			m_PassTextures = new Texture2D[Bloom_Combine4F.MaxPasses];

			for (int i = 0; i < Bloom_Combine4F.MaxPasses; i++)
			{
				pass0[i] = new BloomSurface(2, 2, false, false, true);
				pass1[i] = new BloomSurface(2, 2, false, false, false);
				m_PassTextures[i] = pass0[i].Texture;
			}

			CreateBloomSurfaces(width, height, Quality, samples);
		}

		private void NormaliseKernal()
		{
			float sum = 0;

			for (int c = 0; c < kernel.Length; c++)
			{
				sum += kernel[c];
			}

			for (int c = 0; c < kernel.Length; c++)
			{
				kernel[c] /= sum;
			}
		}

		private void CreateBloomSurfaces(int width, int height, VisualQuality quality, MultiSamples samples)
		{
			m_HighPass = SharedEffects.Effects["Bloom_HighPass"] as Bloom_HighPass;
			m_HighPass_Downsample = SharedEffects.Effects["Bloom_HighPass_Downsample_" + samples.ToString()] as Bloom_HighPass;	

			m_PassCount = 1;

			int w = width;
			int h = height;

			int shiftSize = 1;

			switch (quality)
			{
				case VisualQuality.Terrible:
                case VisualQuality.Disabled:
					w = w >> 3;
					h = h >> 3;
					shiftSize = 3; 
					break;
				case VisualQuality.Acceptable:
					w = w >> 3;
					h = h >> 3;
					shiftSize = 2; 
					break;
				case VisualQuality.Superior:
					w = w >> 2;
					h = h >> 2;
					shiftSize = 2; 
					break;
				case VisualQuality.Preposterous:
					w = w >> 1;
					h = h >> 1;
					break;
				default:
					break;
			}

			while (m_PassCount < (Bloom_Combine4F.MaxPasses - 1) && w > 8 && h > 8)
			{
				m_PassCount++;

				w = w >> shiftSize;
				h = h >> shiftSize;
			}
			
			w = width; 
			h = height;

			switch (quality)
			{
				case VisualQuality.Terrible:
                case VisualQuality.Disabled:
					w = w >> 5;
					h = h >> 5;
					break;
				case VisualQuality.Acceptable:
					w = w >> 3;
					h = h >> 3;
					break;
				case VisualQuality.Superior:
					w = w >> 2;
					h = h >> 2;
					break;
				case VisualQuality.Preposterous:
					w = w >> 1;
					h = h >> 1;
					break;
				default:
					break;
			}

			for (int p = 0; p < m_PassCount; p++)
			{
				pass0[p].Resize(w, h);
				pass1[p].Resize(w, h);
				m_PassTextures[p] = pass0[p].Texture;

				w = w >> shiftSize;
				h = h >> shiftSize;
			}

			m_Combiner = SharedEffects.Effects["Bloom_Combine4F_" + m_PassCount] as Bloom_Combine4F;
		}

		public void Run(View3D view, Texture2D initial, bool downSample)
		{
			GLState.EnableBlend = false;

			float blurPower = Settings.BlurPower;

			for (int c = 0; c < kernel.Length; c++)
			{
				kernel[c] = (float)Math.Pow(Settings.BlurBase, blurPower);

				blurPower += Settings.BlurIncrement;
			}

			NormaliseKernal();

			pass0[0].Bind();

			if (downSample == true)
			{
				m_HighPass_Downsample.Render(initial, Settings.Lower, Settings.Upper, m_Box.Vertices);
			}
		    else 
			{
				m_HighPass.Render(initial, Settings.Lower, Settings.Upper, m_Box.Vertices);
			}

			pass0[0].Unbind();
            
			// Downsample the scene into the source surfaces.
			for (int p = 0; p < m_PassCount; p++)
			{
				if (p > 0)
				{
					pass0[p].Bind();

					m_Box.Render(view);

					pass0[p].Unbind();
				}

				// Perform the horizontal blurring pass.
				Blur(view, pass0, pass1, p, Direction.Horizontal);

				// Perform the vertical blurring pass.
				Blur(view, pass1, pass0, p, Direction.Vertical);

				m_Box.Texture = pass0[p].Texture; 
			}
		}

		public void Render(View3D view)
		{		
			GLState.Viewport = view.Viewport;
			GLState.Apply(view);

			Vector4 amount = Settings.Amount; 

			switch (Quality)
			{
				case VisualQuality.Terrible:
                case VisualQuality.Disabled:
					amount.W *= 2.5f;
					break;
				case VisualQuality.Acceptable:
					amount.W *= 1.5f;
					break;
				case VisualQuality.Superior:
					amount.W *= 1.25f;
					break;
				case VisualQuality.Preposterous:
					break;
				default:
					break;
			}

			m_Combiner.Render(m_PassTextures, amount, m_Box.Vertices); 
		}

		private void Blur(View3D view, BloomSurface[] sources, BloomSurface[] dests, int index, Direction direction)
		{
			Vector3 kernalVec = new Vector3(kernel[0], kernel[1], kernel[2]);

			Vector2 offsets;

			if (direction == Direction.Horizontal)
			{
				offsets = new Vector2(Settings.Spread / (float)sources[index].Width, 0f);  
			}
			else 
			{
				offsets = new Vector2(0f, Settings.Spread / (float)sources[index].Height); 
			}

			dests[index].Bind();

			m_Blur.Render(sources[index].Texture, kernalVec, offsets, m_Box.Vertices);

			dests[index].Unbind(); 
		}		

		public void LoadResources()
		{
			m_Box.LoadResources(); 
		}

        int IResourceManager.Count { get { return 1; } }

        int IResourceManager.Total { get { return 1; } }

        public void LoadNext(ResourceManagerLoadProcess process)
        {
            LoadResources();

            process.Increment();
            process.Pop();
        }

		public void UnloadResources()
		{
			m_Box.UnloadResources(); 
		}

		public void Resize(int width, int height, VisualQuality quality, MultiSamples samples)
		{
			Quality = quality; 

			CreateBloomSurfaces(width, height, quality, samples);
		}

		public void Dispose()
		{
			m_Box.Dispose();
		}

	}
}