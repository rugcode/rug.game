﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Effect.PostProcess
{
	public class Bloom_HighPass : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/PostProcess/Bloom_HighPass";

		private int m_Source;
		private int uLevel;

		#endregion

		public override string Name
		{
			get { return "Post Process: Bloom HighPass"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Bloom_HighPass()
		{
			Defines = new string[0];
		}

		public Bloom_HighPass(MultiSamples samples) 
		{ 
			Defines = new string[] { "Downsample", samples.ToString() }; 
		}

		public void Render(Texture2D source, float lower, float upper, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, source);

			Vector2 level = new Vector2(lower, upper);

			GL.Uniform2(uLevel, ref level);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Source = GL.GetUniformLocation(ProgramHandle, "source");
			uLevel = GL.GetUniformLocation(ProgramHandle, "level");

			GL.Uniform1(m_Source, 0);

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
