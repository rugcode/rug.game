﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;
using Rug.Game.Core.Textures;

namespace Rug.Game.Effect.PostProcess
{
	public class Bloom_Row3F : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/PostProcess/Bloom_Row3F";

		private int m_Source;
		private int uCoefficients;
		private int uOffsets;

		#endregion

		public override string Name
		{
			get { return "Post Process: Bloom Row3F"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Bloom_Row3F() { }

		public void Render(Texture2D source, Vector3 coefficients, Vector2 offsets, VertexBuffer vertex)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

            GLState.BindTexture(TextureUnit.Texture0, TextureTarget.Texture2D, source);

			GL.Uniform3(uCoefficients, ref coefficients);
			GL.Uniform2(uOffsets, ref offsets); 

			GL.BindBuffer(BufferTarget.ArrayBuffer, vertex.ResourceHandle);
			SimpleVertex.Bind();

			GL.DrawArrays(PrimitiveType.TriangleStrip, 0, 4);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			SimpleVertex.Unbind();
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			m_Source = GL.GetUniformLocation(ProgramHandle, "source");
			uCoefficients = GL.GetUniformLocation(ProgramHandle, "coefficients");
			uOffsets = GL.GetUniformLocation(ProgramHandle, "offset");
			
			GL.Uniform1(m_Source, 0);


			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{

		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
