#version 410
#pragma optionNV (unroll all)

#section Downsample
uniform sampler2DMS source;
#end 
#section !Downsample
uniform sampler2D source;
#end 

uniform vec2 level = vec2(0.3, 0.6);

in vec2 output_tex; 

out vec4 colorFrag;

void main(void)
{
	vec4 c = vec4(0.0);

#section Downsample

#section X1
	int samples = 1;
	vec4 div = vec4(1.0);
#end

#section X2
	int samples = 2;
	vec4 div = vec4(0.5);
#end

#section X4
	int samples = 4;
	vec4 div = vec4(0.25);
#end

#section X8
	int samples = 8;
	vec4 div = vec4(0.125);
#end

#section X16
	int samples = 16;
	vec4 div = vec4(0.0625);
#end

	ivec2 texcoord = ivec2(textureSize(source) * output_tex); // used to fetch msaa texel location

	for (int i = 0; i < samples; i++)
	{
		c += texelFetch(source, texcoord, i);  // add  color samples together
	}
 
	c *= div; //divide by num of samples to get color avg.	
#end 

#section !Downsample
	c = texture(source, output_tex);
#end

	c.rgb = smoothstep(level.x, level.y, c.rgb); 

    colorFrag = c;
}