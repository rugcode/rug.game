﻿using Rug.Game.Core.Effect;
using Rug.Game.Effect.PostProcess;

namespace Rug.Game.Shaders.PostProcess
{
	public static class PostProcessHelper
	{
		public static void RegisterBloomShaders()
		{
			#region Post Process

			SharedEffects.Effects.Add("Bloom_HighPass", new Bloom_HighPass());

			for (int i = 0; i <= (int)MultiSamples.X16; i++)
			{
				SharedEffects.Effects.Add("Bloom_HighPass_Downsample_" + ((MultiSamples)i).ToString(), new Bloom_HighPass((MultiSamples)i));
				SharedEffects.Effects.Add("DownSample_" + ((MultiSamples)i).ToString(), new DownSample((MultiSamples)i));
			}

			for (int i = 1; i < Bloom_Combine4F.MaxPasses; i++)
			{
				SharedEffects.Effects.Add("Bloom_Combine4F_" + i, new Bloom_Combine4F(i));
			}

			SharedEffects.Effects.Add("Bloom_Row3F", new Bloom_Row3F());			

			#endregion
		}

		public static DownSample SelectDownSample(MultiSamples samples)
		{
			return SharedEffects.Effects["DownSample_" + samples.ToString()] as DownSample;
		}
	}
}
