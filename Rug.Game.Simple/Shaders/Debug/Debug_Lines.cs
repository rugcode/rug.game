﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using Rug.Game.Core.Buffers;
using Rug.Game.Core.Data;
using Rug.Game.Core.Effect;

namespace Rug.Game.Effect
{	
	public class Debug_Lines : BasicEffectBase
	{
		#region Private Members

		private string m_ShaderLocation = @"~/Shaders/Debug/Lines";

		private int uObjectMatrix;
		private int uWorldMatrix;
		private int uPerspectiveMatrix;

		#endregion

		public override string Name
		{
			get { return "Debug: Lines"; }
		}

		public override string ShaderLocation { get { return m_ShaderLocation; } }

		public Debug_Lines() { }

		public void Begin(ref Matrix4 worldMatrix, ref Matrix4 perspectiveMatrix)
		{
			if (State != ProgramState.Linked)
			{
				return;
			}

			GL.UseProgram(ProgramHandle);

			GL.UniformMatrix4(uWorldMatrix, false, ref worldMatrix);
			GL.UniformMatrix4(uPerspectiveMatrix, false, ref perspectiveMatrix);
		}

		public void Render(ref Matrix4 objectMatrix, int indexCount, DrawElementsType indexType)
		{
			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.DrawElements(PrimitiveType.Lines, indexCount, indexType, 0);
			Rug.Game.Environment.DrawCallsAccumulator++; 
		}

		public void Render(ref Matrix4 objectMatrix, VertexBuffer verts)
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			DebugVertex.Bind();

			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.DrawArrays(PrimitiveType.Lines, 0, 6);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			DebugVertex.Unbind();

		}

		public void Render(ref Matrix4 objectMatrix, VertexBuffer verts, int count)
		{
			GL.BindBuffer(BufferTarget.ArrayBuffer, verts.ResourceHandle);
			DebugVertex.Bind();

			GL.UniformMatrix4(uObjectMatrix, false, ref objectMatrix);
			GL.DrawArrays(PrimitiveType.Lines, 0, count);
			Rug.Game.Environment.DrawCallsAccumulator++; 

			GL.UseProgram(0);

			DebugVertex.Unbind();

		}

		public void End()
		{
			GL.UseProgram(0);
		}

		#region IResourceManager

		public override void LoadResources()
		{
			base.LoadResources();

			GL.UseProgram(ProgramHandle);

			uObjectMatrix = GL.GetUniformLocation(ProgramHandle, "objectMatrix");
			uWorldMatrix = GL.GetUniformLocation(ProgramHandle, "worldMatrix");
			uPerspectiveMatrix = GL.GetUniformLocation(ProgramHandle, "perspectiveMatrix");			

			GL.UseProgram(0);
		}

		protected override void OnLoadResources()
		{
			GL.BindFragDataLocation(ProgramHandle, 0, "colorFrag");
			
		}

		protected override void OnUnloadResources()
		{

		}

		#endregion
	}
}
