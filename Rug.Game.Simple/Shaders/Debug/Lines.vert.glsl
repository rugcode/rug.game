#version 410

uniform mat4 objectMatrix;
uniform mat4 worldMatrix;
uniform mat4 perspectiveMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;

out vec4 color_var;

void main()
{
    vec4 localSpace = objectMatrix * vec4(position, 1);
    vec4 worldSpace = worldMatrix * localSpace;
    gl_Position = perspectiveMatrix * worldSpace;  
  
    color_var = color;
}