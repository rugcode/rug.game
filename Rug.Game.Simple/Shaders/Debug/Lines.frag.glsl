#version 410

in vec4 color_var;

out vec4 colorFrag;

void main()
{
    colorFrag = color_var;
}