﻿using OpenTK;
using Rug.Game.Input;
using System;

namespace Rug.Game.Core.Rendering
{
    public class FpsCameraController : IInputHandler
    {        
        System.Drawing.Point pointer_current, pointer_previous;
        Vector2 Look_Delta;

        public float Yaw { get; set; }
        public float Pitch { get; set; }  

        private bool Move_Forward = false;
        private bool Move_Backward = false;
        private bool Move_Left = false;
        private bool Move_Right = false;
        private bool Move_Boost = false;

        public float TravelSpeed { get; set; }
        public float TravelBoost { get; set; }
        public float MouseSpeed { get; set; } 

        public bool Enabled { get; set; }

        public bool HasFocus { get { return Enabled; } }

        public System.Windows.Forms.Form InputForm { get; set; }

        public View3D View { get; set; } 

        public FpsCameraController()
        {
            Enabled = true;
            TravelBoost = 3f; 
            TravelSpeed = 10f;
            MouseSpeed = 1f; 
        }

        public void Update(float delta)
        {
            if (Enabled == false)
            {
                View.Camera.Update(); 
                View.UpdateProjection(); 
                return; 
            }

            float divide = 200.0f;

            ResetMousePositionToCenter(); 

			Yaw += ((float)Look_Delta.X / divide) * MouseSpeed;
			Pitch += ((float)Look_Delta.Y / divide) * MouseSpeed;

			Yaw += MathHelper.TwoPi; 
			Yaw %= MathHelper.TwoPi; 
			Pitch = Math.Max(-MathHelper.PiOver2, Math.Min(MathHelper.PiOver2, Pitch));
			
			Matrix3 pitchMatrix = Matrix3.CreateRotationX(Pitch);

			Matrix3 yawMatrix = Matrix3.CreateRotationY(Yaw); 

			View.Camera.SetRotation(Quaternion.FromMatrix(yawMatrix * pitchMatrix));

            View.Camera.Update();

            float speed = TravelSpeed * delta;

            if (Move_Boost == true)
            {
                speed *= TravelBoost; 
            }

            if (Move_Forward == true)
            {
                View.Camera.Translate(new Vector3(0, 0, 1) * speed);
            }                                                
                                                             
            if (Move_Backward == true)                       
            {                                                
                View.Camera.Translate(new Vector3(0, 0, -1) * speed);
            }                                                
                                                             
            if (Move_Left == true)                           
            {                                                
                View.Camera.Translate(new Vector3(1, 0, 0) * speed);
            }                                                
                                                             
            if (Move_Right == true)                          
            {                                                
                View.Camera.Translate(new Vector3(-1, 0, 0) * speed);
            }

            View.UpdateProjection(); 
        }

        public void OnKeyDown(System.Windows.Forms.KeyEventArgs args, out bool handled)
        {
            handled = false;

            if (args.KeyCode == System.Windows.Forms.Keys.Space)
            {
                Move_Boost = true;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.W)
            {
                Move_Forward = true;
                handled = true; 
            }

            if (args.KeyCode == System.Windows.Forms.Keys.S)
            {
                Move_Backward = true;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.A)
            {
                Move_Left = true;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.D)
            {
                Move_Right = true;
                handled = true;
            }            
        }

        public void OnKeyUp(System.Windows.Forms.KeyEventArgs args, out bool handled)
        {
            handled = false;

            if (args.KeyCode == System.Windows.Forms.Keys.Space)
            {
                Move_Boost = false;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.W)
            {
                Move_Forward = false;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.S)
            {
                Move_Backward = false;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.A)
            {
                Move_Left = false;
                handled = true;
            }

            if (args.KeyCode == System.Windows.Forms.Keys.D)
            {
                Move_Right = false;
                handled = true;
            }   
        }

        public void OnKeyPress(char @char, out bool handled)
        {
            handled = false;
        }

        public void OnMouseDown(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool handled)
        {
            handled = false;
        }

        public void OnMouseUp(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool handled)
        {
            handled = false;
        }

        public void OnMouseClick(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, System.Windows.Forms.MouseButtons mouseButtons, out bool handled)
        {
            handled = false;
        }

        public void OnMouseMoved(Rug.Game.Core.Rendering.View3D view, OpenTK.Vector2 mousePosition, out bool handled)
        {
            handled = true; 
        }

        public void OnMouseWheel(System.Windows.Forms.MouseEventArgs e, out bool handled)
        {
            handled = false;
        }

        public void Invalidate()
        {
            
        }

        System.Drawing.Point CenterPoint()
        {
            return new System.Drawing.Point(InputForm.Bounds.Left + (InputForm.Bounds.Width / 2), InputForm.Bounds.Top + (InputForm.Bounds.Height / 2));
        }

        void ResetMousePositionToCenter()
        {
            if (InputForm.ContainsFocus == false)
            {
                Look_Delta = new Vector2(0, 0); 
                return;
            }

            pointer_previous = pointer_current;
            pointer_current = System.Windows.Forms.Cursor.Position;

            System.Drawing.Point centerPos = CenterPoint();

            Look_Delta = new Vector2(pointer_current.X - centerPos.X, pointer_current.Y - centerPos.Y);         

            System.Windows.Forms.Cursor.Position = CenterPoint(); 
        }
    }
}
